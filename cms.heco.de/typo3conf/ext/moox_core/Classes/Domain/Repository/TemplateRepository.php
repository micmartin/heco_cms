<?php
namespace Neulandlotsen\MooxCore\Domain\Repository;

/**
 * This file is part of the "moox_core" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

use TYPO3\CMS\Extbase\Persistence\QueryInterface;
use Neulandlotsen\MooxCore\Domain\Repository\MooxRepository;

class TemplateRepository extends MooxRepository
{
	protected $defaultOrderings = ['title' => QueryInterface::ORDER_ASCENDING];

	/**
	 * Returns a constraint array created by a given filter array
	 *
	 * @param \TYPO3\CMS\Extbase\Persistence\QueryInterface $query
	 * @param array $filter
	 * @param array $constraints
	 * @return \TYPO3\CMS\Extbase\Persistence\Generic\Qom\ConstraintInterface|null
	 */
	protected function createFilterConstraints(\TYPO3\CMS\Extbase\Persistence\QueryInterface $query,$filter = NULL,$constraints = NULL)
	{
		if(is_null($constraints))
		{
			$constraints = [];
		}

		if(isset($filter['hidden']) && in_array($filter['hidden'],[0,1]))
		{
			$constraints[] = $query->equals('hidden', $filter['hidden']);
		}

		$searchFields = ['title'];
		if(isset($filter['query']) && $filter['query']!="")
		{
			$searchConstraints = [];
			foreach($searchFields AS $searchField)
			{
				$searchConstraints[] =  $query->like($searchField, "%".$filter['query']."%");
			}

			if(count($searchConstraints)>0)
			{
				if(count($searchConstraints)==1)
				{
					$constraints[] = $searchConstraints[0];
				}
				else
				{
					$constraints[] = $query->logicalOr($searchConstraints);
				}
			}
		}

		if(count($constraints)<1)
		{
			$constraints = NULL;
		}

		return $constraints;
	}

	/**
	 * Finds all templates (overwrite)
	 *
	 * @param bool $RespectStoragePage
	 * @return \TYPO3\CMS\Extbase\Persistence\QueryResultInterface The templates
	 */
	public function findAll($respectStoragePage = true)
	{
		$query = $this->createQuery();

		if(!$respectStoragePage)
		{
			$query->getQuerySettings()->setRespectStoragePage(FALSE);
		}

		return $query->execute();
	}

	/**
	 * Override default findByUid function to enable also the option to turn of
	 * the enableField setting
	 *
	 * @param int $uid id of record
	 * @param bool $respectEnableFields if set to false, hidden records are shown
	 * @return \Neulandlotsen\MooxAddress\Domain\Model\Template
	 */
	public function findByUid($uid, $respectEnableFields = TRUE)
	{
		$query = $this->createQuery();
		$query->getQuerySettings()->setRespectStoragePage(FALSE);
		$query->getQuerySettings()->setRespectSysLanguage(FALSE);
		$query->getQuerySettings()->setIgnoreEnableFields(!$respectEnableFields);

		return $query->matching(
			$query->logicalAnd(
				$query->equals('uid', $uid),
				$query->equals('deleted', 0)
			)
		)->execute()->getFirst();
	}
}
?>