<?php
defined('TYPO3_MODE') or die();

$ll = 'LLL:EXT:moox_news/Resources/Private/Language/locallang_db.xlf:';

return [
    'ctrl' => [
        'title' => $ll . 'tx_mooxnews_domain_model_target',
        'descriptionColumn' => 'notes',
        'label' => 'title',
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'cruser_id' => 'cruser_id',
        'default_sortby' => 'ORDER BY title',
        'delete' => 'deleted',
        'enablecolumns' => [
            'disabled' => 'hidden',
        ],
        'typeicon_classes' => [
            'default' => 'ext-news-target'
        ],
        'searchFields' => 'uid,title',
    ],
    'interface' => [
        'showRecordFieldList' => 'hidden,title'
    ],
    'columns' => [
        'pid' => [
            'label' => 'pid',
            'config' => [
                'type' => 'passthrough'
            ]
        ],
        'crdate' => [
            'label' => 'crdate',
            'config' => [
                'type' => 'passthrough',
            ]
        ],
        'tstamp' => [
            'label' => 'tstamp',
            'config' => [
                'type' => 'passthrough',
            ]
        ],
        'hidden' => [
            'exclude' => true,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.hidden',
            'config' => [
                'type' => 'check',
                'default' => 0
            ]
        ],
        'title' => [
            'exclude' => false,
            'label' => $ll . 'tx_mooxnews_domain_model_target.title',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'required,unique,trim',
            ],
			// special moox configuration
			'moox' => [
				'extkey' => 'moox_news_seminar',
				'backend' => [
					'type' => 'text',
				],
			],
        ],
        'seo_headline' => [
            'exclude' => true,
            'label' => $ll . 'tx_mooxnews_domain_model_target.seo.seo_headline',
            'config' => [
                'type' => 'input',
            ],
        ],
        'seo_title' => [
            'exclude' => true,
            'label' => $ll . 'tx_mooxnews_domain_model_target.seo.seo_title',
            'config' => [
                'type' => 'input',
            ],
        ],
        'seo_description' => [
            'exclude' => true,
            'label' => $ll . 'tx_mooxnews_domain_model_target.seo.seo_description',
            'config' => [
                'type' => 'text',
            ],
        ],
        'seo_text' => [
            'exclude' => true,
            'label' => $ll . 'tx_mooxnews_domain_model_target.seo.seo_text',
            'config' => [
                'type' => 'text',
            ],
            'defaultExtras' => 'richtext:rte_transform[mode=ts_css]',
        ],
		'slug' => [
			'exclude' => true,
			'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_tca.xlf:pages.slug',
			'displayCond' => 'USER:' . \TYPO3\CMS\Core\Compatibility\PseudoSiteTcaDisplayCondition::class . '->isInPseudoSite:pages:false',
			'config' => [
				'type' => 'slug',
				'size' => 50,
				'generatorOptions' => [
					'fields' => ['title'],
					'replacements' => [
						'/' => '-'
					],
				],
				'fallbackCharacter' => '-',
				'eval' => 'uniqueInSite',
				'default' => ''
			]
		],
        'notes' => [
            'label' => $ll . 'notes',
            'config' => [
                'type' => 'text',
                'rows' => 10,
                'cols' => 48
            ]
        ],
    ],
    'types' => [
        0 => [
            'showitem' => 'title, --palette--;;paletteCore,
            --div--;' . $ll . 'tx_mooxnews_domain_model_target.tabs.seo, seo_title, seo_description, seo_headline, seo_text,
            --div--;' . $ll . 'notes,
                    notes,
			--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.extended,'
        ]
    ],
    'palettes' => [
        'paletteCore' => [
            'showitem' => 'hidden,',
        ],
    ]
];
