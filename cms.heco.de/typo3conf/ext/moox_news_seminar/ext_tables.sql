#
# Table structure for table 'tx_mooxnewsseminar_domain_model_speaker'
#
CREATE TABLE tx_mooxnewsseminar_domain_model_speaker (

	uid int(11) NOT NULL auto_increment,
	pid int(11) DEFAULT '0' NOT NULL,
	tstamp int(11) DEFAULT '0' NOT NULL,
	crdate int(11) DEFAULT '0' NOT NULL,
	cruser_id int(11) DEFAULT '0' NOT NULL,
	sorting int(11) DEFAULT '0' NOT NULL,
	deleted tinyint(4) DEFAULT '0' NOT NULL,
	hidden tinyint(4) DEFAULT '0' NOT NULL,
	starttime int(11) unsigned DEFAULT '0' NOT NULL,
	endtime int(11) unsigned DEFAULT '0' NOT NULL,
	t3ver_oid int(11) DEFAULT '0' NOT NULL,
	t3ver_id int(11) DEFAULT '0' NOT NULL,
	t3_origuid int(11) DEFAULT '0' NOT NULL,
	t3ver_wsid int(11) DEFAULT '0' NOT NULL,
	t3ver_label varchar(30) DEFAULT '' NOT NULL,
	t3ver_state tinyint(4) DEFAULT '0' NOT NULL,
	t3ver_stage tinyint(4) DEFAULT '0' NOT NULL,
	t3ver_count int(11) DEFAULT '0' NOT NULL,
	t3ver_tstamp int(11) DEFAULT '0' NOT NULL,
  	t3ver_move_id int(11) DEFAULT '0' NOT NULL,
	sys_language_uid int(11) DEFAULT '0' NOT NULL,
	l10n_parent int(11) DEFAULT '0' NOT NULL,
	l10n_diffsource mediumblob,

	title varchar(255) DEFAULT '' NOT NULL,
	position text,
	description text,
	image int(11) DEFAULT '0' NOT NULL,

	PRIMARY KEY (uid),	
	KEY t3ver_oid (t3ver_oid,t3ver_wsid),
	KEY language (l10n_parent,sys_language_uid)

);

#
# Extend table structure for table 'tx_mooxnews_domain_model_news'
#
CREATE TABLE tx_mooxnews_domain_model_news (
	moox_news_seminar_startdate int(11) DEFAULT '0' NOT NULL,
	moox_news_seminar_enddate int(11) DEFAULT '0' NOT NULL,
	moox_news_seminar_timing int(11) DEFAULT '0' NOT NULL,
	moox_news_seminar_price varchar(50) DEFAULT NULL,
	moox_news_seminar_price_info text,
	moox_news_seminar_subscriber_min int(11) DEFAULT '0' NOT NULL,
	moox_news_seminar_subscriber_max int(11) DEFAULT '0' NOT NULL,
	moox_news_seminar_register_countdown int(11) DEFAULT '0' NOT NULL,
	moox_news_seminar_register_deadline int(11) DEFAULT '0' NOT NULL,
	moox_news_seminar_register_all_year tinyint(4) DEFAULT '0' NOT NULL,
	moox_news_seminar_search_index text,
	moox_news_seminar_speakers text,
);