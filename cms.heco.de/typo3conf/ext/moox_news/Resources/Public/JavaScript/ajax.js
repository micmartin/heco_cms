$(document).ready(function() {
    
	$(this).ajaxStart(function(){
          $("body").append("<div id='tx-moox-news-overlay'><div class='social_loader text-primary'><span class='fa fa-spinner fa-pulse fa-3x fa-fw margin-bottom'></span><span class='sr-only'>Loading news posts...</span></div></div>");
    });
	
	$(this).ajaxStop(function(){
          $("#tx-moox-news-overlay").remove();
    });
			
	initFilter();
	initPagination();
	
	$('.ajaxnews').each(function() {
		var cid = $(this).attr("id").replace("ajaxnews-c","");
		var pluginSelectorId = "ajaxnews-c"+cid;
		$('#'+pluginSelectorId+'-page-title').val(document.title);
		updatePagination(false,cid,pluginSelectorId);	
	});
	
	hash = location.hash.replace("#","");
	if(hash!=''){
		routing = hash.split("/");

		var cid = routing[0];
		var pluginSelectorId = "ajaxnews-c"+cid;	
		
		var mode = $('#'+pluginSelectorId+'-mode').val();
		var append = 0;
		if(mode=='append'){
			append = 1;
		}
			
		if(cid>0 && routing[1]>1){
			url = $('#'+pluginSelectorId+'-url').val();
			url += '&tx_mooxnews_pi4[action]=fetchList';
			page = routing[1];
			settings = $('#'+pluginSelectorId+'-settings').val();		
			
			$('#'+pluginSelectorId+' .pagination li').removeClass('active');		
			$('#'+pluginSelectorId+' .ajaxnews-paginate-'+page).each(function() {
				$(this).parent().addClass("active");
			});
			
			overwriteDemand = { };
			overwriteDemand['tx_mooxnews_pi4[page]'] = page;		
			overwriteDemand['tx_mooxnews_pi4[settings]'] = settings;
					
			$.ajax(url, {
				method: "POST",
				data: overwriteDemand,
				success: function(data) {
				
					$('#'+pluginSelectorId+' .ajaxnews-list-inner').html($(data));
					
					$('#'+pluginSelectorId+' .ajaxnews-fetch-detail').click(function(event) {		
						
						event.preventDefault();		
						
						url = $('#'+pluginSelectorId+'-url').val();
						url += '&tx_mooxnews_pi4[action]=fetchDetail';
						uid = $(this).attr("id");	
						uid = uid.replace("ajaxnews-fetch-detail-","");		
						url += '&tx_mooxnews_pi4[news]='+uid;
						
						settings = $('#'+pluginSelectorId+'-settings').val();
						overwriteDemand = { };
						overwriteDemand['tx_mooxnews_pi4[settings]'] = settings;
						
						$.ajax(url, {
							method: "POST",
							data: overwriteDemand,
							success: function(data) {
							
								$('#'+pluginSelectorId).prepend($(data));
								$('#'+pluginSelectorId+' .ajaxnews-list').hide();
								$('#'+pluginSelectorId+' .ajaxnews-filterset').hide();
								
								$('#'+pluginSelectorId+' .ajaxnews-close-detail').click(function(event) {
						
									event.preventDefault();
									
									uid = $(this).attr('id');		
									uid = uid.replace('ajaxnews-close-detail-','');
									
									$('#'+pluginSelectorId+' #ajaxnews-detail-'+uid).remove();
									$('#'+pluginSelectorId+' .ajaxnews-list').show();
									$('#'+pluginSelectorId+' .ajaxnews-filterset').show();
									
									page = $('#'+pluginSelectorId+'-page').val();
									if(page>1){					
										location.hash = cid+'/'+page;
									} else {
										location.hash = cid+'/1';
									}
									document.title = $('#'+pluginSelectorId+'-page-title').val();
											
								});
								
								$('#'+pluginSelectorId+' .lightbox-gallery').each(function() {
									$(this).magnificPopup({
										delegate: 'a',
										type: 'image',
										gallery: {
											enabled:true
										}
									});
								});
								
								page = $('#'+pluginSelectorId+'-page').val();
								title = encodeTitle($('#ajaxnews-request-title').val());
								
								$('#ajaxnews-request-title').remove();
								if(!append){
									location.hash = cid+'/'+page+'/'+uid+'/'+encodeURIComponent(title);
								} else {
									location.hash = cid+'/1/'+uid+'/'+encodeURIComponent(title);
								}
								document.title = document.title+' - '+title;
								
							},
							error: function() {
								alert('error');
							}
						});		
					});
					
					if(page>1){					
						location.hash = cid+'/'+page;
					} else {
						location.hash = cid+'/1';
					}
					document.title = $('#'+pluginSelectorId+'-page-title').val();

					$('#'+pluginSelectorId+'-page').val(page);

					updatePagination(false,cid,pluginSelectorId);
					
					if(routing.length>2){
			
						url = $('#'+pluginSelectorId+'-url').val();
						url += '&tx_mooxnews_pi4[action]=fetchDetail';
						uid = routing[2];	
						url += '&tx_mooxnews_pi4[news]='+uid;
						
						settings = $('#'+pluginSelectorId+'-settings').val();
						overwriteDemand = { };
						overwriteDemand['tx_mooxnews_pi4[settings]'] = settings;												
						
						$.ajax(url, {
							method: "POST",
							data: overwriteDemand,
							success: function(data) {
							
								$('#'+pluginSelectorId).prepend($(data));
								$('#'+pluginSelectorId+' .ajaxnews-list').hide();
								$('#'+pluginSelectorId+' .ajaxnews-filterset').hide();
								
								$('#'+pluginSelectorId+' .ajaxnews-close-detail').click(function(event) {
						
									event.preventDefault();
									
									uid = $(this).attr('id');		
									uid = uid.replace('ajaxnews-close-detail-','');
									
									$('#'+pluginSelectorId+' #ajaxnews-detail-'+uid).remove();
									$('#'+pluginSelectorId+' .ajaxnews-list').show();
									$('#'+pluginSelectorId+' .ajaxnews-filterset').show();
									
									page = $('#'+pluginSelectorId+'-page').val();
									if(page>1){					
										location.hash = cid+'/'.page;
									} else {
										location.hash = cid+'/1';
									}
									document.title = $('#'+pluginSelectorId+'-page-title').val();
											
								});
								
								page = $('#'+pluginSelectorId+'-page').val();
								title = encodeTitle($('#ajaxnews-request-title').val());
								
								$('#ajaxnews-request-title').remove();
								if(!append){
									location.hash = cid+'/'.page+'/'+uid+'/'+encodeURIComponent(title);
								} else {
									location.hash = cid+'/1/'+uid+'/'+encodeURIComponent(title);
								}
								document.title = document.title+' - '+title;
							},
							error: function() {
								alert('error');
							}
						});		
					} 		
				},
				error: function() {
					alert('error');
				}
			});
		} 
		if(cid>0 && routing.length>2 && routing[1]<2){
			
			url = $('#'+pluginSelectorId+'-url').val();
			url += '&tx_mooxnews_pi4[action]=fetchDetail';
			uid = routing[2];	
			url += '&tx_mooxnews_pi4[news]='+uid;
			
			settings = $('#'+pluginSelectorId+'-settings').val();
			overwriteDemand = { };
			overwriteDemand['tx_mooxnews_pi4[settings]'] = settings;
			
			$.ajax(url, {
				method: "POST",
				data: overwriteDemand,
				success: function(data) {
					
					$('#'+pluginSelectorId).prepend($(data));
					$('#'+pluginSelectorId+' .ajaxnews-list').hide();
					$('#'+pluginSelectorId+' .ajaxnews-filterset').hide();
					
					$('#'+pluginSelectorId+' .ajaxnews-close-detail').click(function(event) {
			
						event.preventDefault();
						
						uid = $(this).attr('id');		
						uid = uid.replace('ajaxnews-close-detail-','');
						
						$('#'+pluginSelectorId+' #ajaxnews-detail-'+uid).remove();
						$('#'+pluginSelectorId+' .ajaxnews-list').show();
						$('#'+pluginSelectorId+' .ajaxnews-filterset').show();
						
						page = $('#'+pluginSelectorId+'-page').val();
						if(page>1){					
							location.hash = cid+'/'+page;
						} else {
							location.hash = cid+'/1';
						}
						document.title = $('#'+pluginSelectorId+'-page-title').val();
								
					});
					
					page = $('#'+pluginSelectorId+'-page').val();
					title = encodeTitle($('#ajaxnews-request-title').val());
					
					$('#ajaxnews-request-title').remove();
					if(!append){
						location.hash = cid+'/'+page+'/'+uid+'/'+encodeURIComponent(title);
					} else {
						location.hash = cid+'/1/'+uid+'/'+encodeURIComponent(title);
					}
					document.title = document.title+' - '+title;
				},
				error: function() {
					alert('error');
				}
			});		
		} 		
	}
	
	$('.ajaxnews-fetch-detail').click(function(event) {		
		
		event.preventDefault();
		
		var cid = $(this).parents(".ajaxnews").attr("id").replace("ajaxnews-c","");
		var pluginSelectorId = "ajaxnews-c"+cid;
		
		var mode = $('#'+pluginSelectorId+'-mode').val();
		var append = 0;
		if(mode=='append'){
			append = 1;
		}
		
		url = $('#'+pluginSelectorId+'-url').val();
		url += '&tx_mooxnews_pi4[action]=fetchDetail';
		uid = $(this).attr("id");	
		uid = uid.replace("ajaxnews-fetch-detail-","");		
		url += '&tx_mooxnews_pi4[news]='+uid;
				
		settings = $('#'+pluginSelectorId+'-settings').val();
		overwriteDemand = { };
		overwriteDemand['tx_mooxnews_pi4[settings]'] = settings;
						
		$.ajax(url, {
			method: "POST",
			data: overwriteDemand,
			success: function(data) {
				$('#'+pluginSelectorId).prepend($(data));
				$('#'+pluginSelectorId+' .ajaxnews-list').hide();
				$('#'+pluginSelectorId+' .ajaxnews-filterset').hide();
				
				$('#'+pluginSelectorId+' .ajaxnews-close-detail').click(function(event) {
		
					event.preventDefault();
					
					uid = $(this).attr('id');		
					uid = uid.replace('ajaxnews-close-detail-','');
					$('#'+pluginSelectorId+' #ajaxnews-detail-'+uid).remove();
					$('#'+pluginSelectorId+' .ajaxnews-list').show();
					$('#'+pluginSelectorId+' .ajaxnews-filterset').show();
					
					page = $('#'+pluginSelectorId+'-page').val();
					if(page>1){					
						location.hash = cid+'/'+page;
					} else {
						location.hash = cid+'/1';
					}
					document.title = $('#'+pluginSelectorId+'-page-title').val();
							
				});
				
				$('#'+pluginSelectorId+' .lightbox-gallery').each(function() {
					$(this).magnificPopup({
						delegate: 'a',
						type: 'image',
						gallery: {
							enabled:true
						}
					});
				});
				
				page = $('#'+pluginSelectorId+'-page').val();
				title = encodeTitle($('#ajaxnews-request-title').val());

				$('#ajaxnews-request-title').remove();
				if(!append){
					location.hash = cid+'/'+page+'/'+uid+'/'+encodeURIComponent(title);
				} else {
					location.hash = cid+'/1/'+uid+'/'+encodeURIComponent(title);
				}
				document.title = document.title+' - '+title;
			},
			error: function() {
				alert('error');
			}
		});		
	});
	
	$('.ajaxnews-paginate').click(function(event) {
		
		event.preventDefault();
		
		var cid = $(this).parents(".ajaxnews").attr("id").replace("ajaxnews-c","");
		var pluginSelectorId = "ajaxnews-c"+cid;
		
		var mode = $('#'+pluginSelectorId+'-mode').val();
		var append = 0;
		if(mode=='append'){
			append = 1;
		}
		
		url = $('#'+pluginSelectorId+'-url').val();
		url += '&tx_mooxnews_pi4[action]=fetchList';
		page = $(this).attr("id");				
		page = page.replace("ajaxnews-paginate-","");
		if(page=='next' || page=='append'){			
			page = parseInt($('#'+pluginSelectorId+'-page').val());
			page = page+1;
		} else if(page=='previous'){
			page = parseInt($('#'+pluginSelectorId+'-page').val());
			page = page-1;
			if(page<1){
				page = 1;
			}
		}
		settings = $('#'+pluginSelectorId+'-settings').val();		
		
		$('#'+pluginSelectorId+' .pagination li').removeClass('active');		
		$('#'+pluginSelectorId+' .ajaxnews-paginate-'+page).each(function() {
		  $(this).parent().addClass("active");
		});
		
		overwriteDemand = { };
		overwriteDemand['tx_mooxnews_pi4[page]'] = page;		
		overwriteDemand['tx_mooxnews_pi4[settings]'] = settings;
		
		$('#'+pluginSelectorId+' .ajaxnews-filter').each(function() {
			filterName = $(this).attr("id");				
			filterName = filterName.replace(pluginSelectorId+'-filter-',"");
			filterValue = $(this).val();
			if(!filterValue){
				filterValue = $(this).children("select").val();
			}
			if(filterName!="" && filterValue!=""){
				overwriteDemand['tx_mooxnews_pi4[overwriteDemand]['+filterName+']'] = filterValue;
			}
			if(filterName = 'categories'){
				categoryConjunction = $('#'+pluginSelectorId+' .ajaxnews-filter-categoryConjunction').val();			
				if(categoryConjunction){
					overwriteDemand['tx_mooxnews_pi4[overwriteDemand][categoryConjunction]'] = categoryConjunction;
				}
				includeSubCategories = $('#'+pluginSelectorId+' .ajaxnews-filter-includeSubCategories').val();			
				if(includeSubCategories){
					overwriteDemand['tx_mooxnews_pi4[overwriteDemand][includeSubCategories]'] = includeSubCategories;
				}				
			}
		});
		
		$.ajax(url, {
			method: "POST",
			data: overwriteDemand,
			success: function(data) {								
				
				if(append){
					$('#'+pluginSelectorId+' .ajaxnews-list-inner').append($(data));					
				} else {
					$('#'+pluginSelectorId+' .ajaxnews-list-inner').html($(data));					
				}				
				
				links = $($('#'+pluginSelectorId+' .ajaxnews-fetch-detail').get().reverse());				
				if(append){					
					links.off('click');
				}
				links.click(function(event) {						
					
					event.preventDefault();		
					
					url = $('#'+pluginSelectorId+'-url').val();
					url += '&tx_mooxnews_pi4[action]=fetchDetail';
					uid = $(this).attr("id");	
					uid = uid.replace("ajaxnews-fetch-detail-","");		
					url += '&tx_mooxnews_pi4[news]='+uid;
					
					settings = $('#'+pluginSelectorId+'-settings').val();
					overwriteDemand = { };
					overwriteDemand['tx_mooxnews_pi4[settings]'] = settings;
						
					$.ajax(url, {
						method: "POST",
						data: overwriteDemand,
						success: function(data) {
							$('#'+pluginSelectorId).prepend($(data));
							$('#'+pluginSelectorId+' .ajaxnews-list').hide();
							$('#'+pluginSelectorId+' .ajaxnews-filterset').hide();
							
							$('#'+pluginSelectorId+' .ajaxnews-close-detail').click(function(event) {
					
								event.preventDefault();
								
								uid = $(this).attr('id');		
								uid = uid.replace('ajaxnews-close-detail-','');
								
								$('#'+pluginSelectorId+' #ajaxnews-detail-'+uid).remove();
								$('#'+pluginSelectorId+' .ajaxnews-list').show();
								$('#'+pluginSelectorId+' .ajaxnews-filterset').show();
								
								page = $('#'+pluginSelectorId+'-page').val();
								if(!append){
									if(page>1){					
										location.hash = cid+'/'+page;
									} else {
										location.hash = cid+'/1';
									}
								} else {
									location.hash = '';
								}
								document.title = $('#'+pluginSelectorId+'-page-title').val();
										
							});
							
							$('#'+pluginSelectorId+' .lightbox-gallery').each(function() {
								$(this).magnificPopup({
									delegate: 'a',
									type: 'image',
									gallery: {
										enabled:true
									}
								});
							});
							
							page = $('#'+pluginSelectorId+'-page').val();
							title = encodeTitle($('#ajaxnews-request-title').val());
							
							$('#ajaxnews-request-title').remove();
							if(!append){
								location.hash = cid+'/'+page+'/'+uid+'/'+encodeURIComponent(title);
							} else {
								location.hash = cid+'/1/'+uid+'/'+encodeURIComponent(title);
							}
							document.title = document.title+' - '+title;
							
						},
						error: function() {
							alert('error');
						}
					});		
				});								
				
				if(!append){			
					if(page>1){					
						location.hash = cid+'/'+page;
					} else {
						location.hash = cid+'/1';
					}
				}
				document.title = $('#'+pluginSelectorId+'-page-title').val();

				$('#'+pluginSelectorId+'-page').val(page);

				updatePagination(false,cid,pluginSelectorId);
			},
			error: function() {
				alert('error');
			}
		});		
	});
});

function initFilter(){
		
	$('.ajaxnews-filter-submit').remove();	

	$('.ajaxnews-filter-form').submit(function( event ) {
		event.preventDefault();
	});
	
	$('.ajaxnews-filter-selectivity .ajaxnews-filter-select').each(function() {
		$(this).removeClass('form-control');
		$(this).selectivity({
			 allowClear: false,
		});		
	});
	
	$('.ajaxnews-char-navigation .ajaxnews-char').click(function(event) {
		selector = "#"+$(this).data("field");
		value = $(this).data("char");
		$(selector).val(value);
		$(selector).change();
		$('.ajaxnews-char-navigation .ajaxnews-char').removeClass("ajaxnews-char-active");
		$(this).addClass("ajaxnews-char-active");
	});
		
	$('.ajaxnews-filter-selectivity .ajaxnews-filter').change(function(event) {
		
		event.preventDefault();
		
		var cid = $(this).parents(".ajaxnews").attr("id").replace("ajaxnews-c","");
		var pluginSelectorId = "ajaxnews-c"+cid;
		var changedFilterName = $(this).attr("id").replace(pluginSelectorId+'-filter-',"");
				
		var mode = $('#'+pluginSelectorId+'-mode').val();
		var append = 0;
		if(mode=='append'){
			append = 1;
		}
		
		updateDependingFilters(pluginSelectorId,changedFilterName);
		updateFilterDisplays(pluginSelectorId);
		
		url = $('#'+pluginSelectorId+'-url').val();
		url += '&tx_mooxnews_pi4[action]=fetchList';
		settings = $('#'+pluginSelectorId+'-settings').val();		
		page = 1;
		$('#'+pluginSelectorId+' .pagination li').removeClass('active');		
		$('#'+pluginSelectorId+' .ajaxnews-paginate-'+page).each(function() {
		  $(this).parent().addClass("active");
		});
		
		overwriteDemand = { };
		overwriteDemand['tx_mooxnews_pi4[page]'] = page;		
		overwriteDemand['tx_mooxnews_pi4[settings]'] = settings;
		
		showResetButton = false;
		$('#'+pluginSelectorId+' .ajaxnews-filter-select').each(function() {
			filterName = $(this).attr("id");
			filterName = filterName.replace(pluginSelectorId+'-filter-',"");
			filterValue = $(this).children("select").val();
			if(filterName!="" && filterValue!=""){
				if(filterValue!="all"){
					showResetButton = true;
				}
				overwriteDemand['tx_mooxnews_pi4[overwriteDemand]['+filterName+']'] = filterValue;
			}
			if(filterName = 'categories'){
				categoryConjunction = $('#'+pluginSelectorId+' .ajaxnews-filter-categoryConjunction').val();			
				if(categoryConjunction){
					overwriteDemand['tx_mooxnews_pi4[overwriteDemand][categoryConjunction]'] = categoryConjunction;
				}
				includeSubCategories = $('#'+pluginSelectorId+' .ajaxnews-filter-includeSubCategories').val();			
				if(includeSubCategories){
					overwriteDemand['tx_mooxnews_pi4[overwriteDemand][includeSubCategories]'] = includeSubCategories;
				}				
			}
		});
		$('#'+pluginSelectorId+' .ajaxnews-filter-input').each(function() {
			filterName = $(this).attr("id");				
			filterName = filterName.replace(pluginSelectorId+'-filter-',"");
			filterValue = $(this).val();
			if(filterName!="" && filterValue!=""){
				if(filterValue!="all"){
					showResetButton = true;
				}
				overwriteDemand['tx_mooxnews_pi4[overwriteDemand]['+filterName+']'] = filterValue;
			}			
		});
		$('#'+pluginSelectorId+' .ajaxnews-filter-datepicker').each(function() {
			filterName = $(this).attr("id");				
			filterName = filterName.replace(pluginSelectorId+'-filter-',"");
			filterValue = $(this).val();
			if(filterName!="" && filterValue!=""){
				if(filterValue!="all"){
					showResetButton = true;
				}
				overwriteDemand['tx_mooxnews_pi4[overwriteDemand]['+filterName+']'] = filterValue;
			}			
		});
		if(showResetButton){
			$('#'+pluginSelectorId+' .ajaxnews-filter-reset').show();
		} else {
			$('#'+pluginSelectorId+' .ajaxnews-filter-reset').hide();
		}
					
		$.ajax(url, {
			method: "POST",
			data: overwriteDemand,
			success: function(data) {
			
				$('#'+pluginSelectorId+' .ajaxnews-list-inner').html($(data));
				
				$('#'+pluginSelectorId+' .ajaxnews-fetch-detail').click(function(event) {		
					
					event.preventDefault();		
					
					url = $('#'+pluginSelectorId+'-url').val();
					url += '&tx_mooxnews_pi4[action]=fetchDetail';
					uid = $(this).attr("id");	
					uid = uid.replace("ajaxnews-fetch-detail-","");		
					url += '&tx_mooxnews_pi4[news]='+uid;
						
					settings = $('#'+pluginSelectorId+'-settings').val();
					overwriteDemand = { };
					overwriteDemand['tx_mooxnews_pi4[settings]'] = settings;
						
					$.ajax(url, {
						method: "POST",
						data: overwriteDemand,
						success: function(data) {
							$('#'+pluginSelectorId).prepend($(data));
							$('#'+pluginSelectorId+' .ajaxnews-list').hide();
							$('#'+pluginSelectorId+' .ajaxnews-filterset').hide();
							
							$('#'+pluginSelectorId+' .ajaxnews-close-detail').click(function(event) {
					
								event.preventDefault();
								
								uid = $(this).attr('id');		
								uid = uid.replace('ajaxnews-close-detail-','');
								
								$('#'+pluginSelectorId+' #ajaxnews-detail-'+uid).remove();
								$('#'+pluginSelectorId+' .ajaxnews-list').show();
								$('#'+pluginSelectorId+' .ajaxnews-filterset').show();
								
								page = $('#'+pluginSelectorId+'-page').val();
								if(!append){
									if(page>1){					
										location.hash = cid+'/'+page;
									} else {
										location.hash = cid+'/1';
									}
								} else {
									location.hash = '';
								}
								document.title = $('#'+pluginSelectorId+'-page-title').val();
										
							});	

							$('#'+pluginSelectorId+' .lightbox-gallery').each(function() {
								$(this).magnificPopup({
									delegate: 'a',
									type: 'image',
									gallery: {
										enabled:true
									}
								});
							});	
								
							page = $('#'+pluginSelectorId+'-page').val();
							title = encodeTitle($('#ajaxnews-request-title').val());
							
							$('#ajaxnews-request-title').remove();
							if(!append){
								location.hash = cid+'/'+page+'/'+uid+'/'+encodeURIComponent(title);
							} else {
								location.hash = cid+'/1/'+uid+'/'+encodeURIComponent(title);
							}
							document.title = document.title+' - '+title;
						},
						error: function() {
							alert('error');
						}
					});		
				});
				
				append = 0;
				if($('#'+pluginSelectorId+'-mode').val()=='append'){
					append = 1;
				}
				
				if(!append){
					location.hash = cid+'/1';
				} else {
					location.hash = '';
				}
				document.title = $('#'+pluginSelectorId+'-page-title').val();
								
				$('#'+pluginSelectorId+'-page').val(page);				
				
				updatePagination(true,cid,pluginSelectorId);
			},
			error: function() {
				alert('error');
			}
		});	
	});
	
	$('.ajaxnews-filter-default .ajaxnews-filter').change(function(event) {
		
		event.preventDefault();
		
		var cid = $(this).parents(".ajaxnews").attr("id").replace("ajaxnews-c","");
		var pluginSelectorId = "ajaxnews-c"+cid;
		
		var mode = $('#'+pluginSelectorId+'-mode').val();
		var append = 0;
		if(mode=='append'){
			append = 1;
		}
		
		url = $('#'+pluginSelectorId+'-url').val();
		url += '&tx_mooxnews_pi4[action]=fetchList';
		settings = $('#'+pluginSelectorId+'-settings').val();		
		page = 1;
		$('#'+pluginSelectorId+' .pagination li').removeClass('active');		
		$('#'+pluginSelectorId+' .ajaxnews-paginate-'+page).each(function() {
		  $(this).parent().addClass("active");
		});
		
		overwriteDemand = { };
		overwriteDemand['tx_mooxnews_pi4[page]'] = page;		
		overwriteDemand['tx_mooxnews_pi4[settings]'] = settings;
		
		showResetButton = false;
		$('#'+pluginSelectorId+' .ajaxnews-filter').each(function() {
			filterName = $(this).attr("id");				
			filterName = filterName.replace(pluginSelectorId+'-filter-',"");
			filterValue = $(this).val();
			if(!filterValue){
				filterValue = $(this).children("select").val();
			}			
			if(filterName!="" && filterValue!=""){
				if(filterValue!="all"){
					showResetButton = true;
				}
				overwriteDemand['tx_mooxnews_pi4[overwriteDemand]['+filterName+']'] = filterValue;
			}
			if(filterName = 'categories'){
				categoryConjunction = $('#'+pluginSelectorId+' .ajaxnews-filter-categoryConjunction').val();			
				if(categoryConjunction){
					overwriteDemand['tx_mooxnews_pi4[overwriteDemand][categoryConjunction]'] = categoryConjunction;
				}
				includeSubCategories = $('#'+pluginSelectorId+' .ajaxnews-filter-includeSubCategories').val();			
				if(includeSubCategories){
					overwriteDemand['tx_mooxnews_pi4[overwriteDemand][includeSubCategories]'] = includeSubCategories;
				}				
			}
		});
		if(showResetButton){
			$('#'+pluginSelectorId+' .ajaxnews-filter-reset').show();
		} else {
			$('#'+pluginSelectorId+' .ajaxnews-filter-reset').hide();
		}
					
		$.ajax(url, {
			method: "POST",
			data: overwriteDemand,
			success: function(data) {
			
				$('#'+pluginSelectorId+' .ajaxnews-list-inner').html($(data));
				
				$('#'+pluginSelectorId+' .ajaxnews-fetch-detail').click(function(event) {		
					
					event.preventDefault();		
					
					url = $('#'+pluginSelectorId+'-url').val();
					url += '&tx_mooxnews_pi4[action]=fetchDetail';
					uid = $(this).attr("id");	
					uid = uid.replace("ajaxnews-fetch-detail-","");		
					url += '&tx_mooxnews_pi4[news]='+uid;
						
					settings = $('#'+pluginSelectorId+'-settings').val();
					overwriteDemand = { };
					overwriteDemand['tx_mooxnews_pi4[settings]'] = settings;
						
					$.ajax(url, {
						method: "POST",
						data: overwriteDemand,
						success: function(data) {
							$('#'+pluginSelectorId).prepend($(data));
							$('#'+pluginSelectorId+' .ajaxnews-list').hide();
							$('#'+pluginSelectorId+' .ajaxnews-filterset').hide();
							
							$('#'+pluginSelectorId+' .ajaxnews-close-detail').click(function(event) {
					
								event.preventDefault();
								
								uid = $(this).attr('id');		
								uid = uid.replace('ajaxnews-close-detail-','');
								
								$('#'+pluginSelectorId+' #ajaxnews-detail-'+uid).remove();
								$('#'+pluginSelectorId+' .ajaxnews-list').show();
								$('#'+pluginSelectorId+' .ajaxnews-filterset').show();
								
								page = $('#'+pluginSelectorId+'-page').val();
								if(page>1){					
									location.hash = cid+'/'+page;
								} else {
									location.hash = cid+'/1';
								}
								document.title = $('#'+pluginSelectorId+'-page-title').val();
										
							});
							
							$('#'+pluginSelectorId+' .lightbox-gallery').each(function() {
								$(this).magnificPopup({
									delegate: 'a',
									type: 'image',
									gallery: {
										enabled:true
									}
								});
							});
							
							page = $('#'+pluginSelectorId+'-page').val();
							title = encodeTitle($('#ajaxnews-request-title').val());
							
							$('#ajaxnews-request-title').remove();
							if(!append){
								location.hash = cid+'/'+page+'/'+uid+'/'+encodeURIComponent(title);
							} else {
								location.hash = cid+'/1/'+uid+'/'+encodeURIComponent(title);
							}
							document.title = document.title+' - '+title;
						},
						error: function() {
							alert('error');
						}
					});		
				});
				
				append = 0;
				if($('#'+pluginSelectorId+'-mode').val()=='append'){
					append = 1;
				}
				
				if(!append){
					location.hash = cid+'/1';
				} else {
					location.hash = '';
				}
				document.title = $('#'+pluginSelectorId+'-page-title').val();
								
				$('#'+pluginSelectorId+'-page').val(page);				
				
				updatePagination(true,cid,pluginSelectorId);
			},
			error: function() {
				alert('error');
			}
		});
	});
}

function initPagination(){
	$('ul.ajaxnews-pagination li.next').hide();
	$('ul.ajaxnews-pagination li.previous').hide();
	$('ul.ajaxnews-pagination li.first').hide();
	$('ul.ajaxnews-pagination li.last').hide();
}

function updatePagination(filtered,cid,pluginSelectorId){
	
	count	= parseInt($('#'+pluginSelectorId+'-count').val());
	perpage = parseInt($('#'+pluginSelectorId+'-perpage').val());
	page 	= parseInt($('#'+pluginSelectorId+'-page').val());
	pages 	= parseInt($('#'+pluginSelectorId+'-pages').val());
	
	if(filtered){	

		pages = parseInt($('#ajaxnews-filter-pages').val());		
		count = parseInt($('#ajaxnews-filter-count').val());				
		
		$('#'+pluginSelectorId+'pages').val(pages);
		
		$('#'+pluginSelectorId+' .ajaxnews-paginate-last a').each(function() {	
			$(this).attr('id','ajaxnews-paginate-' + pages);
		});
		
		$('#'+pluginSelectorId+' .ajaxnews-paginate-pages').each(function() {	
			pageNum = parseInt($(this).attr("id").replace("ajaxnews-paginate-",""));
			if(pageNum>pages){	 
				$(this).parent().hide();
			} else {
				$(this).parent().show();
			}
		});
	}
	
	if(pages>1){
		if(page>1){
			$('#'+pluginSelectorId+' .ajaxnews-paginate-first').show();
			$('#'+pluginSelectorId+' .ajaxnews-paginate-previous').show();
		} else {
			$('#'+pluginSelectorId+' .ajaxnews-paginate-first').hide();
			$('#'+pluginSelectorId+' .ajaxnews-paginate-previous').hide();
		}
		if(page<pages){
			$('#'+pluginSelectorId+' .ajaxnews-paginate-last').show();
			$('#'+pluginSelectorId+' .ajaxnews-paginate-next').show();
		} else {
			$('#'+pluginSelectorId+' .ajaxnews-paginate-last').hide();
			$('#'+pluginSelectorId+' .ajaxnews-paginate-next').hide();
		}
		$('#'+pluginSelectorId+' ul.ajaxnews-pagination').show();		
	} else {
		$('#'+pluginSelectorId+' .ajaxnews-paginate-first').hide();
		$('#'+pluginSelectorId+' .ajaxnews-paginate-previous').hide();
		$('#'+pluginSelectorId+' .ajaxnews-paginate-last').hide();
		$('#'+pluginSelectorId+' .ajaxnews-paginate-next').hide();
		$('#'+pluginSelectorId+' ul.ajaxnews-pagination').hide();		
	}
	
	if(filtered){
		$('#ajaxnews-filter-pages').remove();
		$('#ajaxnews-filter-count').remove();
	}
}

function updateDependingFilters(pluginSelectorId,triggerField){
	
	var triggerFieldValue = $('#'+pluginSelectorId+'-filter-'+triggerField).children("select").val();
	
	$('#'+pluginSelectorId+' .ajaxnews-filter-depends-'+triggerField).each(function() {			
		var dependingFilterId 		= $(this).attr("id");	
		dependingFilterName 		= $(this).attr("id").replace(pluginSelectorId+'-filter-',"");
		allFilterClasses 			= $(this).attr("class").split(" ");
		dependingFilterClasses 		= new Array();
		dependingFilterClassesIndex = 0;
		for (index = 0; index < allFilterClasses.length; ++index) {
			if(allFilterClasses[index].indexOf('ajaxnews-filter-depends-')===0){
				dependingFilterClasses[dependingFilterClassesIndex] = allFilterClasses[index].replace("ajaxnews-filter-depends-","");
				dependingFilterClassesIndex 						= dependingFilterClassesIndex+1;
			}
		}
				
		url = $('#'+pluginSelectorId+'-url').val();
		url += '&tx_mooxnews_pi4[action]=fetchFilter';					
		settings = $('#'+pluginSelectorId+'-settings').val();
		dependDemand = { };
		dependDemand['tx_mooxnews_pi4[filter]'] 	= dependingFilterName;
		dependDemand['tx_mooxnews_pi4[settings]'] 	= settings;
		
		$('#'+pluginSelectorId+' .ajaxnews-filter-select').each(function() {
			filterName = $(this).attr("id");
			filterName = filterName.replace(pluginSelectorId+'-filter-',"");
			filterValue = $(this).children("select").val();
			if(filterName!="" && filterValue!="" && $.inArray(filterName,dependingFilterClasses)>-1){				
				dependDemand['tx_mooxnews_pi4[dependDemand]['+filterName+']'] = filterValue;
			}			
		});
		
		$.ajax(url, {
			method: "POST",
			data: dependDemand,
			success: function(data) {
				options = $(data).find('option');
				items 	= new Array();
				cnt 	= 0;
				
				current = $('#'+pluginSelectorId+' #'+dependingFilterId).selectivity('data');
				
				options.each(function() {										
					item		 	= {}
					item["id"] 		= $(this).val();
					item["text"]  	= $(this).text();
					items[cnt] 		= item					
					cnt++;
					if(triggerFieldValue!='all' && current["id"]==$(this).val()){
						setitem = current;
					} else {
						if($(this).val()=='all'){
							setitem = {};
							setitem["id"]		= $(this).val();
							setitem["text"]		= $(this).text();
						}	
					}	
				});
				if(options.length>0){
					$('#'+pluginSelectorId+' #'+dependingFilterId).selectivity({
						items: items,					
					});
				}
				if(setitem["text"]!=""){
					$('#'+pluginSelectorId+' #'+dependingFilterId).selectivity('data', setitem);
				} else {
					$('#'+pluginSelectorId+' #'+dependingFilterId).selectivity('data', { id: 'all', text: ''});
				}
				if(options.length<2){
					$('#'+pluginSelectorId+' #'+dependingFilterId).hide();
				} else {
					$('#'+pluginSelectorId+' #'+dependingFilterId).css( "display", "inline-block" );					
				}
			},
			error: function() {
				alert('error');
			}
		});
		
	});
}

function updateFilterDisplays(pluginSelectorId){
	displays = $('#'+pluginSelectorId+' .ajaxnews-filter-displays');
	displays.each(function() {			
		
		hide = false;
		
		allDisplayClasses 	= $(this).attr("class").split(" ");
		displayClasses 		= new Array();
		displayClassesIndex = 0;
		
		for (index = 0; index < allDisplayClasses.length; ++index) {
			if(allDisplayClasses[index].indexOf('ajaxnews-filter-displays-')===0){
				displayClasses[displayClassesIndex] = allDisplayClasses[index].replace("ajaxnews-filter-displays-","");
				displayClassesIndex = displayClassesIndex+1;
			}
		}
		
		for (index = 0; index < displayClasses.length; ++index) {
			filter = $('#'+pluginSelectorId+'-filter-'+displayClasses[index]);			
			filterValue = filter.val();
			if(!filterValue){
				filterValue = filter.children("select").val();
			}
			if(filterValue=="all"){
				hide = true;
				break;
			}
		}
		
		if(hide){
			$(this).addClass("hidden")
		} else {
			$(this).removeClass("hidden")
		}
		
	});
}

function encodeTitle(title){
	
	title = title.replace(/ /g,'-');
	title = title.replace(/Ä/g,'Ae');
	title = title.replace(/Ü/g,'Ue');
	title = title.replace(/Ö/g,'Oe');
	title = title.replace(/ä/g,'ae');
	title = title.replace(/ü/g,'ue');
	title = title.replace(/ö/g,'oe');
	title = title.replace(/ß/g,'ss');
	title = title.replace(/\//g,'-');
	title = title.replace(/\\/g,'-');
	title = title.replace(/„/g,'');
	title = title.replace(/“/g,'');
	title = title.replace(/"/g,'');
	title = title.replace(/:/g,'');
	title = title.replace(/\./g,'');
	title = title.replace(/\,/g,'');
	title = title.replace(/\!/g,'');
	title = title.replace(/\?/g,'');
	title = title.replace(/\[/g,'');
	title = title.replace(/\]/g,'');
	title = title.replace(/\(/g,'');
	title = title.replace(/\)/g,'');
	title = title.replace(/\{/g,'');
	title = title.replace(/\}/g,'');
	
	return title.toLowerCase();
}