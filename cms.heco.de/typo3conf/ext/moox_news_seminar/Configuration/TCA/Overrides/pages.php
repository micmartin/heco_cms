<?php
if (!defined('TYPO3_MODE')) {
	die ('Access denied.');
}

// Override news icon
$GLOBALS['TCA']['pages']['columns']['module']['config']['items'][] = [
    0 => 'LLL:EXT:moox_news_seminar/Resources/Private/Language/locallang_be.xlf:folder-name',
    1 => 'mxspeakers',
    2 => 'apps-pagetree-folder-contains-mxspeakers'
];
$GLOBALS['TCA']['pages']['ctrl']['typeicon_classes']['contains-mxspeakers'] = 'apps-pagetree-folder-contains-mxspeakers';