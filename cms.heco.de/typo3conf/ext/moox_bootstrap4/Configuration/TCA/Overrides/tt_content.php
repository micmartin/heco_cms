<?php
if (!defined('TYPO3_MODE'))
{
	die ('Access denied.');
}

// Set language source file
$ll = 'LLL:EXT:moox_bootstrap4/Resources/Private/Language/locallang.xlf:';

$visibilityItems = [
	[ $ll.'tt_content.tx_mooxbootstrap4_visibility.xs', 'hidden-xs' ],
	[ $ll.'tt_content.tx_mooxbootstrap4_visibility.sm', 'hidden-sm' ],
	[ $ll.'tt_content.tx_mooxbootstrap4_visibility.md', 'hidden-md' ],
	[ $ll.'tt_content.tx_mooxbootstrap4_visibility.lg', 'hidden-lg' ],
	[ $ll.'tt_content.tx_mooxbootstrap4_visibility.xl', 'hidden-xl' ],
	[ $ll.'tt_content.tx_mooxbootstrap4_visibility.print', 'hidden-print' ],
	[ $ll.'tt_content.tx_mooxbootstrap4_visibility.minimal', 'hidden-minimal' ],
];

if(\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::isLoaded('moox_news'))
{
	$visibilityItems[] = [ $ll.'tt_content.tx_mooxbootstrap4_visibility.moox_news_detail', 'hidden-moox_news_detail' ];
	$visibilityItems[] = [ $ll.'tt_content.tx_mooxbootstrap4_visibility.moox_news_filter', 'hidden-moox_news_filter' ];
	$visibilityItems[] = [ $ll.'tt_content.tx_mooxbootstrap4_visibility.moox_news_pagination', 'hidden-moox_news_pagination' ];
}

$cardItems = [
	[ $ll.'tt_content.tx_mooxbootstrap4_card.none', '' ],
	[ $ll.'tt_content.tx_mooxbootstrap4_card.default', 'card' ],
	[ $ll.'tt_content.tx_mooxbootstrap4_card.bg-primary', 'card text-white bg-primary' ],
	[ $ll.'tt_content.tx_mooxbootstrap4_card.bg-secondary', 'card text-white bg-secondary' ],
	[ $ll.'tt_content.tx_mooxbootstrap4_card.bg-success', 'card text-white bg-success' ],
	[ $ll.'tt_content.tx_mooxbootstrap4_card.bg-danger', 'card text-white bg-danger' ],
	[ $ll.'tt_content.tx_mooxbootstrap4_card.bg-warning', 'card text-white bg-warning' ],
	[ $ll.'tt_content.tx_mooxbootstrap4_card.bg-info', 'card text-white bg-info' ],
	[ $ll.'tt_content.tx_mooxbootstrap4_card.bg-light', 'card bg-light' ],
	[ $ll.'tt_content.tx_mooxbootstrap4_card.bg-dark', 'card text-white bg-dark' ],
	[ $ll.'tt_content.tx_mooxbootstrap4_card.border-primary', 'card border-primary' ],
	[ $ll.'tt_content.tx_mooxbootstrap4_card.border-secondary', 'card border-secondary' ],
	[ $ll.'tt_content.tx_mooxbootstrap4_card.border-success', 'card border-success' ],
	[ $ll.'tt_content.tx_mooxbootstrap4_card.border-danger', 'card border-danger' ],
	[ $ll.'tt_content.tx_mooxbootstrap4_card.border-warning', 'card border-warning' ],
	[ $ll.'tt_content.tx_mooxbootstrap4_card.border-info', 'card border-info' ],
	[ $ll.'tt_content.tx_mooxbootstrap4_card.border-light', 'card border-light' ],
	[ $ll.'tt_content.tx_mooxbootstrap4_card.border-dark', 'card border-dark' ],
	[ $ll.'tt_content.tx_mooxbootstrap4_card.blockquote', 'blockquote' ],
];

$shadowItems = [
	[ $ll.'tt_content.tx_mooxbootstrap4_shadow.none', '' ],
	[ $ll.'tt_content.tx_mooxbootstrap4_shadow.shadow-sm', 'shadow-sm' ],
	[ $ll.'tt_content.tx_mooxbootstrap4_shadow.shadow', 'shadow' ],
	[ $ll.'tt_content.tx_mooxbootstrap4_shadow.shadow-lg', 'shadow-lg' ],
];

$containerItems = [
	[ $ll.'tt_content.tx_mooxbootstrap4_container.none', '' ],
	[ $ll.'tt_content.tx_mooxbootstrap4_container.container', 'container' ],
	[ $ll.'tt_content.tx_mooxbootstrap4_container.container-fluid', 'container-fluid' ],
];

/***************
 * Add extra fields to the be_users record
 */
$newTtContentColumns = [
	'tx_mooxbootstrap4_visibility' => [
		'exclude' => 1,
		'label' => $ll.'tt_content.tx_mooxbootstrap4_visibility',
		'config' => [
			'type' => 'select',
			'renderType' => 'selectCheckBox',
			'allowNonIdValues' => true,
			'items' => $visibilityItems,
			'default' => ''
		],
	],
	'tx_mooxbootstrap4_container' => [
		'exclude' => 1,
		'label' => $ll.'tt_content.tx_mooxbootstrap4_container',
		'config' => [
			'type' => 'select',
			'renderType' => 'selectSingle',
			'allowNonIdValues' => true,
			'items' => $containerItems,
		],
	],
	'tx_mooxbootstrap4_card' => [
		'exclude' => 1,
		'label' => $ll.'tt_content.tx_mooxbootstrap4_card',
		'config' => [
			'type' => 'select',
			'renderType' => 'selectSingle',
			'allowNonIdValues' => true,
			'items' => $cardItems,
		],
	],
	'tx_mooxbootstrap4_shadow' => [
		'exclude' => 1,
		'label' => $ll.'tt_content.tx_mooxbootstrap4_shadow',
		'config' => [
			'type' => 'select',
			'renderType' => 'selectSingle',
			'allowNonIdValues' => true,
			'items' => $shadowItems,
		],
	],
	'tx_mooxbootstrap4_card_include_header' => [
		'exclude' => 1,
		'label' => $ll.'tt_content.tx_mooxbootstrap4_card_include_header',
		'config' => [
			'type' => 'check',
		],
	],
];

// Add new TCA fields
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns(
	'tt_content',
	$newTtContentColumns,
	1
);
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addFieldsToPalette(
	'tt_content',
	'mooxBootstrap4Visibility',
	'tx_mooxbootstrap4_container,tx_mooxbootstrap4_card,--linebreak--,tx_mooxbootstrap4_shadow,tx_mooxbootstrap4_card_include_header',
	'after:linkToTop'
);
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes(
	'tt_content',
	'--div--;'.$ll.'tt_content.tx_mooxbootstrap4_tab.visibility,tx_mooxbootstrap4_visibility',
	'',
	'after:linkToTop'
);
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes(
	'tt_content',
	'--palette--;'.$ll.'tt_content.tx_mooxbootstrap4_panel.bootstrap;mooxBootstrap4Visibility',
	'',
	'after:linkToTop'
);

$mooxBootstrap4Elements = [
	'mooxbootstrap4_2columns',
	'mooxbootstrap4_3columns',
	'mooxbootstrap4_4columns',
	'mooxbootstrap4_accordion',
	'mooxbootstrap4_alert',
	'mooxbootstrap4_button',
	'mooxbootstrap4_card',
	'mooxbootstrap4_carousel',
	'mooxbootstrap4_dynamictable'
];
$fieldsToHideFromMooxBootstrap4Elements = [
	'subheader',
	'header_link',
	'date',
	'categories'
];
foreach ($fieldsToHideFromMooxBootstrap4Elements AS $field)
{
	if(!is_array($GLOBALS['TCA']['tt_content']['columns'][$field]['displayCond']['AND']))
	{
		$GLOBALS['TCA']['tt_content']['columns'][$field]['displayCond']['AND'] = [];
	}
	foreach ($mooxBootstrap4Elements AS $element)
	{
		$GLOBALS['TCA']['tt_content']['columns'][$field]['displayCond']['AND'][] = 'FIELD:CType:!=:'.$element;
	}
}





















