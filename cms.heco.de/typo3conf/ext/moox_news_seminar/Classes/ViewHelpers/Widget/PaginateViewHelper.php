<?php
namespace Neulandlotsen\MooxNewsSeminar\ViewHelpers\Widget;

/**
 * This file is part of the "moox_news_seminar" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

use Neulandlotsen\MooxNewsSeminar\ViewHelpers\Widget\Controller\PaginateController;
use TYPO3\CMS\Extbase\Persistence\QueryResultInterface;
use TYPO3\CMS\Fluid\Core\Widget\AbstractWidgetViewHelper;
 
class PaginateViewHelper extends AbstractWidgetViewHelper
{
	/**
	 * @var PaginateController
	 */
	protected $controller;

    /**
     * @param PaginateController $controller
     */
    public function injectPaginateController(PaginateController $controller)
    {
        $this->controller = $controller;
    }

	/**
	 * Render everything
	 *
	 * @param \TYPO3\CMS\Extbase\Persistence\QueryResultInterface $objects
	 * @param string $as
	 * @param mixed $configuration
	 * @param array $initial
	 * @internal param array $initial
	 * @return string
	 */
	public function render(QueryResultInterface $objects, $as, $configuration = ['itemsPerPage' => 10, 'insertAbove' => FALSE, 'insertBelow' => TRUE, 'action' => 'list'], $initial = [])
	{
		return $this->initiateSubRequest();
	}
}
