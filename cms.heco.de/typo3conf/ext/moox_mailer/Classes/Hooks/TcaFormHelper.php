<?php
namespace Neulandlotsen\MooxMailer\Hooks;

/**
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */

use TYPO3\CMS\Core\Utility\GeneralUtility; 

class TcaFormHelper 
{
	
	/**
	 * objectManager
	 *
	 * @var \TYPO3\CMS\Extbase\Object\ObjectManager	
	 */
	protected $objectManager;	

	/**
	 * templateRepository
	 *
	 * @var \Neulandlotsen\MooxMailer\Domain\Repository\TemplateRepository
	 */
	protected $templateRepository;
	
	/**
	 * tagRepository
	 *
	 * @var \Neulandlotsen\MooxMailer\Domain\Repository\TagRepository
	 */
	protected $tagRepository;
	
	/**
     * initialize action
	 *
     * @return void
     */
    public function initialize() {					
		
		// initialize object manager
		$this->objectManager = GeneralUtility::makeInstance('TYPO3\CMS\Extbase\Object\ObjectManager');		
		
		// init template repository
		$this->templateRepository = $this->objectManager->get('Neulandlotsen\\MooxMailer\\Domain\\Repository\TemplateRepository');
		
		// init tag repository
		$this->tagRepository = $this->objectManager->get('Neulandlotsen\\MooxMailer\\Domain\\Repository\TagRepository');
		
		// init zipregion repository
		$this->zipregionRepository = $this->objectManager->get('Neulandlotsen\\MooxMailer\\Domain\\Repository\ZipregionRepository');
    }
	
	/**
	 * Modifies the select box of template-options
	 *
	 * @param array &$config configuration array
	 * @return void
	 */
	public function templates(array &$config) {
		
		// initialize
		$this->initialize();
		
		$templates = $this->templateRepository->findAll(false);
		
		foreach($templates AS $template){
			$config['items'][] = array($template->getTitle(),$template->getUid());
		}						
	}	
	
	/**
	 * Modifies the select box of tag-options
	 *
	 * @param array &$config configuration array
	 * @return void
	 */
	public function tags(array &$config) {
		
		// initialize
		$this->initialize();
		
		$tags = $this->tagRepository->findAll(false);
		
		foreach($tags AS $tag){
			$config['items'][] = array($tag->getTitle(),$tag->getUid());
		}						
	}	
	
	/**
	 * Modifies the select box of zipregion-options
	 *
	 * @param array &$config configuration array
	 * @return void
	 */
	public function zipregions(array &$config) {
		
		// initialize
		$this->initialize();
		
		$tags = $this->zipregionRepository->findAll(false);
		
		foreach($tags AS $tag){
			$config['items'][] = array($tag->getTitle(),$tag->getUid());
		}						
	}
	
	/**
	 * Modifies the select box of zipmodes-options
	 *
	 * @param array &$config configuration array
	 * @return void
	 */
	public function zipmodes(array &$config) {
		
		// initialize
		$this->initialize();
		
		$tags = $this->zipregionRepository->findAll(false);
		
		$config['items'][] = array('Keine Einschränkung nach Postleitzahlen','');
		$config['items'][] = array('Freier Postleitzahlenbereich (kommagetrennt)','');
		foreach($tags AS $tag){
			$config['items'][] = array($tag->getTitle(),$tag->getUid());
		}						
	}
}