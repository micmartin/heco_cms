<?php
namespace Neulandlotsen\MooxNewsSeminar\Service;

/**
 * This file is part of the "moox_news_seminar" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

use TYPO3\CMS\Core\SingletonInterface;

class IcsService implements SingletonInterface
{	
	public function setIcsValues_MooxNewsSeminar(
		\Neulandlotsen\MooxNews\Domain\Model\News $news = null,
		array $settings = []
	){
		$values = [];
		if($news->getType()=='moox_news_seminar')
		{
			$values['start'] = ";VALUE=DATE:".date("Ymd", $news->getMooxNewsSeminarStartdate());
			if($news->getMooxNewsSeminarEnddate()>0)
			{
				$values['end'] = ";VALUE=DATE:".date("Ymd", $news->getMooxNewsSeminarEnddate() + (24*60*60));
			}
			if($settings['shortcutUrl']!="")
			{
				$values['url'] = str_replace("#uid#", $news->getUid(), $settings['shortcutUrl']);
			}
			$values['description'] = (($news->getTeaser()!='')?$news->getTeaser()." ":"").$values['url'];
			$values['filename'] = "seminar";
		}
		
		return $values;
	}
}
?>