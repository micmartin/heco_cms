<?php
namespace Neulandlotsen\MooxNews\Controller;

/**
 * This file is part of the "moox_news" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */
 
use Neulandlotsen\MooxNews\Domain\Repository\Backend\TargetRepository;
use Neulandlotsen\MooxNews\Domain\Session\BackendSessionHandler;
use Neulandlotsen\MooxNews\Service\HelperService;
use TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface;
use TYPO3\CMS\Extbase\Persistence\PersistenceManagerInterface;
use TYPO3\CMS\Frontend\Page\PageRepository;
 
class Mod2Controller extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController 
{
	/**
	 * @var PersistenceManagerInterface
	 */
	protected $persistenceManager;
	
	/**
	 * @var TargetRepository
	 */
	protected $targetRepository;		
	
	/**
	 * @var PageRepository
	 */
	protected $pageRepository;		
	
	/**
     * @var BackendSessionHandler
     */
	protected $backendSession;
	
	/**
	 * @var HelperService
	 */
	protected $helperService;
	
	/**
	 * @var int
	 */
	protected $storagePage;
	
	/**
	 * @var bool
	 */
	protected $extConf;

	/**
	 * @var array
	 */
	protected $groupedFolders = [];

	/**
	 * @var array
	 */
	protected $folders = [];
	
	/**
	 * @var array
	 */
	protected $filter;
	
	/**
	 * @var array
	 */
	protected $ordering;

    /**
     * @param PersistenceManagerInterface $persistenceManager
     */
    public function injectPersistenceManagerInterface(PersistenceManagerInterface $persistenceManager)
    {
        $this->persistenceManager = $persistenceManager;
    }

    /**
     * @param TargetRepository $targetRepository
     */
    public function injectTargetRepository(TargetRepository $targetRepository)
    {
        $this->targetRepository = $targetRepository;
    }

    /**
     * @param PageRepository $pageRepository
     */
    public function injectPageRepository(PageRepository $pageRepository)
    {
        $this->pageRepository = $pageRepository;
    }

    /**
     * @param BackendSessionHandler $backendSession
     */
    public function injectBackendSessionHandler(BackendSessionHandler $backendSession)
    {
        $this->backendSession = $backendSession;
    }

    /**
     * @param HelperService $helperService
     */
    public function injectHelperService(HelperService $helperService)
    {
        $this->helperService = $helperService;
    }

	/**
	 * initialize the controller
	 *	 
	 * @return void
	 */
	protected function initializeAction()
	{
		// parent initialization
		parent::initializeAction();
		
		// set backend session key 
		//$this->backendSession->setStorageKey("tx_mooxnews_mod2"); 

		// get news folders
		$this->groupedFolders = $this->helperService->getFolders();
		foreach ($this->groupedFolders AS $groupedFolder)
		{
			foreach ($groupedFolder AS $folder)
			{
				$this->folders[] = $folder;
			}
		}
		
		// set filter
		$this->filter = $this->helperService->buildFilter(self::class,$this->settings);
		
		// set ordering
		$this->ordering = $this->helperService->buildOrdering(self::class,$this->settings);
		
		// set storage page
		$this->setStoragePage(
			$this->helperService->redirectToFolder($this->groupedFolders,$this->filter,$this->settings,self::class)
		);
		
		// initialize storage settings
		$this->initializeStorageSettings();									
		
		// set ext conf
		$this->extConf = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Configuration\ExtensionConfiguration::class)->get('moox_news');
	}
	
	/**
	 * initialize storage settings
	 *	 
	 * @return void
	 */
	protected function initializeStorageSettings()
	{				
		// set helper storage pids 
		$this->helperService->setStoragePids([$this->getStoragePage()]);
		
		//fallback to current pid if no storagePid is defined
		$configuration = $this->configurationManager->getConfiguration(ConfigurationManagerInterface::CONFIGURATION_TYPE_FRAMEWORK);
		$currentPid['persistence']['storagePid'] = $this->getStoragePage();
		$this->configurationManager->setConfiguration(array_merge($configuration, $currentPid));		
	}
	
	/**
	 * action list
	 *
	 * @param bool $refresh refresh
	 * @param bool $ajax ajax	
	 * @param string $returnUrl return url
	 * @return void
	 */
	public function listAction($refresh = FALSE, $ajax = FALSE, $returnUrl = NULL)
	{		
		// init action arrays and booleans
		$messages = [];
		$errors = [];	
		
		// get targets			
		$targets = $this->targetRepository->findByFilter($this->filter,[$this->ordering['field']=>$this->ordering['direction']],NULL,NULL,[$this->getStoragePage()],['disabled','starttime','endtime']);				
		
		// set template variables		
		$this->view->assign('targets', $targets);	
		$this->view->assign('fields', $this->helperService->getListViewFields("tx_mooxnews_domain_model_target",$this->filter['fields']));
		$this->view->assign('fieldsSeparator', $this->helperService->getFieldsSeparator($this->settings));
		$this->view->assign('filter', $this->filter);	
		$this->view->assign('ordering', $this->ordering);
		$this->view->assign('groupedFolders', $this->groupedFolders);
		$this->view->assign('folders', $this->folders);
		$this->view->assign('moduleItems', $this->helperService->getAdditionalModuleSelectItems($this->settings));
		$this->view->assign('storagePage', $this->storagePage);
		$this->view->assign('replacements', $this->helperService->getQueryReplacements($filter['query']));				
		$this->view->assign('conf', $this->extConf);
		$this->view->assign('returnUrl', $this->uriBuilder->getRequest()->getRequestUri());
		$this->view->assign('refresh', $refresh);
		$this->view->assign('action', 'list');
		$this->view->assign('module', 'mod2');
		$this->view->assign('icon', 'ext-news-target');
	}		
	
	/**
	 * action filter
	 *
	 * @param array $filter filter
	 * @param bool $ajax ajax
	 * @param bool $reset reset
	 * @param string $returnUrl return url
	 * @return void
	 */
	public function filterAction($filter = NULL, $ajax = FALSE, $reset = FALSE, $returnUrl = NULL)
	{
		if($reset)
		{			
			$filter['page'] = $this->filter['page'];
			$this->helperService->resetFilter(self::class);
			$this->filter = $this->helperService->buildFilter(self::class,$this->settings);
			$this->filter['page'] = $filter['page'];
			$this->helperService->updateFilter($this->filter,self::class);			
		} 
		else 
		{			
			if($filter['page']!=$this->filter['page'])
			{
				$globalFilter = $this->helperService->buildFilter('Neulandlotsen\MooxNews\Controller\Mod1Controller',$this->settings);
				$globalFilter['page'] = $filter['page'];
				$this->helperService->updateFilter($globalFilter,'Neulandlotsen\MooxNews\Controller\Mod1Controller');
			}
			if(is_array($filter))
			{
				foreach($filter AS $key => $value)
				{
					if($value!="" && $value!="all")
					{
						$this->filter[$key] = $value;
					} 
					elseif(isset($this->filter[$key]))
					{
						unset($this->filter[$key]);
					}
				}
				$this->helperService->updateFilter($this->filter,self::class);
			} 			
		}
		
		if(!$ajax)
		{			
			if($returnUrl)
			{
				$this->redirectToUri($returnUrl);
			}			
		} 
		else 
		{
			header ("HTTP/1.0 200 Ok");
			exit();
		}
	}
	
	/**
	 * action ordering
	 *
	 * @param array $ordering ordering
	 * @param bool $ajax ajax
	 * @param bool $reset reset
	 * @param string $returnUrl return url
	 * @return void
	 */
	public function orderingAction($ordering = NULL, $ajax = FALSE, $reset = FALSE, $returnUrl = NULL)
	{
		if($reset)
		{			
			$this->helperService->resetOrdering(self::class);	
			$this->ordering = $this->helperService->buildOrdering(self::class,$this->settings);
		} 
		else 
		{			
			if(is_array($ordering))
			{
				$this->ordering = $ordering;				
				$this->helperService->updateOrdering($this->ordering,self::class);
			} 			
		}
		
		if(!$this->ajax)
		{
			if($returnUrl)
			{
				$this->redirectToUri($returnUrl);
			}
		} 
		else 
		{
			header ("HTTP/1.0 200 Ok");
			exit();
		}
	}
	
	/**
	 * action hidden
	 *
	 * @param int $uid	
	 * @param string $returnUrl return url
	 * @return void
	 */
	public function hiddenAction($uid = 0, $returnUrl = NULL)
	{					
		if($uid>0)
		{									
			$item = $this->targetRepository->findByUid($uid,FALSE);
			
			if($item->getHidden()==1)
			{
				$item->setHidden(0);				
			} 
			else 
			{
				$item->setHidden(1);				
			}			
			
			$this->targetRepository->update($item);								
			$this->persistenceManager->persistAll();
								
		} 
		
		if($returnUrl)
		{
			$this->redirectToUri($returnUrl);
		} 
		else 
		{
			$this->redirect("list");
		}		
	}
	
	/**
	 * action move
	 *
	 * @param int $uid
	 * @param int $pid		 	 
	 * @return void
	 */
	public function moveAction($uid = 0,$pid = 0)
	{					
		if(is_numeric($pid) && $uid>0)
		{									
			$item = $this->targetRepository->findByUid($uid,FALSE);			
			$item->setPid($pid);	
			
			$this->targetRepository->update($item);														
			$this->persistenceManager->persistAll();							
		} 
		
		$this->redirect("list");			
	}
	
	/**
	 * @return int
	 */
	public function getStoragePage()
	{
		return $this->storagePage;
	}

	/**
	 * @param int $storagePage storage page
	 */
	public function setStoragePage($storagePage)
	{
		$this->storagePage = $storagePage;
	}
	
	/**
	 * @return array
	 */
	public function getExtConf()
	{
		return $this->extConf;
	}

	/**
	 * @param array $extConf ext conf
	 */
	public function setExtConf($extConf)
	{
		$this->extConf = $extConf;
	}		
	
	/**
	 * @return array
	 */
	public function getFolders()
	{
		return $this->folders;
	}

	/**
	 * @param array $folders folders
	 */
	public function setFolders($folders)
	{
		$this->folders = $folders;
	}
	
	/**
	 * @return array
	 */
	public function getFilter()
	{
		return $this->filter;
	}

	/**
	 * @param array $filter filter
	 */
	public function setFilter($filter)
	{
		$this->filter = $filter;
	}
	
	/**
	 * @return array
	 */
	public function getOrdering()
	{
		return $this->ordering;
	}

	/**
	 * @param array $ordering ordering
	*/
	public function setOrdering($ordering)
	{
		$this->ordering = $ordering;
	}	
}
?>