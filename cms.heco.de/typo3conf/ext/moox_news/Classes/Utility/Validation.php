<?php

namespace Neulandlotsen\MooxNews\Utility;

/**
 * This file is part of the "moox_news" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Validation
 *
 */
class Validation
{

    /**
     * Validate ordering as extbase can't handle that currently
     *
     * @param string $fieldToCheck
     * @param string $allowedSettings
     * @return bool
     */
    public static function isValidOrdering($fieldToCheck, $allowedSettings)
    {
		$isValid = true;

        if (empty($fieldToCheck)) {
            return $isValid;
        } elseif (empty($allowedSettings)) {
            return false;
        }

        $fields = GeneralUtility::trimExplode(',', $fieldToCheck, true);

		foreach($GLOBALS['TCA']['tx_mooxnews_domain_model_news']['columns'] AS $fieldname => $field){
			if((is_array($GLOBALS['TCA']['tx_mooxnews_domain_model_news']['moox']['additionalFrontendSortingFields']) && in_array($fieldname,$GLOBALS['TCA']['tx_mooxnews_domain_model_news']['moox']['additionalFrontendSortingFields'])) || (is_array($GLOBALS['TCA']['tx_mooxnews_domain_model_news']['moox']['additionalBackendSortingFields']) && in_array($fieldname,$GLOBALS['TCA']['tx_mooxnews_domain_model_news']['moox']['additionalBackendSortingFields']))){					
				$allowedSettings .= ",".GeneralUtility::underscoredToLowerCamelCase($fieldname);
			} 
			$nameParts = explode("_",$fieldname);
			if($nameParts[count($nameParts)-1]=="sortfield"){
				if(isset($GLOBALS['TCA']['tx_mooxnews_domain_model_news']['columns'][str_replace("_sortfield","",$fieldname)])){
					$allowedSettings .= ",".GeneralUtility::underscoredToLowerCamelCase($fieldname);
				}
			}
		}

		foreach ($fields as $field) {
            if ($isValid === true) {
                $split = GeneralUtility::trimExplode(' ', $field, true);
                $count = count($split);
                switch ($count) {
                    case 1:
                        if (!GeneralUtility::inList($allowedSettings, $split[0])) {
                            $isValid = false;
                        }
                        break;
                    case 2:
                        if ((strtolower($split[1]) !== 'desc' && strtolower($split[1]) !== 'asc') ||
                            !GeneralUtility::inList($allowedSettings, $split[0])
                        ) {
                            $isValid = false;
                        }
                        break;
                    default:
                        $isValid = false;
                }
            }
        }

        return $isValid;
    }
}
