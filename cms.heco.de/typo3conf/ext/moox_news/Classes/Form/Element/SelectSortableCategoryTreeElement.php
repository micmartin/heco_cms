<?php
namespace Neulandlotsen\MooxNews\Form\Element;

/**
 * This file is part of the "moox_news" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

use TYPO3\CMS\Backend\Form\Element\AbstractFormElement;
use TYPO3\CMS\Core\Authentication\BackendUserAuthentication;
use TYPO3\CMS\Core\Imaging\Icon;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Utility\MathUtility;
use TYPO3\CMS\Core\Utility\StringUtility;
use TYPO3\CMS\Backend\Utility\BackendUtility;
use TYPO3\CMS\Lang\LanguageService;

/**
 * Render a widget with two boxes side by side.
 *
 * This is rendered for config type=select, renderType=selectSortableCategoryTree set
 */
class SelectSortableCategoryTreeElement extends AbstractFormElement
{
	/**
	 * @var \TYPO3\CMS\Extbase\Object\ObjectManager
	 */
	protected $objectManager;

	/**
	 * @var \Neulandlotsen\MooxNews\Domain\Repository\CategoryRepository
	 */
	protected $categoryRepository;

	/**
	 * @var \Neulandlotsen\MooxNews\Service\HelperService
	 */
	protected $helperService;

	/**
	 * @var array
	 */
	protected $filter;

	/**
	 * Default field controls for this element.
	 *
	 * @var array
	 */
	protected $defaultFieldControl = [
		'editPopup' => [
			'renderType' => 'editPopup',
			'disabled' => true,
		],
		'addRecord' => [
			'renderType' => 'addRecord',
			'disabled' => true,
			'after' => [ 'editPopup' ],
		],
		'listModule' => [
			'renderType' => 'listModule',
			'disabled' => true,
			'after' => [ 'addRecord' ],
		],
	];

	/**
	 * Default field wizards enabled for this element.
	 *
	 * @var array
	 */
	protected $defaultFieldWizard = [
		'localizationStateSelector' => [
			'renderType' => 'localizationStateSelector',
		],
		'otherLanguageContent' => [
			'renderType' => 'otherLanguageContent',
			'after' => [
				'localizationStateSelector'
			],
		],
		'defaultLanguageDifferences' => [
			'renderType' => 'defaultLanguageDifferences',
			'after' => [
				'otherLanguageContent',
			],
		],
	];

	/**
	 * Render side by side element.
	 *
	 * @return array As defined in initializeResultArray() of AbstractNode
	 */
	public function render()
	{
		$languageService = $this->getLanguageService();
		$resultArray = $this->initializeResultArray();

		$parameterArray = $this->data['parameterArray'];
		$config = $parameterArray['fieldConf']['config'];
		$elementName = $parameterArray['itemFormElName'];

		if ($config['readOnly']) {
			// Early return for the relatively simple read only case
			return $this->renderReadOnly();
		}

		$this->objectManager = GeneralUtility::makeInstance('TYPO3\\CMS\\Extbase\\Object\\ObjectManager');
		$this->categoryRepository = $this->objectManager->get('Neulandlotsen\MooxNews\Domain\Repository\CategoryRepository');
		$this->helperService = $this->objectManager->get('Neulandlotsen\MooxNews\Service\HelperService');

		// set filter
		$this->filter = $this->helperService->buildFilter('Neulandlotsen\MooxNews\Controller\Mod1Controller');

		$pids = [];
		$flexformMode = false;
		if(is_array($this->data['databaseRow']['pi_flexform']['data']['sDEF']['lDEF']['settings.startingpoint']['vDEF']))
		{
			foreach($this->data['databaseRow']['pi_flexform']['data']['sDEF']['lDEF']['settings.startingpoint']['vDEF'] AS $page)
			{
				$pids[] = $page['uid'];
			}
			$flexformMode = true;
			unset($this->filter['page']);
		}

		// get categories
		$categories = $this->categoryRepository->findParentCategoriesByPids((count($pids))?$pids:$this->filter['page'], [], [], true);
		$idList = [];
		foreach ($categories as $c)
		{
			if($c->getNewsTypes()=="" || $this->filter['type']=="" || ($c->getNewsTypes()!="" && $this->filter['type']!="" && in_array($this->filter['type'],explode(",",$c->getNewsTypes()))))
			{
				$idList[] = $c->getUid();
			} elseif(is_array($this->filter['categories']) && in_array($c->getUid(),$this->filter['categories'])){
				if(($key = array_search($c->getUid(), $this->filter)) !== false) {
					unset($this->filter[$key]);
				}
			}
		}
		if (empty($idList) && !$this->getBackendUserAuthentication()->isAdmin())
		{
			$idList = $this->getBackendUserAuthentication()->getCategoryMountPoints();
		}
		$categories = $this->categoryRepository->findTree($idList,NULL,true);

		$possibleItems = $config['items'];
		$selectedItems = $parameterArray['itemFormElValue'] ?: [];
		$selectedItemsCount = count($selectedItems);
		$main = $selectedItems[0];

		$maxItems = $config['maxitems'];
		$autoSizeMax = MathUtility::forceIntegerInRange($config['autoSizeMax'], 0);
		$size = 2;
		if (isset($config['size'])) {
			$size = (int)$config['size'];
		}
		if ($autoSizeMax >= 1) {
			$size = MathUtility::forceIntegerInRange($selectedItemsCount + 1, MathUtility::forceIntegerInRange($size, 1), $autoSizeMax);
		}
		$itemCanBeSelectedMoreThanOnce = !empty($config['multiple']);

		$listOfSelectedValues = [];
		$selectedItemsHtml = [];
		foreach ($selectedItems as $itemValue) {
			foreach ($possibleItems as $possibleItem) {
				if ($possibleItem[1] == $itemValue) {
					$title = $possibleItem[0];
					$selectedItem = $this->categoryRepository->findByUid($itemValue);
					if($selectedItem)
					{
						$description = $selectedItem->getDescription();
					}
					$listOfSelectedValues[] = $itemValue;
					$selectedItemsHtml[] = '<option value="' . htmlspecialchars($itemValue) . '" title="' . htmlspecialchars($title) . '">' . htmlspecialchars($title) . (($description)?" (".$description.")":"") .'</option>';
					break;
				}
			}
		}

		$selectableItemsHtml = [];
		foreach ($possibleItems as $possibleItem) {
			$disabledAttr = '';
			$classAttr = '';
			if (!$itemCanBeSelectedMoreThanOnce && in_array((string)$possibleItem[1], $selectedItems, true)) {
				$disabledAttr = ' disabled="disabled"';
				$classAttr = ' class="hidden"';
			}
			$selectableItemsHtml[] =
				'<option value="'
				. htmlspecialchars($possibleItem[1])
				. '" title="' . htmlspecialchars($possibleItem[0]) . '"'
				. $classAttr . $disabledAttr
				. '>'
				. htmlspecialchars($possibleItem[0]) .
				'</option>';
		}

		// Html stuff for filter and select filter on top of right side of multi select boxes
		$filterTextfield = [];
		if ($config['enableMultiSelectFilterTextfield']) {
			$filterTextfield[] = '<span class="input-group input-group-sm">';
			$filterTextfield[] =    '<span class="input-group-addon">';
			$filterTextfield[] =        '<span class="fa fa-filter"></span>';
			$filterTextfield[] =    '</span>';
			$filterTextfield[] =    '<input class="t3js-formengine-multiselect-filter-textfield form-control" value="">';
			$filterTextfield[] = '</span>';
		}
		$filterDropDownOptions = [];
		if (isset($config['multiSelectFilterItems']) && is_array($config['multiSelectFilterItems']) && count($config['multiSelectFilterItems']) > 1) {
			foreach ($config['multiSelectFilterItems'] as $optionElement) {
				$value = $languageService->sL($optionElement[0]);
				$label = $value;
				if (isset($optionElement[1]) && trim($optionElement[1]) !== '') {
					$label = $languageService->sL($optionElement[1]);
				}
				$filterDropDownOptions[] = '<option value="' . htmlspecialchars($value) . '">' . htmlspecialchars($label) . '</option>';
			}
		}
		$filterHtml = [];
		if (!empty($filterTextfield) || !empty($filterDropDownOptions)) {
			$filterHtml[] = '<div class="form-multigroup-item-wizard">';
			if (!empty($filterTextfield) && !empty($filterDropDownOptions)) {
				$filterHtml[] = '<div class="t3js-formengine-multiselect-filter-container form-multigroup-wrap">';
				$filterHtml[] =     '<div class="form-multigroup-item form-multigroup-element">';
				$filterHtml[] =         '<select class="form-control input-sm t3js-formengine-multiselect-filter-dropdown">';
				$filterHtml[] =             implode(LF, $filterDropDownOptions);
				$filterHtml[] =         '</select>';
				$filterHtml[] =     '</div>';
				$filterHtml[] =     '<div class="form-multigroup-item form-multigroup-element">';
				$filterHtml[] =         implode(LF, $filterTextfield);
				$filterHtml[] =     '</div>';
				$filterHtml[] = '</div>';
			} elseif (!empty($filterTextfield)) {
				$filterHtml[] = implode(LF, $filterTextfield);
			} else {
				$filterHtml[] = '<select class="form-control input-sm t3js-formengine-multiselect-filter-dropdown">';
				$filterHtml[] =     implode(LF, $filterDropDownOptions);
				$filterHtml[] = '</select>';
			}
			$filterHtml[] = '</div>';
		}

		$classes = [];
		$classes[] = 'form-control';
		$classes[] = 'tceforms-multiselect';
		if($flexformMode)
		{
			$classes[] = 'is-flexform';
		}
		if ($maxItems === 1) {
			$classes[] = 'form-select-no-siblings';
		}
		$multipleAttribute = '';
		if ($maxItems !== 1 && $size !== 1) {
			$multipleAttribute = ' multiple="multiple"';
		}

		$fieldInformationResult = $this->renderFieldInformation();
		$fieldInformationHtml = $fieldInformationResult['html'];
		$resultArray = $this->mergeChildReturnIntoExistingResult($resultArray, $fieldInformationResult, false);

		$fieldControlResult = $this->renderFieldControl();
		$fieldControlHtml = $fieldControlResult['html'];
		$resultArray = $this->mergeChildReturnIntoExistingResult($resultArray, $fieldControlResult, false);

		$fieldWizardResult = $this->renderFieldWizard();
		$fieldWizardHtml = $fieldWizardResult['html'];
		$resultArray = $this->mergeChildReturnIntoExistingResult($resultArray, $fieldWizardResult, false);

		$uniqueID = StringUtility::getUniqueId('tceforms-multiselect-');

		$html = [];
		$html[] = '<div class="formengine-field-item t3js-formengine-field-item">';
		$html[] =   $fieldInformationHtml;
		$html[] =   '<div class="form-wizards-wrap">';
		$html[] =       '<div class="form-wizards-element">';
		$html[] =           '<input type="hidden" data-formengine-input-name="' . htmlspecialchars($elementName) . '" value="' . (int)$itemCanBeSelectedMoreThanOnce . '" />';
		$html[] =           '<div class="form-multigroup-wrap t3js-formengine-field-group">';
		$html[] =               '<div class="form-multigroup-item form-multigroup-element">';
		$html[] =                   '<label>';
		$html[] =                       htmlspecialchars($languageService->sL('LLL:EXT:lang/Resources/Private/Language/locallang_core.xlf:labels.selected'));
		$html[] =                   '</label>';
		$html[] =                   '<div class="form-wizards-wrap form-wizards-aside">';
		$html[] =                       '<div class="form-wizards-element">';
		$html[] =                           '<select';
		$html[] =                               ' id="' . $uniqueID . '"';
		$html[] =                               ' size="' . $size . '"';
		$html[] =                               ' class="' . implode(' ', $classes) . '"';
		$html[] =                               $multipleAttribute;
		$html[] =                               ' data-formengine-input-name="' . htmlspecialchars($elementName) . '"';
		$html[] =                           '>';
		$html[] =                               implode(LF, $selectedItemsHtml);
		$html[] =                           '</select>';
		$html[] =                       '</div>';
		$html[] =                       '<div class="form-wizards-items-aside buttons-'.$uniqueID.'">';
		$html[] =                           '<div class="btn-group-vertical">';
		if ($maxItems > 1 && $size >= 5) {
			$html[] =                           '<a href="#"';
			$html[] =                               ' class="btn btn-default t3js-btn-moveoption-top"';
			$html[] =                               ' data-fieldname="' . htmlspecialchars($elementName) . '"';
			if($flexformMode)
			{
				$html[] =                           ' title="' . htmlspecialchars($languageService->sL('LLL:EXT:lang/Resources/Private/Language/locallang_core.xlf:labels.move_to_top')) . '"';
				$html[] =                       '>';
				$html[] =                           $this->iconFactory->getIcon('actions-move-to-top', Icon::SIZE_SMALL)->render();
			}
			else
			{
				$html[] =                           ' title="Ausgewählten Eintrag als Haupteintrag auswählen"';
				$html[] =                       '>';
				$html[] =                           $this->iconFactory->getIcon('actions-system-shortcut-active', Icon::SIZE_SMALL)->render();
			}

			$html[] =                           '</a>';
		}
		if ($maxItems > 1) {
			$html[] =                           '<a href="#"';
			$html[] =                               ' class="btn btn-default t3js-btn-moveoption-up"';
			$html[] =                               ' data-fieldname="' . htmlspecialchars($elementName) . '"';
			$html[] =                               ' title="' . htmlspecialchars($languageService->sL('LLL:EXT:lang/Resources/Private/Language/locallang_core.xlf:labels.move_up')) . '"';
			$html[] =                           '>';
			$html[] =                               $this->iconFactory->getIcon('actions-move-up', Icon::SIZE_SMALL)->render();
			$html[] =                           '</a>';
			$html[] =                           '<a href="#"';
			$html[] =                               ' class="btn btn-default t3js-btn-moveoption-down"';
			$html[] =                               ' data-fieldname="' . htmlspecialchars($elementName) . '"';
			$html[] =                               ' title="' . htmlspecialchars($languageService->sL('LLL:EXT:lang/Resources/Private/Language/locallang_core.xlf:labels.move_down')) . '"';
			$html[] =                           '>';
			$html[] =                               $this->iconFactory->getIcon('actions-move-down', Icon::SIZE_SMALL)->render();
			$html[] =                           '</a>';
		}
		if ($maxItems > 1 && $size >= 5) {
			$html[] =                           '<a href="#"';
			$html[] =                               ' class="btn btn-default t3js-btn-moveoption-bottom"';
			$html[] =                               ' data-fieldname="' . htmlspecialchars($elementName) . '"';
			$html[] =                               ' title="' . htmlspecialchars($languageService->sL('LLL:EXT:lang/Resources/Private/Language/locallang_core.xlf:labels.move_to_bottom')) . '"';
			$html[] =                           '>';
			$html[] =                               $this->iconFactory->getIcon('actions-move-to-bottom', Icon::SIZE_SMALL)->render();
			$html[] =                           '</a>';
		}
		$html[] =                               '<a href="#"';
		$html[] =                                   ' class="btn btn-default t3js-btn-removeoption"';
		$html[] =                                   ' data-fieldname="' . htmlspecialchars($elementName) . '"';
		$html[] =                                   ' title="' . htmlspecialchars($languageService->sL('LLL:EXT:lang/Resources/Private/Language/locallang_core.xlf:labels.remove_selected')) . '"';
		$html[] =                               '>';
		$html[] =                                   $this->iconFactory->getIcon('actions-selection-delete', Icon::SIZE_SMALL)->render();
		$html[] =                               '</a>';
		$html[] =                           '</div>';
		$html[] =                       '</div>';
		$html[] =                   '</div>';
		$html[] =               '</div>';
		$html[] =               '<div class="form-multigroup-item form-multigroup-element">';
		$html[] =                   '<label>';
		$html[] =                       htmlspecialchars($languageService->sL('LLL:EXT:lang/Resources/Private/Language/locallang_core.xlf:labels.items'));
		$html[] =                   '</label>';
		$tree = $this->getTreeAsUl($categories, $main, $uniqueID,0,$flexformMode);
		if($tree)
		{
			$html[] =               '<div class="well well-tree">';
			$html[] = 					$tree;
			$html[] =               '</div>';
		}
		else
		{
			$html[] =               '<div class="alert alert-info">Es wurden noch keine Kategorien angelegt. Um Kategorien nutzen zu können, legen Sie zunächst  eine Kategorie über das Listen-Modul an oder fragen Sie Ihren Administrator.</div>';
		}
		/*
		$html[] =                   implode(LF, $filterHtml);
		$html[] =                   '<select';
		$html[] =                       ' data-relatedfieldname="' . htmlspecialchars($elementName) . '"';
		$html[] =                       ' data-exclusivevalues="' . htmlspecialchars($config['exclusiveKeys']) . '"';
		$html[] =                       ' id="' . StringUtility::getUniqueId('tceforms-multiselect-') . '"';
		$html[] =                       ' data-formengine-input-name="' . htmlspecialchars($elementName) . '"';
		$html[] =                       ' class="form-control t3js-formengine-select-itemstoselect"';
		$html[] =                       ' size="' . $size . '"';
		$html[] =                       ' onchange="' . htmlspecialchars(implode('', $parameterArray['fieldChangeFunc'])) . '"';
		$html[] =                       ' data-formengine-validation-rules="' . htmlspecialchars($this->getValidationDataAsJsonString($config)) . '"';
		$html[] =                       $selectableListStyle;
		$html[] =                   '>';
		$html[] =                       implode(LF, $selectableItemsHtml);
		$html[] =                   '</select>';
		*/
		$html[] =               '</div>';
		$html[] =           '</div>';
		$html[] =           '<input type="hidden" id="'.$uniqueID.'-hidden" name="' . htmlspecialchars($elementName) . '" value="' . htmlspecialchars(implode(',', $listOfSelectedValues)) . '" />';
		$html[] =       '</div>';
		$html[] =       '<div class="form-wizards-items-aside">';
		$html[] =           '<div class="btn-group-vertical">';
		$html[] =               $fieldControlHtml;
		$html[] =           '</div>';
		$html[] =       '</div>';
		$html[] =       '<div class="form-wizards-items-bottom">';
		$html[] =           $fieldWizardHtml;
		$html[] =       '</div>';
		$html[] =   '</div>';
		$html[] = '</div>';

		$resultArray['html'] = '<div class="moox-news-select-sortable-tree">'.implode(LF, $html).'</div>';

		$resultArray['requireJsModules']['selectSortableCategoryTreeElement'] = [
			'TYPO3/CMS/MooxNews/FormEngine/Element/SelectSortableCategoryTreeElement' => 'function (SelectSortableCategoryTreeElement) { SelectSortableCategoryTreeElement.initialize("'.$uniqueID.'") }'
		];

		$resultArray['stylesheetFiles'][] = "/typo3conf/ext/moox_news/Resources/Public/Css/Backend/tca.css";

		return $resultArray;
	}

	/**
	 * Create HTML of a read only multi select. Right side is not
	 * rendered, but just the left side with the selected items.
	 *
	 * @return array
	 */
	protected function renderReadOnly()
	{
		$languageService = $this->getLanguageService();

		$parameterArray = $this->data['parameterArray'];
		$config = $parameterArray['fieldConf']['config'];
		$fieldName = $parameterArray['itemFormElName'];

		$possibleItems = $config['items'];
		$selectedItems = $parameterArray['itemFormElValue'] ?: [];
		$selectedItemsCount = count($selectedItems);

		$autoSizeMax = MathUtility::forceIntegerInRange($config['autoSizeMax'], 0);
		$size = 2;
		if (isset($config['size'])) {
			$size = (int)$config['size'];
		}
		if ($autoSizeMax >= 1) {
			$size = MathUtility::forceIntegerInRange($selectedItemsCount + 1, MathUtility::forceIntegerInRange($size, 1), $autoSizeMax);
		}
		$multiple = '';
		if ($size !== 1) {
			$multiple = ' multiple="multiple"';
		}

		$listOfSelectedValues = [];
		$optionsHtml = [];
		foreach ($selectedItems as $itemValue) {
			foreach ($possibleItems as $possibleItem) {
				if ($possibleItem[1] == $itemValue) {
					$title = $possibleItem[0];
					$listOfSelectedValues[] = $itemValue;
					$optionsHtml[] = '<option value="' . htmlspecialchars($itemValue) . '" title="' . htmlspecialchars($title) . '">' . htmlspecialchars($title) . '</option>';
					break;
				}
			}
		}

		$html = [];
		$html[] = '<div class="formengine-field-item t3js-formengine-field-item">';
		$html[] =   '<div class="form-wizards-wrap">';
		$html[] =       '<div class="form-wizards-element">';
		$html[] =           '<label>';
		$html[] =               htmlspecialchars($languageService->sL('LLL:EXT:lang/Resources/Private/Language/locallang_core.xlf:labels.selected'));
		$html[] =           '</label>';
		$html[] =           '<div class="form-wizards-wrap form-wizards-aside">';
		$html[] =               '<div class="form-wizards-element">';
		$html[] =                   '<select';
		$html[] =                       ' id="' . StringUtility::getUniqueId('tceforms-multiselect-') . '"';
		$html[] =                       ' size="' . $size . '"';
		$html[] =                       ' class="form-control tceforms-multiselect"';
		$html[] =                       $multiple;
		$html[] =                       ' data-formengine-input-name="' . htmlspecialchars($fieldName) . '"';
		$html[] =                       ' disabled="disabled">';
		$html[] =                   '/>';
		$html[] =                       implode(LF, $optionsHtml);
		$html[] =                   '</select>';
		$html[] =               '</div>';
		$html[] =           '</div>';
		$html[] =           '<input type="hidden" name="' . htmlspecialchars($fieldName) . '" value="' . htmlspecialchars(implode(',', $listOfSelectedValues)) . '" />';
		$html[] =       '</div>';
		$html[] =   '</div>';
		$html[] = '</div>';

		$resultArray = $this->initializeResultArray();
		$resultArray['html'] = '<div class="moox-news-select-sortable-tree">'.implode(LF, $html).'</div>';
		return $resultArray;
	}

	/**
	 * @param array $tree
	 * @param int $depth
	 * @return string options
	 */
	protected function getTreeAsOptions($tree = [],$depth = 0)
	{
		$optionsHtml = "";
		if(count($tree))
		{
			foreach($tree AS $item)
			{
				$optionsHtml .= '<options value="'.$item['item']->getUid().'">'.$depth.'-'.$item['item']->getTitle().'</options>';
				if(count($item['children']))
				{
					$optionsHtml .= $this->getTreeAsOptions($item['children'],$depth+1);
				}
			}
		}
		return $optionsHtml;
	}

	/**
	 * @param array $tree
	 * @param int $main
	 * @param string $target
	 * @param int $depth
	 * @param bool $flexformMode
	 * @return string options
	 */
	protected function getTreeAsUl($tree = [],$main = 0,$target = '', $depth = 0, $flexformMode = false)
	{
		$ulHtml = "";

		if($depth>0 || count($tree))
		{
			if($depth==0)
			{
				$ulHtml .= '<div class="tree-selector-wrapper" data-target="'.$target.'">';
			}

			$ulHtml .= '<ul class="tree-selector tree-selector-'.$depth.'">';

			if(count($tree))
			{
				foreach($tree AS $item)
				{
					$isMain = (($item['item']->getUid()==$main)?true:false);
					$ulHtml .= '<li class="tree-selector-item tree-selector-item-'.$item['item']->getUid().'" data-value="'.$item['item']->getUid().'" data-title="'.$item['item']->getTitle().'">';
					$ulHtml .=      '<span class="is-cat" title="UID: '.$item['item']->getUid().'">'.$this->iconFactory->getIconForRecord('sys_category', BackendUtility::getRecordWSOL('sys_category', $item['item']->getUid(), '*', '', false), Icon::SIZE_SMALL)->render().'</span>';
					if(!$flexformMode){
						$ulHtml .=      '<span title="Eintrag ist Haupteintrag" class="is-main'.((!$isMain)?" hidden":"").'">'.$this->iconFactory->getIcon('actions-system-shortcut-active', Icon::SIZE_SMALL)->render().'</span>';
						$ulHtml .=      '<span title="Eintrag als Haupteintrag markieren" class="not-main'.(($isMain)?" hidden":"").'">'.$this->iconFactory->getIcon('actions-system-shortcut-new', Icon::SIZE_SMALL)->render().'</span>';
					}
					$ulHtml .= 		'<span title="Eintrag hinzufügen" class="do-add">';
					$ulHtml .=      	$this->iconFactory->getIcon('actions-add', Icon::SIZE_SMALL)->render();
					$ulHtml .=			$item['item']->getTitle();
					if($item['item']->getDescription())
					{
						$ulHtml .=		" (".$item['item']->getDescription().")";
					}
					$ulHtml .=		'</span>';;
					$ulHtml .= '</li>';
					if(is_array($item['children']) && count($item['children']))
					{
						$ulHtml .= '<li>';
						$ulHtml .= $this->getTreeAsUl($item['children'],$main,$target,$depth+1,$flexformMode);
						$ulHtml .= '</li>';
					}
				}
			}

			$ulHtml .= '</ul>';

			if($depth==0)
			{
				$ulHtml .= '</div>';
			}
		}

		return $ulHtml;
	}

	/**
	 * @return LanguageService
	 */
	protected function getLanguageService()
	{
		return $GLOBALS['LANG'];
	}

	/**
	 * @return BackendUserAuthentication
	 */
	protected function getBackendUserAuthentication()
	{
		return $GLOBALS['BE_USER'];
	}
}
