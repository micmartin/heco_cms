<?php
namespace Neulandlotsen\MooxNews\ViewHelpers;

/**
 * This file is part of the "moox_news" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper;
use TYPO3\CMS\Core\Database\Connection;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Utility\GeneralUtility;

class YearsViewHelper extends AbstractViewHelper
{
	public function initializeArguments()
	{
		parent::initializeArguments();
		$this->registerArgument('settings', 'array', 'settings of news-plugin', true);
		$this->registerArgument('excludeEmptyYear', 'bool', 'exclude entries with no year', false);
		$this->registerArgument('as', 'string', 'return result as variable with this name', false);
	}

	/**
	 * @return string
	 */
	public function render()
	{
		$settings = $this->arguments['settings'];
		$excludeEmptyYear = $this->arguments['excludeEmptyYear'];
		$as = $this->arguments['as'];

		$queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)
			->getQueryBuilderForTable('tx_mooxnews_domain_model_news');

		$queryBuilder
			->select('year')
			->from('tx_mooxnews_domain_model_news')
			->orderBy('year','DESC')
			->groupBy('year');

		if($settings['categories']!="")
		{
			$queryBuilder
				->rightJoin(
					'tx_mooxnews_domain_model_news',
					'sys_category_record_mm',
					'sys_category_record_mm',
					$queryBuilder->expr()->eq('sys_category_record_mm.uid_foreign', $queryBuilder->quoteIdentifier('tx_mooxnews_domain_model_news.uid'))
				);
		}

		$where = [];

		if($settings['startingpoint'])
		{
			$where[] = $queryBuilder->expr()->in('tx_mooxnews_domain_model_news.pid', $queryBuilder->createNamedParameter(array_map('intval', explode(',', $settings['startingpoint'])), Connection::PARAM_INT_ARRAY));
		}

		if(trim($settings['types']))
		{
			$where[] = 	$queryBuilder->expr()->in('tx_mooxnews_domain_model_news.type', $queryBuilder->createNamedParameter(array_map('strval', explode(',', $settings['types'])), Connection::PARAM_STR_ARRAY));
		}

		if($settings['archiveRestriction']=="active")
		{
			$archiveStmt = $queryBuilder->expr()->orX();
			$archiveStmt->add($queryBuilder->expr()->eq('tx_mooxnews_domain_model_news.archive', $queryBuilder->createNamedParameter(0, \PDO::PARAM_INT)));
			$archiveStmt->add($queryBuilder->expr()->gte('tx_mooxnews_domain_model_news.archive', $queryBuilder->createNamedParameter(time(), \PDO::PARAM_INT)));
			$where[] = $archiveStmt;
		}
		elseif($settings['archiveRestriction']=="archived")
		{
			$archiveStmt = $queryBuilder->expr()->orX();
			$archiveStmt->add($queryBuilder->expr()->eq('tx_mooxnews_domain_model_news.archive', $queryBuilder->createNamedParameter(0, \PDO::PARAM_INT)));
			$archiveStmt->add($queryBuilder->expr()->lt('tx_mooxnews_domain_model_news.archive', $queryBuilder->createNamedParameter(time(), \PDO::PARAM_INT)));
			$where[] = $archiveStmt;
		}

		if($settings['topNewsRestriction']=="1")
		{
			$where[] = $queryBuilder->expr()->eq('tx_mooxnews_domain_model_news.istopnews', $queryBuilder->createNamedParameter(1, \PDO::PARAM_INT));
		}
		elseif($settings['topNewsRestriction']=="2")
		{
			$where[] = $queryBuilder->expr()->eq('tx_mooxnews_domain_model_news.istopnews', $queryBuilder->createNamedParameter(0, \PDO::PARAM_INT));
		}

		if($settings['categories']!="")
		{
			if(in_array($settings['categoryConjunction'],['and','notand']))
			{
				$catStmt = $queryBuilder->expr()->andX();
			}
			else
			{
				$catStmt = $queryBuilder->expr()->orX();
			}

			foreach (explode(",",$settings['categories']) AS $categoryUid)
			{
				$catStmt->add($queryBuilder->expr()->eq('sys_category_record_mm.uid_local', $queryBuilder->createNamedParameter($categoryUid, \PDO::PARAM_INT)));
			}
			if(in_array($settings['categoryConjunction'],['notor','notand']))
			{
				// TODO: implement not statemnet in these cases
				$where[] = $catStmt;
			}
			else
			{
				$where[] = $catStmt;
			}
		}

		foreach($where AS $stmt)
		{
			$queryBuilder->andWhere($stmt);
		}

		$foundYears = $queryBuilder->execute();

		$years = [];
		while($year = $foundYears->fetch())
		{
			if(!$excludeEmptyYear || $year['year']>1)
			{
				$years[] = $year['year'];
			}
		}

		if($as)
		{
			$this->templateVariableContainer->add($this->arguments['as'], $years);
		}
		else
		{
			return $years;
		}
	}
}
