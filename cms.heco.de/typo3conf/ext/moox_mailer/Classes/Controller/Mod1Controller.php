<?php
namespace Neulandlotsen\MooxMailer\Controller;

/**
 * This file is part of the "moox_mailer" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

use Neulandlotsen\MooxCore\Utility\Backend\HelperUtility;
use Neulandlotsen\MooxMailer\Service\HelperService;
use Neulandlotsen\MooxMailer\Domain\Session\BackendSessionHandler;
use TYPO3\CMS\Core\Messaging\FlashMessage;

class Mod1Controller extends \Neulandlotsen\MooxCore\Controller\TemplateController
{
	/**
	 * @var HelperUtility
	 */
	protected $helperUtility;

	/**
	 * @var HelperService
	 */
	protected $helperService;

	/**
	 * @var BackendSessionHandler
	 */
	protected $backendSession;

	/**
	 * @var string
	 */
	protected $table = 'tx_mooxmailer_domain_model_template';

	/**
	 * @var string
	 */
	protected $sessionStorageKey = 'tx_mooxmailer_mod1';

	/**
	 * @var array
	 */
	protected $messages = [];

    /**
     * @param HelperUtility $helperUtility
     */
    public function injectHelperUtility(HelperUtility $helperUtility)
    {
        $this->helperUtility = $helperUtility;
    }

    /**
     * @param HelperService $helperService
     */
    public function injectHelperService(HelperService $helperService)
    {
        $this->helperService = $helperService;
    }

    /**
     * @param BackendSessionHandler $backendSession
     */
    public function injectBackendSessionHandler(BackendSessionHandler $backendSession)
    {
        $this->backendSession = $backendSession;
    }

	/**
	 * initialize the controller
	 */
	protected function initializeAction()
	{
		// set backend session key
		$this->backendSession->setStorageKey($this->sessionStorageKey);
	}

	/**
	 * @param bool $refresh
	 * @param bool $ajax
	 * @param string $returnUrl
	 */
	public function listAction($refresh = FALSE, $ajax = FALSE, $returnUrl = NULL)
	{
		$this->view->assign('tasks', $this->helperService->getMailerTasks());
		$this->view->assign('returnUrl', $this->uriBuilder->getRequest()->getRequestUri());
		$this->view->assign('action', 'list');
	}

	/**
	 * @param int $uid
	 */
	public function executeTaskAction($uid = NULL)
	{
		global $BE_USER;

		$returnToList = FALSE;

		if($uid)
		{
			$taskInfo = $this->helperService->getMailerTask($uid);

			if(is_array($taskInfo) && count($taskInfo))
			{
				$task = $taskInfo['task'];
				unset($taskInfo['task']);
			}

			$executionInfo = $task->execute(true);

			if(is_array($executionInfo) && !$executionInfo['locked'])
			{
				$mailer = $this->objectManager->get('Neulandlotsen\MooxMailer\Service\MooxNewsMailerService');
				$mailer->initialize($task);

				if($taskInfo['mode']=="daemon")
				{
					if($executionInfo['preparedCnt']>0)
					{
						$this->messages[] = [
							"text" => 'Es wurden '.$executionInfo['preparedCnt'].' Mail(s) für den Versand vorbereitet',
							"type" => FlashMessage::OK,
						];
						$this->messages[] = [
							"text" => 'Beachten Sie, dass die Mails von einem Hintergrunddienst intervallweise versendet werden. Abhängig von der Anzahl der zu versendenden Mails, kann es daher einige Zeit in Anspruch nehmen bis wirklich alles Mails versendet wurden.',
							"type" => FlashMessage::INFO,
						];
						if(!$BE_USER->isAdmin() || true)
						{
							$returnToList = TRUE;
						}
					}
					else
					{
						$this->messages[] = [
							"text" => 'Es wurden keine Mails für den Versand vorbereitet. Für den Versand markierte News-Einträge wurden zurückgesetzt.',
							"type" => FlashMessage::WARNING,
						];
						$returnToList = TRUE;
					}
				}
				else
				{
					if(($executionInfo['successCnt']+$executionInfo['errorCnt'])>0) {
						$this->messages[] = [
							"text" => 'Es wurden ' . $executionInfo['successCnt'] . ' Mail(s) versendet.',
							"type" => FlashMessage::OK,
						];
						if ($executionInfo['errorCnt'] > 0) {
							$this->messages[] = [
								"text" => $executionInfo['errorCnt'] . ' Mail(s) konnten aufgrund von Fehlern nicht versendet werden. Überprüfen Sie die Logdatei der Ausführung für Details zur Ursache',
								"type" => FlashMessage::ERROR,
							];
						}
					}
					else
					{
						$this->messages[] = [
							"text" => 'Es wurden keine Mails versendet. Für den Versand markierte News-Einträge wurden zurückgesetzt.',
							"type" => FlashMessage::WARNING,
						];
						$returnToList = TRUE;
					}
				}

				// set flash messages
				$this->helperUtility->setFlashMessages($this, $this->extensionName);

				$BE_USER->writelog(4, 0, 0, 0, 'Mailer-Job "'.$taskInfo['description'].' [UID: '.$taskInfo['uid'].']" executed', "", "tx_scheduler_task", $taskInfo['uid'], 0, 0, 0);

				if($returnToList)
				{
					$this->redirect('list');
				}

				$this->view->assign('task', $task);
				$this->view->assign('log', $mailer->readLog($task->getLogfile()));
			}
			else
			{
				if($executionInfo['locked'])
				{
					// add message
					$this->messages[] = [
						"text" => 'Das Mailsystem ist derzeit durch eine andere Ausführung blockiert (Job-UID: '.$executionInfo['locked'].', '.$executionInfo['date'].'). Bitte versuche Sie es zu einem späteren Zeitpunkt noch einemal oder wenden Sie sich an Ihren Administrator.',
						"type" => FlashMessage::WARNING,
					];
				}
				else
				{
					// add message
					$this->messages[] = [
						"text" => 'Job konnte nicht ausgeführt werden. Bitte wenden Sie sich an Ihren Administrator.',
						"type" => FlashMessage::WARNING,
					];
				}



				// set flash messages
				$this->helperUtility->setFlashMessages($this, $this->extensionName);

				$BE_USER->writelog(4, 0, 2, 0, 'Mailer-Job "'.$taskInfo['description'].' [UID: '.$taskInfo['uid'].']" failed', "", "tx_scheduler_task", $taskInfo['uid'], 0, 0, 0);

				$this->redirect('list');
			}
		}
	}
}
?>