<?php

namespace Neulandlotsen\MooxNews\Backend\FormDataProvider;

/**
 * This file is part of the "moox_news" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */
use Neulandlotsen\MooxNews\Utility\EmConfiguration;
use TYPO3\CMS\Backend\Form\FormDataProviderInterface;

/**
 * Fill the news records with default values
 */
class NewsRowInitializeNew implements FormDataProviderInterface
{

	/** @var  EmConfiguration */
	protected $emConfiguration;

	public function __construct()
	{
		$this->emConfiguration = EmConfiguration::getSettings();
	}

	/**
	 * @param array $result
	 * @return array
	 */
	public function addData(array $result)
	{
		if ($result['tableName'] !== 'tx_mooxnews_domain_model_news') {
			return $result;
		}

		$result = $this->setTagListingId($result);

		if ($result['command'] === 'new') {
			$result = $this->fillDateField($result);
		}

		return $result;
	}

	/**
	 * @param array $result
	 * @return array
	 */
	protected function fillDateField(array $result)
	{
		if ($this->emConfiguration->getDateTimeRequired()) {
			$result['databaseRow']['datetime'] = $GLOBALS['EXEC_TIME'];
		}

		if (is_array($result['pageTsConfig']['tx_mooxnews.'])
			&& is_array($result['pageTsConfig']['tx_mooxnews.']['predefine.'])
		) {
			if (isset($result['pageTsConfig']['tx_mooxnews.']['predefine.']['author']) && (int)$result['pageTsConfig']['tx_mooxnews.']['predefine.']['author'] === 1) {
				$result['databaseRow']['author'] = $GLOBALS['BE_USER']->user['realName'];
				$result['databaseRow']['author_email'] = $GLOBALS['BE_USER']->user['email'];
			}

			if (isset($result['pageTsConfig']['tx_mooxnews.']['predefine.']['archive'])) {
				$calculatedTime = strtotime($result['pageTsConfig']['tx_mooxnews.']['predefine.']['archive']);

				if ($calculatedTime !== false) {
					$result['databaseRow']['archive'] = $calculatedTime;
				}
			}
		}

		return $result;
	}

	/**
	 * @param array $result
	 * @return array
	 */
	protected function setTagListingId(array $result)
	{
		if (!is_array($result['pageTsConfig']['tx_mooxnews.']) || !isset($result['pageTsConfig']['tx_mooxnews.']['tagPid'])) {
			return $result;
		}
		$tagPid = (int)$result['pageTsConfig']['tx_mooxnews.']['tagPid'];
		if ($tagPid <= 0) {
			return $result;
		}

		$result['processedTca']['columns']['tags']['config']['fieldControl']['listModule']['options']['pid'] = $tagPid;

		return $result;
	}

	/**
	 * @param array $result
	 * @return array
	 */
	protected function setTargetListingId(array $result)
	{
		if (!is_array($result['pageTsConfig']['tx_mooxnews.']) || !isset($result['pageTsConfig']['tx_mooxnews.']['targetPid'])) {
			return $result;
		}
		$targetPid = (int)$result['pageTsConfig']['tx_mooxnews.']['targetPid'];
		if ($targetPid <= 0) {
			return $result;
		}

		$result['processedTca']['columns']['targets']['config']['fieldControl']['listModule']['options']['pid'] = $targetPid;

		return $result;
	}
}