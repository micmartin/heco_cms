<?php
namespace Neulandlotsen\MooxNews\ViewHelpers\Be;

/**
 * This file is part of the "moox_news" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

use TYPO3\CMS\Backend\Utility\BackendUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper;

class TranslationsViewHelper extends AbstractViewHelper
{
	public function initializeArguments()
	{
		parent::initializeArguments();
		$this->registerArgument('newsItem', 'integer', 'news item uid', true);
		$this->registerArgument('languages', 'array', 'languages', true);
		$this->registerArgument('as', 'string', 'return result as variable with this name', false);
	}

	/**
	 * @return string
	 */
	public function render()
	{
		$translations = [];
		$newsItem = $this->arguments['newsItem'];
		$languages = $this->arguments['languages'];
		$as = $this->arguments['as'];
		$isFirstLanguage = true;

		foreach ($languages AS $language) {
			if ($isFirstLanguage)
			{
				$record = BackendUtility::getRecord(
					"tx_mooxnews_domain_model_news",
					$newsItem
				);
				if($record)
				{
					$translations[$language->getLanguageId()] = $record;
				}
				$isFirstLanguage = false;
			}
			else
			{
				$translation = BackendUtility::getRecordLocalization(
					"tx_mooxnews_domain_model_news",
					$newsItem,
					$language->getLanguageId()
				);
				if($translation)
				{
					$translations[$language->getLanguageId()] = $translation[0];
				}
			}

		}

		if($as)
		{
			$this->templateVariableContainer->add($this->arguments['as'], $translations);
		}
		else
		{
			return $translations;
		}
	}
}
