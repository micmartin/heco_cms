<?php
namespace Neulandlotsen\MooxMailer\Domain\Repository;

/**
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */
 
use TYPO3\CMS\Extbase\Persistence\QueryInterface; 
 
class AddressRepository extends \Neulandlotsen\MooxAddress\Domain\Repository\AddressRepository 
{
	
	protected $defaultOrderings = array ('surname' => QueryInterface::ORDER_ASCENDING, 'forename' => QueryInterface::ORDER_ASCENDING);
	
	/**
	 * Find user by pid(list) and group(list)
	 *
	 * @param array $pids pids
	 * @param array $groups groups
	 * @return \TYPO3\CMS\Extbase\Persistence\QueryInterface
	 */
	public function findByPidsAndGroups($pids = array(), $groups = array()) 
	{				
		$pids 	= (is_array($pids))?$pids:array($pids);		
		$groups = (is_array($groups))?$groups:array($groups);
		
		if(count($groups)){
		
			$query = $this->createQuery();
			
			$query->getQuerySettings()->setStoragePageIds($pids);
			
			$constraints = array();
			
			if(isset($groups['nogroup'])){
				$constraints[] = $query->equals('usergroup', "");
				unset($groups['nogroup']);
			}
			
			foreach($groups AS $group){
				$constraints[] = $query->contains('usergroup', $group);
			}
						
			return $query->matching(
				$query->logicalAnd(
					$query->logicalNot($query->equals('email', "")),
					$query->equals('mailingAllowed', 1),
					$query->logicalOr($constraints)
				)
			)->execute();				
		} else {
			return array();
		}
	}
	
	/**
	 * Find addresses by pid(list)
	 *
	 * @param array $pids pids
	 * @param bool $raw raw
	 * @return \TYPO3\CMS\Extbase\Persistence\QueryInterface
	 */
	public function findByPids($pids = array(), $raw = false)
	{		
		$pids 	= (is_array($pids))?$pids:array($pids);		
		
		$query = $this->createQuery();
			
		$query->getQuerySettings()->setStoragePageIds($pids);
		
		return $query->matching(
			$query->logicalAnd(
				$query->logicalNot($query->equals('email', "")),
				$query->equals('mailingAllowed', 1)
			)
		)->execute($raw);						
	}
	
	/**
	 * Find all (overwrite)
	 *
	 * @param boo $RespectStoragePage 	
	 * @return \TYPO3\CMS\Extbase\Persistence\QueryInterface
	 */
	public function findAll($RespectStoragePage = TRUE)
	{				
		$query = $this->createQuery();
		
		$query->getQuerySettings()->setRespectStoragePage($RespectStoragePage);		
			
		return $query->execute();
		
	}
	
	/**
	 * Find addresses by quality
	 *	
	 * @param array $qualities	
	 * @return \TYPO3\CMS\Extbase\Persistence\QueryInterface
	 */
	public function findByQuality($qualities = array())
	{		
		if(!is_array($qualities) && $qualities!=""){
			$qualities = array($qualities);
		}
		
		$query = $this->createQuery();
		$query->getQuerySettings()->setRespectStoragePage(FALSE);
		$query->getQuerySettings()->setRespectSysLanguage(FALSE);
		$query->getQuerySettings()->setIgnoreEnableFields(TRUE);

		return $query->matching(
			$query->logicalAnd(
				$query->in('quality', $qualities),
				$query->equals('deleted', 0)
			))->execute();
	}
	
	/**
	 * Override default findByUid function
	 *
	 * @param int $uid id of record	 
	 * @return \Neulandlotsen\MooxMailer\Domain\Model\Address
	 */
	public function findBouncedByUid($uid)
	{
		$query = $this->createQuery();
		$query->getQuerySettings()->setRespectStoragePage(FALSE);
		$query->getQuerySettings()->setRespectSysLanguage(FALSE);
		$query->getQuerySettings()->setIgnoreEnableFields(TRUE);

		return $query->matching(
			$query->logicalAnd(
				$query->equals('uid', $uid),
				$query->equals('deleted', 0)
			))->execute()->getFirst();
	}
}
?>