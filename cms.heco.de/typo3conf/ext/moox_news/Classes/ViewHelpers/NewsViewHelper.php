<?php
namespace Neulandlotsen\MooxNews\ViewHelpers;

/**
 * This file is part of the "moox_news" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

use Neulandlotsen\MooxNews\Controller\NewsController;
use Neulandlotsen\MooxNews\Domain\Repository\NewsRepository;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper;

class NewsViewHelper extends AbstractViewHelper
{
	/**
	* @var NewsController
	*/
	protected $newsController;

	/**
	 * @var NewsRepository
	 */
	protected $newsRepository;

    /**
     * @param NewsController $newsController
     */
    public function injectNewsController(NewsController $newsController)
    {
        $this->newsController = $newsController;
    }

    /**
     * @param NewsRepository $newsRepository
     */
    public function injectNewsRepository(NewsRepository $newsRepository)
    {
        $this->newsRepository = $newsRepository;
    }

	public function initializeArguments()
	{
		parent::initializeArguments();
		$this->registerArgument('settings', 'array', 'settings of news-plugin', false, []);
		$this->registerArgument('overwriteSettings', 'array', 'overwrite settings of news-plugin', false, []);
		$this->registerArgument('filter', 'array', 'filter of news-plugin', false, []);
		$this->registerArgument('uid', 'string', 'get single element with this uid', false);
		$this->registerArgument('preview', 'boolean', 'get single element in preview mode', false, false);
		$this->registerArgument('as', 'string', 'return result as variable with this name', false);
	}

	/**
	 * @return string
	 */
	public function render()
	{
		$settings = $this->arguments['settings'];
		if(!is_array($settings))
		{
			$objectManager 	= GeneralUtility::makeInstance('TYPO3\CMS\Extbase\Object\ObjectManager');
			$configurationManager = $objectManager->get('TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface');
			$settings = $configurationManager->getConfiguration(
				\TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface::CONFIGURATION_TYPE_FULL_TYPOSCRIPT,
				'mooxnews'
			);
		}
		else
		{
			unset($settings['dateField']);
			$settings['search'] = null;
		}
		$overwriteSettings = $this->arguments['overwriteSettings'];
		$overwriteDemand = $this->arguments['filter'];
		$uid = $this->arguments['uid'];
		$as = $this->arguments['as'];
		$preview = $this->arguments['preview'];

		/*
		if($overwriteDemand['dateFrom'] || $overwriteDemand['dateTo'])
		{
			$overwriteDemand['year'] = 'duration';
		}
		elseif(!$overwriteDemand['dateFrom'] && !$overwriteDemand['dateTo'])
		{
			unset($overwriteDemand['dateFrom']);
			unset($overwriteDemand['dateTo']);
			if($overwriteDemand['year']=="duration")
			{
				unset($overwriteDemand['year']);
			}
		}
		*/

		if(is_array($overwriteSettings) && count($overwriteSettings))
		{
			$settings = array_merge($settings,$overwriteSettings);
		}

		$this->newsController->setSettings($settings);

		$demand = $this->newsController->createDemandObjectFromSettings($settings);
		$demand->setActionAndClass(__METHOD__, __CLASS__);

		if(is_array($overwriteDemand) && count($overwriteDemand))
		{
			$demand =  $this->newsController->overwriteDemandObject($demand, $overwriteDemand);
		}
		if($uid)
		{
			if($preview)
			{
				$news = $this->newsRepository->findByUid($uid,false);
			}
			else
			{
				$demand->setUids([$uid]);
				$news = $this->newsRepository->findDemanded($demand);
				if($news->count())
				{
					$news = $news->getFirst();
				}
			}			
		}
		else
		{
			$news = $this->newsRepository->findDemanded($demand);
		}

		if($as)
		{
			$this->templateVariableContainer->add($this->arguments['as'], $news);
		}
		else
		{
			return $news;
		}
	}
}
