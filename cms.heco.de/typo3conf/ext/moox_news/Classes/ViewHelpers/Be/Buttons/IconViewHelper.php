<?php
namespace Neulandlotsen\MooxNews\ViewHelpers\Be\Buttons;

/**
 * This file is part of the "moox_news" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */
 
use TYPO3\CMS\Core\Imaging\Icon;
use TYPO3\CMS\Core\Imaging\IconFactory;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Viewhelper which returns save button with icon
 *
 * # Example: Basic example
 * <code>
 * <f:be.buttons.icon uri="{f:uri.action()}" />
 * </code>
 * <output>
 * An icon button as known from the TYPO3 backend, skinned and linked
 * with the default action of the current controller.
 * </output>
 *
 * # Example: Basic example II
 * <code>
 * <n:be.buttons.icon uri="{f:uri.action(action:'index')}" icon="tcarecords-tx_mooxnews_domain_model_news-default"
 * title="{f:translate(key:'LLL:EXT:news/Resources/Private/Language/locallang_be.xlf:module.newsListing')}" />
 * </code>
 * <output>
 * A linked button with the icon of a news record which is linked to the index action
 * </output>
 *
 * @license http://www.gnu.org/copyleft/gpl.html
 */
class IconViewHelper extends \TYPO3\CMS\Fluid\ViewHelpers\Be\AbstractBackendViewHelper
{
    /**
     * @var bool
     */
    protected $escapeOutput = false;
    
    /**
     * Renders an icon link as known from the TYPO3 backend
     *
     * @param string $icon Icon to be used
     * @param string $uri the target URI for the link
     * @param string $title Title attribute of the resulting link
     * @param string $onclick onclick setting
     * @param string $class css class
     * @return string the rendered icon link
     */
    public function render($icon = 'closedok', $uri = '', $title = '', $onclick = '', $class = '')
    {
        $iconFactory = GeneralUtility::makeInstance(IconFactory::class);
        $icon = '<span title="' . htmlspecialchars($title) . '">'
            . $iconFactory->getIcon($icon, Icon::SIZE_SMALL)
            . '</span>';

        if (empty($uri) && empty($onclick))
		{
            $content = $icon;
        } 
		else 
		{
            $content = '<a onclick="' . htmlspecialchars($onclick) . '" href="' . htmlspecialchars($uri) . '" class="' . htmlspecialchars($class) . '">' . $icon . '</a>';
        }

        return $content;
    }
}
