<?php
namespace Neulandlotsen\MooxNews\Service;

/**
 * This file is part of the "moox_news" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Utility\DebuggerUtility;

class DataHandler extends \TYPO3\CMS\Core\DataHandling\DataHandler 
{

	/**
	* __construct
	*/
	public function __construct() 
	{
		parent::__construct();
	}

	/**
	* Simulate Backend User for DataHandler
	*/
	public function simulateBackendUser() 
	{
		/** @var \TYPO3\CMS\Backend\FrontendBackendUserAuthentication $BE_USER */
		$BE_USER = GeneralUtility::makeInstance('TYPO3\\CMS\\Backend\\FrontendBackendUserAuthentication');
		$BE_USER->setBeUserByName('datahandler');
		if ($BE_USER->user['uid'])
		{
			$BE_USER->fetchGroupData();
		}
		$BE_USER->uc_default['copyLevels']= '9999';
		$BE_USER->uc = $BE_USER->uc_default;
		$GLOBALS['PAGES_TYPES'][254]['allowedTables'] = '*';
		return $BE_USER;
	}

	/**
	* Copying a single record
	*
	*/
	public function copyRecordLocal($table = "tx_mooxnews_domain_model_news", $object, $first = 0, $overrideValues = [], $excludeFields = '', $language = 0, $ignoreLocalization = false)
	{
		if($object instanceof \TYPO3\CMS\Extbase\DomainObject\DomainObjectInterface)
		{			
			$this->BE_USER = $this->simulateBackendUser();
			$this->userid = $this->BE_USER->user['uid'];
			$this->username = $this->BE_USER->user['username'];
			$this->admin = true;

			if (!is_object($GLOBALS['LANG']))
			{
				$GLOBALS['LANG'] = GeneralUtility::makeInstance('\\TYPO3\\CMS\\Lang\\LanguageService');
				$GLOBALS['LANG']->csConvObj = GeneralUtility::makeInstance('\\TYPO3\\CMS\\Core\\Charset\\CharsetConverter');
			}
			
			$newTitle = "Kopie von ".$object->getTitle();
			
			$overrideValues['title'] = $newTitle;
			
			//return parent::copyRecord($table, $object->getUid(), $object->getPid(), 1, $overrideValues, $excludeFields, $language);
			return parent::copyRecord_raw($table, $object->getUid(), $object->getPid(), $overrideValues);
		}
	}
	
	/**
	* Copying a single record
	*
	* @param \TYPO3\CMS\Extbase\DomainObject\DomainObjectInterface $value
	* @param string $table
	* @param boolean $first Is a flag set, if the record copied is NOT a 'slave' to another record copied. That is, if this record was asked to be copied in the cmd-array
	* @param array $overrideValues Associative array with field/value pairs to override directly. Notice; Fields must exist in the table record and NOT be among excluded fields!
	* @param string $excludeFields Commalist of fields to exclude from the copy process (might get default values)
	* @param integer $language Language ID (from sys_language table)
	* @param boolean $ignoreLocalization
	* @return integer ID of new record, if any
	*/
	public function copyNewsRecord($table = "tx_mooxnews_domain_model_news", $object, $overrideValues = [], $excludeFields = [])
	{
		if($object instanceof \TYPO3\CMS\Extbase\DomainObject\DomainObjectInterface)
		{			
			$this->BE_USER = $this->simulateBackendUser();
			$this->userid = $this->BE_USER->user['uid'];
			$this->username = $this->BE_USER->user['username'];
			$this->admin = true;

			if (!is_object($GLOBALS['LANG']))
			{
				$GLOBALS['LANG'] = GeneralUtility::makeInstance('\\TYPO3\\CMS\\Lang\\LanguageService');
				$GLOBALS['LANG']->csConvObj = GeneralUtility::makeInstance('\\TYPO3\\CMS\\Core\\Charset\\CharsetConverter');
			}
			
			if($overrideValues['postfix']=="")
			{
				$overrideValues['postfix'] = "Kopie";
			}
			
			$newTitle = $object->getTitle()." [".$overrideValues['postfix']."]";
			unset($overrideValues['postfix']);
			
			$overrideValues['title'] = $newTitle;
			
			echo "ebbes";
			
			return $this->copyRecord($table, $object->getUid(), $object->getPid(), 1, $overrideValues, $excludeFields, $language);
			//return parent::copyRecord_raw($table, $object->getUid(), $object->getPid(), $overrideValues);
		}
	}
}

?>