<?php
$EM_CONF[$_EXTKEY] = [
	'title' => 'MOOX Core',
	'description' => 'TYPO3 Responsive using Sass, Bootstrap and jQuery. Extended by MOOX Templates, MOOX News and other MOOX Extensions.',
	'category' => 'fe',
	'state' => 'beta',
	'uploadfolder' => false,
	'clearCacheOnLoad' => true,
	'author' => 'Dominic Martin',
	'author_email' => 'typo3@neulandlotsen.de',
	'author_company' => 'Neulandlotsen',
	'version' => '9.5.2',
	'constraints' => [
		'depends' => [
			'typo3' => '9.5.0-9.5.99',
			'flux' => '9.2.0-0.0.0',
			'vhs' => '5.2.0-0.0.0',
			'fluidpages' => '5.0.0-0.0.0',
			'min' => '1.7.0-0.0.0',
		],
		'suggests' => [
			'powermail' => '7.3.0-0.0.0',
			'staticfilecache' => '8.2.0-0.0.0',
			'fal_securedownload' => '2.2.0-0.0.0'
		],
	],
	'autoload' => [
		'psr-4' => [
			'Neulandlotsen\\MooxCore\\' => 'Classes',
			'Leafo\\ScssPhp\\' => 'ScssPhp'
		]
	]
];