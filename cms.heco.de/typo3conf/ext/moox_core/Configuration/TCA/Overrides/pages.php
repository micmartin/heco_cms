<?php
if (!defined('TYPO3_MODE'))
{
	die ('Access denied.');
}

$GLOBALS['TCA']['pages']['columns']['tx_fed_page_controller_action']['config']['renderType'] = "selectSingleWithPreviewImage";
$GLOBALS['TCA']['pages']['columns']['tx_fed_page_controller_action_sub']['config']['renderType'] = "selectSingleWithPreviewImage";
$GLOBALS['TCA']['pages']['columns']['slug']['config']['generatorOptions']['fields'][0] = ['nav_title', 'title'];
$GLOBALS['TCA']['pages']['columns']['slug']['config']['generatorOptions']['replacements']['/'] = "-";
$GLOBALS['TCA']['pages']['columns']['tx_fed_page_controller_action']['l10n_mode'] = 'exclude';
$GLOBALS['TCA']['pages']['columns']['tx_fed_page_controller_action_sub']['l10n_mode'] = 'exclude';
