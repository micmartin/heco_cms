<?php
$return = array(
	'ctrl' => array(
		'title'	=> 'LLL:EXT:moox_mailer/Resources/Private/Language/locallang_db.xlf:tx_mooxmailer_domain_model_opening',
		'label' => 'title',
		'tstamp' => 'tstamp',
		'crdate' => 'crdate',
		'cruser_id' => 'cruser_id',
		'dividers2tabs' => TRUE,
		//'sortby' => 'crdate',
		'default_sortby' => 'ORDER BY crdate DESC',
		'versioningWS' => 2,
		'versioning_followPages' => TRUE,
		'origUid' => 't3_origuid',
		'languageField' => 'sys_language_uid',
		'transOrigPointerField' => 'l10n_parent',
		'transOrigDiffSourceField' => 'l10n_diffsource',
		'delete' => 'deleted',
		'enablecolumns' => array(
			'disabled' => 'hidden',
			'starttime' => 'starttime',
			'endtime' => 'endtime',
		),		
		'searchFields' => 'title,body',
        'iconfile' => 'EXT:moox_mailer/Resources/Public/Icons/tx_mooxmailer_domain_model_opening.png',
		'hideTable' => TRUE
	),
	'interface' => array(
		'showRecordFieldList' => 'sys_language_uid, l10n_parent, l10n_diffsource, fe_user, address, job',
	),
	'types' => array(
		'1' => array('showitem' => 'sys_language_uid;;;;1-1-1, l10n_parent, l10n_diffsource, fe_user, address, job'),
	),
	'palettes' => array(
		'1' => array('showitem' => ''),
	),
	'columns' => array(
		'sys_language_uid' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.language',
			'config' => array(
				'type' => 'select',
				'foreign_table' => 'sys_language',
				'foreign_table_where' => 'ORDER BY sys_language.title',
				'items' => array(
					array('LLL:EXT:lang/locallang_general.xlf:LGL.allLanguages', -1),
					array('LLL:EXT:lang/locallang_general.xlf:LGL.default_value', 0)
				),
			),
		),
		'l10n_parent' => array(
			'displayCond' => 'FIELD:sys_language_uid:>:0',
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.l18n_parent',
			'config' => array(
				'type' => 'select',
				'items' => array(
					array('', 0),
				),
				'foreign_table' => 'tx_mooxmailer_domain_model_opening',
				'foreign_table_where' => 'AND tx_mooxmailer_domain_model_opening.pid=###CURRENT_PID### AND tx_mooxmailer_domain_model_opening.sys_language_uid IN (-1,0)',
			),
		),
		'l10n_diffsource' => array(
			'config' => array(
				'type' => 'passthrough',
			),
		),
		't3ver_label' => array(
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.versionLabel',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'max' => 255,
			)
		),
		'hidden' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.hidden',
			'config' => array(
				'type' => 'passthrough',
			)
		),
		'cruser_id' => array(
			'label' => 'cruser_id',
			'config' => array(
				'type' => 'passthrough'
			)
		),
		'pid' => array(
			'label' => 'pid',
			'config' => array(
				'type' => 'passthrough'
			)
		),
		'crdate' => array(
			'label' => 'crdate',
			'config' => array(
				'type' => 'passthrough',
			)
		),
		'tstamp' => array(
			'label' => 'tstamp',
			'config' => array(
				'type' => 'passthrough',
			)
		),
		'starttime' => array(
			'exclude' => 1,
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.starttime',
			'config' => array(
				'type' => 'passthrough',
			),
		),
		'endtime' => array(
			'exclude' => 1,
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.endtime',
			'config' => array(
				'type' => 'passthrough',
			),
		),
		'fe_user' => array(			
			'exclude' => 0,
			'label' => 'LLL:EXT:moox_mailer/Resources/Private/Language/locallang_db.xlf:tx_mooxmailer_domain_model_opening.fe_user',
			'config' => array(
				'type' => 'select',
				'allowNonIdValues' => 1,
				'default' => '',
				'foreign_table' => 'fe_users',
				'foreign_table_where' => 'ORDER BY fe_users.last_name',
        		'size' => 1,				
				'maxitems'      => 1,
				'minitems'      => 0,
				'multiple' => 0,        		
				'items' => array(
					array('Keine Auswahl', ''),
				),				
				'wizards' => array(
					'_PADDING' => 1,
					'_VERTICAL' => 1,
          			'suggest' => array(
            		'type' => 'suggest'
          			),
				),
			),			
		),
		'address' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:moox_mailer/Resources/Private/Language/locallang_db.xlf:tx_mooxmailer_domain_model_opening.address',
			'config' => array(
				'type' => 'select',
				'allowNonIdValues' => 1,
				'default' => '',
				'foreign_table' => 'tx_mooxaddress_domain_model_address',
				'foreign_table_where' => 'ORDER BY tx_mooxaddress_domain_model_address.surname',
				'foreign_field' => 'bounces',
        		'size' => 1,				
				'maxitems'      => 1,
				'minitems'      => 0,
				'multiple' => 0,        		
				'items' => array(
					array('Keine Auswahl', ''),
				),				
				'wizards' => array(
					'_PADDING' => 1,
					'_VERTICAL' => 1,
          			'suggest' => array(
            		'type' => 'suggest'
          			),
				),
			),			
		),
		'title' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:moox_mailer/Resources/Private/Language/locallang_db.xlf:tx_mooxmailer_domain_model_opening.title',
			'config' => array(
				'type' => 'input',
				'size' => 40,
				'eval' => 'required,trim'
			),
		),
		'job' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:moox_mailer/Resources/Private/Language/locallang_db.xlf:tx_mooxmailer_domain_model_opening.job',
			'config' => array(
				'type' => 'input',
				'size' => 40,
				'eval' => 'required,trim'
			),
		),
		'job_type' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:moox_mailer/Resources/Private/Language/locallang_db.xlf:tx_mooxmailer_domain_model_opening.job_type',
			'config' => array(
				'type' => 'passthrough',
			),
		),
		'first_opening' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:moox_mailer/Resources/Private/Language/locallang_db.xlf:tx_mooxmailer_domain_model_opening.first_opening',
			'config' => array(
				'type' => 'passthrough',
			),
		),
		'last_opening' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:moox_mailer/Resources/Private/Language/locallang_db.xlf:tx_mooxmailer_domain_model_opening.last_opening',
			'config' => array(
				'type' => 'passthrough',
			),
		),
		'openings' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:moox_mailer/Resources/Private/Language/locallang_db.xlf:tx_mooxmailer_domain_model_opening.openings',
			'config' => array(
				'type' => 'passthrough',
			),
		),					
	),
);
// set default language file as ll-reference
$extConf = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Configuration\ExtensionConfiguration::class)->get('moox_mailer');

// if mailer tracking is active
if($extConf['useMailerTracking']){
	if(!\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::isLoaded('moox_address')){
		unset($return['columns']['address']);
	}
	return $return;
} else {
	return array();
}
?>