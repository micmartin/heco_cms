<?php
$EM_CONF[$_EXTKEY] = [
	'title' => 'MOOX Template (heco.de)',
	'description' => 'MOOX-Vorlage für heco.de',
	'category' => 'fe',
	'state' => 'beta',
	'uploadfolder' => false,
	'clearCacheOnLoad' => true,
	'author' => 'Dominic Martin',
	'author_email' => 'typo3@neulandlotsen.de',
	'author_company' => 'Neulandlotsen',
	'version' => '9.5.0',
	'constraints' => [
		'depends' => [			
			'moox_bootstrap4' => '9.5.0-9.5.99'
		],
		'conflicts' => [],
		'suggests' => [],
	],
	'autoload' => [
		'psr-4' => [
			'Neulandlotsen\\MooxTemplateHeco\\' => 'Classes'
		]
	]
];
?>

