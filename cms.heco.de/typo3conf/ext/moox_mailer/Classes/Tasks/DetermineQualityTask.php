<?php
namespace Neulandlotsen\MooxMailer\Tasks;

/**
 * This file is part of the "moox_mailer" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */
 
use Neulandlotsen\MooxMailer\Service\BounceService;
use Neulandlotsen\MooxMailer\Utility\SchedulerHelper;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Messaging\FlashMessage;
use TYPO3\CMS\Core\Messaging\FlashMessageService;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
 
/**
 * Determin feuser/address quality
 *
 * @package moox_mailer
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class DetermineQualityTask extends \TYPO3\CMS\Scheduler\Task\AbstractTask 
{		
	/**
	 * @var \TYPO3\CMS\Core\Messaging\FlashMessageService
	 */
	public $flashMessageService;
	
	/**
	 * Determin feuser/address quality
	 *
	 * @return	bool	Returns TRUE on success, FALSE if no items were indexed or none were found.
	 * @see	typo3/sysext/scheduler/tx_scheduler_Task#execute()
	 */
	public function execute()
	{											
		$executionSucceeded = TRUE;
			
		$objectManager = GeneralUtility::makeInstance('TYPO3\CMS\Extbase\Object\ObjectManager');
		
		$flashMessageService = $objectManager->get(FlashMessageService::class);
		$flashMessageQueue = $flashMessageService->getMessageQueueByIdentifier();
			
		$bounceRepository = $objectManager->get('Neulandlotsen\MooxMailer\Domain\Repository\BounceRepository');			
		$frontendUserRepository = $objectManager->get('Neulandlotsen\MooxMailer\Domain\Repository\FrontendUserRepository');
			
		if(ExtensionManagementUtility::isLoaded('moox_address'))
		{
			$addressRepository = $objectManager->get('Neulandlotsen\MooxMailer\Domain\Repository\AddressRepository');
			$mooxAddressLoaded = true;
		}			
					
		$resetOlderThanThat = mktime (date("H"),date("i"),date("s"),date("n"),date("j")-14,date("Y"));
					
		$feUserResetCnt = 0;
		$feUserErrorCnt = 0;					
					
		foreach ($frontendUserRepository->findByQuality([1,2]) AS $feUser)
		{
			if($feUser->getEmail()!="" && SchedulerHelper::checkMail($feUser->getEmail())!="")
			{
				$feUser->setQuality(2);
				$frontendUserRepository->update($feUser);
				$feUserErrorCnt++;
			} 
			else 
			{
				$bounces = $feUser->getBounces();							
				if($bounces->count()>0)
				{
					$latestBounce = $feUser->getBounces()->current();
					if($latestBounce->getBounced()<$resetOlderThanThat)
					{
						$feUser->setQuality(0);
						$frontendUserRepository->update($feUser);
						$feUserResetCnt++;
					}								
				} 
				else 
				{
					$feUser->setQuality(0);
					$frontendUserRepository->update($feUser);
					$feUserResetCnt++;
				}
			}
		}
		
		if($mooxAddressLoaded)
		{			
			$addressResetCnt = 0;
			$addressErrorCnt = 0;
			
			foreach ($addressRepository->findByQuality([1,2]) AS $address)
			{
				if($address->getEmail()!="" && SchedulerHelper::checkMail($address->getEmail())!="")
				{
					$address->setQuality(2);
					$addressRepository->update($address);
					$addressErrorCnt++;
				} 
				else 
				{
					$bounces = $address->getBounces();							
					if($bounces->count()>0)
					{
						$latestBounce = $address->getBounces()->current();
						if($latestBounce->getBounced()<$resetOlderThanThat)
						{
							$address->setQuality(0);
							$addressRepository->update($address);
							$addressResetCnt++;
						}								
					} 
					else 
					{
						$address->setQuality(0);
						$addressRepository->update($address);
						$addressResetCnt++;
					}
				}
			}
		}
		
		if($feUserResetCnt || $feUserErrorCnt || $addressResetCnt || $addressErrorCnt)
		{
			$objectManager->get('TYPO3\CMS\Extbase\Persistence\PersistenceManagerInterface')->persistAll();
		}
		
		$messageTxt = "";
		
		if($feUserResetCnt)
		{
			$messageTxt .= ', '.$feUserResetCnt.' user(s) resetted to good quality';
		}
		if($feUserErrorCnt)
		{
			$messageTxt .= ', '.$feUserErrorCnt.' user(s) set to poor quality';
		}
		
		if($mooxAddressLoaded)
		{
			if($addressResetCnt)
			{
				$messageTxt .= ', '.$addressResetCnt.' address(es) resetted to good quality';
			}
			if($addressErrorCnt)
			{
				$messageTxt .= ', '.$addressErrorCnt.' address(es) set to poor quality';
			}
		}
		
		if($messageTxt==""
		){
			$messageTxt = "Nothing to do";
		}

		$message = GeneralUtility::makeInstance('TYPO3\\CMS\\Core\\Messaging\\FlashMessage',
			$messageTxt,
			'', // the header is optional
			FlashMessage::OK, // the severity is optional as well and defaults to FlashMessage::OK
			TRUE // optional, whether the message should be stored in the session or only in the FlashMessageQueue object (default is FALSE)
		);
		$flashMessageQueue->addMessage($message);
				
		return $executionSucceeded;
	}
	
	/**
	 * This method returns the sleep duration as additional information
	 *
	 * @return string Information to display
	 */
	public function getAdditionalInformation()
	{		
		$info = "";
				
		return $info;
	}		
}
?>