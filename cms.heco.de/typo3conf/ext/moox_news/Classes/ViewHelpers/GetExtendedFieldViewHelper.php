<?php
namespace Neulandlotsen\MooxNews\ViewHelpers;

/**
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */
 
class GetExtendedFieldViewHelper extends \TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper {

	/**
	 * @param object $newsItem current news object
	 * @param string $extendedFieldName	
	 * @return string
	 */
	public function render($newsItem, $extendedFieldName) {
		$value = "";
		if($extendedFieldName != ""){
			$getCall = "get".ucfirst($extendedFieldName);
			if(method_exists($newsItem, $getCall)){
				$value = $newsItem->$getCall(true);
			} elseif(is_array($newsItem)){
				$value = $newsItem[$extendedFieldName];
			}
		}
		return $value;
	}
}