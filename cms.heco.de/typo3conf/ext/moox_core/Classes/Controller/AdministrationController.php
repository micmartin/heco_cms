<?php
namespace Neulandlotsen\MooxCore\Controller;

/**
 * This file is part of the "moox_core" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;
use Leafo\ScssPhp\Compiler;
use Leafo\ScssPhp\Formatter\Compressed;
 
/**
 * The main Controller, managing all the tasks for moox core dashboard management
 */
class AdministrationController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController {

    /**
     * extConf
     *
     * @var boolean
     */
    protected $extConf;

    /**
     * initialize the controller
     *
     * @return void
     */
    protected function initializeAction() {
        parent::initializeAction();
        $this->extConf = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Configuration\ExtensionConfiguration::class)->get('moox_core');
    }
 
    /**
     * action dashboard
     *
     * @return void
     */
    public function dashboardAction()
    {
    }

    /**
     * action designer
     *
     * @return void
     */
    public function designerAction() {
    }
}