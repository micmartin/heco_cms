<?php
namespace Neulandlotsen\MooxMailer\Tasks;

/**
 * This file is part of the "moox_mailer" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */
 
use Neulandlotsen\MooxMailer\Service\BounceService;
use Neulandlotsen\MooxMailer\Service\MailerService;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Messaging\FlashMessage;
use TYPO3\CMS\Core\Messaging\FlashMessageService;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;

class GetBouncesTask extends \TYPO3\CMS\Scheduler\Task\AbstractTask 
{			
	/**
	 * flash message service
	 *
	 * @var \TYPO3\CMS\Core\Messaging\FlashMessageService
	 */
	public $flashMessageService;
	
	/**
	 * Process bounce mails from mailbox
	 *
	 * @return	bool	Returns TRUE on success, FALSE if no items were indexed or none were found.
	 * @see	typo3/sysext/scheduler/tx_scheduler_Task#execute()
	 */
	public function execute()
	{											
		if(function_exists("imap_open"))
		{		
			$executionSucceeded = TRUE;
			
			$objectManager = GeneralUtility::makeInstance('TYPO3\CMS\Extbase\Object\ObjectManager');
			
			$flashMessageService = $objectManager->get(FlashMessageService::class);
			$flashMessageQueue = $flashMessageService->getMessageQueueByIdentifier();
			
			$bounceRepository = $objectManager->get('Neulandlotsen\MooxMailer\Domain\Repository\BounceRepository');			
			$frontendUserRepository = $objectManager->get('Neulandlotsen\MooxMailer\Domain\Repository\FrontendUserRepository');
			
			if(ExtensionManagementUtility::isLoaded('moox_address'))
			{
				$addressRepository = $objectManager->get('Neulandlotsen\MooxMailer\Domain\Repository\AddressRepository');
				$mooxAddressLoaded = true;
			}
			
			$newsletterRepository = $objectManager->get('Neulandlotsen\MooxMailer\Domain\Repository\NewsletterRepository');
			
			$deliveryRepository = $objectManager->get('Neulandlotsen\MooxMailer\Domain\Repository\DeliveryRepository');
			
			// Get the extensions's configuration
			$extConf = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Configuration\ExtensionConfiguration::class)->get('moox_mailer');
			
			$bounceService = new BounceService;
			
			if($extConf['bouncePop3Server']!="" && $extConf['bouncePop3Username']!="" && $extConf['bouncePop3Password']!="")
			{				
				if($extConf['bouncePop3Encrypt']=="ssl")
				{
					$imap = \imap_open("{".$extConf['bouncePop3Server']."/pop3/ssl/novalidate-cert}INBOX",$extConf['bouncePop3Username'],$extConf['bouncePop3Password']);
				} 
				else 
				{
					$imap = \imap_open("{".$extConf['bouncePop3Server']."/pop3}INBOX",$extConf['bouncePop3Username'],$extConf['bouncePop3Password']);
				}
				if ($imap)
				{
					$totalrows = imap_num_msg($imap);
					$bounceCnt = 0;
					
					//iterate through all unread mails
					for ($index = 0; $index < $totalrows; $index++)
					{
						$bounceObject = [];
						
						// TODO: bounced datetime
						
						$header = imap_header($imap, $index + 1);
						$mailheader = imap_fetchheader($imap , $index + 1);
						$mailbody = imap_body($imap, $index + 1);
						$mailbody = imap_utf8($mailbody);
						$mailbody = str_replace("=09","",$mailbody);
						$mailbody = str_replace("=3D"," ",$mailbody);
						$bouncemail = imap_utf8($mailheader).$mailbody;
						
						$bounceObject['return-to'] = $header->toaddress;
						
						if(strpos($bounceObject['return-to'],"@".$extConf['bounceDomain']))
						{						
							$bounceObject['title'] = $bounceObject['return-to'];
							$bounceObject['bounced'] = strtotime($header->date);
							
							$identifier = explode("@",$bounceObject['return-to']);
							$identifier = explode("_",$identifier[0]);
							
							$bounceObject['job'] = $identifier[0]."_".$identifier[1];					
							switch ($identifier[2])
							{
								case 1:
									$bounceObject['task'] = "mooxnewssendtask";
									$bounceObject['jobType'] = "task";
								break;
								case 2:
									$bounceObject['task'] = "newsletter";
									$bounceObject['jobType'] = "newsletter";
								break;						
								default:
									$bounceObject['task'] = "undefined";
									$bounceObject['jobType'] = "undefined";
							}	
							$feUser = NULL;
							$address = NULL;
							switch ($identifier[3])
							{
								case 1:
									$bounceObject['receiver_type'] = "feuser";
									$feUser = $frontendUserRepository->findBouncedByUid($identifier[4]);
								break;
								case 2:
									$bounceObject['receiver_type'] = "address";
									if($mooxAddressLoaded)
									{
										$address = $addressRepository->findBouncedByUid($identifier[4]);
									}
								break;						
								default:
									$bounceObject['receiver_type'] = "unknown";
							}					
							$bounceObject['receiver_uid'] = $identifier[4];					
							$bounceObject['sent'] = strtotime($identifier[6]);
							
							$facts = $bounceService->get_the_facts($bouncemail);
							
							$bounceObject['action'] = $facts[0]['action'];
							$bounceObject['code'] = $facts[0]['status'];
							$bounceObject['recipient'] = $facts[0]['recipient'];					
							$human = $bounceService->fetch_status_message_as_array(
								$facts[0]['status']                      
							);
							
							$bounceObject['major_code'] = $human['major_code'];
							$bounceObject['sub_code'] = $human['sub_code'];
							$bounceObject['status'] = $human['title'];
							$bounceObject['status_description'] = $human['description'];
							$bounceObject['type'] = $human['sub_title'];
							$bounceObject['type_description'] = $human['sub_description'];
							
							$bounceObject['mail'] = $bouncemail;
							
							$newBounce = $objectManager->get('Neulandlotsen\MooxMailer\Domain\Model\Bounce');
							
							$newBounce->setTitle($bounceObject['title']);
							$newBounce->setAction($bounceObject['action']);
							$newBounce->setCode($bounceObject['code']);
							$newBounce->setMajorCode($bounceObject['major_code']);
							$newBounce->setSubCode($bounceObject['sub_code']);
							$newBounce->setMail($bounceObject['mail']);
							$newBounce->setJob($bounceObject['job']);
							$newBounce->setJobType($bounceObject['jobType']);
							$newBounce->setBounced($bounceObject['bounced']);
							$newBounce->setSent($bounceObject['sent']);
							$newBounce->setStatus($bounceObject['status']);
							$newBounce->setStatusDescription($bounceObject['status_description']);
							$newBounce->setType($bounceObject['type']);
							$newBounce->setTypeDescription($bounceObject['type_description']);
							$newBounce->setRecipient($bounceObject['recipient']);
							$newBounce->setReceiverType($bounceObject['receiver_type']);
							$newBounce->setReceiverUid($bounceObject['receiver_uid']);
							
							$bounceRepository->add($newBounce);
							
							if(is_object($feUser))
							{
								$feUser->addBounce($newBounce);
								if($bounceObject['action']=='failed')
								{
									$feUser->setQuality(2);							
								} 
								elseif($bounceObject['action']=='transient')
								{
									$feUser->setQuality(1);								
								}
								$frontendUserRepository->update($feUser);
							}
							
							if($mooxAddressLoaded && is_object($address))
							{
								$address->addBounce($newBounce);
								if($bounceObject['action']=='failed')
								{
									$address->setQuality(2);								
								} 
								elseif($bounceObject['action']=='transient')
								{
									$address->setQuality(1);								
								}
								$addressRepository->update($address);
							}
							
							imap_delete($imap, $index + 1);	
							
							if($bounceObject['jobType']=="newsletter")
							{
								$jobInfo = MailerService::parseMailerJob($bounceObject['job']);
								$newsletter = $newsletterRepository->findByUid($jobInfo['uid'],FALSE);
								if(is_object($newsletter))
								{
									$newsletter->setBounces($newsletter->getBounces()+1);
									$newsletterRepository->update($newsletter);
									unset($newsletter);
								}
								if($extConf['useMailerTracking'])
								{
									if($bounceObject['receiver_type']=="feuser")
									{
										$delivery = $deliveryRepository->findByJobAndFeUser($bounceObject['job'],$bounceObject['receiver_uid']);					
									} 
									elseif($bounceObject['receiver_type']=="address")
									{
										$delivery = $deliveryRepository->findByJobAndAddress($bounceObject['job'],$bounceObject['receiver_uid']);
									}									
									if(is_object($delivery))
									{
										$delivery->setBounced(1);										
										$deliveryRepository->update($delivery);
										unset($delivery);
									}
								}
							}
							$bounceCnt++;					
						}
					}
					
					$resetOlderThanThat = mktime (date("H"),date("i"),date("s"),date("n"),date("j")-14,date("Y"));
					
					$feUserResetCnt = 0;
					$feUserErrorCnt = 0;					
					
					foreach ($frontendUserRepository->findByQuality([1,2]) AS $feUser)
					{
						if($feUser->getEmail()!="" && MailerService::checkMail($feUser->getEmail())!="")
						{
							$feUser->setQuality(2);
							$frontendUserRepository->update($feUser);
							$feUserErrorCnt++;
						} 
						else 
						{
							$bounces = $feUser->getBounces();							
							if($bounces->count()>0)
							{
								$latestBounce = $feUser->getBounces()->current();
								if($latestBounce->getBounced()<$resetOlderThanThat)
								{
									$feUser->setQuality(0);
									$frontendUserRepository->update($feUser);
									$feUserResetCnt++;
								}								
							} 
							else 
							{
								$feUser->setQuality(0);
								$frontendUserRepository->update($feUser);
								$feUserResetCnt++;
							}
						}
					}
					
					if($mooxAddressLoaded)
					{
						
						$addressResetCnt = 0;
						$addressErrorCnt = 0;
						
						foreach ($addressRepository->findByQuality([1,2]) AS $address)
						{
							if($address->getEmail()!="" && MailerService::checkMail($address->getEmail())!="")
							{
								$address->setQuality(2);
								$addressRepository->update($address);
								$addressErrorCnt++;
							} 
							else 
							{
								$bounces = $address->getBounces();							
								if($bounces->count()>0)
								{
									$latestBounce = $address->getBounces()->current();
									if($latestBounce->getBounced()<$resetOlderThanThat)
									{
										$address->setQuality(0);
										$addressRepository->update($address);
										$addressResetCnt++;
									}								
								} 
								else 
								{
									$address->setQuality(0);
									$addressRepository->update($address);
									$addressResetCnt++;
								}
							}
						}
					}
					
					if($bounceCnt || $feUserResetCnt || $feUserErrorCnt || $addressResetCnt || $addressErrorCnt)
					{
						$objectManager->get('TYPO3\CMS\Extbase\Persistence\PersistenceManagerInterface')->persistAll();
					}
					
					//close connection to mailbox
					imap_expunge($imap);
					imap_close($imap);
					
					$messageTxt = $bounceCnt.' Bounce(s) catched';
					
					if($feUserResetCnt)
					{
						$messageTxt .= ', '.$feUserResetCnt.' user(s) resetted to good quality';
					}
					if($feUserErrorCnt)
					{
						$messageTxt .= ', '.$feUserErrorCnt.' user(s) set to poor quality';
					}
					
					if($mooxAddressLoaded)
					{
						if($addressResetCnt)
						{
							$messageTxt .= ', '.$addressResetCnt.' address(es) resetted to good quality';
						}
						if($addressErrorCnt)
						{
							$messageTxt .= ', '.$addressErrorCnt.' address(es) set to poor quality';
						}
					}

					$message = GeneralUtility::makeInstance('TYPO3\\CMS\\Core\\Messaging\\FlashMessage',
						$messageTxt,
						'', // the header is optional
						FlashMessage::OK, // the severity is optional as well and defaults to FlashMessage::OK
						TRUE // optional, whether the message should be stored in the session or only in the FlashMessageQueue object (default is FALSE)
					);
					$flashMessageQueue->addMessage($message);
				 }
				 else
				 {
					 $message = GeneralUtility::makeInstance('TYPO3\\CMS\\Core\\Messaging\\FlashMessage',
						'Can\'t connect: ' . imap_last_error(),
						'', // the header is optional
						FlashMessage::ERROR, // the severity is optional as well and defaults to FlashMessage::OK
						TRUE // optional, whether the message should be stored in the session or only in the FlashMessageQueue object (default is FALSE)
					);
					$flashMessageQueue->addMessage($message);				
				 }
				
			} 
			else 
			{
				$message = GeneralUtility::makeInstance('TYPO3\\CMS\\Core\\Messaging\\FlashMessage',
					'No mailbox defined',
					'', // the header is optional
					FlashMessage::ERROR, // the severity is optional as well and defaults to FlashMessage::OK
					TRUE // optional, whether the message should be stored in the session or only in the FlashMessageQueue object (default is FALSE)
				);
				$flashMessageQueue->addMessage($message);
			}
		} 
		else 
		{
			$message = GeneralUtility::makeInstance('TYPO3\\CMS\\Core\\Messaging\\FlashMessage',
				'Die Funktion "imap_open" die f�r den Abruf von Bounce-Mails ben�tigt werden, ist auf dem Server nicht installiert.',
				'Bounce-Handling',
				FlashMessage::ERROR, // the severity is optional as well and defaults to FlashMessage::OK
				TRUE // optional, whether the message should be stored in the session or only in the FlashMessageQueue object (default is FALSE)
			);
			$flashMessageQueue->addMessage($message);
		}
				
		return $executionSucceeded;
	}
	
	/**
	 * This method returns the sleep duration as additional information
	 *
	 * @return string Information to display
	 */
	public function getAdditionalInformation()
	{		
		$info = "";
				
		return $info;
	}		
}
?>