<?php
namespace Neulandlotsen\MooxMailer\Domain\Model;

/**
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */

use TYPO3\CMS\Extbase\Persistence\ObjectStorage;

class FrontendUser extends \TYPO3\CMS\Extbase\Domain\Model\FrontendUser
{

	/**
	 * @var int
	 */
	protected $crdate;

	/**
	 * @var int
	 */
	protected $tstamp;

	/**
	 * @var int
	 */
	protected $gender;

	/**
	 * @var string
	 */
	protected $username;

	/**
	 * @var bool
	 */
	protected $disallowMailing;

	/**
	 * @var int
	 */
	protected $quality;

	/**
	 * @var string
	 */
	protected $autoHash;

	/**
	 * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Neulandlotsen\MooxMailer\Domain\Model\Bounce>
	 */
	protected $bounces = NULL;

	/**
	 * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Neulandlotsen\MooxMailer\Domain\Model\Error>
	 */
	protected $errors = NULL;

	/**
	 * @var string
	 */
	protected $mailerTags;

	/**
	 * @var string
	 */
	protected $mailerZipregions;

	/**
	 * __construct
	 */
	public function __construct()
	{
		//Do not remove the next line: It would break the functionality
		$this->initStorageObjects();
	}

	/**
	 * Initializes all ObjectStorage properties
	 * Do not modify this method!
	 * It will be rewritten on each save in the extension builder
	 * You may modify the constructor of this class instead
	 *
	 * @return void
	 */
	protected function initStorageObjects()
	{
		$this->bounces = new ObjectStorage();
		$this->errors = new ObjectStorage();
	}

	/**
	 * @return int $crdate gender
	 */
	public function getCrdate()
	{
		return $this->crdate;
	}

	/**
	 * @param int $crdate crdate
	 * @return void
	 */
	public function setCrdate($crdate)
	{
		$this->crdate = $crdate;
	}

	/**
	 * @return int
	 */
	public function getTstamp()
	{
		return $this->tstamp;
	}

	/**
	 * @param int $tstamp time stamp
	 * @return void
	 */
	public function setTstamp($tstamp)
	{
		$this->tstamp = $tstamp;
	}

	/**
	 * @return int $gender gender
	 */
	public function getGender()
	{
		return $this->gender;
	}

	/**
	 * @param int $gender gender
	 * @return void
	 */
	public function setGender($gender)
	{
		$this->gender = $gender;
	}

	/**
	 * @return string $username username
	 */
	public function getUsername() {
		return $this->username;
	}

	/**
	 * @param int $username username
	 * @return void
	 */
	public function setUsername($username)
	{
		$this->username = $username;
	}

	/**
	 * @return int $disallowMailing disallow mailing
	 */
	public function getDisallowMailing()
	{
		return $this->disallowMailing;
	}

	/**
	 * @param int $disallowMailing disallow mailing
	 * @return void
	 */
	public function setDisallowMailing($disallowMailing)
	{
		$this->disallowMailing = $disallowMailing;
	}

	/**
	 * @return int $quality quality
	 */
	public function getQuality()
	{
		return $this->quality;
	}

	/**
	 * @param int $quality quality
	 * @return void
	 */
	public function setQuality($quality)
	{
		$this->quality = $quality;
	}

	/**
	 * @return string
	 */
	public function getAutoHash()
	{
		return md5(
			$this->uid.$this->crdate.$this->username.$this->password.$GLOBALS['TYPO3_CONF_VARS']['SYS']['encryptionKey']
		);
	}

	/**
	 * @param \Neulandlotsen\MooxMailer\Domain\Model\Bounce $bounce
	 * @return void
	 */
	public function addBounce(\Neulandlotsen\MooxMailer\Domain\Model\Bounce $bounce)
	{
		$this->bounces->attach($bounce);
	}

	/**
	 * @param \Neulandlotsen\MooxMailer\Domain\Model\Bounce $bounceToRemove The bounce to be removed
	 * @return void
	 */
	public function removeBounce(\Neulandlotsen\MooxMailer\Domain\Model\Bounce $bounceToRemove)
	{
		$this->bounces->detach($bounceToRemove);
	}

	/**
	 * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Neulandlotsen\MooxMailer\Domain\Model\Bounce> $bounces
	 */
	public function getBounces()
	{
		return $this->bounces;
	}

	/**
	 * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Neulandlotsen\MooxMailer\Domain\Model\Bounce> $bounces
	 * @return void
	 */
	public function setBounces(ObjectStorage $bounces)
	{
		$this->bounces = $bounces;
	}

	/**
	 * @param \Neulandlotsen\MooxMailer\Domain\Model\Error $error
	 * @return void
	 */
	public function addError(\Neulandlotsen\MooxMailer\Domain\Model\Error $error)
	{
		$this->bounces->attach($error);
	}

	/**
	 * @param \Neulandlotsen\MooxMailer\Domain\Model\Error $errorToRemove The error to be removed
	 * @return void
	 */
	public function removeError(\Neulandlotsen\MooxMailer\Domain\Model\Error $errorToRemove)
	{
		$this->errors->detach($errorToRemove);
	}

	/**
	 * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Neulandlotsen\MooxMailer\Domain\Model\Error> $error
	 */
	public function getErrors()
	{
		return $this->error;
	}

	/**
	 * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Neulandlotsen\MooxMailer\Domain\Model\Error> $error
	 * @return void
	 */
	public function setErrors(ObjectStorage $error)
	{
		$this->error = $error;
	}

	/**
	 * @return string $mailerTags
	 */
	public function getMailerTags()
	{
		return $this->mailerTags;
	}

	/**
	 * @param string $mailerTags
	 * @return void
	 */
	public function setMailerTags($mailerTags)
	{
		$this->mailerTags = $mailerTags;
	}

	/**
	 * @return string $mailerZipregions
	 */
	public function getMailerZipregions()
	{
		return $this->mailerZipregions;
	}

	/**
	 * @param string $mailerZipregions
	 * @return void
	 */
	public function setMailerZipregions($mailerZipregions)
	{
		$this->mailerZipregions = $mailerZipregions;
	}
}
?>