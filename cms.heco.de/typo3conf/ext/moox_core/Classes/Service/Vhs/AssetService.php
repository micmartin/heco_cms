<?php
namespace Neulandlotsen\MooxCore\Service\Vhs;

/*
 * This file is part of the "moox_core" project under GPLv2 or later.
 *
 * For the full copyright and license information, please read the
 * LICENSE.md file that was distributed with this source code.
 */


/**
 * Asset Handling Service
 *
 * Inject this Service in your class to access VHS Asset
 * features - include assets etc.
 */
class AssetService extends \FluidTYPO3\Vhs\Service\AssetService
{
	/**
	 * @param string $markerName
	 * @param mixed $assets
	 * @return void
	 */
	protected function insertAssetsAtMarker($markerName, $assets)
	{
		$assetMarker = '<!-- VhsAssets' . $markerName . ' -->';
		if (false === strpos($GLOBALS['TSFE']->content, $assetMarker)) {
			$inFooter = (boolean) (false !== strpos($markerName, 'Footer'));
			$tag = true === $inFooter ? '</body>' : '<link rel="stylesheet" type="text/css" href="/typo3temp/mooxcore/theme_';
			$content = $GLOBALS['TSFE']->content;
			$position = strrpos($content, $tag);

			if ($position) {
				$GLOBALS['TSFE']->content = substr_replace($content, $assetMarker . LF, $position, 0);
			}
		}
		if (true === is_array($assets)) {
			$chunk = $this->buildAssetsChunk($assets);
		} else {
			$chunk = $assets;
		}
		$GLOBALS['TSFE']->content = str_replace($assetMarker, $chunk, $GLOBALS['TSFE']->content);
	}
}
