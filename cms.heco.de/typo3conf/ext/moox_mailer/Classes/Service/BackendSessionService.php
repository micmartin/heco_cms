<?php
namespace Neulandlotsen\MooxMailer\Service;

/**
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */
 
class BackendSessionService extends \TYPO3\CMS\Extbase\Persistence\Repository {	
	
	/**
     * @var string 
     */
	protected $storageKey = 'tx_mooxmailer';

	/**
      * @param string $storageKey 
	  */
	public function setStorageKey($storageKey)
	{ 
		$this->storageKey = $storageKey; 
	}

	/**
	  * Wert in der Session speichern
	  * @param string $key
	  * @param mixed $value
	  * @return bool
	  */
	public function store($key, $value)
	{
		$data = $GLOBALS['BE_USER']->getSessionData($this->storageKey);
		$data[ $key ] = $value;
		return $GLOBALS['BE_USER']->setAndSaveSessionData($this->storageKey, $data);
	}

	/**
	  * Wert l�schen
	  * @param string $key
	  * @return bool
	  */
	public function delete($key)
	{
		$data = $GLOBALS['BE_USER']->getSessionData($this->storageKey);
		unset($data[ $key ]);
		return $GLOBALS['BE_USER']->setAndSaveSessionData($this->storageKey, $data);
	}


	/**
	  * Wert auslesen
	  * @param string $key
	  * @return mixed
	  */
	public function get($key)
	{
		$data = $GLOBALS['BE_USER']->getSessionData($this->storageKey);
		return isset($data[ $key ]) ? $data[ $key ] : NULL;
	}
}
?>