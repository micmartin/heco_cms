<?php
namespace Neulandlotsen\MooxMailer\Controller;

/**
 * This file is part of the "moox_mailer" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */
 
use Neulandlotsen\MooxMailer\Domain\Repository\TemplateRepository;
use Neulandlotsen\MooxMailer\Domain\Repository\ScheduleRepository;
use Neulandlotsen\MooxMailer\Domain\Session\BackendSessionHandler;
use Neulandlotsen\MooxMailer\Service\HelperService;
use Neulandlotsen\MooxMailer\Service\MooxNewsMailerService;
use TYPO3\CMS\Core\Messaging\FlashMessage;
use TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface;
 
class TemplateController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController 
{
	/**
	 * @var TemplateRepository
	 */
	protected $templateRepository;
	
	/**
	 * @var ConfigurationManagerInterface
	 */
	protected $configurationManager;
	
	/**
	 * @var ScheduleRepository
	 */
	protected $scheduleRepository;	
	
	/**
	 * @var HelperService
	 */
	protected $helperService;
	
	/**
	 * @var MooxNewsMailerService
	 */
	protected $mooxNewsMailerService;
	
	/**
	 * @var array 	
	 */
	protected $fields;
	
	/**
	 * @var bool
	 */
	protected $extConf;	
	
	/**
	 * @var array	
	 */
	protected $configuration;

    /**
     * @param TemplateRepository $templateRepository
     */
    public function injectTemplateRepository(TemplateRepository $templateRepository)
    {
        $this->templateRepository = $templateRepository;
    }

    /**
     * @param ConfigurationManagerInterface $configurationManager
     */
    public function injectConfigurationManagerInterface(ConfigurationManagerInterface $configurationManager)
    {
        $this->configurationManager = $configurationManager;
    }

    /**
     * @param ScheduleRepository $scheduleRepository
     */
    public function injectScheduleRepository(ScheduleRepository $scheduleRepository)
    {
        $this->scheduleRepository = $scheduleRepository;
    }

    /**
     * @param HelperService $helperService
     */
    public function injectHelperService(HelperService $helperService)
    {
        $this->helperService = $helperService;
    }

    /**
     * @param MooxNewsMailerService $mooxNewsMailerService
     */
    public function injectMooxNewsMailerService(MooxNewsMailerService $mooxNewsMailerService)
    {
        $this->mooxNewsMailerService = $mooxNewsMailerService;
    }

    /**
     * @param BackendSessionHandler $backendSession
     */
    public function injectBackendSessionHandler(BackendSessionHandler $backendSession)
    {
        $this->backendSession = $backendSession;
    }
	
	/**
	 * initialize the controller
	 *
	 * @return void
	 */
	protected function initializeAction()
	{
		$this->extConf 	= \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Configuration\ExtensionConfiguration::class)->get('moox_mailer');
				
		$this->fields['sender_address'] = array (
			'key' => 'sender_address',
			'extkey' => 'moox_comment',			
			'config' => array (
				'type' => 'email',
				'validate' => 1,
				'validator' => 'email'
			)
		);
		
		$this->fields['reply_to_address'] = array (
			'key' => 'reply_to_address',
			'extkey' => 'moox_comment',			
			'config' => array (
				'type' => 'email',
				'validate' => 1,
				'validator' => 'email'
			)
		);
		
		$this->configuration = $this->configurationManager->getConfiguration(ConfigurationManagerInterface::CONFIGURATION_TYPE_FRAMEWORK,"MooxMailer");
	}
	
	/**
	 * action index
	 *
	 * @return void
	 */
	public function indexAction()
	{					
		$this->view->assign('templates', $this->templateRepository->findAll(false));
		$this->view->assign('action', 'show');
	}
	
	/**
	 * action add
	 *	
	 * @param array $add
	 * @return void
	 */
	public function addAction($add = array())
	{					
		// init action arrays
		$messages = array();
		$errors = array();
		
		if(isset($add['save']) || isset($add['saveAndClose']) ||  isset($add['saveAndNew'])){
						
			// check fields
			$this->helperService->checkFields($this->fields,$add,$messages,$errors);	
			
			// no errors -> add comment
			if(!count($errors)){
			
				$object = $this->objectManager->get('Neulandlotsen\\MooxMailer\\Domain\\Model\\Template');
				$object->setTitle($add['title']);
				$object->setSubject($add['subject']);
				$object->setSenderAddress($add['sender_address']);
				$object->setSenderName($add['sender_name']);
				$object->setReplyToAddress($add['reply_to_address']);
				$object->setTemplate($add['template']);
				
				$this->templateRepository->add($object);								
				$this->objectManager->get('TYPO3\CMS\Extbase\Persistence\PersistenceManagerInterface')->persistAll();
				
				$this->addFlashMessage(
					'', 
					'Vorlage wurde erfolgreich gespeichert.', 
					FlashMessage::OK
				);
				
			} else {
				// set flash messages
				$this->helperService->setFlashMessages($this,$messages);
			}
		}
		
		if(!count($errors)){
			if(isset($add['save'])){
				$this->redirect("edit",NULL,NULL,array('uid' => $object->getUid()));
			} elseif(isset($add['saveAndClose'])){
				$this->redirect("index");
			} elseif(isset($add['saveAndNew'])){			
				$this->redirect("add");
			} 
		} 			
	
		$this->view->assign('object', $add);			
		$this->view->assign('action', 'add');
	}
	
	/**
	 * action edit
	 *
	 * @param int $uid
	 * @param array $edit
	 * @return void
	 */
	public function editAction($uid = 0, $edit = array())
	{					
		// init action arrays
		$messages = array();
		$errors = array();
		
		if($uid>0){
		
			$object = $this->templateRepository->findByUid($uid);
			
			if(!count($edit)){
				$edit['title'] = $object->getTitle();
				$edit['subject'] = $object->getSubject();
				$edit['sender_address'] = $object->getSenderAddress();
				$edit['sender_name'] = $object->getSenderName();
				$edit['reply_to_address'] = $object->getReplyToAddress();				
				$edit['template'] = $object->getTemplate();				
				$edit['uid'] = $object->getUid();				
			}
			$edit['subject_prepared'] = $this->mooxNewsMailerService->prepareSubject($edit['subject'],[
				'title' => 'Lorem ipsum dolor sit amet',
				'datetime' => time(),
				'mooxNewsSeminarStartdate' => time(),
				'mooxNewsSeminarEnddate' => time()+86400,
			]);
			if(isset($edit['save']) || isset($edit['saveAndClose']) ||  isset($edit['saveAndNew'])){				
				
				// check fields
				$this->helperService->checkFields($this->fields,$edit,$messages,$errors);	
				
				// no errors -> add comment
				if(!count($errors)){
				
					$object->setTitle($edit['title']);
					$object->setSubject($edit['subject']);
					$object->setSenderAddress($edit['sender_address']);
					$object->setSenderName($edit['sender_name']);
					$object->setReplyToAddress($edit['reply_to_address']);
					$object->setTemplate($edit['template']);
					
					$this->templateRepository->update($object);								
					$this->objectManager->get('TYPO3\CMS\Extbase\Persistence\PersistenceManagerInterface')->persistAll();
					
					$this->addFlashMessage(
						'', 
						'Änderungen wurden erfolgreich gespeichert.', 
						FlashMessage::OK
					);
					
				} else {
					// set flash messages
					$this->helperService->setFlashMessages($this,$messages);
				}
			}
			
			if(!count($errors)){
				if(isset($edit['saveAndClose'])){
					$this->redirect("index");
				} elseif(isset($edit['saveAndNew'])){
					$this->redirect("add");
				} 
			} 
			
			$this->view->assign('object', $edit);				
			$this->view->assign('action', 'edit');
			$this->view->assign('uid', $uid);
			
		} else {
			$this->redirect("index");
		}
	}
	
	/**
	 * action delete
	 *	
	 * @param int $uid
	 * @return void
	 */
	public function deleteAction($uid = 0)
	{					
		if($uid>0){
		
			$object = $this->templateRepository->findByUid($uid);
			
			$this->templateRepository->remove($object);
			
			$this->objectManager->get('TYPO3\CMS\Extbase\Persistence\PersistenceManagerInterface')->persistAll();
			
			$this->addFlashMessage(
				'', 
				'Vorlage wurde gelöscht.', 
				FlashMessage::OK
			);
						
		} 
		
		$this->redirect("index");
	}
	
	/**
	 * action preview iframe
	 *
	 * @param int $uid
	 * @return void
	 */
	public function previewIframeAction($uid = 0)
	{									
		if($uid>0){
		
			$template = $this->templateRepository->findByUid($uid);
			
			$data['gender'] = 1;
			$data['title'] = "Dr.";
			$data['name'] = "Hans Mustermann";
			$data['firstName'] = "Hans";
			$data['middleName'] = "Jürgen";
			$data['lastName'] = "Mustermann";
			$data['email'] = "email@example.net";
			$data['isSingle'] = 0;
			$data['templateUid'] = $uid;
			$data['redirectorPid'] = 0;
			$data['redirectorType'] = "javascript:void()";
			
			$data['news'][1]['category'] = "Kategorie 1";
			$data['news'][1]['baseUrl'] = "https://www.neulandlotsen.de";
			$data['news'][1]['categoryImage']['originalResource']['originalFile']['name'] = "previewCategory1.jpg";
			$data['news'][1]['categoryImage']['originalResource']['originalFile']['identifier'] = "/mailer/previewCategory1.jpg";
			$data['news'][1]['items'][1]['mooxNewsSeminarStartdate'] = time();
			$data['news'][1]['items'][1]['mooxNewsSeminarEnddate'] = time()+86400;
			$data['news'][1]['items'][1]['singlePid'] = 0;
			$data['news'][1]['items'][1]['baseUrl'] = '';
			$data['news'][1]['items'][1]['url'] = 'javascript:void()';
			$data['news'][1]['items'][1]['datetime'] = time();
			$data['news'][1]['items'][1]['title'] = 'Lorem ipsum dolor sit amet';
			$data['news'][1]['items'][1]['teaser'] = 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.';
			$data['news'][1]['items'][1]['bodytext'] = 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.';
			$data['news'][1]['items'][1]['fulltext'] = 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.';
			
			$data['news'][1]['items'][2]['mooxNewsSeminarStartdate'] = time();
			//$data['news'][1]['items'][2]['mooxNewsSeminarEnddate'] = time()+86400;
			$data['news'][1]['items'][2]['singlePid'] = 0;
			$data['news'][1]['items'][2]['baseUrl'] = '';
			$data['news'][1]['items'][2]['url'] = 'javascript:void()';
			$data['news'][1]['items'][2]['datetime'] = time();
			$data['news'][1]['items'][2]['type'] = 'moox_news_seminar';
			$data['news'][1]['items'][2]['title'] = 'Seminar: Consetetur sadipscing elitr';
			$data['news'][1]['items'][2]['teaser'] = 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.';
			$data['news'][1]['items'][2]['bodytext'] = 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.';
			$data['news'][1]['items'][2]['fulltext'] = 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.';
			
			$data['news'][2]['category'] = "Kategorie 2";
			$data['news'][2]['baseUrl'] = "https://www.neulandlotsen.de";
			$data['news'][2]['categoryImage']['originalResource']['originalFile']['name'] = "previewCategory2.jpg";
			$data['news'][2]['categoryImage']['originalResource']['originalFile']['identifier'] = "/mailer/previewCategory2.jpg";
			$data['news'][2]['items'][1]['mooxNewsSeminarStartdate'] = time();
			$data['news'][2]['items'][1]['mooxNewsSeminarEnddate'] = time()+86400;
			$data['news'][2]['items'][1]['singlePid'] = 0;
			$data['news'][2]['items'][1]['baseUrl'] = '';
			$data['news'][2]['items'][1]['url'] = 'javascript:void()';
			$data['news'][2]['items'][1]['datetime'] = time();
			$data['news'][2]['items'][1]['type'] = 'moox_news_seminar';
			$data['news'][2]['items'][1]['title'] = 'Seminar: Sed diam nonumy eirmod tempor';
			$data['news'][2]['items'][1]['teaser'] = 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.';
			$data['news'][2]['items'][1]['bodytext'] = 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.';
			$data['news'][2]['items'][1]['fulltext'] = 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.';
			
			$previewView = $this->objectManager->get('TYPO3\\CMS\\Fluid\\View\StandaloneView');
			$previewView->setFormat('html');      
			$previewView->setTemplateSource($template->getTemplate());
			$previewView->setPartialRootPaths($this->configuration['view']['partialRootPaths']);
			$previewView->assignMultiple(array("data" => $data));
			$preview = $previewView->render();
			
			$this->view->assign('preview', $preview);
		} else {
			$this->view->assign('preview', "Vorschau kann nicht angezeigt werden.");
		}		
	}		
	
	/**
	 * Returns ext conf
	 *
	 * @return array
	 */
	public function getExtConf()
	{
		return $this->extConf;
	}

	/**
	 * Set ext conf
	 *
	 * @param array $extConf ext conf
	 * @return void
	 */
	public function setExtConf($extConf)
	{
		$this->extConf = $extConf;
	}
}
?>