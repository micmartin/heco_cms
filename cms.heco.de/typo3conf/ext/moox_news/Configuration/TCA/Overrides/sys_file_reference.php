<?php
if (!defined('TYPO3_MODE'))
{
	die ('Access denied.');
}

$ll = 'LLL:EXT:moox_news/Resources/Private/Language/locallang_db.xlf:';

/**
 * Add extra field showinpreview and some special news controls to sys_file_reference record
 */
$newSysFileReferenceColumns = [
    'showinpreview' => [
        'exclude' => true,
        'label' => $ll . 'tx_mooxnews_domain_model_media.showinpreview',
        'config' => [
            'type' => 'check',
            'default' => 0
        ]
    ],
	'hideindetail' => [
		'exclude' => true,
		'label' => $ll . 'tx_mooxnews_domain_model_media.hideindetail',
		'config' => [
			'type' => 'check',
			'default' => 0
		]
	],
	'import_id' => [
		'label' => $ll . 'tx_mooxnews_domain_model_news.import_id',
		'config' => [
			'type' => 'passthrough'
		]
	],
	'import_source' => [
		'label' => $ll . 'tx_mooxnews_domain_model_news.import_source',
		'config' => [
			'type' => 'passthrough'
		]
	],
];

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('sys_file_reference', $newSysFileReferenceColumns);

// add special news palette
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addFieldsToPalette(
	'sys_file_reference', 
	'newsPalette',
	'showinpreview,hideindetail'
);
