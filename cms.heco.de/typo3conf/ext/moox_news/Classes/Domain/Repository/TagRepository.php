<?php

namespace Neulandlotsen\MooxNews\Domain\Repository;

/**
 * This file is part of the "moox_news" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */
use Neulandlotsen\MooxNews\Domain\Model\DemandInterface;
use Neulandlotsen\MooxNews\Utility\Validation;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Persistence\QueryInterface;

/**
 * Repository for tag objects
 */
class TagRepository extends \Neulandlotsen\MooxNews\Domain\Repository\AbstractDemandedRepository
{

    /**
     * Find categories by a given pid
     *
     * @param array $idList list of id s
     * @param array $ordering ordering
     * @param string $startingPoint starting point uid or comma separated list
     * @return QueryInterface
     */
    public function findByIdList(array $idList, array $ordering = [], $startingPoint = null)
    {
        if (empty($idList)) {
            throw new \InvalidArgumentException('The given id list is empty.', 1484823596);
        }
        $query = $this->createQuery();
        $query->getQuerySettings()->setRespectStoragePage(false);
        $query->getQuerySettings()->setRespectSysLanguage(false);

        if (count($ordering) > 0) {
            $query->setOrderings($ordering);
        }

        $conditions = [];
        $conditions[] = $query->in('uid', $idList);

        if ($startingPoint !== null) {
            $conditions[] = $query->in('pid', GeneralUtility::trimExplode(',', $startingPoint, true));
        }

        return $query->matching(
            $query->logicalAnd(
                $conditions
            ))->execute();
    }

    /**
     * Returns an array of constraints created from a given demand object.
     *
     * @param QueryInterface $query
     * @param DemandInterface $demand
	 * @param bool $respectEnableFields
     * @return array<\TYPO3\CMS\Extbase\Persistence\Generic\Qom\ConstraintInterface>
     */
    protected function createConstraintsFromDemand(QueryInterface $query, DemandInterface $demand, $respectEnableFields = true)
    {
        $constraints = [];

        // Storage page
        if ($demand->getStoragePage() != 0) {
            $pidList = GeneralUtility::intExplode(',', $demand->getStoragePage(), true);
            $constraints[] = $query->in('pid', $pidList);
        }

        // Tags
        if ($demand->getTags()) {
            $tagList = GeneralUtility::intExplode(',', $demand->getTags(), true);
            $constraints[] = $query->in('uid', $tagList);
        }

        // Clean not used constraints
        foreach ($constraints as $key => $value) {
            if (is_null($value)) {
                unset($constraints[$key]);
            }
        }

        return $constraints;
    }

    /**
     * Returns an array of orderings created from a given demand object.
     *
     * @param DemandInterface $demand
     * @return array<\TYPO3\CMS\Extbase\Persistence\Generic\Qom\ConstraintInterface>
     */
    protected function createOrderingsFromDemand(DemandInterface $demand)
    {
        $orderings = [];

        if (Validation::isValidOrdering($demand->getOrder(), $demand->getOrderByAllowed())) {
            $orderList = GeneralUtility::trimExplode(',', $demand->getOrder(), true);

            if (!empty($orderList)) {
                // go through every order statement
                foreach ($orderList as $orderItem) {
                    list($orderField, $ascDesc) = GeneralUtility::trimExplode(' ', $orderItem, true);
                    // count == 1 means that no direction is given
                    if ($ascDesc) {
                        $orderings[$orderField] = ((strtolower($ascDesc) == 'desc') ?
                            QueryInterface::ORDER_DESCENDING :
                            QueryInterface::ORDER_ASCENDING);
                    } else {
                        $orderings[$orderField] = QueryInterface::ORDER_ASCENDING;
                    }
                }
            }
        }

        return $orderings;
    }
	
	/**
     * Find tags by a given pid
     *
     * @param int $pid pid
     * @return QueryInterface
     */
    public function findByPid($pid)
    {
        $query = $this->createQuery();
        $query->getQuerySettings()->setRespectStoragePage(false);
		$query->matching(
			$query->equals('pid', (int)$pid)
		);
		$query->setOrderings(
			[
				'title' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING
			]
		);
		return $query->execute();
    }

	/**
	 * Find tags by a given pids
	 *
	 * @param array $pids pids
	 * @return QueryInterface
	 */
	public function findByPids($pids = [])
	{
		$query = $this->createQuery();

		if(!is_array($pids) && $pids)
		{
			$pids = explode(",",$pids);
		}
		elseif(!is_array($pids))
		{
			$pids = [];
		}

		$query->getQuerySettings()->setRespectStoragePage(false);
		$query->matching(
			$query->in('pid', $pids)
		);
		$query->setOrderings(
			[
				'title' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING
			]
		);
		return $query->execute();
	}

	/**
	 * find all tags (overwrite)
	 *	
	 * @param boolean $respectStoragePage
	 * @return array
	 */
	public function findAll($respectStoragePage = TRUE)
	{
		
		$query = $this->createQuery();		
		$query->getQuerySettings()->setRespectStoragePage($respectStoragePage);		
		$result = $query->execute();
		return $result;
				
	}

}
