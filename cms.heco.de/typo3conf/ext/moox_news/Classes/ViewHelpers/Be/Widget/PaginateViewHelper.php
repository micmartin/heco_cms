<?php
namespace Neulandlotsen\MooxNews\ViewHelpers\Be\Widget;

/**
 * This file is part of the "moox_news" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

use Neulandlotsen\MooxNews\ViewHelpers\Be\Widget\Controller\PaginateController;
use TYPO3\CMS\Fluid\Core\Widget\AbstractWidgetViewHelper;
 
class PaginateViewHelper extends AbstractWidgetViewHelper
{
	/**
	 * @var PaginateController
	 */
	protected $controller;

    /**
     * @param PaginateController $paginateController
     */
    public function injectPaginateController(PaginateController $paginateController)
    {
        $this->paginateController = $paginateController;
    }

    public function initializeArguments()
    {
        $this->registerArgument('objects', 'mixed', 'Object', true);
        $this->registerArgument('as', 'string', 'as', true);
        $this->registerArgument('configuration', 'array', 'configuration', false, ['itemsPerPage' => 10, 'insertAbove' => false, 'insertBelow' => true, 'recordsLabel' => '']);
    }

	/**
     * @return string
     */
    public function render()
    {
        return $this->initiateSubRequest();
    }
}