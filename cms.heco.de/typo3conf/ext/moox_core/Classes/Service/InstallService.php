<?php
namespace Neulandlotsen\MooxCore\Service;

use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Core\Messaging\FlashMessage;
use TYPO3\CMS\Core\Utility\VersionNumberUtility;

class InstallService 
{
    /**
    * @var string
    */
    protected $extKey = 'moox_core';
    
    /**
     * @var string
     */
    protected $messageQueueByIdentifier = '';

    /**
     * Initializes the install service
     */
    public function __construct()
	{
	    if(VersionNumberUtility::convertVersionNumberToInteger(TYPO3_version) >= 7000000)
		{
		    $this->messageQueueByIdentifier = 'extbase.flashmessages.tx_extensionmanager_tools_extensionmanagerextensionmanager';
	    }
		else
		{
		    $this->messageQueueByIdentifier = 'core.template.flashMessages';
		}
	}

	/**
	 * @param string $extension
	 */
	public function generateApacheHtaccess($extension = NULL)
	{
		if($extension == $this->extKey)
		{
			if(substr($_SERVER['SERVER_SOFTWARE'], 0, 6) === 'Apache')
			{
				$this->createDefaultHtaccessFile();
			}
		}
	}
	/**
	 * Creates .htaccess file inside the root directory
	 *
	 * @param string $htaccessFile Path of .htaccess file
	 * @return void
	 */
    public function createDefaultHtaccessFile()
	{
        $htaccessFile = GeneralUtility::getFileAbsFileName(".htaccess");
                
        if(file_exists($htaccessFile))
		{            
            /**
             * Add Flashmessage that there is already an .htaccess file and we are not going to override this.
             */
            $flashMessage = GeneralUtility::makeInstance('TYPO3\\CMS\\Core\\Messaging\\FlashMessage',
                'There is already an Apache .htaccess file in the root directory, please make sure that the url rewritings are set properly.'
                . 'An example configuration is located at: typo3conf/ext/moox_core/Configuration/Apache/.htaccess',
                'Apache .htaccess file already exists',
                FlashMessage::NOTICE,
                TRUE
            );
            $this->addFlashMessage($flashMessage);			
		}
		else
		{
			$htaccessContent = GeneralUtility::getUrl(ExtensionManagementUtility::extPath($this->extKey).'/Configuration/Apache/_.htaccess');
			GeneralUtility::writeFile($htaccessFile, $htaccessContent, TRUE);
			
			/**
			 * Add Flashmessage that the example htaccess file was placed in the root directory
			 */
			$flashMessage = GeneralUtility::makeInstance('TYPO3\\CMS\\Core\\Messaging\\FlashMessage',
				'Default .htaccess file was placed in your root directory.'
				. ' Please check if the RewriteBase correctly set for your environment. ',
				'Apache example .htaccess was placed in the root directory.',
				FlashMessage::OK,
				TRUE
		   );
		   $this->addFlashMessage($flashMessage);
		}
    }

	/**
	 * Creates AdditionalConfiguration.php file inside the typo3conf directory
	 *
	 * @param string $configurationFile Path of AdditionalConfiguration.php file
	 * @return void
	 */
	public function createDefaultAdditionalConfiguration($extension = NULL)
	{
		if($extension == $this->extKey)
		{
			$configurationFile = GeneralUtility::getFileAbsFileName("typo3conf/AdditionalConfiguration.php");

			if(file_exists($configurationFile))
			{
				/**
				 * Add Flashmessage that there is already an AdditionalConfiguration.php file and we are not going to override this.
				 */
				$flashMessage = GeneralUtility::makeInstance('TYPO3\\CMS\\Core\\Messaging\\FlashMessage',
						'There is already an AdditionalConfiguration file in the typo3conf directory, please make this line to your configuration "$GLOBALS[\'TYPO3_CONF_VARS\'][\'FE\'][\'contentRenderingTemplates\'] = array(\'mooxcore/Configuration/TypoScript/\');".'
						. 'An example configuration is located at: typo3conf/ext/moox_core/Configuration/AdditionalConfiguration/AdditionalConfiguration.php',
						'AdditionalConfiguration.php file already exists',
						FlashMessage::NOTICE,
						TRUE
				);
				$this->addFlashMessage($flashMessage);
				return;
			}

			$configurationContent = GeneralUtility::getUrl(ExtensionManagementUtility::extPath($this->extKey).'/Configuration/AdditionalConfiguration/AdditionalConfiguration.php');
			GeneralUtility::writeFile($configurationFile, $configurationContent, TRUE);

			/**
			 * Add Flashmessage that the example AdditionalConfiguration.php file was placed in the typo3conf directory
			 */
			$flashMessage = GeneralUtility::makeInstance('TYPO3\\CMS\\Core\\Messaging\\FlashMessage',
				'AdditionalConfiguration',
				'AdditionalConfiguration.php was placed in the typo3conf directory.',
				FlashMessage::OK,
				TRUE
			);
			$this->addFlashMessage($flashMessage);
		}
	}

	/**
	 * Creates robots.txt file inside the root directory
	 *
	 * @param string $robotsFile Path of robots.txt file
	 * @return void
	 */
    public function createDefaultRobots($extension = NULL)
	{
		if($extension == $this->extKey)
		{
			$robotsFileDisallowed = GeneralUtility::getFileAbsFileName("robots.txt");
		    
			if(file_exists($robotsFileDisallowed))
			{			
				/**
				 * Add Flashmessage that there is already an robots.txt file and we are not going to override this.
				 */
				$flashMessage = GeneralUtility::makeInstance('TYPO3\\CMS\\Core\\Messaging\\FlashMessage',
					'robots.txt',
					'There is already an robots.txt file in the root directory.',
					FlashMessage::NOTICE,
					TRUE
				);
				$this->addFlashMessage($flashMessage);				
			}
			else
			{	
				$robotsFileContentDisallowed = "User-Agent: * \n";
				$robotsFileContentDisallowed .= " \n";
				$robotsFileContentDisallowed .= "Disallow: / \n";
				$robotsFileContentDisallowed .= " \n";
				GeneralUtility::writeFile($robotsFileDisallowed, $robotsFileContentDisallowed, TRUE);
				
				/**
				 * Add Flashmessage that there is robots.txt file placed in the website root
				 */
				$flashMessage = GeneralUtility::makeInstance('TYPO3\\CMS\\Core\\Messaging\\FlashMessage',
					'robots.txt',
					'robots.txt was placed in the root directory. Be sure to replace it with robots_allowed.txt after developing.',
					FlashMessage::OK,
					TRUE
				);
				$this->addFlashMessage($flashMessage);
			}
			
			$robotsFileAllowed = GeneralUtility::getFileAbsFileName("robots_allowed.txt");
		    
			if(file_exists($robotsFileAllowed))
			{			
				/**
				 * Add Flashmessage that there is already an robots_allowed.txt file and we are not going to override this.
				 */
				$flashMessage = GeneralUtility::makeInstance('TYPO3\\CMS\\Core\\Messaging\\FlashMessage',
					'robots_allowed.txt',
					'There is already an robots_allowed.txt file in the root directory.',
					FlashMessage::NOTICE,
					TRUE
				);
				$this->addFlashMessage($flashMessage);				
			}
			else
			{	
				$robotsFileContentAllowed = "User-Agent: * \n";
				$robotsFileContentAllowed .= " \n";
				$robotsFileContentAllowed .= "Allow: / \n";
				$robotsFileContentAllowed .= " \n";
				$robotsFileContentAllowed .= # Only allow URLs generated with RealURL \n";
				$robotsFileContentAllowed .= "Disallow: /*?id=* \n";
				$robotsFileContentAllowed .= "Disallow: /*&id=* \n";
				$robotsFileContentAllowed .= " \n";
				$robotsFileContentAllowed .= "# L=0 is the default language \n";
				$robotsFileContentAllowed .= "Disallow: /*?L=0* \n";
				$robotsFileContentAllowed .= "Disallow: /*&L=0* \n";
				$robotsFileContentAllowed .= " \n";
				$robotsFileContentAllowed .= "# typeNum = 98 is usually the print version. \n";
				$robotsFileContentAllowed .= "Disallow: /*?type=98* \n";
				$robotsFileContentAllowed .= "Disallow: /*&type=98* \n";
				$robotsFileContentAllowed .= "Disallow: /*?type=100* \n";
				$robotsFileContentAllowed .= "Disallow: /*&type=100* \n";
				$robotsFileContentAllowed .= " \n";
				$robotsFileContentAllowed .= "# Should always be protected (.htaccess) \n";
				$robotsFileContentAllowed .= "Disallow: /*/Private/* \n";
				$robotsFileContentAllowed .= "Disallow: /fileadmin/templates/html/* \n";
				$robotsFileContentAllowed .= "Disallow: /*/Configuration/* \n";
				$robotsFileContentAllowed .= " \n";
				$robotsFileContentAllowed .= "# Other rules \n";
				$robotsFileContentAllowed .= "Disallow: /*cHash \n";
				$robotsFileContentAllowed .= "Disallow: /typo3/ \n";
				$robotsFileContentAllowed .= "Disallow: /dwnld/* \n";		
				$robotsFileContentAllowed .= " \n";
				$robotsFileContentAllowed .= "Sitemap: http://" .$_SERVER['HTTP_HOST']. "/sitemap.xml";
				GeneralUtility::writeFile($robotsFileAllowed, $robotsFileContentAllowed, TRUE);
			
				/**
				 * Add Flashmessage that there is robots_allowed.txt file placed in the website root
				 */
				$flashMessage = GeneralUtility::makeInstance('TYPO3\\CMS\\Core\\Messaging\\FlashMessage',
					'robots.txt',
					'robots_allowed.txt was placed in the root directory. Be sure to edit and replace it with current robots.txt after developing.',
					FlashMessage::OK,
					TRUE
				);
				$this->addFlashMessage($flashMessage);
			}
		}
	}
    
    /**
     * Adds a Flash Message to the Flash Message Queue
     *
     * @param FlashMessage $flashMessage
     */
    public function addFlashMessage(FlashMessage $flashMessage)
	{
	    if($flashMessage)
		{
		    /** @var $flashMessageService \TYPO3\CMS\Core\Messaging\FlashMessageService */
		    $flashMessageService = GeneralUtility::makeInstance('TYPO3\\CMS\\Core\\Messaging\\FlashMessageService');
		    /** @var $flashMessageQueue \TYPO3\CMS\Core\Messaging\FlashMessageQueue */
		    $flashMessageQueue = $flashMessageService->getMessageQueueByIdentifier($this->messageQueueByIdentifier);
		    $flashMessageQueue->enqueue($flashMessage);
	    }
    }

}
