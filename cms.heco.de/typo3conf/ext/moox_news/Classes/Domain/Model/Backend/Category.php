<?php
namespace Neulandlotsen\MooxNews\Domain\Model\Backend;

/**
 * This file is part of the "moox_news" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

/**
 * Category model for backend
 */
class Category extends \Neulandlotsen\MooxNews\Domain\Model\Category
{
	/**
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Neulandlotsen\MooxNews\Domain\Model\Backend\Category>
     * @TYPO3\CMS\Extbase\Annotation\ORM\Lazy
     */
    protected $children;
		
	/**
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Neulandlotsen\MooxNews\Domain\Model\Backend\Category>
     */
    public function getChildren()
    {
		$this->children = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
		$objectManager 	= \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\CMS\Extbase\Object\ObjectManager');
		$categoryRepository = $objectManager->get('Neulandlotsen\MooxNews\Domain\Repository\Backend\CategoryRepository'); 
		$children = $categoryRepository->findByParent($this->uid,false);
		if($children)
		{
			foreach($children AS $child)
			{
				$this->children->attach($child);
			}
		}
		return $this->children;
    }
}
