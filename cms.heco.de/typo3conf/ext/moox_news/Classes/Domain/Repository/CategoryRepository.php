<?php

namespace Neulandlotsen\MooxNews\Domain\Repository;

/**
 * This file is part of the "moox_news" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */
use Neulandlotsen\MooxNews\Domain\Model\Category;
use Neulandlotsen\MooxNews\Domain\Model\DemandInterface;
use Neulandlotsen\MooxNews\Service\CategoryService;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Persistence\QueryInterface;

/**
 * Category repository with all callable functionality
 *
 */
class CategoryRepository extends \Neulandlotsen\MooxNews\Domain\Repository\AbstractDemandedRepository
{
	protected function createConstraintsFromDemand(
		QueryInterface $query,
		DemandInterface $demand,
		$respectEnableFields = true
	) {
	}

	protected function createOrderingsFromDemand(DemandInterface $demand)
	{
	}

	/**
	 * Find category by import source and import id
	 *
	 * @param string $importSource import source
	 * @param int $importId import id
	 * @return Category
	 */
	public function findOneByImportSourceAndImportId($importSource, $importId)
	{
		$query = $this->createQuery();
		$query->getQuerySettings()->setRespectStoragePage(false);
		$query->getQuerySettings()->setIgnoreEnableFields(true);

		return $query->matching(
			$query->logicalAnd(
				$query->equals('importSource', $importSource),
				$query->equals('importId', $importId)
			)
		)->execute()->getFirst();
	}

	/**
	 * Find parent categories by a given pid
	 *
	 * @param int $pid pid
	 * @return QueryInterface
	 */
	public function findParentCategoriesByPid($pid)
	{
		$query = $this->createQuery();
		$query->getQuerySettings()->setRespectStoragePage(false);
		return $query->matching(
			$query->logicalAnd(
				$query->equals('pid', (int)$pid),
				$query->equals('parentcategory', 0)
			)
		)->execute();
	}

	/**
	 * Find parent categories by a given pids
	 *
	 * @param mixed $pids pids
	 * @param mixed $includeUids includeUids
	 * @param mixed $excludeUids excludeUids
	 * @param bool $ignoreDisabledField ignoreDisabledField
	 * @return QueryInterface
	 */
	public function findParentCategoriesByPids($pids = [], $includeUids = [], $excludeUids = [], $ignoreDisabledField = false)
	{
	    if(is_string($pids))
		{
			if($pids!="")
			{
				$pids = explode(",",$pids);
			}
			else
			{
				$pids = [];
			}
		}
		elseif(is_int($pids))
		{
			$pids = [$pids];
		}
	    elseif (is_null($pids))
        {
            $pids = [];
        }

		if(is_string($includeUids))
		{
			if($includeUids!="")
			{
				$includeUids = explode(",",$includeUids);
			}
			else
			{
				$includeUids = [];
			}
		}
		if(is_string($excludeUids))
		{
			if($excludeUids!="")
			{
				$excludeUids = explode(",",$excludeUids);
			}
			else
			{
				$excludeUids = [];
			}
		}

		$query = $this->createQuery();

		if($ignoreDisabledField)
		{
			$query->getQuerySettings()->setIgnoreEnableFields(true);
			$query->getQuerySettings()->setEnableFieldsToBeIgnored('disabled');
		}

		$query->getQuerySettings()->setRespectStoragePage(false);
		$constraints = [];

		if(count($pids))
		{
			$constraints[] = $query->in('pid', $pids);
		}

		if(count($includeUids))
		{
			$constraints[] = $constraints[] = $query->in('uid', $includeUids);
		}

		if(count($excludeUids))
		{
			$constraints[] = $constraints[] = $query->logicalNot(
				$query->in('uid', $excludeUids)
			);
		}

		$constraints[] = $query->equals('parentcategory', 0);


		$query->setOrderings(
			[
				'title' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING
			]
		);

		return $query->matching(
			$query->logicalAnd($constraints)
		)->execute();
	}

	/**
	 * Find categories by a given pid and parent uid
	 *
	 * @param integer $pid pid
	 * @param integer $parent parent uid
	 * @param integer $sysLanguageUid
	 * @return QueryInterface
	 */
	public function findCategoriesByParentAndPid(
		$pid,
		$parent = 0,
		$sysLanguageUid = 0
	){
		$query = $this->createQuery();
		$query->getQuerySettings()->setRespectStoragePage(FALSE);
		if($sysLanguageUid>0)
		{
			$query->getQuerySettings()->setSysLanguageUid($sysLanguageUid);
		}
		return $query->matching(
			$query->logicalAnd(
				$query->equals('pid', (int)$pid),
				$query->equals('parentcategory', (int)$parent)
			)
		)->execute();
	}

	/**
	 * Find categories by a given pid and type
	 *
	 * @param integer $pid pid
	 * @param array $types types
	 * @param integer $sysLanguageUid
	 * @return QueryInterface
	 */
	public function findCategoriesByTypeAndPid(
		$pid,
		$types = [],
		$sysLanguageUid = 0
	){
		$query = $this->createQuery();

		$query->getQuerySettings()->setRespectStoragePage(FALSE);

		if($sysLanguageUid>0)
		{
			$query->getQuerySettings()->setSysLanguageUid($sysLanguageUid);
		}
		if($sysLanguageUid=="" || $sysLanguageUid<0)
		{
			$sysLanguageUid = 0;
		}
		$langStmt = "sys_language_uid=".$sysLanguageUid." AND ";

		if(!is_array($types)){
			$types = [$types];
		}

		$typeStmt = "";
		foreach($types as $type)
		{
			$typeStmt .= ' OR FIND_IN_SET ("'.$type.'", news_types)';
		}

		$stmt = 'SELECT * FROM sys_category WHERE '.$langStmt.'pid='.(int)$pid.' AND parent=0 AND (news_types=""'.$typeStmt.')';

		return $query->statement($stmt)->execute();
		// TODO: check if still relevant
		/*
		return $query->matching(
			$query->logicalAnd(
				$query->equals('pid', (int)$pid),
				$query->logicalAnd(
					$query->equals('parentcategory', 0),
					$query->logicalOr(
						$query->equals('news_type', ''),
						$query->in('news_types', array($type))
					)
				)
			)
		)->execute();
		*/
	}

	/**
	 * Find all categories
	 *
	 * @param integer $sysLanguageUid
	 * @return QueryInterface
	 */
	public function findAllCategories($sysLanguageUid = 0)
	{
		$query = $this->createQuery();
		$query->getQuerySettings()->setRespectStoragePage(FALSE);
		if($sysLanguageUid>0)
		{
			$query->getQuerySettings()->setSysLanguageUid($sysLanguageUid);
		}
		return $query->execute();
		// TODO: check if still relevant
		/*
		return $query->matching(
			$query->equals('parentcategory', 0)
		)->execute();
		*/
	}

	/**
	 * Find all categories by parent
	 *
	 * @param integer $parent parent uid
	 * @param integer $sysLanguageUid
	 * @return QueryInterface
	 */
	public function findAllCategoriesByParent(
		$parent = 0,
		$sysLanguageUid = 0
	){
		$query = $this->createQuery();
		$query->getQuerySettings()->setRespectStoragePage(FALSE);
		if($sysLanguageUid>0)
		{
			$query->getQuerySettings()->setSysLanguageUid($sysLanguageUid);
		}

		return $query->matching(
			$query->equals('parentcategory', (int)$parent)
		)->execute();
	}

	/**
	 * Find all categories by types
	 *
	 * @param array $types types
	 * @param integer $sysLanguageUid
	 * @return QueryInterface
	 */
	public function findAllCategoriesByType(
		$types = [],
		$sysLanguageUid = 0
	) {
		$query = $this->createQuery();
		$query->getQuerySettings()->setRespectStoragePage(FALSE);
		if($sysLanguageUid>0)
		{
			$query->getQuerySettings()->setSysLanguageUid($sysLanguageUid);
		}
		if($sysLanguageUid=="" || $sysLanguageUid<0)
		{
			$sysLanguageUid = 0;
		}
		$langStmt = "sys_language_uid=".$sysLanguageUid." AND ";

		if(!is_array($types)){
			$types = [$types];
		}

		$typeStmt = "";
		foreach($types as $type)
		{
			$typeStmt .= ' OR FIND_IN_SET ("'.$type.'", news_types)';
		}

		$stmt = 'SELECT * FROM sys_category WHERE '.$langStmt.' parent=0 AND (news_types=""'.$typeStmt.')';

		return $query->statement($stmt)->execute();
		// TODO: check if still relevant
		/*
		return $query->matching(
			$query->logicalAnd(
				$query->equals('parentcategory', 0),
				$query->logicalOr(
					$query->equals('news_type', ''),
					$query->in('news_type', array($type))
				)
			)
		)->execute();
		*/
	}

	/**
	 * Find category tree
	 *
	 * @param array $rootIdList list of id s
	 * @param string $startingPoint startingPoint
	 * @param bool $ignoreDisabledField ignoreDisabledField
	 * @param mixed $includeUids includeUids
	 * @param mixed $excludeUids excludeUids
	 * @return QueryInterface|array
	 */
	public function findTree(
		array $rootIdList,
		$startingPoint = null,
		$ignoreDisabledField = false,
		$includeUids = [],
		$excludeUids = []
	)
	{
		if (!is_array($rootIdList) || count($rootIdList)<1) {
			return [];
		}

		$subCategories = CategoryService::getChildrenCategories(implode(',', $rootIdList));

		$idList = explode(',', $subCategories);
		if (empty($idList)) {
			return [];
		}

		if(is_string($includeUids))
		{
			if($includeUids!="")
			{
				$includeUids = explode(",",$includeUids);
			}
			else
			{
				$includeUids = [];
			}
		}
		if(is_string($excludeUids))
		{
			if($excludeUids!="")
			{
				$excludeUids = explode(",",$excludeUids);
			}
			else
			{
				$excludeUids = [];
			}
		}

		$ordering = ['sorting' => QueryInterface::ORDER_ASCENDING];
		$categories = $this->findByIdList($idList, $ordering, $startingPoint, $ignoreDisabledField);
		$flatCategories = [];
		/** @var Category $category */
		foreach ($categories as $category)
		{
			if((!count($excludeUids) || (count($excludeUids) && !in_array($category->getUid(),$excludeUids))) && (!count($includeUids) || (count($includeUids) && in_array($category->getUid(),$includeUids))))
			{
				$flatCategories[$category->getUid()] = [
					'item' => $category,
					'parent' => ($category->getParentcategory()) ? $category->getParentcategory()->getUid() : null
				];
			}
		}

		$tree = [];

		// If leaves are selected without its parents selected, those are shown as parent
		foreach ($flatCategories as $id => &$flatCategory)
		{
			if (!isset($flatCategories[$flatCategory['parent']]))
			{
				$flatCategory['parent'] = null;
			}
		}

		foreach ($flatCategories as $id => &$node)
		{
			if ($node['parent'] === null) {
				$tree[$id] = &$node;
			} else {
				$flatCategories[$node['parent']]['children'][$id] = &$node;
			}
		}

		return $tree;
	}

	/**
	 * Find categories by a given pid
	 *
	 * @param array $idList list of id s
	 * @param array $ordering ordering
	 * @param string $startingPoint startingPoint
	 * @param bool $ignoreDisabledField ignoreDisabledField
	 * @return QueryInterface
	 */
	public function findByIdList(array $idList, array $ordering = [], $startingPoint = null, $ignoreDisabledField = false)
	{
		if (empty($idList)) {
			throw new \InvalidArgumentException('The given id list is empty.', 1484823597);
		}
		$query = $this->createQuery();
		$query->getQuerySettings()->setRespectStoragePage(false);
		$query->getQuerySettings()->setRespectSysLanguage(false);
		if($ignoreDisabledField)
		{
			$query->getQuerySettings()->setIgnoreEnableFields(true);
			$query->getQuerySettings()->setEnableFieldsToBeIgnored(['disabled','fe_group']);
		}

		if (count($ordering) > 0) {
			$query->setOrderings($ordering);
		}
		$this->overlayTranslatedCategoryIds($idList);

		$conditions = [];
		$conditions[] = $query->in('uid', $idList);

		if (is_null($startingPoint) === false) {
			$conditions[] = $query->in('pid', GeneralUtility::trimExplode(',', $startingPoint, true));
		}

		return $query->matching(
			$query->logicalAnd(
				$conditions
			))->execute();
	}

	/**
	 * Find categories by a given parent
	 *
	 * @param int $parent parent
	 * @return QueryInterface
	 */
	public function findChildren($parent)
	{
		$query = $this->createQuery();
		$query->getQuerySettings()->setRespectStoragePage(false);
		return $query->matching(
			$query->logicalAnd(
				$query->equals('parentcategory', (int)$parent)
			))->execute();
	}

	/**
	 * Overlay the category ids with the ones from current language
	 *
	 * @param array $idList
	 * return void
	 */
	protected function overlayTranslatedCategoryIds(array &$idList)
	{
		$language = $this->getSysLanguageUid();
		if ($language > 0 && !empty($idList)) {
			if (isset($GLOBALS['TSFE']) && is_object($GLOBALS['TSFE'])) {
				$whereClause = 'sys_language_uid=' . $language . ' AND l10n_parent IN(' . implode(',',
						$idList) . ')' . $GLOBALS['TSFE']->sys_page->enableFields('sys_category');
				$rows = (array)$GLOBALS['TYPO3_DB']->exec_SELECTgetRows('l10n_parent, uid,sys_language_uid', 'sys_category',
					$whereClause);

				$idList = $this->replaceCategoryIds($idList, $rows);
			}
			// @todo currently only implemented for the frontend
		}
	}

	/**
	 * Get the current sys language uid
	 *
	 * @return int
	 */
	protected function getSysLanguageUid()
	{
		$sysLanguage = 0;
		if (isset($GLOBALS['TSFE']) && is_object($GLOBALS['TSFE'])) {
			$sysLanguage = $GLOBALS['TSFE']->sys_language_content;
		} elseif (intval(\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('L'))) {
			$sysLanguage = intval(\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('L'));
		}

		return $sysLanguage;
	}

	/**
	 * Replace ids in array by the given ones
	 *
	 * @param array $idList
	 * @param array $rows
	 * @return array
	 */
	protected function replaceCategoryIds(array $idList, array $rows)
	{
		foreach ($rows as $row) {
			$pos = array_search($row['l10n_parent'], $idList);
			if ($pos !== false) {
				$idList[$pos] = (int)$row['uid'];
			}
		}

		return $idList;
	}

	/**
	 * Find news by pid(list)
	 *
	 * @param array $pids pids
	 * @return QueryInterface
	 */
	public function findByPids($pids = [])
	{

		$pids 	= (is_array($pids))?$pids:[$pids];

		$query = $this->createQuery();

		$query->getQuerySettings()->setStoragePageIds($pids);

		return $query->execute();
	}
}
