# ==============================================
# FE-Plugin configuration for EXT:moox_news
# ==============================================
plugin.tx_mooxnews {
	mvc.callDefaultActionIfActionCantBeResolved = 1
	features {
		requireCHashArgumentForActionArguments = 0
	}
	# Modify the translation
	_LOCAL_LANG {
		default {
			# read_more = more >>
		}
	}
	
	# ====================================
	# Settings available inside Controller and View by accessing $this->settings or {settings.xyz}
	# ====================================
	settings {
		
		languageDefaultIsoFlag =
		languageDefaultLanguageLabel =
		
		cssFile = {$plugin.tx_mooxnews.settings.cssFile}
		feLoginPid = {$plugin.tx_mooxnews.settings.feLoginPid}
		icsNamespace = {$module.tx_mooxnews.settings.icsNamespace}
		icsDomain = {$module.tx_mooxnews.settings.icsDomain}
		icsLanguage = {$module.tx_mooxnews.settings.icsLanguage}
		shortcutUrl = {$module.tx_mooxnews.settings.shortcutUrl}

		#Displays a dummy image if the news have no media items
		displayDummyIfNoMedia = 1

		# Output format
		format = html

		# general settings
		overrideFlexformSettingsIfEmpty = cropMaxCharacters,dateField,timeRestriction,orderBy,orderDirection,backPid,listPid,startingpoint,recursive,list.paginate.itemsPerPage,list.paginate.templatePath
		allowEmptyStringsForOverwriteDemand = 0

		includeSubCategories = 0

		analytics {
			social {
				facebookLike = 1
				facebookShare = 1
				twitter = 1
			}
		}

		detailPidDetermination = flexform, categories, item, default
		
		defaultDetailPid = {$plugin.tx_mooxnews.settings.defaultDetailPid}
		dateField = datetime

		link {
			typesOpeningInNewWindow = 2
			hrDate = 0
			hrDate {
				day = j
				month = n
				year = Y
			}

			skipControllerAndAction = 0
		}

		cropMaxCharacters = 150
		orderBy = datetime
		orderDirection = desc
		topNewsFirst = 0
		orderByAllowed = sorting,author,uid,title,teaser,author,tstamp,crdate,datetime,categories.title

		facebookLocale = en_US
		googlePlusLocale = en
		disqusLocale = en

		demandClass =


		# --------------
		#  Search
		# --------------
		search {
			fields = teaser,title,bodytext
		}

		# --------------
		#  Detail
		# --------------
		detail {
			errorHandling = showStandaloneTemplate,EXT:moox_news/Resources/Private/Templates/News/DetailNotFound.html,404
			checkPidOfNewsRecord = 0
			registerProperties = keywords,title
			showPrevNext = 0
			showSocialShareButtons = 1
			disqusShortname =

			# media configuration
			media {
				image {
					maxWidth = 360
					maxHeight =

					# Get lightbox settings from css_styled_content
					lightbox {
						enabled = {$styles.content.imgtext.linkWrap.lightboxEnabled}
						class = {$styles.content.imgtext.linkWrap.lightboxCssClass}
						width = {$styles.content.imgtext.linkWrap.width}
						height = {$styles.content.imgtext.linkWrap.height}
						rel = lightbox[myImageSet]
					}
				}

				video {
					width = 360
					height = 300
				}
			}

			loadList = 0
		}

		# --------------
		#  List
		# --------------
		list {
			# media configuration
			media {
				image {
					maxWidth = 100
					maxHeight = 100
				}

				dummyImage = typo3conf/ext/moox_news/Resources/Public/Images/dummy-preview-image.png
			}

			# Paginate configuration.
			paginate {
				itemsPerPage = 10
				insertAbove = 1
				insertBelow = 1
				templatePath =
				prevNextHeaderTags = 1
				maximumNumberOfLinks = 3
				addQueryStringMethod = POST,GET
				useCacheHash = false
			}

			autoLoadFirstItemDetailView = 0

			rss {
				channel {
					title = {$plugin.tx_mooxnews.rss.channel.title}
					description = {$plugin.tx_mooxnews.rss.channel.description}
					language = {$plugin.tx_mooxnews.rss.channel.language}
					copyright = {$plugin.tx_mooxnews.rss.channel.copyright}
					generator = {$plugin.tx_mooxnews.rss.channel.generator}
					link = {$plugin.tx_mooxnews.rss.channel.link}
				}
			}
		}

		# Opengraph implementation
		opengraph {
			site_name = {$plugin.tx_mooxnews.opengraph.site_name}
			type = article
			admins =
			email =
			phone_number =
			fax_number =
			latitude =
			longitude =
			street-address =
			locality =
			region =
			postal-code =
			country-name =

			twitter {
				card = {$plugin.tx_mooxnews.opengraph.twitter.card}
				site = {$plugin.tx_mooxnews.opengraph.twitter.site}
				creator = {$plugin.tx_mooxnews.opengraph.twitter.creator}
			}
		}
	}
}

# EXT:fluid_styled_content uses different constants
[{$styles.content.textmedia.maxW} > 0]
	plugin.tx_mooxnews.settings.detail.media.image.lightbox {
		enabled = {$styles.content.textmedia.linkWrap.lightboxEnabled}
		class = {$styles.content.textmedia.linkWrap.lightboxCssClass}
		width = {$styles.content.textmedia.linkWrap.width}
		height = {$styles.content.textmedia.linkWrap.height}
	}
[END]


# Rendering of content elements in detail view
lib.tx_mooxnews.contentElementRendering = RECORDS
lib.tx_mooxnews.contentElementRendering {
	tables = tt_content
	source.current = 1
	dontCheckPid = 1
}

# Rendering of news, displayed by "Insert Record" content element
tt_content.shortcut.20.tables := addToList(tx_mooxnews_domain_model_news)
tt_content.shortcut.20.conf.tx_mooxnews_domain_model_news = USER
tt_content.shortcut.20.conf.tx_mooxnews_domain_model_news {
	userFunc = TYPO3\CMS\Extbase\Core\Bootstrap->run
	extensionName = MooxNews
	pluginName = Pi1
	vendorName = Neulandlotsen
	switchableControllerActions {
		News {
			1 = detail
		}
	}

	settings =< plugin.tx_mooxnews.settings
	settings {
		singleNews.field = uid
		useStdWrap = singleNews
		insertRecord = 1
		isShortcut = 1
	}
}
# For fluid_styled_content
tt_content.shortcut.variables.shortcuts.tables := addToList(tx_mooxnews_domain_model_news)
tt_content.shortcut.variables.shortcuts.conf.tx_mooxnews_domain_model_news < tt_content.shortcut.20.conf.tx_mooxnews_domain_model_news

# ==============================================
# BE-module configuration for EXT:moox_news
# ==============================================
module.tx_mooxnews < plugin.tx_mooxnews
module.tx_mooxnews {
	persistence {
		storagePid = {$module.tx_mooxnews.persistence.storagePid}
	}
	settings {	
		list.paginate.itemsPerPage = 10	
		previewPid {
			default = {$module.tx_mooxnews.settings.previewPid.default}
		}
		defaultSortField = {$module.tx_mooxnews.settings.defaultSortField}
		defaultSortDirection = {$module.tx_mooxnews.settings.defaultSortDirection}
		itemsPerPage = {$module.tx_mooxnews.settings.itemsPerPage}
		backendListViewFields {
			tx_mooxnews_domain_model_news {
				moox_news = {$module.tx_mooxnews.settings.backendListViewFields.tx_mooxnews_domain_model_news.moox_news}
			}
		}
		resetFieldsOnDuplicate {
			tx_mooxnews_domain_model_news {
				moox_news = {$module.tx_mooxnews.settings.resetFieldsOnDuplicate.tx_mooxnews_domain_model_news.moox_news}
			}
		}
		listViewFieldSeparator = {$module.tx_mooxnews.settings.listViewFieldSeparator}
		moduleStartPid = {$module.tx_mooxnews.settings.moduleStartPid}
		shortcutUrl = {$module.tx_mooxnews.settings.shortcutUrl}
		icsNamespace = {$module.tx_mooxnews.settings.icsNamespace}
		icsDomain = {$module.tx_mooxnews.settings.icsDomain}
		icsLanguage = {$module.tx_mooxnews.settings.icsLanguage}
	}
}

# ==============================================
# Persistence object mapping configuration
# ==============================================
config.tx_extbase.persistence.classes {
	Neulandlotsen\MooxNews\Domain\Model\News {
		subclasses {
			0 = Neulandlotsen\MooxNews\Domain\Model\NewsDefault
			1 = Neulandlotsen\MooxNews\Domain\Model\NewsExternal
			2 = Neulandlotsen\MooxNews\Domain\Model\NewsInternal
		}
	}

	Neulandlotsen\MooxNews\Domain\Model\NewsDefault {
		mapping {
			recordType = 0
			tableName = tx_mooxnews_domain_model_news
		}
	}

	Neulandlotsen\MooxNews\Domain\Model\NewsExternal {
		mapping {
			recordType = 1
			tableName = tx_mooxnews_domain_model_news
		}
	}

	Neulandlotsen\MooxNews\Domain\Model\NewsInternal {
		mapping {
			recordType = 2
			tableName = tx_mooxnews_domain_model_news
		}
	}
}

[{$plugin.tx_mooxnews.settings.useAjax} == 1]
plugin.tx_mooxnews {
	view {
		templateRootPaths {
			0 = EXT:moox_news/Resources/Private/Ajax/Templates/
			1 = {$plugin.tx_mooxnews.view.ajaxTemplateRootPath}
		}
		partialRootPaths {
			0 = EXT:moox_news/Resources/Private/Ajax/Partials/
			1 = {$plugin.tx_mooxnews.view.ajaxPartialRootPath}
		}
		layoutRootPaths {
			0 = EXT:moox_news/Resources/Private/Ajax/Layouts/
			1 = {$plugin.tx_mooxnews.view.ajaxLayoutRootPath}
		}
		widget.Neulandlotsen\MooxNews\ViewHelpers\Widget\PaginateViewHelper.templateRootPath = EXT:moox_news/Resources/Private/Ajax/Templates/
	}
	settings.list.paginate.maximumNumberOfLinks = 1000
}
# Nur für ein bestimmtes Plugin
plugin.tx_mooxnews.view.widget.Neulandlotsen\MooxNews\ViewHelpers\Widget\PaginatorViewHelper.templateRootPath= {$plugin.tx_mooxnews.view.ajaxTemplateRootPath}

[ELSE]
plugin.tx_mooxnews {
	view {
		templateRootPaths {
			0 = EXT:moox_news/Resources/Private/Templates/
			1 = {$plugin.tx_mooxnews.view.templateRootPath}
		}

		partialRootPaths {
			0 = EXT:moox_news/Resources/Private/Partials/
			1 = {$plugin.tx_mooxnews.view.partialRootPath}
		}

		layoutRootPaths {
			0 = EXT:moox_news/Resources/Private/Layouts/
			1 = {$plugin.tx_mooxnews.view.layoutRootPath}
		}
		widget.Neulandlotsen\MooxNews\ViewHelpers\Widget\PaginateViewHelper.templateRootPath = EXT:moox_news/Resources/Private/Templates/
	}
}
[END]

[request.getQueryParams()['tx_mooxnews_pi1']['news']>0]
config.defaultGetVars {
  tx_mooxnews_pi1.action = detail
}
[END]


# EXT: seo
###################################################
plugin.tx_seo {
	config {
		xmlSitemap {
			sitemaps {
				moox_news {
					provider = TYPO3\CMS\Seo\XmlSitemap\RecordsXmlSitemapDataProvider
					config {
						table = tx_mooxnews_domain_model_news
						additionalWhere = {$plugin.tx_mooxnews.xmlSitemap.additionalWhere}
						sortField = {$plugin.tx_mooxnews.xmlSitemap.sortField}
						lastModifiedField = {$plugin.tx_mooxnews.xmlSitemap.lastModifiedField}
						pid = {$plugin.tx_mooxnews.xmlSitemap.pid}
						url {
							pageId = {$plugin.tx_mooxnews.xmlSitemap.pageId}
							fieldToParameterMap {
								uid = tx_mooxnews_pi1[news]
							}
							additionalGetParameters {
								tx_mooxnews_pi1.controller = News
								tx_mooxnews_pi1.action = detail
							}
							useCacheHash = 1
						}
					}
				}
			}
		}
	}
}
