<?php
// Set language source file
$ll = 'LLL:EXT:moox_mailer/Resources/Private/Language/locallang_db.xlf:';

return [
	'ctrl' => [
		'title'	=> $ll.'tx_mooxmailer_domain_model_template',
		'label' => 'title',
		'tstamp' => 'tstamp',
		'crdate' => 'crdate',
		'cruser_id' => 'cruser_id',
		'sortby' => 'sorting',
		'versioningWS' => 2,
		'versioning_followPages' => TRUE,
		'origUid' => 't3_origuid',
		'languageField' => 'sys_language_uid',
		'transOrigPointerField' => 'l10n_parent',
		'transOrigDiffSourceField' => 'l10n_diffsource',
		'delete' => 'deleted',
		'rootLevel' => -1,
		'enablecolumns' => [
			'disabled' => 'hidden',
			'starttime' => 'starttime',
			'endtime' => 'endtime',
		],		
		'searchFields' => 'title,subject,template',		
		'iconfile' => 'EXT:moox_mailer/Resources/Public/Icons/mooxmailer_domain_model_template.svg',
		'hideTable' => TRUE
	],
	'interface' => [
		'showRecordFieldList' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, title, category, subject, sender_address, sender_name, reply_to_address, template, text',
	],
	'types' => [
		'1' => ['showitem' => 'l10n_parent, l10n_diffsource, title, category, subject, sender_address, sender_name, reply_to_address, template, text,--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:access,hidden, starttime, endtime'],
	],
	'palettes' => [
		'1' => ['showitem' => ''],
	],
	'columns' => [
		'sys_language_uid' => [
			'exclude' => 1,
			'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.language',
			'config' => [
				'type' => 'select',
				'foreign_table' => 'sys_language',
				'foreign_table_where' => 'ORDER BY sys_language.title',
				'items' => [
					['LLL:EXT:lang/locallang_general.xlf:LGL.allLanguages', -1],
					['LLL:EXT:lang/locallang_general.xlf:LGL.default_value', 0]
				],
			],
		],
		'l10n_parent' => [
			'displayCond' => 'FIELD:sys_language_uid:>:0',
			'exclude' => 1,
			'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.l18n_parent',
			'config' =>[
				'type' => 'select',
				'items' => [
					['', 0],
				],
				'foreign_table' => 'tx_mooxmailer_domain_model_template',
				'foreign_table_where' => 'AND tx_mooxmailer_domain_model_template.pid=###CURRENT_PID### AND tx_mooxmailer_domain_model_template.sys_language_uid IN (-1,0)',
			],
		],
		'l10n_diffsource' => [
			'config' => [
				'type' => 'passthrough',
			],
		],
		't3ver_label' => [
			'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.versionLabel',
			'config' => [
				'type' => 'input',
				'size' => 30,
				'max' => 255,
			]
		],
		'hidden' => [
			'exclude' => 1,
			'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.enabled',
			'config' => [
				'type' => 'check',
			],
		],
		'starttime' => [
			'exclude' => 1,
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.starttime',
			'config' => [
				'type' => 'input',
				'size' => 13,
				'max' => 20,
				'eval' => 'datetime',
				'checkbox' => 0,
				'default' => 0,
				'range' => [
					'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
				],
			],
		],
		'endtime' => [
			'exclude' => 1,
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.endtime',
			'config' => [
				'type' => 'input',
				'size' => 13,
				'max' => 20,
				'eval' => 'datetime',
				'checkbox' => 0,
				'default' => 0,
				'range' => [
					'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
				],
			],
		],
		'title' => [
			'exclude' => 0,
			'label' => $ll.'tx_mooxmailer_domain_model_template.title',
			'config' => [
				'type' => 'input',
				'size' => 40,
				'eval' => 'required,trim,uniqueInPid'
			],
		],
		'subject' => [
			'exclude' => 0,
			'label' => $ll.'tx_mooxmailer_domain_model_template.subject',
			'config' => [
				'type' => 'input',
				'size' => 40,
				'eval' => 'required,trim'
			],
		],
		/*
		'category' => [
			'exclude' => 0,
			'label' => $ll.'tx_mooxmailer_domain_model_template.category',
			'config' => [
				'type' => 'select',
				'size' => 1,
				'minitems' => 1,
				'maxitems' => 1,
				'itemsProcFunc' => 'Neulandlotsen\MooxMailer\Hooks\FlexFormHelper->templateCategory'
			],
		],*/
		'sender_address' => array(
			'exclude' => 0,
			'label' => $ll.'tx_mooxmailer_domain_model_template.sender_address',
			'config' => array(
				'type' => 'input',
				'size' => 40,
				'eval' => 'trim,email'
			),
		),
		'sender_name' => array(
			'exclude' => 0,
			'label' => $ll.'tx_mooxmailer_domain_model_template.sender_name',
			'config' => array(
				'type' => 'input',
				'size' => 40,
				'eval' => 'trim'
			),
		),
		'reply_to_address' => array(
			'exclude' => 0,
			'label' => $ll.'tx_mooxmailer_domain_model_template.reply_to_address',
			'config' => array(
				'type' => 'input',
				'size' => 40,
				'eval' => 'trim,email'
			),
		),
		'template' => [
			'exclude' => 0,
			'label' => $ll.'tx_mooxmailer_domain_model_template.template',
			'config' => [
				'type' => 'text',
				'cols' => 40,
				'rows' => 20,
				'renderType' => 'mooxCoreTemplate',
				'eval' => 'trim',
				'format' => 'html',
				'preview' => [
					'M' => 'MooxMailerMooxmailer_MooxMailerTemplatemanagement',
					'action' => 'previewIframe',
					'controller' => 'Mod9',
					'extension' => 'MooxMailer',
					'format' => 'html',
					'helperClass' => 'Neulandlotsen\MooxMailer\Service\HelperService',
				],
			],
		],
		'text' => [
			'exclude' => 0,
			'label' => $ll.'tx_mooxmailer_domain_model_template.text',
			'config' => [
				'type' => 'text',
				'cols' => 40,
				'rows' => 20,
				'renderType' => 'mooxCoreTemplate',
				'eval' => 'trim',
				'format' => 'html',
				'preview' => [
					'M' => 'MooxMailerMooxmailer_MooxMailerTemplatemanagement',
					'action' => 'previewIframe',
					'controller' => 'Mod9',
					'extension' => 'MooxMailer',
					'format' => 'text',
					'helperClass' => 'Neulandlotsen\MooxMailer\Service\HelperService',
				],
			],
		],
	],
];
?>