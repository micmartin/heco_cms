<?php
if (!defined ('TYPO3_MODE')) die ('Access denied.');

// extend moox_news model classes
//$GLOBALS['TYPO3_CONF_VARS']['EXT']['moox_news']['classes']['Domain/Model/News'][] = 'moox_news_grabber_tt_news_heco';

$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['scheduler']['tasks']['Neulandlotsen\\MooxNewsGrabberTtNewsHeco\\Tasks\\GrabberTask'] = [
	'extension' => 'moox_news_grabber_tt_news_heco',
	'title' => 'LLL:EXT:moox_news_grabber_tt_news_heco/Resources/Private/Language/locallang_scheduler.xlf:tx_mooxnewsgrabberttnewsheco_tasks_grabbertask.title',
	'description' => 'LLL:EXT:moox_news_grabber_tt_news_heco/Resources/Private/Language/locallang_scheduler.xlf:tx_mooxnewsgrabberttnewsheco_tasks_grabbertask.description',
	'additionalFields' => 'Neulandlotsen\\MooxNewsGrabberTtNewsHeco\\Tasks\\GrabberTaskAdditionalFieldProvider'
];
?>