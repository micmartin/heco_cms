<?php
namespace Neulandlotsen\MooxNews\Domain\Model;

    /**
     * This file is part of the TYPO3 CMS project.
     *
     * It is free software; you can redistribute it and/or modify it under
     * the terms of the GNU General Public License, either version 2
     * of the License, or any later version.
     *
     * For the full copyright and license information, please read the
     * LICENSE.txt file that was distributed with this source code.
     *
     * The TYPO3 project - inspiring people to share!
     */

/**
 * Target model
 *
 */
class Target extends \TYPO3\CMS\Extbase\DomainObject\AbstractValueObject
{
	/**
	 * @var int
	 */
    protected $uid;
	
	/**
	 * @var int
	 */
    protected $pid;
	
	/**
	 * @var bool
	 */
    protected $hidden;	
	
    /**
     * @var \DateTime
     */
    protected $crdate;

    /**
     * @var \DateTime
     */
    protected $tstamp;

    /**
     * @var string
     */
    protected $title;
	
	/**
     * @return int $uid uid
     */
    public function getUid()
	{
       return $this->uid;
    }
     
    /**
     * @param int $uid uid
	 */
    public function setUid($uid)
	{
        $this->uid = $uid;
    }
	
	/**
     * @return int $pid pid
     */
    public function getPid()
	{
       return $this->pid;
    }
     
    /**
     * @param int $pid pid
	 */
    public function setPid($pid)
	{
        $this->pid = $pid;
    }
	
	/**
     * @return bool $hidden hidden
     */
    public function getHidden()
	{
       return $this->hidden;
    }
     
    /**
     * @param bool $hidden hidden
	 */
    public function setHidden($hidden)
	{
        $this->hidden = $hidden;
    }
	
    /**
     * Get crdate
     *
     * @return \DateTime
     */
    public function getCrdate()
    {
        return $this->crdate;
    }

    /**
     * Set crdate
     *
     * @param \DateTime $crdate crdate
     * @return void
     */
    public function setCrdate($crdate)
    {
        $this->crdate = $crdate;
    }

    /**
     * Get Tstamp
     *
     * @return \DateTime
     */
    public function getTstamp()
    {
        return $this->tstamp;
    }

    /**
     * Set tstamp
     *
     * @param \DateTime $tstamp tstamp
     * @return void
     */
    public function setTstamp($tstamp)
    {
        $this->tstamp = $tstamp;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set title
     *
     * @param string $title title
     * @return void
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }
}