<?php
namespace Neulandlotsen\MooxMailer\Controller;

/**
 * This file is part of the "moox_mailer" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

use Neulandlotsen\MooxCore\Utility\HelperUtility as CoreHelperUtility;
use Neulandlotsen\MooxCore\Utility\AccessUtility;
use Neulandlotsen\MooxMailer\Domain\Repository\FrontendUserRepository;
use Neulandlotsen\MooxMailer\Service\HelperService;
use Neulandlotsen\MooxMailer\Service\MailerService;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Database\Query\Restriction\DeletedRestriction;
use TYPO3\CMS\Core\Database\Query\Restriction\HiddenRestriction;
use TYPO3\CMS\Extbase\Object\ObjectManager;

class RedirectorController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{
	/**
	 * @var ObjectManager
	 */
	protected $objectManager;

	/**
	 * @var FrontendUserRepository
	 */
	protected $frontendUserRepository;

	/**
	 * @var \Neulandlotsen\MooxNews\Domain\Repository\NewsRepository
	 */
	protected $newsRepository;

	/**
	 * @var HelperService
	 */
	protected $helperService;

	/**
	 * @var \TYPO3\CMS\Core\Database\DatabaseConnection
	 */
	protected $database;

	/**
	 * @var string
	 */
	public static $indexUrl = "index.php?id=";

    /**
     * @param ObjectManager $objectManager
     */
    public function injectObjectManager(ObjectManager $objectManager)
    {
        $this->objectManager = $objectManager;
    }

    /**
     * @param FrontendUserRepository $frontendUserRepository
     */
    public function injectFrontendUserRepository(FrontendUserRepository $frontendUserRepository)
    {
        $this->frontendUserRepository = $frontendUserRepository;
    }

    /**
     * @param HelperService $helperService
     */
    public function injectHelperService(HelperService $helperService)
    {
        $this->helperService = $helperService;
    }

	/**
	 * Constructor
	 */
	public function __construct()
	{
		$this->database = $GLOBALS['TYPO3_DB'];
	}

	/**
	 * initialize the controller
	 */
	protected function initializeAction()
	{
		parent::initializeAction();
		if(ExtensionManagementUtility::isLoaded('moox_news'))
		{
			$this->newsRepository = $this->objectManager->get('Neulandlotsen\\MooxNews\\Domain\\Repository\\NewsRepository');
		}
	}

	/**
	 * action redirect
	 *
	 * @param string $mode
	 * @param int $id
	 * @param int $itemUid
	 * @param array $autoLogin
	 * @param string $hash
	 * @return void
	 */
	public function redirectAction($mode = "page", $id = 0, $itemUid = 0, $autoLogin = NULL, $hash = NULL)
	{
		$baseUrl = MailerService::getBaseUrl($id);

		$loginRedirectorPid = MailerService::getLoginRedirectorPid($id);

		// TODO: Mapping auslagern
		if($itemUid==10846)$itemUid=10857;

		$cObj = GeneralUtility::makeInstance('TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer');



		if($loginRedirectorPid>0)
		{
			$loginurl = $cObj->typoLink_URL(['returnLast' => 'url', 'forceAbsoluteUrl' => 1,'parameter' => $loginRedirectorPid, 'additionalParams' => '&redirect_url=redirecturi', 'useCacheHash' => 0]);
		}

		if(true)
		{
			if(is_array($autoLogin) && $autoLogin['id']>0 && $autoLogin['hash']!="" && strlen($autoLogin['hash'])==32 && $this->checkFrontendUserHash($autoLogin['id'],$autoLogin['hash']))
			{
				$user = $this->frontendUserRepository->findByUid(intval($autoLogin['id']));
			}

			if($mode=="news")
			{
				$news = $this->newsRepository->findByUid($itemUid,FALSE);

				if(is_a($news, 'Neulandlotsen\MooxNews\Domain\Model\News'))
				{

					$uri = $this->uriBuilder->setTargetPageUid($id)->setLinkAccessRestrictedPages(true)->setCreateAbsoluteUri(true)->uriFor('detail', array("news" => $itemUid), 'News', 'MooxNews', 'Pi1');

					if(!($news->getHidden() || $news->getStarttime()>time() || ($news->getEndtime()>0 && $news->getEndtime()>time())))
					{
						if($news->getFeGroup()!=0)
						{
							if(!is_array($GLOBALS["TSFE"]->fe_user->user) && is_a($user, 'Neulandlotsen\MooxMailer\Domain\Model\FrontendUser'))
							{
								$this->loginUserById($user->getUid());

								if($GLOBALS['TSFE']->fe_user->user['usergroup']!=0)
								{
									$hasAccess = false;
									$usergroups = explode(",",$GLOBALS['TSFE']->fe_user->user['usergroup']);
									$newsusergroups = explode(",",$news->getFeGroup());
									foreach($newsusergroups AS $newsusergroup)
									{
										if(in_array($newsusergroup,$usergroups))
										{
											$hasAccess 	= true;
											break;
										}
									}
									if($hasAccess)
									{
										$this->redirectToURI($uri);
									}
									else
									{
										$this->redirectToURI($baseUrl);
									}
								}
								else
								{
									$this->redirectToURI($baseUrl);
								}
							}
							elseif(is_array($GLOBALS["TSFE"]->fe_user->user))
							{
								if($GLOBALS['TSFE']->fe_user->user['usergroup']!=0)
								{
									$hasAccess = false;
									$usergroups = explode(",",$GLOBALS['TSFE']->fe_user->user['usergroup']);
									$newsusergroups = explode(",",$news->getFeGroup());
									foreach($newsusergroups AS $newsusergroup)
									{
										if(in_array($newsusergroup,$usergroups))
										{
											$hasAccess 	= true;
											break;
										}
									}
									if($hasAccess)
									{
										$this->redirectToURI($uri);
									}
									else
									{
										$this->redirectToURI($baseUrl);
									}
								}
								else
								{
									$this->redirectToURI($baseUrl);
								}
							}
							else
							{
								if($loginurl!="")
								{
									$this->redirectToURI(str_replace("redirecturi",urlencode($uri),$loginurl));
								}
								else
								{
									$this->redirectToURI($baseUrl);
								}
							}
						}
						else
						{
							$this->redirectToURI($uri);
						}
					}
					else
					{
						$this->redirectToURI($baseUrl);
					}
				}
				else
				{
					$this->redirectToURI($baseUrl);
				}
			}
			elseif($mode=="direct_news" )
			{
				if($hash && strlen($hash)>32)
				{
					$data = json_decode(CoreHelperUtility::decryptText($hash),true);

					if(is_array($data) && $data['uid']>0 && $data['pid']>0 && $data['sec']!="" && $data['valid']>time())
					{
						$news = $this->newsRepository->findByUid($data['uid'],FALSE);

						if(is_a($news, 'Neulandlotsen\MooxNews\Domain\Model\News'))
						{
							if(!($news->getHidden() || $news->getStarttime()>time() || ($news->getEndtime()>0 && $news->getEndtime()>time())))
							{
								$directNewsData = [];
								$directNewsData['uid'] = $news->getUid();
								$directNewsData['sec'] = $news->getHash();
								$directNewsData['valid'] = time()+(86400*7);

								$directNewsHash = CoreHelperUtility::encryptText(json_encode($directNewsData));
								$directNewsHashEncoded = urlencode($directNewsHash);

								$uri = $this->uriBuilder->setNoCache(true)->setTargetPageUid($data['pid'])->setLinkAccessRestrictedPages(true)->uriFor('detail', array('news'=>$data['uid'],'dnews'=>$directNewsHash), 'News', 'MooxNews', 'Pi1');

								exit("direct news");
							}
						}
					}
				}
			}
			else
			{
				$uri = $this->uriBuilder->reset()->setTargetPageUid($id)->setCreateAbsoluteUri(true)->setNoCache(TRUE)->build();
				if($this->request->hasArgument('key'))
				{
					if($this->request->getArgument('key') == "f907a83d1cce65747f92162859ce016b")
					{
						if(strpos($uri,"?") !== false)
						{
							// Workaround because of appending typeNum
							if(strpos($uri,"type=9001") !== false)
							{
								$uri =  str_replace("type=9001","key=".$this->request->getArgument('key'),$uri);
							}
							else
							{
								$uri =  $uri."&key=".$this->request->getArgument('key');
							}
						}
						else
						{
							$uri =  $uri."?key=".$this->request->getArgument('key');
						}
					}
				}

				if(!is_array($GLOBALS["TSFE"]->fe_user->user) && is_a($user, 'Neulandlotsen\MooxMailer\Domain\Model\FrontendUser'))
				{
					$this->loginUserById($user->getUid());
				}
				if(substr($uri,0,4)=="http")
				{
					$this->redirectToURI($uri);
				}
				else
				{
					$this->redirectToURI($uri);
				}
			}
		}
		echo "can't redirect to page";
		exit();
	}

	/**
	 * This function checks, if the given password is correct for the given feuser
	 * @param int $uid				uid of feuser to login
	 * @param string $hash		hash-string must be correct md5(username.password)
	 */
	public function checkFrontendUserHash($uid,$hash)
	{
		$user = $this->frontendUserRepository->findByUid(intval($uid));

		if(is_a($user, 'Neulandlotsen\MooxMailer\Domain\Model\FrontendUser'))
		{
			if($user->getAutoHash() == $hash)
			{
				return true;
			}
		}
		return false;
	}

	/**
	 * Login user by id
	 *
	 * @param int $userid
	 * @return void
	 */
	public function loginUserById($userid)
	{
		$userdata = $this->fetchFrontendUserData($userid);

		if(is_array($userdata))
		{
			AccessUtility::initFrontendUser($userdata);
		}
	}

	/**
	 * Fetch user data from fe_user table
	 *
	 * @param int $uid
	 * @return array
	 */
	protected function fetchFrontendUserData($uid = 0)
	{
		if($uid>0)
		{
			$queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)
				->getQueryBuilderForTable('fe_users');

			$queryBuilder
				->getRestrictions()
				->removeAll()
				->add(GeneralUtility::makeInstance(DeletedRestriction::class))
				->add(GeneralUtility::makeInstance(HiddenRestriction::class));

			$res = $queryBuilder
				->select('*')
				->from('fe_users')
				->where(
					$queryBuilder->expr()->eq('uid', $queryBuilder->createNamedParameter($uid, \PDO::PARAM_INT))
				)
				->execute();

			if($userdata = $res->fetch())
			{
				return $userdata;
			}
		}

	}
}
?>