<?php
return [
	'ctrl' => [
		'title'	=> 'LLL:EXT:moox_news_seminar/Resources/Private/Language/locallang_db.xlf:tx_mooxnewsseminar_domain_model_speaker',
		'label' => 'title',
		'default_sortby' => 'ORDER BY title ASC',
		'tstamp' => 'tstamp',
		'crdate' => 'crdate',
		'cruser_id' => 'cruser_id',
		//'sortby' => 'sorting',
		'delete' => 'deleted',
		'enablecolumns' => [
			'disabled' => 'hidden'
		],
		'searchFields' => 'title',
		'iconfile' => 'EXT:moox_news_seminar/Resources/Public/Icons/mooxnewsseminar_domain_model_speaker.svg',
	],
	'interface' => [
		'showRecordFieldList' => 'hidden,title,position,description,image',
	],
	'types' => [
		'1' => ['showitem' => 'title, position, description, image, hidden'],
	],
	'columns' => [
		'pid' => [
            'label' => 'pid',
            'config' => [
                'type' => 'passthrough'
            ]
        ],
		'hidden' => [
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.hidden',
			'config' => [
				'type' => 'check',
			],
		],
		'title' => [
			'exclude' => 0,
			'label' => 'LLL:EXT:moox_news_seminar/Resources/Private/Language/locallang_db.xlf:tx_mooxnewsseminar_domain_model_speaker.title',
			'config' => [
				'type' => 'input',
				'size' => 50,
				'eval' => 'trim,required'
			],
			// special moox configuration
			'moox' => [
				'extkey' => 'moox_news_seminar',
				'backend' => [
					'type' => 'text',
				],
			],
		],
		'position' => [
			'exclude' => 1,
			'label' => 'LLL:EXT:moox_news_seminar/Resources/Private/Language/locallang_db.xlf:tx_mooxnewsseminar_domain_model_speaker.position',
			'config' => [
				'type' => 'text',
				'cols' => 40,
				'rows' => 5,
				'eval' => 'trim'
			],
			// special moox configuration
			'moox' => [
				'extkey' => 'moox_news_seminar',
				'backend' => [
					'type' => 'text',
				],
			],
		],
		'description' => [
			'exclude' => 1,
			'label' => 'LLL:EXT:moox_news_seminar/Resources/Private/Language/locallang_db.xlf:tx_mooxnewsseminar_domain_model_speaker.description',
			'config' => [
				'type' => 'text',
				'enableRichtext' => true,
				'eval' => 'trim'
			],
			// special moox configuration
			'moox' => [
				'extkey' => 'moox_news_seminar',
				'backend' => [
					'type' => 'html',
				],
			],
		],
		'image' => [
			'exclude' => 1,
			'label' => 'LLL:EXT:moox_news_seminar/Resources/Private/Language/locallang_db.xlf:tx_mooxnewsseminar_domain_model_speaker.image',
			'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig(
				'image',
				[
					'maxitems' => 1,
					'appearance' => [
						'fileUploadAllowed' => 0,
						'useSortable' => 1,
						'collapseAll' => 1,
						'expandSingle' => 0,
						'enabledControls' => [
							'info' => FALSE,
							'new' => FALSE,
							'dragdrop' => TRUE,
							'sort' => FALSE,
							'hide' => TRUE,
							'delete' => TRUE,
							'localize' => FALSE,
						],
						'createNewRelationLinkTitle' => 'LLL:EXT:moox_news_seminar/Resources/Private/Language/locallang_db.xlf:tx_mooxnewsseminar_domain_model_speaker.image.addbutton'
					],
					'overrideChildTca' => [
						'types' => [
							\TYPO3\CMS\Core\Resource\File::FILETYPE_IMAGE => [
								'showitem' => '
									--palette--;LLL:EXT:lang/Resources/Private/Language/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
									--palette--;;filePalette'
							]
						]
					]
				],
				$GLOBALS['TYPO3_CONF_VARS']['GFX']['imagefile_ext']
			),
			// special moox configuration
			'moox' => [
				'extkey' => 'moox_news_seminar',
				'backend' => [
					'type' => 'images',
				],				
			],
		],
	],
];
?>