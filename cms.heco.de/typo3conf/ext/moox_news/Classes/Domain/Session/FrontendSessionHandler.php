<?php
namespace Neulandlotsen\MooxNews\Domain\Session;

/**
 * This file is part of the "moox_news" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

class FrontendSessionHandler extends \Neulandlotsen\MooxNews\Domain\Session\SessionHandler 
{	
	/**
     * @var string
     */
    protected $mode = "FE";
}
?>