<?php
if (!defined('TYPO3_MODE')) {
	die('Access denied.');
}

// set additional import fields foe moox_feusers
$GLOBALS['TCA']['fe_users']['additionalImportFields']['moox_mailer'] = "disallow_mailing,quality,password";

// Set language source file
$ll = 'LLL:EXT:moox_mailer/Resources/Private/Language/locallang_db.xlf:';

$newFeUserColumns = [
	'gender' => [	
		'exclude' => 1,		
		'label'	=> $ll.'tx_mooxmailer_domain_model_frontend_user.gender',		
		'config' => [
			'type' => 'select',
			'items' => [
					[$ll.'tx_mooxmailer_domain_model_frontend_user.gender.none', 0],
					[$ll.'tx_mooxmailer_domain_model_frontend_user.gender.male', 1],
					[$ll.'tx_mooxmailer_domain_model_frontend_user.gender.female', 2],					
			],			
			'size' => 1,	
			'maxitems' => 1,
		],
		// special moox configuration		
		'moox' => [
			'extkey' => 'moox_community',
			'plugins' => [
				"mooxcommunity" => [
					"add","edit","editUser","detailUser"
				],
			],
			'sortable' => 0,	
		],
	],
	'disallow_mailing' => [
		'exclude' => 1,
		'label' => $ll.'tx_mooxmailer_domain_model_frontend_user.disallow_mailing',
		'config' => [
			'type' => 'check'
		]
	],
	'quality' => [
		'exclude' => 1,
		'label' => $ll.'tx_mooxmailer_domain_model_frontend_user.quality',
		'config' => [
			'type' => 'select',
			'items' => [
					[$ll.'tx_mooxmailer_domain_model_frontend_user.quality.0', 0],
					[$ll.'tx_mooxmailer_domain_model_frontend_user.quality.1', 1],
					[$ll.'tx_mooxmailer_domain_model_frontend_user.quality.2', 2],					
			],			
			'size' => 1,	
			'maxitems' => 1,
		]
	],
	'bounces' => [
		'exclude' => 0,
		'label' => $ll.'tx_mooxmailer_domain_model_bounce',
		'config' => [
			'type' => 'select',
			'foreign_table' => 'tx_mooxmailer_domain_model_bounce',
			'foreign_field' => 'fe_user',
			'size' => 10,
			'autoMaxSize' => 10,
			'maxitems'      => 999999,
			'multiple' => 0,
			'MM' => 'tx_mooxmailer_bounce_feuser_mm',
			'MM_opposite_field' => 'fe_user'
		],
	],
	'errors' => [
		'exclude' => 0,
		'label' => $ll.'tx_mooxmailer_domain_model_error',
		'config' => [
			'type' => 'select',
			'foreign_table' => 'tx_mooxmailer_domain_model_error',
			'foreign_field' => 'fe_user',
			'size' => 10,
			'autoMaxSize' => 10,
			'maxitems'      => 999999,
			'multiple' => 0,
			'MM' => 'tx_mooxmailer_error_feuser_mm',
			'MM_opposite_field' => 'fe_user'
		],
	],
	'mailer_tags' => [		
		'exclude' => 1,		
		'label' => $ll.'tx_mooxmailer_domain_model_frontend_user.mailer_tags',		
		'config' => [
			'type' => 'select',
			'type' => 'select',
			'renderType' => 'selectMultipleSideBySide',
			'enableMultiSelectFilterTextfield' => true,
			'allowNonIdValues' => 1,				
			'size' => 6,
			'minitems' => 0,
			'maxitems' => 99,
			'multiple' => 1,
			'itemsProcFunc' => 'Neulandlotsen\MooxMailer\Hooks\TcaFormHelper->tags',			
		],		
	],
];

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('fe_users',$newFeUserColumns,1);
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes('fe_users', 'gender', '','before:title');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes('fe_users', '--div--;LLL:EXT:moox_mailer/Resources/Private/Language/locallang_db.xlf:tx_mooxmailer_domain_model_news.tabs.mailer,disallow_mailing,mailer_tags','','after:tx_extbase_type');
$TCA['fe_users']['columns']['usergroup']['config']['minitems'] = '0';