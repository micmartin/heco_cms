<?php
if (!defined('TYPO3_MODE'))
{
	die ('Access denied.');
}

$GLOBALS['TYPO3_CONF_VARS']['EXT']['moox_core']['scss']['folders'][$_EXTKEY] = 'Bootstrap';

if (\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::isLoaded('flux'))
{
	// register extension as page provider
	//\FluidTYPO3\Flux\Core::registerProviderExtensionKey("Neulandlotsen.".$_EXTKEY, 'Page');

	// register extension as content provider
	\FluidTYPO3\Flux\Core::registerProviderExtensionKey("Neulandlotsen.MooxBootstrap4", 'Content');
}

/***************
 * Add default RTE configuration for bootstrap package
 */
$GLOBALS['TYPO3_CONF_VARS']['RTE']['Presets']['bootstrap4'] = 'EXT:'.$_EXTKEY.'/Configuration/CKEditor/Bootstrap4.yaml';
$GLOBALS['TYPO3_CONF_VARS']['RTE']['Presets']['bootstrap4_minimal'] = 'EXT:'.$_EXTKEY.'/Configuration/CKEditor/Bootstrap4Minimal.yaml';
$GLOBALS['TYPO3_CONF_VARS']['RTE']['Presets']['bootstrap4_full'] = 'EXT:'.$_EXTKEY.'/Configuration/CKEditor/Bootstrap4Full.yaml';

/***************
 * Use moox_core PAGE TSconfig
 */
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig('<INCLUDE_TYPOSCRIPT: source="FILE:EXT:'.$_EXTKEY.'/Configuration/PageTS/pageTSconfig.typoscript">');

?>