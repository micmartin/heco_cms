<?php
namespace Neulandlotsen\MooxMailer\Tasks;

/**
 * This file is part of the "moox_mailer" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

use Neulandlotsen\MooxMailer\Service\CronjobService;
use TYPO3\CMS\Core\Core\Environment;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Messaging\FlashMessage;
use TYPO3\CMS\Core\Messaging\FlashMessageService;
 
class MooxNewsSendTask extends \TYPO3\CMS\Scheduler\Task\AbstractTask 
{		
	
	/**
	 * PIDs der Seite/Ordner in dem die News gespeichert sind die versendet werden sollen
	 *
	 * @var string
	 */
	public $newsPids;
	
	/**
	 * PIDs der Seite/Ordner in dem die User gespeichert sind an die versendet werden soll
	 *
	 * @var string
	 */
	public $userPids;
	
	/**
	 * PIDs der Seite/Ordner in dem die Adressen gespeichert sind an die versendet werden soll
	 *
	 * @var string
	 */
	public $addressesPids;

	/**
	 * Ausgeschlossene Benutzer-Gruppen
	 *
	 * @var string
	 */
	public $addressesCategories;

	/**
	 * Ausgeschlossene Benutzer-Gruppen
	 *
	 * @var string
	 */
	public $excludedUserGroups;	
	
	/**
	 * News Kategorien die versendet werden sollen
	 *
	 * @var string
	 */
	public $includedNewsCategoriesUids;
	
	/**
	 * News Kategorien die vom Versand ausgeschlossen werden sollen
	 *
	 * @var string
	 */
	public $excludedNewsCategoriesUids;
	
	/**
	 * @var boolean
	 */
	public $sendSingleMails;
	
	/**
	 * @var string
	 */
	public $subject;
	
	/**
	 * @var bool
	 */
	public $forceSubject;
	
	/**
	 * template uid
	 *
	 * @var int
	 */
	public $templateUid;
	
	/**
	 * @var bool
	 */
	public $forceTemplateInSingleMailing;
	
	/**
	 * @var int
	 */
	public $scheduleUid;
	
	/**
	 * @var bool
	 */
	public $forceScheduledTemplateInSingleMailing;
	
	/**
	 * @var string
	 */
	public $format;
	
	/**
	 * @var string
	 */
	public $senderAddress;
	
	/**
	 * @var string
	 */
	public $senderName;
	
	/**
	 * @var string
	 */
	public $replyToAddress;
	
	/**
	 * @var string
	 */
	public $cc;
	
	/**
	 * @var string
	 */
	public $bcc;
	
	/**
	 * @var string
	 */
	public $rightsMapping;
	
	/**
	 * Simulierte Benutzergruppen für Adressen und FE-User ohne Gruppen
	 *
	 * @var string
	 */
	public $simulatedUserGroups;
	
	/**
	 * @var bool
	 */
	public $sendRestrictedNewsOnly;
	
	/**
	 * @var bool
	 */
	public $sendToUsersWithGroupsOnly;
	
	/**
	 * @var string
	 */
	public $mode;
	
	/**
	 * @var string
	 */
	public $sendMode;
	
	/**
	 * @var bool
	 */
	public $debug;
	
	/**
	 * @var string
	 */
	public $debugAddress;
	
	/**
	 * @var string
	 */
	public $debugMailLimit;
	
	/**
	 * @var string
	 */
	public $mailerDir;	
	
	/**
	 * @var bool
	 */
	public $isPreview;	
	
	/**
	 * @var bool
	 */
	public $respectTags;
	
	/**
	 * @var bool
	 */
	public $respectZipregions;

	/**
	 * @var bool
	 */
	public $noLog;		
	
	/**
	 * @var string
	 */
	public $countMode;
	
	/**
	 * @var int
	 */
	public $memoryLimit;
	
	/**
	 * @var int
	 */
	public $scriptTimeout;
	
	/**
	 * @var int
	 */
	public $timestamp;
	
	/**
	 * @var string
	 */
	public $job;
	
	/**
	 * @var string
	 */
	public $logfile;
	
	/**
	 * @var int
	 */
	public $detailPid;

	/**
	 * @var int
	 */
	public $loginPid;

	/**
	 * @var bool
	 */
	public $attachRelated;

	/**
	 * @var bool
	 */
	public $attachSelf;

	/**
	 * @var bool
	 */
	public $directNews;

	/**
	 * @var int
	 */
	public $newsLimit;

	/**
	 * @var int
	 */
	public $autoLoginUser;
	
	// ToDo: check if still needed - 2019-08-01
	/**
	 * @var string
	 */
	//public $redirectorShortLink;
	
	/**
	 * is cli call
	 *
	 * @var bool
	 */
	public $isCli;
	
	/**
	 * @var \TYPO3\CMS\Core\Messaging\FlashMessageService
	 */
	public $flashMessageService;
	
	/**
	 * Prepares the mailer job.
	 *
	 * @var bool $directCall
	 * @return	bool	Returns TRUE on success, FALSE if errors occured.
	 * @see	typo3/sysext/scheduler/tx_scheduler_Task#execute()
	 */
	public function execute($directCall = FALSE)
	{
		if ( basename( Environment::getCurrentScript() ) == 'typo3' )
		{
			$isCli = true;
		}
		else
		{
			$isCli = false;
		}

		$objectManager = GeneralUtility::makeInstance('TYPO3\CMS\Extbase\Object\ObjectManager');
		if(!$isCli)
		{
			$flashMessageService = $objectManager->get(FlashMessageService::class);
			$flashMessageQueue = $flashMessageService->getMessageQueueByIdentifier();
		}

		// Set global settings
		$this->setMailerDir(Environment::getPublicPath().'/maildir');
		//$this->setIsPreview(false);		
		$this->setCountMode('live');
		$this->setScriptTimeout(90);
		$this->setMemoryLimit(1024);
		$this->setTimestamp(time());
		$this->setNoLog(false);
		
		// check if maildir ist present
		if(is_dir($this->getMailerDir()))
		{			
			/*
			$execution 	= $this->getExecution();
			$interval 	= $execution->getInterval();
			*/

			// initiate daemon
			$daemon = $objectManager->get('Neulandlotsen\MooxMailer\Service\DaemonService');
			$daemon->initialize($this);						
			
			// initiate mailer job
			$mailer = $objectManager->get('Neulandlotsen\MooxMailer\Service\MooxNewsMailerService');
			$mailer->initialize($this);
			
			$this->setLogfile($mailer->getIdentifier()."_".$mailer->getDescription().".log");

			// if mailer ready, set mailer lock
			if(!$this->getIsPreview() && $mailer->getMode()=="daemon" && $daemon->mailerReady())
			{
				//$daemon->lockMailer("[".date("Y-m-d H:i:s")."] locked by ".$mailer->getDescription());
				$lock = true;				
			}
			elseif($mailer->getMode()=="direct")
			{
				$lock = true;
			}
			
			if($lock || $this->getIsPreview())
			{
				// prepare mailer job
				if(!$this->getIsPreview())
				{
					$process = $mailer->process();
				} 
				else 
				{
					$previewdata = $mailer->preview();					
				}
				
				// do only if not in preview mode
				if(!$this->getIsPreview())
				{															
					// no news found
					if($mailer->getMailCount()<1 && !($mailer->getMode()!="daemon" && $process['processedCnt']>0))
					{							
						// log to console
						if($process['newsLimit']>0)
						{
							$text = "[".date("Y-m-d H:i:s")."] TASK [".(($this->description)?$this->description:$this->taskUid)."] says: not enough news found, only ".$process['newsCount']." of ".$process['newsLimit']."...\n";
							$severity = FlashMessage::WARNING;
						}
						else
						{
							$text = "[".date("Y-m-d H:i:s")."] TASK [".(($this->description)?$this->description:$this->taskUid)."] says: nothing to do...\n";
						}

					} 
					// mailer in daemon mode
					elseif($mailer->getMode()=="daemon")
					{						
						// log to console
						$text = "[".date("Y-m-d H:i:s")."] TASK [".(($this->description)?$this->description:$this->taskUid)."] job ".$mailer->getIdentifier()."_".$mailer->getDescription()." prepared (".$mailer->getMailCount()." mail(s) prepared)...\n";					
					} 
					// mailer in direct mode
					else 
					{
						// log to console
						$text = "[".date("Y-m-d H:i:s")."] TASK [".(($this->description)?$this->description:$this->taskUid)."] job ".$mailer->getIdentifier()."_".$mailer->getDescription()." processed (".($process['successCnt'])." mail(s) sent, ".$process['errorCnt']." error(s))...\n";
					}

				
					if(!$directCall && !$isCli)
					{
						$message = GeneralUtility::makeInstance('TYPO3\\CMS\\Core\\Messaging\\FlashMessage',
							$text,
							 '', // the header is optional
							($severity)?$severity:FlashMessage::OK, // the severity is optional as well and defaults to FlashMessage::OK
							 TRUE // optional, whether the message should be stored in the session or only in the FlashMessageQueue object (default is FALSE)
						);
						$flashMessageQueue->addMessage($message);
					}
									
					// remove mailer lock
					$daemon->removeLock();					
				}
				
				$executionSucceeded = TRUE;	
				
			} 
			else 
			{				
				$lockInfo = $daemon->lockInfo();
				$lockInfoParts = explode("]",$lockInfo);
				$lockInfoText = trim($lockInfoParts[1]);
				$lockInfoDate = trim($lockInfoParts[0])."]";
				
				// log to console
				$text = "[".date("Y-m-d H:i:s")."] TASK [".(($this->description)?$this->description:$this->taskUid)."] says: mailer ".$lockInfoText." ".$lockInfoDate."...\n";

				if(!$directCall && !$isCli)
				{	
					$message = GeneralUtility::makeInstance('TYPO3\\CMS\\Core\\Messaging\\FlashMessage',
						$text,
						 '', // the header is optional
						 FlashMessage::ERROR,
						 TRUE
					);
					$flashMessageQueue->addMessage($message);
				}

				if($directCall)
				{
					$process['locked'] = true;
					$process['date'] = $lockInfoDate;
					$process['info'] = $lockInfoText;
				}

				$executionSucceeded = FALSE;	
			}						
		} 
		else 
		{
			if(!$directCall && !$isCli)
			{
				$message = GeneralUtility::makeInstance('TYPO3\\CMS\\Core\\Messaging\\FlashMessage',
					"[".date("Y-m-d H:i:s")."] TASK: ".(($this->description)?$this->description:$this->taskUid)." says: Folder \"maildir\" is missing. Please use \"Mailer-Admin\" to generate mailer folders.",
					 '', // the header is optional
					 FlashMessage::ERROR, // the severity is optional as well and defaults to FlashMessage::OK
					 TRUE // optional, whether the message should be stored in the session or only in the FlashMessageQueue object (default is FALSE)
				);
				$flashMessageQueue->addMessage($message);
			};

			$executionSucceeded = FALSE;		
		}
		
		if(is_array($previewdata))
		{
			return $previewdata;
		} 
		else 
		{
			if($directCall)
			{
				return $process;
			}
			else
			{
				return $executionSucceeded;
			}
		}
	}
	
	/**
	 * This method returns the sleep duration as additional information
	 *
	 * @return string Information to display
	 */
	public function getAdditionalInformation()
	{
		//$info = $this->getDescription();
		
		/*
		$cronCmd = $this->getExecution()->getCronCmd();
		if($cronCmd!=""){
			$cron = \Neulandlotsen\MooxMailer\Service\CronjobService::factory($cronCmd);
			$cron->isDue();
			//echo $cron->getNextRunDate()->format('Y-m-d H:i:s');
			$info = "next c: ".$cron->getPreviousRunDate()->format('Y-m-d H:i:s');
		} else {
			$info = "next e: ".date("Y-m-d H:i:s",$this->getExecution()->getStart()+$this->getExecution()->getInterval());
		}

		return $info;
		*/
	}
	
	/**
	 * @return string
	 */
	public function getNewsPids()
	{
		return $this->newsPids;
	}

	/**
	 * @param string $newsPids
	 */
	public function setNewsPids($newsPids)
	{
		$this->newsPids = $newsPids;
	}
	
	/**
	 * @return string
	 */
	public function getUserPids()
	{
		return $this->userPids;
	}

	/**
	 * @param string $userPids
	 * @return void
	 */
	public function setUserPids($userPids)
	{
		$this->userPids = $userPids;
	}
	
	/**
	 * @return string
	 */
	public function getAddressesPids()
	{
		return $this->addressesPids;
	}

	/**
	 * @param string $addressesPids
	 */
	public function setAddressesPids($addressesPids)
	{
		$this->addressesPids = $addressesPids;
	}
	
	/**
	 * @return string
	 */
	public function getExcludedUserGroups()
	{
		return $this->excludedUserGroups;
	}

	/**
	 * @param string $excludedUserGroups
	 */
	public function setExcludedUserGroups($excludedUserGroups)
	{
		$this->excludedUserGroups = $excludedUserGroups;
	}

	/**
	 * @return string
	 */
	public function getAddressesCategories()
	{
		return $this->addressesCategories;
	}

	/**
	 * @param string $addressesCategories
	 */
	public function setAddressesCategories($addressesCategories)
	{
		$this->addressesCategories = $addressesCategories;
	}
	
	/**
	 * @return string
	 */
	public function getIncludedNewsCategoriesUids()
	{
		return $this->includedNewsCategoriesUids;
	}

	/**
	 * @param string $includedNewsCategoriesUids
	 */
	public function setIncludedNewsCategoriesUids($includedNewsCategoriesUids)
	{
		$this->includedNewsCategoriesUids = $includedNewsCategoriesUids;
	}
	
	/**
	 * @return string
	 */
	public function getExcludedNewsCategoriesUids()
	{
		return $this->excludedNewsCategoriesUids;
	}

	/**
	 * @param string $excludedNewsCategoriesUids
	 */
	public function setExcludedNewsCategoriesUids($excludedNewsCategoriesUids)
	{
		$this->excludedNewsCategoriesUids = $excludedNewsCategoriesUids;
	}
	
	/**
	 * @return bool
	 */
	public function getSendSingleMails()
	{
		return $this->sendSingleMails;
	}

	/**
	 * @param bool $sendSingleMails
	 */
	public function setSendSingleMails($sendSingleMails)
	{
		$this->sendSingleMails = $sendSingleMails;
	}
	
	/**
	 * @return string
	 */
	public function getSubject()
	{
		return $this->subject;
	}

	/**
	 * @param string $subject
	 */
	public function setSubject($subject)
	{
		$this->subject = $subject;
	}
	
	/**
	 * @return bool
	 */
	public function getForceSubject()
	{
		return $this->forceSubject;
	}

	/**
	 * @param bool $forceSubject
	 */
	public function setForceSubject($forceSubject)
	{
		$this->forceSubject = $forceSubject;
	}
	
	/**
	 * @return int
	 */
	public function getTemplateUid()
	{
		return $this->templateUid;
	}

	/**
	 * @param int $templateUid
	 */
	public function setTemplateUid($templateUid)
	{
		$this->templateUid = $templateUid;
	}
	
	/**
	 * @return bool
	 */
	public function getForceTemplateInSingleMailing()
	{
		return $this->forceTemplateInSingleMailing;
	}

	/**
	 * @param bool $forceTemplateInSingleMailing
	 */
	public function setForceTemplateInSingleMailing($forceTemplateInSingleMailing)
	{
		$this->forceTemplateInSingleMailing = $forceTemplateInSingleMailing;
	}
	
	/**
	 * @return int
	 */
	public function getScheduleUid()
	{
		return $this->scheduleUid;
	}

	/**
	 * @param int $scheduleUid
	 */
	public function setScheduleUid($scheduleUid)
	{
		$this->scheduleUid = $scheduleUid;
	}
	
	/**
	 * @return bool
	 */
	public function getForceScheduledTemplateInSingleMailing()
	{
		return $this->forceScheduledTemplateInSingleMailing;
	}

	/**
	 * @param bool $forceScheduledTemplateInSingleMailing
	 */
	public function setForceScheduledTemplateInSingleMailing($forceScheduledTemplateInSingleMailing)
	{
		$this->forceScheduledTemplateInSingleMailing = $forceScheduledTemplateInSingleMailing;
	}
	
	/**
	 * @return string
	 */
	public function getFormat()
	{
		return $this->format;
	}

	/**
	 * @param string $format
	 */
	public function setFormat($format)
	{
		$this->format = $format;
	}
	
	/**
	 * @return string
	 */
	public function getSenderAddress()
	{
		return $this->senderAddress;
	}

	/**
	 * @param string $senderAddress
	 */
	public function setSenderAddress($senderAddress)
	{
		$this->senderAddress = $senderAddress;
	}
	
	/**
	 * @return string
	 */
	public function getSenderName()
	{
		return $this->senderName;
	}

	/**
	 * @param string $senderAddress
	 */
	public function setSenderName($senderName)
	{
		$this->senderName = $senderName;
	}
	
	/**
	 * @return string
	 */
	public function getReplyToAddress()
	{
		return $this->replyToAddress;
	}

	/**
	 * @param string $replyToAddress
	 */
	public function setReplyToAddress($replyToAddress)
	{
		$this->replyToAddress = $replyToAddress;
	}
	
	/**
	 * @return string
	 */
	public function getCc()
	{
		return $this->cc;
	}

	/**
	 * @param string $cc
	 */
	public function setCc($cc)
	{
		$this->cc = $cc;
	}
	
	/**
	 * @return string
	 */
	public function getBcc()
	{
		return $this->bcc;
	}

	/**
	 * @param string $bcc
	 */
	public function setBcc($bcc)
	{
		$this->bcc = $bcc;
	}
	
	/**
	 * @return string
	 */
	public function getRightsMapping()
	{
		return $this->rightsMapping;
	}

	/**
	 * @param string $rightsMapping
	 */
	public function setRightsMapping($rightsMapping)
	{
		$this->rightsMapping = $rightsMapping;
	}
	
	/**
	 * @return string
	 */
	public function getSimulatedUserGroups()
	{
		return $this->simulatedUserGroups;
	}

	/**
	 * @param string $simulatedUserGroups
	 */
	public function setSimulatedUserGroups($simulatedUserGroups)
	{
		$this->simulatedUserGroups = $simulatedUserGroups;
	}
	
	/**
	 * @return bool
	 */
	public function getSendRestrictedNewsOnly()
	{
		return $this->sendRestrictedNewsOnly;
	}

	/**
	 * @param bool $sendRestrictedNewsOnly
	 */
	public function setSendRestrictedNewsOnly($sendRestrictedNewsOnly)
	{
		$this->sendRestrictedNewsOnly = $sendRestrictedNewsOnly;
	}
	
	/**
	 * @return bool
	 */
	public function getSendToUsersWithGroupsOnly()
	{
		return $this->sendToUsersWithGroupsOnly;
	}

	/**
	 * @param bool $sendToUsersWithGroupsOnly
	 */
	public function setSendToUsersWithGroupsOnly($sendToUsersWithGroupsOnly)
	{
		$this->sendToUsersWithGroupsOnly = $sendToUsersWithGroupsOnly;
	}
	
	/**
	 * @return string
	 */
	public function getMode()
	{
		return $this->mode;
	}

	/**
	 * @param string $mode mode
	 */
	public function setMode($mode)
	{
		$this->mode = $mode;
	}
	
	/**
	 * @return string
	 */
	public function getSendMode()
	{
		return $this->sendMode;
	}

	/**
	 * @param string $sendMode
	 */
	public function setSendMode($sendMode)
	{
		$this->sendMode = $sendMode;
	}
	
	/**
	 * @return bool
	 */
	public function getDebug()
	{
		return $this->debug;
	}

	/**
	 * @param bool $debug debug
	 */
	public function setDebug($debug)
	{
		$this->debug = $debug;
	}
	
	/**
	 * @return string
	 */
	public function getDebugAddress()
	{
		return $this->debugAddress;
	}

	/**
	 * @param string $debugAddress
	 */
	public function setDebugAddress($debugAddress)
	{
		$this->debugAddress = $debugAddress;
	}
	
	/**
	 * @return string
	 */
	public function getDebugMailLimit()
	{
		return $this->debugMailLimit;
	}

	/**
	 * @param string $debugMailLimit
	 */
	public function setDebugMailLimit($debugMailLimit)
	{
		$this->debugMailLimit = $debugMailLimit;
	}
	
	/**
	 * @return string
	 */
	public function getMailerDir() 
	{
		return $this->mailerDir;
	}

	/**
	 * @param string $mailerDir
	 */
	public function setMailerDir($mailerDir)
	{
		$this->mailerDir = $mailerDir;
	}
	
	/**
	 * @return bool
	 */
	public function getIsPreview()
	{
		return $this->isPreview;
	}

	/**
	 * @param string $isPreview
	 */
	public function setIsPreview($isPreview)
	{
		$this->isPreview = $isPreview;
	}	
	
	/**
	 * @return bool
	 */
	public function getNoLog()
	{
		return $this->noLog;
	}

	/**
	 * @param string $noLog no log
	 */
	public function setNoLog($noLog)
	{
		$this->noLog = $noLog;
	}	
	
	/**
	 * @return string
	 */
	public function getCountMode()
	{
		return $this->countMode;
	}

	/**
	 * @param string $countMode
	 */
	public function setCountMode($countMode)
	{
		$this->countMode = $countMode;
	}
	
	/**
	 * @return int
	 */
	public function getMemoryLimit()
	{
		return $this->memoryLimit;
	}

	/**
	 * @param int $memoryLimit
	 */
	public function setMemoryLimit($memoryLimit)
	{
		$this->memoryLimit = $memoryLimit;
	}
	
	/**
	 * @return int
	 */
	public function getScriptTimeout()
	{
		return $this->scriptTimeout;
	}

	/**
	 * @param int $scriptTimeout
	 */
	public function setScriptTimeout($scriptTimeout)
	{
		$this->scriptTimeout = $scriptTimeout;
	}
	
	/**
	 * @return int
	 */
	public function getTimestamp()
	{
		return $this->timestamp;
	}

	/**
	 * @param int $timestamp
	 */
	public function setTimestamp($timestamp)
	{
		$this->timestamp = $timestamp;
	}
	
	/**
	 * @return string
	 */
	public function getJob()
	{
		return $this->job;
	}

	/**
	 * @param string $job
	 */
	public function setJob($job)
	{
		$this->job = $job;
	}
	
	/**
	 * @return string
	 */
	public function getLogfile()
	{
		return $this->logfile;
	}

	/**
	 * @param string $logfile
	 */
	public function setLogfile($logfile)
	{
		$this->logfile = $logfile;
	}
	
	/**
	 * @return int
	 */
	public function getDetailPid()
	{
		return $this->detailPid;
	}

	/**
	 * @param int $detailPid
	 */
	public function setDetailPid($detailPid)
	{
		$this->detailPid = $detailPid;
	}
	
	/**
	 * @return int
	 */
	public function getAutoLoginUser()
	{
		return $this->autoLoginUser;
	}

	/**
	 * @param int $autoLoginUser
	 */
	public function setAutoLoginUser($autoLoginUser)
	{
		$this->autoLoginUser = $autoLoginUser;
	}

	/**
	 * @return string
	 */
	/*
	public function getRedirectorShortLink()
	{
		return $this->redirectorShortLink;
	}
	*/

	/**
	 * @param string $redirectorShortLink
	 */
	/*
	public function setRedirectorShortLink($redirectorShortLink)
	{
		$this->redirectorShortLink = $redirectorShortLink;
	}
	*/
	
	/**
	 * @return bool
	 */
	public function getRespectTags()
	{
		return $this->respectTags;
	}

	/**
	 * @param string $respectTags
	 */
	public function setRespectTags($respectTags)
	{
		$this->respectTags = $respectTags;
	}
	
	/**
	 * @return bool
	 */
	public function getRespectZipregions()
	{
		return $this->respectZipregions;
	}

	/**
	 * @param string $respectZipregions
	 */
	public function setRespectZipregions($respectZipregions)
	{
		$this->respectZipregions = $respectZipregions;
	}

	/**
	 * @return bool
	 */
	public function getAttachRelated()
	{
		return $this->attachRelated;
	}

	/**
	 * @param string $attachRelated
	 */
	public function setAttachRelated($attachRelated)
	{
		$this->attachRelated = $attachRelated;
	}

	/**
	 * @return bool
	 */
	public function getAttachSelf()
	{
		return $this->attachSelf;
	}

	/**
	 * @param string $attachSelf
	 */
	public function setAttachSelf($attachSelf)
	{
		$this->attachSelf = $attachSelf;
	}

	/**
	 * @return bool
	 */
	public function getDirectNews()
	{
		return $this->directNews;
	}

	/**
	 * @param string $attachSelf
	 */
	public function setDirectNews($directNews)
	{
		$this->directNews = $directNews;
	}

	/**
 * @return int
 */
	public function getLoginPid()
	{
		return $this->loginPid;
	}

	/**
	 * @param int $loginPid
	 */
	public function setLoginPid($loginPid)
	{
		$this->loginPid = $loginPid;
	}

	/**
	 * @return int
	 */
	public function getNewsLimit()
	{
		return $this->newsLimit;
	}

	/**
	 * @param int $newsLimit
	 */
	public function setNewsLimit($newsLimit)
	{
		$this->newsLimit = $newsLimit;
	}
}
?>