define(['jquery'], function ($) {

    var MooxModule = {};

    MooxModule.initClearButtons = function () {

        $('.moox-module .has-clear input, .has-clear textarea').on('keyup', function () {
            if ($(this).val() == '') {
                $(this).parents('.form-group').addClass('has-empty-value');
            } else {
                $(this).parents('.form-group').removeClass('has-empty-value');
            }
        }).trigger('change');

        $('.moox-module .has-clear .form-control-clear').on('click', function () {
            var input = $(this).parents('.form-group').find('input,textarea');
            input.val('').trigger('change');
            input.val('').trigger('keyup');

            // Trigger a "cleared" event on the input for extensibility purpose
            input.trigger('cleared');

            if ($(this).hasClass("form-control-reload")) {
                input.parents('form').first().submit();
            }
        });

        $('.moox-module .has-clear input, .has-clear textarea').each(function () {
            if ($(this).val() == '') {
                $(this).parents('.form-group').addClass('has-empty-value');
            } else {
                $(this).parents('.form-group').removeClass('has-empty-value');
            }
        });
    }

    return MooxModule;

});