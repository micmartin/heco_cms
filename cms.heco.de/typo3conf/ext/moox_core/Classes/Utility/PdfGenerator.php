<?php
namespace Neulandlotsen\MooxCore\Utility;

/**
 * This file is part of the "moox_core" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

use Neulandlotsen\MooxCore\Utility\HelperUtility as CoreHelperUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Utility\File\BasicFileUtility;
use Dompdf\Dompdf;
use Dompdf\Options;

/**
 * Class PdfGenerator
 */
class PdfGenerator implements \TYPO3\CMS\Core\SingletonInterface
{
	/**
	 * @var string
	 */
	public static $tempFolder = "/typo3temp/var/dompdf";
	
	/**
     * @param string $html html code to genarte pdf file
	 * @param boolean $forceDownload
	 * @param string $filename
	 * @param boolean $stream
	 * @param string $cache
	 * @return array $return
     */
    public function generateFromHtml($html = "", $forceDownload = false, $filename = "", $stream = true, $cache = '')
    {
		$dompdf = NULL;
    	if($html)
		{
			require_once(\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('moox_core') . 'DomPdf/autoload.inc.php');

			if(!is_dir($_SERVER['DOCUMENT_ROOT'].self::$tempFolder)){
				mkdir($_SERVER['DOCUMENT_ROOT'].self::$tempFolder);
			}

			$options = new Options();
			$options->set('tempDir',$_SERVER['DOCUMENT_ROOT'].self::$tempFolder);
			$options->set('isRemoteEnabled',true);

			// instantiate and use the dompdf class
			$dompdf = new Dompdf($options);

			//exit($html);
			$dompdf->loadHtml($html);

			// Render the HTML as PDF
			$dompdf->render();

			// Output the generated PDF to Browser
			$basicFileUtility 	= GeneralUtility::makeInstance(BasicFileUtility::class);

			if($filename=="")
			{
				$filename = $basicFileUtility->cleanFileName(CoreHelperUtility::getTitleFromHtml($html));
				if($filename=="")
				{
					$filename = "PDF";
				}
			}
			else
			{
				$filename = $basicFileUtility->cleanFileName($filename);
			}

			if($cache)
			{
				$output = $dompdf->output();
				$pdf = $this->writeCache($cache, $output, $filename);
			}
			if($stream)
			{
				$dompdf->stream($filename,array('Attachment'=>$forceDownload));
			}
			else
			{
				return [
					"data" => ($output)?$output:$dompdf->output(),
					"filename" => $filename
				];
			}
		}
	}

	/**
	 * @param string $identifier
	 * @return array $return
	 */
	public function isCached($identifier = "")
	{
		if($identifier)
		{
			$filepath = $_SERVER['DOCUMENT_ROOT'].self::$tempFolder."/".$identifier.".pdf";

			if(file_exists($filepath))
			{
				return [
					"time" => filemtime($filepath),
					"filename" => $filepath
				];
			}
		}
		return false;
	}

	/**
	 * @param string $identifier
	 * @param boolean $sendToBrowser
	 * @param string $filename
	 * @param boolean $forceDownload
	 * @return array $return
	 */
	public function getCached($identifier = "", $sendToBrowser = TRUE, $filename = "PDF", $forceDownload = TRUE)
	{
		if($identifier)
		{
			$filepath = $_SERVER['DOCUMENT_ROOT'].self::$tempFolder."/".$identifier.".pdf";

			if(file_exists($filepath))
			{
				$basicFileUtility = GeneralUtility::makeInstance(BasicFileUtility::class);
				$filename = $basicFileUtility->cleanFileName($filename);
				if($sendToBrowser)
				{
					header("Content-type:application/pdf");
					header('Content-Length: '.filesize($filepath));
					header("Content-Disposition:".(($forceDownload)?'attachment':'inline').";filename=".$filename.".pdf");
					header('Cache-Control: public, must-revalidate, max-age=0');
					header('Pragma: public');
					header('Expires: Sat, 26 Jul 1997 05:00:00 GMT');
					header('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
					readfile($filepath);
					exit();
				}
				return [
					'filepath' => $filepath,
					'filename' => $filename
				];
			}
		}
		return false;
	}

	/**
	 * @param string $identifier
	 * @param string $data
	 * @param string $filename
	 * @return array $filepath
	 */
	public function writeCache($identifier = "", $data = "", $filename = "PDF")
	{
		if($identifier && $data)
		{
			$filepath = $_SERVER['DOCUMENT_ROOT'].self::$tempFolder."/".$identifier.".pdf";

			if(file_exists($filepath))
			{
				unlink($filepath);
			}
			file_put_contents($filepath, $data);

			$basicFileUtility = GeneralUtility::makeInstance(BasicFileUtility::class);

			return [
				'filepath' => $filepath,
				'filename' => $basicFileUtility->cleanFileName($filename)
			];
		}
	}
}
