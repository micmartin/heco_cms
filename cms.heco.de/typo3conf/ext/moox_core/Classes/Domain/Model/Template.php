<?php
namespace Neulandlotsen\MooxCore\Domain\Model;

/**
 * This file is part of the "moox_core" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

use TYPO3\CMS\Extbase\DomainObject\AbstractEntity;
 
class Template extends AbstractEntity
{	
	/**
	 * @var int
	 */
    protected $uid;
	
	/**
	 * @var int
	 */
    protected $pid;
	
	/**
	 * @var int
	 */
    protected $tstamp;
	
	/**
	 * @var bool
	 */
    protected $hidden;
	
	/**
	 * @var int
	 */
    protected $starttime;
	
	/**
	 * @var int
	 */
    protected $endtime;		
	
	/**
	 * @var int
	 */
    protected $crdate;
	
	/**
	 * @var string
	 * @TYPO3\CMS\Extbase\Annotation\Validate("NotEmpty")
	 */
	protected $title;
	
	/**
	 * @var string
	 * @TYPO3\CMS\Extbase\Annotation\Validate("NotEmpty")
	 */
	protected $subject;

	/**
	 * @var string
	 */
	protected $senderAddress;

	/**
	 * @var string
	 */
	protected $senderName;

	/**
	 * @var string
	 */
	protected $replyToAddress;
	
	/**
	 * @var string
	 */
	protected $category;
	
	/**
	 * @var string
	 */
	protected $template;
	
	/**
	 * @var string
	 */
	protected $text;
	
	/**
	 * @var string
	 */
    protected $autoName;
	
	/**
     * @return int $uid uid
     */
    public function getUid()
	{
       return $this->uid;
    }
     
    /**
     * @param int $uid uid
	 */
    public function setUid($uid)
	{
        $this->uid = $uid;
    }
	
	/**
     * @return int $pid pid
     */
    public function getPid()
	{
       return $this->pid;
    }
     
    /**
     * @param int $pid pid
	 */
    public function setPid($pid)
	{
        $this->pid = $pid;
    }
	
	/**
     * @return int $tstamp tstamp
     */
    public function getTstamp()
	{
       return $this->tstamp;
    }
     
    /**
     * @param int $tstamp tstamp
	 */
    public function setTstamp($tstamp)
	{
        $this->tstamp = $tstamp;
    }		
	
	/**
     * @return bool $hidden hidden
     */
    public function getHidden()
	{
       return $this->hidden;
    }
     
    /**
     * @param bool $hidden hidden
	 */
    public function setHidden($hidden)
	{
        $this->hidden = $hidden;
    }		
	
	/**
     * @return int $starttime starttime
     */
    public function getStarttime()
	{
       return $this->starttime;
    }
     
    /**
     * @param int $starttime starttime
	 */
    public function setStarttime($starttime)
	{
        $this->starttime = $starttime;
    }
	
	/**
     * @return int $endtime endtime
     */
    public function getEndtime()
	{
       return $this->endtime;
    }
     
    /**
     * @param int $endtime endtime
	 */
    public function setEndtime($endtime)
	{
        $this->endtime = $endtime;
    }
	
	/**
     * @return int $crdate crdate
     */
    public function getCrdate()
	{
       return $this->crdate;
    }
     
    /**
     * @param int $crdate crdate
	 */
    public function setCrdate($crdate)
	{
        $this->crdate = $crdate;
    }
	
	/**
	 * @return int
	 */
	public function getYearOfCrdate()
	{
		return $this->getCrdate()->format('Y');
	}

	/**
	 * @return int
	 */
	public function getMonthOfCrdate()
	{
		return $this->getCrdate()->format('m');
	}

	/**
	 * @return int
	 */
	public function getDayOfCrdate()
	{
		return (int)$this->crdate->format('d');
	}
	
	/**
	 * Returns the title
	 *
	 * @return string $title
	 */
	public function getTitle()
	{
		return $this->title;
	}

	/**
	 * @param string $title
	 */
	public function setTitle($title)
	{
		$this->title = $title;
	}
	
	/**
	 * @return string $subject
	 */
	public function getSubject()
	{
		return $this->subject;
	}

	/**
	 * @param string $subject
	 */
	public function setSubject($subject)
	{
		$this->subject = $subject;
	}
	
	/**
	 * @return string $category
	 */
	public function getCategory()
	{
		return $this->category;
	}

	/**
	 * @param string $category
	 */
	public function setCategory($category)
	{
		$this->category = $category;
	}

	/**
	 * @return string $senderAddress
	 */
	public function getSenderAddress()
	{
		return $this->senderAddress;
	}

	/**
	 * @param string $senderAddress
	 */
	public function setSenderAddress($senderAddress)
	{
		$this->senderAddress = $senderAddress;
	}

	/**
	 * @return string $senderName
	 */
	public function getSenderName()
	{
		return $this->senderName;
	}

	/**
	 * @param string $senderName
	 */
	public function setSenderName($senderName)
	{
		$this->senderName = $senderName;
	}

	/**
	 * @return string $replyToAddress
	 */
	public function getReplyToAddress()
	{
		return $this->replyToAddress;
	}

	/**
	 * @param string $replyToAddress
	 */
	public function setReplyToAddress($replyToAddress)
	{
		$this->replyToAddress = $replyToAddress;
	}

	/**
	 * @return string $template
	 */
	public function getTemplate()
	{
		return $this->template;
	}

	/**
	 * @param string $template
	 */
	public function setTemplate($template)
	{
		$this->template = $template;
	}
	
	/**
	 * @return string $text
	 */
	public function getText()
	{
		return $this->text;
	}
	
	/**
	 * @param string $text
	 */
	public function setText($text)
	{
		$this->text = $text;
	}
	
	/**
	 * @return string $autoName
	 */
	public function getAutoName()
	{
	   $autoName = $this->title;
	   return $autoName;
	}
}
?>