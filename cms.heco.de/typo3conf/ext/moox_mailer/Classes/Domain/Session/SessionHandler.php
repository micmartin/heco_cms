<?php
namespace Neulandlotsen\MooxMailer\Domain\Session;

/**
 * This file is part of the "moox_mailer" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

use TYPO3\CMS\Core\SingletonInterface;

class SessionHandler extends \Neulandlotsen\MooxCore\Domain\Session\SessionHandler
{
    /**
     * The key the data is stored in the session.
     * @var string
     */
    protected $storageKey = 'tx_mooxmailer';
}
?>