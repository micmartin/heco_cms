<?php
namespace Neulandlotsen\MooxNewsSeminar\Service;

/**
 * This file is part of the "moox_news" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */
	 
class GrabberService extends \Neulandlotsen\MooxNews\Service\GrabberService
{
	/**
	 * @param array $news
	 * @return string $value
	 */
	public function prepareMooxNewsSeminarSearchIndex($news = [])
	{
		$value = $news['moox_news_seminar_search_index'];

		if($news['moox_news_seminar_startdate']>0) {
			$index = [];
			$index[] = date("d.m.Y", $news['moox_news_seminar_startdate']);
			$index[] = date("j.n.Y", $news['moox_news_seminar_startdate']);
			$index[] = date("d.m.y", $news['moox_news_seminar_startdate']);
			$index[] = date("j.n.y", $news['moox_news_seminar_startdate']);
			$index[] = date("Y-m-d", $news['moox_news_seminar_startdate']);
			$index[] = date("y-m-d", $news['moox_news_seminar_startdate']);
			$index[] = date("Y-n-j", $news['moox_news_seminar_startdate']);
			$index[] = date("y-n-j", $news['moox_news_seminar_startdate']);
			$value = trim($value." ".implode(" ", $index));
		}

		return $value;
	}
}
?>