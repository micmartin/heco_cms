<?php
namespace Neulandlotsen\MooxCore\Domain\Session;

/**
 * This file is part of the "moox_core" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

use TYPO3\CMS\Core\SingletonInterface;

class SessionHandler implements SingletonInterface
{
	/**
	 * Keeps TYPO3 mode.
	 * Either 'FE' or 'BE'.
	 *
	 * @var string
	 */
	protected $mode = NULL;

	/**
	 * The User-Object with the session-methods.
	 * Either $GLOBALS['BE_USER'] or $GLOBALS['TSFE']->fe_user.
	 *
	 * @var object
	 */
	protected $sessionObject = NULL;

	/**
	 * The key the data is stored in the session.
	 * @var string
	 */
	protected $storageKey = 'tx_mooxcore';

	/**
	 * Class constructor.
	 * @param string $mode
	 */
	public function __construct($mode = NULL)
	{
		if ($mode)
		{
			$this->mode = $mode;
		}

		if ($this->mode === NULL || ($this->mode != "BE" && $this->mode != "FE"))
		{
			throw new \Exception( "Typo3-Mode is not defined!", 1388660107 );
		}
		$this->sessionObject = ($this->mode == "BE") ? $GLOBALS['BE_USER'] : $GLOBALS['TSFE']->fe_user;
	}

	/**
	 * @param string $storageKey
	 */
	public function setStorageKey($storageKey)
	{
		$this->storageKey = $storageKey;
	}

	/**
	 * @param string $key
	 * @param mixed $value
	 * @return void
	 */
	public function store($key, $value)
	{
		if($this->sessionObject) {
			$sessionData = $this->sessionObject->getSessionData($this->storageKey);
			$sessionData[$key] = $value;
			$this->sessionObject->setAndSaveSessionData($this->storageKey, $sessionData);
		}
	}

	/**
	 * @param string $key
	 */
	public function delete($key)
	{
		if($this->sessionObject)
		{
			$sessionData = $this->sessionObject->getSessionData($this->storageKey);
			unset($sessionData[$key]);
			$this->sessionObject->setAndSaveSessionData($this->storageKey, $sessionData);
		}
	}

	/**
	 * @param string $key
	 * @return mixed
	 */
	public function get($key)
	{
		if($this->sessionObject)
		{
			$sessionData = $this->sessionObject->getSessionData($this->storageKey);
			return isset($sessionData[$key]) ? $sessionData[$key] : NULL;
		}
		return NULL;
	}

	/**
	 * Check value from session
	 * @param string $key
	 * @return mixed
	 */
	public function stored($key)
	{
		if($this->sessionObject)
		{
			$sessionData = $this->sessionObject->getSessionData( $this->storageKey );
			return isset( $sessionData[$key] ) ? TRUE:FALSE;
		}
		return FALSE;
	}
}
?>