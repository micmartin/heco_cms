<?php
defined('TYPO3_MODE') or die();

$ll = 'LLL:EXT:moox_news/Resources/Private/Language/locallang_db.xlf:';

// Extension manager configuration
$configuration = \Neulandlotsen\MooxNews\Utility\EmConfiguration::getSettings();

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToInsertRecords('tx_mooxnews_domain_model_news');

$tx_mooxnews_domain_model_news = [
    'ctrl' => [
        'title' => $ll . 'tx_mooxnews_domain_model_news',
        'descriptionColumn' => 'notes',
        'label' => 'title',
		'label_alt' => 'datetime',
		'label_alt_force' => 'title',
        'prependAtCopy' => $configuration->getPrependAtCopy() ? 'LLL:EXT:lang/locallang_general.xlf:LGL.prependAtCopy' : '',
        'hideAtCopy' => true,
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'cruser_id' => 'cruser_id',
        'versioningWS' => true,
        'origUid' => 't3_origuid',		
        'editlock' => 'editlock',
        /*
			'type' => 'type',
	        'typeicon_column' => 'type',
	        'typeicon_classes' => [
	            'default' => 'ext-news-type-default',
	            '1' => 'ext-news-type-internal',
	            '2' => 'ext-news-type-external',
	        ],
		*/
        'useColumnsForDefaultValues' => 'type',
        'languageField' => 'sys_language_uid',
        'transOrigPointerField' => 'l10n_parent',
        'transOrigDiffSourceField' => 'l10n_diffsource',
        'translationSource' => 'l10n_source',
        'default_sortby' => 'ORDER BY datetime DESC',
        'sortby' => ($configuration->getManualSorting() ? 'sorting' : ''),
        'delete' => 'deleted',
        'enablecolumns' => [
            'disabled' => 'hidden',
            'starttime' => 'starttime',
            'endtime' => 'endtime',
            'fe_group' => 'fe_group',
        ],
        'iconfile' => 'EXT:moox_news/Resources/Public/Icons/mooxnews_domain_model_news.svg',
        'searchFields' => 'uid,title',
        'thumbnail' => 'fal_media',		
    ],
	'moox' => [
		'fieldGroups' => [],
		'additionalFrontendDemandFields' => [],
		'additionalFrontendListFields' => [],
		'additionalFrontendDetailFields' => [],
		'additionalFrontendSortingFields' => [],
		'additionalFrontendDateFields' => [],
		'backendPreviewFields' => [
			'moox_news' => [
				'type',
				'title',
				'subtitle',
				'datetime',
				'event_starttime',
				'event_endtime',
				'fal_media',
				'categories',
				'tags',
				'targets',
				'fe_group',
				'teaser',				
				'bodytext',				
				'starttime',
				'endtime',
				'notes',
				'path_segment'
			],
		],	
		'allowedBackendListViewFields' => [
			'uid',
			'title',
			'subtitle',
			'datetime',
			'event_starttime',
			'event_endtime',
			'crdate',
			'archive',
			'tstamp',
			'archive',
			'endtime',
			'starttime'
		],
		'allowedGrabberFields' => [
			'pid',
			'type',
			'hidden',
			'year',
			'title',
			'datetime',
			'subtitle',
			'event_starttime',
			'event_endtime',
			'starttime',
			'endtime',
			'crdate',
			'archive',
			'tstamp',
			'endtime',
			'teaser',
			'bodytext',
			'categories',
			'targets',
			'fe_group',
			'related',
			'fal_media',
			'fal_related_files',
			'author',
			'author_email',
			'keywords',
			'description',
			'grabber_remote_uid',
			'grabber_remote_hash',
			'import_source',
			'import_id',
			'path_segment',
			'l10n_parent',
			'l10n_source',
			'l10n_state',
			'sys_language_uid',
			't3_origuid'
		],
		'searchFields' => [
			'moox_news' => [
				'title',
				'teaser',
				'bodytext',
			],
		],
		'dateTimeDefaultField' => [
			'moox_news' => 'datetime'
		],
	],
    'interface' => [
        'showRecordFieldList' => 'cruser_id,pid,sys_language_uid,l10n_parent,l10n_diffsource,hidden,starttime,endtime,fe_group,title,subtitle,teaser,bodytext,datetime,event_starttime,event_endtime,archive,author,author_email,categories,related,type,keywords,media,internalurl,externalurl,istopnews,related_files,related_links,content_elements,tags,path_segment,alternative_title,fal_related_files,ics_download_allowed,grabber_remote_uid'
    ],
    'columns' => [
        'sys_language_uid' => [
            'exclude' => true,
			'label' => 'LLL:EXT:lang/Resources/Private/Language/locallang_general.xlf:LGL.language',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'special' => 'languages',
                'items' => [
                    [
                        'LLL:EXT:lang/locallang_general.xlf:LGL.allLanguages',
                        -1,
                        'flags-multiple'
                    ],
                ],
                'default' => 0,
            ]
        ],
        'l10n_parent' => [
            'displayCond' => 'FIELD:sys_language_uid:>:0',
            'exclude' => true,
			'label' => 'LLL:EXT:lang/Resources/Private/Language/locallang_general.xlf:LGL.l18n_parent',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'items' => [
                    ['', 0],
                ],
                'foreign_table' => 'tx_mooxnews_domain_model_news',
                'foreign_table_where' => 'AND tx_mooxnews_domain_model_news.pid=###CURRENT_PID### AND tx_mooxnews_domain_model_news.sys_language_uid IN (-1,0)',
                'showIconTable' => false,
				'fieldWizard' => [
                    'selectIcons' => [
                        'disabled' => true,
                    ],
                ],
                'default' => 0,
            ]
        ],
		't3_origuid' => [
			'config' => [
				'type' => 'passthrough'
			]
		],
        'l10n_source' => [
            'config' => [
                'type' => 'passthrough'
            ]
        ],
		'l10n_state' => [
			'config' => [
				'type' => 'passthrough'
			]
		],
        'l10n_diffsource' => [
            'config' => [
                'type' => 'passthrough',
                'default' => ''
            ]
        ],
        'hidden' => [
            'exclude' => true,
			'label' => 'LLL:EXT:lang/Resources/Private/Language/locallang_general.xlf:LGL.hidden',
            'config' => [
                'type' => 'check',
                'default' => ($configuration->getHiddenByDefault()?1:0)
            ],
			// special moox configuration
			'moox' => [
				'extkey' => 'moox_news',
				'plugins' => [
					'mooxnewsfrontend' => [
						'add','edit','list','detail'
					],
				],
				'sortable' => 1,
			],
        ],
        'cruser_id' => [
            'label' => 'cruser_id',
            'config' => [
                'type' => 'passthrough'
            ]
        ],
		'uid' => [
            'label' => $ll . 'tx_mooxnews_domain_model_news.uid',
            'config' => [
                'type' => 'passthrough'
            ],
			// special moox configuration
			'moox' => [
				'extkey' => 'moox_news',
				'backend' => [
					'type' => 'default',
				],
				'sortable' => 1,
			],
        ],					
        'pid' => [
            'label' => 'pid',
            'config' => [
                'type' => 'passthrough'
            ]
        ],
        'crdate' => [
            'label' => 'crdate',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'eval' => 'datetime',
            ],
			// special moox configuration
			'moox' => [
				'extkey' => 'moox_news',
				'plugins' => [
					'mooxnewsfrontend' =>[
						'list','detail'
					],
				],
				'backend' => [
					'type' => 'datetime',
				],
				'sortable' => 1,
			],
        ],
        'tstamp' => [
            'label' => 'tstamp',
            'config' => [
                'type' => 'passthrough',
            ],
			// special moox configuration
			'moox' => [
				'extkey' => 'moox_news',
				'plugins' => [
					'mooxnewsfrontend' => [
						'list','detail'
					],
				],
				'backend' => [
					'type' => 'datetime',
				],
				'sortable' => 1,
			],
        ],
        'sorting' => [
            'label' => 'sorting',
            'config' => [
                'type' => 'passthrough',
            ],
			// special moox configuration
			'moox' => [
				'extkey' => 'moox_news',
				'plugins' => [
					'mooxnewsfrontend' => [
						'list'
					],
				],
				'sortable' => 1,
			],
        ],
        'starttime' => [
            'exclude' => true,
            'label' => 'LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:starttime_formlabel',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'size' => 16,
                'eval' => 'datetime,int',
                'default' => 0,
				'behaviour' => [
                    'allowLanguageSynchronization' => true,
                ],
            ],
			// special moox configuration
			'moox' => [
				'extkey' => 'moox_news',
				'plugins' => [
					'mooxnewsfrontend' => [
						'list','detail'
					],
				],	
				'backend' => [
					'type' => 'datetime',
				],
				'sortable' => 1,
			],
        ],
        'endtime' => [
            'exclude' => true,
            'label' => 'LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:endtime_formlabel',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'size' => 16,
                'eval' => 'datetime,int',
                'default' => 0,
				'behaviour' => [
                    'allowLanguageSynchronization' => true,
                ],
            ],
			// special moox configuration
			'moox' => [
				'extkey' => 'moox_news',
				'plugins' => [
					'mooxnewsfrontend' => [
						'list','detail'
					],
				],
				'backend' => [
					'type' => 'datetime',
				],
				'sortable' => 1,
			],
        ],
        'fe_group' => [
            'exclude' => true,
            'label' => 'LLL:EXT:lang/Resources/Private/Language/locallang_general.xlf:LGL.fe_group',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectMultipleSideBySide',
                'size' => 5,
                'maxitems' => 999,               
				'items' => [
                    [
                        'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.hide_at_login',
                        -1
                    ],
                    [
                        'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.any_login',
                        -2
                    ],
                    [
                        'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.usergroups',
                        '--div--'
                    ]
                ],
                'exclusiveKeys' => '-1,-2',
                'foreign_table' => 'fe_groups',
                'foreign_table_where' => 'ORDER BY fe_groups.title',
            ],
			// special moox configuration
			'moox' => [
				'extkey' => 'moox_news',
				'backend' => [
					'type' => 'list',
					'icons' => [
						'default' => 'status-user-group-frontend',
						'main' => 'status-user-group-backend'
					]
				],
				'sortable' => 0,
			],
        ],
        'title' => [
            'exclude' => false,
            'l10n_mode' => 'prefixLangTitle',
            'label' => 'LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:header_formlabel',
            'onChange' => ((1 || $configuration->getForceReloadByTitleChanges())?'reload':''),
			'config' => [
                'type' => 'input',
                'size' => 60,
                'eval' => 'required',
            ],
			// special moox configuration
			'moox' => [
				'extkey' => 'moox_news',
				'plugins' => [
					'mooxnewsfrontend' => [
						'add','edit','list','detail'
					],
				],
				'backend' => [
					'type' => 'default',
				],
				'sortable' => 1,
				'searchable' => 1,
			],
        ],
        'subtitle' => [
            'exclude' => true,
			'l10n_mode' => 'prefixLangTitle',
            'label' => $ll . 'tx_mooxnews_domain_model_news.subtitle',
            'config' => [
                'type' => 'input',
                'size' => 60,
            ],
            // special moox configuration
            'moox' => [
                'extkey' => 'moox_news',
                'plugins' => [
                    'mooxnewsfrontend' => [
                        'add','edit','list','detail'
                    ],
                ],
                'backend' => [
                    'type' => 'default',
                ],
                'sortable' => 1,
            ],
        ],
        'alternative_title' => [
            'exclude' => true,
            'label' => $ll . 'tx_mooxnews_domain_model_news.alternative_title',
            'config' => [
                'type' => 'input',
                'size' => 30,
            ],
			// special moox configuration
			'moox' => [
				'extkey' => 'moox_news',
				'plugins' => [
					'mooxnewsfrontend' => [
						'add','edit','list','detail'
					],
				],
				'backend' => [
					'type' => 'default',
				],
				'sortable' => 1,
			],
        ],
        'teaser' => [
            'exclude' => true,
            'label' => $ll . 'tx_mooxnews_domain_model_news.teaser',
            'config' => [
                'type' => 'text',
				'enableRichtext' => $configuration->getRteForTeaser()?true:false,
                'cols' => 50,   
				'rows' => 5
            ],
			// special moox configuration
			'moox' => [
				'extkey' => 'moox_news',
				'plugins' => [
					'mooxnewsfrontend' => [
						'add','edit','detail'
					],
				],
				'backend' => [
					'type' => 'text',
				],
				'sortable' => 0,
				'searchable' => 1,
			],
        ],
        'bodytext' => [
            'exclude' => false,
            'label' => $ll . 'tx_mooxnews_domain_model_news.bodytext',
            'config' => [
                'type' => 'text',
                'cols' => 30,
                'rows' => 5,
                'softref' => 'rtehtmlarea_images,typolink_tag,images,email[subst],url',
                'enableRichtext' => true,
            ]
        ],
        'datetime' => [
            'exclude' => false,
            'label' => $ll . 'tx_mooxnews_domain_model_news.datetime',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'size' => 16,
                'eval' => 'datetime,int' . ($configuration->getDateTimeRequired() ? ',required' : ''),
            ],
			// special moox configuration
			'moox' => [
				'extkey' => 'moox_news',
				'plugins' => [
					'mooxnewsfrontend' => [
						'add','edit','list','detail'
					],
				],
				'backend' => [
					'type' => 'datetime',
					'label' => $ll . 'tx_mooxnews_domain_model_news.datetime.list',
				],
				'sortable' => 1,
			],
        ],
		'event_starttime' => [
            'exclude' => true,
            'label' => $ll . 'tx_mooxnews_domain_model_news.event_starttime',
            'config' => [
                'type' => 'input',
				'renderType' => 'inputDateTime',
                'size' => 16,
				'eval' => 'datetime,int'
            ],
			// special moox configuration
			'moox' => [
				'extkey' => 'moox_news',
				'plugins' => [
					'mooxnewsfrontend' => [
						'add','edit','list','detail'
					],
				],
				'backend' => [
					'type' => 'datetime',
					'label' => $ll . 'tx_mooxnews_domain_model_news.event_starttime.list',
				],
				'sortable' => 1,
			],
        ],
		'event_endtime' => [
            'exclude' => true,
            'label' => $ll . 'tx_mooxnews_domain_model_news.event_endtime',
            'config' => [
                'type' => 'input',
				'renderType' => 'inputDateTime',
                'size' => 16,
				'eval' => 'datetime,int'
            ],
			// special moox configuration
			'moox' => [
				'extkey' => 'moox_news',
				'plugins' => [
					'mooxnewsfrontend' => [
						'add','edit','list','detail'
					],
				],
				'backend' => [
					'type' => 'datetime',
					'label' => $ll . 'tx_mooxnews_domain_model_news.event_endtime.list',
				],
				'sortable' => 1,
			],
        ],
        'archive' => [
            'exclude' => true,
            'l10n_mode' => 'copy',
            'label' => $ll . 'tx_mooxnews_domain_model_news.archive',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'size' => 30,
                'eval' => $configuration->getArchiveDate() . ',int',
                'default' => 0
            ],
			// special moox configuration
			'moox' => [
				'extkey' => 'moox_news',
				'plugins' => [
					'mooxnewsfrontend' => [
						'add','edit','list','detail'
					],
				],
				'backend' => [
					'type' => 'datetime',
					'label' => $ll . 'tx_mooxnews_domain_model_news.archive.list',
				],
				'sortable' => 1,
			],
        ],
        'author' => [
            'exclude' => true,
            'label' => 'LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.author_formlabel',
            'config' => [
                'type' => 'input',
                'size' => 30,
				'behaviour' => [
                    'allowLanguageSynchronization' => true,
                ],
            ],
			// special moox configuration
			'moox' => [
				'extkey' => 'moox_news',
				'plugins' => [
					'mooxnewsfrontend' => [
						'add','edit','list','detail'
					],
				],
				'backend' => [
					'type' => 'default',
				],
				'sortable' => 1,
			],
        ],
        'author_email' => [
            'exclude' => true,
            'label' => 'LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.author_email_formlabel',
            'config' => [
                'type' => 'input',
                'size' => 30,
				'behaviour' => [
                    'allowLanguageSynchronization' => true,
                ],
            ],
			// special moox configuration
			'moox' => [
				'extkey' => 'moox_news',
				'plugins' => [
					'mooxnewsfrontend' => [
						'add','edit','list','detail'
					],
				],
				'backend' => [
					'type' => 'email',
				],
				'sortable' => 1,
			],
        ],
        'categories' => [
            'exclude' => true,
            'label' => $ll . 'tx_mooxnews_domain_model_news.categories',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSortableCategoryTree',
                'treeConfig' => [
                    'dataProvider' => \Neulandlotsen\MooxNews\TreeProvider\DatabaseTreeDataProvider::class,
                    'parentField' => 'parent',
                    'appearance' => [
                        'showHeader' => true,
                        //'allowRecursiveMode' => true,
                        'expandAll' => true,
                        'maxLevels' => 99,
                    ],
                ],
                'MM' => 'sys_category_record_mm',
                'MM_match_fields' => [
                    'fieldname' => 'categories',
                    'tablenames' => 'tx_mooxnews_domain_model_news',
                ],
                'MM_opposite_field' => 'items',
                'foreign_table' => 'sys_category',
                'foreign_table_where' => ' AND (sys_category.sys_language_uid = 0 OR sys_category.l10n_parent = 0) ORDER BY sys_category.sorting',
                'size' => 10,
                'autoSizeMax' => 20,
                'minitems' => $configuration->getMinCategoryCount(),
				'maxitems' => ($configuration->getMaxCategoryCount()>0)?$configuration->getMaxCategoryCount():9999,
				'behaviour' => [
                    'allowLanguageSynchronization' => true,
                ],
            ],
			// special moox configuration
			'moox' => [
				'extkey' => 'moox_news',
				'plugins' => [
					'mooxnewsfrontend' => [
						'add','edit'
					],
				],
				'backend' => [
					'type' => 'list',
					'icons' => [
						'default' => 'mimetypes-x-sys_category',
						'main' => 'actions-system-shortcut-new'
					]
				],
				'sortable' => 0,
			],
        ],
		'single_pid' => [
			'exclude' => 1,
			'label' => $ll . 'tx_mooxnews_domain_model_news.single_pid',
			'config' => [
				'type' => 'group',
				'internal_type' => 'db',
				'allowed' => 'pages',
				'size' => 1,
				'maxitems' => 1,
				'minitems' => 0,
				'show_thumbs' => 0,
				'wizards' => [
	                'suggest' => [
	                    'type' => 'suggest',
	                    'default' => [
	                        'searchWholePhrase' => true
	                    ]
	                ],
	            ],
			],
			// special moox configuration
			'moox' => [
				'extkey' => 'moox_news',				
			],
		],
        'related' => [
            'exclude' => true,
            'label' => $ll . 'tx_mooxnews_domain_model_news.related',
            'config' => [
                'type' => 'group',
                'internal_type' => 'db',
                'allowed' => 'tx_mooxnews_domain_model_news',
                'foreign_table' => 'tx_mooxnews_domain_model_news',
                'MM_opposite_field' => 'related_from',
                'size' => 5,
                'minitems' => 0,
                'maxitems' => 100,
                'MM' => 'tx_mooxnews_domain_model_news_related_mm',
                'suggestOptions' => [
                    'default' => [
                        'suggestOptions' => true,
                        'addWhere' => ' AND tx_mooxnews_domain_model_news.uid != ###THIS_UID###'
                    ]
                ],
                'behaviour' => [
                    'allowLanguageSynchronization' => true,
                ],
            ],
			// special moox configuration
			'moox' => [
				'extkey' => 'moox_news',				
			],			
        ],
        'related_from' => [
            'exclude' => true,
            'label' => $ll . 'tx_mooxnews_domain_model_news.related_from',
            'config' => [
                'type' => 'group',
                'internal_type' => 'db',
                'foreign_table' => 'tx_mooxnews_domain_model_news',
                'allowed' => 'tx_mooxnews_domain_model_news',
                'size' => 5,
                'maxitems' => 100,
                'MM' => 'tx_mooxnews_domain_model_news_related_mm',
                'readOnly' => 1,
            ],
			// special moox configuration
			'moox' => [
				'extkey' => 'moox_news',				
			],
        ],
        'related_links' => [
            'exclude' => true,
            'label' => $ll . 'tx_mooxnews_domain_model_news.related_links',
            'config' => [
                'type' => 'inline',
                'allowed' => 'tx_mooxnews_domain_model_link',
                'foreign_table' => 'tx_mooxnews_domain_model_link',
                'foreign_sortby' => 'sorting',
                'foreign_field' => 'parent',
                'size' => 5,
                'minitems' => 0,
                'maxitems' => 100,
                'appearance' => [
                    'collapseAll' => true,
                    'expandSingle' => true,
                    'levelLinksPosition' => 'bottom',
                    'useSortable' => true,
                    'showPossibleLocalizationRecords' => true,
                    'showRemovedLocalizationRecords' => true,
                    'showAllLocalizationLink' => true,
                    'showSynchronizationLink' => true,
                    'enabledControls' => [
                        'info' => false,
                    ]
                ],
				'behaviour' => [
                    'allowLanguageSynchronization' => true,
                ],
            ],
			// special moox configuration
			'moox' => [
				'extkey' => 'moox_news',				
			],
        ],
        'type' => [
            'exclude' => false,
            'label' => 'LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.doktype_formlabel',
			'onChange' => 'reload',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'items' => [
					[$ll . 'tx_mooxnews_domain_model_news.type.moox_news', 'moox_news', 'ext-news-type-default'],
					//[$ll . 'tx_mooxnews_domain_model_news.type.I.0', 0, 'ext-news-type-default'],
                    //[$ll . 'tx_mooxnews_domain_model_news.type.I.1', 1, 'ext-news-type-internal'],
                    //[$ll . 'tx_mooxnews_domain_model_news.type.I.2', 2, 'ext-news-type-external']
				],
				'itemsProcFunc' => 'Neulandlotsen\MooxNews\Hooks\ItemsProcFunc->user_types_restricted',
                'fieldWizard' => [
                    'selectIcons' => [
                        'disabled' => false,
                    ],
                ],
				'size' => 1,
                'maxitems' => 1,
            ],
			// special moox configuration
			'moox' => [
				'extkey' => 'moox_news',
				'plugins' => [
					'mooxnewsfrontend' => [
						'add','edit','list','detail'
					],
				],
				'backend' => [
					'type' => 'translate',
				],
				'sortable' => 1,
			],
        ],
        'keywords' => [
            'exclude' => true,
            'label' => $GLOBALS['TCA']['pages']['columns']['keywords']['label'],
            'config' => [
                'type' => 'text',
                'placeholder' => $ll . 'tx_mooxnews_domain_model_news.keywords.placeholder',
                'cols' => 30,
                'rows' => 5,
				'behaviour' => [
                    'allowLanguageSynchronization' => true,
                ],
            ],
			// special moox configuration
			'moox' => [
				'extkey' => 'moox_news',
				'backend' => [
					'type' => 'default',
				],				
			],
        ],
        'description' => [
            'exclude' => true,
            'label' => 'LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.description_formlabel',
            'config' => [
                'type' => 'text',
                'cols' => 30,
                'rows' => 5,
				'behaviour' => [
                    'allowLanguageSynchronization' => true,
                ],
            ],
			// special moox configuration
			'moox' => [
				'extkey' => 'moox_news',
				'backend' => [
					'type' => 'text',
				],				
			],
        ],
        'internalurl' => [
            'exclude' => false,
            'label' => $ll . 'tx_mooxnews_domain_model_news.type.I.1',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputLink',
                'size' => 30,
                'max' => 255,
                'eval' => 'trim,required',
                'softref' => 'typolink'
            ],
			// special moox configuration
			'moox' => [
				'extkey' => 'moox_news',							
			],
        ],
        'externalurl' => [
            'exclude' => false,
            'label' => 'LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.doktype.I.8',
            'config' => [
                'type' => 'input',
                'size' => 50,
                'eval' => 'required',
                'softref' => 'typolink'
            ],
			// special moox configuration
			'moox' => [
				'extkey' => 'moox_news',							
			],
        ],
        'istopnews' => [
            'exclude' => true,
            'label' => $ll . 'tx_mooxnews_domain_model_news.istopnews',
            'config' => [
                'type' => 'check',
                'default' => 0
            ],
			// special moox configuration
			'moox' => [
				'extkey' => 'moox_news',
				'plugins' => [
					'mooxnewsfrontend' => [
						'add','edit','list','detail'
					],
				],
				'sortable' => 1,
			],
        ],
        'editlock' => [
            'exclude' => true,
            'label' => 'LLL:EXT:lang/Resources/Private/Language/locallang_tca.xlf:editlock',
            'config' => [
                'type' => 'check',
                'default' => 0,
                'behaviour' => [
                    'allowLanguageSynchronization' => true,
                ],
            ],
			// special moox configuration
			'moox' => [
				'extkey' => 'moox_news',				
			],
        ],
        'content_elements' => [
            'exclude' => true,
            'label' => $ll . 'tx_mooxnews_domain_model_news.content_elements',
            'config' => [
                'type' => 'inline',
                'allowed' => 'tt_content',
                'foreign_table' => 'tt_content',
                'foreign_sortby' => 'sorting',
                'foreign_field' => 'tx_mooxnews_related_news',
                'minitems' => 0,
                'maxitems' => 99,
                'appearance' => [
                    'useXclassedVersion' => $configuration->getContentElementPreview(),
                    'collapseAll' => true,
                    'expandSingle' => true,
                    'levelLinksPosition' => 'bottom',
                    'useSortable' => true,
                    'showPossibleLocalizationRecords' => true,
                    'showRemovedLocalizationRecords' => true,
                    'showAllLocalizationLink' => true,
                    'showSynchronizationLink' => true,
                    'enabledControls' => [
                        'info' => false,
						'new' => true,
						'dragdrop' => true,
						'sort' => true,
						'hide' => true,
						'delete' => true,
						'localize' => false,
                    ]
                ],
				'behaviour' => [
                    'allowLanguageSynchronization' => true,
                ],
            ],
			// special moox configuration
			'moox' => [
				'extkey' => 'moox_news',				
			],
        ],
		'content_pages' => [
			'exclude' => 1,
			'label' => $ll . 'tx_mooxnews_domain_model_news.content_pages',
			'config' => [
				'type' => 'group',
				'internal_type' => 'db',
				'allowed' => 'pages',
				'size' => 3,
				'maxitems' => 10,
				'minitems' => 0,
				'show_thumbs' => 0,
				'wizards' => [
	                'suggest' => [
	                    'type' => 'suggest',
	                    'default' => [
	                        'searchWholePhrase' => true
	                    ]
	                ],
	            ],
				'behaviour' => [
                    'allowLanguageSynchronization' => true,
                ],
			],
			// special moox configuration
			'moox' => [
				'extkey' => 'moox_news',				
			],
		],
        'tags' => [
            'exclude' => true,
            'label' => $ll . 'tx_mooxnews_domain_model_news.tags',
            'config' => [
                'type' => 'select',
				'renderType' => 'selectMultipleSideBySide',
				'enableMultiSelectFilterTextfield' => true,
                'internal_type' => 'db',
                'allowed' => 'tx_mooxnews_domain_model_tag',
                'MM' => 'tx_mooxnews_domain_model_news_tag_mm',
                'foreign_table' => 'tx_mooxnews_domain_model_tag',
                'foreign_table_where' => 'AND tx_mooxnews_domain_model_tag.deleted=0 AND tx_mooxnews_domain_model_tag.hidden=0 ORDER BY tx_mooxnews_domain_model_tag.title',
				'itemsProcFunc' => 'Neulandlotsen\MooxNews\Hooks\ItemsProcFunc->user_tags',
                'size' => 10,
                'autoSizeMax' => 20,
                'minitems' => 0,
                'maxitems' => 99,
                'fieldInformation' => [
                    'tagInformation' => [
                        'renderType' => 'NewsStaticText',
                        'options' => [
                            'labels' => [
                                [
                                    'label' => '',
                                    'bold' => true,
                                    'italic' => true,
                                ],
                            ],
                        ],
                    ],
                ],
                'suggestOptions' => [
                    'default' => [
                        'minimumCharacters' => 2,
                        'searchWholePhrase' => true,
                        'receiverClass' => \Neulandlotsen\MooxNews\Backend\Wizard\SuggestWizardReceiver::class
                    ],
                ],
                'fieldControl' => [
                    'editPopup' => [
                        'disabled' => false,
                        'options' => [
                            'type' => 'popup',
                            'title' => $ll . 'tx_mooxnews_domain_model_news.tags.edit',
                            'module' => [
                                'name' => 'wizard_edit',
                            ],
                            'popup_onlyOpenIfSelected' => true,
                            'icon' => 'actions-open',
                            'JSopenParams' => 'height=350,width=580,status=0,menubar=0,scrollbars=1',
                        ],
                    ],
                    'listModule' => [
                        'disabled' => false,
                        'options' => [
                            'type' => 'script',
                            'title' => $ll . 'tx_mooxnews_domain_model_news.tags.list',
                            'icon' => 'actions-system-list-open',
                            'params' => [
                                'table' => 'tx_news_domain_model_tag',
                                'pid' => $configuration->getTagPid(),
                            ],
                            'module' => [
                                'name' => 'wizard_list',
                            ],
                        ],
                    ],
                ],
                'behaviour' => [
                    'allowLanguageSynchronization' => true,
                ],
            ],
			// special moox configuration
			'moox' => [
				'extkey' => 'moox_news',				
				'backend' => [
					'type' => 'list',
					'icons' => [
						'default' => 'mimetypes-x-sys_workspace'
					]
				],				
			],
        ],
		'targets' => [
            'exclude' => true,
            'l10n_mode' => 'mergeIfNotBlank',
            'label' => $ll . 'tx_mooxnews_domain_model_news.targets',
            'config' => [
                'type' => 'select',
				'renderType' => 'selectMultipleSideBySide',
				'enableMultiSelectFilterTextfield' => true,
                'internal_type' => 'db',
                'allowed' => 'tx_mooxnews_domain_model_target',
                'MM' => 'tx_mooxnews_domain_model_news_target_mm',
                'foreign_table' => 'tx_mooxnews_domain_model_target',
                'foreign_table_where' => 'AND tx_mooxnews_domain_model_target.deleted=0 AND tx_mooxnews_domain_model_target.hidden=0 ORDER BY tx_mooxnews_domain_model_target.title',
				'itemsProcFunc' => 'Neulandlotsen\MooxNews\Hooks\ItemsProcFunc->user_targets',
                'size' => 10,
                'autoSizeMax' => 20,
                'minitems' => 0,
                'maxitems' => 99,
                'fieldInformation' => [
                    'targetInformation' => [
                        'renderType' => 'NewsStaticText',
                        'options' => [
                            'labels' => [
                                [
                                    'label' => '',
                                    'bold' => true,
                                    'italic' => true,
                                ],
                            ],
                        ],
                    ],
                ],
                'suggestOptions' => [
                    'default' => [
                        'minimumCharacters' => 2,
                        'searchWholePhrase' => true,
                        'receiverClass' => \Neulandlotsen\MooxNews\Backend\Wizard\SuggestWizardReceiver::class
                    ],
                ],
                'fieldControl' => [
                    'editPopup' => [
                        'disabled' => false,
                        'options' => [
                            'type' => 'popup',
                            'title' => $ll . 'tx_mooxnews_domain_model_news.tags.edit',
                            'module' => [
                                'name' => 'wizard_edit',
                            ],
                            'popup_onlyOpenIfSelected' => true,
                            'icon' => 'actions-open',
                            'JSopenParams' => 'height=350,width=580,status=0,menubar=0,scrollbars=1',
                        ],
                    ],
                    'listModule' => [
                        'disabled' => false,
                        'options' => [
                            'type' => 'script',
                            'title' => $ll . 'tx_mooxnews_domain_model_news.tags.list',
                            'icon' => 'actions-system-list-open',
                            'params' => [
                                'table' => 'tx_news_domain_model_tag',
                                'pid' => $configuration->getTargetPid(),
                            ],
                            'module' => [
                                'name' => 'wizard_list',
                            ],
                        ],
                    ],
                ],
                'behaviour' => [
                    'allowLanguageSynchronization' => true,
                ],
            ],
			// special moox configuration
			'moox' => [
				'extkey' => 'moox_news',				
				'backend' => [
					'type' => 'list',
					'icons' => [
						'default' => 'status-user-group-frontend'
					]
				],				
			],
        ],
        'path_segment' => [
            'exclude' => true,
            'label' => $ll . 'tx_mooxnews_domain_model_news.path_segment',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'nospace,alphanum_x,lower,unique',
            ],
			// special moox configuration
			'moox' => [
				'extkey' => 'moox_news',				
			],
        ],
        'import_id' => [
            'label' => $ll . 'tx_mooxnews_domain_model_news.import_id',
            'config' => [
                'type' => 'passthrough'
            ]
        ],
        'import_source' => [
            'label' => $ll . 'tx_mooxnews_domain_model_news.import_source',
            'config' => [
                'type' => 'passthrough'
            ]
        ],
        'fal_media' => [
            'exclude' => true,
            'label' => $ll . 'tx_mooxnews_domain_model_news.fal_media',
            'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig(
                'fal_media',
                [
                    'behaviour' => [
                        'allowLanguageSynchronization' => true,
                    ],
                    'appearance' => [
                        'createNewRelationLinkTitle' => $ll . 'tx_mooxnews_domain_model_news.fal_media.add',
                        'showPossibleLocalizationRecords' => true,
                        'showRemovedLocalizationRecords' => true,
                        'showAllLocalizationLink' => true,
                        'showSynchronizationLink' => true
                    ],
                    'foreign_match_fields' => [
                        'fieldname' => 'fal_media',
                        'tablenames' => 'tx_mooxnews_domain_model_news',
                        'table_local' => 'sys_file',
                    ],
                    // custom configuration for displaying fields in the overlay/reference table
                    // to use the newsPalette and imageoverlayPalette instead of the basicoverlayPalette
                    'foreign_types' => [
                        '0' => [
                            'showitem' => '
						--palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;newsPalette,
						--palette--;;imageoverlayPalette,
						--palette--;;filePalette'
                        ],
                        \TYPO3\CMS\Core\Resource\File::FILETYPE_TEXT => [
                            'showitem' => '
						--palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;newsPalette,
						--palette--;;imageoverlayPalette,
						--palette--;;filePalette'
                        ],
                        \TYPO3\CMS\Core\Resource\File::FILETYPE_IMAGE => [
                            'showitem' => '
						--palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;newsPalette,
						--palette--;;imageoverlayPalette,
						--palette--;;filePalette'
                        ],
                        \TYPO3\CMS\Core\Resource\File::FILETYPE_AUDIO => [
                            'showitem' => '
						--palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;newsPalette,
						--palette--;;imageoverlayPalette,
						--palette--;;filePalette'
                        ],
                        \TYPO3\CMS\Core\Resource\File::FILETYPE_VIDEO => [
                            'showitem' => '
						--palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;newsPalette,
						--palette--;;imageoverlayPalette,
						--palette--;;filePalette'
                        ],
                        \TYPO3\CMS\Core\Resource\File::FILETYPE_APPLICATION => [
                            'showitem' => '
						--palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;newsPalette,
						--palette--;;imageoverlayPalette,
						--palette--;;filePalette'
                        ]
                    ]
                ],
                $GLOBALS['TYPO3_CONF_VARS']['SYS']['mediafile_ext']
            ),
			// special moox configuration
			'moox' => [
				'extkey' => 'moox_news',
				'backend' => [
					'type' => 'images',
				],				
			],
        ],
        'fal_related_files' => [
            'exclude' => true,
            'label' => $ll . 'tx_mooxnews_domain_model_news.fal_related_files',
            'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig(
                'fal_related_files',
                [
                    'behaviour' => [
                        'allowLanguageSynchronization' => true,
                    ],
                    'appearance' => [
                        'createNewRelationLinkTitle' => $ll . 'tx_mooxnews_domain_model_news.fal_related_files.add',
                        'showPossibleLocalizationRecords' => true,
                        'showRemovedLocalizationRecords' => true,
                        'showAllLocalizationLink' => true,
                        'showSynchronizationLink' => true
                    ],
                    'inline' => [
                        'inlineOnlineMediaAddButtonStyle' => 'display:none'
                    ],
                    'foreign_match_fields' => [
                        'fieldname' => 'fal_related_files',
                        'tablenames' => 'tx_mooxnews_domain_model_news',
                        'table_local' => 'sys_file',
                    ],
                ]
            ),
			// special moox configuration
			'moox' => [
				'extkey' => 'moox_news',				
			],
        ],
        'notes' => [
            'label' => $ll . 'notes',
            'config' => [
                'type' => 'text',
                'rows' => 10,
                'cols' => 48
            ],
			// special moox configuration
			'moox' => [
				'extkey' => 'moox_news',
				'backend' => [
					'type' => 'text',
				],				
			],
        ],
		'exclude_from_rss' => [
			'exclude' => true,
			'label' => $ll . 'tx_mooxnews_domain_model_news.exclude_from_rss',
			'config' => [
				'type' => 'check',
				'default' => ($configuration->getExcludeFromRssDefault())?1:0,
			],
			// special moox configuration
			'moox' => [
				'extkey' => 'moox_news',				
			],
		],
		'grabber_remote_uid' => [
			'exclude' => true,
			'label' => $ll . 'tx_mooxnews_domain_model_news.grabber_remote_uid',
			'config' => [
				'type'     => 'input',
				'size'     => '4',
				'max'      => '10000',
				'eval'     => 'int',
				'checkbox' => '0',
				'default' => 0
			],
			// special moox configuration
			'moox' => [
				'extkey' => 'moox_news',				
			],
		],
		'grabber_remote_hash' => [
			'exclude' => true,
			'label' => $ll . 'tx_mooxnews_domain_model_news.grabber_remote_hash',
			'config' => [
				'type' => 'input',
				'size' => '30',
			],
			// special moox configuration
			'moox' => [
				'extkey' => 'moox_news',				
			],
		],
		'search_index' => [
			'label' => $ll . 'tx_mooxnews_domain_model_news.search_index',
			'config' => [
				'type' => 'passthrough'
			],
			// special moox configuration
			'moox' => [
				'extkey' => 'moox_news',				
			],
		],
		'indexed' => [
			'exclude' => 1,
			'label' => $ll . 'tx_mooxnews_domain_model_news.indexed',
			'config' => [
				'type' => 'input',
				'size' => 16,
				'max' => 20,
				'eval' => 'datetime',
				'default' => 0,
			],
			// special moox configuration
			'moox' => [
				'extkey' => 'moox_news',				
			],
		],
		'year' => [
			'label' => $ll . 'tx_mooxnews_domain_model_news.year',
			'config' => [
				'type' => 'passthrough'
			],
			// special moox configuration
			'moox' => [
				'extkey' => 'moox_news',				
			],
		],
		'ics_download_allowed' => [
            'exclude' => true,
            'label' => $ll . 'tx_mooxnews_domain_model_news.ics_download_allowed',
            'config' => [
                'type' => 'check',
                'default' => 0
            ],
			// special moox configuration
			'moox' => [
				'extkey' => 'moox_news',
				'sortable' => 0,
			],
        ],
		'hash' => [
			'label' => $ll . 'tx_mooxnews_domain_model_news.hash',
			'config' => [
				'type' => 'passthrough'
			],
			// special moox configuration
			'moox' => [
				'extkey' => 'moox_news',
			],
		],
    ],
    'types' => [
        // default news
        '0' => [            
            'showitem' => 'l10n_parent, l10n_diffsource,
					--palette--;;paletteTitles,
					--palette--;;paletteCore,
					--palette--;;paletteNavtitle,				
					--palette--;Datumsinformationen des Artikels;paletteDate,
					--palette--;Datumsinformationen des Termins (optional);paletteEvent,					
					ics_download_allowed,teaser,bodytext,                				
				--div--;Kategorien,categories,
				--div--;Zielgruppen und Tags,targets,tags,				
				--div--;' . $ll . 'tx_mooxnews_domain_model_news.tabs.relations,fal_media,fal_related_files,related,related_from,
				--div--;' . $ll . 'tx_mooxnews_domain_model_news.tabs.embedded_elements,content_elements,content_pages,
				--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.metadata,
					--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.editorial;paletteAuthor,
					--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.metatags;paletteMetatags,					
				--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.access,
					--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.access;paletteAccess,
				--div--;' . $ll . 'notes,
                    notes,
				--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.extended,'
        ],
        /*
		// internal url
        '1' => [            
            'showitem' => 'l10n_parent, l10n_diffsource,
					title,--palette--;;paletteCore, teaser,
					--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.editorial;paletteAuthor,
					--palette--;;paletteDate,
					internalurl,

				--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.access,
					--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.access;paletteAccess,

				--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.options,categories,tags,
				--div--;' . $ll . 'tx_mooxnews_domain_model_news.tabs.relations,fal_media,fal_related_files,related_links,related,related_from,
				--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.metadata,
					--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.metatags;metatags,
					--palette--;' . $ll . 'tx_mooxnews_domain_model_news.palettes.alternativeTitles;alternativeTitles,
				--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.extended,'
        ],
        // external url
        '2' => [           
            'showitem' => 'l10n_parent, l10n_diffsource,
					title,--palette--;;paletteCore, teaser,
					--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.editorial;paletteAuthor,
					--palette--;;paletteDate,
					externalurl,

				--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.access,
					--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.access;paletteAccess,

				--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.options,categories,tags,
				--div--;' . $ll . 'tx_mooxnews_domain_model_news.tabs.relations,fal_media,fal_related_files,related_links,related,related_from,
				--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.metadata,
					--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.metatags;metatags,
					--palette--;' . $ll . 'tx_mooxnews_domain_model_news.palettes.alternativeTitles;alternativeTitles,
				--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.extended,'
        ],
		*/
    ],
    'palettes' => [
        'paletteAuthor' => [
            'showitem' => 'author,author_email,',
        ],
        'paletteDate' => [
            'showitem' => 'datetime,archive,',
        ], 
		'paletteEvent' => [
            'showitem' => 'event_starttime,event_endtime,',			
        ],
        'paletteCore' => [
            'showitem' => 'type,hidden,istopnews,sys_language_uid',
        ],
        'paletteNavtitle' => [
            'showitem' => 'alternative_title,path_segment',
        ],
        'paletteAccess' => [
            'showitem' => 'starttime;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:starttime_formlabel,
					endtime;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:endtime_formlabel,
					--linebreak--, fe_group;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:fe_group_formlabel,
					--linebreak--,editlock,exclude_from_rss,--linebreak--,',
        ],
        'paletteMetatags' => [
            'showitem' => 'keywords,--linebreak--,description,',
        ],
        'paletteAlternativeTitles' => [
            'showitem' => 'alternative_title,--linebreak--,path_segment',
        ],
        'paletteTitles' => [
            'showitem' => 'title,--linebreak--,subtitle',
        ],
    ]
];

// category restriction based on settings in extension manager
$categoryRestrictionSetting = $configuration->getCategoryRestriction();
if ($categoryRestrictionSetting) {
    $categoryRestriction = '';
    switch ($categoryRestrictionSetting) {
        case 'current_pid':
            $categoryRestriction = ' AND sys_category.pid=###CURRENT_PID### ';
            break;
		case 'storage_pid':
			$categoryRestriction = ' AND sys_category.pid=###STORAGE_PID### ';
			break;
        case 'siteroot':
            $categoryRestriction = ' AND sys_category.pid IN (###SITEROOT###) ';
            break;
        case 'page_tsconfig':
            $categoryRestriction = ' AND sys_category.pid IN (###PAGE_TSCONFIG_IDLIST###) ';
            break;
        default:
            $categoryRestriction = '';
    }

    // prepend category restriction at the beginning of foreign_table_where
    if (!empty($categoryRestriction)) {
        $tx_mooxnews_domain_model_news['columns']['categories']['config']['foreign_table_where'] = $categoryRestriction .
            $tx_mooxnews_domain_model_news['columns']['categories']['config']['foreign_table_where'];
    }
}

if (!$configuration->getContentElementRelation()) {
    unset($tx_mooxnews_domain_model_news['columns']['content_elements']);
}

if (!$configuration->getContentPageRelation()) {
	unset($tx_mooxnews_domain_model_news['columns']['content_pages']);
}

return $tx_mooxnews_domain_model_news;
