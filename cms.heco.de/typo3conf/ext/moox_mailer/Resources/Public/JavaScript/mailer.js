function hideTaskList(){
	TYPO3.jQuery( "#task-expander" ).removeClass( "t3-icon-move-up t3-icon-move-down" );
	TYPO3.jQuery( "#task-expander" ).addClass( "t3-icon-move-down" );
	TYPO3.jQuery( "#task-list" ).hide("fast");
}

function hideUserList(){
	TYPO3.jQuery( "#user-expander" ).removeClass( "t3-icon-move-up t3-icon-move-down" );
	TYPO3.jQuery( "#user-expander" ).addClass( "t3-icon-move-down" );
	TYPO3.jQuery( "#user-list" ).hide("fast");
}

function toggleInfoField(id){	
	if ( TYPO3.jQuery(id).is(":visible") ) {
		visible = true;
	} else { 
		visible = false;
	}
	hideUserList();
	hideTaskList();
	TYPO3.jQuery(".news-info-field").hide("fast");
	TYPO3.jQuery(".news-item-expander-reset").removeClass( "t3-icon-move-up t3-icon-move-down" );
	TYPO3.jQuery(".news-item-expander-reset").addClass( "t3-icon-move-down" );
	if (visible) {		
		TYPO3.jQuery(id).hide("fast");
		TYPO3.jQuery(id + "-expander").removeClass( "t3-icon-move-up t3-icon-move-down" );
		TYPO3.jQuery(id + "-expander").addClass( "t3-icon-move-down" );
	} else { 
		TYPO3.jQuery(id).show("fast");
		TYPO3.jQuery(id + "-expander").removeClass( "t3-icon-move-up t3-icon-move-down" );
		TYPO3.jQuery(id + "-expander").addClass( "t3-icon-move-up" );
	}	
}

function toggleDraftDetails(id){
	if ( TYPO3.jQuery(id).is(":visible") ) {
		visible = true;
	} else { 
		visible = false;
	}	
	TYPO3.jQuery(".draft-details").hide("fast");
	TYPO3.jQuery(".draft-details-expander-reset").removeClass( "t3-icon-move-up t3-icon-move-down" );
	TYPO3.jQuery(".draft-details-expander-reset").addClass( "t3-icon-move-down" );
	if (visible) {		
		TYPO3.jQuery(id).hide("fast");
		TYPO3.jQuery(id + "-expander").removeClass( "t3-icon-move-up t3-icon-move-down" );
		TYPO3.jQuery(id + "-expander").addClass( "t3-icon-move-down" );
	} else { 
		TYPO3.jQuery(id).show("fast");
		TYPO3.jQuery(id + "-expander").removeClass( "t3-icon-move-up t3-icon-move-down" );
		TYPO3.jQuery(id + "-expander").addClass( "t3-icon-move-up" );
	}	
}

function toggleSentDetails(id){
	if ( TYPO3.jQuery(id).is(":visible") ) {
		visible = true;
	} else { 
		visible = false;
	}	
	TYPO3.jQuery(".sent-details").hide("fast");
	TYPO3.jQuery(".sent-details-expander-reset").removeClass( "t3-icon-move-up t3-icon-move-down" );
	TYPO3.jQuery(".sent-details-expander-reset").addClass( "t3-icon-move-down" );
	if (visible) {		
		TYPO3.jQuery(id).hide("fast");
		TYPO3.jQuery(id + "-expander").removeClass( "t3-icon-move-up t3-icon-move-down" );
		TYPO3.jQuery(id + "-expander").addClass( "t3-icon-move-down" );
	} else { 
		TYPO3.jQuery(id).show("fast");
		TYPO3.jQuery(id + "-expander").removeClass( "t3-icon-move-up t3-icon-move-down" );
		TYPO3.jQuery(id + "-expander").addClass( "t3-icon-move-up" );
	}	
}

function toggleTaskDetails(id){
	if ( TYPO3.jQuery(id).is(":visible") ) {
		visible = true;
	} else { 
		visible = false;
	}	
	TYPO3.jQuery(".task-details").hide("fast");
	TYPO3.jQuery(".task-details-expander-reset").removeClass( "t3-icon-move-up t3-icon-move-down" );
	TYPO3.jQuery(".task-details-expander-reset").addClass( "t3-icon-move-down" );
	if (visible) {		
		TYPO3.jQuery(id).hide("fast");
		TYPO3.jQuery(id + "-expander").removeClass( "t3-icon-move-up t3-icon-move-down" );
		TYPO3.jQuery(id + "-expander").addClass( "t3-icon-move-down" );
	} else { 
		TYPO3.jQuery(id).show("fast");
		TYPO3.jQuery(id + "-expander").removeClass( "t3-icon-move-up t3-icon-move-down" );
		TYPO3.jQuery(id + "-expander").addClass( "t3-icon-move-up" );
	}	
}

function updateUserList(){	
	TYPO3.jQuery.ajax({
		url: TYPO3.jQuery( "#getUserListAjaxUrl" ).text(),
		data: '',
		success: function(result) {			
			if(result!=''){				
				TYPO3.jQuery( "#user-list" ).html(result);
			}
		}
	});	
}

function addScheduleSubmit(){	
	TYPO3.jQuery( "#addScheduleForm" ).submit();
}

function reloadIframe(id,src,display,email){	
	TYPO3.jQuery(id).attr('src', src);
	TYPO3.jQuery(id + "-display").html(display);
	if(display=='Alle Benutzer/Adressen'){
		TYPO3.jQuery('#userNav0').hide("fast");
	} else {
		TYPO3.jQuery('#userNav0').show("fast");
	}
	hideUserList();
}

TYPO3.jQuery(document).ready(function() {
	
	TYPO3.jQuery(this).ajaxStart(function(){
		  TYPO3.jQuery("body").append("<div id='tx-moox-mailer-admin-overlay'><img src='/typo3conf/ext/moox_mailer/Resources/Public/Images/ajax-loader.gif' /></div>");
    });
	
	TYPO3.jQuery(this).ajaxStop(function(){
          TYPO3.jQuery("#tx-moox-mailer-admin-overlay").remove();
    });
	
	TYPO3.jQuery("#task-selector").click(function(){
		hideUserList();
		if ( TYPO3.jQuery( "#task-list" ).is(":visible") ) {
			TYPO3.jQuery( "#task-expander" ).removeClass( "t3-icon-move-up t3-icon-move-down" );
			TYPO3.jQuery( "#task-expander" ).addClass( "t3-icon-move-down" );
		} else { 
			TYPO3.jQuery( "#task-expander" ).removeClass( "t3-icon-move-up t3-icon-move-down" );
			TYPO3.jQuery( "#task-expander" ).addClass( "t3-icon-move-up" );
		}		
		TYPO3.jQuery("#task-list").toggle("fast");
	});
	
	TYPO3.jQuery("#task-list-close").click(function(){
		TYPO3.jQuery("#task-list").hide("fast");
	});
	
	TYPO3.jQuery("#user-selector").click(function(){
		hideTaskList();
		if ( TYPO3.jQuery( "#user-list" ).is(":visible") ) {
			TYPO3.jQuery( "#user-expander" ).removeClass( "t3-icon-move-up t3-icon-move-down" );
			TYPO3.jQuery( "#user-expander" ).addClass( "t3-icon-move-down" );
		} else { 
			TYPO3.jQuery( "#user-expander" ).removeClass( "t3-icon-move-up t3-icon-move-down" );
			TYPO3.jQuery( "#user-expander" ).addClass( "t3-icon-move-up" );
		}		
		TYPO3.jQuery("#user-list").toggle("fast");
	});
	
	TYPO3.jQuery("#user-list-close").click(function(){
		TYPO3.jQuery("#user-list").hide("fast");
	});
	
	TYPO3.jQuery(".sent-expander").click(function(event){
		
		parent = TYPO3.jQuery(this).parents(".sent");
		
		if (parent.hasClass("open")){
			visible = true;
		} else { 
			visible = false;
		}
		
		TYPO3.jQuery(".sent").removeClass("open");
		
		if (!visible) {		
			parent.addClass("open");
		}		
	});
	
	TYPO3.jQuery(".draft-expander").click(function(event){
		
		parent = TYPO3.jQuery(this).parents(".draft");
		
		if (parent.hasClass("open")){
			visible = true;
		} else { 
			visible = false;
		}
		
		TYPO3.jQuery(".draft").removeClass("open");
		
		if (!visible) {		
			parent.addClass("open");
		}		
	});
	
	TYPO3.jQuery(".task-expander").click(function(event){
		
		parent = TYPO3.jQuery(this).parents(".task");
		
		if (parent.hasClass("open")){
			visible = true;
		} else { 
			visible = false;
		}
		
		TYPO3.jQuery(".task").removeClass("open");
		
		if (!visible) {		
			parent.addClass("open");
		}		
	});
	

});