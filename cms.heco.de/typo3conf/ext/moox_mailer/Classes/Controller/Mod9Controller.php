<?php
namespace Neulandlotsen\MooxMailer\Controller;

/**
 * This file is part of the "moox_mailer" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

use Neulandlotsen\MooxCore\Utility\Backend\HelperUtility;
use Neulandlotsen\MooxMailer\Domain\Repository\TemplateRepository;
use Neulandlotsen\MooxMailer\Service\HelperService;
use Neulandlotsen\MooxMailer\Domain\Session\BackendSessionHandler;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface;

class Mod9Controller extends \Neulandlotsen\MooxCore\Controller\TemplateController
{
	/**
	 * @var TemplateRepository
	 */
	protected $templateRepository;
	
	/**
	 * @var HelperService
	 */
	protected $helperService;

	/**
	 * @var HelperUtility
	 */
	protected $helperUtility;

	/**
	 * @var BackendSessionHandler
	 */
	protected $backendSession;

	/**
	 * @var string
	 */
	protected $table = 'tx_mooxmailer_domain_model_template';

	/**
	 * @var string
	 */
	protected $sessionStorageKey = 'tx_mooxmailer_mod9';

	/**
	 * @var string
	 */
	protected $controllerClass = 'Neulandlotsen\MooxMailer\Controller\Mod9Controller';

    /**
     * @param TemplateRepository $templateRepository
     */
    public function injectTemplateRepository(TemplateRepository $templateRepository)
    {
        $this->templateRepository = $templateRepository;
    }

    /**
     * @param HelperService $helperService
     */
    public function injectHelperService(HelperService $helperService)
    {
        $this->helperService = $helperService;
    }

    /**
     * @param HelperUtility $helperUtility
     */
    public function injectHelperUtility(HelperUtility $helperUtility)
    {
        $this->helperUtility = $helperUtility;
    }

    /**
     * @param BackendSessionHandler $backendSession
     */
    public function injectBackendSessionHandler(BackendSessionHandler $backendSession)
    {
        $this->backendSession = $backendSession;
    }

	/**
	 * initialize the controller
	 */
	protected function initializeAction()
	{
		// set backend session key
		$this->backendSession->setStorageKey($this->sessionStorageKey);

		$this->helperUtility->setBackendSession($this->backendSession);

		// set filter
		$this->filter = $this->helperUtility->buildFilter($this->controllerClass,$this->settings,"MooxMailer","mxmailer");

		// set ordering
		$this->ordering = $this->helperUtility->buildOrdering($this->controllerClass,$this->settings);

		// initialize storage settings
		$this->initializeStorageSettings();

		// set ext conf
		$this->extConf = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Configuration\ExtensionConfiguration::class)->get(GeneralUtility::camelCaseToLowerCaseUnderscored($this->extensionName));

		// get typoscript configuration
		$this->configuration = $this->configurationManager->getConfiguration(
			ConfigurationManagerInterface::CONFIGURATION_TYPE_FRAMEWORK,
			$this->extensionName
		);

		$this->helperUtility->setAllowedListViewFields(
			 [
				"mod1" => [],
				"mod9" => [
					"uid",
					"title",
					"crdate",
					"tstamp"
				]
			]
		);
	}
}
?>