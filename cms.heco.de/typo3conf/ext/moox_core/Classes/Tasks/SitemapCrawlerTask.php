<?php
namespace Neulandlotsen\MooxCore\Tasks;

/**
 * This file is part of the "moox_core" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

use Neulandlotsen\MooxCore\Utility\HelperUtility;
use TYPO3\CMS\Core\Core\Environment;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Messaging\FlashMessage;
use TYPO3\CMS\Core\Messaging\FlashMessageService;
use TYPO3\CMS\Ex\Service\ClearCacheService;
 
class SitemapCrawlerTask extends \TYPO3\CMS\Scheduler\Task\AbstractTask
{
	/**
	 * @var string
	 */
	public $sitemapUrl;

	/**
	 * @var boolean
	 */
	public $clearCache;

	/**
	 * Get and import news data.
	 *
	 * @return	boolean	Returns TRUE on success, FALSE if no items were indexed or none were found.
	 * @see	typo3/sysext/scheduler/tx_scheduler_Task#execute()
	 */
	public function execute() 
	{
	    if ( basename( Environment::getCurrentScript() ) == 'typo3' )
		{
			$isCli = true;
		}
		else
		{
			$isCli = false;
		}

		$executionSucceeded = true;
		
		$objectManager = GeneralUtility::makeInstance('TYPO3\CMS\Extbase\Object\ObjectManager');
		$cacheService = $objectManager->get('TYPO3\\CMS\\Extbase\\Service\\CacheService');

		if(!$isCli)
		{
			$flashMessageService = $objectManager->get(FlashMessageService::class);
			$flashMessageQueue = $flashMessageService->getMessageQueueByIdentifier();
		}

		// get response data
		$response = HelperUtility::request($this->sitemapUrl);

		$successCnt = 0;
		$errorCnt = 0;

		if($response)
		{
			$response = $this->xmlToArray(new \SimpleXMLElement($response));
			if(isset($response['sitemap']))
			{
				if($response['sitemap']['loc'])
				{
					$loc = $response['sitemap']['loc'];
					$lastmod = $response['sitemap']['lastmod'];
					$response['sitemap'] = [];
					$response['sitemap'][] = ['loc' => $loc, 'lastmod'=>$lastmod];
				}
				if(is_array($response['sitemap']) && count($response['sitemap']))
				{
					if($this->clearCache)
					{
						$cacheService->clearCachesOfRegisteredPageIds();
					}

					foreach ( $response['sitemap'] as $sitemap)
					{
						$sitemapResponse = HelperUtility::request($sitemap['loc']);
						if($sitemapResponse)
						{
							$sitemapResponse = $this->xmlToArray(new \SimpleXMLElement($sitemapResponse));
							if(is_array($sitemapResponse['url']) && count($sitemapResponse['url']))
							{
								foreach ($sitemapResponse['url'] AS $url)
								{
									$pageResponse = HelperUtility::request($url['loc']);
									if($pageResponse)
									{
										$successCnt++;
									}
									else
									{
										$errorCnt++;
									}
								}
							}
						}
						else
						{
							if(!$isCli)
							{
								$message = GeneralUtility::makeInstance('TYPO3\\CMS\\Core\\Messaging\\FlashMessage',
									"Sitemap \"" . $sitemap['loc'] . "\" could not be loaded",
									'', // the header is optional
									FlashMessage::ERROR,
									TRUE
								);
								$flashMessageQueue->addMessage($message);
							}
						}
					}
				}
			}
			if(!$isCli)
			{
				$message = GeneralUtility::makeInstance('TYPO3\\CMS\\Core\\Messaging\\FlashMessage',
					$successCnt . " page(s) crawled successfully, " . $errorCnt . " page(s) could net be loaded.",
					'', // the header is optional
					FlashMessage::OK,
					TRUE
				);
				$flashMessageQueue->addMessage($message);
			}
		}
		else
		{
			if(!$isCli)
			{
				$message = GeneralUtility::makeInstance('TYPO3\\CMS\\Core\\Messaging\\FlashMessage',
					"Sitemap \"" . $this->sitemapUrl . "\" could not be loaded",
					'', // the header is optional
					FlashMessage::ERROR,
					TRUE
				);
				$flashMessageQueue->addMessage($message);
			}

			$executionSucceeded = false;
		}

		return $executionSucceeded;
	}
	
	/**
	 * This method returns the sleep duration as additional information
	 *
	 * @return string Information to display
	 */
	public function getAdditionalInformation()
	{		
		$info = "Sitemap-Url: ".$this->getSitemapUrl().", Clear-Cache: ".$this->getClearCache();

		return $info;
	}

	/**
	 * convert xml to array
	 * @param \SimpleXMLElement $xml
	 * @return array $output
	 */
	public function xmlToArray(\SimpleXMLElement $xml)
	{
		$json = json_encode($xml);
		$output	= json_decode($json,TRUE);
		return $output;
	}

	/**
	 * @return string
	 */
	public function getSitemapUrl()
	{
		return $this->sitemapUrl;
	}

	/**
	 * @param string $sitemapUrl
	 */
	public function setSitemapUrl($sitemapUrl) {
		$this->sitemapUrl = $sitemapUrl;
	}

	/**
	 * @return boolean
	 */
	public function getClearCache()
	{
		return $this->clearCache;
	}

	/**
	 * @param boolean $clearCache
	 */
	public function setClearCache($clearCache)
	{
		$this->clearCache = $clearCache;
	}

}
?>