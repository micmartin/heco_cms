<?php
namespace Neulandlotsen\MooxNews\Domain\Repository;

/**
 * This file is part of the "moox_news" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */
use Neulandlotsen\MooxNews\Domain\Model\DemandInterface;
use Neulandlotsen\MooxNews\Utility\Validation;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Persistence\QueryInterface;

/**
 * Repository for tag objects
 */
class TargetRepository extends \Neulandlotsen\MooxNews\Domain\Repository\AbstractDemandedRepository
{
	protected $defaultOrderings = [
		'title' => QueryInterface::ORDER_ASCENDING
	];
	
    /**
     * Returns an array of constraints created from a given demand object.
     *
     * @param QueryInterface $query
     * @param DemandInterface $demand
	 * @param bool $respectEnableFields
     * @return array<\TYPO3\CMS\Extbase\Persistence\Generic\Qom\ConstraintInterface>
     */
    protected function createConstraintsFromDemand(QueryInterface $query, DemandInterface $demand, $respectEnableFields = true)
    {
        $constraints = [];

        // Storage page
        if ($demand->getStoragePage() != 0)
		{
            $pidList = GeneralUtility::intExplode(',', $demand->getStoragePage(), true);
            $constraints[] = $query->in('pid', $pidList);
        }

        // Targets
        if ($demand->getTargets())
		{
            $tagList = GeneralUtility::intExplode(',', $demand->getTags(), true);
            $constraints[] = $query->in('uid', $tagList);
        }

        // Clean not used constraints
        foreach ($constraints as $key => $value)
		{
            if (is_null($value))
			{
                unset($constraints[$key]);
            }
        }

        return $constraints;
    }

    /**
     * Returns an array of orderings created from a given demand object.
     *
     * @param DemandInterface $demand
     * @return array<\TYPO3\CMS\Extbase\Persistence\Generic\Qom\ConstraintInterface>
     */
    protected function createOrderingsFromDemand(DemandInterface $demand)
    {
        $orderings = [];

        if (Validation::isValidOrdering($demand->getOrder(), $demand->getOrderByAllowed()))
		{
            $orderList = GeneralUtility::trimExplode(',', $demand->getOrder(), true);

            if (!empty($orderList))
			{
                // go through every order statement
                foreach ($orderList as $orderItem)
				{
                    list($orderField, $ascDesc) = GeneralUtility::trimExplode(' ', $orderItem, true);
                    // count == 1 means that no direction is given
                    if ($ascDesc) 
					{
                        $orderings[$orderField] = ((strtolower($ascDesc) == 'desc') ?
                            QueryInterface::ORDER_DESCENDING :
                            QueryInterface::ORDER_ASCENDING);
                    } 
					else 
					{
                        $orderings[$orderField] = QueryInterface::ORDER_ASCENDING;
                    }
                }
            }
        }

        return $orderings;
    }
	
	/**
     * Find targets by a given pid
     *
     * @param int $pid pid
     * @return QueryInterface
     */
    public function findByPid($pid)
    {
        $query = $this->createQuery();
        $query->getQuerySettings()->setRespectStoragePage(false);
		$query->matching(
			$query->equals('pid', (int)$pid)
		);
		$query->setOrderings(
			[
				'title' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING
			]
		);
		return $query->execute();
    }

	/**
	 * Find tags by a given pids
	 *
	 * @param array $pids pids
	 * @return QueryInterface
	 */
	public function findByPids($pids = [])
	{
		$query = $this->createQuery();

		if(!is_array($pids) && $pids)
		{
			$pids = explode(",",$pids);
		}
		elseif(!is_array($pids))
		{
			$pids = [];
		}

		$query->getQuerySettings()->setRespectStoragePage(false);
		$query->matching(
			$query->in('pid', $pids)
		);
		$query->setOrderings(
			[
				'title' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING
			]
		);
		return $query->execute();
	}

	/**
	 * find all targets (overwrite)
	 *	
	 * @param boolean $respectStoragePage
	 * @return array
	 */
	public function findAll($respectStoragePage = TRUE)
	{		
		$query = $this->createQuery();		
		$query->getQuerySettings()->setRespectStoragePage($respectStoragePage);		
		$result = $query->execute();
		return $result;				
	}
}
