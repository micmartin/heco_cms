<?php
namespace Neulandlotsen\MooxNews\Domain\Model;

/**
 * This file is part of the "moox_news" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

/**
 * Demanded repository interface
 */
interface DemandInterface
{
}
