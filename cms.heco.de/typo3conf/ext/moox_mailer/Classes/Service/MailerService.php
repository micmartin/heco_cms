<?php
namespace Neulandlotsen\MooxMailer\Service;

/**
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */

use Neulandlotsen\MooxMailer\Domain\Repository\FrontendUserGroupRepository;
use Neulandlotsen\MooxMailer\Domain\Repository\NewsletterRepository;
use Neulandlotsen\MooxMailer\Domain\Repository\ScheduleRepository;
use Neulandlotsen\MooxMailer\Domain\Repository\TemplateRepository;
use Neulandlotsen\MooxMailer\Domain\Repository\FrontendUserRepository;
use Neulandlotsen\MooxMailer\Domain\Repository\AddressRepository;
use Neulandlotsen\MooxMailer\Domain\Repository\DeliveryRepository;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Database\Query\Restriction\DeletedRestriction;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Utility\RootlineUtility;
use TYPO3\CMS\Extbase\Object\ObjectManager;
use TYPO3\CMS\Frontend\Page\PageRepository;
 
class MailerService implements \TYPO3\CMS\Core\SingletonInterface {
	
	/**
	 * @var ObjectManager
	 */
	protected $objectManager;
	
	/**
	 * @var FrontendUserGroupRepository
	 */
	protected $frontendUserGroupRepository;
	
	/**
	 * @var NewsletterRepository
	 */
	protected $newsletterRepository;
	
	/**
	 * @var ScheduleRepository
	 */
	protected $scheduleRepository;
	
	/**
	 * @var TemplateRepository
	 */
	protected $templateRepository;
	
	/**
	 * @var FrontendUserRepository
	 */
	protected $frontendUserRepository;
	
	/**
	 * @var AddressRepository
	 */
	protected $addressRepository;
	
	/**
	 * @var DeliveryRepository
	 */
	protected $deliveryRepository;
	
	/**
	 * @var PageRepository
	 */
	protected $pageRepository;
	
	/**
	 * @var int
	 */
	protected $storagePage;
	
	/**
	 * @var string
	 */
	protected $dir;
	
	/**
	 * @var string
	 */
	protected $mailDir;
	
	/**
	 * Job-Verzeichnis
	 *
	 * @var string
	 */
	protected $jobDir;
	
	/**
	 * @var string
	 */
	protected $logDir;
	
	/**
	 * @var bool
	 */
	protected $noLog;
	
	/**
	 * @var string
	 */
	protected $countMode;
	
	/**
	 * Mode des Mailers
	 *
	 * @var string
	 */
	protected $mode;
	
	/**
	 * @var int
	 */
	protected $templateUid;
	
	/**
	 * @var int
	 */
	protected $scheduleUid;
	
	/**
	 * Format des Mailers
	 *
	 * @var string
	 */
	protected $format;
	
	/**
	 * subject
	 *
	 * @var string
	 */
	protected $subject;
	
	/**
	 * @var array
	 */
	protected $from;
	
	/**
	 * @var string
	 */
	protected $replyTo;
	
	/**
	 * cc
	 *
	 * @var array
	 */
	protected $cc;
	
	/**
	 * @var array
	 */
	protected $bcc;
	
	/**
	 * @var string
	 */
	protected $description;
	
	/**
	 * Identifier des Mailers
	 *
	 * @var string
	 */
	protected $identifier;
	
	/**
	 * @var bool
	 */
	protected $debug;
	
	/**
	 * @var string
	 */
	protected $debugAddress;
	
	/**
	 * @var int
	 */
	protected $debugMailLimit;
	
	/**
	 * @var int
	 */
	protected $mailCount;
	
	/**
	 * @var int
	 */
	protected $mailsPrepared;
	
	/**
	 * Anzahl bearbeiteter Mails
	 *
	 * @var int
	 */
	protected $mailsProcessed;
	
	/**
	 * @var int
	 */
	protected $mailfilesPrepared;
	
	/**
	 * @var int
	 */
	protected $mailfilesProcessed;
	
	/**
	 * @var bool
	 */
	protected $skipMailAction;
	
	/**
	 * @var int
	 */
	protected $memoryLimit;
	
	/**
	 * script timeout
	 *
	 * @var int
	 */
	protected $scriptTimeout;
	
	/**
	 * @var int
	 */
	protected $timestamp;
	
	/**
	 * @var bool
	 */
	protected $resumed;
	
	/**
	 * @var bool
	 */
	protected $enableMailFilter;
	
	/**
	 * @var array
	 */
	protected $whitelist;
	
	/**
	 * @var array
	 */
	protected $blacklist;
	
	/**
	 * @var array
	 */
	protected $schedule;
	
	/**
	 * @var array
	 */
	protected $extConf;
	
	/**
	 * @var bool
	 */
	protected $isTest;
	
	/**
	 * @var string
	 */
	public static $globalLogfile = "global.log";
	
	/**
	 * @var string
	 */	
	public static $globalErrorsLogfile = "errors.log";
	
	/**
	 * @var string
	 */	
	public static $trackingScript = "track.php";
	
	/**
	 * @var string
	 */	
	public static $trackingImage = "track.gif";
	
	/**
	 * @var string
	 */	
	public static $allowedTasks = array("mooxnewssendtask");

    /**
     * @param ObjectManager $objectManager
     */
    public function injectObjectManager(ObjectManager $objectManager)
    {
        $this->objectManager = $objectManager;
    }

    /**
     * @param FrontendUserGroupRepository $frontendUserGroupRepository
     */
    public function injectFrontendUserGroupRepository(FrontendUserGroupRepository $frontendUserGroupRepository)
    {
        $this->frontendUserGroupRepository = $frontendUserGroupRepository;
    }

    /**
     * @param NewsletterRepository $newsletterRepository
     */
    public function injectNewsletterRepository(NewsletterRepository $newsletterRepository)
    {
        $this->newsletterRepository = $newsletterRepository;
    }

    /**
     * @param ScheduleRepository $scheduleRepository
     */
    public function injectScheduleRepository(ScheduleRepository $scheduleRepository)
    {
        $this->scheduleRepository = $scheduleRepository;
    }

    /**
     * @param TemplateRepository $templateRepository
     */
    public function injectTemplateRepository(TemplateRepository $templateRepository)
    {
        $this->templateRepository = $templateRepository;
    }

    /**
     * @param FrontendUserRepository $frontendUserRepository
     */
    public function injectFrontendUserRepository(FrontendUserRepository $frontendUserRepository)
    {
        $this->frontendUserRepository = $frontendUserRepository;
    }

    /**
     * @param AddressRepository $addressRepository
     */
    public function injectAddressRepository(AddressRepository $addressRepository)
    {
        $this->addressRepository = $addressRepository;
    }

    /**
     * @param DeliveryRepository $deliveryRepository
     */
    public function injectDeliveryRepository(DeliveryRepository $deliveryRepository)
    {
        $this->deliveryRepository = $deliveryRepository;
    }

    /**
     * @param PageRepository $pageRepository
     */
    public function injectPageRepository(PageRepository $pageRepository)
    {
        $this->pageRepository = $pageRepository;
    }
	
	/**
	 * @param int $pageUid page uid
	 * @return array $TSObj->setup
	 */
	public static function getTsSetup($pageUid)
	{
		$pageSelect = GeneralUtility::makeInstance('TYPO3\CMS\Frontend\Page\PageRepository');   
		$pageSelect->init(false);
        $rootLine = GeneralUtility::makeInstance(RootlineUtility::class, $pageUid, '', $pageSelect)->get();
		$templateService = GeneralUtility::makeInstance('TYPO3\CMS\Core\TypoScript\TemplateService');             
        $templateService->tt_track = 0;
        $templateService->init();
        $templateService->runThroughTemplates($rootLine);
        $templateService->generateConfig();		
		
		return $templateService->setup;
	}
	
	/**
	 * @param int $pageUid page uid
	 * @return string $baseURL
	 */
	public static function getBaseUrl($pageUid)
	{
		$config = self::getTsSetup($pageUid)['config.'];

		if($config['baseURL']=="" && $config['mooxMailerBaseUrl']!="")
		{
			$config['baseURL'] = $config['mooxMailerBaseUrl'];
		}

		if($_SERVER['HTTPS']=="on")
		{
			$prefix = "https";
		} 
		else 
		{
			$prefix = "http";
		}
		
		if($config['baseURL']==$prefix."://")
		{ 
			$baseURL = $prefix."://".$_SERVER['SERVER_NAME']."/";
		}  
		elseif($config['baseURL']=="//")
		{ 
			$baseURL = $prefix."://".$_SERVER['SERVER_NAME']."/";
		} 
		elseif($config['baseURL']=="/")
		{ 
			$baseURL = $prefix."://".$_SERVER['SERVER_NAME']."/";
		} 
		else 
		{
			$baseURL = $config['baseURL'];
			if(substr(strtolower($baseURL),0,2)=="//")
			{				
				$baseURL = $prefix.":".$baseURL;
			}			
		} 
		
		if(substr($baseURL,(strlen($baseURL)-1))!="/")
		{ 
			$baseURL = $baseURL."/";
		}
		
		return $baseURL;
	}
	
	/**
	 * @param int $pageUid page uid
	 * @return string $loginRedirectorPid
	 */
	public static function getLoginRedirectorPid($pageUid)
	{
		$TS = self::getTsSetup($pageUid);
        
		return $TS['plugin.']['tx_mooxmailer.']['settings.']['feLoginPid'];
	}
	
	/**
	 * @param string $job
	 * @return array $jobInfo
	 */
	public static function parseMailerJob($job)
	{		
		$jobInfo = [];
		
		if($job!=""){
		
			$jobParts = explode("_",$job);
			
			$jobInfo['tstamp'] = strtotime($jobParts[0]);
			
			if(substr($jobParts[1],0,4)=="task"){
				$jobInfo['type'] 	= "task";
				$jobInfo['uid'] 	= str_replace("task","",$jobParts[1]);
			} elseif(substr($jobParts[1],0,10)=="newsletter"){
				$jobInfo['type'] 	= "newsletter";
				$jobInfo['uid'] 	= str_replace("newsletter","",$jobParts[1]);
			} else {
				$jobInfo['type'] 	= "undefined";
				$jobInfo['uid'] 	= preg_replace('![^0-9]!', '', $jobParts[1]); 
			}	
		}

		return $jobInfo;
	}
	
	/**
	 * initialize mailer
	 *
	 * @param array|object $object object
	 */
	public function initialize($object,$job = '')
	{		
		if(is_object($object)){
			$this->setDir($object->getMailerDir());
		} elseif(is_array($object)) {
			$this->setDir($object['mailDir']);
		}
		
		// Get the extensions's configuration
		$this->extConf = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Configuration\ExtensionConfiguration::class)->get('moox_mailer');
		
		$this->setWhitelist([]);
		$this->setBlacklist([]);
		
		if(!empty($this->extConf['enableMailFilter'])){
			$enableMailFilter = false;
			if($this->extConf['mailWhitelist']!=""){
				$whitelist = explode(";",$this->extConf['mailWhitelist']);
				if(count($whitelist) && trim($whitelist[0])!=""){
					$this->setWhitelist($whitelist);
					$enableMailFilter = true;
				}
			}
			if($this->extConf['mailBlacklist']!=""){
				$blacklist = explode(";",$this->extConf['mailBlacklist']);
				if(count($blacklist) && trim($blacklist[0])!=""){
					$this->setBlacklist($blacklist);
					$enableMailFilter = true;
				}
			}
			if($enableMailFilter){
				$this->setEnableMailFilter(true);
			}
		}
		
		if(!($object instanceof \Neulandlotsen\MooxMailer\Tasks\DaemonSendTask))
		{
			if(is_object($object))
			{
				if($object->getMode()=="daemon")
				{
					$this->setMode("daemon");
				}
				else
				{
					$this->setMode("direct");
				}
				
				if($object->getScheduleUid()>0)
				{
					$schedule = $this->loadSchedule($object->getScheduleUid());
					if(count($schedule))
					{
						$this->setSchedule($schedule);
						$this->setScheduleUid($object->getScheduleUid());
					}
				}
				
				$this->setTemplateUid($object->getTemplateUid());
				
				if($object->getFormat()=="text/html")
				{
					$this->setFormat("text/html");
				}
				else
				{
					$this->setFormat("text/plain");
				}

				if($object->getCc()!="")
				{
					$cc = explode("\r\n",$object->getCc());
					if(is_array($cc) && count($cc)>0)
					{
						$this->setCc($cc);
					}
				}

				if($object->getBcc()!="")
				{
					$bcc = explode("\r\n", $object->getBcc());
					if (is_array($bcc) && count($bcc) > 0)
					{
						$this->setBcc($bcc);
					}
				}
				
				if($object->getCountMode()=="jobfile")
				{
					$this->setCountMode("jobfile");
				}
				else
				{
					$this->setCountMode("live");
				}
				
				if($object->getMemoryLimit()<8){
					$this->setMemoryLimit("64M");
				} else {
					$this->setMemoryLimit($object->getMemoryLimit()."M");
				}
				
				if($object->getScriptTimeout()<30){
					$this->setScriptTimeout(30);
				} else {
					$this->setScriptTimeout($object->getScriptTimeout());
				}
				
				if($object->getSubject()!=""){
					$this->setSubject($object->getSubject());
				} else {
					$this->setSubject("Kein Betreff / No subject");
				}
				
				$this->setDebug($object->getDebug());		
				if($this->getDebug()){
					$this->setDebugAddress($object->getDebugAddress());
					$this->setDebugMailLimit($object->getDebugMailLimit());			
				}
				
				$this->setNoLog($object->getNoLog());
				
				$this->setTimestamp($object->getTimestamp());
			
			} elseif(is_array($object)) {
				
				if($object['mode']=="daemon"){
					$this->setMode("daemon");
				} else {
					$this->setMode("direct");
				}
				
				if($object['format']=="text/html"){
					$this->setFormat("text/html");
				} else {
					$this->setFormat("text/plain");
				}
				
				if($object['countMode']=="jobfile"){
					$this->setCountMode("jobfile");
				} else {
					$this->setCountMode("live");
				}
				
				if($object['memoryLimit']<8){
					$this->setMemoryLimit("64M");
				} else {
					$this->setMemoryLimit($object['memoryLimit']."M");
				}
				
				if($object['scriptTimeout']<30){
					$this->setScriptTimeout(30);
				} else {
					$this->setScriptTimeout($object['scriptTimeout']);
				}
								
				$this->setNoLog($object['noLog']);
				
				$this->setTimestamp($object['timestamp']);
				
			}
						
		}
		else
		{
			$this->setMode("daemon");
		}
		
		if($job!="")
		{
			$jobparts = explode("_",$job);			
		}
		else
		{
			$jobparts = [];
		}
		
		// if given job exists
		if(count($jobparts)>=2 && file_exists($this->getJobDir().$job)){
			
			$this->setMailfilesPrepared([]);
			$this->setMailfilesProcessed([]);
				
			$this->setIdentifier($jobparts[0]);
			unset($jobparts[0]);			
			$this->setDescription(implode("_",$jobparts));			
			
			// get current job counters
			$this->updateCounter();
			
			// update list of mailfiles in job
			$this->updateMailfiles();
			
			if($this->getMailCount()<1){
				$this->setMailCount(0);
			}
			
			$this->setResumed(true);
			
		// create a new mailer/job instance
		} else {
		
			if(!($object instanceof \Neulandlotsen\MooxMailer\Tasks\DaemonSendTask)){
				
				if(is_object($object)){			
				
					$this->setIdentifier(date("YmdHis"));
					$this->setDescription('task'.$object->getTaskUid());
					$this->setMailCount(0);
					
					if($object->getReplyToAddress()!=""){
						$this->setReplyTo($object->getReplyToAddress());
					}
					if($object->getSenderAddress()!=""){
						
						$from = [];
						$from['address'] = $object->getSenderAddress();
						if($object->getSenderName()!=""){
							$from['name'] = $object->getSenderName();
						}
						$this->setFrom($from);								
						$this->setSkipMailAction(false);
					
					} else {
						
						// skip mail actions if no sender is defined
						$this->setSkipMailAction(true);
					}
					
				} elseif(is_array($object)) {
					
					$this->setIdentifier(date("YmdHis"));
					$this->setDescription('newsletter'.$object['uid']);
					$this->setMailCount(0);
					
					if($object['replyToAddress']!=""){
						$this->setReplyTo($object['replyToAddress']);
					}
					if($object['senderAddress']!=""){
						
						$from = [];
						$from['address'] = $object['senderAddress'];
						if($object['senderName']!=""){
							$from['name'] = $object['senderName'];
						}
						$this->setFrom($from);								
						$this->setSkipMailAction(false);
					
					} else {
						
						// skip mail actions if no sender is defined
						$this->setSkipMailAction(true);
					}
				}
				
				$this->setResumed(false);
			}
		}
	}
	
	/**
	 * convert simple xml element to array
	 *
	 * @param \SimpleXMLElement $xml xml
	 * @param array $array
	 * @return void
	 */
	static function xmlToArray(\SimpleXMLElement $xml)
	{        
		$json = json_encode($xml);		
        return json_decode($json,TRUE);
    } 
	
	/**
	 * write mail as xml file to mail folder
	 *
	 * @param array $maildata maildata
	 * @return void
	 */
	public function writeMailAsXML($maildata)
	{		
		// increase mail counter for current mailer job
		$this->mailCount++;
		
		// set default values for prepared mail
		$maildata['job'] = $this->identifier."_".$this->description;
		$maildata['state'] = "prepared";
		$maildata['created'] = date("Y-m-d H:i:s");
		$maildata['processed'] = "false";
		$maildata['id']	= $this->mailCount;
		$maildata['error']['info'] = "false";
		$maildata['error']['type'] = "false";
		
		if($maildata['format']==""){
			$maildata['format']	= "text/plain";
		}
				
		// do check minimum requirements for mail generation
		if($maildata['job']!="" && $maildata['to']['address']!="" && $maildata['subject']!="" && $maildata['body']!=""){
		
			// initialize new DOM Document
			$xml = new \DOMDocument('1.0','UTF-8');
			
			// make xml output more readable
			$xml->formatOutput = true;
			
			// root element
			$mail = $xml->createElement('mail');
			$mail = $xml->appendChild($mail);
				
				// job identifier
				$mail_job = $xml->createAttribute ('job');
				$mail_job = $mail->appendChild ($mail_job);
				
					$mail_job_val = $xml->createTextNode ($maildata['job']);
					$mail_job_val = $mail_job->appendChild ($mail_job_val);
					
				if($maildata['jobType']!=""){
					// job type
					$mail_job_type = $xml->createAttribute ('jobType');
					$mail_job_type = $mail->appendChild ($mail_job_type);
					
						$mail_job_type_val = $xml->createTextNode ($maildata['jobType']);
						$mail_job_type_val = $mail_job_type->appendChild ($mail_job_type_val);
				}
				
				// mail id within mail job
				$mail_id = $xml->createAttribute ('id');
				$mail_id = $mail->appendChild ($mail_id);
				
					$mail_id_val = $xml->createTextNode ($maildata['id']);
					$mail_id_val = $mail_id->appendChild ($mail_id_val);
				
				// mail format
				$mail_format = $xml->createAttribute ('format');
				$mail_format = $mail->appendChild ($mail_format);
				
					$mail_format_val = $xml->createTextNode ($maildata['format']);
					$mail_format_val = $mail_format->appendChild ($mail_format_val);
				
				// mail state (prepared,sent,error)
				$mail_state = $xml->createAttribute ('state');
				$mail_state = $mail->appendChild ($mail_state);
				
					$mail_state_val = $xml->createTextNode ($maildata['state']);
					$mail_state_val = $mail_state->appendChild ($mail_state_val);
				
				// creation datetime
				$mail_created = $xml->createAttribute ('created');
				$mail_created = $mail->appendChild ($mail_created);
				
					$mail_created_val = $xml->createTextNode ($maildata['created']);
					$mail_created_val = $mail_created->appendChild ($mail_created_val);
				
				// datetime of last process
				$mail_processed = $xml->createAttribute ('processed');
				$mail_processed = $mail->appendChild ($mail_processed);
				
					$mail_processed_val = $xml->createTextNode ($maildata['processed']);
					$mail_processed_val = $mail_processed->appendChild ($mail_processed_val);
				
				// mail error
				$mail_error = $xml->createElement('error');
				$mail_error = $mail->appendChild($mail_error);
					
					// error info
					$mail_error_info = $xml->createElement('info');
					$mail_error_info = $mail_error->appendChild($mail_error_info);
					
						$mail_error_info_val = $xml->createTextNode($maildata['error']['info']);
						$mail_error_info_val = $mail_error_info->appendChild($mail_error_info_val);
					
					// error type (permanent,temporarily)
					$mail_error_type = $xml->createElement('type');
					$mail_error_type = $mail_error->appendChild($mail_error_type);
					
						$mail_error_type_val = $xml->createTextNode($maildata['error']['type']);
						$mail_error_type_val = $mail_error_type->appendChild($mail_error_type_val);
				
				// mail from address/name
				if(isset($maildata['from']) && is_array($maildata['from']) && $maildata['from']['address']!=""){
					
					$mail_from = $xml->createElement('from');
					$mail_from = $mail->appendChild($mail_from);
						
					if(trim($maildata['from']['name'])!=""){	
					
						$mail_from_name = $xml->createElement('name');
						$mail_from_name = $mail_from->appendChild($mail_from_name);
						
							$mail_from_name_val = $xml->createTextNode(trim($maildata['from']['name']));
							$mail_from_name_val = $mail_from_name->appendChild($mail_from_name_val);
					}
							
						$mail_from_address = $xml->createElement('address');
						$mail_from_address = $mail_from->appendChild($mail_from_address);
						
							$mail_from_address_val = $xml->createTextNode($maildata['from']['address']);
							$mail_from_address_val = $mail_from_address->appendChild($mail_from_address_val);
				}
				
				// mail to address/name			
				$mail_to = $xml->createElement('to');
				$mail_to = $mail->appendChild($mail_to);
					
					if($maildata['to']['uid']!=""){
						// mail to uid
						$mail_to_uid = $xml->createAttribute ('uid');
						$mail_to_uid = $mail_to->appendChild ($mail_to_uid);
					
							$mail_to_uid_val = $xml->createTextNode ($maildata['to']['uid']);
							$mail_to_uid_val = $mail_to_uid->appendChild ($mail_to_uid_val);
					}
					
					if($maildata['to']['type']!=""){
						// mail to type
						$mail_to_type = $xml->createAttribute ('type');
						$mail_to_type = $mail_to->appendChild ($mail_to_type);
					
							$mail_to_type_val = $xml->createTextNode ($maildata['to']['type']);
							$mail_to_type_val = $mail_to_type->appendChild ($mail_to_type_val);
					}
					
					if($maildata['to']['hash']!=""){
						// mail to hash
						$mail_to_hash = $xml->createAttribute ('hash');
						$mail_to_hash = $mail_to->appendChild ($mail_to_hash);
					
							$mail_to_hash_val = $xml->createTextNode ($maildata['to']['hash']);
							$mail_to_hash_val = $mail_to_hash->appendChild ($mail_to_hash_val);
					}
					
				if(trim($maildata['to']['name'])!=""){	
					
					$mail_to_name = $xml->createElement('name');
					$mail_to_name = $mail_to->appendChild($mail_to_name);
					
						$mail_to_name_val = $xml->createTextNode(trim($maildata['to']['name']));
						$mail_to_name_val = $mail_to_name->appendChild($mail_to_name_val);
				}
						
					$mail_to_address = $xml->createElement('address');
					$mail_to_address = $mail_to->appendChild($mail_to_address);
					
						$mail_to_address_val = $xml->createTextNode($maildata['to']['address']);
						$mail_to_address_val = $mail_to_address->appendChild($mail_to_address_val);
				
				// mail reply-to address/name
				if(isset($maildata['reply-to']) && is_array($maildata['reply-to']) && $maildata['reply-to']['address']!=""){
					
					$mail_reply_to = $xml->createElement('reply-to');
					$mail_reply_to = $mail->appendChild($mail_reply_to);
					
					if($maildata['reply-to']['name']!=""){
					
						$mail_reply_to_name = $xml->createElement('name');
						$mail_reply_to_name = $mail_reply_to->appendChild($mail_reply_to_name);
						
							$mail_reply_to_name_val = $xml->createTextNode($maildata['reply-to']['name']);
							$mail_reply_to_name_val = $mail_reply_to_name->appendChild($mail_reply_to_name_val);
					}
										
					$mail_reply_to_address = $xml->createElement('address');
					$mail_reply_to_address = $mail_reply_to->appendChild($mail_reply_to_address);
						
						$mail_reply_to_address_val = $xml->createTextNode($maildata['reply-to']['address']);
						$mail_reply_to_address_val = $mail_reply_to_address->appendChild($mail_reply_to_address_val);				
				}
				
				// mail cc addresses
				if(isset($maildata['cc']) && is_array($maildata['cc']) && count($maildata['cc'])>0){
					
					$mail_cc = $xml->createElement('cc');
					$mail_cc = $mail->appendChild($mail_cc);
					
					foreach($maildata['cc'] AS $address){
					
						$mail_cc_address = $xml->createElement('address');
						$mail_cc_address = $mail_cc->appendChild($mail_cc_address);
						
							$mail_cc_address_val = $xml->createTextNode($address);
							$mail_cc_address_val = $mail_cc_address->appendChild($mail_cc_address_val);
					}
				}
				
				// mail bcc addresses
				if(isset($maildata['bcc']) && is_array($maildata['bcc']) && count($maildata['bcc'])>0){
					
					$mail_bcc = $xml->createElement('bcc');
					$mail_bcc = $mail->appendChild($mail_bcc);
					
					foreach($maildata['bcc'] AS $address){
					
						$mail_bcc_address = $xml->createElement('address');
						$mail_bcc_address = $mail_bcc->appendChild($mail_bcc_address);
						
							$mail_bcc_address_val = $xml->createTextNode($address);
							$mail_bcc_address_val = $mail_bcc_address->appendChild($mail_bcc_address_val);
					}
				}
				
				// mail errors-to address
				if($maildata['return-path']!=""){
					
					$mail_return_path = $xml->createElement('return-path');
					$mail_return_path = $mail->appendChild($mail_return_path);
						
						$mail_return_path_val = $xml->createTextNode($maildata['return-path']);
						$mail_return_path_val = $mail_return_path->appendChild($mail_return_path_val);
				}

				// mail attachments
				if(isset($maildata['attachments']) && is_array($maildata['attachments'])){

					$mail_attachments = $xml->createElement('attachments');
					$mail_attachments = $mail->appendChild($mail_attachments);

					foreach($maildata['attachments'] AS $attachment){
						$mail_attachment = $xml->createElement('attachment');

						if($attachment['type']!="")
						{
							$mail_attachment_type = $xml->createAttribute ('type');
							$mail_attachment_type = $mail_attachment->appendChild ($mail_attachment_type);

							$mail_attachment_type_val = $xml->createTextNode ($attachment['type']);
							$mail_attachment_type_val = $mail_attachment_type->appendChild ($mail_attachment_type_val);
						}

						if($attachment['uid']>0)
						{
							$mail_attachment_uid = $xml->createAttribute ('uid');
							$mail_attachment_uid = $mail_attachment->appendChild ($mail_attachment_uid);

							$mail_attachment_uid_val = $xml->createTextNode ($attachment['uid']);
							$mail_attachment_uid_val = $mail_attachment_uid->appendChild ($mail_attachment_uid_val);
						}

						if($attachment['mime']!="")
						{
							$mail_attachment_mime_type = $xml->createAttribute ('mimeType');
							$mail_attachment_mime_type = $mail_attachment->appendChild ($mail_attachment_mime_type);

							$mail_attachment_mime_type_val = $xml->createTextNode ($attachment['mime']);
							$mail_attachment_mime_type_val = $mail_attachment_mime_type->appendChild ($mail_attachment_mime_type_val);
						}

						if($attachment['name']!="")
						{
							$mail_attachment_name = $xml->createAttribute ('name');
							$mail_attachment_name = $mail_attachment->appendChild ($mail_attachment_name);

							$mail_attachment_name_val = $xml->createTextNode ($attachment['name']);
							$mail_attachment_name_val = $mail_attachment_name->appendChild ($mail_attachment_name_val);
						}

						$mail_attachment_val = $xml->createTextNode($attachment['path']);
						$mail_attachment_val = $mail_attachment->appendChild($mail_attachment_val);

						$mail_attachment = $mail_attachments->appendChild($mail_attachment);
					}
				}
				
				// mail subject
				$mail_subject = $xml->createElement('subject');
				$mail_subject = $mail->appendChild($mail_subject);
					
					$mail_subject_val = $xml->createCDATASection($maildata['subject']);
					$mail_subject_val = $mail_subject->appendChild($mail_subject_val);
				
				// mail body
				$mail_body = $xml->createElement('body');
				$mail_body = $mail->appendChild($mail_body);
					
					$mail_body_val = $xml->createCDATASection($maildata['body']);
					$mail_body_val = $mail_body->appendChild($mail_body_val);
			
			// build name for mail xml file
			$mailfile = $maildata['job']."_".str_pad($maildata['id'],6,"0",STR_PAD_LEFT).".xml";
			
			// save mail xml file
			if($xml->save($this->getMailDir().$mailfile)){
				$this->log("MAILER: Mail successfully prepared ".$mailfile." (to: ".$maildata['to']['address'].")");
			} else {
				$this->log("ERROR: mail not prepared ".$mailfile."(to: ".$maildata['to']['address'].")");	
				$this->errorlog("ERROR: mail not prepared ".$mailfile."(to: ".$maildata['to']['address'].")");
			}
		}
	}
	
	/**
	 * read mail info from xml file and return as array
	 *
	 * @param string $mailfile mailfile
	 * @return array $maildata
	 */
	public function readMailFromXML($mailfile)
	{		
		$maildata = [];
		
		if(file_exists($this->getMailDir().$mailfile))
		{
			$xml = simplexml_load_file($this->getMailDir().$mailfile,'SimpleXMLElement', LIBXML_NOCDATA);

			if($xml)
			{
				$maildata['file'] = $mailfile;

				$maildata = array_merge(
					current($xml->attributes()),
					$maildata
				);

				$maildata['error'] = MailerService::xmlToArray($xml->error);
				$maildata['from'] = MailerService::xmlToArray($xml->from);
				$maildata['to'] = array_merge(
					current($xml->to->attributes()),
					MailerService::xmlToArray($xml->to->children())
				);
				if($xml->{'reply-to'})
				{
					$maildata['reply-to'] = MailerService::xmlToArray($xml->{'reply-to'});
				}
				if($xml->cc)
				{
					$maildata['cc'] = MailerService::xmlToArray($xml->cc);
				}
				if($xml->bcc)
				{
					$maildata['bcc'] = MailerService::xmlToArray($xml->bcc);
				}
				$maildata['subject'] = (string)$xml->subject;
				$maildata['body'] = (string)$xml->body;
				if($xml->attachments)
				{
					$maildata['attachments'] = [];
					foreach($xml->attachments->children() AS $_attachment)
					{
						$attachment = current($_attachment->attributes());
						$attachment['path'] = (string)$_attachment;
						$maildata['attachments'][] = $attachment;
					}
				}

			}
			else
			{
				$maildata['parseError'] = true;
			}
		} 
		
		return $maildata;
	}
		
	/**
	 * update mail xml file
	 *
	 * @param string $mailfile mailfile
	 * @param array $maildata maildata
	 * @return void
	 */
	public function updateMailXML($mailfile,$maildata = [])
	{		
		// check if passed mailfile exists
		if(file_exists($this->getMailDir().$mailfile)){
			
			// load xml file
			$xml = simplexml_load_file($this->getMailDir().$mailfile);   
			
			// set value
			if(isset($maildata['state']) &&  $maildata['state']!=""){
				$xml['state'] = $maildata['state'];
			}
			if(isset($maildata['processed']) &&  $maildata['processed']!=""){
				$xml['processed'] = $maildata['processed'];
			}
			if(isset($maildata['error']['info']) &&  $maildata['error']['info']!=""){
				$xml->error->info = $maildata['error']['info'];
			}
			if(isset($maildata['error']['type']) &&  $maildata['error']['type']!=""){
				$xml->error->type = $maildata['error']['type'];
			}
			
			// save changes to xml file
			$xml->asXML($this->getMailDir().$mailfile);
		} 
	}
		
	/**
	 * save job file
	 *
	 * @return void
	 */
	public function saveJob()
	{		
		if(!file_exists($this->getJobDir().$this->identifier."_".$this->description)){
			
			// create job file in job folder
			file_put_contents($this->getJobDir().$this->identifier."_".$this->description,$this->mailCount."\n",FILE_APPEND | LOCK_EX);
						
			$this->log("MAILER: Job ".$this->identifier."_".$this->description." successfully prepared (".$this->mailCount." mail(s) prepared)");
			$this->log("MAILER: Job ".$this->identifier."_".$this->description." successfully prepared (".$this->mailCount." mail(s) prepared)","global");
		}
	}
		
	/**
	 * check if current job is finished
	 *
	 * @return void
	 */
	public function jobFinished()
	{		
		$this->updateCounter(true);
		
		if($this->mailsPrepared<1){
			return true;
		} else {
			return false;
		}
	}

	/**
	 * finish job
	 *
	 * @return void
	 */
	public function finishJob()
	{		
		if($this->mode=="daemon"){
		
			// delete jobfile from queue
			if(file_exists($this->getJobDir().$this->identifier."_".$this->description)){
				unlink($this->getJobDir().$this->identifier."_".$this->description);
			}

			$this->log("MAILER: Job ".$this->identifier."_".$this->description." successfully finished (".$this->mailCount." mail(s) processed)");
			$this->log("MAILER: Job ".$this->identifier."_".$this->description." successfully finished (".$this->mailCount." mail(s) processed)","global");

		}
		
		// mark job logfile as processed
		if(file_exists($this->getLogDir().$this->identifier."_".$this->description.".log")){
			rename($this->getLogDir().$this->identifier."_".$this->description.".log",$this->getLogDir()."__".$this->identifier."_".$this->description.".log");
		}
		if(file_exists($this->getLogDir().$this->identifier."_".$this->description.".content")){
			rename($this->getLogDir().$this->identifier."_".$this->description.".content",$this->getLogDir()."__".$this->identifier."_".$this->description.".content");
		}
	}
	
	/**
	 * update mail counter
	 *
	 * @param bool $forceLiveUpdate force live update
	 * @return void
	 */
	public function updateCounter($forceLiveUpdate = false)
	{		
		$job = $this->identifier."_".$this->description;
		
		if($this->countMode=="jobfile" && !$forceLiveUpdate){
			$this->setMailCount((int)trim(file_get_contents($this->jobdir.$job)));
		} else {
				
			$prepared = 0;
			$processed = 0;
				
			$handle = opendir($this->getMailDir());
		
			while ($file = readdir ($handle)) {
				if(!in_array($file,array(".",".."))){
					if(substr(strtolower($file),0,strlen($job))==strtolower($job)){
						$prepared++;
					}
					if(substr(strtolower($file),0,strlen("__".$job))=="__".strtolower($job)){
						$processed++;
					}
				}
			}
				
			$this->setMailCount($prepared+$processed);
			$this->setMailsProcessed($processed);
			$this->setMailsPrepared($prepared);		   
		}
	}
		
	/**
	 * update mail files of job
	 *	
	 * @return void
	 */
	public function updateMailfiles()
	{		
		$job = $this->identifier."_".$this->description;
		
		$handle = opendir($this->getMailDir());
		
		while ($file = readdir ($handle)) {
			
			$mailfile = [];
			
			if(!in_array($file,array(".",".."))){
				if(substr(strtolower($file),0,strlen($job))==strtolower($job)){				
					$this->mailfilesPrepared[] 		= $file;
				} elseif(substr(strtolower($file),0,strlen("__".$job))=="__".strtolower($job)){					
					$this->mailfilesProcessed[] 	= $file;
				}
			}
			
			unset($mailfile);
		}
		
		sort($this->mailfilesPrepared);
		sort($this->mailfilesProcessed);
		 	
	}
			
	/**
	 * log job info
	 *
	 * @param string $logtext logtext
	 * @param string $target target
	 * @return void
	 */
	public function log($logtext,$target = "self")
	{		
		$logdate = "[".date("Y-m-d H:i:s")."] ";
		
		if(!$this->nolog){
			if($target == "self"){
				file_put_contents($this->getLogDir().$this->identifier."_".$this->description.".log",$logdate.$logtext."\n",FILE_APPEND | LOCK_EX);
			} elseif($target == "content"){
				file_put_contents($this->getLogDir().$this->identifier."_".$this->description.".content",$logtext."\n",FILE_APPEND | LOCK_EX);
			} else {
				file_put_contents($this->getLogDir().MailerService::$globalLogfile,$logdate.$logtext."\n",FILE_APPEND | LOCK_EX);
			}
		}
    } 
	
	/**
	 * read log
	 *	
	 * @param string $target target
	 * @return void
	 */
	public function readLog($file,$target = "self")
	{		
		if($file != ""){
			if(file_exists($this->getLogDir().$file)){
				return nl2br(file_get_contents($this->getLogDir().$file));
			} else {
				return "No logfile written yet";
			}
		} elseif($target == "self"){
			return nl2br(file_get_contents($this->getLogDir().$this->identifier."_".$this->description.".log"));
		} elseif($target == "content"){
			return nl2br(file_get_contents($this->getLogDir().$this->identifier."_".$this->description.".content"));
		} else {
			return nl2br(file_get_contents($this->getLogDir().MailerService::$globalLogfile,$logdate.$logtext."\n"));
		}		
    } 
		
	/**
	 * log job error
	 *
	 * @param string $logtext logtext
	 * @return void
	 */
	public function errorlog($logtext)
	{		
		$logdate = "[".date("Y-m-d H:i:s")."] ";
		
		if(!$this->nolog){
			file_put_contents($this->getLogDir().MailerService::$globalErrorsLogfile,$logdate.$logtext."\n",FILE_APPEND | LOCK_EX);
		}
    } 
		
	/**
	 * reset time limit to given value (max_execution_time)
	 *
	 * @return void
	 */
	public function resetTimeout()
	{
        set_time_limit($this->scriptTimeout);
    }
		
	/**
	 * raise memory_limit to given value
	 *
	 * @return void
	 */
    private function raiseMemoryLimit()
	{
        ini_set('memory_limit', $this->memoryLimit);
    }
		
	/**
	 * mark mailfile as processed
	 *
	 * @param string $mailfile mailfile
	 * @return void
	 */
	public function markMailfileAsProcessed($mailfile)
	{
		rename($this->getMailDir().$mailfile,$this->getMailDir()."__".$mailfile);
	}
	
	/**
	 * mark mailfile as not parseable
	 *
	 * @param string $mailfile mailfile
	 * @return void
	 */	
	public function markMailfileAsNotParseable($mailfile)
	{
		rename($this->getMailDir().$mailfile,$this->getMailDir()."__PARSE_ERROR__".$mailfile);
	}
	
	/**
	 * check email address
	 *
	 * @param string $email email
	 * @return void
	 */	
	public static function checkMail($email)
	{		
		$error = false;
		$errormsg = "";
		
		/* old check
		if (!preg_match( '/^([a-z0-9]+([-_\.]?[a-z0-9])+)@[a-z0-9���]+([-_\.]?[a-z0-9���])+\.[a-z]{2,4}$/i', $email)) {		
			$errormsg .= "Address in mailbox given [".$email."] does not comply with RFC 2822, 3.6.2";
			return $errormsg;	
		}
		*/
		if(!GeneralUtility::validEmail($email)){
			$error 	= true;
			$errormsg .= "Address in mailbox given [".$email."] does not comply with RFC 3696 (http://tools.ietf.org/html/rfc3696)";
			return $errormsg;
		}

		if (!function_exists('checkdnsrr')) {
			
			function checkdnsrr($host, $type) {
				
				@exec('nslookup -type=' . $type . ' ' . $host, $output);

				foreach ($output as $line){
					if (preg_match('/^' . $host . '/i', $line)) {
						return true;
					}
				}

				return false;
			}
		}

		$host = substr(strrchr($email, '@'), 1);

		if (($checkMX = checkdnsrr($host, 'MX')) or ($checkA = checkdnsrr($host, 'A'))) {
			return $errormsg;
		} else {
			
			if(!$checkMX){
				$errormsg .= "MX-Record check failed";
			}
			if(!$checkA){
				$errormsg .= (($errormsg!="")?", ":"")."A-Record check failed";
			} 
				
			return $errormsg;
		}
	}
		
	/**
	 * send mail / prepare mail for mailing
	 *
	 * @return array $state
	 */
    public function sendMail($maildata,$daemonSend = false)
	{        
		global $TYPO3_CONF_VARS;
		
		if($this->extConf['useSMTP']){
			$TYPO3_CONF_VARS['MAIL']['transport'] = "smtp";
			if($this->extConf['smtpEncrypt']!="" && $this->extConf['smtpEncrypt']!="none"){
				$TYPO3_CONF_VARS['MAIL']['transport_smtp_encrypt'] = $this->extConf['smtpEncrypt'];
			}
			$TYPO3_CONF_VARS['MAIL']['transport_smtp_server'] = $this->extConf['smtpServer'];
			$TYPO3_CONF_VARS['MAIL']['transport_smtp_username'] = $this->extConf['smtpUsername'];
			$TYPO3_CONF_VARS['MAIL']['transport_smtp_password'] = $this->extConf['smtpPassword'];
		}
		
		$successCnt = 0;
		$prepareCnt = 0;
		$errorCnt = 0;		
		
		if($this->skipMailActions)
		{
			return;
		}
		
		if(!is_string($maildata['to']['name']))
		{
			unset($maildata['to']['name']);
		}
		if(!is_string($maildata['from']['name']))
		{
			unset($maildata['from']['name']);
		}
		if(!isset($maildata['from']['address']) || $maildata['from']['address']=="")
		{
			$maildata['from']['address'] = $this->from['address'];
		}		
		if(!isset($maildata['from']['name']) || $maildata['from']['name']=="")
		{
			$maildata['from']['name'] = $this->from['name'];
		}
		if(!isset($maildata['reply-to']['address']) || $maildata['reply-to']['address']=="")
		{
			$maildata['reply-to']['address'] = $this->replyTo;
		}		
		if($this->debug && $this->debugAddress!="")
		{
			$maildata['to']['address'] = $this->debugAddress;
		}	
		if($maildata['format']=="")
		{
			$maildata['format'] = $this->format;
		}		
		if(!is_array($maildata['cc']) && is_array($this->cc))
		{
			$maildata['cc'] = $this->cc;
		}		
		if(!is_array($maildata['bcc']) && is_array($this->bcc))
		{
			$maildata['bcc'] = $this->bcc;
		}		
		if($maildata['subject']=="")
		{
			$maildata['subject'] = $this->subject;
		}
		if($maildata['job']=="")
		{
			$maildata['job'] = $this->identifier."_".$this->description;
		}		
		
		$logtext = "";

		if($this->mode=="daemon" && !$daemonSend)
		{
			$maildata['return-path'] = $this->generateReturnToPrefix($maildata);						
			
			if($this->extConf['useMailerTracking'] && !$maildata['isTest'])
			{				
				$maildata['body'] = $this->addLinkTracking($maildata['body'],$maildata['job'],dechex($maildata['to']['uid'])."_".$maildata['to']['hash'],$maildata['baseUrl']);
				$maildata['body'] = $this->addTrackingPixel($maildata['body'],$maildata['baseUrl'],$maildata['job'],dechex($maildata['to']['uid'])."_".$maildata['to']['hash']);				
			}
			
			if($this->extConf['useMailerTracking'] && !$maildata['isTest'])
			{				
				$this->createDelivery($maildata,"prepared");				
			}
			
			$this->writeMailAsXML($maildata);

			$prepareCnt++;
			
		}
		else
		{
			if($this->extConf['useMailerTracking'] && !$maildata['isTest'] && $this->mode!="daemon")
			{
				$maildata['body'] = $this->addLinkTracking($maildata['body'],$maildata['job'],dechex($maildata['to']['uid'])."_".$maildata['to']['hash'],$maildata['baseUrl']);
				$maildata['body'] = $this->addTrackingPixel($maildata['body'],$maildata['baseUrl'],$maildata['job'],dechex($maildata['to']['uid'])."_".$maildata['to']['hash']);				
			}
			
			if($maildata['return-path']!="")
			{
				$maildata['return-path'] = str_replace("_sent","_sent_".date("YmdHis"),$maildata['return-path']);
			}
			
			if($maildata['from']['name']=="")
			{
				$maildata['from']['name'] = $maildata['from']['address'];
			}
			
			if($maildata['to']['name']=="")
			{
				$maildata['to']['name'] = $maildata['to']['address'];
			}
			
			if($this->debug && $this->debugAddress!="")
			{
				$logtext .= "DEBUG [redirected to: ". $this->debugAddress."]: "; 
			}
			
			if($daemonSend)
			{
				$logtextMode = "DAEMON";						
			}
			else
			{
				$logtextMode = "DIRECT";
			}
            
			try
			{
				if($this->enableMailFilter && !$this->debug)
				{
					$doSent 	= true;
					$isSent 	= false;
					$reason 	= "";
					
					$inWhitelist 	= false;
					$hasWhitelist	= false;
					$inBlacklist 	= false;					
					
					if(count($this->whitelist))
					{
						$hasWhitelist = true;
						foreach($this->whitelist AS $pattern)
						{
							if(strpos($maildata['to']['address'],$pattern)!==FALSE)
							{
								$inWhitelist = true;
							}
						}
					}
					
					if(false && count($this->blacklist))
					{
						foreach($this->blacklist AS $pattern)
						{
							if(strpos($maildata['to']['address'],$pattern)!==FALSE)
							{
								$inBlacklist = true;
							}
						}
					}					
					
					if($hasWhitelist && !$inWhitelist)
					{
						$reason = "skipped because whitelist";
						$doSent = false;						
					}					
					
				}
				else
				{
					$doSent = true;
				}
				
				if($doSent)
				{																
					if(($errormsg = $this->checkMail(($this->debug && $this->debugAddress!="")?$this->debugAddress:$maildata['to']['address']))=="")
					{							
						$mail = GeneralUtility::makeInstance('TYPO3\CMS\Core\Mail\MailMessage');

						$mail->setFrom([$maildata['from']['address'] => $maildata['from']['name']]);
						$mail->setTo([($this->debug && $this->debugAddress!="")?$this->debugAddress:$maildata['to']['address'] => $maildata['to']['name']]);
						
						if(!is_array($maildata['cc']) && $maildata['cc']!="")
						{
							$maildata['cc'] = [$maildata['cc']];
						}
						if(!$maildata['cc'])
						{
							$maildata['cc'] = [];
						}				
						foreach($maildata['cc'] AS $cc)
						{
							$mail->addCc($cc);
						}				
						
						if(!is_array($maildata['bcc']) && $maildata['bcc']!="")
						{
							$maildata['bcc'] = [$maildata['bcc']];
						}
						if(!$maildata['bcc'])
						{
							$maildata['bcc'] = [];
						}
						foreach($maildata['bcc'] AS $bcc)
						{
							$mail->addBcc($bcc);
						}
										
						if($maildata['subject']!="")
						{
							$mail->setSubject($maildata['subject']);
						}
						else
						{
							$mail->setSubject($this->subject);
						}
						
						if($maildata['reply-to']['address']!="")
						{
							$mail->setReplyTo($maildata['reply-to']['address']);
						} 
						
						if($maildata['return-path']!="") {
							$mail->setReturnPath($maildata['return-path']);
						}
						
						$mail->setBody(strip_tags($maildata['body']));
						
						if($maildata['format']=="text/html")
						{
							$mail->addPart($maildata['body'], 'text/html');
						}

						if(is_array($maildata['attachments']))
						{
							foreach($maildata['attachments'] AS $attachment)
							{
								$attachmentToAttach = \Swift_Attachment::fromPath($attachment['path']);
								if($attachment['name']!="")
								{
									$attachmentToAttach->setFilename($attachment['name']);
								}
								if($attachment['mimeType']!="")
								{
									$attachmentToAttach->setContentType($attachment['mimeType']);
								}

								// Attach it to the message
								$mail->attach($attachmentToAttach);
							}
						}

						
						$isSent = $mail->send();					
						
						if($daemonSend)
						{
							$logtextFile = " (".$maildata['file'].")";
						}
						else
						{
							$logtextFile = "";
						}
							
						if($isSent)
						{
							$logtext .= "MAILER [".$logtextMode."]: Mail".$logtextFile." sent successfully (to: ".$maildata['to']['address'].")";
							$successCnt++;
							$isSent = true;
						}
						else
						{
							$logtext = "MAILER [".$logtextMode."]: ERROR - Mail not sent! Undefined error [Address: ".(($this->debug && $this->debugAddress!="")?$this->debugAddress:$maildata['to']['address'])."] ";
							$this->errorlog($logtext);
							if(!$maildata['isTest'])
							{
								$this->createError($maildata,'Undefined error');
							}
							$isSent = false;
							$errorCnt++;
						}
					}
					else
					{
						$logtext = "MAILER [".$logtextMode."]: ERROR - Mail not sent! ".$errormsg;
						$this->errorlog($logtext);
						if(!$maildata['isTest'])
						{
							$this->createError($maildata,$errormsg);
						}
						$isSent = false;
						$errorCnt++;
					}
				}
				else
				{
					// TODO log skipped by whitelist/blacklist to newsletter/mailing
					$logtext .= "MAILER [".$logtextMode."]: Mail ".$reason." (to: ".$maildata['to']['address'].")";
					$successCnt++;
				}

			}
			catch (Exception $e)
			{
				$errormsg = $e->getMessage(); 
				$logtext = "MAILER [".$logtextMode."]: ERROR - ".$errormsg;
				$this->errorlog($logtext);
				$isSent = false;
				$errorCnt++;
			}
			
			$this->log($logtext);
			
			if($daemonSend)
			{
				$maildata['processed']		= date("Y-m-d H:i:s");
				
				if($errormsg!="")
				{
					$maildata['error']['info'] = $errormsg;
					$maildata['error']['type'] = "permanent";
					$maildata['state'] = "error";
				}
				else
				{
					$maildata['state'] = "sent";
				}
				
				$this->updateMailXML($maildata['file'],$maildata);
			}
			
			if($this->extConf['useMailerTracking'] && $daemonSend && !$maildata['isTest'])
			{
				$this->createDelivery($maildata,"processed",$isSent);
			}
			elseif($this->extConf['useMailerTracking'] && !$maildata['isTest'])
			{
				$this->createDelivery($maildata,"sent",$isSent);
			}
			
			$this->resetTimeout();
		}
			
		return array (
			"successCnt" => $successCnt,
			"prepareCnt" => $prepareCnt,
			"errorCnt" 	 => $errorCnt,
			"errorText"  => $errormsg
		);		
    }
		
	/**
	 * process mailer job
	 *
	 * @param array $preparedMaildata prepared maildata
	 * @return array $state
	 */
	public function process($preparedMaildata = [])
	{
		opcache_reset();

		$successCnt = 0;
		$errorCnt 	= 0;
		$prepareCnt = 0;

        $this->raiseMemoryLimit();

		if($preparedMaildata["loadFromDB"])
		{
			$loadFromDB = true;
			unset($preparedMaildata);				
		}

		if(!$loadFromDB && (!is_array($preparedMaildata)))
		{
			$preparedMaildata = $this->prepareMaildata();
		}

		if($this->getIsTest())
		{
			$logPrefix = "[Test] ";
		}
		if($loadFromDB && false)
		{
			$jobsFromDB = $GLOBALS['TYPO3_DB']->exec_SELECTquery('uid,data', 'tx_mooxmailer_domain_model_job', 'job="'.$this->identifier.'_'.$this->description.'"');
			if($GLOBALS['TYPO3_DB']->sql_num_rows($jobsFromDB)>0)
			{
				if($this->mode=="daemon")
				{
					$this->log("MAILER: ".$logPrefix."Preparation of job ".$this->identifier."_".$this->description." started...");
					$this->log("MAILER: ".$logPrefix."Preparation of job ".$this->identifier."_".$this->description." started...","global");
				}
				else
				{
					$this->log("MAILER: Processing of job ".$this->identifier."_".$this->description." started...");
					$this->log("MAILER: Processing of job ".$this->identifier."_".$this->description." started...","global");
				}

				$executionCnt = 0;
				while ($jobFromDB = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($jobsFromDB))
				{
					if($this->debug && $this->debugAddress!="" && $executionCnt>=$this->debugMailLimit)
					{
						break;
					}
					$counter 	= $this->sendMail(unserialize($jobFromDB['data']));
					$prepareCnt = $prepareCnt + $counter['prepareCnt'];
					$successCnt = $successCnt + $counter['successCnt'];
					$errorCnt 	= $errorCnt + $counter['errorCnt'];

					$executionCnt++;
				}
				
				//$deletJobsFromDB = $GLOBALS['TYPO3_DB']->exec_DELETEquery('tx_mooxmailer_domain_model_job', 'job="'.$this->identifier.'_'.$this->description.'"');
				
				if($this->mode=="daemon")
				{
					if(!$this->debug)
					{
						$this->saveJob();
					}
				}
				else
				{
					$this->log("MAILER: Job ".$this->identifier."_".$this->description." successfully finished (".$successCnt." mail(s) sent, ".$errorCnt." error(s))");
					$this->log("MAILER: Job ".$this->identifier."_".$this->description." successfully finished (".$successCnt." mail(s) sent, ".$errorCnt." error(s))","global");
					
					$this->finishJob();
				}
				
			}
			else
			{
				$this->log("MAILER: ".$logPrefix."Nothing to process, no pages/news to send (".$this->description.")...","global");			
			}			
		}
		else
		{
			if(count($preparedMaildata)>0){
				
				if($this->mode=="daemon")
				{
					$this->log("MAILER: ".$logPrefix."Preparation of job ".$this->identifier."_".$this->description." started...");
					$this->log("MAILER: ".$logPrefix."Preparation of job ".$this->identifier."_".$this->description." started...","global");
				}
				else
				{
					$this->log("MAILER: Processing of job ".$this->identifier."_".$this->description." started...");
					$this->log("MAILER: Processing of job ".$this->identifier."_".$this->description." started...","global");
				}

				$executionCnt = 0;
				foreach ($preparedMaildata AS $maildata)
				{
					if($this->debug && $this->debugAddress!="" && $executionCnt>=$this->debugMailLimit)
					{
						break;
					}
					$counter 	= $this->sendMail($maildata);
					$prepareCnt = $prepareCnt + $counter['prepareCnt'];
					$successCnt = $successCnt + $counter['successCnt'];
					$errorCnt 	= $errorCnt + $counter['errorCnt'];

					$executionCnt++;
				}
				
				if($this->mode=="daemon")
				{
					if(!$this->debug)
					{
						$this->saveJob();
					}
				}
				else
				{
					$this->log("MAILER: Job ".$this->identifier."_".$this->description." successfully finished (".$successCnt." mail(s) sent, ".$errorCnt." error(s))");
					$this->log("MAILER: Job ".$this->identifier."_".$this->description." successfully finished (".$successCnt." mail(s) sent, ".$errorCnt." error(s))","global");
					
					$this->finishJob();
				}
			}
			else
			{
				$this->log("MAILER: ".$logPrefix."Nothing to process, no pages/news to send (".$this->description.")...","global");
			}
		}
		
		return array (
			"successCnt" 	=> $successCnt,						
			"errorCnt" 		=> $errorCnt,
			"processedCnt" 	=> ($successCnt+$errorCnt),
			"preparedCnt" 	=> $prepareCnt
		);
	}
		
	/**
	 * simulate process and collect preview data
	 *
	 * @return array $previewdata
	 */
	public function preview()
	{			
		$previewdata = [];
		
		$previewdata['valid'] = 0;
		$previewdata['validAddresses'] = [];
		$previewdata['sent'] = 0;
		$previewdata['prepared'] = 0;
		$previewdata['processed'] = 0;
		$previewdata['errors'] = 0;
		$previewdata['invalidAddresses'] = [];
		
		$previewdata['identifier'] = $this->identifier;
		$previewdata['description'] = $this->description;
		$previewdata['mode'] = $this->mode;				
		
		$this->raiseMemoryLimit();

		if($this->resumed){
			
			$previewdata['prepared'] = $this->mailsPrepared;
			$previewdata['processed'] = $this->mailsProcessed;			
			
		} else {
		
			$preparedMaildata = $this->prepareMaildata();
			
			$previewdata['affectedNewsUids'] = $this->affectedNewsUids;
			$previewdata['affectedNews'] = $this->affectedNews;
			
			$prepareCnt = count($preparedMaildata);
			
			$previewdata['mode'] = $this->mode;
			
			if($this->mode = "daemon"){
				$previewdata['prepared'] = $prepareCnt;				
			} else {
				$previewdata['sent'] = $prepareCnt;
			}
			
			foreach ($preparedMaildata AS $maildata) {
				
				if(($errormsg = $this->checkMail($maildata['to']['address']))!=""){
					
					if(!isset($previewdata['invalidAddresses'][$maildata['to']['address']])){
						$error = [];					
						$error['address'] = $maildata['to']['address'];
						$error['name'] = $maildata['to']['name'];
						$error['message'] = ($errormsg!="")?$errormsg:"unknown";
						$error['count']	= 1;
						$previewdata['invalidAddresses'][$maildata['to']['address']] = $error;
					} else {
						$previewdata['invalidAddresses'][$maildata['to']['address']]['count'] += 1;
					}
										
				} else {
					
					if(!isset($previewdata['validAddresses'][$maildata['to']['address']])){
						$valid = [];					
						$valid['address'] = $maildata['to']['address'];
						$valid['name'] = $maildata['to']['name'];
						$valid['count']	= 1;						
						$previewdata['validAddresses'][$valid['address']] = $valid;
					} else {
						$previewdata['validAddresses'][$maildata['to']['address']]['count'] += 1;
					}
				}
			}
			
			$previewdata['valid'] = count($previewdata['validAddresses']);
			$previewdata['errors'] = count($previewdata['invalidAddresses']);
		}	

		return $previewdata;
	}
	
	/**
	 * dummy!!! code is implemented in child classes
	 *
	 * @return array $maildata
	 */	
	public function prepareMaildata()
	{		
		return [];
	}
	
	/**
	 * Get array of folders with fe-users	
	 *	
	 * @return	array	folders with fe-users	
	 */
	public function getFeUsersFolders()
	{		
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable('pages');

        $queryBuilder
            ->getRestrictions()
            ->removeAll()
            ->add(GeneralUtility::makeInstance(DeletedRestriction::class));

        $query = $queryBuilder
            ->select('*')
            ->from('pages')
            ->where(
                $queryBuilder->expr()->andx(
                    $queryBuilder->expr()->in('doktype', $queryBuilder->createNamedParameter(1, \PDO::PARAM_INT), $queryBuilder->createNamedParameter(254, \PDO::PARAM_INT)),
                    $queryBuilder->expr()->eq('module', $queryBuilder->createNamedParameter('mxfeuser', \PDO::PARAM_STR))
                )
            )
            ->execute();

        $pages = $query->fetch();

		return $pages;		
	}	
	
	/**
	 * Get array of folders with moox-addresses	
	 *	
	 * @return	array	folders with moox-addresses	
	 */
	public function getMooxAddressFolders()
	{		
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable('pages');

        $queryBuilder
            ->getRestrictions()
            ->removeAll()
            ->add(GeneralUtility::makeInstance(DeletedRestriction::class));

        $query = $queryBuilder
            ->select('*')
            ->from('pages')
            ->where(
                $queryBuilder->expr()->andx(
                    $queryBuilder->expr()->in('doktype', $queryBuilder->createNamedParameter(1, \PDO::PARAM_INT), $queryBuilder->createNamedParameter(254, \PDO::PARAM_INT)),
                    $queryBuilder->expr()->eq('module', $queryBuilder->createNamedParameter('mxaddress', \PDO::PARAM_STR))
                )
            )
            ->execute();

        $pages = $query->fetch();

		return $pages;		
	}
	
	/**
	 * get fe user groups (for given pid(list))
	 *
	 * @return \TYPO3\CMS\Extbase\Persistence\QueryInterface $groups
	 */
	public function getFeUsersGroups($pids = [])
	{	
		$pids = (is_array($pids))?$pids:array($pids);
		
		return $this->frontendUserGroupRepository->findByPids($pids);
	}
	
	/**
	 * get templates
	 *
	 * @return array|object $templates
	 */
	public function getTemplates()
	{		
		return $this->templateRepository->findAll(false);
	}
	
	/**
	 * get schedules
	 *
	 * @return array|object $schedules
	 */
	public function getSchedules()
	{		
		return $this->scheduleRepository->findAll(false);
	}
	
	/**
	 * load schedule
	 *
	 * @param int $uid
	 * @return array|object $schedule
	 */
	public function loadSchedule($uid = 0)
	{		
		$schedule = [];
		
		if($uid>0){
		
			$object = $this->scheduleRepository->findByUid($uid);
			
			if($object->getSchedule()!=""){
				$schedule['uid'] = $object->getUid();
				$schedule['title'] = $object->getTitle();
				$schedule['schedule'] = unserialize($object->getSchedule());
				if($schedule['schedule'][date("Y")][(int)date("W")]>0){
					$schedule['template'] = $this->loadTemplate($schedule['schedule'][date("Y")][(int)date("W")]);
				}
			}
		}
		
		return $schedule;
	}
	
	/**
	 * load template
	 *
	 * @param int $uid
	 * @return array|object $template
	 */
	public function loadTemplate($uid = 0)
	{		
		$template = [];
		
		if($uid>0){
		
			$object = $this->templateRepository->findByUid($uid);
			
			if($object->getTemplate()!=""){
				$template['uid'] = $object->getUid();
				$template['title'] = $object->getTitle();
				$template['subject'] = $object->getSubject();
				$template['senderAddress'] = $object->getSenderAddress();
				$template['senderName'] = $object->getSenderName();
				$template['replyToAddress'] = $object->getReplyToAddress();
				$template['template'] = $object->getTemplate();				
			}
		}
		
		return $template;
	}
	
	/**
	 * generate return to 
	 *
	 * @param array $maildata
	 * @return string $returnTo
	 */
	public function generateReturnToPrefix($maildata)
	{		
		$returnTo = "";
		
		if($this->extConf['useBounceHandling']){
			
			if($this->extConf['bounceDomain']!="" && in_array($maildata['bounceInfo']['receiverType'],array("feuser","address"))){
				
				$returnTo .= $this->identifier."_".$this->description;
				
				if($maildata['bounceInfo']['task'] == "mooxnewssendtask"){
					$returnTo .= "_1";
				} elseif($maildata['bounceInfo']['task'] == "newsletter"){
					$returnTo .= "_2";
				} 
				
				if($maildata['bounceInfo']['receiverType'] == "feuser"){
					$returnTo .= "_1";
				} elseif($maildata['bounceInfo']['receiverType'] == "address"){
					$returnTo .= "_2";
				}
				
				$returnTo .= "_".$maildata['bounceInfo']['receiverUid'];
				
				$returnTo .= "_sent";
				
				$returnTo .= "@".$this->extConf['bounceDomain'];
				
			} elseif($this->extConf['defaultBounceEmail']!=""){
				
				$returnTo = $this->extConf['defaultBounceEmail'];
			}
		}
		
		return $returnTo;
		
	}
	
	/**
	 * add tracking to mail links 
	 *
	 * @param string $body
	 * @param string $job
	 * @param string $identifier
	 * @param string $baseUrl
	 * @return string $body
	 */
	public function addLinkTracking($body,$job,$identifier,$baseUrl)
	{			
		$this->addLinkTrackingJob 			= $job;
		$this->addLinkTrackingIdentifier 	= $identifier;
		
		if(substr($baseUrl, -1, 1)=="/"){
			$this->addLinkTrackingBaseUrl = substr($baseUrl, 0, -1);
		} else {
			$this->addLinkTrackingBaseUrl = $baseUrl;
		}
		
		return preg_replace_callback('/<a([^>]*)>/', function ($match) {	
			
			$allowedAttributes = array("style","class","id","name","href");
			$doc = new \DOMDocument();			
			@$doc->loadHTML("<html><body>".$match[0]."link</a></body></html>");
			$xml = simplexml_import_dom($doc);
			$link = $xml->body->a;			
			if(isset($link['rewrite']) && in_array(strtolower($link['rewrite']),array("false","0","none","skip"))){
				return $match[0];
			} elseif(substr(strtolower($link['href']),0,4)=="http" && $this->addLinkTrackingIdentifier!="" && $this->addLinkTrackingJob!="" && $this->addLinkTrackingBaseUrl!="") {				
				if(strpos($link['href'],"//")!==false){
					$sep 	= strpos($link['href'],"/",(strpos($link['href'],"//")+2));
					$domain = substr($link['href'],0,$sep);
					$path 	= substr($link['href'],$sep+1);					
					$attributes = "";
					$attrArray = $link->attributes();
					foreach($link->attributes() AS $key => $value){
						if(in_array(strtolower($key),$allowedAttributes)){
							if(strtolower($key) == "href" && $value!="" && $value!="#"){															
								if(substr(strtolower($value),0,strlen($this->addLinkTrackingBaseUrl))==strtolower($this->addLinkTrackingBaseUrl)){									
									//$value = str_replace($domain.'/','',$value);
									if(substr(strtolower(str_replace($domain.'/','',$value)),0,9)=='index.php'){
										$target = "system";										
									} else {
										$target = "internal";										
									}
									
								} else {
									if(substr(strtolower($value),0,4)=='http'){										
										$target = "external";									
									} else {
										$target = "undefined";
									}
								}								
								$value = urlencode(str_replace("/","~",base64_encode($value)));								
								$href = "";
								$href .= ' href="'.$this->addLinkTrackingBaseUrl;			
								if($link['id'] !=""){
									//$href .= 'id='.urlencode($link['id']).'&';
									$href .= '/'.urlencode($link['id']);
								} else {
									$href .= '/default';
								}
								//$href .= 'job='.$this->addLinkTrackingJob.'&';
								$href .= '/'.$this->addLinkTrackingJob;
								//$href .= 'hash='.$this->addLinkTrackingIdentifier.'&';
								$href .= '/'.$this->addLinkTrackingIdentifier;
								//$href .= 'url='.urlencode(str_replace($domain.'/','',$value)).'"';
								$href .= '/'.$target;	
								$href .= '/'.$value;														
								$href .= '/'.MailerService::$trackingScript;
								//$href .= '/'.urlencode(str_replace($domain.'/','',$value));
								$attributes .= $href.'"';
								
							} else {
								$attributes .= ' '.$key.'="'.$value.'"';
							}			
						}
					}
					$attributes .= ' target="_blank"';
					return '<a'.$attributes.'>';
				} else {
					return $match[0];
				}
			} elseif(substr(strtolower($link['href']),0,6)=="mailto" && $this->addLinkTrackingIdentifier!="" && $this->addLinkTrackingJob!="") {				
				$attributes = "";
				$attrArray = $link->attributes();
				foreach($link->attributes() AS $key => $value){
					if(in_array(strtolower($key),$allowedAttributes)){
						if(strtolower($key) == "href" && $value!="" && $value!="#"){															
							$target = "email";						
							$value = urlencode(str_replace("/","~",base64_encode($value)));								
							$href = "";
							$href .= ' href="'.$this->addLinkTrackingBaseUrl;			
							if($link['id'] !=""){
								$href .= '/'.urlencode($link['id']);
							} else {
								$href .= '/default';
							}
							$href .= '/'.$this->addLinkTrackingJob;
							$href .= '/'.$this->addLinkTrackingIdentifier;
							$href .= '/'.$target;	
							$href .= '/'.$value;														
							$href .= '/'.MailerService::$trackingScript;
							$attributes .= $href.'"';
							
						} else {
							$attributes .= ' '.$key.'="'.$value.'"';
						}			
					}
				}
				$attributes .= ' target="_blank"';
				return '<a'.$attributes.'>';				
			} else {
				return $match[0];
			}
		}, $body);
	}
	
	/**
	 * add tracking pixel 
	 *
	 * @param string $body
	 * @param string $domain
	 * @param string $job
	 * @param string $identifier
	 * @return string $body
	 */
	public function addTrackingPixel($body,$domain,$job,$identifier)
	{
		if($domain!="" && $job!="" && $identifier!=""){
			return str_replace('</body>','<img src="'.$domain.$job.'/'.$identifier.'/'.MailerService::$trackingImage.'" style="display:none;width:1px;height:1px" /></body>',$body);
		} else {
			return $body;
		}
	}
	
	/**
	 * create delivery entry 
	 *
	 * @param array $maildata
	 * @param string $action
	 * @param bool $isSent
	 */
	public function createDelivery($maildata,$action,$isSent = false)
	{		
		if(is_array($maildata)){
		
			$time = time();
		
			if($action!="processed"){
				
				$delivery = [];
				
				if($maildata['to']['type']=="feuser"){
                    $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable('fe_users');
                    $queryBuilder
                        ->getRestrictions()
                        ->removeAll()
                        ->add(GeneralUtility::makeInstance(DeletedRestriction::class));

                    $query = $queryBuilder
                        ->select('uid')
                        ->from('fe_users')
                        ->where(
                            $queryBuilder->expr()->eq('uid', $queryBuilder->createNamedParameter($maildata['to']['uid'], \PDO::PARAM_INT))
                        )
                        ->setMaxResults(1)
                        ->execute();

					$feUser = $query->fetch();
					if($feUser['uid']>0)
					{
						$delivery['fe_user'] = $feUser['uid'];
					}
				} elseif($maildata['to']['type']=="address")
                {
                    $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable('tx_mooxaddress_domain_model_address');
                    $queryBuilder
                        ->getRestrictions()
                        ->removeAll()
                        ->add(GeneralUtility::makeInstance(DeletedRestriction::class));

                    $query = $queryBuilder
                        ->select('uid')
                        ->from('tx_mooxaddress_domain_model_address')
                        ->where(
                            $queryBuilder->expr()->eq('uid', $queryBuilder->createNamedParameter($maildata['to']['uid'], \PDO::PARAM_INT))
                        )
                        ->setMaxResults(1)
                        ->execute();

					$address = $query->fetch();
					if($address['uid']>0)
					{
						$delivery['address'] = $address['uid'];
					}					
				}
				
				$delivery['title'] 	= $maildata['job'].' to '.$maildata['to']['type'].' ['.$maildata['to']['uid'].']';
				$delivery['job'] 	= $maildata['job'];
					
				if($maildata['jobType']=="newsletter")
				{
					$delivery['job_type'] = "newsletter";					
				} elseif(in_array($maildata['jobType'],self::$allowedTasks))
                {
					$delivery['job_type'] = $maildata['jobType'];					
				} else
				    {
					$delivery['job_type'] = "undefined";					
				}
				
				if($action=="sent")
				{
					if($isSent)
					{
						$delivery['sent'] = 1;						
					} else
					    {
						$delivery['failed'] = 1;	
						if($maildata['jobType']=="newsletter")
						{
							$jobInfo = self::parseMailerJob($maildata['job']);

                            $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable('tx_mooxmailer_domain_model_newsletter');
                            $queryBuilder
                                ->getRestrictions()
                                ->removeAll()
                                ->add(GeneralUtility::makeInstance(DeletedRestriction::class));

                            $query = $queryBuilder
                                ->select('uid,fails')
                                ->from('tx_mooxmailer_domain_model_newsletter')
                                ->where(
                                    $queryBuilder->expr()->eq('uid', $queryBuilder->createNamedParameter($jobInfo['uid'], \PDO::PARAM_INT))
                                )
                                ->setMaxResults(1)
                                ->execute();

							$newsletter = $query->fetch();
							if($newsletter['uid']>0)
							{
								$newsletterUpdate = $GLOBALS['TYPO3_DB']->exec_UPDATEquery(
									'tx_mooxmailer_domain_model_newsletter',
									'deleted=0 AND uid='.$jobInfo['uid'],
									array(
										"fails" => $newsletter['fails']+1,
										"tstamp" => $time
									)
								);	
							}
						}
					}
				}				
				
				if($this->storagePage>0)
				{
					$delivery['pid'] 	= $this->storagePage;
				}
				$delivery['cruser_id'] 	= $GLOBALS["BE_USER"]->user["uid"];
				$delivery['crdate'] 	= $time;
				$delivery['tstamp'] 	= $time;
				
				$insertDelivery = $GLOBALS['TYPO3_DB']->exec_INSERTquery(
					'tx_mooxmailer_domain_model_delivery',
					$delivery
				);
				
				unset($delivery);
								
			} 
			else
			{
				if($maildata['to']['type']=="feuser")
				{
                    $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable('tx_mooxmailer_domain_model_delivery');
                    $queryBuilder
                        ->getRestrictions()
                        ->removeAll()
                        ->add(GeneralUtility::makeInstance(DeletedRestriction::class));

                    $query = $queryBuilder
                        ->select('uid')
                        ->from('tx_mooxmailer_domain_model_delivery')
                        ->where(
                            $queryBuilder->expr()->andx(
                                $queryBuilder->expr()->eq('job', $queryBuilder->createNamedParameter($maildata['job'], \PDO::PARAM_STR)),
                                $queryBuilder->expr()->eq('fe_user', $queryBuilder->createNamedParameter($maildata['to']['uid'], \PDO::PARAM_INT))
                            )
                        )
                        ->setMaxResults(1)
                        ->execute();

					$delivery = $query->fetch();
				} 
				elseif($maildata['to']['type']=="address")
                {
                    $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable('tx_mooxmailer_domain_model_delivery');
                    $queryBuilder
                        ->getRestrictions()
                        ->removeAll()
                        ->add(GeneralUtility::makeInstance(DeletedRestriction::class));

                    $query = $queryBuilder
                        ->select('uid')
                        ->from('tx_mooxmailer_domain_model_delivery')
                        ->where(
                            $queryBuilder->expr()->andx(
                                $queryBuilder->expr()->eq('job', $queryBuilder->createNamedParameter($maildata['job'], \PDO::PARAM_STR)),
                                $queryBuilder->expr()->eq('address', $queryBuilder->createNamedParameter($maildata['to']['uid'], \PDO::PARAM_INT))
                            )
                        )
                        ->setMaxResults(1)
                        ->execute();

                    $delivery = $query->fetch();
				}
				
				if($delivery['uid']>0)
				{
					if($isSent)
					{
						$delivery['sent'] = 1;						
					} 
					else
					{
						$delivery['failed'] = 1;
						if($maildata['jobType']=="newsletter")
						{
							$jobInfo = self::parseMailerJob($maildata['job']);

                            $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable('tx_mooxmailer_domain_model_newsletter');
                            $queryBuilder
                                ->getRestrictions()
                                ->removeAll()
                                ->add(GeneralUtility::makeInstance(DeletedRestriction::class));

                            $query = $queryBuilder
                                ->select('uid,fails')
                                ->from('tx_mooxmailer_domain_model_newsletter')
                                ->where(
                                    $queryBuilder->expr()->eq('uid', $queryBuilder->createNamedParameter($jobInfo['uid'], \PDO::PARAM_INT))
                                )
                                ->setMaxResults(1)
                                ->execute();

							$newsletter = $query->fetch();
							if($newsletter['uid']>0)
							{
								$newsletterUpdate = $GLOBALS['TYPO3_DB']->exec_UPDATEquery(
									'tx_mooxmailer_domain_model_newsletter',
									'deleted=0 AND uid='.$jobInfo['uid'],
									array(
										"fails" => $newsletter['fails']+1,
										"tstamp" => $time
									)
								);	
							}
						}
					}
					
					$delivery['tstamp'] = $time;
					
					$updateUid = $delivery['uid'];
					
					unset($delivery['uid']);
					
					$deliveryUpdate = $GLOBALS['TYPO3_DB']->exec_UPDATEquery(
									'tx_mooxmailer_domain_model_delivery',
									'deleted=0 AND uid='.$updateUid,
									$delivery
								);
					
					unset($delivery);
				}				
			}						
		}		
	}
	
	/**
	 * create error entry 
	 *
	 * @param array $maildata
	 * @param string $errormsg
	 */
	public function createError($maildata,$errormsg="")
	{		
		if(is_array($maildata) && $errormsg!=""){
		
			$time = time();
		
			$error = [];
				
			if($maildata['to']['type']=="feuser")
			{
                $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable('fe_users');
                $queryBuilder
                    ->getRestrictions()
                    ->removeAll()
                    ->add(GeneralUtility::makeInstance(DeletedRestriction::class));

                $query = $queryBuilder
                    ->select('uid')
                    ->from('fe_users')
                    ->where(
                        $queryBuilder->expr()->eq('uid', $queryBuilder->createNamedParameter($maildata['to']['uid'], \PDO::PARAM_INT))
                    )
                    ->setMaxResults(1)
                    ->execute();

				$feUser = $query->fetch();
				if($feUser['uid']>0)
				{
					$error['fe_user'] = $feUser['uid'];
					$feUserUpdate = $GLOBALS['TYPO3_DB']->exec_UPDATEquery(
						'fe_users',
						'deleted=0 AND uid='.$feUser['uid'],
						array(
							"quality" => 2,
							"tstamp" => time()
						)
					);
				}
			} 
			elseif($maildata['to']['type']=="address")
            {
                $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable('tx_mooxaddress_domain_model_address');
                $queryBuilder
                    ->getRestrictions()
                    ->removeAll()
                    ->add(GeneralUtility::makeInstance(DeletedRestriction::class));

                $query = $queryBuilder
                    ->select('uid')
                    ->from('tx_mooxaddress_domain_model_address')
                    ->where(
                        $queryBuilder->expr()->eq('uid', $queryBuilder->createNamedParameter($maildata['to']['uid'], \PDO::PARAM_INT))
                    )
                    ->setMaxResults(1)
                    ->execute();

				$address = $query->fetch();
				if($address['uid']>0)
				{
					$error['address'] = $address['uid'];
					$addressUpdate = $GLOBALS['TYPO3_DB']->exec_UPDATEquery(
						'tx_mooxaddress_domain_model_address',
						'deleted=0 AND uid='.$address['uid'],
						[
							"quality" => 2,
							"tstamp" => time()
						]
					);
				}					
			}
				
			$error['title'] = $errormsg;
			$error['email'] = $maildata['to']['address'];
			$error['job'] 	= $maildata['job'];
					
			if($maildata['jobType']=="newsletter"){
				$error['job_type'] = "newsletter";					
			} 
			elseif(in_array($maildata['jobType'],self::$allowedTasks))
			{
				$error['job_type'] = $maildata['jobType'];					
			} 
			else 
			{
				$error['job_type'] = "undefined";					
			}
				
			if($this->storagePage>0)
			{
				$error['pid'] 	= $this->storagePage;
			}
			$error['cruser_id'] = $GLOBALS["BE_USER"]->user["uid"];
			$error['crdate'] 	= $time;
			$error['tstamp'] 	= $time;
			
			$insertError = $GLOBALS['TYPO3_DB']->exec_INSERTquery(
				'tx_mooxmailer_domain_model_error',
				$error
			);
			
			$error['uid'] = $GLOBALS['TYPO3_DB']->sql_insert_id();
			
			if($error['uid']>0)
			{
				if($error['fe_user']>0)
				{
					$insertErrorFeUserMM = $GLOBALS['TYPO3_DB']->exec_INSERTquery(
						'tx_mooxmailer_error_feuser_mm',
						[	
							"uid_local" => $error['uid'], 
							"uid_foreign" => $error['fe_user']
						]
					);
				}
				if($error['address']>0)
				{
					$insertErrorFeUserMM = $GLOBALS['TYPO3_DB']->exec_INSERTquery(
						'tx_mooxmailer_error_address_mm',
						[	
							"uid_local" => $error['uid'], 
							"uid_foreign" => $error['address']
						]
					);
				}				
			}
			
			if(!$this->extConf['useMailerTracking'])
			{
				if($maildata['jobType']=="newsletter")
				{
					$jobInfo = self::parseMailerJob($maildata['job']);

                    $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable('tx_mooxmailer_domain_model_newsletter');
                    $queryBuilder
                        ->getRestrictions()
                        ->removeAll()
                        ->add(GeneralUtility::makeInstance(DeletedRestriction::class));

                    $query = $queryBuilder
                        ->select('uid,fails')
                        ->from('tx_mooxmailer_domain_model_newsletter')
                        ->where(
                            $queryBuilder->expr()->eq('uid', $queryBuilder->createNamedParameter($jobInfo['uid'], \PDO::PARAM_INT))
                        )
                        ->setMaxResults(1)
                        ->execute();

					$newsletter = $query->fetch();
					if($newsletter['uid']>0)
					{
						$newsletterUpdate = $GLOBALS['TYPO3_DB']->exec_UPDATEquery(
							'tx_mooxmailer_domain_model_newsletter',
							'deleted=0 AND uid='.$jobInfo['uid'],
							array(
								"fails" => $newsletter['fails']+1,
								"tstamp" => $time
							)
						);	
					}
				}
			}
				
			unset($error);													
		}		
	}
	
	/**
	 * create delivery entry 
	 *
	 * @param array $maildata
	 * @param string $action
	 * @param bool $isSent
	 */
	public function createDeliveryExtbase($maildata,$action,$isSent = false)
	{		
		if(is_array($maildata)){
			if($action!="processed"){
				
				$delivery = $this->objectManager->get('Neulandlotsen\\MooxMailer\\Domain\\Model\\Delivery');
				
				if($maildata['to']['type']=="feuser"){
					$feUser = $this->frontendUserRepository->findBouncedByUid($maildata['to']['uid']);
					if(is_object($feUser)){
						$delivery->addFeUser($feUser);
					}
				} elseif($maildata['to']['type']=="address"){
					$address = $this->addressRepository->findBouncedByUid($maildata['to']['uid']);
					if(is_object($address)){
						$delivery->addAddress($address);
					}
				}
				
				$delivery->setTitle($maildata['job'].' to '.$maildata['to']['type'].' ['.$maildata['to']['uid'].']');			
				$delivery->setJob($maildata['job']);
					
				if($maildata['jobType']=="newsletter"){
					$delivery->setJobType("newsletter");
				} elseif(in_array($maildata['jobType'],self::$allowedTasks)) {
					$delivery->setJobType($maildata['jobType']);
				} else {
					$delivery->setJobType("undefined");
				}
				
				if($action=="sent"){
					if($isSent){
						$delivery->setSent(1);
					} else {
						$delivery->setFailed(1);
						if($maildata['jobType']=="newsletter"){						
							$jobInfo = self::parseMailerJob($maildata['job']);
							$newsletter = $this->newsletterRepository->findByUid($jobInfo['uid'],FALSE);
							if(is_object($newsletter)){
								$newsletter->setFails($newsletter->getFails()+1);
								$this->newsletterRepository->update($newsletter);
								unset($newsletter);
							}
						}
					}
				}
								
				$this->deliveryRepository->add($delivery);

				unset($delivery);
								
			} else {
				
				if($maildata['to']['type']=="feuser"){
					$delivery = $this->deliveryRepository->findByJobAndFeUser($maildata['job'],$maildata['to']['uid']);					
				} elseif($maildata['to']['type']=="address"){
					$delivery = $this->deliveryRepository->findByJobAndAddress($maildata['job'],$maildata['to']['uid']);
				}
				
				if(is_object($delivery)){
					if($isSent){
						$delivery->setSent(1);
					} else {
						$delivery->setFailed(1);
						if($maildata['jobType']=="newsletter"){
							$jobInfo = self::parseMailerJob($maildata['job']);
							$newsletter = $this->newsletterRepository->findByUid($jobInfo['uid'],FALSE);
							if(is_object($newsletter)){
								$newsletter->setFails($newsletter->getFails()+1);
								$this->newsletterRepository->update($newsletter);
								unset($newsletter);
							}
						}
					}
					$this->deliveryRepository->update($delivery);
					unset($delivery);
				}
				
			}
			
			$this->objectManager->get('TYPO3\CMS\Extbase\Persistence\PersistenceManagerInterface')->persistAll();
		}		
	}
	
	/**
	 * create error entry 
	 *
	 * @param array $maildata
	 * @param string $errormsg
	 */
	public function createErrorExtbase($maildata,$errormsg = "")
	{		
		if(is_array($maildata) && $errormsg!=""){
			
			$error = $this->objectManager->get('Neulandlotsen\\MooxMailer\\Domain\\Model\\Error');
				
			if($maildata['to']['type']=="feuser"){
				$feUser = $this->frontendUserRepository->findBouncedByUid($maildata['to']['uid']);
				if(is_object($feUser)){
					$error->addFeUser($feUser);
					$feUser->setQuality(2);
					$this->frontendUserRepository->update($feUser);
				}				
			} elseif($maildata['to']['type']=="address"){
				$address = $this->addressRepository->findBouncedByUid($maildata['to']['uid']);
				if(is_object($address)){
					$error->addAddress($address);
					$address->setQuality(2);
					$this->addressRepository->update($address);
				}
			}
			
			$error->setTitle($errormsg);
			$error->setEmail($maildata['to']['address']);			
			
			$error->setJob($maildata['job']);
				
			if($maildata['jobType']=="newsletter"){
				$error->setJobType("newsletter");
			} elseif(in_array($maildata['jobType'],self::$allowedTasks)) {
				$error->setJobType($maildata['jobType']);
			} else {
				$error->setJobType("undefined");
			}
						
			$this->errorRepository->add($error);

			unset($error);
			
			if(!$this->extConf['useMailerTracking']){
				if($maildata['jobType']=="newsletter"){
					$jobInfo = self::parseMailerJob($maildata['job']);
					$newsletter = $this->newsletterRepository->findByUid($jobInfo['uid'],FALSE);
					if(is_object($newsletter)){
						$newsletter->setFails($newsletter->getFails()+1);
						$this->newsletterRepository->update($newsletter);
						unset($newsletter);
					}
				}
			}
				
			$this->objectManager->get('TYPO3\CMS\Extbase\Persistence\PersistenceManagerInterface')->persistAll();
		}		
	}
	
	/**
	 * get formats
	 *
	 * @return array $formats
	 */
	public function getFormats()
	{				
		$formats = [];
		$formats[0]['value'] = "text/html";
		$formats[0]['title'] = "Text/HTML";
		$formats[1]['value'] = "text/plain";
		$formats[1]['title'] = "Text/Plain";
		
		return $formats;
	}
	
	/**
	 * get modes
	 *
	 * @return array $modes
	 */
	public function getModes()
	{				
		$modes = [];
		$modes[0]['value'] = "daemon";
		$modes[0]['title'] = "Daemon";
		$modes[1]['value'] = "direct";
		$modes[1]['title'] = "Direct";
		
		return $modes;
	}
	
	/**
	 * Returns storage page
	 *
	 * @return int
	 */
	public function getStoragePage()
	{
		return $this->storagePage;
	}

	/**
	 * Set storage page
	 *
	 * @param int $storagePage storage page
	 * @return void
	 */
	public function setStoragePage($storagePage)
	{
		$this->storagePage = $storagePage;
	}
	
	/**
	 * Returns dir
	 *
	 * @return string $dir
	 */
	public function getDir()
	{
		return $this->dir;
	}

	/**
	 * Set dir
	 *
	 * @param string $dir dir
	 * @return void
	 */
	public function setDir($dir)
	{
		$this->dir = $dir;
		$this->mailDir = $this->dir."/mails/";
		$this->jobDir = $this->dir."/jobs/";
		$this->logDir = $this->dir."/logs/";
	}
	
	/**
	 * Returns mail dir
	 *
	 * @return string $mailDir
	 */
	public function getMailDir()
	{
		return $this->dir."/mails/";
	}

	/**
	 * Returns job dir
	 *
	 * @return string $jobDir
	 */
	public function getJobDir()
	{
		return $this->dir."/jobs/";
	}
	
	/**
	 * Returns log dir
	 *
	 * @return string $logDir
	 */
	public function getLogDir()
	{
		return $this->dir."/logs/";
	}
	
	/**
	 * Returns no log
	 *
	 * @return bool $noLog
	 */
	public function getNoLog()
	{
		return $this->noLog;
	}

	/**
	 * Set no log
	 *
	 * @param bool $noLog no log
	 * @return void
	 */
	public function setNoLog($noLog)
	{
		$this->noLog = $noLog;
	}
	
	/**
	 * Returns counter mode
	 *
	 * @return string $countMode
	 */
	public function getCountType()
	{
		return $this->countMode;
	}

	/**
	 * Set counter mode
	 *
	 * @param string $countMode counter mode
	 * @return void
	 */
	public function setCountMode($countMode)
	{
		$this->countMode = $countMode;
	}
	
	/**
	 * Returns mode
	 *
	 * @return string $mode
	 */
	public function getMode()
	{
		return $this->mode;
	}

	/**
	 * Set mode
	 *
	 * @param string $mode mode
	 * @return void
	 */
	public function setMode($mode)
	{
		$this->mode = $mode;
	}
	
	/**
	 * Returns template uid
	 *
	 * @return int
	 */
	public function getTemplateUid()
	{
		return $this->templateUid;
	}

	/**
	 * Set template uid
	 *
	 * @param int $templateUid template uid
	 * @return void
	 */
	public function setTemplateUid($templateUid)
	{
		$this->templateUid = $templateUid;
	}
	
	/**
	 * Returns schedule uid
	 *
	 * @return int
	 */
	public function getScheduleUid()
	{
		return $this->scheduleUid;
	}

	/**
	 * Set schedule uid
	 *
	 * @param int $scheduleUid schedule uid
	 * @return void
	 */
	public function setScheduleUid($scheduleUid)
	{
		$this->scheduleUid = $scheduleUid;
	}
	
	/**
	 * Returns ext conf
	 *
	 * @return array
	 */
	public function getExtConf()
	{
		return $this->extConf;
	}

	/**
	 * Set ext conf
	 *
	 * @param array $extConf ext conf
	 * @return void
	 */
	public function setExtConf($extConf)
	{
		$this->extConf = $extConf;
	}
	
	/**
	 * Returns format
	 *
	 * @return string $format
	 */
	public function getFormat()
	{
		return $this->format;
	}

	/**
	 * Set format
	 *
	 * @param string $format format
	 * @return void
	 */
	public function setFormat($format)
	{
		$this->format = $format;
	}
	
	/**
	 * Returns subject
	 *
	 * @return string
	 */
	public function getSubject()
	{
		return $this->subject;
	}

	/**
	 * Set subject
	 *
	 * @param boolean $subject subject
	 * @return void
	 */
	public function setSubject($subject)
	{
		$this->subject = $subject;
	}
	
	/**
	 * Returns from
	 *
	 * @return array
	 */
	public function getFrom()
	{
		return $this->from;
	}

	/**
	 * Set from
	 *
	 * @param array $from from
	 * @return void
	 */
	public function setFrom($from)
	{
		$this->from = $from;
	}
	
	/**
	 * Returns reply to
	 *
	 * @return string
	 */
	public function getReplyTo()
	{
		return $this->replyTo;
	}

	/**
	 * Set reply to
	 *
	 * @param string $replyTo reply to
	 * @return void
	 */
	public function setReplyTo($replyTo)
	{
		$this->replyTo = $replyTo;
	}
	
	/**
	 * Returns cc
	 *
	 * @return array
	 */
	public function getCc()
	{
		return $this->cc;
	}

	/**
	 * Set cc
	 *
	 * @param array $cc cc
	 * @return void
	 */
	public function setCc($cc)
	{
		$this->cc = $cc;
	}
	
	/**
	 * Returns bcc
	 *
	 * @return array
	 */
	public function getBcc()
	{
		return $this->bcc;
	}

	/**
	 * Set bcc
	 *
	 * @param array $bcc bcc
	 * @return void
	 */
	public function setBcc($bcc)
	{
		$this->bcc = $bcc;
	}
	
	/**
	 * Returns description
	 *
	 * @return string $description
	 */
	public function getDescription()
	{
		return $this->description;
	}

	/**
	 * Set description
	 *
	 * @param string $description description
	 * @return void
	 */
	public function setDescription($description)
	{
		$this->description = $description;
	}
	
	/**
	 * Returns identifier
	 *
	 * @return string $identifier
	 */
	public function getIdentifier()
	{
		return $this->identifier;
	}

	/**
	 * Set identifier
	 *
	 * @param string $identifier identifier
	 * @return void
	 */
	public function setIdentifier($identifier)
	{
		$this->identifier = $identifier;
	}
	
	/**
	 * Returns debug
	 *
	 * @return bool $debug
	 */
	public function getDebug()
	{
		return $this->debug;
	}

	/**
	 * Set debug
	 *
	 * @param bool $debug debug
	 * @return void
	 */
	public function setDebug($debug)
	{
		$this->debug = $debug;
	}
	
	/**
	 * Returns debug address
	 *
	 * @return string $debugAddress
	 */
	public function getDebugAddress()
	{
		return $this->debugAddress;
	}

	/**
	 * Set debug address
	 *
	 * @param string $debugAddress debug address
	 * @return void
	 */
	public function setDebugAddress($debugAddress)
	{
		$this->debugAddress = $debugAddress;
	}
	
	/**
	 * Returns debug mail limit
	 *
	 * @return int $debugMailLimit
	 */
	public function getDebugMailLimit()
	{
		return $this->debugMailLimit;
	}

	/**
	 * Set debug mail limit
	 *
	 * @param int $debugMailLimit debug mail limit
	 * @return void
	 */
	public function setDebugMailLimit($debugMailLimit)
	{
		$this->debugMailLimit = $debugMailLimit;
	}
	
	/**
	 * Returns mail count
	 *
	 * @return int $mailCount
	 */
	public function getMailCount()
	{
		return $this->mailCount;
	}

	/**
	 * Set mail count
	 *
	 * @param int $mailCount mail count
	 * @return void
	 */
	public function setMailCount($mailCount)
	{
		$this->mailCount = $mailCount;
	}
	
	/**
	 * Returns mails prepared
	 *
	 * @return int $mailsPrepared
	 */
	public function getMailsPrepared()
	{
		return $this->mailsPrepared;
	}

	/**
	 * Set mails prepared
	 *
	 * @param int $mailsPrepared mails prepared
	 * @return void
	 */
	public function setMailsPrepared($mailsPrepared)
	{
		$this->mailsPrepared = $mailsPrepared;
	}		
	
	/**
	 * Returns mails processed
	 *
	 * @return int $mailsProcessed
	 */
	public function getMailsProcessed()
	{
		return $this->mailsProcessed;
	}

	/**
	 * Set mails processed
	 *
	 * @param int $mailsProcessed mails processed
	 * @return void
	 */
	public function setMailsProcessed($mailsProcessed)
	{
		$this->mailsProcessed = $mailsProcessed;
	}		
	
	/**
	 * Returns mailfiles prepared
	 *
	 * @return int $mailfilesPrepared
	 */
	public function getMailfilesPrepared()
	{
		return $this->mailfilesPrepared;
	}

	/**
	 * Set mailfiles prepared
	 *
	 * @param int $mailfilesProcessed mailfiles prepared
	 * @return void
	 */
	public function setMailfilesPrepared($mailfilesPrepared)
	{
		$this->mailfilesPrepared = $mailfilesPrepared;
	}
	
	/**
	 * Returns mailfiles processed
	 *
	 * @return int $mailfilesProcessed
	 */
	public function getMailfilesProcessed()
	{
		return $this->mailfilesProcessed;
	}

	/**
	 * Set mailfiles processed
	 *
	 * @param int $mailfilesProcessed mailfiles processed
	 * @return void
	 */
	public function setMailfilesProcessed($mailfilesProcessed)
	{
		$this->mailfilesProcessed = $mailfilesProcessed;
	}
	
	/**
	 * Returns skip mail action
	 *
	 * @return bool $skipMailAction
	 */
	public function getSkipMailAction()
	{
		return $this->skipMailAction;
	}

	/**
	 * Set skip mail action
	 *
	 * @param bool $skipMailAction skip mail action
	 * @return void
	 */
	public function setSkipMailAction($skipMailAction)
	{
		$this->skipMailAction = $skipMailAction;
	}
	
	/**
	 * Returns memory limit
	 *
	 * @return int
	 */
	public function getMemoryLimit()
	{
		return $this->memoryLimit;
	}

	/**
	 * Set memory limit
	 *
	 * @param int $memoryLimit memory limit
	 * @return void
	 */
	public function setMemoryLimit($memoryLimit)
	{
		$this->memoryLimit = $memoryLimit;
	}
	
	/**
	 * Returns script timeout
	 *
	 * @return int
	 */
	public function getScriptTimeout()
	{
		return $this->scriptTimeout;
	}

	/**
	 * Set script timeout
	 *
	 * @param int $scriptTimeout script timeout
	 * @return void
	 */
	public function setScriptTimeout($scriptTimeout)
	{
		$this->scriptTimeout = $scriptTimeout;
	}
	
	/**
	 * Returns timestamp
	 *
	 * @return int
	 */
	public function getTimestamp()
	{
		return $this->timestamp;
	}

	/**
	 * Set script timeout
	 *
	 * @param int $timestamp timestamp
	 * @return void
	 */
	public function setTimestamp($timestamp)
	{
		$this->timestamp = $timestamp;
	}
	
	/**
	 * Returns resumed
	 *
	 * @return bool $resumed
	 */
	public function getResumed()
	{
		return $this->resumed;
	}

	/**
	 * Set resumed
	 *
	 * @param bool $resumed resumed
	 * @return void
	 */
	public function setResumed($resumed)
	{
		$this->resumed = $resumed;
	}
	
	/**
	 * Returns enable mail filter
	 *
	 * @return bool $enableMailFilter
	 */
	public function getEnableMailFilter()
	{
		return $this->enableMailFilter;
	}

	/**
	 * Set enable mail filter
	 *
	 * @param bool $enableMailFilter enable mail filter
	 * @return void
	 */
	public function setEnableMailFilter($enableMailFilter)
	{
		$this->enableMailFilter = $enableMailFilter;
	}
	
	/**
	 * Returns whitelist
	 *
	 * @return array
	 */
	public function getWhitelist()
	{
		return $this->whitelist;
	}

	/**
	 * Set whitelist
	 *
	 * @param array $whitelist whitelist
	 * @return void
	 */
	public function setWhitelist($whitelist)
	{
		$this->whitelist = $whitelist;
	}
	
	/**
	 * Returns blacklist
	 *
	 * @return array
	 */
	public function getBlacklist()
	{
		return $this->blacklist;
	}

	/**
	 * Set blacklist
	 *
	 * @param array $blacklist blacklist
	 * @return void
	 */
	public function setBlacklist($blacklist)
	{
		$this->blacklist = $blacklist;
	}
	
	/**
	 * Returns schedule
	 *
	 * @return array
	 */
	public function getSchedule()
	{
		return $this->schedule;
	}

	/**
	 * Set schedule
	 *
	 * @param array $schedule schedule
	 * @return void
	 */
	public function setSchedule($schedule)
	{
		$this->schedule = $schedule;
	}
	
	/**
	 * Returns is test
	 *
	 * @return bool $isTest
	 */
	public function getIsTest()
	{
		return $this->isTest;
	}

	/**
	 * Set is test
	 *
	 * @param bool $isTest is test
	 * @return void
	 */
	public function setIsTest($isTest)
	{
		$this->isTest = $isTest;
	}

}
?>