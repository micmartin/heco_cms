<?php 
return array(
	'ctrl' => array(
		'title'	=> 'LLL:EXT:moox_mailer/Resources/Private/Language/locallang_db.xlf:tx_mooxmailer_domain_model_newsletter',
		'label' => 'title',
		'tstamp' => 'tstamp',
		'crdate' => 'crdate',
		'cruser_id' => 'cruser_id',
		'dividers2tabs' => TRUE,
		'sortby' => 'sorting',
		'versioningWS' => 2,
		'versioning_followPages' => TRUE,
		'origUid' => 't3_origuid',
		'languageField' => 'sys_language_uid',
		'transOrigPointerField' => 'l10n_parent',
		'transOrigDiffSourceField' => 'l10n_diffsource',
		'delete' => 'deleted',
		'enablecolumns' => array(
			'disabled' => 'hidden',
			'starttime' => 'starttime',
			'endtime' => 'endtime',
		),		
		'searchFields' => 'title,subject',
		'requestUpdate' => 'zipmode',
		'iconfile' => 'EXT:moox_mailer/Resources/Public/Icons/tx_mooxmailer_domain_model_newsletter.gif',
		'hideTable' => FALSE
	),
	'interface' => array(
		'showRecordFieldList' => 'sys_language_uid, l10n_parent, l10n_diffsource, page, title, subject, sender_name, sender_email, user_folders, address_folders, test_address_folders, move_to_folder, autologin',
	),
	'types' => array(
		'1' => array('showitem' => 'sys_language_uid;;;;1-1-1, l10n_parent, l10n_diffsource, page, title, subject, sender_name, sender_email, user_folders, address_folders, test_address_folders, move_to_folder, autologin'),
	),
	'palettes' => array(
		'1' => array('showitem' => ''),
	),
	'columns' => array(
		'sys_language_uid' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.language',
			'config' => array(
				'type' => 'select',
				'foreign_table' => 'sys_language',
				'foreign_table_where' => 'ORDER BY sys_language.title',
				'items' => array(
					array('LLL:EXT:lang/locallang_general.xlf:LGL.allLanguages', -1),
					array('LLL:EXT:lang/locallang_general.xlf:LGL.default_value', 0)
				),
			),
		),
		'l10n_parent' => array(
			'displayCond' => 'FIELD:sys_language_uid:>:0',
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.l18n_parent',
			'config' => array(
				'type' => 'select',
				'items' => array(
					array('', 0),
				),
				'foreign_table' => 'tx_mooxmailer_domain_model_newsletter',
				'foreign_table_where' => 'AND tx_mooxmailer_domain_model_newsletter.pid=###CURRENT_PID### AND tx_mooxmailer_domain_model_newsletter.sys_language_uid IN (-1,0)',
			),
		),
		'l10n_diffsource' => array(
			'config' => array(
				'type' => 'passthrough',
			),
		),
		't3ver_label' => array(
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.versionLabel',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'max' => 255,
			)
		),
		'hidden' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.hidden',
			'config' => array(
				'type' => 'passthrough',
			)
		),
		'cruser_id' => array(
			'label' => 'cruser_id',
			'config' => array(
				'type' => 'passthrough'
			)
		),
		'pid' => array(
			'label' => 'pid',
			'config' => array(
				'type' => 'passthrough'
			)
		),
		'crdate' => array(
			'label' => 'crdate',
			'config' => array(
				'type' => 'passthrough',
			)
		),
		'tstamp' => array(
			'label' => 'tstamp',
			'config' => array(
				'type' => 'passthrough',
			)
		),
		'starttime' => array(
			'exclude' => 1,
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.starttime',
			'config' => array(
				'type' => 'passthrough',
			),
		),
		'endtime' => array(
			'exclude' => 1,
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.endtime',
			'config' => array(
				'type' => 'passthrough',
			),
		),
		'page' => array(
			'exclude' => 1,
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => 'LLL:EXT:moox_mailer/Resources/Private/Language/locallang_db.xlf:tx_mooxmailer_domain_model_newsletter.page',
			'config' => array(
				'type' => 'group',
				'internal_type' => 'db',
				'allowed' => 'pages',
				'size' => 1,
				'maxitems' => 1,
				'minitems' => 0,
				'show_thumbs' => 1,
				'wizards' => array(
					'suggest' => array(
						'type' => 'suggest',
					),
				),
			)
		),
		'title' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:moox_mailer/Resources/Private/Language/locallang_db.xlf:tx_mooxmailer_domain_model_newsletter.title',
			'config' => array(
				'type' => 'input',
				'size' => 40,
				'eval' => 'required,trim'
			),
		),
		'subject' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:moox_mailer/Resources/Private/Language/locallang_db.xlf:tx_mooxmailer_domain_model_newsletter.subject',
			'config' => array(
				'type' => 'input',
				'size' => 40,
				'eval' => 'trim'
			),
		),
		'sender_name' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:moox_mailer/Resources/Private/Language/locallang_db.xlf:tx_mooxmailer_domain_model_newsletter.sender_name',
			'config' => array(
				'type' => 'input',
				'size' => 40,
				'eval' => 'trim'
			),
		),
		'sender_email' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:moox_mailer/Resources/Private/Language/locallang_db.xlf:tx_mooxmailer_domain_model_newsletter.sender_email',
			'config' => array(
				'type' => 'input',
				'size' => 40,
				'eval' => 'trim,email'
			),
		),
		'reply_to_email' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:moox_mailer/Resources/Private/Language/locallang_db.xlf:tx_mooxmailer_domain_model_newsletter.reply_to_email',
			'config' => array(
				'type' => 'input',
				'size' => 40,
				'eval' => 'trim,email'
			),
		),
		'user_folders' => array(
			'exclude' => 1,
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => 'LLL:EXT:moox_mailer/Resources/Private/Language/locallang_db.xlf:tx_mooxmailer_domain_model_newsletter.user_folders',
			'config' => array(
				'type' => 'group',
				'internal_type' => 'db',
				'allowed' => 'pages',
				'size' => 1,
				'maxitems' => 1,
				'minitems' => 0,
				'show_thumbs' => 1,
				'wizards' => array(
					'suggest' => array(
						'type' => 'suggest',
					),
				),
			)
		),
		'address_folders' => array(
			'exclude' => 1,
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => 'LLL:EXT:moox_mailer/Resources/Private/Language/locallang_db.xlf:tx_mooxmailer_domain_model_newsletter.address_folders',
			'config' => array(
				'type' => 'group',
				'internal_type' => 'db',
				'allowed' => 'pages',
				'size' => 1,
				'maxitems' => 1,
				'minitems' => 0,
				'show_thumbs' => 1,
				'wizards' => array(
					'suggest' => array(
						'type' => 'suggest',
					),
				),
			)
		),
		'test_address_folders' => array(
			'exclude' => 1,
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => 'LLL:EXT:moox_mailer/Resources/Private/Language/locallang_db.xlf:tx_mooxmailer_domain_model_newsletter.test_address_folders',
			'config' => array(
				'type' => 'group',
				'internal_type' => 'db',
				'allowed' => 'pages',
				'size' => 1,
				'maxitems' => 1,
				'minitems' => 0,
				'show_thumbs' => 1,
				'wizards' => array(
					'suggest' => array(
						'type' => 'suggest',
					),
				),
			)
		),
		'move_to_folder' => array(
			'exclude' => 1,
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => 'LLL:EXT:moox_mailer/Resources/Private/Language/locallang_db.xlf:tx_mooxmailer_domain_model_newsletter.move_to_folder',
			'config' => array(
				'type' => 'group',
				'internal_type' => 'db',
				'allowed' => 'pages',
				'size' => 1,
				'maxitems' => 1,
				'minitems' => 0,
				'show_thumbs' => 1,
				'wizards' => array(
					'suggest' => array(
						'type' => 'suggest',
					),
				),
			)
		),
		'bcc' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:moox_mailer/Resources/Private/Language/locallang_db.xlf:tx_mooxmailer_domain_model_newsletter.bcc',
			'config' => array(
				'type' => 'text',
				'cols' => 40,
				'rows' => 20,
				'eval' => 'trim'
			),
		),
		'autologin' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:moox_mailer/Resources/Private/Language/locallang_db.xlf:tx_mooxmailer_domain_model_newsletter.autologin',
			'config' => array(
				'type' => 'check',
			),
		),
		'zipmode' => [
			'exclude' => 0,
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => 'LLL:EXT:moox_mailer/Resources/Private/Language/locallang_db.xlf:tx_mooxmailer_domain_model_newsletter.zipmode',
			'config' => [
				'type' => 'select',
				'allowNonIdValues' => 1,
				'itemsProcFunc' => 'Neulandlotsen\MooxMailer\Hooks\TcaFormHelper->zipmodes',
				'default' => '',
			],
		],	
		'ziplist' => array(
			'exclude' => 0,
			'displayCond' => 'FIELD:zipmode:=:ziplist',
			'label' => 'LLL:EXT:moox_mailer/Resources/Private/Language/locallang_db.xlf:tx_mooxmailer_domain_model_newsletter.ziplist',
			'config' => array(
				'type' => 'text',
				'cols' => 40,
				'rows' => 20,
				'eval' => 'trim,required'
			),
		),
		'template' => array(
			'label' => 'LLL:EXT:moox_mailer/Resources/Private/Language/locallang_db.xlf:tx_mooxmailer_domain_model_newsletter.template',
			'config' => array(
				'type' => 'passthrough',
			)
		),
		'html' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:moox_mailer/Resources/Private/Language/locallang_db.xlf:tx_mooxmailer_domain_model_newsletter.html',
			'config' => array(
				'type' => 'passthrough',
			),
		),			
		'sent' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:moox_mailer/Resources/Private/Language/locallang_db.xlf:tx_mooxmailer_domain_model_newsletter.sent',
			'config' => array(
				'type' => 'passthrough',
			),
		),
		'sent_subject' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:moox_mailer/Resources/Private/Language/locallang_db.xlf:tx_mooxmailer_domain_model_newsletter.sent_subject',
			'config' => array(
				'type' => 'passthrough',
			),
		),		
		'sent_count' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:moox_mailer/Resources/Private/Language/locallang_db.xlf:tx_mooxmailer_domain_model_newsletter.sent_count',
			'config' => array(
				'type' => 'passthrough',
			),
		),	
		'job' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:moox_mailer/Resources/Private/Language/locallang_db.xlf:tx_mooxmailer_domain_model_newsletter.job',
			'config' => array(
				'type' => 'passthrough',
			),
		),	
		'first_opening' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:moox_mailer/Resources/Private/Language/locallang_db.xlf:tx_mooxmailer_domain_model_newsletter.first_opening',
			'config' => array(
				'type' => 'passthrough',
			),
		),
		'last_opening' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:moox_mailer/Resources/Private/Language/locallang_db.xlf:tx_mooxmailer_domain_model_newsletter.last_opening',
			'config' => array(
				'type' => 'passthrough',
			),
		),
		'openings' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:moox_mailer/Resources/Private/Language/locallang_db.xlf:tx_mooxmailer_domain_model_newsletter.openings',
			'config' => array(
				'type' => 'passthrough',
			),
		),
		'openings_unique' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:moox_mailer/Resources/Private/Language/locallang_db.xlf:tx_mooxmailer_domain_model_newsletter.openings_unique',
			'config' => array(
				'type' => 'passthrough',
			),
		),
		'first_click' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:moox_mailer/Resources/Private/Language/locallang_db.xlf:tx_mooxmailer_domain_model_newsletter.first_click',
			'config' => array(
				'type' => 'passthrough',
			),
		),
		'last_click' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:moox_mailer/Resources/Private/Language/locallang_db.xlf:tx_mooxmailer_domain_model_newsletter.last_click',
			'config' => array(
				'type' => 'passthrough',
			),
		),
		'clicks' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:moox_mailer/Resources/Private/Language/locallang_db.xlf:tx_mooxmailer_domain_model_newsletter.clicks',
			'config' => array(
				'type' => 'passthrough',
			),
		),
		'clicks_unique' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:moox_mailer/Resources/Private/Language/locallang_db.xlf:tx_mooxmailer_domain_model_newsletter.clicks_unique',
			'config' => array(
				'type' => 'passthrough',
			),
		),		
		'bounces' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:moox_mailer/Resources/Private/Language/locallang_db.xlf:tx_mooxmailer_domain_model_newsletter.bounces',
			'config' => array(
				'type' => 'passthrough',
			),
		),
		'fails' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:moox_mailer/Resources/Private/Language/locallang_db.xlf:tx_mooxmailer_domain_model_newsletter.fails',
			'config' => array(
				'type' => 'passthrough',
			),
		),
	),
);

?>