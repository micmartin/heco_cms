TYPO3.jQuery(document).ready(function() {
	
	TYPO3.jQuery("#selected-folder").click(function(){
		if ( TYPO3.jQuery("#folder-list").is(":visible") ) {
			TYPO3.jQuery( "#folder-expander" ).removeClass( "t3-icon-move-up t3-icon-move-down" );
			TYPO3.jQuery( "#folder-expander" ).addClass( "t3-icon-move-down" );
		} else { 
			TYPO3.jQuery( "#folder-expander" ).removeClass( "t3-icon-move-up t3-icon-move-down" );
			TYPO3.jQuery( "#folder-expander" ).addClass( "t3-icon-move-up" );
		}		
		TYPO3.jQuery("#folder-list").toggle("fast");
		
	});
	
	TYPO3.jQuery("#folder-list-close").click(function(){
		TYPO3.jQuery("#folder-list").hide("fast");
	});
	
	TYPO3.jQuery("#filter-query").on("click", function () {
	   TYPO3.jQuery(this).select();
	});
	
	TYPO3.jQuery("#filter-mailing").on("change", function () {
	  TYPO3.jQuery("#filterForm").submit();
	});
	
	TYPO3.jQuery("#filter-state").on("change", function () {
	  TYPO3.jQuery("#filterForm").submit();
	});
	
	TYPO3.jQuery("#filter-per-page").on("change", function () {
	  TYPO3.jQuery("#filterForm").submit();
	});
	
	TYPO3.jQuery(this).ajaxStart(function(){
		  TYPO3.jQuery("body").append("<div id='tx-moox-mailer-admin-overlay'><img src='/typo3conf/ext/moox_mailer/Resources/Public/Images/ajax-loader.gif' /></div>");
    });
	
	TYPO3.jQuery(this).ajaxStop(function(){
          TYPO3.jQuery("#tx-moox-mailer-admin-overlay").remove();
    });	
	
	TYPO3.jQuery("#user-selector").click(function(){
		if ( TYPO3.jQuery( "#user-list" ).is(":visible") ) {
			TYPO3.jQuery( "#user-expander" ).removeClass( "t3-icon-move-up t3-icon-move-down" );
			TYPO3.jQuery( "#user-expander" ).addClass( "t3-icon-move-down" );
		} else { 
			TYPO3.jQuery( "#user-expander" ).removeClass( "t3-icon-move-up t3-icon-move-down" );
			TYPO3.jQuery( "#user-expander" ).addClass( "t3-icon-move-up" );
		}		
		TYPO3.jQuery("#user-list").toggle("fast");
	});
	
	TYPO3.jQuery("#user-list-close").click(function(){
		TYPO3.jQuery("#user-list").hide("fast");
	});
	
	TYPO3.jQuery("select#newsletter-zipmode").change(function(){
		if(TYPO3.jQuery("select#newsletter-zipmode").val()=='list'){
			TYPO3.jQuery("div#newsletter-ziplist-wrapper").show();
		} else {
			TYPO3.jQuery("div#newsletter-ziplist-wrapper").hide();
		}
	});
});

function hideUserList(){
	TYPO3.jQuery( "#user-expander" ).removeClass( "t3-icon-move-up t3-icon-move-down" );
	TYPO3.jQuery( "#user-expander" ).addClass( "t3-icon-move-down" );
	TYPO3.jQuery( "#user-list" ).hide("fast");
}

function changePreviewUser(email){
	TYPO3.jQuery( "#newsletter-current-user-email" ).val(email);
	TYPO3.jQuery( "#NewsletterForm" ).submit();
}

function toggleDetails(id){
	TYPO3.jQuery("#folder-list").hide("fast");
	if ( TYPO3.jQuery(id).is(":visible") ) {
		visible = true;
	} else { 
		visible = false;
	}	
	TYPO3.jQuery(".newsletter-details").hide("fast");
	TYPO3.jQuery(".newsletter-details-expander-reset").removeClass( "t3-icon-move-up t3-icon-move-down" );
	TYPO3.jQuery(".newsletter-details-expander-reset").addClass( "t3-icon-move-down" );
	if (visible) {		
		TYPO3.jQuery(id).hide("fast");
		TYPO3.jQuery(id + "-expander").removeClass( "t3-icon-move-up t3-icon-move-down" );
		TYPO3.jQuery(id + "-expander").addClass( "t3-icon-move-down" );
	} else { 
		TYPO3.jQuery(id).show("fast");
		TYPO3.jQuery(id + "-expander").removeClass( "t3-icon-move-up t3-icon-move-down" );
		TYPO3.jQuery(id + "-expander").addClass( "t3-icon-move-up" );
	}	
}

function reloadIframe(id,src,display,email){	
	TYPO3.jQuery(id).attr('src', src);
	TYPO3.jQuery(id + "-display").html(display);
	if(display=='Alle Benutzer/Adressen'){
		TYPO3.jQuery('#userNav0').hide("fast");
	} else {
		TYPO3.jQuery('#userNav0').show("fast");
	}
	if(email!=''){		
		TYPO3.jQuery('#newsletter-current-user-email').val(email);
	}
	hideUserList();
}