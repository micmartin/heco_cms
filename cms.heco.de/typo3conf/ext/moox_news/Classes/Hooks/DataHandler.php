<?php
namespace Neulandlotsen\MooxNews\Hooks;

/**
 * This file is part of the "moox_news" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */
use Neulandlotsen\MooxNews\Service\AccessControlService;
use Neulandlotsen\MooxNews\Service\Transliterator\Transliterator;
use TYPO3\CMS\Backend\Utility\BackendUtility as BackendUtilityCore;
use TYPO3\CMS\Core\Cache\CacheManager;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Database\ConnectionPool;

/**
 * Hook into tcemain which is used to show preview of news item
 *
 */
class DataHandler
{
    /**
     * Flushes the cache if a news record was edited.
     * This happens on two levels: by UID and by PID.
     *
     * @param array $params
     */
    public function clearCachePostProc(array $params)
    {
        if (isset($params['table']) && $params['table'] === 'tx_mooxnews_domain_model_news')
		{
			$cacheTagsToFlush = [];
            if (isset($params['uid']))
			{
				$cacheTagsToFlush[] = 'tx_mooxnews_uid_' . $params['uid'];
			}
            if (isset($params['uid_page']))
			{
				$cacheTagsToFlush[] = 'tx_mooxnews_pid_' . $params['uid_page'];
			}

			$cacheManager = GeneralUtility::makeInstance(CacheManager::class);
            foreach ($cacheTagsToFlush as $cacheTag)
			{
				$cacheManager->flushCachesInGroupByTag('pages', $cacheTag);
			}
		}
    }

    /**
     * Generate a different preview link     *
     *
     * @param string $status status
     * @param string $table table name
     * @param int $recordUid id of the record
     * @param array $fields fieldArray
     * @param \TYPO3\CMS\Core\DataHandling\DataHandler $parentObject parent Object
     */
    public function processDatamap_afterDatabaseOperations(
        $status,
        $table,
        $recordUid,
        array $fields,
        \TYPO3\CMS\Core\DataHandling\DataHandler $parentObject
    ) {
        
		// Clear category cache
        if ($table === 'sys_category')
		{
            $cache = GeneralUtility::makeInstance(CacheManager::class)->getCache('cache_news_category');
            $cache->flush();
        } 
		elseif ($table === 'tx_mooxnews_domain_model_news')
		{			
			$oldRecordUid = $recordUid;
			
			// direct preview
			if (!is_numeric($recordUid))
			{
				if(is_object($parentObject))
				{
					$recordUid = (int)$parentObject->substNEWwithIDs[$recordUid];
				}
			}
			
			if($recordUid)
			{				
				$params = [];
				
				$extConf = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Configuration\ExtensionConfiguration::class)->get('moox_news');
				
				$doUpdate = false;
				
				$objectManager = GeneralUtility::makeInstance('TYPO3\CMS\Extbase\Object\ObjectManager');		
				$newsRepository = $objectManager->get('Neulandlotsen\MooxNews\Domain\Repository\NewsRepository');
				$categoryRepository = $objectManager->get('Neulandlotsen\MooxNews\Domain\Repository\CategoryRepository');
				
				$item = $newsRepository->findByUid($recordUid,false);
				
				if(is_object($item))
				{
					// get post save functions from external extensions
					$externalFunctions = [];
					$functions = get_class_methods(get_class($this));
					foreach($functions AS $function)
					{
						if(substr($function,0,9)=="external_")
						{
							$externalFunctions[] = $function;
						}
					}

					foreach($externalFunctions AS $externalFunction)
					{
						if($returnedItem = $this->$externalFunction($item,$params))
						{
							$item = $returnedItem;
							$doUpdate = true;
						}
					}

					if($GLOBALS['_POST']['data']['tx_mooxnews_domain_model_news'][$oldRecordUid]['categories']!="")
					{
						$categories = explode(",",$GLOBALS['_POST']['data']['tx_mooxnews_domain_model_news'][$oldRecordUid]['categories']);
						if(is_numeric($categories[0]) && $categories[0]>0)
						{
							$itemFirstCategory = $categoryRepository->findByUid($categories[0]);
							if(is_object($itemFirstCategory))
							{	
								$params['firstCategoryTitle'] = $itemFirstCategory->getTitle();
							}
						}
					}
					
					if(is_object($item->getDatetime()) && (int)$item->getDatetime()->format("Y")>0)
					{				
						$year = (int)$item->getDatetime()->format("Y");													
						if($year>1970)
						{										
							$item->setYear($year);						
							$doUpdate = true;
						}
					}
					if ($extConf['autoSetFeGroupOnSave']>0 || $extConf['autoSetSinglePidOnSave']>0)
					{				
						$objectManager->get('TYPO3\CMS\Extbase\Persistence\PersistenceManagerInterface')->persistAll();
																		
						if(is_object($itemFirstCategory)){	
							
							$categoryFeGroups = $itemFirstCategory->getFeGroup();											
							$oldItemGroups = $item->getFeGroup();
							
							if($categoryFeGroups)
							{
								if($extConf['autoSetFeGroupOnSave']>0)
								{
									if(!$extConf['autoSetFeGroupOnSave'] || in_array($item->getFeGroup(),[-1,-2]))
									{
										if($oldItemGroups!=$categoryFeGroups)
										{
											$item->setFeGroup($categoryFeGroups);
											$doUpdate = true;										
										}
									} 
									else 
									{
										$itemFeGroups = [];
										if($item->getFeGroup()!="")
										{
											$itemFeGroups 		= explode(",",$item->getFeGroup());
										}								
										if($categoryFeGroups!="")
										{
											$categoryFeGroups = explode(",",$categoryFeGroups);
										} 
										else 
										{
											$categoryFeGroups = [];
										}
										$newItemFeGroups = $categoryFeGroups;
										foreach($itemFeGroups AS $group)
										{
											if(!in_array($group,$newItemFeGroups))
											{
												$newItemFeGroups[] = $group;
											}
										}
										
										$newItemFeGroups = implode(",",$newItemFeGroups);
										if($oldItemGroups!=$newItemFeGroups)
										{
											$item->setFeGroup($newItemFeGroups);
											$doUpdate = true;										
										}
									}								
								}
							}
								
							if($extConf['autoSetSinglePidOnSave']>0)
							{
								if($item->getSinglePid()!=$itemFirstCategory->getSinglePid())
								{
									$item->setSinglePid($itemFirstCategory->getSinglePid());
									$doUpdate = true;								
								}
							}
						}					
					}

					if(!$item->getPathSegment() || $extConf['forcePathSegmentRecreationOnSave'])
					{
						$pathSegment = Transliterator::urlize($item->getTitle());

						$queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)
							->getQueryBuilderForTable('tx_mooxnews_domain_model_news');

						$duplicates = $queryBuilder
							->select('uid')
							->from('tx_mooxnews_domain_model_news')
							->where(
								$queryBuilder->expr()->eq('path_segment', $queryBuilder->createNamedParameter($pathSegment)),
								$queryBuilder->expr()->neq('uid', $queryBuilder->createNamedParameter($item->getUid()))
							)
							->execute();

						if($duplicates->fetch())
						{
							$pathSegmentBase = $pathSegment;
							for($i=1;$i<1000;$i++)
							{
								$pathSegment = $pathSegmentBase."-".$i;
								$duplicates = $queryBuilder
									->select('uid')
									->from('tx_mooxnews_domain_model_news')
									->where(
										$queryBuilder->expr()->eq('path_segment', $queryBuilder->createNamedParameter($pathSegment)),
										$queryBuilder->expr()->neq('uid', $queryBuilder->createNamedParameter($item->getUid()))
									)
									->execute();
								if(!$duplicates->fetch())
								{
									break;
								}
							}
						}
						$item->setPathSegment($pathSegment);
						$doUpdate = true;
					}

					if(true)
					{
						$item->setHash('');
						$doUpdate = true;
					}

					
					/*
					if($item->getShareTwitterText()=="")
					{
						$item->setShareTwitterText($item->getTitle());
						$doUpdate = true;					
					}
					*/
					
					if($doUpdate)
					{
						$newsRepository->update($item);	
						$objectManager->get('TYPO3\CMS\Extbase\Persistence\PersistenceManagerInterface')->persistAll();						
					}
				}
			}											
		}
    }

	/**
	 * Prevent saving of a news record if the editor doesn't have access to all categories of the news record
	 *
	 * @param array $fieldArray
	 * @param string $table
	 * @param int $id
	 * @param $parentObject \TYPO3\CMS\Core\DataHandling\DataHandler
	 */
	public function processDatamap_preProcessFieldArray(&$fieldArray, $table, $id, $parentObject)
	{
		if ($table === 'tx_mooxnews_domain_model_news') {
			// check permissions of assigned categories
			if (is_int($id) && !$this->getBackendUser()->isAdmin()) {
				$newsRecord = BackendUtilityCore::getRecord($table, $id);
				if (!AccessControlService::userHasCategoryPermissionsForRecord($newsRecord)) {
					$parentObject->log($table, $id, 2, 0, 1,
						"processDatamap: Attempt to modify a record from table '%s' without permission. Reason: the record has one or more categories assigned that are not defined in your BE usergroup.",
						1, [$table]);
					// unset fieldArray to prevent saving of the record
					$fieldArray = [];
				} else {

					// If the category relation has been modified, no | is found anymore
					if (isset($fieldArray['categories']) && strpos($fieldArray['categories'], '|') === false) {
						$deniedCategories = AccessControlService::getAccessDeniedCategories($newsRecord);
						if (is_array($deniedCategories)) {
							foreach ($deniedCategories as $deniedCategory) {
								$fieldArray['categories'] .= ',' . $deniedCategory['uid'];
							}
							// Check if the categories are not empty,
							if (!empty($fieldArray['categories'])) {
								$fieldArray['categories'] = trim($fieldArray['categories'], ',');
							}
						}
					}
				}
			}
		}
	}

	/**
	 * Fill path_segment/slug field with title
	 *
	 * @param string $status
	 * @param string $table
	 * @param string|int $id
	 * @param array $fieldArray
	 * @param \TYPO3\CMS\Core\DataHandling\DataHandler $parentObject
	 */
	public function processDatamap_postProcessFieldArray($status, $table, $id, &$fieldArray, \TYPO3\CMS\Core\DataHandling\DataHandler $parentObject)
	{
		if ($table === 'tx_mooxnews_domain_model_news' && $status === 'new' && version_compare(TYPO3_branch, '9.5', '<=')) {
			if (!isset($fieldArray['path_segment']) || empty($fieldArray['path_segment'])) {
				$fieldArray['path_segment'] = Transliterator::urlize($fieldArray['title']);
			}
		}
	}

	/**
	 * Prevent deleting/moving of a news record if the editor doesn't have access to all categories of the news record
	 *
	 * @param string $command
	 * @param string $table
	 * @param int $id
	 * @param string $value
	 * @param $parentObject \TYPO3\CMS\Core\DataHandling\DataHandler
	 */
	public function processCmdmap_preProcess($command, &$table, $id, $value, $parentObject)
	{
		if ($table === 'tx_mooxnews_domain_model_news' && !$this->getBackendUser()->isAdmin() && is_int($id) && $command !== 'undelete') {
			$newsRecord = BackendUtilityCore::getRecord($table, $id);
			if (is_array($newsRecord) && !AccessControlService::userHasCategoryPermissionsForRecord($newsRecord)) {
				$parentObject->log($table, $id, 2, 0, 1,
					'processCmdmap: Attempt to ' . $command . " a record from table '%s' without permission. Reason: the record has one or more categories assigned that are not defined in the BE usergroup.",
					1, [$table]);
				// unset table to prevent saving
				$table = '';
			}
		}
	}


	/**
     * Returns the current BE user.
     *
     * @return \TYPO3\CMS\Core\Authentication\BackendUserAuthentication
     */
    protected function getBackendUser()
    {
        return $GLOBALS['BE_USER'];
    }

}