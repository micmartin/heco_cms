<?php
if (!defined('TYPO3_MODE')) {
	die('Access denied.');
}

$ll = 'LLL:EXT:moox_mailer/Resources/Private/Language/locallang_db.xlf:';

/**
 * Add extra fields to the sys_category record
 */
$newSysCategoryColumns = array(
	'mailer_forcesinglemail' => Array (		
		'exclude' => 1,		
		'label' => $ll . 'tx_mooxmailer_domain_model_category.mailer_forcesinglemail',		
		'config' => Array (
			'type' => 'check',
		)
	),
	'mailer_template' => Array (		
			'exclude' => 1,		
			'label' => $ll . 'tx_mooxmailer_domain_model_category.mailer_template',		
			'config' => Array (
				'type' => 'select',
				'items' => Array (
						Array('Kein Template', ''),								
				),
				'size' => 1,	
				'maxitems' => 1,
				'itemsProcFunc' => \Neulandlotsen\MooxMailer\Hooks\TcaFormHelper::class . '->templates',					
			)
	),
	'mailer_subject' => Array (		
			'exclude' => 1,		
			'label' => $ll . 'tx_mooxmailer_domain_model_category.mailer_subject',		
			'config' => Array (
				'type' => 'input'
			)
	),
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('sys_category', $newSysCategoryColumns);
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes('sys_category', '--div--;'.$ll.'tx_mooxmailer_domain_model_news.tabs.mailer, mailer_forcesinglemail,mailer_template,mailer_subject', '', 'after:image');
?>