<?php
defined('TYPO3_MODE') or die();
	
$boot = function ()
{
    // CSH - context sensitive help
    foreach (['news', 'media', 'tag', 'link'] as $table) {
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_mooxnews_domain_model_' . $table);
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr(
            'tx_mooxnews_domain_model_' . $table, 'EXT:moox_news/Resources/Private/Language/locallang_csh_' . $table . '.xlf');
    }

    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr(
        'tt_content.pi_flexform.mooxnews_pi1.list', 'EXT:moox_news/Resources/Private/Language/locallang_csh_flexforms.xlf');

    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr(
        'sys_file_reference', 'EXT:moox_news/Resources/Private/Language/locallang_csh_sys_file_reference.xlf');

    $configuration = \Neulandlotsen\MooxNews\Utility\EmConfiguration::getSettings();
		
    if (TYPO3_MODE === 'BE') {

        // Extend user settings
        $GLOBALS['TYPO3_USER_SETTINGS']['columns']['newsoverlay'] = [
            'label' => 'LLL:EXT:moox_news/Resources/Private/Language/locallang_be.xlf:usersettings.overlay',
            'type' => 'select',
            'itemsProcFunc' => \Neulandlotsen\MooxNews\Hooks\ItemsProcFunc::class . '->user_categoryOverlay',
        ];
        $GLOBALS['TYPO3_USER_SETTINGS']['showitem'] .= ',
            --div--;LLL:EXT:moox_news/Resources/Private/Language/locallang_be.xlf:pi1_title,newsoverlay';

        // Add tables to livesearch (e.g. "#news:fo" or "#newscat:fo")
        $GLOBALS['TYPO3_CONF_VARS']['SYS']['livesearch']['news'] = 'tx_mooxnews_domain_model_news';
        $GLOBALS['TYPO3_CONF_VARS']['SYS']['livesearch']['newstag'] = 'tx_mooxnews_domain_model_tag';
		$GLOBALS['TYPO3_CONF_VARS']['SYS']['livesearch']['newstarget'] = 'tx_mooxnews_domain_model_target';				
		
        /* ===========================================================================
            Register main BE-Module
        =========================================================================== */        		
		
		\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerModule(
			'Neulandlotsen.moox_news',
			"mooxdb",
			'',
			'',
			[],
			[
				'access' => 'user,group',
				'icon'   => 'EXT:moox_news/Resources/Public/Icons/module-main.svg',
				'labels' => 'LLL:EXT:moox_news/Resources/Private/Language/locallang_modmain.xlf',
			]
		);
		
		/*
		if ($configuration->getShowImporter()) {
            \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerModule(
                'Neulandlotsen.moox_news',
                'system',
                'tx_mooxnews_m1',
                '',
                ['Import' => 'index, runJob, jobInfo'],
                [
                    'access' => 'user,group',
                    'icon' => 'EXT:moox_news/Resources/Public/Icons/module_import.svg',
                    'labels' => 'LLL:EXT:moox_news/Resources/Private/Language/locallang_mod.xlf',
                ]
            );
        }
		*/

        /* ===========================================================================
            Register BE-Module for Administration
        =========================================================================== */
        if ($configuration->getShowAdministrationModule()) {
            /*
			\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerModule(
                'Neulandlotsen.moox_news',
                'moox',
                'tx_mooxnews_m2',
                '',
                ['Administration' => 'index,newNews,deleteNews,cloneNews,deleteTranslation,toggleNewsState,toggleTranslationState,newCategory,newTag,newTarget,newsPidListing,moveToFolder,multiple'],
                [
                    'access' => 'user,group',
                    'icon' => 'EXT:moox_news/Resources/Public/Icons/module_administration.svg',
                    'labels' => 'LLL:EXT:moox_news/Resources/Private/Language/locallang_modadministration.xlf',
                    'navigationComponentId' => $configuration->getHidePageTreeForAdministrationModule() ? '' : 'typo3-pagetree'
                ]
            );
			*/
			/**
			 * Registers a Backend Module
			 */
			\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerModule(
				'Neulandlotsen.moox_news',
				'mooxdb',	// Make module a submodule of 'mooxdb'
				'NewsManagement',	// Submodule key
				'',	// Position
				[
					'Mod1' => 'list,refresh,filter,ordering,move,multiple,duplicate,copy,addNewsByTca,import,populate,ics,pdf',
				],
				[
					'access' => 'user,group',
					'icon'   => 'EXT:moox_news/Resources/Public/Icons/module-mod1.svg',
					'labels' => 'LLL:EXT:moox_news/Resources/Private/Language/locallang_mod1.xlf',
				]
			);
			\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerModule(
				'Neulandlotsen.moox_news',
				'mooxdb',	// Make module a submodule of 'mooxdb'
				'CategoryManagement',	// Submodule key
				'',	// Position
				[
					'Mod4' => 'list,refresh,filter,ordering,move',
				],
				[
					'access' => 'user,group',
					'icon'   => 'EXT:moox_news/Resources/Public/Icons/module-mod4.svg',
					'labels' => 'LLL:EXT:moox_news/Resources/Private/Language/locallang_mod4.xlf',
				]
			);
			\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerModule(
				'Neulandlotsen.moox_news',
				'mooxdb',	// Make module a submodule of 'mooxdb'
				'TargetManagement',	// Submodule key
				'',	// Position
				[
					'Mod2' => 'list,refresh,filter,ordering,move',					
				],
				[
					'access' => 'user,group',
					'icon'   => 'EXT:moox_news/Resources/Public/Icons/module-mod2.svg',
					'labels' => 'LLL:EXT:moox_news/Resources/Private/Language/locallang_mod2.xlf',
				]
			);
			\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerModule(
				'Neulandlotsen.moox_news',
				'mooxdb',	// Make module a submodule of 'mooxdb'
				'TagManagement',	// Submodule key
				'',	// Position
				[
					'Mod3' => 'list,refresh,filter,ordering,move',
				],
				[
					'access' => 'user,group',
					'icon'   => 'EXT:moox_news/Resources/Public/Icons/module-mod3.svg',
					'labels' => 'LLL:EXT:moox_news/Resources/Private/Language/locallang_mod3.xlf',
				]
			);			
        }
    }
	
	if ($configuration->getShowPageTemplates())
	{
		\FluidTYPO3\Flux\Core::registerProviderExtensionKey('Neulandlotsen.MooxNews', 'Page');
	}

    /* ===========================================================================
        Default configuration
    =========================================================================== */
    $GLOBALS['TYPO3_CONF_VARS']['EXT']['moox_news']['orderByCategory'] = 'uid,title,tstamp,sorting';
    $GLOBALS['TYPO3_CONF_VARS']['EXT']['moox_news']['orderByNews'] = 'tstamp,datetime,crdate,title' . ($configuration->getManualSorting() ? ',sorting' : '');
    $GLOBALS['TYPO3_CONF_VARS']['EXT']['moox_news']['orderByTag'] = 'tstamp,crdate,title';
};

$boot();
unset($boot);

if (TYPO3_MODE === 'BE')
{
	// reorder main module
	if (isset($TBE_MODULES['MooxNewsMooxdb'])){
		$temp_TBE_MODULES = [];
		$mooxNewsMooxdbCfg = $TBE_MODULES['MooxNewsMooxdb'];
		unset($TBE_MODULES['MooxNewsMooxdb']);
		if (isset($TBE_MODULES['moox']))
		{
			$pos = "moox";
		}
		else
		{
			$pos = "web";
		}
		foreach ($TBE_MODULES as $key => $val)
		{
			if ($key == $pos)
			{	
				if($pos == 'moox')
				{
					$temp_TBE_MODULES['MooxNewsMooxdb'] = $mooxNewsMooxdbCfg;									
				}
				$temp_TBE_MODULES[$key] = $val;  
				if($pos == 'web')
				{
					$temp_TBE_MODULES['MooxNewsMooxdb'] = $mooxNewsMooxdbCfg;														
				}
				
			} 
			else 
			{
				$temp_TBE_MODULES[$key] = $val;
			}
		}
		$TBE_MODULES = $temp_TBE_MODULES;
	}
}

/***************
 * ###Neulandlotsen### Added custom stylesheet to modify tca form elements (Dominic)
 */
//$TBE_STYLES['styleSheetFile_post'] = \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath($_EXTKEY) . 'Resources/Public/Css/backend_overwrite.css';


?>