<?php
$EM_CONF[$_EXTKEY] = [
	'title' => 'MOOX News grabber tt_news (Heco)',
	'description' => 'News von tt_news (Heco) importieren',
	'category' => 'services',
	'state' => 'beta',
	'uploadfolder' => false,
	'clearCacheOnLoad' => 1,
	'author' => 'Dominic Martin',
	'author_email' => 'typo3@neulandlotsen.de',
	'author_company' => 'Neulandlotsen',
	'version' => '9.5.0',
	'constraints' => [
		'depends' => [
			'moox_news' => '9.5.0-9.5.99'
		],
		'conflicts' => [],
		'suggests' => []
	],
	'autoload' => [
		'psr-4' => [
			'Neulandlotsen\\MooxNewsGrabberTtNewsHeco\\' => 'Classes'
		]
	]
];

?>
