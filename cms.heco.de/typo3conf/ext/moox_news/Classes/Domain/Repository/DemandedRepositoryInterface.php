<?php

namespace Neulandlotsen\MooxNews\Domain\Repository;

/**
 * This file is part of the "moox_news" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */
use Neulandlotsen\MooxNews\Domain\Model\DemandInterface;

/**
 * Demand domain model interface
 *
 */
interface DemandedRepositoryInterface
{
    public function findDemanded(DemandInterface $demand, $respectEnableFields = true);

    public function countDemanded(DemandInterface $demand);
}
