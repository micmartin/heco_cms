<?php
namespace Neulandlotsen\MooxNews\Domain\Repository\Backend;

/**
 * This file is part of the "moox_news" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

use TYPO3\CMS\Extbase\Persistence\QueryInterface; 
use Neulandlotsen\MooxNews\Domain\Repository\MooxRepository;

class TargetRepository extends MooxRepository
{	
	protected $defaultOrderings = ['title' => QueryInterface::ORDER_ASCENDING];
	
	/**
	 * Returns a constraint array created by a given filter array
	 *
	 * @param \TYPO3\CMS\Extbase\Persistence\QueryInterface $query
	 * @param array $filter
	 * @param array $constraints	
	 * @return \TYPO3\CMS\Extbase\Persistence\Generic\Qom\ConstraintInterface|null
	 */
	protected function createFilterConstraints(\TYPO3\CMS\Extbase\Persistence\QueryInterface $query,$filter = NULL,$constraints = NULL)
	{				
		if(is_null($constraints))
		{			
			$constraints = [];			
		}
		
		if(isset($filter['hidden']) && in_array($filter['hidden'],[0,1]))
		{			
			$constraints[] = $query->equals('hidden', $filter['hidden']);			
		}
		
		$searchFields = ['title'];
		if(isset($filter['query']) && $filter['query']!="")
		{			
			$searchConstraints = [];
			foreach($searchFields AS $searchField)
			{
				$searchConstraints[] =  $query->like($searchField, "%".$filter['query']."%");
			}
			
			if(count($searchConstraints)>0)
			{
				if(count($searchConstraints)==1)
				{
					$constraints[] = $searchConstraints[0];
				} 
				else 
				{
					$constraints[] = $query->logicalOr($searchConstraints);
				}
			}			
		}
		
		if(count($constraints)<1)
		{			
			$constraints = NULL;			
		}
		
		return $constraints;
	}
	
	/**
     * Find target by a given pid
     *
     * @param int $pid pid
     * @return QueryInterface
     */
    public function findByPid($pid)
    {
        $query = $this->createQuery();
        $query->getQuerySettings()->setRespectStoragePage(false);
		$query->matching(
			$query->equals('pid', (int)$pid)
		);
		$query->setOrderings(
			[
				'title' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING
			]
		);
		return $query->execute();
    }
	
	/**
     * Find targets by a given pid and uids
     *
     * @param int $pid pid
	 * @param array $uids uids
     * @return QueryInterface
     */
    public function findByPidAndUids($pid,$uids = [])
    {
        $query = $this->createQuery();
        $query->getQuerySettings()->setRespectStoragePage(false);
		$query->matching(
			$query->equals('pid', (int)$pid),
			(count($uids))?$query->in('uid', $uids):NULL
		);
		$query->setOrderings(
			[
				'title' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING
			]
		);
		return $query->execute();
    }
}
?>