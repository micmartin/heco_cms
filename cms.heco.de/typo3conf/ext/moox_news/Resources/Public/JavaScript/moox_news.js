// In your Javascript (external .js resource or <script> tag)
$(document).ready(function() {
    $('.news .select2').select2({
        minimumResultsForSearch: -1
    });
    $('.news input.filter-dateFrom, .news input.filter-dateTo').attr('autocomplete','off');
    if($('#news-filter-form').hasClass('auto-submit')){
        $( "#news-filter-form select.filter" ).on( "change", function() {
            form = $(this).parents('#news-filter-form');
            form.submit();
        });
        $( "#news-filter-form input[type=text].filter" ).on( "keydown", function(event) {
           if(event.key == "Enter"){
                form = $(this).parents('#news-filter-form');
                form.submit();
            }
        });
        $( "#news-filter-form input[type=text].filter" ).on( "change", function() {
            form = $(this).parents('#news-filter-form');
            form.submit();
        });
    }
    $(document).on("keydown", "#news-filter-form", function(event) {
        return event.key != "Enter";
    });
    var today = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate());
    config = {
        locale: 'de-de',
        uiLibrary: 'bootstrap4'
    };
    var $mooxNewsDatepickerFrom = $('.news input.filter-dateFrom').datepicker({
        locale: 'de-de',
        format: 'dd.mm.yyyy',
        uiLibrary: 'bootstrap4',
        iconsLibrary: 'fontawesome',
        minDate: today,
        maxDate: function () {
            return $('.news input.filter-dateTo').val();
        }
    });
    var $mooxNewsDatepickerTo = $('.news input.filter-dateTo').datepicker({
        locale: 'de-de',
        format: 'dd.mm.yyyy',
        uiLibrary: 'bootstrap4',
        iconsLibrary: 'fontawesome',
        minDate: function () {
            return $('.news input.filter-dateFrom').val();
        }
    });
    /*
    $( "input-group.input-group-clearable" ).on( "click", function() {
        console.log( $( this ).text() );
    });
     */
});
