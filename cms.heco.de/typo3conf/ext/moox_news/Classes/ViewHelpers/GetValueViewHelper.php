<?php
namespace Neulandlotsen\MooxNews\ViewHelpers;

/**
 * This file is part of the "moox_news" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper;
use \TYPO3\CMS\Core\Utility\GeneralUtility;
 
class GetValueViewHelper extends AbstractViewHelper
{
	public function initializeArguments()
	{
		parent::initializeArguments();
		$this->registerArgument('item', 'mixed', 'source object/array', true);
		$this->registerArgument('field', 'string', 'field to return', true);
		$this->registerArgument('raw', 'boolean', 'return raw data', false, false);
	}

	/**
	 * @return mixed $value
	 */
	public function render()
	{
		$item = $this->arguments['item'];
		$field = $this->arguments['field'];
		$raw = $this->arguments['raw'];
		$value = "";

		if($field != "")
		{
			if(is_array($item))
			{
				$value = $item[$field];
			}
			elseif(is_object($item))
			{
				$getCall = "get".GeneralUtility::underscoredToUpperCamelCase($field);
				if(method_exists($item, $getCall))
				{
				    if($raw)
				    {
				        $value = $item->$getCall();
					} else {
						$value = $item->$getCall('preview');
					}
				} elseif(is_array($item))
				{
					$value = $item[$field];
				}
			}
		}
		
		return $value;
	}
}
