<?php
namespace Neulandlotsen\MooxNews\Controller;

/**
 * This file is part of the "moox_news" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

class RedirectorController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{				
	/**
	 * action redirect
	 *	
	 * @param int $pid
	 * @param int $uid	
	 */
	public function redirectAction($pid = 0, $uid = 0) 
	{		
		
		if($pid>0 && $uid>0)
		{								
			$uri = $this->uriBuilder->setTargetPageUid($pid)->setCreateAbsoluteUri(true)->uriFor('detail', array("news" => $uid), 'News', 'MooxNews', 'Pi1');			
			//$uri = str_replace("//","/",$uri);
			$this->redirectToURI($uri);						
		} 
		else 
		{
			echo "can't redirect to news item";
			exit();
		}
	}
}
?>