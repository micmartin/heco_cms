<?php
namespace Neulandlotsen\MooxNewsSeminar\Service;

/**
 * This file is part of the "moox_news_seminar" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

use TYPO3\CMS\Core\SingletonInterface;

//TODO: check if this class used und use session class instead
class BackendSessionService implements SingletonInterface
{		
	/**
     * @var string 
     */
	protected $storageKey = 'tx_mooxnewsseminar';

	/**
      * @param string $storageKey 
	  */
	public function setStorageKey($storageKey)
	{ 
		$this->storageKey = $storageKey; 
	}

	/**
	  * @param string $key
	  * @param mixed $value
	  * @return bool
	  */
	public function store($key, $value)
	{
		$data = $GLOBALS['BE_USER']->getSessionData($this->storageKey);
		$data[$key] = $value;
		return $GLOBALS['BE_USER']->setAndSaveSessionData($this->storageKey, $data);
	}

	/**
	  * @param string $key
	  * @return bool
	  */
	public function delete($key)
	{
		$data = $GLOBALS['BE_USER']->getSessionData($this->storageKey);
		unset($data[$key]);
		return $GLOBALS['BE_USER']->setAndSaveSessionData($this->storageKey, $data);
	}


	/**
	  * @param string $key
	  * @return mixed
	  */
	public function get($key)
	{
		$data = $GLOBALS['BE_USER']->getSessionData($this->storageKey);
		return isset($data[$key]) ? $data[$key] : NULL;
	}
}
?>