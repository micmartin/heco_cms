<?php
if (!defined('TYPO3_MODE'))
{
	die ('Access denied.');
}

// extend moox_news classes
$GLOBALS['TYPO3_CONF_VARS']['EXT']['moox_news']['classes']['Domain/Model/News'][] = 'moox_template_heco';

if (\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::isLoaded('flux'))
{
	// register extension as page provider
	\FluidTYPO3\Flux\Core::registerProviderExtensionKey("Neulandlotsen.MooxTemplateHeco", 'Page');

	// register extension as content provider
	\FluidTYPO3\Flux\Core::registerProviderExtensionKey("Neulandlotsen.MooxTemplateHeco", 'Content');
}

?>