<?php

/***************************************************************
 * Extension Manager/Repository config file for ext "min".
 *
 * Auto generated 18-07-2019 11:48
 *
 * Manual updates:
 * Only the data in the array - everything else is removed by next
 * writing. "version" and "dependencies" must not be touched!
 ***************************************************************/

$EM_CONF[$_EXTKEY] = array (
  'title' => 'Minifier for TYPO3',
  'description' => 'Extends TYPO3\'s compressor for JS and CSS with minifier. This may save you up to 60% of default compressed file size. Also compresses HTML output of TYPO3. Full composer support.',
  'category' => 'services',
  'author' => 'Armin Vieweg',
  'author_email' => 'armin@v.ieweg.de',
  'state' => 'stable',
  'uploadfolder' => false,
  'createDirs' => '',
  'clearCacheOnLoad' => 0,
  'version' => '1.7.0',
  'constraints' => 
  array (
    'depends' => 
    array (
      'php' => '7.0.0',
      'typo3' => '7.6.0-9.5.99',
    ),
    'conflicts' => 
    array (
      'tinysource' => '',
    ),
    'suggests' => 
    array (
    ),
  ),
  'clearcacheonload' => false,
  'author_company' => NULL,
);

