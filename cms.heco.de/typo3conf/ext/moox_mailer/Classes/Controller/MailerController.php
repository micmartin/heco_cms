<?php
namespace Neulandlotsen\MooxMailer\Controller;

/**
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */

use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Database\Query\Restriction\DeletedRestriction;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Utility\HttpUtility;
use TYPO3\CMS\Core\Messaging\FlashMessage;
use Neulandlotsen\MooxMailer\Service\CronjobService;
use TYPO3\CMS\Core\Utility\RootlineUtility;

//require_once ExtensionManagementUtility::extPath('moox_mailer','Classes/Service/CronjobService.php'); 
 
class MailerController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController 
{
	
	static public $weekdays = array("Sonntag","Montag","Dienstag","Mittwoch","Donnerstag","Freitag","Samstag");
	
	/**
	 * frontendUserRepository
	 *
	 * @var \Neulandlotsen\MooxMailer\Domain\Repository\FrontendUserRepository
	 */
	protected $frontendUserRepository;
	
	/**
	 * frontendUserGroupRepository
	 *
	 * @var \Neulandlotsen\MooxMailer\Domain\Repository\FrontendUserGroupRepository
	 */
	protected $frontendUserGroupRepository;
	
	/**
	 * scheduleRepository
	 *
	 * @var \Neulandlotsen\MooxMailer\Domain\Repository\ScheduleRepository
	 */
	protected $scheduleRepository;
	
	/**
	 * templateRepository
	 *
	 * @var \Neulandlotsen\MooxMailer\Domain\Repository\TemplateRepository
	 */
	protected $templateRepository;
	
	/**
	 * backendSession
	 *
	 * @var \Neulandlotsen\MooxMailer\Service\BackendSessionService
	 */
	protected $backendSession;
	
	/**
	 * newsRepository
	 *
	 * @var \Neulandlotsen\MooxNews\Domain\Repository\NewsRepository
	 */
	protected $newsRepository;
	
	/**
	 * categoryRepository
	 *
	 * @var \Neulandlotsen\MooxNews\Domain\Repository\CategoryRepository
	 */
	protected $categoryRepository;
	
	/**
	 * addressRepository
	 *
	 * @var \Neulandlotsen\MooxMailer\Domain\Repository\AddressRepository
	 */
	protected $addressRepository;
	
	/**
	 * newsletterRepository
	 *
	 * @var \Neulandlotsen\MooxMailer\Domain\Repository\NewsletterRepository
	 */
	protected $newsletterRepository;
	
	/**
	 * deliveryRepository
	 *
	 * @var \Neulandlotsen\MooxMailer\Domain\Repository\DeliveryRepository
	 */
	protected $deliveryRepository;
	
	/**
	 * clickRepository
	 *
	 * @var \Neulandlotsen\MooxMailer\Domain\Repository\ClickRepository
	 */
	protected $clickRepository;
	
	/**
	 * zipregionRepository
	 *
	 * @var \Neulandlotsen\MooxMailer\Domain\Repository\ZipregionRepository
	 */
	protected $zipregionRepository;
	
	/**
	 * pageRepository
	 *
	 * @var \TYPO3\CMS\Frontend\Page\PageRepository
	 */
	protected $pageRepository;
	
	/**
	 * extConf
	 *
	 * @var array
	 */
	protected $extConf;
	
	/**
	 * newsInstalled
	 *
	 * @var bool
	 */
	protected $newsInstalled;
		
	/**
	 * page
	 *
	 * @var int
	 */
	protected $page;
	
	// copy a directory and all subdirectories and files (recursive)
	// void dircpy( str 'source directory', str 'destination directory' [, bool 'overwrite existing files'] )
	public static function dircpy($basePath, $source, $dest, $overwrite = false)
	{
		if(!is_dir($basePath . $dest)) //Lets just make sure our new folder is already created. Alright so its not efficient to check each time... bite me
		mkdir($basePath . $dest);
		if($handle = opendir($basePath . $source)){        // if the folder exploration is sucsessful, continue
			while(false !== ($file = readdir($handle))){ // as long as storing the next file to $file is successful, continue
				if($file != '.' && $file != '..'){
					$path = $source . '/' . $file;
					if(is_file($basePath . $path)){
						if(!is_file($basePath . $dest . '/' . $file) || $overwrite)
						if(!@copy($basePath . $path, $basePath . $dest . '/' . $file)){
							echo '<font color="red">File ('.$path.') could not be copied, likely a permissions problem.</font>';
						}
					} elseif(is_dir($basePath . $path)){
						if(!is_dir($basePath . $dest . '/' . $file))
						mkdir($basePath . $dest . '/' . $file); // make subdirectory before subdirectory is copied
						self::dircpy($basePath, $path, $dest . '/' . $file, $overwrite); //recurse!
					}
				}
			}
			closedir($handle);
		}
	}
	
	/**
	 * Clean up ziplist
	 *
	 * @param string $list
	 * @return void
	 */
	public static function cleanUpZiplist($list) 
	{		
		$cleaned = "";
		$zipcodes = [];
		$list = trim($list);
		if($list != ''){
			$list = str_replace(";",",",$list);
			$zipcodes_tmp = explode(",",$list);
			foreach($zipcodes_tmp AS $zipcode){
				if(is_numeric(trim($zipcode))){
					$zipcodes[] = trim($zipcode);
				}
			}
			if(count($zipcodes)){
				sort($zipcodes,SORT_STRING);
				$cleaned = implode(",",$zipcodes);
			}
		}
		return $cleaned;
	}
	
	/**
	 * initialize the controller
	 *
	 * @return void
	 */
	protected function initializeAction() 
	{
		parent::initializeAction();		
		
		$this->setPage((int)GeneralUtility::_GET('id'));
		$this->extConf = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Configuration\ExtensionConfiguration::class)->get('moox_mailer');
		$this->newsInstalled = ExtensionManagementUtility::isLoaded('moox_news');
		if(ExtensionManagementUtility::isLoaded('moox_address')){
			$this->addressRepository = $this->objectManager->get('Neulandlotsen\\MooxMailer\\Domain\\Repository\\AddressRepository');
		}
		$this->pageRepository = $this->objectManager->get('TYPO3\\CMS\\Frontend\\Page\\PageRepository');
		$this->templateRepository = $this->objectManager->get('Neulandlotsen\\MooxMailer\\Domain\\Repository\\TemplateRepository');			
		$this->scheduleRepository = $this->objectManager->get('Neulandlotsen\\MooxMailer\\Domain\\Repository\\ScheduleRepository');
		$this->newsletterRepository = $this->objectManager->get('Neulandlotsen\\MooxMailer\\Domain\\Repository\\NewsletterRepository');
		$this->deliveryRepository = $this->objectManager->get('Neulandlotsen\\MooxMailer\\Domain\\Repository\\DeliveryRepository');
		$this->clickRepository = $this->objectManager->get('Neulandlotsen\\MooxMailer\\Domain\\Repository\\ClickRepository');
		$this->zipregionRepository = $this->objectManager->get('Neulandlotsen\\MooxMailer\\Domain\\Repository\\ZipregionRepository');
		if($this->newsInstalled) {
			$this->newsRepository = $this->objectManager->get('Neulandlotsen\\MooxNews\\Domain\\Repository\\NewsRepository');
			$this->categoryRepository = $this->objectManager->get('Neulandlotsen\\MooxNews\\Domain\\Repository\\CategoryRepository');	
		}
		$this->frontendUserRepository = $this->objectManager->get('Neulandlotsen\\MooxMailer\\Domain\\Repository\\FrontendUserRepository');
		$this->frontendUserGroupRepository = $this->objectManager->get('Neulandlotsen\\MooxMailer\\Domain\\Repository\\FrontendUserGroupRepository');
		$this->backendSession = $this->objectManager->get('Neulandlotsen\\MooxMailer\\Service\\BackendSessionService');
		$this->newsInstalled = ExtensionManagementUtility::isLoaded('moox_news');
	}
	
	/**
	 * action show tasks
	 *
	 * @param int $uid
	 * @param string $redirector
	 * @return void
	 */
	public function indexAction($uid = 0, $redirector = "")
	{			
		$reset 	= (int)GeneralUtility::_GET('reset');
		
		if(strlen($this->page)>0){
			$this->page = (int)$this->page;
		}
		if($this->page==0 && $this->backendSession->get("id")!==FALSE){
			$this->page = (int)$this->backendSession->get("id");
			if($this->page>0){				
				HttpUtility::redirect($_SERVER['REQUEST_URI']."&id=".$this->page);
			} elseif((int)$this->settings['newsletterStartPid']>0 && $init) {
				$this->page = (int)$this->settings['newsletterStartPid'];
				HttpUtility::redirect($_SERVER['REQUEST_URI']."&id=".$this->page);
			} else {
				$this->page = 0;
			}
		} 
		
		if(is_dir($GLOBALS["_SERVER"]["DOCUMENT_ROOT"].'/maildir')){					
			
			if((int)$this->settings['showDraftsCount']<1){
				$draftsCnt = 1;
			} else {
				$draftsCnt = (int)$this->settings['showDraftsCount'];
			}
			if((int)$this->settings['showSentCount']<1){
				$sentCnt = 1;
			} else {
				$sentCnt = (int)$this->settings['showSentCount'];
			}
			
			if((string)$this->backendSession->get("noMailerFolders")=="" || 0){
				$mailerFolders = $this->getFolders("mxmailer");
				if(!count($mailerFolders)){
					$this->addFlashMessage(
						'Es empfiehlt sich einen gesonderten Mailer-Ordner �ber den Seitenbaum anzulegen. Mailer-Ordner dienen zur gruppierten Ablage von Newsletter, Vorlagen und Zeitpl�nen. Au�erdem k�nnen Sie innerhalb eines Mailer-Ordners Entwurfs- und Archiv-Ordner anlegen. Sie k�nnen verschiedene Voreinstellungen pro Mailer-Ordner festlegen und somit �ber das Anlegen verschiedener Mailer-Ordner, verschiedene Varianten von Newslettern verwalten.',
						'Keine Mailer-Ordner gefunden', 					 
						FlashMessage::WARNING
					);					
				}
				$this->backendSession->store("noMailerFolders","shown");
			}
			
			$this->view->assign('tasks', $this->getMailerTasks());
			$this->view->assign('drafts', $this->newsletterRepository->findDrafts($draftsCnt));
			$this->view->assign('sentNewsletters', $this->newsletterRepository->findSentNewsletters($sentCnt));
			
			$daemonTask = $this->getDaemonMailerTask();			
			if(!$daemonTask['taskUid']){
				$this->addFlashMessage(
					'Der Scheduler-Task für den zyklischen Mail-Versand (Daemon) wurde noch nicht eingerichtet. Ohne den Task werden Emails beim Versenden nur in eine Warteschlange eingereiht, aber nicht wirklich versendet. Beachten Sie das nach dem Einrichten des Tasks alle Emails versendet werden die sich bereits in der Warteschlange befinden. Bei Fragen hierzu wenden Sie sich an Ihren Administrator.',
					'Newsletter-Daemon nicht eingerichtet oder deaktiviert', 					 
					FlashMessage::WARNING
				);
			}
			
			if(!function_exists("imap_open")){
				$this->addFlashMessage(
					'Die Funktion "imap_open" die für den Abruf von Bounce-Mails benötigt werden, ist auf dem Server nicht installiert.',
					'Bounce-Handling', 					 
					FlashMessage::WARNING
				);
			}
			
			if(!function_exists("proc_open")){
				$this->addFlashMessage(
					'Die Funktionen "proc_*" wie z.B. "proc_open" die von TYPO3 für den reibungslosen Versand benötigt wird, ist auf dem Server nicht installiert.',
					'Mail-Versand', 					 
					FlashMessage::WARNING
				);
			}
			
			if($uid>0 && $redirector=="executeTask"){
				$currentTask 	= $this->getMailerTask($uid);
				$task 			= $currentTask['task'];
				
				// initiate mailer job
				$mailer = $this->objectManager->get('Neulandlotsen\MooxMailer\Service\MooxNewsMailerService');
				$mailer->initialize($task);
				
				$lastAction = $this->getLastAction('task'.$currentTask['taskUid'],'log');
				
				if($lastAction['logcontent']==""){
					$lastAction['logcontent'] = "No logfile written yet";
				}
				
				$this->view->assign('log', nl2br($lastAction['logcontent']));
				$this->view->assign('redirector', $redirector);
				$this->view->assign('isTask', true);
				$this->view->assign('taskUid', $uid);
				
			} elseif($uid>0 && $redirector=="previewNewsTask"){
				
				$currentTask 	= $this->getMailerTask($uid);
				$task 			= $currentTask['task'];
				
				// Set global settings
				$task->setMailerDir($GLOBALS["_SERVER"]["DOCUMENT_ROOT"].'/maildir');
				$task->setIsPreview(true);		
				$task->setCountMode('live');
				$task->setScriptTimeout(90);
				$task->setMemoryLimit(1024);
				$task->setTimestamp(time());
				$task->setNoLog(false);
			
				// initiate mailer job
				$mailer = $this->objectManager->get('Neulandlotsen\MooxMailer\Service\MooxNewsMailerService');
				$mailer->initialize($task);				
				
				$maildata = $mailer->prepareMaildata();					
				$previewMaildata = $mailer->getPreviewMaildata();
						
				$this->view->assign('mails', $previewMaildata);
				$this->view->assign('uid', $uid);
				$this->view->assign('redirector', $redirector);
				$this->view->assign('isTask', true);
				$this->view->assign('taskUid', $uid);
				
			} elseif($uid>0 && $redirector=="previewTask"){
				
				$currentTask = $this->getMailerTask($uid);
				$task = $currentTask['task'];
				
				// Set global settings
				$task->setMailerDir($GLOBALS["_SERVER"]["DOCUMENT_ROOT"].'/maildir');
				$task->setIsPreview(true);		
				$task->setCountMode('live');
				$task->setScriptTimeout(90);
				$task->setMemoryLimit(1024);
				$task->setTimestamp(time());
				$task->setNoLog(true);
			
				// initiate mailer job
				$mailer = $this->objectManager->get('Neulandlotsen\MooxMailer\Service\MooxNewsMailerService');
				$mailer->initialize($task);
						
				$previewData = $mailer->preview();
		
				$this->view->assign('preview', $previewData);
				$this->view->assign('redirector', $redirector);
				$this->view->assign('isTask', true);
				$this->view->assign('taskUid', $uid);
				
			} elseif($uid>0 && $redirector=="previewNewsletter"){
				
				$newsletter = $this->newsletterRepository->findByUid($uid,FALSE);
				
				$userFolders = array();
				if($newsletter->getUserFolders()!=""){
					$userFolders = unserialize($newsletter->getUserFolders());
				}				
				if($newsletter->getAutologin() && count($userFolders)>0){
					$newsletterController = $this->objectManager->get('Neulandlotsen\MooxMailer\Controller\NewsletterController');
					$affectedUsers = $newsletterController->getFeUsers($userFolders);				
				}
				if(strpos($edit['currentUserEmail'],"@")!==FALSE){
					$currentUser = $affectedUsers[$edit['currentUserEmail']];
				}
				$this->view->assign('users', $affectedUsers);
				$this->view->assign('object', $this->newsletterRepository->findByUid($uid,FALSE));
				$this->view->assign('redirector', $redirector);				
			} elseif($uid>0 && $redirector=="showSentNewsletter"){
								
				$this->view->assign('object', $this->newsletterRepository->findByUid($uid,FALSE));
				$this->view->assign('redirector', $redirector);				
			} elseif($uid>0 && in_array($redirector,array("showDraftReceivers","showSentReceivers"))){
								
				$object = $this->newsletterRepository->findByUid($uid,FALSE);
				
				if($this->extConf['useMailerTracking'] && $redirector=="showSentReceivers"){
					$users = $this->deliveryRepository->findByJobAndType($object->getJob(),"feuser");
					$usersCount = $users->count();
					$this->view->assign('receiverUsers', $users);
					$addresses = $this->deliveryRepository->findByJobAndType($object->getJob(),"address");
					$addressesCount = $addresses->count();
					$this->view->assign('receiverAddresses', $addresses);
				} else {
				
					if($object->getUserFolders()!=""){
						$userFolders = unserialize($object->getUserFolders());
						if(count($userFolders)){
							$users_tmp = $this->frontendUserRepository->findByPids($userFolders,true);
							if($object->getZipmode()=='list' && $object->getZiplist()!=''){
								$users = [];
								$ziplist = explode(",",$object->getZiplist());
								foreach($users_tmp AS $testUser){
									if($testUser['zip']!='' && in_array($testUser['zip'],$ziplist)){
										$users[] = $testUser;
									}
								}
								$usersCount = count($users);
							} elseif($object->getZipmode()!='' && is_numeric($object->getZipmode())){
								$users = [];
								$zipregion = $this->zipregionRepository->findByUid($object->getZipmode());
								if(is_object($zipregion)){
									$ziplist = [];
									$ziplist_cleaned = self::cleanUpZiplist($zipregion->getList());
									if($zipregion->getMode()=='list' && $ziplist_cleaned!=''){
										$ziplist_tmp = explode(",",$ziplist_cleaned);
										foreach($ziplist_tmp AS $zip){
											if(is_numeric(trim($zip))){
												$ziplist[] = trim($zip);
											}
										}
									}
									foreach($users_tmp AS $testUser){
										if(($zipregion->getMode()=='area' && (int)$zipregion->getLower()>=0 && (int)$zipregion->getUpper()>0) || ($zipregion->getMode()=='list' AND count($ziplist))){
											if($testUser['zip']!='' && ($zipregion->getMode()=='area' && (int)$testUser['zip']>=(int)$zipregion->getLower() && (int)$testUser['zip']<=(int)$zipregion->getUpper()) || ($zipregion->getMode()=='list' && in_array((int)$testUser['zip'],$ziplist))){										
												$users[] = $testUser;
											}
										}
									}
								}
								$usersCount = count($users);
							} else {
								$users = $users_tmp;
								$users_tmp = NULL;
								$usersCount = count($users);
							}
							$this->view->assign('receiverUsers', $users);
						}
					}
					
					if($object->getAddressFolders()!="" && ExtensionManagementUtility::isLoaded('moox_address')){
						$addressFolders = unserialize($object->getAddressFolders());					
						if(count($addressFolders)){
							$addresses_tmp = $this->addressRepository->findByPids($addressFolders);
							
							if($object->getZipmode()=='list' && $object->getZiplist()!=''){
								$addresses = [];
								$ziplist = explode(",",$object->getZiplist());								
								foreach($addresses_tmp AS $testAddress){
									if($testAddress->getZip()!='' && in_array($testAddress->getZip(),$ziplist)){
										$addresses[] = $testAddress;
									}
								}
								$addressesCount = count($addresses);
							} elseif($object->getZipmode()!='' && is_numeric($object->getZipmode())){
								$addresses = [];
								$zipregion = $this->zipregionRepository->findByUid($object->getZipmode());
								if(is_object($zipregion)){
									$ziplist = [];
									$ziplist_cleaned = self::cleanUpZiplist($zipregion->getList());
									if($zipregion->getMode()=='list' && $ziplist_cleaned!=''){
										$ziplist_tmp = explode(",",$ziplist_cleaned);
										foreach($ziplist_tmp AS $zip){
											if(is_numeric(trim($zip))){
												$ziplist[] = trim($zip);
											}
										}
									}
									foreach($addresses_tmp AS $testAddress){
										if(($zipregion->getMode()=='area' && (int)$zipregion->getLower()>=0 && (int)$zipregion->getUpper()>0) || ($zipregion->getMode()=='list' AND count($ziplist))){
											if($testAddress->getZip()!='' && ($zipregion->getMode()=='area' && (int)$testAddress->getZip()>=(int)$zipregion->getLower() && (int)$testAddress->getZip()<=(int)$zipregion->getUpper()) || ($zipregion->getMode()=='list' && in_array((int)$testAddress->getZip(),$ziplist))){										
												$addresses[] = $testAddress;
											}
										}
									}
								}
								$addressesCount = count($addresses);
							} else {
								$addresses = $addresses_tmp;
								$addresses_tmp = NULL;
								$addressesCount = $addresses->count();
							}
							
							$this->view->assign('receiverAddresses', $addresses);
						}
					}
				}
				$this->view->assign('count', ($usersCount+$addressesCount));
				$this->view->assign('usersCount', (int)$usersCount);
				$this->view->assign('addressesCount', (int)$addressesCount);
				$this->view->assign('object', $object);
				$this->view->assign('redirector', $redirector);				
			} elseif($uid>0 && $redirector=="showSentStats"){
				$newsletter = $this->newsletterRepository->findByUid($uid,FALSE);
				$this->view->assign('object', $newsletter);				
				$this->view->assign('clicks', $this->clickRepository->findTopXByJob($newsletter->getJob(),true,10));
				$this->view->assign('redirector', $redirector);				
			}

		} else {
			$this->view->assign('noFolders',1);
		}
		if($this->extConf['adminUsergroupUid']>1){
			$this->view->assign('role',$this->extConf['adminUsergroupUid']);
		} elseif($this->extConf['adminUsergroup']!=""){
			$this->view->assign('role',$this->extConf['adminUsergroup']);
		} else{
			$this->view->assign('role',0);
		}
		$this->view->assign('newsInstalled',$this->getNewsInstalled());
		$this->view->assign('page',$this->page);
		$this->view->assign('extConf', $this->extConf);
	}
	
	/**
	 * action show tasks
	 *
	 * @return void
	 */
	public function showTaskAction() 
	{			
		if(is_dir($GLOBALS["_SERVER"]["DOCUMENT_ROOT"].'/maildir')){
		
			$currentTaskUid = $this->backendSession->get("taskUid");
			
			if($currentTaskUid){
				$currentTask = $this->getMailerTask($currentTaskUid);
			} else {
				$firstTask = $this->getFirstMailerTask();
				if($firstTask['taskUid']>0){
					$currentTask = $this->getMailerTask($firstTask['taskUid']);
				}
			}
			
			$task = $currentTask['task'];
			
			if($task){

				$daemonTask = $this->getDaemonMailerTask();
				
				// Set global settings
				$task->setMailerDir($GLOBALS["_SERVER"]["DOCUMENT_ROOT"].'/maildir');
				$task->setIsPreview(true);		
				$task->setCountMode('live');
				$task->setScriptTimeout(90);
				$task->setMemoryLimit(1024);
				$task->setTimestamp(time());
				$task->setNoLog(false);
			
				// initiate mailer job
				$mailer = $this->objectManager->get('Neulandlotsen\MooxMailer\Service\MooxNewsMailerService');
				$mailer->initialize($task);
				
				$maildata = $mailer->prepareMaildata();
				
				$affectedUsers = $mailer->getAffectedUsers();
				$affectedNews = $mailer->getAffectedNews();
				
				$schedule = $mailer->getSchedule();
				
				$this->view->assign('schedule', $schedule);
							
				$this->view->assign('mailCount', count($maildata));
				$this->view->assign('newsCount', count($affectedNews));
				$this->view->assign('userCount', count($affectedUsers));
				$this->view->assign('currentTask', $currentTask);
				$this->view->assign('daemonTask', $daemonTask);
				$this->view->assign('tasks', $this->getMailerTasks());
				$this->view->assign('extConf', $this->extConf);
				
				
				if($mailer->getEnableMailFilter()){				
					$this->view->assign('whitelist', $mailer->getWhitelist());
				}
			}
			
		} else {
			$this->view->assign('noFolders',1);
		}
	}	

	/**
	 * action show news
	 *
	 * @return void
	 */
	public function showNewsAction() 
	{					
		$currentTaskUid = $this->backendSession->get("taskUid");
		
		if($currentTaskUid){
			$currentTask = $this->getMailerTask($currentTaskUid);
		} else {
			$firstTask = $this->getFirstMailerTask();
			$currentTask = $this->getMailerTask($firstTask['taskUid']);
		}
		
		$task = $currentTask['task'];
		
		$currentUserEmail = $this->backendSession->get("userEmail");	

		// Set global settings
		$task->setMailerDir($GLOBALS["_SERVER"]["DOCUMENT_ROOT"].'/maildir');
		$task->setIsPreview(true);		
		$task->setCountMode('live');
		$task->setScriptTimeout(90);
		$task->setMemoryLimit(1024);
		$task->setTimestamp(time());
		$task->setNoLog(false);
	
		// initiate mailer job
		$mailer = $this->objectManager->get('Neulandlotsen\MooxMailer\Service\MooxNewsMailerService');
		$mailer->initialize($task);
		
		$maildata 			= $mailer->prepareMaildata();		
		$affectedUsers 		= $mailer->getAffectedUsers();
		$affectedNews		= $mailer->getAffectedNews();
				
		$this->view->assign('currentTask', $currentTask);		
		if(strpos($currentUserEmail,"@")!==FALSE){
			$this->view->assign('currentUser', $affectedUsers[$currentUserEmail]);
			$affectedNewsUids 		= $mailer->getAffectedNewsUidsByEmail();
			$affectedNewsUids 		= $affectedNewsUids[$currentUserEmail];
			$affectedNewsByFeUser	= array();
			foreach($affectedNews AS $uid => $news){
				if(is_array($affectedNewsUids)){
					if(in_array($uid,$affectedNewsUids)){
						$affectedNewsByEmail[] = $news;
					}
				}
			}
			$this->view->assign('news', $affectedNewsByEmail);
		} else {
			$this->view->assign('news', $affectedNews);
		}
		$this->view->assign('users', $affectedUsers);
		$this->view->assign('tasks', $this->getMailerTasks());
	}		

	/**
	 * action get user list ajax
	 *
	 * @return void
	 */
	public function getUserListAjaxAction() 
	{					
		$this->backendSession = $this->objectManager->get('Neulandlotsen\MooxMailer\Service\SessionService');
		
		$currentTaskUid = $this->backendSession->get("taskUid");
		
		if($currentTaskUid){
			$currentTask = $this->getMailerTask($currentTaskUid);
		} else {
			$firstTask = $this->getFirstMailerTask();
			$currentTask = $this->getMailerTask($firstTask['taskUid']);
		}
		
		$task = $currentTask['task'];
		
		$currentUserEmail = $this->backendSession->get("userEmail");		
		
		// Set global settings
		$task->setMailerDir($GLOBALS["_SERVER"]["DOCUMENT_ROOT"].'/maildir');
		$task->setIsPreview(true);		
		$task->setCountMode('live');
		$task->setScriptTimeout(90);
		$task->setMemoryLimit(1024);
		$task->setTimestamp(time());
		$task->setNoLog(false);
	
		// initiate mailer job
		$mailer = $this->objectManager->get('Neulandlotsen\MooxMailer\Service\MooxNewsMailerService');
		$mailer->initialize($task);
		
		$maildata = $mailer->prepareMaildata();		
		$affectedUsers = $mailer->getAffectedUsers();	
				
		$this->view->assign('currentTask', $currentTask);		
		if(strpos($currentUserEmail,"@")!==FALSE){
			$this->view->assign('currentUser', $affectedUsers[$currentUserEmail]);			
		} 
		$this->view->assign('users', $affectedUsers);		
		
	}

	/**
	 * action preview news
	 *
	 * @param string $uid		
	 * @return void
	 */
	public function previewNewsAction($uid)
	{					
		$currentTaskUid = $this->backendSession->get("taskUid");
		
		if($currentTaskUid){
			$currentTask = $this->getMailerTask($currentTaskUid);
		} else {
			$firstTask 		= $this->getFirstMailerTask();
			$currentTask 	= $this->getMailerTask($firstTask['taskUid']);
		}
		
		$task = $currentTask['task'];
		
		$currentUserEmail = $this->backendSession->get("userEmail");		
		
		// Set global settings
		$task->setMailerDir($GLOBALS["_SERVER"]["DOCUMENT_ROOT"].'/maildir');
		$task->setIsPreview(true);		
		$task->setCountMode('live');
		$task->setScriptTimeout(90);
		$task->setMemoryLimit(1024);
		$task->setTimestamp(time());
		$task->setNoLog(false);
	
		// initiate mailer job
		$mailer = $this->objectManager->get('Neulandlotsen\MooxMailer\Service\MooxNewsMailerService');
		$mailer->initialize($task);
		if($uid!="all" && $uid>0 ){
			$mailer->setPreviewMaildataUid($uid);
		}
		if(strpos($currentUserEmail,"@")!==FALSE){
			$mailer->setPreviewMaildataUser($currentUserEmail);
		}
		
		$maildata = $mailer->prepareMaildata();	
		$affectedUsers = $mailer->getAffectedUsers();
		$previewMaildata = $mailer->getPreviewMaildata();
				
		$this->view->assign('currentTask', $currentTask);		
		
		if(strpos($currentUserEmail,"@")!==FALSE){
			$this->view->assign('currentUser', $affectedUsers[$currentUserEmail]);
			$this->view->assign('mails', $maildata);			
		} else {
			$this->view->assign('mails', $previewMaildata);			
		}
		
		$this->view->assign('tasks', $this->getMailerTasks());
		$this->view->assign('uid', $uid);
	}				
	
	/**
	 * action preview news iframe
	 *
	 * @param string $uid
	 * @param int $taskUid
	 * @param int $index
	 * @return void
	 */
	public function previewNewsIframeAction($uid,$taskUid = 0,$index = 0)
	{					
		if($taskUid>0){
			$currentTaskUid = $taskUid;
		} else {
			$currentTaskUid = $this->backendSession->get("taskUid");
		}
		
		if($currentTaskUid){
			$currentTask = $this->getMailerTask($currentTaskUid);
		} else {
			$firstTask = $this->getFirstMailerTask();
			$currentTask = $this->getMailerTask($firstTask['taskUid']);
		}
		
		$task = $currentTask['task'];
		
		$currentUserEmail = $this->backendSession->get("userEmail");		
		
		// Set global settings
		$task->setMailerDir($GLOBALS["_SERVER"]["DOCUMENT_ROOT"].'/maildir');
		$task->setIsPreview(true);		
		$task->setCountMode('live');
		$task->setScriptTimeout(90);
		$task->setMemoryLimit(1024);
		$task->setTimestamp(time());
		$task->setNoLog(false);
	
		// initiate mailer job
		$mailer = $this->objectManager->get('Neulandlotsen\MooxMailer\Service\MooxNewsMailerService');
		$mailer->initialize($task);
		if($uid!="all" && $uid>0 ){			
			$mailer->setPreviewMaildataUid($uid);
		}
		if(strpos($currentUserEmail,"@")!==FALSE){
			$mailer->setPreviewMaildataUser($currentUserEmail);
		}
				
		$maildata = $mailer->prepareMaildata();	
		$previewMaildata = $mailer->getPreviewMaildata();
				
		$this->view->assign('currentTask', $currentTask);		
		
		if(strpos($currentUserEmail,"@")!==FALSE){
			foreach($maildata AS $mail){
				if($mail['index']==$index){				
					$this->view->assign('mails', array($mail));
					break;
				}
			}
		} else {
			foreach($previewMaildata AS $mail){
				if($mail['index']==$index){					
					$this->view->assign('mails', array($mail));
					break;
				}
			}			
		}
		
		$this->view->assign('tasks', $this->getMailerTasks());
		
	}

	/**
	 * action show log
	 *
	 * @return void
	 */
	public function showLogAction() 
	{					
		$logfolder = $GLOBALS["_SERVER"]["DOCUMENT_ROOT"]."/maildir/logs/";
		
		$currentTaskUid = $this->backendSession->get("taskUid");
		
		if($currentTaskUid){
			$currentTask = $this->getMailerTask($currentTaskUid);
		} else {
			$firstTask = $this->getFirstMailerTask();
			$currentTask = $this->getMailerTask($firstTask['taskUid']);
		}
	
		if($currentTask['lastRunLogfile']){
			if(file_exists($logfolder.$currentTask['lastRunLogfile'])){
				$log = nl2br(file_get_contents($logfolder.$currentTask['lastRunLogfile']));
			}
		}		
	
		$this->view->assign('currentTask', $currentTask);
		$this->view->assign('tasks', $this->getMailerTasks());
		$this->view->assign('log', $log);
		
	}	
	
	/**
	 * action preview task
	 *
	 * @return void
	 */
	public function previewTaskAction() 
	{					
		$currentTaskUid = $this->backendSession->get("taskUid");
		
		if($currentTaskUid){
			$currentTask = $this->getMailerTask($currentTaskUid);
		} else {
			$firstTask = $this->getFirstMailerTask();
			$currentTask = $this->getMailerTask($firstTask['taskUid']);
		}

		$task = $currentTask['task'];
		
		// Set global settings
		$task->setMailerDir($GLOBALS["_SERVER"]["DOCUMENT_ROOT"].'/maildir');
		$task->setIsPreview(true);		
		$task->setCountMode('live');
		$task->setScriptTimeout(90);
		$task->setMemoryLimit(1024);
		$task->setTimestamp(time());
		$task->setNoLog(true);
	
		// initiate mailer job
		$mailer = $this->objectManager->get('Neulandlotsen\MooxMailer\Service\MooxNewsMailerService');
		$mailer->initialize($task);
				
		$previewData = $mailer->preview();
		
		$this->view->assign('currentTask', $currentTask);
		$this->view->assign('tasks', $this->getMailerTasks());
		$this->view->assign('preview', $previewData);
		
	}	
	
	/**
	 * action execute task
	 *
	 * @param int $uid
	 * @param string $redirectorController
	 * @param string $redirectorAction
	 * @return void
	 */
	public function executeTaskAction($uid = 0, $redirectorController = "Mailer", $redirectorAction = "executeTask")
	{					
		if($uid>0){
			$currentTaskUid = $uid;
		} else {
			$currentTaskUid = $this->backendSession->get("taskUid");
		}
		
		if($currentTaskUid){
			$currentTask = $this->getMailerTask($currentTaskUid);
		} else {
			$firstTask 		= $this->getFirstMailerTask();
			$currentTask 	= $this->getMailerTask($firstTask['taskUid']);
		}

		$task = $currentTask['task'];
		
		// Set global settings
		$task->execute();
		
		// initiate mailer job
		$mailer = $this->objectManager->get('Neulandlotsen\MooxMailer\Service\MooxNewsMailerService');
		$mailer->initialize($task);
		
		$this->addFlashMessage(
			'Beachten Sie, dass die Mails von einem Hintergrunddienst intervallweise versendet werden. Abhängig von der Anzahl der zu versendenden Mails, kann es daher einige Zeit in Anspruch nehmen bis wirklich alles Mails versendet wurden.', 
			'', 
			FlashMessage::INFO
		);
		
		if($redirectorAction!="executeTask"){
			if($redirectorAction=="index"){
				$this->redirect($redirectorAction,$redirectorController,NULL,array("uid" => $uid, "redirector" => 'executeTask'));
			}			
		} else {
			$this->view->assign('currentTask', $currentTask);
			$this->view->assign('tasks', $this->getMailerTasks());
			$this->view->assign('log', $mailer->readLog($task->getLogfile()));
		}
	}
	
	/**
	 * action set current task
	 *
	 * @param int $taskUid
	 * @return void
	 */
	public function setCurrentTaskAction($taskUid) 
	{					
		$this->backendSession->store("taskUid",$taskUid);
		
		$this->redirect("showTask");
		
	}
	
	/**
	 * action generate folders
	 *
	 * @param bool $index
	 * @return void
	 */
	public function generateFoldersAction($index = false)
	{					
		self::dircpy("",ExtensionManagementUtility::extPath("moox_mailer")."Resources/Skeleton/maildir",$GLOBALS["_SERVER"]["DOCUMENT_ROOT"]."/maildir");
		if(file_exists($GLOBALS["_SERVER"]["DOCUMENT_ROOT"]."/maildir/archive.sh")){
			chmod($GLOBALS["_SERVER"]["DOCUMENT_ROOT"]."/maildir/archive.sh",0760);
		}
		
		$this->addFlashMessage(
			'', 
			'Ordner wurden angelegt. Bitte leeren Sie den Cache über das Install-Tool!', 
			FlashMessage::OK
		);
		
		if($index){
			$this->redirect("index");
		} else {
			$this->redirect("showTask");
		}
		
	}			
	
	/**
	 * action set current user
	 *
	 * @param string $email
	 * @return void
	 */
	public function setCurrentUserAction($email = "") 
	{					
		if(strpos($email,"@")!==FALSE){
			$this->backendSession->store("userEmail",$email);
		} else {
			$this->backendSession->delete("userEmail");
		}
		
		$this->redirect("showNews");
		
	}	

	/**
	 * action remove news
	 *
	 * @param int $uid
	 * @return void
	 */
	public function removeNewsAction($uid) 
	{						
		if($uid>0){
			$news = $this->newsRepository->findByUid($uid);
			$news->setMailerFrequency(0);			
			$this->newsRepository->update($news);				
			$this->objectManager->get('TYPO3\CMS\Extbase\Persistence\PersistenceManagerInterface')->persistAll();	
		} 
		
		$this->redirect("showNews");
		
	}	
	
	/**
	 * get mailer tasks
	 *
	 * @param bool $extended
	 * @return void
	 */
	public function getMailerTasks($extended = false) 
	{
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable('tx_scheduler_task');
        $queryBuilder->getRestrictions()->removeAll()->add(GeneralUtility::makeInstance(DeletedRestriction::class));

        $query = $queryBuilder
            ->select('*')
            ->from('tx_scheduler_task')
            ->where(
                $queryBuilder->expr()->eq('disable', $queryBuilder->createNamedParameter(0, \PDO::PARAM_INT))
            )
            ->execute();
		
		$res = $query->fetch();
		$tasks = array();
		
		foreach($res AS $task){
			
			$mailerTask = unserialize($task['serialized_task_object']);
			if($mailerTask instanceof \Neulandlotsen\MooxMailer\Tasks\MooxNewsSendTask){
				
				$addTask = array();				
				$addTask['taskUid']	= $mailerTask->getTaskUid();
				$addTask['taskType'] = "MooxNewsSendTask";
				$addTask['description']	= $mailerTask->getDescription();

				$cronCmd = $mailerTask->getExecution()->getCronCmd();				
				if($cronCmd!=""){
					$cron = CronjobService::factory($cronCmd);
					$cron->isDue();
					$nextRun = $cron->getNextRunDate()->format('U');	
					$addTask['nextRun'] = self::$weekdays[date("w",$nextRun)].", ".date("d.m.Y H:i:s",$nextRun);
				} else {
					$executionTime = $mailerTask->getExecutionTime();
					if($executionTime>0){
						$nextRun = $executionTime;					
					} else {
						$nextRun = $mailerTask->getExecution()->getStart()+$mailerTask->getExecution()->getInterval();
					}					
					$addTask['nextRun'] = self::$weekdays[date("w",$nextRun)].", ".date("d.m.Y H:i:s",$executionTime);
					$lastRun = $executionTime-$mailerTask->getExecution()->getInterval();				
					$addTask['lastRun'] = self::$weekdays[date("w",$nextRun)].", ".date("d.m.Y H:i:s",$executionTime-$mailerTask->getExecution()->getInterval());
				}
				
				$addTask['nextRunCountdown'] = $this->getTimeDifference(time(),$nextRun);				
				$addTask['includedCategories'] = $this->categoryRepository->findByIdList(explode(",",$mailerTask->getIncludedNewsCategoriesUids()));
				$addTask['excludedCategories'] = $this->categoryRepository->findByIdList(explode(",",$mailerTask->getExcludedNewsCategoriesUids()));
				$addTask['task'] = $mailerTask;
				$tasks[] = $addTask;
			}
		}
		
		return $tasks;		
	}		
	
	/**
	 * get mailer task
	 *
	 * @param int $uid
	 * @return void
	 */
	public function getMailerTask($uid) 
	{			
		$task = array();

        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable('tx_scheduler_task');
        $queryBuilder
            ->getRestrictions()
            ->removeAll()
            ->add(GeneralUtility::makeInstance(DeletedRestriction::class));
        $query = $queryBuilder
            ->select('*')
            ->from('tx_scheduler_task')
            ->where(
                $queryBuilder->expr()->andx(
                    $queryBuilder->expr()->eq('disable', $queryBuilder->createNamedParameter(0, \PDO::PARAM_INT)),
                    $queryBuilder->expr()->eq('uid', $queryBuilder->createNamedParameter($uid, \PDO::PARAM_INT))
                )
            )
            ->execute();

		$schedulertask = $query->fetch();
		
		$mailerTask = unserialize($schedulertask['serialized_task_object']);
		
		if($mailerTask instanceof \Neulandlotsen\MooxMailer\Tasks\MooxNewsSendTask){
			
			$task['taskUid'] = $mailerTask->getTaskUid();
			$task['taskType'] = "MooxNewsSendTask";
			$task['description'] = $mailerTask->getDescription();

			$cronCmd = $mailerTask->getExecution()->getCronCmd();				
			
			if($cronCmd!=""){				
				$cron = CronjobService::factory($cronCmd);
				$cron->isDue();									
				$nextRun = $cron->getNextRunDate()->format('U');				
				$task['nextRun'] = self::$weekdays[date("w",$nextRun)].", ".date("d.m.Y H:i:s",$nextRun);				
			} else {
				$executionTime = $mailerTask->getExecutionTime();
				if($executionTime>0){
					$nextRun = $executionTime;					
				} else {
					$nextRun = $mailerTask->getExecution()->getStart()+$mailerTask->getExecution()->getInterval();
				}
				$task['nextRun'] = self::$weekdays[date("w",$nextRun)].", ".date("d.m.Y H:i:s",$nextRun);				
				$lastRun = $executionTime-$mailerTask->getExecution()->getInterval();				
				$task['lastRun'] = self::$weekdays[date("w",$lastRun)].", ".date("d.m.Y H:i:s",$executionTime-$mailerTask->getExecution()->getInterval());
			}
			
			$task['nextRunCountdown'] = $this->getTimeDifference(time(),$nextRun);
			
			$lastAction = $this->getLastAction('task'.$task['taskUid'],'log');
			
			$task['lastRunStart'] = self::$weekdays[date("w",$lastRun)].", ".date("d.m.Y H:i:s",$lastAction['logstarttime']);
						
			if($lastAction['logendtime']!="running"){
				$task['lastRunEnd'] = self::$weekdays[date("w",$lastRun)].", ".date("d.m.Y H:i:s",$lastAction['logendtime']);
			} else {
				$task['lastRunEnd'] = "nocht nicht beendet";
			}
			$task['lastRunLogfile'] = $lastAction['logfile'];
			$cc = explode("\r\n",$mailerTask->getCc());
			if(count($cc)==1 && $cc[0]==""){
				$cc = array();
			} 
			$task['cc'] = $cc;
			$bcc = explode("\r\n",$mailerTask->getBcc());
			if(count($bcc)==1 && $bcc[0]==""){
				$bcc = array();
			}
			$task['bcc'] 				= $bcc;
			$task['format']				= $mailerTask->getFormat();		
			$task['senderAddress']		= $mailerTask->getSenderAddress();
			$task['senderName']			= $mailerTask->getSenderName();
			$task['subject']			= $mailerTask->getSubject();		
			$task['mode']				= $mailerTask->getMode();
			$task['sendMode']			= $mailerTask->getSendMode();
			$task['includedCategories']	= $this->categoryRepository->findByIdList(explode(",",$mailerTask->getIncludedNewsCategoriesUids()));
			$task['excludedCategories']	= $this->categoryRepository->findByIdList(explode(",",$mailerTask->getExcludedNewsCategoriesUids()));
			$task['newsFolders']		= $this->getMooxNewsFolders(($mailerTask->getNewsPids()!="")?explode(",",$mailerTask->getNewsPids()):"");
			$task['userFolders']		= $this->getFeUsersFolders(($mailerTask->getUserPids()!="")?explode(",",$mailerTask->getUserPids()):"");
			$task['addressFolders']		= $this->getMooxAddressFolders(($mailerTask->getAddressesPids()!="")?explode(",",$mailerTask->getAddressesPids()):"");
			$task['excludedUserGroups']	= $this->getFeUsersGroups(explode(",",$mailerTask->getExcludedUserGroups()));
			$task['template']			= $this->getTemplate($mailerTask->getTemplateUid());
			$task['schedule']			= $this->getSchedule($mailerTask->getScheduleUid());
			$task['sendSingleMails']	= $mailerTask->getSendSingleMails();			
			$task['forceSubject']		= $mailerTask->getForceSubject();
			$task['forceTemplateInSingleMailing']			= $mailerTask->getForceTemplateInSingleMailing();
			$task['forceScheduledTemplateInSingleMailing']	= $mailerTask->getForceScheduledTemplateInSingleMailing();
			$task['sendRestrictedNewsOnly']					= $mailerTask->getSendRestrictedNewsOnly();
			$task['sendToUsersWithGroupsOnly']				= $mailerTask->getSendToUsersWithGroupsOnly();
			$task['debug']				= $mailerTask->getDebug();
			$task['debugAddress']		= $mailerTask->getDebugAddress();
			$task['debugMailLimit']		= $mailerTask->getDebugMailLimit();			
			$task['task']				= $mailerTask;
		}
		
		return $task;		
	}
	
	/**
	 * get first mailer task
	 *
	 * @return void
	 */
	public function getFirstMailerTask() 
	{
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable('tx_scheduler_task');

        $query = $queryBuilder
            ->select('*')
            ->from('tx_scheduler_task')
            ->where(
                $queryBuilder->expr()->eq('disable', $queryBuilder->createNamedParameter(0, \PDO::PARAM_INT))
            )
            ->execute();

        $res = $query->fetch();

		$firstTask = array();

        foreach($res AS $task){
			
			$mailerTask = unserialize($task['serialized_task_object']);
			if($mailerTask instanceof \Neulandlotsen\MooxMailer\Tasks\MooxNewsSendTask){
				
				$firstTask['taskUid'] = $mailerTask->getTaskUid();								
				
				break;
			}
		}
		
		return $firstTask;
	}
	
	/**
	 * get daemon mailer task
	 *
	 * @return void
	 */
	public function getDaemonMailerTask() 
	{
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable('tx_scheduler_task');

        $query = $queryBuilder
            ->select('*')
            ->from('tx_scheduler_task')
            ->where(
                $queryBuilder->expr()->eq('disable', $queryBuilder->createNamedParameter(0, \PDO::PARAM_INT))
            )
            ->execute();

        $res = $query->fetch();
		$daemonTask = array();
		
		foreach($res AS $task){
			
			$mailerTask = unserialize($task['serialized_task_object']);
			if($mailerTask instanceof \Neulandlotsen\MooxMailer\Tasks\DaemonSendTask){
				
				$daemonTask['taskUid'] = $mailerTask->getTaskUid();				
				$daemonTask['description'] = $mailerTask->getDescription();

				$cronCmd = $mailerTask->getExecution()->getCronCmd();				
				
				if($cronCmd!=""){
				
					$cron = CronjobService::factory($cronCmd);
					$cron->isDue();					
					
					$nextRun = $cron->getNextRunDate()->format('U');
					
					$daemonTask['nextRun'] = self::$weekdays[date("w",$nextRun)].", ".date("d.m.Y H:i:s",$nextRun);
					
				} else {
					
					$executionTime = $mailerTask->getExecutionTime();
					
					if($executionTime>0){
						
						$nextRun = $executionTime;
						
						$daemonTask['nextRun'] = self::$weekdays[date("w",$nextRun)].", ".date("d.m.Y H:i:s",$executionTime);
						
					} else {
						
						$nextRun = $mailerTask->getExecution()->getStart()+$mailerTask->getExecution()->getInterval();
						
						$daemonTask['nextRun'] = self::$weekdays[date("w",$nextRun)].", ".date("d.m.Y H:i:s",$mailerTask->getExecution()->getStart()+$mailerTask->getExecution()->getInterval());
					}
					
					$daemonTask['nextRun'] = self::$weekdays[date("w",$nextRun)].", ".date("d.m.Y H:i:s",$nextRun)." Uhr";
					
					$lastRun = $executionTime-$mailerTask->getExecution()->getInterval();
					
					$daemonTask['lastRun'] = self::$weekdays[date("w",$lastRun)].", ".date("d.m.Y H:i:s",$lastRun);
				}
				
				$daemonTask['nextRunCountdown'] = $this->getTimeDifference(time(),$nextRun);
				
				$daemonTask['mailsPerProcess'] = $mailerTask->getMailsPerProcess();
				
				$daemonTask['interval']['sec'] = $mailerTask->getExecution()->getInterval();
				
				if($cronCmd!=""){
					$tempCronCmd = explode(" ",$cronCmd);
					$daemonTask['interval']['min'] 	= str_replace("*/","",$tempCronCmd[0]);
				} else {
					$daemonTask['interval']['min'] 	= floor($daemonTask['interval']['sec']/60);
				}
				
				break;
			}
		}
		
		return $daemonTask;
	}
	
	/**
	 * get last task action info 
	 *
	 * @param int $taskUid
	 * @param string $what
	 * @return	array	timestamp or log
	 */
	private function getLastAction($taskUid,$what = "timestamp") 
	{		
		$logfolder = $GLOBALS["_SERVER"]["DOCUMENT_ROOT"]."/maildir/logs/";
		
		$handle = opendir ($logfolder);
		
		$logfile = "";
		
		$logtime = 0;
		
		while ($log = readdir ($handle)) 
		{											
			if(strpos($log,$taskUid.".log")>0){
				
				if(strpos($log,"__")===0){
					$running = false;
					$datetime['year'] 	= substr($log,2,4);
					$datetime['month'] 	= substr($log,6,2);
					$datetime['day'] 	= substr($log,8,2);
					$datetime['hour'] 	= substr($log,10,2);
					$datetime['minute'] = substr($log,12,2);
					$datetime['second'] = substr($log,14,2);									
				} else {
					$running = true;
					$datetime['year'] 	= substr($log,0,4);
					$datetime['month'] 	= substr($log,4,2);
					$datetime['day'] 	= substr($log,6,2);
					$datetime['hour'] 	= substr($log,8,2);
					$datetime['minute'] = substr($log,10,2);
					$datetime['second'] = substr($log,12,2);
				}
				
				$curtime = mktime($datetime['hour'],$datetime['minute'],$datetime['second'],$datetime['month'],$datetime['day'],$datetime['year']);
				
				if($curtime>$logtime){					
					$isrunning = $running;
					$logfile = $log;
					$logtime = $curtime;
				}												
			}
		}
		
		$return['logfile'] = $logfile;
		
		if($logfile!=""){
			if($isrunning){
				$return['logendtime'] = "running";
			} else {
				$return['logendtime'] = filemtime($logfolder.$logfile);
			}
			$return['logstarttime'] = $logtime;
		}
	
		if($logfile!="" && $what=="log"){
			$return['logcontent'] = file_get_contents($logfolder.$logfile);
		}
			
		closedir($handle);
		
		return $return;
	}
	
	/**
	 * get time diff between two timestamps 
	 *
	 * @param int $first
	 * @param int $second
	 * @return array	timediff
	 */
	function getTimeDifference($first, $second)
	{    
		if($first > $second){
			$td['dif'] = $first - $second;
		} else {
			$td['dif'] = $second - $first;
		}
		
		$td['sec'] = $td['dif'] % 60; // 67 = 7

		$td['min'] = (($td['dif'] - $td['sec']) / 60) % 60; 
		
		$td['std'] = (((($td['dif'] - $td['sec']) /60)- 
		$td['min']) / 60) % 24;
		
		$td['day'] = floor( ((((($td['dif'] - $td['sec']) /60)- 
		$td['min']) / 60) / 24) );
						
		return $td;
		
	}
	
	/**
	 * Get array of folders with newsletter module	
	 *
	 * @param string $module
	 * @return	array	folders with newsletter module	
	 */
	public function getFolders($module = "mxmailer") 
	{		
		global $BE_USER;
		
		$folders = array();

        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable('pages');
        $queryBuilder
            ->getRestrictions()
            ->removeAll()
            ->add(GeneralUtility::makeInstance(DeletedRestriction::class));

        $query = $queryBuilder
            ->select('*')
            ->from('pages')
            ->where(
                $queryBuilder->expr()->andx(
                    $queryBuilder->expr()->in('doktype', $queryBuilder->createNamedParameter(1, \PDO::PARAM_INT), $queryBuilder->createNamedParameter(254, \PDO::PARAM_INT)),
                    $queryBuilder->expr()->eq('module', $queryBuilder->createNamedParameter($module, \PDO::PARAM_STR))
                )
            )
            ->execute();
		
		$folderCnt = 0;
		while($row = $query->fetch())
        {
			$folders[$folderCnt] = $row;
			$rootline = GeneralUtility::makeInstance(RootlineUtility::class, $row['uid'], '', $this->pageRepository)->get();
			
			foreach($rootline AS $rootlinepage){
				if($rootlinepage['is_siteroot']){
					$folders[$folderCnt]['rootpage'] = $rootlinepage;
					break;
				}
			}
			
			if(!$rootpage){
				$folders[$folderCnt]['rootpage'] = $rootline[0];
			}
					
			$rootline = array_reverse($rootline);
			
			if(isset($rootline[count($rootline)-2])){			
				$pageInfo = $this->pageRepository->getPage((int)$rootline[count($rootline)-2]['uid']);		
				if($pageInfo['module']==$module){
					$folders[$folderCnt]['folder'] = $pageInfo['uid'];				
				}
				
			}
			
			$folders[$folderCnt]['rootline'] = $rootline;
			$folderCnt++;
		}
				
		usort($folders, array("\Neulandlotsen\MooxMailer\Controller\NewsletterController", "sortByFolderAndTitle"));
		
		$folders = array_reverse($folders);
		
		return $folders;		
	}
	
	/**
	 * get template
	 *
	 * @param int $uid
	 * @return array|object $templates
	 */
	public function getTemplate($uid) 
	{		
		$template = array();
		
		if($uid){					
			$template = $this->templateRepository->findByUid($uid);
		}
		
		return $template;
	}
	
	/**
	 * get schedule
	 *
	 * @param int $uid
	 * @return array|object $schedules
	 */
	public function getSchedule($uid) 
	{		
		$schedule = array();
		
		if($uid){					
			$schedule = $this->scheduleRepository->findByUid($uid);		
		}
		
		return $schedule;
	}
	
	/**
	 * Get array of folders with news module	
	 *	
	 * @param array $uids
	 * @return	array	folders with news module	
	 */
	public function getMooxNewsFolders($uids = array()) 
	{		
		$pages = array();
		
		if(is_array($uids) && count($uids))
		{
            $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable('pages');
            $queryBuilder
                ->getRestrictions()
                ->removeAll()
                ->add(GeneralUtility::makeInstance(DeletedRestriction::class));

            $query = $queryBuilder
                ->select('*')
                ->from('pages')
                ->where(
                    $queryBuilder->expr()->in('doktype', $queryBuilder->createNamedParameter(1, \PDO::PARAM_INT), $queryBuilder->createNamedParameter(254, \PDO::PARAM_INT)),
                    $queryBuilder->expr()->eq('module', $queryBuilder->createNamedParameter('mxnews', \PDO::PARAM_STR)),
                    $queryBuilder->expr()->in('uid', $queryBuilder->createNamedParameter($uids, \PDO::PARAM_INT_ARRAY))
                )
                ->execute();

			while($row = $query->fetch())
            {
				$pages[] = $row;
			}
		}
		return $pages;		
	}
	
	/**
	 * Get array of folders with moox-addresses	
	 *
	 * @param array $uids	 
	 * @return	array	folders moox-addresses	
	 */
	public function getMooxAddressFolders($uids = array()) 
	{		
		$pages = array();
				
		if(is_array($uids) && count($uids))
		{
            $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable('pages');
            $queryBuilder
                ->getRestrictions()
                ->removeAll()
                ->add(GeneralUtility::makeInstance(DeletedRestriction::class));

            $query = $queryBuilder
                ->select('*')
                ->from('pages')
                ->where(
                    $queryBuilder->expr()->in('doktype', $queryBuilder->createNamedParameter(1, \PDO::PARAM_INT), $queryBuilder->createNamedParameter(254, \PDO::PARAM_INT)),
                    $queryBuilder->expr()->eq('module', $queryBuilder->createNamedParameter('mxaddress', \PDO::PARAM_STR)),
                    $queryBuilder->expr()->in('uid', $queryBuilder->createNamedParameter($uids, \PDO::PARAM_INT_ARRAY))
                )
                ->execute();

			while($row = $query->fetch())
            {
				$pages[] = $row;
			}
		}
		
		return $pages;		
	}
	
	/**
	 * Get array of folders with fe-users	
	 *
	 * @param array $uids	 
	 * @return	array	folders with fe-users	
	 */
	public function getFeUsersFolders($uids = array()) 
	{		
		$pages = array();
		
		if(is_array($uids) && count($uids))
		{
            $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable('pages');
            $queryBuilder
                ->getRestrictions()
                ->removeAll()
                ->add(GeneralUtility::makeInstance(DeletedRestriction::class));

            $query = $queryBuilder
                ->select('*')
                ->from('pages')
                ->where(
                    $queryBuilder->expr()->in('doktype', $queryBuilder->createNamedParameter(1, \PDO::PARAM_INT), $queryBuilder->createNamedParameter(254, \PDO::PARAM_INT)),
                    $queryBuilder->expr()->eq('module', $queryBuilder->createNamedParameter('mxfeuser', \PDO::PARAM_STR)),
                    $queryBuilder->expr()->in('uid', $queryBuilder->createNamedParameter($uids, \PDO::PARAM_INT_ARRAY))
                )
                ->execute();

			while($row = $query->fetch())
            {
				$pages[] = $row;
			}
		}
		
		return $pages;		
	}
	
	/**
	 * get fe user groups (for given pid(list))
	 *
	 * @param array $uids
	 * @return \TYPO3\CMS\Extbase\Persistence\QueryInterface $groups
	 */
	public function getFeUsersGroups($uids = array()) 
	{	
		$groups = array();
		
		if(is_array($uids) && count($uids)){				
			$groups	= $this->frontendUserGroupRepository->findByIdList($uids);
		}
		
		return $groups;
	}
	
	/**
	 * Redirect to form to create a task record
	 *
	 * @return void
	 */
	public function newTaskAction() 
	{
		$this->redirectToCreateNewRecord('');
	}
		
	/**
	 * Returns page
	 *
	 * @return int
	 */
	public function getPage() 
	{
		return $this->page;
	}

	/**
	 * Set page
	 *
	 * @param int $page page
	 * @return void
	 */
	public function setPage($page)
	{
		$this->page = $page;
	}
	
	/**
	 * Returns ext conf
	 *
	 * @return array
	 */
	public function getExtConf()
	{
		return $this->extConf;
	}

	/**
	 * Set ext conf
	 *
	 * @param array $extConf ext conf
	 * @return void
	 */
	public function setExtConf($extConf)
	{
		$this->extConf = $extConf;
	}
	
	/**
	 * Returns ext conf
	 *
	 * @return array
	 */
	public function getNewsInstalled()
	{
		return $this->newsInstalled;
	}

	/**
	 * Set news installed 
	 *
	 * @param array $newsInstalled news installed 
	 * @return void
	 */
	public function setNewsInstalled($newsInstalled)
	{
		$this->newsInstalled = $newsInstalled;
	}
}
?>