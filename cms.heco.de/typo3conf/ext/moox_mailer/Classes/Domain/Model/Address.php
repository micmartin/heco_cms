<?php
namespace Neulandlotsen\MooxMailer\Domain\Model;

/**
 * This file is part of the "moox_mailer" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

use TYPO3\CMS\Extbase\Persistence\ObjectStorage;
 
class Address extends \Neulandlotsen\MooxAddress\Domain\Model\Address
{
	
	/**
	 * @var int
	 */
    protected $crdate;
	
	/**
	 * @var int
	 */
	protected $tstamp;
	
	/**
	 * @var int
	 */
    protected $quality;
	
	/**
	 * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Neulandlotsen\MooxMailer\Domain\Model\Bounce>
	 */
	protected $bounces = NULL;
	
	/**
	 * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Neulandlotsen\MooxMailer\Domain\Model\Error>
	 */
	protected $errors = NULL;
  
	/**
	 * __construct
	 */
	public function __construct() 
	{
		//Do not remove the next line: It would break the functionality
		$this->initStorageObjects();
	}
  
	/**
	 * Initializes all ObjectStorage properties
	 * Do not modify this method!
	 * It will be rewritten on each save in the extension builder
	 * You may modify the constructor of this class instead
	 *
	 * @return void
	 */
	protected function initStorageObjects()
	{
		$this->bounces = new ObjectStorage();
		$this->errors = new ObjectStorage();
	}
	
	/**
     * get crdate
	 *
     * @return int $crdate gender
     */
    public function getCrdate()
	{
       return $this->crdate;
    }
     
    /**
     * set crdate
	 *
     * @param int $crdate crdate
	 * @return void
     */
    public function setCrdate($crdate)
	{
        $this->crdate = $crdate;
    }
	
	/**
	 * Get timestamp
	 *
	 * @return int
	 */
	public function getTstamp() 
	{
		return $this->tstamp;
	}

	/**
	 * Set time stamp
	 *
	 * @param int $tstamp time stamp
	 * @return void
	 */
	public function setTstamp($tstamp)
	{
		$this->tstamp = $tstamp;
	}
	
	/**
     * get quality
	 *
     * @return int $quality quality
     */
    public function getQuality()
	{
       return $this->quality;
    }
     
    /**
     * set quality
	 *
     * @param int $quality quality
	 * @return void
     */
    public function setQuality($quality)
	{
        $this->quality = $quality;
    }
	
	/**
	 * Adds a bounce
	 *
	 * @param \Neulandlotsen\MooxMailer\Domain\Model\Bounce $bounce
	 * @return void
	 */
	public function addBounce(\Neulandlotsen\MooxMailer\Domain\Model\Bounce $bounce)
	{
		$this->bounces->attach($bounce);
	}

	/**
	 * Removes a bounce
	 *
	 * @param \Neulandlotsen\MooxMailer\Domain\Model\Bounce $bounceToRemove The bounce to be removed
	 * @return void
	 */
	public function removeBounce(\Neulandlotsen\MooxMailer\Domain\Model\Bounce $bounceToRemove)
	{
		$this->bounces->detach($bounceToRemove);
	}

	/**
	 * Returns the bounces
	 *
	 * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Neulandlotsen\MooxMailer\Domain\Model\Bounce> $bounces
	 */
	public function getBounces()
	{
		return $this->bounces;
	}

	/**
	 * Sets the bounces
	 *
	 * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Neulandlotsen\MooxMailer\Domain\Model\Bounce> $bounces
	 * @return void
	 */
	public function setBounces(ObjectStorage $bounces)
	{
		$this->bounces = $bounces;
	}
	
	/**
	 * Adds a error
	 *
	 * @param \Neulandlotsen\MooxMailer\Domain\Model\Error $error
	 * @return void
	 */
	public function addError(\Neulandlotsen\MooxMailer\Domain\Model\Error $error)
	{
		$this->bounces->attach($error);
	}

	/**
	 * Removes a error
	 *
	 * @param \Neulandlotsen\MooxMailer\Domain\Model\Error $errorToRemove The error to be removed
	 * @return void
	 */
	public function removeError(\Neulandlotsen\MooxMailer\Domain\Model\Error $errorToRemove)
	{
		$this->errors->detach($errorToRemove);
	}

	/**
	 * Returns the bounces
	 *
	 * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Neulandlotsen\MooxMailer\Domain\Model\Error> $error
	 */
	public function getErrors()
	{
		return $this->error;
	}

	/**
	 * Sets the error
	 *
	 * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Neulandlotsen\MooxMailer\Domain\Model\Error> $error
	 * @return void
	 */
	public function setErrors(ObjectStorage $error)
	{
		$this->error = $error;
	}
}
?>