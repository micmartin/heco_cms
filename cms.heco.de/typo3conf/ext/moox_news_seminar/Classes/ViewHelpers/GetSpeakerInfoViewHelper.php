<?php
namespace Neulandlotsen\MooxNewsSeminar\ViewHelpers;

/**
 * This file is part of the "moox_news_seminar" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

use Neulandlotsen\MooxNewsSeminar\Domain\Repository\SpeakerRepository;
use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper;

class GetSpeakerInfoViewHelper extends AbstractViewHelper
{	
	/**
	 * @var SpeakerRepository
	 */
	protected $speakerRepository;

    /**
     * @param SpeakerRepository $speakerRepository
     */
    public function injectSpeakerRepository(SpeakerRepository $speakerRepository)
    {
        $this->speakerRepository = $speakerRepository;
    }
	
	/**
	 * @param int $uid
	 * @param string $field		 
	 * @return string
	 */
	public function render($uid, $field)
	{		
		$speaker = "";
		
		if($uid && $field)
		{
			$speaker = $this->speakerRepository->findByUid($uid);
			if(is_object($speaker))
			{
				$speaker = $speaker->getTitle();
			}
		}
		
		return $speaker;
	}
}
