<?php
if (!defined('TYPO3_MODE'))
{
	die ('Access denied.');
}

$GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['moox_core']['setup'] = unserialize($_EXTCONF);

/* ===========================================================================
 	Register BE-Module for Administration
=========================================================================== */

if (TYPO3_MODE === 'BE')
{
    /**
     * Add own CSS for Backend
     */
    $TBE_STYLES['skins'][$_EXTKEY]['name'] = $_EXTKEY;
    $TBE_STYLES['skins'][$_EXTKEY]['stylesheetDirectories']['structure'] = 'EXT:' . ($_EXTKEY) . '/Resources/Public/Stylesheets/Backend/';

    /***************
     * Register Main Module
     *
	$mainModuleName = "moox";
	if (!isset($TBE_MODULES[$mainModuleName])) 
	{
        $temp_TBE_MODULES = [];
        foreach ($TBE_MODULES as $key => $val) 
		{
            if ($key == 'file') 
			{
				$temp_TBE_MODULES[$mainModuleName] = '';
                $temp_TBE_MODULES[$key] = $val;               
            } 
			else 
			{
                $temp_TBE_MODULES[$key] = $val;
            }
        }
        $TBE_MODULES = $temp_TBE_MODULES;		
		\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerModule(
			'Neulandlotsen.'.$_EXTKEY,
			$mainModuleName,
			'',
			'',
			[],
			[
				'access' => 'user,group',
				'icon'   => 'EXT:'.$_EXTKEY.'/Resources/Public/Icons/module-mooxcore-white.svg',
				'labels' => 'LLL:EXT:'.$_EXTKEY.'/Resources/Private/Language/Module.xlf:moox_main_module_tab',
			]
		);
    }

    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerModule(
        'Neulandlotsen.'.$_EXTKEY,
        $mainModuleName,
        'dashboard',
        'top',
        array(
			'Administration' => 'dashboard,designer',
		),
		array(
			'access' => 'user,group',
			'icon' => 'EXT:' . $_EXTKEY . '/Resources/Public/Icons/module-mooxcore.svg',
			'labels' => 'LLL:EXT:' . $_EXTKEY . '/Resources/Private/Language/Module.xlf',
		)
    );
	*/

}