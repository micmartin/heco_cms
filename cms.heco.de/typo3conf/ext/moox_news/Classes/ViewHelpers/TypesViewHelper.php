<?php
namespace Neulandlotsen\MooxNews\ViewHelpers;

/**
 * This file is part of the "moox_news" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper;
use TYPO3\CMS\Extbase\Utility\LocalizationUtility;

class TypesViewHelper extends AbstractViewHelper
{
	public function initializeArguments()
	{
		parent::initializeArguments();
		$this->registerArgument('settings', 'array', 'settings of news-plugin', true);
		$this->registerArgument('as', 'string', 'return result as variable with this name', false);
	}

	/**
	 * @return string
	 */
	public function render()
	{
		$settings = $this->arguments['settings'];
		$as = $this->arguments['as'];

		if($this->settings['types']!="")
		{
			$allowedTypes = explode(",",$settings['types']);
		}
		else
		{
			$allowedTypes = [];
		}

		$types = [];
		foreach($GLOBALS['TCA']['tx_mooxnews_domain_model_news']['columns']['type']['config']['items'] AS $option)
		{
			if(!count($allowedTypes) || in_array($option[1],$allowedTypes))
			{
				$types[$option[1]] = LocalizationUtility::translate($option[0], "moox_news");
			}
		}

		if($as)
		{
			$this->templateVariableContainer->add($this->arguments['as'], $types);
		}
		else
		{
			return $types;
		}
	}
}
