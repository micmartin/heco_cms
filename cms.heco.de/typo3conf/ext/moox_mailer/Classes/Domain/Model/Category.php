<?php
namespace Neulandlotsen\MooxMailer\Domain\Model;

/**
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */
 
class Category extends \Neulandlotsen\MooxNews\Domain\Model\Category
{
		
	/**
	 * @var int
	 */
	protected $mailerForcesinglemail;
	
	/**
	 * @var string
	 */
	protected $mailerTemplate;
	
	/**
	 * @var string
	 */
	protected $mailerSubject;
		
	/**
	 * Set mailer forcesinglemail
	 *
	 * @param  int $mailerForcesinglemail mailer forcesinglemail
	 * @return void
	 */
	public function setMailerForcesinglemail($mailerForcesinglemail)
	{
		$this->mailerForcesinglemail = $mailerForcesinglemail;
	}

	/**
	 * Get mailer forcesinglemail
	 *
	 * @return int
	 */
	public function getMailerForcesinglemail()
	{
		return $this->mailerForcesinglemail;
	}
	
	/**
	 * Set mailer template
	 *
	 * @param  string $mailerTemplate mailer template
	 * @return void
	 */
	public function setMailerTemplate($mailerTemplate)
	{
		$this->mailerTemplate = $mailerTemplate;
	}

	/**
	 * Get mailer template
	 *
	 * @return string
	 */
	public function getMailerTemplate()
	{
		return $this->mailerTemplate;
	}
	
	/**
	 * Set mailer subject
	 *
	 * @param  string $mailerSubject mailer subject
	 * @return void
	 */
	public function setMailerSubject($mailerSubject)
	{
		$this->mailerSubject = $mailerSubject;
	}

	/**
	 * Get mailer subject
	 *
	 * @return string
	 */
	public function getMailerSubject()
	{
		return $this->mailerSubject;
	}
}
