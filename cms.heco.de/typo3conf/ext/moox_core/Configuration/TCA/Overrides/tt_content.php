<?php
$fieldsToHideFromMooxCoreElements = [
	'subheader',
	'header_link',
	'date',
	'categories'
];
foreach ($fieldsToHideFromMooxCoreElements AS $field)
{
	$GLOBALS['TCA']['tt_content']['columns'][$field]['displayCond'] = [
		'AND' => [
			'FIELD:CType:!=:mooxcore_csscontainer',
			'FIELD:CType:!=:mooxcore_embedvideo'
		],
	];
}

?>
