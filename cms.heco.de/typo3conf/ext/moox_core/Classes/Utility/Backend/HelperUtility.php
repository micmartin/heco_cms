<?php
namespace Neulandlotsen\MooxCore\Utility\Backend;

/**
 * This file is part of the "moox_core" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

use TYPO3\CMS\Core\SingletonInterface;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Database\Connection;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Database\Query\Restriction\DeletedRestriction;
use TYPO3\CMS\Core\Database\Query\Restriction\HiddenRestriction;
use TYPO3\CMS\Backend\Utility\BackendUtility;
use TYPO3\CMS\Core\DataHandling\DataHandler;
use TYPO3\CMS\Core\Messaging\FlashMessageService;

class HelperUtility implements SingletonInterface
{
	/**
	 * @var \TYPO3\CMS\Core\DataHandling\DataHandler
	 */
	protected $dataHandler = NULL;

	/**
	 * @var \Neulandlotsen\MooxCore\Domain\Session\SessionHandler
	 */
	protected $backendSession = NULL;

	/**
	 * @var FlashMessageService
	 */
	protected $flashMessageService;

	/**
	 * @var array
	 */
	protected $allowedListViewFields = [];

    /**
     * @param FlashMessageService $flashMessageService
     */
    public function injectFlashMessageService(FlashMessageService $flashMessageService)
    {
        $this->flashMessageService = $flashMessageService;
    }

	/**
	 * @param string $listType
	 * @return	array $pages
	 */
	static public function getPluginPages($listType = "")
	{
		$pages = [];

		if($listType!="")
		{
			//ToDo: Remove commented lines if no errors occur - 2019-07-29
			//$objectManager = GeneralUtility::makeInstance('TYPO3\CMS\Extbase\Object\ObjectManager');
			//$pageRepository = $objectManager->get('TYPO3\\CMS\\Frontend\\Page\\PageRepository');
			//$pageRepository->init(true);

			$queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)
				->getQueryBuilderForTable('tt_content');
			$queryBuilder
				->getRestrictions()
				->removeAll()
				->add(GeneralUtility::makeInstance(DeletedRestriction::class))
				->add(GeneralUtility::makeInstance(HiddenRestriction::class));

			$res = $queryBuilder
				->select('pid')
				->from('tt_content')
				->where(
					$queryBuilder->expr()->eq('CType', $queryBuilder->createNamedParameter('list', \PDO::PARAM_STR)),
					$queryBuilder->expr()->eq('list_type', $queryBuilder->createNamedParameter($listType, \PDO::PARAM_STR))
				)
				->groupBy('pid')
				->execute();

			foreach ($res as $page)
			{

				$pageInfo 	= BackendUtility::readPageAccess((int)$page['pid'], '');
				if(is_array($pageInfo))
				{
					$pages[] = ["uid" => $pageInfo['uid'], "title" => $pageInfo['title']];
				}
			}
		}

		return $pages;
	}

	/**
	 * @param string $module
	 * @param array $doktypes
	 * @param bool $grouped
	 * @param array $doktypes
	 * @return uids
	 */
	static public function getFolders($module = '', $doktypes = [1,254], $grouped = false, $uids = [])
	{
		global $BE_USER;

		$folders = [];

		if($module!="")
		{
			//ToDo: Remove commented lines if no errors occur - 2019-07-29
			//$objectManager = GeneralUtility::makeInstance('TYPO3\CMS\Extbase\Object\ObjectManager');
			//$pageRepository = $objectManager->get('TYPO3\\CMS\\Frontend\\Page\\PageRepository');
			//$pageRepository->init(true);

			$queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)
				->getQueryBuilderForTable('pages');
			$queryBuilder
				->getRestrictions()
				->removeAll()
				->add(GeneralUtility::makeInstance(DeletedRestriction::class))
				->add(GeneralUtility::makeInstance(HiddenRestriction::class));

			$res = $queryBuilder
				->select('*')
				->from('pages')
				->where(
					$queryBuilder->expr()->eq('module', $queryBuilder->createNamedParameter($module, \PDO::PARAM_STR)),
					$queryBuilder->expr()->in('doktype', $queryBuilder->createNamedParameter($doktypes, Connection::PARAM_STR_ARRAY)),
					(is_array($uids) && count($uids))?$queryBuilder->expr()->in('uid', $queryBuilder->createNamedParameter($uids, Connection::PARAM_INT_ARRAY)):NULL
				)
				->execute();

			$folderCnt = 0;

			while($row = $res->fetch())
			{
				$folders[$folderCnt] = $row;
				$rootline = BackendUtility::BEgetRootLine($row['uid']);

				foreach($rootline AS $rootlinepage)
				{
					if($rootlinepage['is_siteroot'])
					{
						$folders[$folderCnt]['rootpage'] = $rootlinepage;
						break;
					}
				}

				if(!$folders[$folderCnt]['rootpage'])
				{
					$folders[$folderCnt]['rootpage'] = $rootline[0];
				}

				$rootline = array_reverse($rootline);

				if(isset($rootline[count($rootline)-2]))
				{
					$pageInfo 	= BackendUtility::readPageAccess((int)$rootline[count($rootline)-2]['uid'], '');
					if($pageInfo['module']==$module)
					{
						$folders[$folderCnt]['folder'] = $pageInfo['uid'];
					}

				}

				$folders[$folderCnt]['rootline'] = $rootline;
				$folderCnt++;
			}

			usort($folders, ["\Neulandlotsen\MooxCore\Utility\HelperUtility", "sortByFolderAndTitle"]);

			$folders = array_reverse($folders);

			if($grouped)
			{

				$groupedFolders = [];

				foreach($folders AS $folder)
				{
					$groupedFolders[($folder['rootpage']['nav_title'])?$folder['rootpage']['nav_title']:$folder['rootpage']['title']][] = $folder;
				}

				return $groupedFolders;
			}
			else
			{
				return $folders;
			}
		}
	}

	/**
	 * @param string $query
	 * @return array
	 */
	public function getQueryReplacements($query = "")
	{
		$replacements = [];

		if($query!="")
		{
			$replacements[] = ["search" => $query, "replace" => '<span class="bg-info">'.$query.'</span>'];
			$replacements[] = ["search" => strtolower($query), "replace" => '<span class="bg-info">'.strtolower($query).'</span>'];
			$replacements[] = ["search" => strtoupper($query), "replace" => '<span class="bg-info">'.strtoupper($query).'</span>'];
			$replacements[] = ["search" => ucfirst($query), "replace" => '<span class="bg-info">'.ucfirst($query).'</span>'];
			$replacements[] = ["search" => '<span class="bg-info"><span class="bg-info">', "replace" => '<span class="bg-info">'];
			$replacements[] = ["search" => '</span></span>', "replace" => '</span>'];
		}

		return $replacements;
	}

	/**
	 * set flash messages
	 *
	 * @param mixed &$parent
	 * @param string $extensionName
	 * @param array $messages
	 * @param string $identifier
	 */
	public function setFlashMessages(&$parent = NULL, $extensionName = NULL, $messages = [], $identifier = '', $storeInSession = true)
	{
		if($parent && $extensionName)
		{
			if(!$identifier)
			{
				$identifier = 'extbase.flashmessages.tx_'.strtolower($extensionName);
			}
			// overwrite message queue
			if (TYPO3_MODE === 'BE')
			{
				$identifier .= ".backend";
			}
			if(is_array($parent->getMessages())){
				$messages = array_merge($parent->getMessages(),$messages);
				$parent->setMessages([]);
			}

			// overwrite message queue
			$flashMessageQueue = $this->flashMessageService->getMessageQueueByIdentifier($identifier);

			// set flash messages
			foreach($messages AS $message)
			{
				if(!is_array($message))
				{
					$message = [];
				}
				if($message['text']=="")
				{
					$message['text'] = "Unbekannter Fehler / Unknown error";
				}
				if(false && $message['icon']!="" && $message['title']!="")
				{
					$message['title'] = $message['icon'].$message['title'];
				}

				$flashMessageQueue->addMessage(
					GeneralUtility::makeInstance('TYPO3\\CMS\\Core\\Messaging\\FlashMessage',
						$message['text'],
						($message['title']!="")?$message['title'].": ":"",
						$message['type'],
						$storeInSession
					)
				);
			}
		}
	}

	/**
	 * @param string $identifier
	 * @param array $settings
	 * @param string $extensionName
	 * @param string $pageModule
	 * @return array
	 */
	public function buildFilter($identifier = NULL, $settings = [], $extensionName = "MooxCore", $pageModule = "mxcore")
	{
		global $BE_USER;

		$filter = NULL;

		$settingsKey = $this->getSettingsKeyFromIdentifier($identifier);

		if($identifier!="")
		{
			$identifier = $identifier."Filter";

			if($this->backendSession->stored($identifier))
			{
				if($this->backendSession->get($identifier)!="")
				{
					$filter = unserialize($this->backendSession->get($identifier));
				}
			}

			if(!is_array($filter))
			{

				if((int)$settings['itemsPerPage'][$settingsKey]>0)
				{
					$filter['perPage'] = (int)$settings['itemsPerPage'][$settingsKey];
				}
				else
				{
					$filter['perPage'] = 10;
				}
				$pageTsModuleStartPid = $BE_USER->getTSConfig()['tx_'.strtolower($extensionName).'.']['moduleStartPid'];

				if((int)$pageTsModuleStartPid>0)
				{
					$pid = $pageTsModuleStartPid;
				}
				elseif((int)$settings['moduleStartPid']>0)
				{
					$pid = (int)$settings['moduleStartPid'];
				}
				else
				{
					$folders = $this->getFolders($pageModule);
					if(count($folders))
					{
						$pid = $folders[0]['uid'];
					}
					else
					{
						$pid = 0;
					}
				}
				$readPageAccess = BackendUtility::readPageAccess($pid, '');
				if(!is_array($readPageAccess) || !$pid)
				{
					$pid = 0;
				}
				$filter['page'] = $pid;
				$this->backendSession->store($identifier,serialize($filter));
			}
		}

		return $filter;
	}

	/**
	 * update filter
	 *
	 * @param array $filter
	 * @param string $identifier
	 */
	public function updateFilter($filter = NULL, $identifier = NULL)
	{
		if(is_array($filter) && $identifier!="")
		{
			$identifier = $identifier."Filter";
			$this->backendSession->store($identifier,serialize($filter));
		}
		return $filter;
	}

	/**
	 * overwrite filter
	 *
	 * @param array $overwriteFilter
	 * @param string $identifier
	 * @param array $settings
	 */
	public function overwriteFilter($overwriteFilter = NULL, $identifier = NULL, $settings = [])
	{
		$filter = [];

		if(is_array($overwriteFilter) && $identifier!="")
		{
			$identifier = $identifier."Filter";
			$filter = [];
			if($this->backendSession->stored($identifier))
			{
				if($this->backendSession->get($identifier)!="")
				{
					$filter = unserialize($this->backendSession->get($identifier));
					if(count($filter)>0)
					{
						$filter = array_merge($filter,$overwriteFilter);
						$this->backendSession->store($identifier,serialize($filter));
					}
				}
			}

		}

		return $filter;
	}

	/**
	 * @param string $identifier
	 */
	public function resetFilter($identifier = NULL)
	{
		if($identifier!="")
		{
			$identifier = $identifier."Filter";
			$this->backendSession->delete($identifier);
		}
	}

	/**
	 * @param string $identifier
	 * @param array $settings
	 * @return array
	 */
	public function buildOrdering($identifier = NULL, $settings = [])
	{
		$settingsKey = $this->getSettingsKeyFromIdentifier($identifier);

		global $BE_USER;

		$ordering = NULL;

		if($identifier!="")
		{
			$identifier = $identifier."Ordering";

			if($this->backendSession->stored($identifier)){
				if($this->backendSession->get($identifier)!=""){
					$ordering = unserialize($this->backendSession->get($identifier));
				}
			}

			if(!is_array($ordering))
			{
				if(substr($identifier, 0, 13)=="Neulandlotsen" && substr($identifier, -18)=="ControllerOrdering")
				{
					$ordering['field'] = "title";
					$ordering['direction'] = "asc";
				}
				else
				{
					if($settings['defaultSortField'][$settingsKey]!='' && in_array($settings['defaultSortField'][$settingsKey],$this->allowedListViewFields))
					{
						$ordering['field'] = $settings['defaultSortField'][$settingsKey];
					}
					elseif($settings['listViewFields'][$settingsKey]!="")
					{
						$fields = explode(",",$settings['listViewFields'][$settingsKey]);
						$ordering['field'] = $fields[0];
					}
					else
					{
						$ordering['field'] = 'title';
					}
					if($settings['defaultSortDirection'][$settingsKey]!='' && in_array($settings['defaultSortDirection'][$settingsKey],['asc','desc']))
					{
						$ordering['direction'] = $settings['defaultSortDirection'][$settingsKey];
					}
					else
					{
						$ordering['direction'] = 'asc';
					}
				}
				$this->backendSession->store($identifier,serialize($ordering));
			}
		}

		return $ordering;
	}

	/**
	 * @param array $ordering
	 * @param string $identifier
	 */
	public function updateOrdering($ordering = NULL, $identifier = NULL)
	{
		if(is_array($ordering) && $identifier!="")
		{
			$identifier = $identifier."Ordering";
			$this->backendSession->store($identifier,serialize($ordering));
		}
		return $ordering;
	}

	/**
	 * @param string $identifier
	 */
	public function resetOrdering($identifier = NULL)
	{
		if($identifier!="")
		{
			$identifier = $identifier."Ordering";
			$this->backendSession->delete($identifier);
		}
	}

	/**
	 * @param string $identifier
	 */
	public function getSettingsKeyFromIdentifier($identifier = NULL)
	{
		$settingsKey = "mod";

		if($identifier!="")
		{
			$settingsKey = str_replace("controller","",strtolower(array_pop (explode("\\",$identifier))));
		}

		return $settingsKey;
	}

	/**
	 * copy
	 *
	 * @return $uid
	 */
	public function copyBackendRecord($tableName, $uid, $pageId, array $recordData = null)
	{
		$commandMap = [
			$tableName => [
				$uid => [
					'copy' => $pageId,
				],
			],
		];
		if ($recordData !== null) {
			$commandMap[$tableName][$uid]['copy'] = [
				'action' => 'paste',
				'target' => $pageId,
				'update' => $recordData,
			];
		}
		$this->createDataHandler();
		$this->dataHandler->start([], $commandMap);
		$this->dataHandler->process_cmdmap();
		return $this->dataHandler->copyMappingArray;
	}

	/**
	 * initialize and get DataHandler
	 *
	 * @return DataHandler
	 */
	protected function createDataHandler()
	{
		$this->dataHandler = GeneralUtility::makeInstance(DataHandler::class);
		$backendUser = $this->getBackendUser();
		if (isset($backendUser->uc['copyLevels']))
		{
			$this->dataHandler->copyTree = $backendUser->uc['copyLevels'];
		}
		return $this->dataHandler;
	}

	/**
	 * @return DataHandler
	 */
	public function getDataHandler()
	{
		return $this->dataHandler;
	}

	/**
	 * @return \Neulandlotsen\MooxCore\Domain\Session\SessionHandler
	 */
	public function getBackendSession()
	{
		return $this->backendSession;
	}

	/**
	 * @param \Neulandlotsen\MooxCore\Domain\Session\SessionHandler $backendSession
	 */
	public function setBackendSession(&$backendSession)
	{
		$this->backendSession = $backendSession;
	}

	/**
	 * @return string
	 */
	public function getAllowedListViewFields()
	{
		return $this->allowedListViewFields;
	}

	/**
	 * @param string $allowedListViewFields
	 */
	public function setAllowedListViewFields($allowedListViewFields)
	{
		$this->allowedListViewFields = $allowedListViewFields;
	}

	/**
	 * @return \TYPO3\CMS\Core\Authentication\BackendUserAuthentication
	 */
	protected function getBackendUser()
	{
		return $GLOBALS['BE_USER'];
	}
}
?>