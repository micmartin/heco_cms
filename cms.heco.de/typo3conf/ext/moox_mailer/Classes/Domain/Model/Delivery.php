<?php
namespace Neulandlotsen\MooxMailer\Domain\Model;

/**
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */
 
use TYPO3\CMS\Extbase\DomainObject\AbstractEntity;
use TYPO3\CMS\Extbase\Persistence\ObjectStorage; 

class Delivery extends AbstractEntity
{
	
	/**
	 * @var DateTime
	 */
	protected $crdate;

	/**
	 * @var DateTime
	 */
	protected $tstamp;
	
	/**
	 * Sichtbarkeit
	 *
	 * @var \integer	
	 */
	protected $hidden;		
	
	/**
	 * fe user
	 *
	 * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Neulandlotsen\MooxMailer\Domain\Model\FrontendUser>
	 */
	protected $feUser = NULL;
	
	/**
	 * address
	 *
	 * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Neulandlotsen\MooxMailer\Domain\Model\Address>
	 */
	protected $address = NULL;
	
	/**
	 * Titel
	 *
	 * @var string
	 * @TYPO3\CMS\Extbase\Annotation\Validate("NotEmpty")
	 */
	protected $title;
	
	/**
	 * job
	 *
	 * @var string
	 */
	protected $job;
	
	/**
	 * job type
	 *
	 * @var string
	 */
	protected $jobType;
	
	/**
	 * first opening
	 *
	 * @var int
	 */
	protected $firstOpening;
	
	/**
	 * last opening
	 *
	 * @var int
	 */
	protected $lastOpening;
	
	/**
	 * openings
	 *
	 * @var int
	 */
	protected $openings;
	
	/**
	 * openings unique
	 *
	 * @var int
	 */
	protected $openingsUnique;
	
	/**
	 * first click
	 *
	 * @var int
	 */
	protected $firstClick;
	
	/**
	 * last click
	 *
	 * @var int
	 */
	protected $lastClick;
	
	/**
	 * clicks
	 *
	 * @var int
	 */
	protected $clicks;
	
	/**
	 * clicks unique
	 *
	 * @var int
	 */
	protected $clicksUnique;
	
	/**
	 * sent
	 *
	 * @var int
	 */
	protected $sent;
	
	/**
	 * bounced
	 *
	 * @var int
	 */
	protected $bounced;
	
	/**
	 * failed
	 *
	 * @var int
	 */
	protected $failed;
	
	/**
	 * __construct
	 */
	public function __construct()
	{
		//Do not remove the next line: It would break the functionality
		$this->initStorageObjects();
	}

	/**
	 * Initializes all ObjectStorage properties
	 * Do not modify this method!
	 *
	 * @return void
	 */
	protected function initStorageObjects()
	{	
		$this->feUser = new ObjectStorage();
		$this->address = new ObjectStorage();
	}
	
	/**
	 * Get creation date
	 *
	 * @return int
	 */
	public function getCrdate()
	{
		return $this->crdate;
	}

	/**
	 * Set creation date
	 *
	 * @param int $crdate
	 * @return void
	 */
	public function setCrdate($crdate)
	{
		$this->crdate = $crdate;
	}

	/**
	 * Get year of crdate
	 *
	 * @return int
	 */
	public function getYearOfCrdate()
	{
		return $this->getCrdate()->format('Y');
	}

	/**
	 * Get month of crdate
	 *
	 * @return int
	 */
	public function getMonthOfCrdate()
	{
		return $this->getCrdate()->format('m');
	}

	/**
	 * Get day of crdate
	 *
	 * @return int
	 */
	public function getDayOfCrdate()
	{
		return (int)$this->crdate->format('d');
	}

	/**
	 * Get timestamp
	 *
	 * @return int
	 */
	public function getTstamp()
	{
		return $this->tstamp;
	}

	/**
	 * Set time stamp
	 *
	 * @param int $tstamp time stamp
	 * @return void
	 */
	public function setTstamp($tstamp)
	{
		$this->tstamp = $tstamp;
	}
	
	/**
	 * Returns the hidden
	 *
	 * @return int $hidden
	 */
	public function getHidden()
	{
		return $this->hidden;
	}

	/**
	 * Sets the hidden
	 *
	 * @param int $hidden
	 * @return void
	 */
	public function setHidden($hidden)
	{
		$this->hidden = $hidden;
	}
	
	/**
	 * Adds a fe user
	 *
	 * @param \Neulandlotsen\MooxMailer\Domain\Model\FrontendUser $feUser
	 * @return void
	 */
	public function addFeUser(\Neulandlotsen\MooxMailer\Domain\Model\FrontendUser $feUser)
	{
		$this->feUser->attach($feUser);
	}

	/**
	 * Removes a fe user
	 *
	 * @param \Neulandlotsen\MooxMailer\Domain\Model\FrontendUser $feUserToRemove The fe user to be removed
	 * @return void
	 */
	public function removeFeUser(\Neulandlotsen\MooxMailer\Domain\Model\FrontendUser $feUserToRemove)
	{
		$this->feUser->detach($feUserToRemove);
	}

	/**
	 * Returns the fe user
	 *
	 * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Neulandlotsen\MooxMailer\Domain\Model\FrontendUser> $feUser
	 */
	public function getFeUser()
	{
		return $this->feUser;
	}

	/**
	 * Sets the fe user
	 *
	 * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Neulandlotsen\MooxMailer\Domain\Model\FrontendUser> $feUser
	 * @return void
	 */
	public function setFeUser(ObjectStorage $feUser)
	{
		$this->feUser = $feUser;
	}
	
	/**
	 * Adds a address
	 *
	 * @param \Neulandlotsen\MooxMailer\Domain\Model\Address $address
	 * @return void
	 */
	public function addAddress(\Neulandlotsen\MooxMailer\Domain\Model\Address $address)
	{
		$this->address->attach($address);
	}

	/**
	 * Removes a address
	 *
	 * @param \Neulandlotsen\MooxMailer\Domain\Model\Address $addressToRemove The address to be removed
	 * @return void
	 */
	public function removeAddress(\Neulandlotsen\MooxMailer\Domain\Model\Address $addressToRemove)
	{
		$this->address->detach($addressToRemove);
	}

	/**
	 * Returns the address
	 *
	 * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Neulandlotsen\MooxMailer\Domain\Model\Address> $address
	 */
	public function getAddress()
	{
		return $this->address;
	}

	/**
	 * Sets the address
	 *
	 * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Neulandlotsen\MooxMailer\Domain\Model\Address> $address
	 * @return void
	 */
	public function setAddress(ObjectStorage $address)
	{
		$this->address = $address;
	}

	/**
	 * Returns the title
	 *
	 * @return string $title
	 */
	public function getTitle()
	{
		return $this->title;
	}
	
	/**
	 * Sets the title
	 *
	 * @param string $title
	 * @return void
	 */
	public function setTitle($title)
	{
		$this->title = $title;
	}

	/**
	 * Returns the job
	 *
	 * @return string $job
	 */
	public function getJob()
	{
		return $this->job;
	}

	/**
	 * Sets the job
	 *
	 * @param string $job
	 * @return void
	 */
	public function setJob($job)
	{
		$this->job = $job;
	}
	
	/**
	 * Returns the job type
	 *
	 * @return string $jobType
	 */
	public function getJobType()
	{
		return $this->jobType;
	}
	
	/**
	 * Sets the job type
	 *
	 * @param string $jobType type
	 * @return void
	 */
	public function setJobType($jobType)
	{
		$this->jobType = $jobType;
	}
	
	/**
	 * Returns the first opening
	 *
	 * @return int $firstOpening
	 */
	public function getFirstOpening()
	{
		return $this->firstOpening;
	}

	/**
	 * Sets the first opening
	 *
	 * @param int $firstOpening
	 * @return void
	 */
	public function setFirstOpening($firstOpening)
	{
		$this->firstOpening = $firstOpening;
	}
	
	/**
	 * Returns the last opening
	 *
	 * @return int $lastOpening
	 */
	public function getLastOpening()
	{
		return $this->lastOpening;
	}

	/**
	 * Sets the last opening
	 *
	 * @param int $lastOpening
	 * @return void
	 */
	public function setLastOpening($lastOpening)
	{
		$this->lastOpening = $lastOpening;
	}
	
	/**
	 * Returns the openings
	 *
	 * @return int $openings
	 */
	public function getOpenings() {
		return $this->openings;
	}

	/**
	 * Sets the openings
	 *
	 * @param int $openings
	 * @return void
	 */
	public function setOpenings($openings)
	{
		$this->openings = $openings;
	}
		
	/**
	 * Returns the first click
	 *
	 * @return int $firstClick
	 */
	public function getFirstClick()
	{
		return $this->firstClick;
	}

	/**
	 * Sets the first click
	 *
	 * @param int $firstClick
	 * @return void
	 */
	public function setFirstClick($firstClick)
	{
		$this->firstClick = $firstClick;
	}
	
	/**
	 * Returns the last click
	 *
	 * @return int $lastClick
	 */
	public function getLastClick()
	{
		return $this->lastClick;
	}

	/**
	 * Sets the last click
	 *
	 * @param int $lastClick
	 * @return void
	 */
	public function setLastClick($lastClick)
	{
		$this->lastClick = $lastClick;
	}
	
	/**
	 * Returns the clicks
	 *
	 * @return int $clicks
	 */
	public function getClicks()
	{
		return $this->clicks;
	}

	/**
	 * Sets the clicks
	 *
	 * @param int $clicks
	 * @return void
	 */
	public function setClicks($clicks)
	{
		$this->clicks = $clicks;
	}	
	
	/**
	 * Returns the sent
	 *
	 * @return int $sent
	 */
	public function getSent()
	{
		return $this->sent;
	}

	/**
	 * Sets the sent
	 *
	 * @param int $sent
	 * @return void
	 */
	public function setSent($sent)
	{
		$this->sent = $sent;
	}	
	
	/**
	 * Returns the bounced
	 *
	 * @return int $bounced
	 */
	public function getBounced()
	{
		return $this->bounced;
	}

	/**
	 * Sets the bounced
	 *
	 * @param int $bounced
	 * @return void
	 */
	public function setBounced($bounced)
	{
		$this->bounced = $bounced;
	}	
	
	/**
	 * Returns the failed
	 *
	 * @return int $failed
	 */
	public function getFailed()
	{
		return $this->failed;
	}

	/**
	 * Sets the failed
	 *
	 * @param int $failed
	 * @return void
	 */
	public function setFailed($failed)
	{
		$this->failed = $failed;
	}	
}
?>