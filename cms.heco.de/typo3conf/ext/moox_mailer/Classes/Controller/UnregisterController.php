<?php
namespace Neulandlotsen\MooxMailer\Controller;

/**
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */
 
use Neulandlotsen\MooxMailer\Service\MailerService;
 
class UnregisterController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController 
{
		
	/**
	 * action redirect
	 *
	 * @param int $id
	 * @param string $email
	 * @param string $hash
	 * @return void
	 */
	public function redirectAction($id = 0, $email = "", $hash = "")
	{		
		$baseUrl = MailerService::getBaseUrl($id);
		
		if($baseUrl!=""){
			
			if(\TYPO3\CMS\Core\Utility\VersionNumberUtility::convertVersionNumberToInteger(\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getExtensionVersion("moox_address"))>=8007016)
			{
				$plugin = "Pi2";
			}
			else
			{
				$plugin = "Pi1";
			}
			
			$uri = $this->uriBuilder->reset()->setCreateAbsoluteUri(true)->setTargetPageUid($id)->setNoCache(TRUE)->uriFor("unregister",array("email" => $email, "hash" => $hash),$plugin,"MooxAddress",$plugin);
			if($plugin=="Pi2")
			{
				$uri = $this->uriBuilder->reset()->setCreateAbsoluteUri(true)->setTargetPageUid($id)->setNoCache(TRUE)->uriFor("unregister",array("email" => $email, "hash" => $hash),$plugin,"MooxAddress",$plugin);
				$uri = str_replace("type=9002&","",$uri);
				$this->redirectToURI($uri);				
			}
			else
			{
				$uri = $this->uriBuilder->setTargetPageUid($id)->setNoCache(TRUE)->uriFor("unregister",array("email" => $email, "hash" => $hash),$plugin,"MooxAddress",$plugin);
				$this->redirectToURI($baseUrl.$uri);
			}			
			
		} else {
			echo "can't redirect to page";
			exit();
		}
	}
}
?>