<?php
namespace Neulandlotsen\MooxMailer\Domain\Model;

/**
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */

use TYPO3\CMS\Extbase\DomainObject\AbstractEntity;
 
class Zipregion extends AbstractEntity
{	
	/**
	 * @var string
	 * @TYPO3\CMS\Extbase\Annotation\Validate("NotEmpty")
	 */
	protected $title;
	
	/**
	 * @var string
	 * @TYPO3\CMS\Extbase\Annotation\Validate("NotEmpty")
	 */
	protected $mode;
	
	/**
	 * @var string
	 * @TYPO3\CMS\Extbase\Annotation\Validate("NotEmpty")
	 */
	protected $lower;
	
	/**
	 * @var string
	 * @TYPO3\CMS\Extbase\Annotation\Validate("NotEmpty")
	 */
	protected $upper;
	
	/**
	 * @var string
	 * @TYPO3\CMS\Extbase\Annotation\Validate("NotEmpty")
	 */
	protected $list;
	
	/**
	 * @return string $title
	 */
	public function getTitle()
	{
		return $this->title;
	}

	/**
	 * @param string $title
	 * @return void
	 */
	public function setTitle($title)
	{
		$this->title = $title;
	}
	
	/**
	 * @return string $mode
	 */
	public function getMode()
	{
		return $this->mode;
	}

	/**
	 * @param string $mode
	 * @return void
	 */
	public function setMode($mode)
	{
		$this->mode = $mode;
	}
	
	/**
	 * @return string $lower
	 */
	public function getLower()
	{
		return $this->lower;
	}

	/**
	 * @param string $lower
	 * @return void
	 */
	public function setLower($lower)
	{
		$this->lower = $lower;
	}
	
	/**
	 * @return string $upper
	 */
	public function getUpper()
	{
		return $this->upper;
	}

	/**
	 * @param string $upper
	 * @return void
	 */
	public function setUpper($upper)
	{
		$this->upper = $upper;
	}
	
	/**
	 * @return string $list
	 */
	public function getList()
	{
		return $this->list;
	}

	/**
	 * @param string $list
	 * @return void
	 */
	public function setList($list)
	{
		$this->list = $list;
	}
}
?>