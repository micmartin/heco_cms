define([
	'jquery',
	'TYPO3/CMS/MooxCore/Backend/Module'], function ($, MooxModule) {

	var MooxMailerMod9 = {};

	MooxMailerMod9.init = function ()
	{
		MooxModule.initClearButtons();
		
		$(document).on('submit', '#moox-mailer-module-filter form.use-ajax', function(event) {
		
			event.preventDefault();
			
			var url = $(this).attr("action");
			var wrapper = MooxMailerMod9.getWrapper($(this));
			var form = "#" + $(this).attr("id");
			
			var data = { };
				
			$(form + ' input.is-grouped-filter').each(function() {
				data[$(this).attr('name')] = $(this).val();		
			});
			
			$(form + ' select.is-grouped-filter').each(function() {
				data[$(this).attr('name')] = $(this).val();		
			});
			
			$.ajax(url, {
				method: "POST",
				cache: false,
				data: data,
				success: function(data) {				
					/*mooxAddressMod9Refresh(wrapper);*/
					body = $("#"+wrapper+".use-ajax #moox-mailer-module-body").first();								
					window.location = body.data('url');
				},
				error: function(data) {				
					
				},
				beforeSend: function(){
					$("body").append("<div id='moox-mailer-module-overlay'><img src='../typo3conf/ext/moox_core/Resources/Public/Images/Backend/loading.gif' /></div>");
				},
				complete: function(){
					/*$("#moox-mailer-module-overlay").remove();*/
				},
			});
			
			return false;
		});
			
		$(document).on('click', '#moox-mailer-module-body a.show-detail', function(event) {
			event.preventDefault();
			
			visible = false;
			if($('#moox-mailer-module-body #template-detail-'+$(this).data('uid')).is(":visible")){
				visible = true;			
			} 
			
			$('#moox-mailer-module-body .template-detail').hide();
			if(!visible && $('#moox-mailer-module-body #template-detail-'+$(this).data('uid'))){
				$('#moox-mailer-module-body #template-detail-'+$(this).data('uid')).show();
			}
			$('#moox-mailer-module-body a.show-detail .icon-up').hide();
			$('#moox-mailer-module-body a.show-detail .icon-down').show();
			if(visible){
				$(this).find('.icon-down').show();
				$(this).find('.icon-up').hide();
			} else {
				$(this).find('.icon-down').hide();
				$(this).find('.icon-up').show();
				
			}
		});
		
		$(document).on('change', '#moox-mailer-module-filter select.auto-submit', function(event) {
			event.preventDefault();
			$('#moox-mailer-module-filter form').submit();		
		});
		
		$(document).on('change', '#moox-mailer-module-filter input.auto-submit', function(event) {
			event.preventDefault();
			$('#moox-mailer-module-filter form').submit();		
		});
		
		$(document).on('click', '#moox-mailer-module-filter a.filter-reset', function(event) {
			event.preventDefault();
			
			var url = $(this).attr("href");
			var wrapper = MooxMailerMod9.getWrapper($(this));
			
			var data = { };
			data['tx_mooxmailer_mooxmailermooxmailer_mooxmailertemplatemanagement[ajax]'] = 1;
			
			$.ajax(url, {
				method: "POST",
				cache: false,
				data: data,
				success: function(data) {				
					body = $("#"+wrapper+".use-ajax #moox-mailer-module-body").first();								
					window.location = body.data('url');
				},
				error: function(data) {				
					
				},
				beforeSend: function(){
					$("body").append("<div id='moox-mailer-module-overlay'><img src='../typo3conf/ext/moox_core/Resources/Public/Images/Backend/loading.gif' /></div>");
				},
				complete: function(){
					/*$("#moox-mailer-module-overlay").remove();*/
				},
			});
			
			return false;		
		});
		
		$(document).on('click', '.moox-mailer-module.use-ajax #moox-mailer-module-body a.change-ordering', function(event) {
			event.preventDefault();
			
			var url = $(this).attr("href");
			var wrapper = MooxMailerMod9.getWrapper($(this));
			
			var data = { };
			data['tx_mooxmailer_mooxmailermooxmailer_mooxmailertemplatemanagement[ajax]'] = 1;
			
			$.ajax(url, {
				method: "POST",
				cache: false,
				data: data,
				success: function(data) {				
					body = $("#"+wrapper+".use-ajax #moox-mailer-module-body").first();								
					window.location = body.data('url');
				},
				error: function(data) {				
					
				},
				beforeSend: function(){
					$("body").append("<div id='moox-mailer-module-overlay'><img src='../typo3conf/ext/moox_core/Resources/Public/Images/Backend/loading.gif' /></div>");
				},
				complete: function(){
					/*$("#moox-mailer-module-overlay").remove();*/
				},
			});
			
			return false;		
		});
	};

	MooxMailerMod9.refresh = function (wrapper)
	{			
		var body = $("#"+wrapper+".use-ajax #moox-mailer-module-body").first();		
		
		var data = { };
		
		data['tx_mooxmailer_mooxmailermooxmailer_mooxmailertemplatemanagement[ajax]'] = 1;
		
		$.ajax(body.data("url"), {
			method: "POST",
			data: data,
			success: function(data) {			
				body.html($(data));				
			},
			beforeSend: function(){
				$("body").append("<div id='moox-mailer-module-overlay'><img src='../typo3conf/ext/moox_core/Resources/Public/Images/Backend/loading.gif' /></div>");
			},
			complete: function(){
				$("#moox-mailer-module-overlay").remove();
			},
		});
	};
	
	MooxMailerMod9.getWrapper = function (template)
	{
		return template.parents('.moox-mailer-module-mod9').attr("id");
	};

	MooxMailerMod9.init();

	return MooxMailerMod9;

});