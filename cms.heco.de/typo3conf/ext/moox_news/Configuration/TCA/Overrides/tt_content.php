<?php
defined('TYPO3_MODE') or die();

/***************
 * Plugin
 */
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'moox_news',
    'Pi1',
    'LLL:EXT:moox_news/Resources/Private/Language/locallang_be.xlf:pi1_title'
);

$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_excludelist']['mooxnews_pi1'] = 'recursive,select_key,pages';
$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_addlist']['mooxnews_pi1'] = 'pi_flexform';
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue('mooxnews_pi1',
    'FILE:EXT:moox_news/Configuration/FlexForms/flexform_mooxnews.xml');

// TypoScript
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile('moox_news', 'Configuration/TypoScript/default', 'MOOX News (Default)');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile('moox_news', 'Configuration/TypoScript/preview', 'MOOX News (Preview)');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile('moox_news', 'Configuration/TypoScript/rss', 'MOOX News (RSS)');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile('moox_news', 'Configuration/TypoScript/Sitemap', 'MOOX News Sitemap');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile('moox_news', 'Configuration/TypoScript/Styles/Twb', 'MOOX News Styles Twitter Bootstrap');
