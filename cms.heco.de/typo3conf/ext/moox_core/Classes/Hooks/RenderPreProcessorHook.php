<?php
namespace Neulandlotsen\MooxCore\Hooks;

/**
 * This file is part of the "moox_core" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

use TYPO3\CMS\Core\Cache\Backend\FileBackend;
use TYPO3\CMS\Core\Cache\CacheManager;
use TYPO3\CMS\Core\Page\PageRenderer;
use TYPO3\CMS\Core\Utility\DebugUtility;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Log\LogManager;
use TYPO3\CMS\Core\Utility\VersionNumberUtility;
use TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer;
use Leafo\ScssPhp\Compiler;
use Leafo\ScssPhp\Formatter\Compressed;
use TYPO3\CMS\Core\Core\Environment;

/**
 * Hook to preprocess scss files
 *
 * @author Sven Wappler <typo3YYYY@wapplersystems.de>
 * @author Jozef Spisiak <jozef@pixelant.se>
 *
 */
class RenderPreProcessorHook
{
	/**
	 * @var string
	 */
	public static $compiledFileName = 'moox_theme';

	/**
	 * @var string
	 */
	public static $defaultOutputDir = 'typo3temp/mooxcore/';

	/**
	 * @var array
	 */
	private static $visitedFiles = [];

	/**
	 * @var array
	 */
	private $variables = [];

	/**
	 * @var bool
	 */
	protected $extConf;

	/**
	 * @var \TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer
	 */
	private $contentObjectRenderer;

	/**
	 * Main hook function
	 *
	 * @param array $params Array of CSS/javascript and other files
	 * @param PageRenderer $pagerenderer Pagerenderer object
	 * @return void
	 * @throws \BadFunctionCallException
	 * @throws \RuntimeException
	 * @throws \InvalidArgumentException
	 */
	public function renderPreProcessorProc(&$params, PageRenderer $pagerenderer)
	{
	    if (!is_array($params['cssFiles'])) {
			return;
		}

		// set ext conf
		$this->extConf = GeneralUtility::makeInstance(\TYPO3\CMS\Core\Configuration\ExtensionConfiguration::class)->get('moox_core');

	    $filePathSanitizer = GeneralUtility::makeInstance(\TYPO3\CMS\Frontend\Resource\FilePathSanitizer::class);

		$setup = $GLOBALS['TSFE']->tmpl->setup;
		if (is_array($setup['plugin.']['tx_mooxcore.']['variables.'])) {

			$variables = $setup['plugin.']['tx_mooxcore.']['variables.'];

			$parsedTypoScriptVariables = [];
			foreach ($variables as $variable => $key)
			{
				if (array_key_exists($variable . '.', $variables))
				{
					if ($this->contentObjectRenderer === null)
					{
						$this->contentObjectRenderer = GeneralUtility::makeInstance(ContentObjectRenderer::class);
					}
					$content = $this->contentObjectRenderer->cObjGetSingle($variables[$variable], $variables[$variable . '.']);
					$parsedTypoScriptVariables[$variable] = $content;

				}
				elseif (substr($variable, -1) !== '.')
				{
					$parsedTypoScriptVariables[$variable] = $key;
				}
			}
			$this->variables = $parsedTypoScriptVariables;
		}

		$variablesHash = count($this->variables) > 0 ? hash('md5',implode(',', $this->variables)) : null;

		// we need to rebuild the CSS array to keep order of CSS files
		$cssFiles = [];
		foreach ($params['cssFiles'] as $file => $conf)
		{
			$pathInfo = pathinfo($conf['file']);

			if ($pathInfo['extension'] !== 'scss')
			{
				$cssFiles[$file] = $conf;
				continue;
			}

			$outputDir = self::$defaultOutputDir;

			$inlineOutput = false;
			$filename = $pathInfo['filename'];
			$formatter = null;
			$showLineNumber = false;
			$useSourceMap = false;
			$outputFile = '';

			// search settings for scss file
			foreach ($GLOBALS['TSFE']->pSetup['includeCSS.'] as $key => $subconf)
			{
			    try
                {
                    if (is_string($GLOBALS['TSFE']->pSetup['includeCSS.'][$key]) && $filePathSanitizer->sanitize($GLOBALS['TSFE']->pSetup['includeCSS.'][$key]) === $filePathSanitizer->sanitize($file))
                    {
                        $outputDir = isset($GLOBALS['TSFE']->pSetup['includeCSS.'][$key . '.']['outputdir']) ? trim($GLOBALS['TSFE']->pSetup['includeCSS.'][$key . '.']['outputdir']) : $outputDir;
                        $outputFile = isset($GLOBALS['TSFE']->pSetup['includeCSS.'][$key . '.']['outputfile']) ? trim($GLOBALS['TSFE']->pSetup['includeCSS.'][$key . '.']['outputfile']) : null;

                        if($this->extConf['scssCompilerDebugMode'])
                        {
                            $formatter = "Leafo\ScssPhp\Formatter\Nested";
                            $showLineNumber = true;
                        }
                        else
                        {
                            $formatter = isset($GLOBALS['TSFE']->pSetup['includeCSS.'][$key . '.']['formatter']) ? trim($GLOBALS['TSFE']->pSetup['includeCSS.'][$key . '.']['formatter']) : null;
                            $showLineNumber = false;
                        }

                        if (isset($GLOBALS['TSFE']->pSetup['includeCSS.'][$key . '.']['linenumber']))
                        {
                            if ($GLOBALS['TSFE']->pSetup['includeCSS.'][$key . '.']['linenumber'] === 'true' || (int)$GLOBALS['TSFE']->pSetup['includeCSS.'][$key . '.']['linenumber'] === 1) {
                                $showLineNumber = true;
                            }
                        }
                        $useSourceMap = isset($GLOBALS['TSFE']->pSetup['includeCSS.'][$key . '.']['sourceMap']) ? true : false;

                        if (isset($GLOBALS['TSFE']->pSetup['includeCSS.'][$key . '.']['inlineOutput']))
                        {
                            $inlineOutput = (bool)trim($GLOBALS['TSFE']->pSetup['includeCSS.'][$key . '.']['inlineOutput']);
                        }
                    }
                }
                catch (\TYPO3\CMS\Core\Resource\Exception\FileDoesNotExistException $exception)
                {
                   // ToDo überprüfen 2019-08-30
                }

			}

			if ($outputFile != '')
			{
				$outputDir = dirname($outputFile);
				$filename = basename($outputFile);
			}

			$outputDir = (substr($outputDir, -1) === '/') ? $outputDir : $outputDir . '/';

			if (!strcmp(substr($outputDir, 0, 4), 'EXT:'))
			{
				[$extKey, $script] = explode('/', substr($outputDir, 4), 2);
				if ($extKey && ExtensionManagementUtility::isLoaded($extKey))
				{
					$extPath = ExtensionManagementUtility::extPath($extKey);
					$outputDir = substr($extPath, strlen(Environment::getPublicPath() . '/')) . $script;
				}
			}

			$scssFilename = GeneralUtility::getFileAbsFileName($conf['file']);

			// create filename - hash is important due to the possible
			// conflicts with same filename in different folders
			GeneralUtility::mkdir_deep(Environment::getPublicPath() . '/' . $outputDir);
			$cssRelativeFilename = $outputDir . $filename . (($outputDir === self::$defaultOutputDir) ? '_' . hash('sha1',$file) : (count($this->variables) > 0 ? '_'.$variablesHash : '')) . (($formatter=='Leafo\ScssPhp\Formatter\Compressed')?'.min':'').'.css';
			$cssFilename = Environment::getPublicPath() . '/' . $cssRelativeFilename;

			/** @var FileBackend $cache */
			$cache = GeneralUtility::makeInstance(CacheManager::class)->getCache('moox_core_scss');

			$cacheKey = hash('sha1', $cssRelativeFilename);
			$contentHash = $this->calculateContentHash($scssFilename, implode(',', $this->variables));
			if ($showLineNumber)
			{
				$contentHash .= 'l1';
			}
			if ($useSourceMap)
			{
				$contentHash .= 'sm';
			}
			$contentHash .= md5($formatter);
			$contentHashCache = '';
			if ($cache->has($cacheKey))
			{
				$contentHashCache = $cache->get($cacheKey);
			}

			$css = '';

			try
			{
				if ($contentHashCache === '' || $contentHashCache !== $contentHash)
				{
					$css = $this->compileScss($scssFilename, $cssFilename, $this->variables, $showLineNumber, $formatter, $cssRelativeFilename, $useSourceMap);

					$cache->set($cacheKey, $contentHash, ['scss'], 0);
				}
			}
			catch (Exception $ex)
			{
				DebugUtility::debug($ex->getMessage());

				/** @var $logger \TYPO3\CMS\Core\Log\Logger */
				$logger = GeneralUtility::makeInstance(LogManager::class)->getLogger(__CLASS__);
				$logger->error($ex->getMessage());
			}

			if ($inlineOutput)
			{
				unset($cssFiles[$cssRelativeFilename]);
				if ($css === '')
				{
					$css = file_get_contents($cssFilename);
				}

				// TODO: compression
				$params['cssInline'][$cssRelativeFilename] = [
					'code' => $css,
				];
			}
			else
			{
				$cssFiles[$cssRelativeFilename] = $params['cssFiles'][$file];
				$cssFiles[$cssRelativeFilename]['file'] = $cssRelativeFilename;
			}


		}
		$params['cssFiles'] = $cssFiles;
	}

	/**
	 * Compiling Scss with scss
	 *
	 * @param string $scssFilename Existing scss file absolute path
	 * @param string $cssFilename File to be written with compiled CSS
	 * @param array $vars Variables to compile
	 * @param boolean $showLineNumber Show line numbers
	 * @param string $formatter name
	 * @param string $cssRelativeFilename
	 * @param boolean $useSourceMap Use SourceMap
	 * @return string
	 * @throws \BadFunctionCallException
	 */
	protected function compileScss($scssFilename, $cssFilename, $vars = [], $showLineNumber = false, $formatter = null, $cssRelativeFilename = null, $useSourceMap = false): string
	{
		$parser = new Compiler();

		if (file_exists($scssFilename))
		{
			$parser->setVariables($vars);

			if ($showLineNumber)
			{
				$parser->setLineNumberStyle(Compiler::LINE_COMMENTS);
			}
			if ($formatter !== null)
			{
				$parser->setFormatter($formatter);
			}

			$importFolders = [GeneralUtility::dirname($scssFilename)];
			if(is_array($GLOBALS['TYPO3_CONF_VARS']['EXT']['moox_core']['scss']['folders']))
			{
				foreach($GLOBALS['TYPO3_CONF_VARS']['EXT']['moox_core']['scss']['folders'] AS $extKey => $folders)
				{
					if(ExtensionManagementUtility::isLoaded($extKey))
					{
						if(is_array($folders))
						{
							foreach($folders AS $folder)
							{
								$importFolders[] = ExtensionManagementUtility::extPath($extKey).'Resources/Private/Scss/'.$folder.'/';
							}
						}
						else
						{
							$importFolders[] = ExtensionManagementUtility::extPath($extKey).'Resources/Private/Scss/'.$folders.'/';
						}
					}
				}
			}

			$parser->setImportPaths($importFolders);

			if ($useSourceMap)
			{
				$parser->setSourceMap(Compiler::SOURCE_MAP_INLINE);

				$parser->setSourceMapOptions([
					'sourceMapWriteTo' => $cssFilename . '.map',
					'sourceMapURL' => $cssRelativeFilename . '.map',
					'sourceMapBasepath' => Environment::getPublicPath() . '/',
					'sourceMapRootpath' => '/',
				]);
			}

			$css = $parser->compile(file_get_contents($scssFilename));

			GeneralUtility::writeFile($cssFilename, $css);

			return $css;
		}

		return '';
	}

	/**
	 * Calculating content hash to detect changes
	 *
	 * @param string $scssFilename Existing scss file absolute path
	 * @param string $vars
	 * @return string
	 */
	protected function calculateContentHash($scssFilename, $vars = ''): string
	{
		if (in_array($scssFilename, self::$visitedFiles, true))
		{
			return '';
		}
		self::$visitedFiles[] = $scssFilename;

		$content = file_get_contents($scssFilename);
		$pathinfo = pathinfo($scssFilename);

		$hash = hash('sha1', $content);
		if ($vars !== '')
		{
			$hash = hash('sha1', $hash . $vars);
		} // hash variables too

		preg_match_all('/@import "([^"]*)"/', $content, $imports);

		foreach ($imports[1] as $import)
		{
			$hashImport = '';

			if (file_exists($pathinfo['dirname'] . '/' . $import . '.scss'))
			{
				$hashImport = $this->calculateContentHash($pathinfo['dirname'] . '/' . $import . '.scss');
			}
			else
				{
				$parts = explode('/', $import);
				$filename = '_' . array_pop($parts);
				$parts[] = $filename;
				if (file_exists($pathinfo['dirname'] . '/' . implode('/', $parts) . '.scss'))
				{
					$hashImport = $this->calculateContentHash($pathinfo['dirname'] . '/' . implode('/',$parts) . '.scss');
				}
			}
			if ($hashImport !== '')
			{
				$hash = hash('sha1', $hash . $hashImport);
			}
		}

		return $hash;
	}
}