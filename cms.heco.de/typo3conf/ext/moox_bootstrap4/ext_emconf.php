<?php
$EM_CONF[$_EXTKEY] = [
	'title' => 'MOOX Bootstrap V4',
	'description' => 'Bootstrap 4 für MOOX',
	'category' => 'fe',
	'state' => 'beta',
	'uploadfolder' => false,
	'clearCacheOnLoad' => true,
	'author' => 'Dominic Martin',
	'author_email' => 'typo3@neulandlotsen.de',
	'author_company' => 'Neulandlotsen',
	'version' => '9.5.2',
	'constraints' => [
		'depends' => [
			'moox_core' => '9.5.0-9.5.99',
			'rte_ckeditor_image' => '9.0.3-9.9.99',
		],
		'conflicts' => [],
		'suggests' => [],
	]	
];
?>