<?php
namespace Neulandlotsen\MooxNews\ViewHelpers;

/**
 * This file is part of the "moox_news" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

use Neulandlotsen\MooxNews\Domain\Repository\CategoryRepository;
use Neulandlotsen\MooxNews\Domain\Model\News;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Database\Query\Restriction\HiddenRestriction;
use TYPO3\CMS\Core\Database\Query\Restriction\DeletedRestriction;
use TYPO3\CMS\Core\Database\Query\Restriction\BackendWorkspaceRestriction;
use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper;

class CategoriesViewHelper extends AbstractViewHelper
{
	/**
	* @var CategoryRepository
	*/
	protected $categoryRepository;

    /**
     * @param CategoryRepository $categoryRepository
     */
    public function injectCategoryRepository(CategoryRepository $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
    }

	public function initializeArguments()
	{
		parent::initializeArguments();
		$this->registerArgument('settings', 'array', 'settings of news-plugin', true);
		$this->registerArgument('newsItem', News::class, 'news item', false);
		$this->registerArgument('as', 'string', 'return result as variable with this name', false);
	}

	/**
	 * @return string
	 */
	public function render()
	{
		$settings = $this->arguments['settings'];
		$as = $this->arguments['as'];
		$newsItem = $this->arguments['newsItem'];

		if(!$settings['categoryConjunction'])
		{
			$settings['categories'] = "";
		}

		if(is_object($newsItem))
		{
			$queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)
				->getQueryBuilderForTable('sys_category_record_mm');
			$queryBuilder
				->getRestrictions()
				->removeAll()
				->add(GeneralUtility::makeInstance(DeletedRestriction::class))
				->add(GeneralUtility::makeInstance(HiddenRestriction::class));

			$res = $queryBuilder
				->select('uid_local')
				->from('sys_category_record_mm')
				->where(
					$queryBuilder->expr()->eq('uid_foreign', $queryBuilder->createNamedParameter($newsItem->getUid(), \PDO::PARAM_INT)),
					$queryBuilder->expr()->eq('tablenames', $queryBuilder->createNamedParameter('tx_mooxnews_domain_model_news', \PDO::PARAM_STR)),
					$queryBuilder->expr()->eq('fieldname', $queryBuilder->createNamedParameter('categories', \PDO::PARAM_STR))
				)
				->orderBy('sorting_foreign','ASC')
				->execute();

			$idList = [];
			while($row = $res->fetch())
			{
				$idList[] = $row['uid_local'];
			}
			if(count($idList))
			{
				$categories = $this->categoryRepository->findByIdList($idList,[],NULL,true);
			}
		}
		elseif($settings['categories']!="")
		{
			$idList = explode(",",$settings['categories']);
			if(count($idList))
			{
				$categories = $this->categoryRepository->findTree($idList, NULL, false, $settings['categories']);
			}
		}
		else
		{
			$idList = [];
			$categories = $this->categoryRepository->findParentCategoriesByPids($settings['startingpoint'], $settings['categories']);
			foreach ($categories as $category) {
				$idList[] = $category->getUid();
			}
			if(count($idList))
			{
				$categories = $this->categoryRepository->findTree($idList, NULL, false, $settings['categories']);
			}
		}


		if($as)
		{
			$this->templateVariableContainer->add($this->arguments['as'], $categories);
		}
		else
		{
			return $categories;
		}
	}
}
