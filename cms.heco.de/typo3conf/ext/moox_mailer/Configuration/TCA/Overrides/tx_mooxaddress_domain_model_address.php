<?php
if (!defined('TYPO3_MODE')) {
	die('Access denied.');
}
$newMooxAddressColumns = array(		
	'quality' => array(
		'exclude' => 1,
		'label' => 'quality',
		'config' => array(
			'type' => 'passthrough',
		)
	),
	'bounces' => array(
		'exclude' => 1,
		'label' => 'LLL:EXT:moox_mailer/Resources/Private/Language/locallang_db.xlf:tx_mooxmailer_domain_model_bounce',
		'config' => array(
			'type' => 'select',
			'foreign_table' => 'tx_mooxmailer_domain_model_bounce',
			'foreign_field' => 'address',
			'size' => 10,
			'autoMaxSize' => 10,
			'maxitems'      => 999999,
			'multiple' => 0,
			'MM' => 'tx_mooxmailer_bounce_address_mm',
			'MM_opposite_field' => 'address'
		),
	),
	'errors' => array(
		'exclude' => 1,
		'label' => 'LLL:EXT:moox_mailer/Resources/Private/Language/locallang_db.xlf:tx_mooxmailer_domain_model_error',
		'config' => array(
			'type' => 'select',
			'foreign_table' => 'tx_mooxmailer_domain_model_error',
			'foreign_field' => 'address',
			'size' => 10,
			'autoMaxSize' => 10,
			'maxitems'      => 999999,
			'multiple' => 0,
			'MM' => 'tx_mooxmailer_error_address_mm',
			'MM_opposite_field' => 'address'
		),
	),
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('tx_mooxaddress_domain_model_address',$newMooxAddressColumns,1);
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes('tx_mooxaddress_domain_model_address', '--div--;LLL:EXT:moox_mailer/Resources/Private/Language/locallang_init.xlf:mlang_tabs_tab,bounces');
?>