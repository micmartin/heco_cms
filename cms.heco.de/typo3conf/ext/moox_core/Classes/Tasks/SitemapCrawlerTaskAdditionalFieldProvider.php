<?php
namespace Neulandlotsen\MooxCore\Tasks;

/**
 * This file is part of the "moox_core" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

use Neulandlotsen\MooxCore\Utility\HelperUtility;
use Neulandlotsen\MooxNewsSeminarEvewa\Utility\SchedulerHelper;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Utility\LocalizationUtility;
use TYPO3\CMS\Core\Messaging\FlashMessage;
use TYPO3\CMS\Core\Messaging\FlashMessageService;
use TYPO3\CMS\Scheduler\AbstractAdditionalFieldProvider;
use TYPO3\CMS\Scheduler\Task\Enumeration\Action;

class SitemapCrawlerTaskAdditionalFieldProvider extends AbstractAdditionalFieldProvider {

	/**
	 * This method is used to define new fields for adding or editing a task
	 * In this case, it adds an pid field
	 *
	 * @param array $taskInfo Reference to the array containing the info used in the add/edit form
	 * @param object $task When editing, reference to the current task object. Null when adding.
	 * @param \TYPO3\CMS\Scheduler\Controller\SchedulerModuleController $parentObject Reference to the calling object (Scheduler's BE module)
	 * @return array	Array containing all the information pertaining to the additional fields
	 */
	public function getAdditionalFields(array &$taskInfo, $task, \TYPO3\CMS\Scheduler\Controller\SchedulerModuleController $parentObject)
	{
        $currentSchedulerModuleAction = $parentObject->getCurrentAction();

	    // Initialize extra field value
		if (empty($taskInfo['sitemapUrl']))
		{
			if ($currentSchedulerModuleAction->equals(Action::EDIT))
			{
				// In case of edit, set to internal value if no data was submitted already
				$taskInfo['sitemapUrl'] = $task->sitemapUrl;
			} 
			else 
			{
				// Otherwise set an empty value, as it will not be used anyway
				$taskInfo['sitemapUrl'] = '';
			}
		}
		if (empty($taskInfo['clearCache']))
		{
			if ($currentSchedulerModuleAction->equals(Action::EDIT))
			{
				// In case of edit, set to internal value if no data was submitted already
				$taskInfo['clearCache'] = $task->clearCache;
			}
			else
			{
				// Otherwise set an empty value, as it will not be used anyway
				$taskInfo['clearCache'] = 0;
			}
		}
		
		$additionalFields = [];
				
		// Write the code for the field
		$fieldID = 'task_sitemapUrl';
		$fieldCode = '<input type="text" name="tx_scheduler[sitemapUrl]" id="' . $fieldID . '" value="' . $taskInfo['sitemapUrl'] . '" class="form-control" />';
		$additionalFields[$fieldID] = array(
			'code'     => $fieldCode,
			'label' => LocalizationUtility::translate('LLL:EXT:moox_core/Resources/Private/Language/locallang_scheduler.xlf:tx_mooxcore_tasks_sitemapcrawlertask.sitemap_url_label', 'moox_core'),
			'cshKey'   => '_MOD_tools_txschedulerM1',
			'cshLabel' => $fieldID
		);

		// Write the code for the field
		$fieldID = 'task_clearCache';
		$fieldCode = '<input type="checkbox" name="tx_scheduler[clearCache]" id="' . $fieldID . '" value="1" '.($taskInfo['clearCache']?'checked="checked" ':'').'/>';
		$additionalFields[$fieldID] = [
			'code' => $fieldCode,
			'label' => LocalizationUtility::translate('LLL:EXT:moox_core/Resources/Private/Language/locallang_scheduler.xlf:tx_mooxcore_tasks_sitemapcrawlertask.clear_cache_label', 'moox_core'),
			'cshKey' => '_MOD_tools_txschedulerM1',
			'cshLabel' => $fieldID
		];

		return $additionalFields;
	}

	/**
	 * This method checks any additional data that is relevant to the specific task
	 * If the task class is not relevant, the method is expected to return TRUE
	 *
	 * @param array $submittedData Reference to the array containing the data submitted by the user
	 * @param \TYPO3\CMS\Scheduler\Controller\SchedulerModuleController $parentObject Reference to the calling object (Scheduler's BE module)
	 * @return boolean TRUE if validation was ok (or selected class is not relevant), FALSE otherwise
	 */
	public function validateAdditionalFields(array &$submittedData, \TYPO3\CMS\Scheduler\Controller\SchedulerModuleController $parentObject)
	{				
		$submittedData['sitemapUrl'] = (string)$submittedData['sitemapUrl'];

		if ($submittedData['sitemapUrl']=="" || !HelperUtility::request($submittedData['sitemapUrl']))
		{
			$this->addMessage($GLOBALS['LANG']->sL('LLL:EXT:moox_core/Resources/Private/Language/locallang_scheduler.xlf:tx_mooxcore_tasks_sitemapcrawlertask.sitemap_url_error'), \TYPO3\CMS\Core\Messaging\FlashMessage::ERROR);
			$result = false;
		} 
		else 
		{
			$result = true;
		}

		return $result;
	}

	/**
	 * This method is used to save any additional input into the current task object
	 * if the task class matches
	 *
	 * @param array $submittedData Array containing the data submitted by the user
	 * @param \TYPO3\CMS\Scheduler\Task\AbstractTask $task Reference to the current task object
	 * @return void
	 */
	public function saveAdditionalFields(array $submittedData, \TYPO3\CMS\Scheduler\Task\AbstractTask $task)
	{
		$task->sitemapUrl = $submittedData['sitemapUrl'];
		$task->clearCache = $submittedData['clearCache'];
	}		
}

?>