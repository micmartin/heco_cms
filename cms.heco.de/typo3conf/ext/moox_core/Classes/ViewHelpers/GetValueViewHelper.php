<?php
namespace Neulandlotsen\MooxCore\ViewHelpers;

/**
 * This file is part of the "moox_core" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper;
use TYPO3\CMS\Core\Utility\GeneralUtility; 

class GetValueViewHelper extends AbstractViewHelper
{
	public function initializeArguments()
	{
		parent::initializeArguments();
		$this->registerArgument('item', 'mixed', 'source object/array', true);
		$this->registerArgument('field', 'string', 'field to return', true);
		$this->registerArgument('transform', 'string', 'transformation', false, NULL);
		$this->registerArgument('unit', 'string', 'unit', false, NULL);
		$this->registerArgument('raw', 'boolean', 'return raw data', false, true);
	}

	/**
	 * @return string
	 */
	public function render()
	{
		$item = $this->arguments['item'];
		$field = $this->arguments['field'];
		$transform = $this->arguments['transform'];
		$unit = $this->arguments['unit'];
		$raw = $this->arguments['raw'];

		$value = "";
		if($field != "")
		{
			$getCall = "get".GeneralUtility::underscoredToUpperCamelCase($field);
			if(method_exists($item, $getCall))
			{
				if($raw)
				{
					$value = $item->$getCall();
				}
				else
				{
					$value = $item->$getCall('preview');
				}
			}
			elseif(is_array($item))
			{
				$value = $item[$field];
			}
		}

		if($transform=="currency")
		{
			$value = number_format($value+0, 2, ',', '.');
		}
		elseif($transform=="float")
		{
			$value = number_format($value+0, 4, ',', '');
		}

		if($unit!="")
		{
			$value .= " ".$unit;
		}

		return $value;
	}
}