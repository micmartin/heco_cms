<?php

namespace Neulandlotsen\MooxNews\Hooks;

/**
 * This file is part of the "moox_news" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;

/**
 * AutoConfiguration-Hook for RealURL
 *
 */
class RealUrlAutoConfiguration
{

    /**
     * Generates additional RealURL configuration and merges it with provided configuration
     *
     * @param       array $params Default configuration
     * @return      array Updated configuration
     */
    public function addNewsConfig($params)
    {

        // Check for proper unique key
        //$postVar = (ExtensionManagementUtility::isLoaded('tt_news') ? 'tx_mooxnews' : 'news');

        return array_merge_recursive($params['config'], 
			[
                'postVarSets' => [
                    '_DEFAULT' => [
                        'article' => [
                            [
                                'GETvar' => 'tx_mooxnews_pi1[news]',
                                'lookUpTable' => [
                                    'table' => 'tx_mooxnews_domain_model_news',
                                    'id_field' => 'uid',
                                    'alias_field' => 'IF(path_segment!="",path_segment,title)',
                                    'addWhereClause' => ' AND NOT deleted',
                                    'useUniqueCache' => 1,
                                    'expireDays' => 180,
                                    'enable404forInvalidAlias' => true,
                                ],
                            ],
                        ],
						'controller' => [
							[
								'GETvar' => 'tx_mooxnews_pi1[action]',
								'noMatch' => 'bypass'
							],
							[
								'GETvar' => 'tx_mooxnews_pi1[controller]',
								'noMatch' => 'bypass'
							]
						],
						'dateFilter' => [
							[
								'GETvar' => 'tx_mooxnews_pi1[overwriteDemand][year]',
							],
							[
								'GETvar' => 'tx_mooxnews_pi1[overwriteDemand][month]',
							],
						],
						'page' => [
							[
								'GETvar' => 'tx_mooxnews_pi1[@widget_0][currentPage]',
							],
						],
                    ]
                ]
            ]
        );
    }
}