<?php
namespace Neulandlotsen\MooxNews\Hooks;

/**
 * This file is part of the "moox_news" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Persistence\QueryInterface;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Database\Query\Restriction\HiddenRestriction;
use TYPO3\CMS\Core\Database\Query\Restriction\DeletedRestriction;
use TYPO3\CMS\Core\Database\Query\Restriction\BackendWorkspaceRestriction;

class SchedulerHelper 
{
	/**
	 * Path to the locallang file
	 * @var string
	 */
	const LLPATH = 'LLL:EXT:moox_news/Resources/Private/Language/locallang_scheduler.xlf:';

	/**
	 * Get select box of folders with news articles
	 *
	 * @param string $selectorName selector name
	 * @param string $selected current storage pids
	 * @param bool $multiple allow multiple selection
	 * @return	string	Folder selector HTML code
	 */
	public static function getMooxNewsFoldersSelector($selectorName,$selected,$multiple=FALSE)
	{
		$selected = explode(",",$selected);

		$queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)
			->getQueryBuilderForTable('pages');

		$queryBuilder
			->getRestrictions()
			->removeAll()
			->add(GeneralUtility::makeInstance(DeletedRestriction::class))
			->add(GeneralUtility::makeInstance(HiddenRestriction::class));

		$folders = $queryBuilder
			->select('*')
			->from('pages')
			->where(
				$queryBuilder->expr()->eq('module', $queryBuilder->createNamedParameter('mxnews', \PDO::PARAM_STR))
			)
			->execute();

		$selector = '<select name="'.$selectorName.'[]" id="task_'.$selectorName.'" '.(($multiple)?'" multiple="multiple" size="5"':'').' class="form-control" >';

		while($folder = $folders->fetch())
		{
			$selector .= '<option '.(in_array($folder['uid'],$selected)?'selected="selected" ':'').'value="'.$folder['uid'].'">'.$folder['title'].' ['.$folder['uid'].']</option>';
		}

		$selector .= '</select>';

		return $selector;
	}
}