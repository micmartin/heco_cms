<?php
$return = array(
	'ctrl' => array(
		'title'	=> 'LLL:EXT:moox_mailer/Resources/Private/Language/locallang_db.xlf:tx_mooxmailer_domain_model_bounce',
		'label' => 'title',
		'tstamp' => 'tstamp',
		'crdate' => 'crdate',
		'cruser_id' => 'cruser_id',
		'dividers2tabs' => TRUE,
		//'sortby' => 'bounced',
		'default_sortby' => 'ORDER BY bounced DESC',
		'versioningWS' => 2,
		'versioning_followPages' => TRUE,
		'origUid' => 't3_origuid',
		'languageField' => 'sys_language_uid',
		'transOrigPointerField' => 'l10n_parent',
		'transOrigDiffSourceField' => 'l10n_diffsource',
		'delete' => 'deleted',
		'enablecolumns' => array(
			'disabled' => 'hidden',
			'starttime' => 'starttime',
			'endtime' => 'endtime',
		),		
		'searchFields' => 'title,body',
        'iconfile' => 'EXT:moox_mailer/Resources/Public/Icons/tx_mooxmailer_domain_model_bounce.png',
		'hideTable' => TRUE
	),
	'interface' => array(
		'showRecordFieldList' => 'sys_language_uid, l10n_parent, l10n_diffsource, fe_user, address, title',
	),
	'types' => array(
		'1' => array('showitem' => 'sys_language_uid;;;;1-1-1, l10n_parent, l10n_diffsource, fe_user, address, title'),
	),
	'palettes' => array(
		'1' => array('showitem' => ''),
	),
	'columns' => array(
		'sys_language_uid' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.language',
			'config' => array(
				'type' => 'select',
				'foreign_table' => 'sys_language',
				'foreign_table_where' => 'ORDER BY sys_language.title',
				'items' => array(
					array('LLL:EXT:lang/locallang_general.xlf:LGL.allLanguages', -1),
					array('LLL:EXT:lang/locallang_general.xlf:LGL.default_value', 0)
				),
			),
		),
		'l10n_parent' => array(
			'displayCond' => 'FIELD:sys_language_uid:>:0',
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.l18n_parent',
			'config' => array(
				'type' => 'select',
				'items' => array(
					array('', 0),
				),
				'foreign_table' => 'tx_mooxmailer_domain_model_bounce',
				'foreign_table_where' => 'AND tx_mooxmailer_domain_model_bounce.pid=###CURRENT_PID### AND tx_mooxmailer_domain_model_bounce.sys_language_uid IN (-1,0)',
			),
		),
		'l10n_diffsource' => array(
			'config' => array(
				'type' => 'passthrough',
			),
		),
		't3ver_label' => array(
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.versionLabel',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'max' => 255,
			)
		),
		'hidden' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.hidden',
			'config' => array(
				'type' => 'passthrough',
			)
		),
		'cruser_id' => array(
			'label' => 'cruser_id',
			'config' => array(
				'type' => 'passthrough'
			)
		),
		'pid' => array(
			'label' => 'pid',
			'config' => array(
				'type' => 'passthrough'
			)
		),
		'crdate' => array(
			'label' => 'crdate',
			'config' => array(
				'type' => 'passthrough',
			)
		),
		'tstamp' => array(
			'label' => 'tstamp',
			'config' => array(
				'type' => 'passthrough',
			)
		),
		'starttime' => array(
			'exclude' => 1,
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.starttime',
			'config' => array(
				'type' => 'passthrough',
			),
		),
		'endtime' => array(
			'exclude' => 1,
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.endtime',
			'config' => array(
				'type' => 'passthrough',
			),
		),
		'fe_user' => array(
			/*
			'exclude' => 1,
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => 'LLL:EXT:moox_mailer/Resources/Private/Language/locallang_db.xlf:tx_mooxmailer_domain_model_bounce.fe_user',
			'config' => array(
				'type' => 'group',
				'internal_type' => 'db',
				'allowed' => 'fe_users',
				'size' => 1,
				'maxitems' => 1,
				'minitems' => 0,				
				'wizards' => array(
					'suggest' => array(
						'type' => 'suggest',
					),
				),
			)
			*/
			'exclude' => 0,
			'label' => 'LLL:EXT:moox_mailer/Resources/Private/Language/locallang_db.xlf:tx_mooxmailer_domain_model_bounce.fe_user',
			'config' => array(
				'type' => 'select',
				'allowNonIdValues' => 1,
				'default' => '',
				'foreign_table' => 'fe_users',
				'foreign_table_where' => 'ORDER BY fe_users.last_name',
        		'size' => 1,				
				'maxitems'      => 1,
				'minitems'      => 0,
				'multiple' => 0,
        		'MM' => 'tx_mooxmailer_bounce_feuser_mm',
				'MM_opposite_field' => 'bounces',
				'items' => array(
					array('Keine Auswahl', ''),
				),				
				'wizards' => array(
					'_PADDING' => 1,
					'_VERTICAL' => 1,
          			'suggest' => array(
            		'type' => 'suggest'
          			),
				),
			),			
		),
		'address' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:moox_mailer/Resources/Private/Language/locallang_db.xlf:tx_mooxmailer_domain_model_bounce.address',
			'config' => array(
				'type' => 'select',
				'allowNonIdValues' => 1,
				'default' => '',
				'foreign_table' => 'tx_mooxaddress_domain_model_address',
				'foreign_table_where' => 'ORDER BY tx_mooxaddress_domain_model_address.surname',
				'foreign_field' => 'bounces',
        		'size' => 1,				
				'maxitems'      => 1,
				'minitems'      => 0,
				'multiple' => 0,
        		'MM' => 'tx_mooxmailer_bounce_address_mm',
				'MM_opposite_field' => 'bounces',
				'items' => array(
					array('Keine Auswahl', ''),
				),				
				'wizards' => array(
					'_PADDING' => 1,
					'_VERTICAL' => 1,
          			'suggest' => array(
            		'type' => 'suggest'
          			),
				),
			),			
		),
		'title' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:moox_mailer/Resources/Private/Language/locallang_db.xlf:tx_mooxmailer_domain_model_bounce.title',
			'config' => array(
				'type' => 'input',
				'size' => 40,
				'eval' => 'required,trim'
			),
		),
		'action' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:moox_mailer/Resources/Private/Language/locallang_db.xlf:tx_mooxmailer_domain_model_bounce.action',
			'config' => array(
				'type' => 'passthrough',
			),
		),
		'code' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:moox_mailer/Resources/Private/Language/locallang_db.xlf:tx_mooxmailer_domain_model_bounce.code',
			'config' => array(
				'type' => 'passthrough',
			),
		),
		'major_code' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:moox_mailer/Resources/Private/Language/locallang_db.xlf:tx_mooxmailer_domain_model_bounce.major_code',
			'config' => array(
				'type' => 'passthrough',
			),
		),
		'sub_code' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:moox_mailer/Resources/Private/Language/locallang_db.xlf:tx_mooxmailer_domain_model_bounce.sub_code',
			'config' => array(
				'type' => 'passthrough',
			),
		),
		'type' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:moox_mailer/Resources/Private/Language/locallang_db.xlf:tx_mooxmailer_domain_model_bounce.type',
			'config' => array(
				'type' => 'passthrough',
			),
		),	
		'type_description' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:moox_mailer/Resources/Private/Language/locallang_db.xlf:tx_mooxmailer_domain_model_bounce.type_description',
			'config' => array(
				'type' => 'passthrough',
			),
		),			
		'status' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:moox_mailer/Resources/Private/Language/locallang_db.xlf:tx_mooxmailer_domain_model_bounce.status',
			'config' => array(
				'type' => 'passthrough',
			),
		),	
		'status_description' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:moox_mailer/Resources/Private/Language/locallang_db.xlf:tx_mooxmailer_domain_model_bounce.status_description',
			'config' => array(
				'type' => 'passthrough',
			),
		),		
		'recipient' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:moox_mailer/Resources/Private/Language/locallang_db.xlf:tx_mooxmailer_domain_model_bounce.recipient',
			'config' => array(
				'type' => 'passthrough',
			),
		),			
		'mail' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:moox_mailer/Resources/Private/Language/locallang_db.xlf:tx_mooxmailer_domain_model_bounce.mail',
			'config' => array(
				'type' => 'passthrough',
			),
		),
		'job' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:moox_mailer/Resources/Private/Language/locallang_db.xlf:tx_mooxmailer_domain_model_bounce.job',
			'config' => array(
				'type' => 'passthrough',
			),
		),
		'job_type' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:moox_mailer/Resources/Private/Language/locallang_db.xlf:tx_mooxmailer_domain_model_bounce.job_type',
			'config' => array(
				'type' => 'passthrough',
			),
		),
		'receiver_type' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:moox_mailer/Resources/Private/Language/locallang_db.xlf:tx_mooxmailer_domain_model_bounce.receiver_type',
			'config' => array(
				'type' => 'passthrough',
			),
		),
		'receiver_uid' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:moox_mailer/Resources/Private/Language/locallang_db.xlf:tx_mooxmailer_domain_model_bounce.receiver_uid',
			'config' => array(
				'type' => 'passthrough',
			),
		),
		'sent' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:moox_mailer/Resources/Private/Language/locallang_db.xlf:tx_mooxmailer_domain_model_bounce.sent',
			'config' => array(
				'type' => 'passthrough',
			),
		),
		'bounced' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:moox_mailer/Resources/Private/Language/locallang_db.xlf:tx_mooxmailer_domain_model_bounce.bounced',
			'config' => array(
				'type' => 'passthrough',
			),
		),				
	),
);
if(!\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::isLoaded('moox_address')){
	unset($return['columns']['address']);
}
return $return; 
?>