<?php
namespace Neulandlotsen\MooxMailer\Service;

/**
 * This file is part of the "moox_mailer" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

use Neulandlotsen\MooxMailer\Service\CronjobService;
use Neulandlotsen\MooxCore\Utility\Backend\HelperUtility;
use TYPO3\CMS\Core\SingletonInterface;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Messaging\FlashMessage;
use TYPO3\CMS\Extbase\Utility\LocalizationUtility;
use TYPO3\CMS\Core\Database\Connection;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Database\Query\Restriction\DeletedRestriction;
use TYPO3\CMS\Core\Database\Query\Restriction\HiddenRestriction;
 
class HelperService implements SingletonInterface
{
	/**
	 * @var array
	 */
	protected $extConf;

	/**
	 * @var int
	 */
	protected $storagePage;

	/**
	 * @var array
	 */
	protected $allowedFormatTypes = [
		'text',
		'date',
		'time',
		'datetime',
		'email',
		'url',
		'plain',
		'html',
		'currency',
		'default',
		'translate'
	];

	/**
	 * @var array
	 */
	protected $dateTimeFields = [
		"mod1" => [],
		"mod9" => [
			"tstamp",
			"starttime",
			"endtime",
			"crdate"
		]
	];

	/**
	 * @var array
	 */
	public $subjectDummyData = [
		'title' => 'Lorem ipsum dolor sit amet',
		'mooxNewsSeminarStartdate' => 1736985600,
		'mooxNewsSeminarEnddate' =>  1737072000,
		'datetime' =>  1736985600
	];

	/**
	 * @var array
	 */
	public $allowedMailerTasks = [
		'Neulandlotsen\MooxMailer\Tasks\MooxNewsSendTask'
	];

	/**
	 * @var array
	 */
	public $templateDummyData = [
		'gender' => 1,
		'title' => "Dr.",
		'name' => "Hans Mustermann",
		'firstName' => "Hans",
		'middleName' => "Jürgen",
		'lastName' => "Mustermann",
		'email' => "email@example.net",
		'isSingle' => 0,
		'templateUid' => 1,
		'redirectorPid' => 0,
		'redirectorType' => "javascript:void()",
		'news' => [
			1 => [
				'category' => "Kategorie 1",
				'baseUrl' => "https://www.neulandlotsen.de",
				'categoryImage' => [
					'originalResource' => [ 
						'originalFile' => [
							'name' => "previewCategory1.jpg",
							'identifier' => "/mailer/previewCategory1.jpg"
						]
					]
				],
				'items' => [
					1 => [
						'baseUrl' => '',
						'url' => 'javascript:void()',
						'data' => [
							'mooxNewsSeminarStartdate' => 1736985600,
							'mooxNewsSeminarEnddate' => 1737072000,
							'singlePid' => 0,
							'datetime' => 1736985600,
							'title' => 'Lorem ipsum dolor sit amet',
							'teaser' => 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.',
							'bodytext' => 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.',
							'mailerTitle' => 'Lorem ipsum dolor sit amet',
							'mailerTeaser' => 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.',
						],
						'fulltext' => 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.'
					],
					2 => [
						'baseUrl' => '',
						'url' => 'javascript:void()',
						'data' => [
							'mooxNewsSeminarStartdate' => 1736985600,
							'singlePid' => 0,
							'datetime' => 1736985600,
							'type' => 'moox_news_seminar',
							'title' => 'Seminar: Consetetur sadipscing elitr',
							'teaser' => 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.',
							'bodytext' => 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.',
							'mailerTitle' => 'Seminar: Consetetur sadipscing elitr',
							'mailerTeaser' => 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.',
						],
						'fulltext' => 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.'
					],
				]
			],
			2 => [
				'category' => "Kategorie 2",
				'baseUrl' => "https://www.neulandlotsen.de",
				'categoryImage' => [
					'originalResource' => [
						'originalFile' => [
							'name' => "previewCategory2.jpg",
							'identifier' => "/mailer/previewCategory2.jpg"
						]
					]
				],
				'items' => [
					1 => [
						'baseUrl' => '',
						'url' => 'javascript:void()',
						'data' => [
							'mooxNewsSeminarStartdate' => 1736985600,
							'mooxNewsSeminarEnddate' => 1737072000,
							'singlePid' => 0,
							'datetime' => 1736985600,
							'type' => 'moox_news_seminar',
							'title' => 'Seminar: Consetetur sadipscing elitr',
							'teaser' => 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.',
							'bodytext' => 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.',
							'mailerTitle' => 'Seminar: Consetetur sadipscing elitr',
							'mailerTeaser' => 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.',
						],
						'fulltext' => 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.'
					],
				]
			]
		],
	];

	/**
	 * @var array
	 */
	static public $weekdays = ["Sonntag", "Montag", "Dienstag", "Mittwoch", "Donnerstag", "Freitag", "Samstag"];

	/**
	 * Path to the locallang file
	 * @var string
	 */
	const LLPATH = 'LLL:EXT:moox_mailer/Resources/Private/Language/locallang.xlf:';
		
	/**
     *
     * @return void
     */
    public function initialize() 
	{										
		// get extensions's configuration
		$this->extConf = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Configuration\ExtensionConfiguration::class)->get('moox_mailer');
    }	
		
	/**
	 * set flash messages
	 *
	 * @param mixed &$parent
	 * @param array $messages
	 * @return void
	 */
	public function setFlashMessages(&$parent = NULL, $messages = array()) 
	{						
		if($parent){
		
			// set flash messages
			foreach($messages AS $message){
				if(!is_array($message)){
					$message = array();
				}
				if($message['text']==""){
					$message['text'] = "Unbekannter Fehler / Unknown error";
				}				
				if($message['icon']!="" && $message['title']!=""){
					$message['title'] = $message['icon'].$message['title'];
				}
				$parent->addFlashMessage($message['text'],($message['title']!="")?$message['title'].": ":"",$message['type'],true);				
			}
		}
	}
	
	/**
	 * check dynamic form fields
	 *
	 * @param array $fields fields
	 * @param array $item item
	 * @param array &$messages messages
	 * @param array &$errors errors
	 * @return void
	 */
	public function checkFields($fields = array(), $item = array(), &$messages, &$errors)
	{		
		// initialize
		$this->initialize();
				
		// check fields
		foreach($fields AS $field){
			
			$msgtitle = LocalizationUtility::translate(self::LLPATH.'form.'.$field['key'],$this->extensionName);
			// set fallback title
			if(!$msgtitle){
				$msgtitle = LocalizationUtility::translate(str_replace("moox_mailer",$field['extkey'],self::LLPATH).'form.'.$field['key'],$field['extkey']);
			}
			
			// check required fields only		
			if(!in_array($field['config']['type'],array("file")) && ($field['config']['required'] || isset($field['config']['maxlength']) || isset($field['config']['minlength']) || isset($field['config']['limit-low']) || isset($field['config']['limit-high']) || (!$field['config']['required'] && in_array($field['config']['validator'],array("email"))))){
				
				// check if field has a value
				if($field['config']['required'] && (trim($item[$field['key']])=="" || ($field['key']=="gender" && trim($item[$field['key']])==0))){
					
					// prepare message
					$message = LocalizationUtility::translate(self::LLPATH.'form.'.$field['key'].'.error.empty',$this->extensionName);
					
					// set fallback message
					if(!$message){
						$message = LocalizationUtility::translate(self::LLPATH.'form.error.empty',$this->extensionName);
					}
					
					// add message
					$messages[] = array( 
						"icon" => '<span class="glyphicon glyphicon-warning-sign icon-alert" aria-hidden="true"></span>',
						"title" => $msgtitle,
						"text" => $message,
						"type" => FlashMessage::ERROR
					);	

					// set error
					$errors[$field['key']] = true;	
				// check if field value smaller than maxlength
				} elseif(trim($item[$field['key']])!="" && isset($field['config']['maxlength']) && strlen(trim($item[$field['key']]))>$field['config']['maxlength']){
					
					// prepare message
					$message = LocalizationUtility::translate(self::LLPATH.'form.'.$field['key'].'.error.too_long',$this->extensionName,array($field['config']['maxlength']));
					
					// set fallback message
					if(!$message){
						$message = LocalizationUtility::translate(self::LLPATH.'form.error.too_long',$this->extensionName,array($field['config']['maxlength']));
					}
					
					// add message
					$messages[] = array( 
						"icon" => '<span class="glyphicon glyphicon-warning-sign icon-alert" aria-hidden="true"></span>',
						"title" => $msgtitle,
						"text" => $message,
						"type" => FlashMessage::ERROR
					);	

					// set error
					$errors[$field['key']] = true;
					
				// check if field value larger than minlength
				} elseif(trim($item[$field['key']])!="" && isset($field['config']['minlength']) && strlen(trim($item[$field['key']]))<$field['config']['minlength']){
					
					// prepare message
					$message = LocalizationUtility::translate(self::LLPATH.'form.'.$field['key'].'.error.too_short',$this->extensionName,array($field['config']['minlength']));
					
					// set fallback message
					if(!$message){
						$message = LocalizationUtility::translate(self::LLPATH.'form.error.too_short',$this->extensionName,array($field['config']['minlength']));
					}
					
					// add message
					$messages[] = array( 
						"icon" => '<span class="glyphicon glyphicon-warning-sign icon-alert" aria-hidden="true"></span>',
						"title" => $msgtitle,
						"text" => $message,
						"type" => FlashMessage::ERROR
					);	

					// set error
					$errors[$field['key']] = true;
				
				// check if field value greater than lowlimit
				} elseif(trim($item[$field['key']])!="" && isset($field['config']['limit-low']) && trim($item[$field['key']])<$field['config']['limit-low']){
					
					// prepare message
					$message = LocalizationUtility::translate(self::LLPATH.'form.'.$field['key'].'.error.too_small',$this->extensionName,array($field['config']['limit-low']));
					
					// set fallback message
					if(!$message){
						$message = LocalizationUtility::translate(self::LLPATH.'form.error.too_small',$this->extensionName,array($field['config']['limit-low']));
					}
					
					// add message
					$messages[] = array( 
						"icon" => '<span class="glyphicon glyphicon-warning-sign icon-alert" aria-hidden="true"></span>',
						"title" => $msgtitle,
						"text" => $message,
						"type" => FlashMessage::ERROR
					);	

					// set error
					$errors[$field['key']] = true;
				
				// check if field value smaller than highlimit
				} elseif(trim($item[$field['key']])!="" && isset($field['config']['limit-high']) && trim($item[$field['key']])>$field['config']['limit-high']){
					
					// prepare message
					$message = LocalizationUtility::translate(self::LLPATH.'form.'.$field['key'].'.error.too_large',$this->extensionName,array($field['config']['limit-high']));
					
					// set fallback message
					if(!$message){
						$message = LocalizationUtility::translate(self::LLPATH.'form.error.too_large',$this->extensionName,array($field['config']['limit-high']));
					}
					
					// add message
					$messages[] = array( 
						"icon" => '<span class="glyphicon glyphicon-warning-sign icon-alert" aria-hidden="true"></span>',
						"title" => $msgtitle,
						"text" => $message,
						"type" => FlashMessage::ERROR
					);	

					// set error
					$errors[$field['key']] = true;
				
				// check if email is valid
				} elseif(trim($item[$field['key']])!="" && $field['config']['validator']=="email" && !GeneralUtility::validEmail(trim($item[$field['key']]))){
					
					// prepare message
					$message = LocalizationUtility::translate(self::LLPATH.'form.'.$field['key'].'.error.invalid',$this->extensionName);
					
					// set fallback message
					if(!$message){
						$message = LocalizationUtility::translate(self::LLPATH.'form.error.invalid',$this->extensionName);
					}
					
					// add message
					$messages[] = array( 
						"icon" => '<span class="glyphicon glyphicon-warning-sign icon-alert" aria-hidden="true"></span>',
						"title" => $msgtitle,
						"text" => $message,
						"type" => FlashMessage::ERROR
					);	

					// set error
					$errors[$field['key']] = true;							
				} 			
			}
		}		
	}

	/**
	 * @param string $table
	 * @param string|array $fieldList
	 * @param array $excludeFields
	 * @param array $includeFields
	 * @param boolean $forceFieldList
	 * @return array list view fields
	 */
	public function getListViewFields($table = "tx_mooxmailer_domain_model_newsletter", $fieldList = [], $excludeFields = [], $includeFields = [], $forceFieldList = false)
	{
		global $BE_USER;

		$this->initialize();

		$listViewFields = [];

		if($table=='tx_mooxmailer_domain_model_template')
		{
			if(!in_array("title",$excludeFields))
			{
				$listViewFieldInfo = $GLOBALS['TCA'][$table]['columns']["title"];

				if($listViewFieldInfo['moox']['backend']['type']=="" || !in_array($listViewFieldInfo['moox']['backend']['type'],$this->allowedFormatTypes))
				{
					$listViewFieldInfo['moox']['backend']['type'] = 'default';
				}

				$listViewFields[0] = [
					"name" => "title",
					"length" => 10000,
					"label" => ($listViewFieldInfo['moox']['backend']['label'])?$listViewFieldInfo['moox']['backend']['label']:$listViewFieldInfo['label'],
					"extkey" => $listViewFieldInfo['moox']['extkey'],
				];
			}
		}

		return $listViewFields;
	}

	/**
	 * prepare mail subject
	 *
	 * @param string $subject subject
	 * @param mix $vars vars
	 * @return string $subject
	 */
	public function prepareMailSubject($subject,$data = NULL)
	{
		// TODO: Extender-Variablen über Extender einschleußen - 2019-08-02
		if(is_object($data))
		{
			$vars['mooxNewsSeminarStartdate'] = $data->getMooxNewsSeminarStartdate();
			$vars['mooxNewsSeminarEnddate'] = $data->getMooxNewsSeminarEnddate();
		}
		else
		{
			$vars = $data;
		}

		$subject = str_replace("#KW#",date("W"),$subject);
		$subject = str_replace("#YEAR#",date("Y"),$subject);
		$subject = str_replace("#MONTH#",date("m"),$subject);
		$subject = str_replace("#DAY#",date("d"),$subject);
		$subject = str_replace("#TODAY#",date("d.m.Y"),$subject);
		$subject = str_replace("#TITLE#",$vars['title'],$subject);
		$subject = str_replace("#DATE#",date('d.m.Y',(int)$vars['datetime']),$subject);
		$subject = str_replace("#DATETIME#",date('d.m.Y H:i',(int)$vars['datetime']),$subject);
		if($vars['mooxNewsSeminarStartdate']>0)
		{
			$subject = str_replace("#STARTDATE#",date("d.m.Y",(int)$vars['mooxNewsSeminarStartdate']),$subject);
			if($vars['mooxNewsSeminarEnddate']>0)
			{
				$subject = str_replace("#FULLDATE#",date("d.m.Y",(int)$vars['mooxNewsSeminarStartdate'])." - ".date("d.m.Y",(int)$vars['mooxNewsSeminarEnddate']),$subject);
			}
			else
			{
				$subject = str_replace("#FULLDATE#",date("d.m.Y",(int)$vars['mooxNewsSeminarStartdate']),$subject);
			}
		}

		return $subject;
	}

	/**
	 * get fields separator
	 *
	 * @param array $settings
	 * @return string $listViewFieldSeparator
	 */
	public function getFieldsSeparator($settings = [])
	{
		// get list view field separator
		if($settings['listViewFieldSeparator']!="")
		{
			$listViewFieldSeparator = $settings['listViewFieldSeparator'];
		}
		else
		{
			$listViewFieldSeparator = "&nbsp;|&nbsp;";
		}

		return $listViewFieldSeparator;
	}

	/**
	 * @return int
	 */
	public function getStoragePage()
	{
		return $this->storagePage;
	}

	/**
	 * @param int $storagePage
	 */
	public function setStoragePage($storagePage)
	{
		$this->storagePage = $storagePage;
	}

	/**
	 * @param bool $extended
	 */
	public function getMailerTasks()
	{
		$queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)
			->getQueryBuilderForTable('tx_scheduler_task');

		$queryBuilder
			->getRestrictions()
			->removeAll()
			->add(GeneralUtility::makeInstance(DeletedRestriction::class));

		$res = $queryBuilder
			->select('*')
			->from('tx_scheduler_task')
			->where(
				$queryBuilder->expr()->eq('deleted', $queryBuilder->createNamedParameter(0, \PDO::PARAM_INT))
			)
			->execute();

		$tasks = [];

		while($task = $res->fetch())
		{
			$mailerTask = unserialize($task['serialized_task_object']);

			if(in_array(get_class($mailerTask), $this->allowedMailerTasks))
			{
				$taskInfo = $this->getMailerTaskInfo('MooxNewsSendTask',$mailerTask);
				if(is_array($taskInfo) && count($taskInfo))
				{
					$tasks[] = array_merge($task,$taskInfo);
				}

			}
		}

		return $tasks;
	}

	/**
	 * @param int $uid
	 */
	public function getMailerTask($uid)
	{
		$task = [];

		if($uid)
		{
			$queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)
				->getQueryBuilderForTable('tx_scheduler_task');

			$queryBuilder
				->getRestrictions()
				->removeAll()
				->add(GeneralUtility::makeInstance(DeletedRestriction::class));

			$res = $queryBuilder
				->select('*')
				->from('tx_scheduler_task')
				->where(
					$queryBuilder->expr()->eq('uid', $queryBuilder->createNamedParameter($uid, \PDO::PARAM_INT)),
					$queryBuilder->expr()->eq('deleted', $queryBuilder->createNamedParameter(0, \PDO::PARAM_INT))
				)
				->execute();

			$loadedTask = $res->fetch();

			if($loadedTask['serialized_task_object'])
			{
				$mailerTask = unserialize($loadedTask['serialized_task_object']);

				if(in_array(get_class($mailerTask), $this->allowedMailerTasks))
				{
					$loadedTask = $this->getMailerTaskInfo('MooxNewsSendTask',$mailerTask, true);
					if(is_array($loadedTask) && count($loadedTask))
					{
						$task = $loadedTask;
					}
				}
			}
		}

		return $task;
	}

	/**
	 * @param string $type
	 * @param mixed $object
	 * @param bool $extended
	 */
	public function getMailerTaskInfo($type = "", $object = NULL, $extended = false)
	{
		$task = [];
		if($type && $object)
		{
			//$task['uid'] = $object->getTaskUid();
			//$task['disabled'] = $object->isDisabled();
			$task['description'] = $object->getDescription();
			$task['type'] = $type;
			
			$cronCmd = $object->getExecution()->getCronCmd();

			if($cronCmd!="")
			{
				$cron = CronjobService::factory($cronCmd);
				$cron->isDue();
				$nextRun = $cron->getNextRunDate()->format('U');
				$task['nextRun'] = self::$weekdays[date("w",$nextRun)].", ".date("d.m.Y H:i:s",$nextRun);
			}
			else
				{
				$executionTime = $object->getExecutionTime();
				if($executionTime>0)
				{
					$nextRun = $executionTime;
				}
				else
				{
					$nextRun = $object->getExecution()->getStart()+$object->getExecution()->getInterval();
				}
				$task['nextRun'] = self::$weekdays[date("w",$nextRun)].", ".date("d.m.Y H:i:s",$nextRun);
				$lastRun = $executionTime-$object->getExecution()->getInterval();
				$task['lastRun'] = self::$weekdays[date("w",$lastRun)].", ".date("d.m.Y H:i:s",$executionTime-$object->getExecution()->getInterval());
			}
			$task['nextRunCountdown'] = $this->getTimeDifference(time(),$nextRun);

			$lastAction = $this->getMailerTaskLastAction('task'.$task['taskUid'],'log');
			$task['lastRunStart'] = self::$weekdays[date("w",$lastRun)].", ".date("d.m.Y H:i:s",$lastAction['logstarttime']);
			if($lastAction['logendtime']!="running")
			{
				$task['lastRunEnd'] = self::$weekdays[date("w",$lastRun)].", ".date("d.m.Y H:i:s",$lastAction['logendtime']);
			}
			else
			{
				$task['lastRunEnd'] = "nocht nicht beendet";
			}
			$task['lastRunLogfile'] = $lastAction['logfile'];
			$cc = explode("\r\n",$object->getCc());
			if(count($cc)==1 && $cc[0]=="")
			{
				$cc = array();
			}
			$task['cc'] = $cc;
			$bcc = explode("\r\n",$object->getBcc());
			if(count($bcc)==1 && $bcc[0]=="")
			{
				$bcc = array();
			}
			$task['bcc'] = $bcc;
			$task['format']	= $object->getFormat();
			$task['senderAddress'] = $object->getSenderAddress();
			$task['senderName']	= $object->getSenderName();
			$task['subject'] = $object->getSubject();
			$task['mode'] = $object->getMode();
			$task['debug'] = $object->getDebug();
			$task['debugAddress'] = $object->getDebugAddress();
			$task['debugMailLimit'] = $object->getDebugMailLimit();
			$task['task'] = $object;
			
			if($type == "MooxNewsSendTask")
			{
				$objectManager = GeneralUtility::makeInstance('TYPO3\CMS\Extbase\Object\ObjectManager');
				$categoryRepository = $objectManager->get('Neulandlotsen\\MooxNews\\Domain\\Repository\\CategoryRepository');

				$mailer = $objectManager->get('Neulandlotsen\MooxMailer\Service\MooxNewsMailerService');
				$mailer->initialize($object);
				$news = $mailer->getNews();

				$task['news'] = $news;
				$task['newsCount'] = $news->count();
				$task['sendMode'] = $object->getSendMode();
				$task['includedCategories'] = $categoryRepository->findByIdList(explode(",",$object->getIncludedNewsCategoriesUids()));
				$task['excludedCategories'] = $categoryRepository->findByIdList(explode(",",$object->getExcludedNewsCategoriesUids()));
				$task['sendSingleMails'] = $object->getSendSingleMails();
				$task['forceSubject'] = $object->getForceSubject();
				$task['forceTemplateInSingleMailing'] = $object->getForceTemplateInSingleMailing();
				$task['forceScheduledTemplateInSingleMailing'] = $object->getForceScheduledTemplateInSingleMailing();
				$task['sendRestrictedNewsOnly']	= $object->getSendRestrictedNewsOnly();
				$task['sendToUsersWithGroupsOnly'] = $object->getSendToUsersWithGroupsOnly();
				$task['attachSelf'] = $object->getAttachSelf();
				$task['attachRelated'] = $object->getAttachRelated();
				$task['directNews'] = $object->getDirectNews();
				$task['newsLimit'] = $object->getNewsLimit();

				if($extended)
				{
					$templateRepository = $objectManager->get('Neulandlotsen\\MooxMailer\\Domain\\Repository\\TemplateRepository');
					$scheduleRepository = $objectManager->get('Neulandlotsen\\MooxMailer\\Domain\\Repository\\ScheduleRepository');
					$frontendUserGroupRepository = $objectManager->get('Neulandlotsen\\MooxMailer\\Domain\\Repository\\FrontendUserGroupRepository');

					$task['template'] = ($object->getTemplateUid())?$templateRepository->findByUid($object->getTemplateUid()):'';
					$task['schedule'] = ($object->getScheduleUid())?$scheduleRepository->findByUid($object->getScheduleUid()):'';
					$task['newsFolders'] = ($object->getNewsPids()!="")?HelperUtility::getFolders("mxnews",[1,254],false,explode(",",$object->getNewsPids())):[];
					$task['userFolders'] = ($object->getUserPids()!="")?HelperUtility::getFolders("mxfeuser",[1,254],false,explode(",",$object->getUserPids())):[];
					$task['addressFolders'] = ($object->getAddressesPids()!="")?HelperUtility::getFolders("mxaddress",[1,254],false,explode(",",$object->getAddressesPids())):[];
					$task['excludedUserGroups']	= ($object->getExcludedUserGroups()!="")?$frontendUserGroupRepository->findByIdList(explode(",",$object->getAddressesPids())):[];
				}
			}
			
			$tasks[] = $task;
		}
		return $task;
	}

	/**
	 * get time diff between two timestamps
	 *
	 * @param int $first
	 * @param int $second
	 * @return array	timediff
	 */
	public function getTimeDifference($first, $second)
	{
		if($first > $second){
			$td['dif'] = $first - $second;
		} else {
			$td['dif'] = $second - $first;
		}

		$td['sec'] = $td['dif'] % 60; // 67 = 7

		$td['min'] = (($td['dif'] - $td['sec']) / 60) % 60;

		$td['std'] = (((($td['dif'] - $td['sec']) /60)-
					$td['min']) / 60) % 24;

		$td['day'] = floor( ((((($td['dif'] - $td['sec']) /60)-
					$td['min']) / 60) / 24) );

		return $td;

	}

	/**
	 * get contents from url
	 * @param string $url
	 * @return array $header
	 */
	public function getContentsFromUrl($url)
	{
		$extConf = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Configuration\ExtensionConfiguration::class)->get('moox_mailer');
		$options =[
			CURLOPT_RETURNTRANSFER => true,     // return web page
			CURLOPT_HEADER         => false,    // don't return headers
			CURLOPT_FOLLOWLOCATION => true,     // follow redirects
			CURLOPT_ENCODING       => "",       // handle all encodings
			CURLOPT_USERAGENT      => "typo3_moox_mailer", // who am i
			CURLOPT_AUTOREFERER    => true,     // set referer on redirect
			CURLOPT_CONNECTTIMEOUT => 120,      // timeout on connect
			CURLOPT_TIMEOUT        => 120,      // timeout on response
			CURLOPT_MAXREDIRS      => 10,       // stop after 10 redirects
			CURLOPT_COOKIESESSION  => true,
		];
		if($extConf['curlCredentials']!="")
		{
			$options[CURLOPT_USERPWD] = $extConf['curlCredentials'];
		}

		$ch = curl_init( $url );
		@curl_setopt_array( $ch, $options );
		$content = curl_exec( $ch );
		$err = curl_errno( $ch );
		$errmsg = curl_error( $ch );
		$header = curl_getinfo( $ch );
		curl_close( $ch );

		$header['errno'] = $err;
		$header['errmsg'] = $errmsg;
		$header['content'] = trim($content);

		if($header['http_code']=="303"){
			if(strpos($header['content'],'meta http-equiv="refresh"')){
				$html = json_decode(json_encode(simplexml_load_string($header['content'])),TRUE);
				foreach($html['head']['meta'] AS $meta){
					if($meta['http-equiv']=="refresh"){
						$redirect = substr($meta['content'],((int)strpos($meta['content'],"url=")+4));
						break;
					}
				}
			}
			if($redirect!=""){
				$header = $this->getContentsFromUrl($redirect);
			}
		}

		return $header;
	}

	/**
	* @param int $taskUid
	* @param string $what
	* @return	array	timestamp or log
	*/
	private function getMailerTaskLastAction($uid,$what = "timestamp")
	{
		$logfolder = $GLOBALS["_SERVER"]["DOCUMENT_ROOT"]."/maildir/logs/";

		$handle = opendir ($logfolder);

		$logfile = "";
		$logtime = 0;

		while ($log = readdir ($handle))
		{
			if(strpos($log,$uid.".log")>0)
			{
				if(strpos($log,"__")===0)
				{
					$running = false;
					$datetime['year'] 	= substr($log,2,4);
					$datetime['month'] 	= substr($log,6,2);
					$datetime['day'] 	= substr($log,8,2);
					$datetime['hour'] 	= substr($log,10,2);
					$datetime['minute'] = substr($log,12,2);
					$datetime['second'] = substr($log,14,2);
				} else {
					$running = true;
					$datetime['year'] 	= substr($log,0,4);
					$datetime['month'] 	= substr($log,4,2);
					$datetime['day'] 	= substr($log,6,2);
					$datetime['hour'] 	= substr($log,8,2);
					$datetime['minute'] = substr($log,10,2);
					$datetime['second'] = substr($log,12,2);
				}

				$curtime = mktime(
					$datetime['hour'],
					$datetime['minute'],
					$datetime['second'],
					$datetime['month'],
					$datetime['day'],
					$datetime['year']
				);

				if($curtime>$logtime)
				{
					$isrunning = $running;
					$logfile = $log;
					$logtime = $curtime;
				}
			}
		}

		$return['logfile'] = $logfile;

		if($logfile!="")
		{
			if($isrunning)
			{
				$return['logendtime'] = "running";
			}
			else
			{
				$return['logendtime'] = filemtime($logfolder.$logfile);
			}
			$return['logstarttime'] = $logtime;
		}

		if($logfile!="" && $what=="log")
		{
			$return['logcontent'] = file_get_contents($logfolder.$logfile);
		}

		closedir($handle);

		return $return;
	}
}
?>