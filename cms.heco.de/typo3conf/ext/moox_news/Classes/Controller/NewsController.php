<?php
namespace Neulandlotsen\MooxNews\Controller;

/**
 * This file is part of the "moox_news" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

use Neulandlotsen\MooxCore\Utility\HelperUtility AS CoreHelperUtility;
use Neulandlotsen\MooxCore\Utility\PdfGenerator;
use Neulandlotsen\MooxNews\Utility\EmConfiguration;
use Neulandlotsen\MooxNews\Utility\Cache;
use Neulandlotsen\MooxNews\Utility\Page;
use Neulandlotsen\MooxNews\Utility\TypoScript;
use Neulandlotsen\MooxNews\Domain\Repository\NewsRepository;
use Neulandlotsen\MooxNews\Domain\Repository\CategoryRepository;
use Neulandlotsen\MooxNews\Domain\Repository\TagRepository;
use Neulandlotsen\MooxNews\Domain\Repository\TargetRepository;
use Neulandlotsen\MooxNews\Service\HelperService;
use Neulandlotsen\MooxNews\Service\IcsService;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Service\TypoScriptService;
use TYPO3\CMS\Extbase\Utility\LocalizationUtility;

/**
 * Controller of news records
 *
 */
class NewsController extends NewsBaseController
{
	const SIGNAL_NEWS_LIST_ACTION = 'listAction';
	const SIGNAL_NEWS_CALENDAR_ACTION = 'calendarAction';
	const SIGNAL_NEWS_DETAIL_ACTION = 'detailAction';
	const SIGNAL_NEWS_DATEMENU_ACTION = 'dateMenuAction';
	const SIGNAL_NEWS_SEARCHFORM_ACTION = 'searchFormAction';
	const SIGNAL_NEWS_SEARCHRESULT_ACTION = 'searchResultAction';

	/**
	 * @var NewsRepository
	 */
	protected $newsRepository;

	/**
	 * @var CategoryRepository
	 */
	protected $categoryRepository;

	/**
	 * @var TagRepository
	 */
	protected $tagRepository;

	/**
	 * @var TargetRepository
	 */
	protected $targetRepository;

	/**
	 * @var HelperService
	 */
	protected $helperService;

	/**
	 * @var PdfGenerator
	 */
	protected $pdfGenerator;

	/**
	 * @var IcsService
	 */
	protected $icsService;

	/**
	 * @var \TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface
	 */
	protected $configurationManager;

	/** @var array */
	protected $ignoredSettingsForOverride = ['demandclass', 'orderbyallowed'];

	/**
	 * @var bool
	 */
	public $isAjaxRequest;

	/**
	 * @var array
	 */
	public $charList;

	/**
	 * @var bool
	 */
	protected $extConf;

	/**
	 * @param NewsRepository $newsRepository
	 */
	public function injectNewsRepository(NewsRepository $newsRepository)
	{
		$this->newsRepository = $newsRepository;
	}

	/**
	 * @param CategoryRepository $categoryRepository
	 */
	public function injectCategoryRepository(CategoryRepository $categoryRepository)
	{
		$this->categoryRepository = $categoryRepository;
	}

	/**
	 * @param TagRepository $tagRepository
	 */
	public function injectTagRepository(TagRepository $tagRepository)
	{
		$this->tagRepository = $tagRepository;
	}

	/**
	 * @param TargetRepository $targetRepository
	 */
	public function injectTargetRepository(TargetRepository $targetRepository)
	{
		$this->targetRepository = $targetRepository;
	}

	/**
	 * @param HelperService $helperService
	 */
	public function injectHelperService(HelperService $helperService)
	{
		$this->helperService = $helperService;
	}

	/**
	 * @param PdfGenerator $pdfGenerator
	 */
	public function injectPdfGenerator(PdfGenerator $pdfGenerator)
	{
		$this->pdfGenerator = $pdfGenerator;
	}

	/**
	 * @param IcsService $icsService
	 */
	public function injectIcsService(IcsService $icsService)
	{
		$this->icsService = $icsService;
	}

	/**
	 * Initializes the current action
	 *
	 */
	public function initializeAction()
	{
		if (isset($this->settings['format'])) {
			$this->request->setFormat($this->settings['format']);
		}
		// Only do this in Frontend Context
		if (!empty($GLOBALS['TSFE']) && is_object($GLOBALS['TSFE']))
		{
			// We only want to set the tag once in one request, so we have to cache that statically if it has been done
			static $cacheTagsSet = false;

			/** @var $typoScriptFrontendController \TYPO3\CMS\Frontend\Controller\TypoScriptFrontendController */
			$typoScriptFrontendController = $GLOBALS['TSFE'];
			if (!$cacheTagsSet)
			{
				$typoScriptFrontendController->addCacheTags(['tx_mooxnews']);
				$cacheTagsSet = true;
			}
		}
		foreach (range('A', 'Z') as $char)
		{
			$this->charList[$char] = $char;
		}
		$this->charList['other'] = "Andere";

		// set ext conf
		$this->extConf = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Configuration\ExtensionConfiguration::class)->get('moox_news');
	}

	/**
	 * Create the demand object which define which records will get shown
	 *
	 * @param array $settings
	 * @param string $class optional class which must be an instance of \Neulandlotsen\MooxNews\Domain\Model\Dto\NewsDemand
	 * @return \Neulandlotsen\MooxNews\Domain\Model\Dto\NewsDemand
	 */
	public function createDemandObjectFromSettings(
		$settings,
		$class = 'Neulandlotsen\\MooxNews\\Domain\\Model\\Dto\\NewsDemand'
	) {
		$class = isset($settings['demandClass']) && !empty($settings['demandClass']) ? $settings['demandClass'] : $class;

		/* @var $demand \Neulandlotsen\MooxNews\Domain\Model\Dto\NewsDemand */
		$demand = $this->objectManager->get($class, $settings);
		if (!$demand instanceof \Neulandlotsen\MooxNews\Domain\Model\Dto\NewsDemand)
		{
			throw new \UnexpectedValueException(
				sprintf('The demand object must be an instance of \Neulandlotsen\\MooxNews\\Domain\\Model\\Dto\\NewsDemand, but %s given!',
					$class),
				1423157953);
		}

		$demand->setCategories(GeneralUtility::trimExplode(',', $settings['categories'], true));
		$demand->setCategoryConjunction($settings['categoryConjunction']);
		$demand->setIncludeSubCategories($settings['includeSubCategories']);
		if(is_string($settings['tags']) && $settings['tags']!="")
		{
			$settings['tags'] = explode(",",$settings['tags']);
		}
		$demand->setTags($settings['tags']);
		if(is_string($settings['targets']) && $settings['targets']!="")
		{
			$settings['targets'] = explode(",",$settings['targets']);
		}
		$demand->setTargets($settings['targets']);
		//ToDo: über mit moox_news_frontend einschleußen
		$demand->setPrivacy($settings['privacy']);
		$demand->setTopNewsRestriction($settings['topNewsRestriction']);
		//$demand->setTimeRestriction($settings['timeRestriction']);
		//$demand->setTimeRestrictionHigh($settings['timeRestrictionHigh']);
		if($settings['dateYear'])
		{
			if(in_array($settings['dateMode'],['eq','gte']))
			{
				if($settings['dateMonth'])
				{
					$dateFrom = mktime(0,0,0,$settings['dateMonth'],1,$settings['dateYear']);
				}
				else
				{
					$dateFrom = mktime(0,0,0,1,1,$settings['dateYear']);
				}
				$demand->setDateFrom($dateFrom);
			}
			if(in_array($settings['dateMode'],['eq','lte']))
			{
				if($settings['dateMonth'])
				{
					$dateTo = mktime(0,0,0,$settings['dateMonth']+1,-1,$settings['dateYear']);
				}
				else
				{
					$dateTo = mktime(0,0,0,1,-1,$settings['dateYear']+1);
				}
				$demand->setDateTo($dateTo);
			}
		}

		$demand->setArchiveRestriction($settings['archiveRestriction']);
		$demand->setExcludeAlreadyDisplayedNews($settings['excludeAlreadyDisplayedNews']);
		$demand->setIncludeOnlySelectedNewsInRss($settings['list']['rss']['showSelectedOnly']);
		$demand->setHideIdList($settings['hideIdList']);

		if ($settings['orderBy']) {
			$demand->setOrder($settings['orderBy'] . ' ' . $settings['orderDirection']);
		}
		$demand->setOrderByAllowed($settings['orderByAllowed']);

		$demand->setTopNewsFirst($settings['topNewsFirst']);

		$demand->setLimit($settings['limit']);
		$demand->setOffset($settings['offset']);

		$demand->setSearchFields($settings['search']['fields']);
		$demand->setDateField($settings['dateField']);
		$demand->setMonth($settings['month']);
		$demand->setYear($settings['year']);

		if($settings['isRss'] && $settings['list']['rss']['excludeSelected'])
		{
			$demand->setExcludeFromRss(1);
		}

		if(!is_array($settings['types']) && $settings['types']!="")
		{
			$settings['types'] = explode(",",$settings['types']);
		}

		$demand->setTypes($settings['types']);
		if(is_array($settings['types']) && count($settings['types'])==1)
		{
			$demand->setType($settings['types'][0]);
		}

		// TODO: needed for multi language support???
		/*
		if(is_numeric($settings['language'])){
			$demand->setSysLanguageUid($settings['language']);
		} elseif($settings['language']=="page" && is_numeric($GLOBALS['TSFE']->sys_language_uid)){
			$demand->setSysLanguageUid($GLOBALS['TSFE']->sys_language_uid);
		} 
		*/

		// set default values for extended filter fields from external extension
		if(is_array($GLOBALS['TCA']['tx_mooxnews_domain_model_news']['moox']['additionalFrontendDemandFields']))
		{
			foreach($GLOBALS['TCA']['tx_mooxnews_domain_model_news']['moox']['additionalFrontendDemandFields'] AS $fieldname)
			{
				if(isset($GLOBALS['TCA']['tx_mooxnews_domain_model_news']['columns'][$fieldname]))
				{
					$field = $GLOBALS['TCA']['tx_mooxnews_domain_model_news']['columns'][$fieldname];
					$fieldname = GeneralUtility::underscoredToLowerCamelCase($fieldname);
					if(isset($settings[$fieldname]) && $settings[$fieldname]!="")
					{
						$setCall = "set".ucfirst($fieldname);
						if(method_exists($demand, $setCall))
						{
							$demand->$setCall($settings[$fieldname]);
						}
					}
				}
			}
		}

		$demand->setStoragePage(Page::extendPidListByChildren(
			$settings['startingpoint'],
			$settings['recursive'])
		);

		$demand->setCharList($this->charList);

		return $demand;
	}

	/**
	 * Overwrites a given demand object by an propertyName =>  $propertyValue array
	 *
	 * @param \Neulandlotsen\MooxNews\Domain\Model\Dto\NewsDemand $demand
	 * @param array $overwriteDemand
	 * @return \Neulandlotsen\MooxNews\Domain\Model\Dto\NewsDemand
	 */
	public function overwriteDemandObject($demand, $overwriteDemand)
	{
		foreach ($this->ignoredSettingsForOverride as $property)
		{
			unset($overwriteDemand[$property]);
		}

		foreach ($overwriteDemand as $propertyName => $propertyValue)
		{
			if (in_array(strtolower($propertyName), $this->ignoredSettingsForOverride, true))
			{
				continue;
			}
			if ($propertyValue !== '' || $this->settings['allowEmptyStringsForOverwriteDemand'])
			{
				\TYPO3\CMS\Extbase\Reflection\ObjectAccess::setProperty($demand, $propertyName, $propertyValue);
			}
		}
		return $demand;
	}

	/**
	 * Output a list view of news
	 *
	 * @param array $overwriteDemand
	 */
	public function listAction(array $overwriteDemand = null)
	{
		// Extension manager configuration
		$configuration 	= EmConfiguration::getSettings();

		$demand = $this->createDemandObjectFromSettings($this->settings);
		$demand->setActionAndClass(__METHOD__, __CLASS__);

		// get extended filter
		$extendedFilters = [];
		if(!$this->settings['hideAllFilter'])
		{
			$extendedFilters = $this->getExtendedFilters($demand,$overwriteDemand);
		}

		if($overwriteDemand['reset'])
		{
			$overwriteDemand = null;
		}

		if ($this->settings['disableOverrideDemand'] != 1 && $overwriteDemand !== null)
		{

			if($overwriteDemand['dateFrom'] || $overwriteDemand['dateTo'])
			{
				$overwriteDemand['year'] = 'duration';
			}
			elseif(!$overwriteDemand['dateFrom'] && !$overwriteDemand['dateTo'])
			{
				unset($overwriteDemand['dateFrom']);
				unset($overwriteDemand['dateTo']);
				if($overwriteDemand['year']=="duration")
				{
					unset($overwriteDemand['year']);
				}
			}
			if(!isset($overwriteDemand['categories']) || !$overwriteDemand['categories'] || $overwriteDemand['categories']=='all')
			{
				unset($overwriteDemand['categoryConjunction']);
				unset($overwriteDemand['includeSubCategories']);
			}
			$resetFields = ["type","types","categories","target","targets","years","year","tag","tags","char","query"];
			foreach($resetFields AS $resetField)
			{
				if(isset($overwriteDemand[$resetField]) && ($overwriteDemand[$resetField]=="all" || $overwriteDemand[$resetField]==""))
				{
					unset($overwriteDemand[$resetField]);
				}
			}
			foreach($extendedFilters AS $resetField)
			{
				if($resetField['type']=="select" && isset($overwriteDemand[$resetField['name']]) && $overwriteDemand[$resetField['name']]=="all")
				{
					unset($overwriteDemand[$resetField['name']]);
				}
			}
			// set default values for extended filter fields from external extension
			if(is_array($GLOBALS['TCA']['tx_mooxnews_domain_model_news']['moox']['additionalFrontendDemandFields'])) {
				foreach ($GLOBALS['TCA']['tx_mooxnews_domain_model_news']['moox']['additionalFrontendDemandFields'] AS $fieldname) {
					if (isset($GLOBALS['TCA']['tx_mooxnews_domain_model_news']['columns'][$fieldname])) {
						$field = $GLOBALS['TCA']['tx_mooxnews_domain_model_news']['columns'][$fieldname];
						$fieldname = GeneralUtility::underscoredToLowerCamelCase($fieldname);
						if (isset($overwriteDemand[$fieldname]) && $overwriteDemand[$fieldname] == "all") {
							unset($overwriteDemand[$fieldname]);
						}
					}
				}
			}
			$demand = $this->overwriteDemandObject($demand, $overwriteDemand);
		}

		if(!$demand->getCategories() && !$overwriteDemand['categories'] && $this->settings['categories'])
		{
			$demand->setCategories($this->settings['categories']);
		}

		$newsRecords = $this->newsRepository->findDemanded($demand);

		if ($this->settings['list']['autoLoadFirstItemDetailView'])
		{
			$firstNewsItem = $newsRecords->getFirst();
			if(is_object($firstNewsItem))
			{
				$this->redirect('detail',NULL,NULL,['news'=>$firstNewsItem->getUid()]);
			}
		}

		if($this->isAjaxRequest)
		{
			$allDemand = $demand;
			$allDemand->setLimit(NULL);
			$allDemand->setOffset(NULL);
			$count = $this->newsRepository->findDemanded($allDemand)->count();
			$pages = ceil($count/$this->settings['list']['paginate']['itemsPerPage']);
		}
		else
		{
			$count = $newsRecords->count();
			$pages = ceil($count/$this->settings['list']['paginate']['itemsPerPage']);
		}

		if($GLOBALS['TSFE']->id==810 && false){
			header("Content-Type: text/html; charset=utf-8");
			//echo '"UID/Code";"Ab wann ist Seminar sichtbar (leer = sofort)";"Bis wann ist Seminar sichtbar (leer = immer)";"Titel des Seminars";"Kurzbeschreibung";"Langbeschreibung";"Autor";"Email-Adresse des Autors";"Kategorie(n)";"Zielgruppen";"Vom RSS-Feed auschließen";"Export zu biwecon";"Bei Twitter veröffentlichen";"Text für Twitter";"Startdatum";"Enddatum";"Preis";"Kosten agvChemie";"Kosten VCI";"Kosten Unterkunft/Sonstiges";"Externe Abrechnung";"Zusätzliche Preisinformationen";"Mindest-Teilnehmerzahl";"Maximalen Teilnehmerzahl";"Anmeldung nur bis zu diesem Datum möglich";"Uhrzeit 1";"Uhrzeit 2";"Uhrzeit 3";"Inhalt-Strich 1";"Inhalt-Strich 2";"Inhalt-Strich 3";"Inhalt-Strich 4";"Inhalt-Strich 5";"Inhalt-Strich 6";"Referent 1";"Referent 2";"Referent 3";"Nur noch begrenzt Plätze verfügbar";"Veranstalter";"Veranstaltungsort";"Veranstaltungsort-Straße/Hausnummer";"Veranstaltungsort-PLZ";"Veranstaltungsort-Stadt/Ort";"Veranstaltungsort-Land";"Veranstaltungsort-URL";"Kommentare erlaubt?"';echo "\r\n";
			echo "'UID/Code';'Ab wann ist Seminar sichtbar (leer = sofort)';'Bis wann ist Seminar sichtbar (leer = immer)';'Titel des Seminars';'Kurzbeschreibung';'Langbeschreibung';'Autor';'Email-Adresse des Autors';'Kategorie(n)';'Zielgruppen';'Vom RSS-Feed auschließen';'Export zu biwecon';'Bei Twitter veröffentlichen';'Text für Twitter';'Startdatum';'Enddatum';'Preis';'Kosten agvChemie';'Kosten VCI';'Kosten Unterkunft/Sonstiges';'Externe Abrechnung';'Zusätzliche Preisinformationen';'Mindest-Teilnehmerzahl';'Maximalen Teilnehmerzahl';'Anmeldung nur bis zu diesem Datum möglich';'Uhrzeit 1';'Uhrzeit 2';'Uhrzeit 3';'Inhalt-Strich 1';'Inhalt-Strich 2';'Inhalt-Strich 3';'Inhalt-Strich 4';'Inhalt-Strich 5';'Inhalt-Strich 6';'Referent 1';'Referent 2';'Referent 3';'Nur noch begrenzt Plätze verfügbar';'Veranstalter';'Veranstaltungsort';'Veranstaltungsort-Straße/Hausnummer';'Veranstaltungsort-PLZ';'Veranstaltungsort-Stadt/Ort';'Veranstaltungsort-Land';'Veranstaltungsort-URL';'Kommentare erlaubt?'";echo "\r\n";
			foreach($newsRecords AS $newsRecord){
				if($newsRecord->getYear()==2018){
					$title = $this->csvTrim($newsRecord->getTitle());
					$teaser = $this->csvTrim($newsRecord->getTeaser());
					$bodytext = $this->csvTrim2($newsRecord->getBodytext());
					$bodytext = str_replace("<b></b>","",$bodytext);
					$twittertext = $this->csvTrim($newsRecord->getPushToTwitterText());
					$delimiter = "'";
					echo $delimiter.$newsRecord->getUid().$delimiter.';';
					echo $delimiter.(($newsRecord->getStarttime()>0)?date("d.m.Y",$newsRecord->getStarttime()->getTimestamp()):"").$delimiter.';';
					echo $delimiter.(($newsRecord->getEndtime()>0)?date("d.m.Y",$newsRecord->getEndtime()->getTimestamp()):"").$delimiter.';';
					echo $delimiter.$title.$delimiter.';';
					echo $delimiter.$teaser.$delimiter.';';
					echo $delimiter.$bodytext.$delimiter.';';
					echo $delimiter.$newsRecord->getAuthor().$delimiter.';';
					echo $delimiter.$newsRecord->getAuthorEmail().$delimiter.';';
					$cats = [];
					foreach($newsRecord->getCategories() AS $cat){
						$cats[] = $cat->getUid();
					}
					echo $delimiter.implode(",",$cats).$delimiter.';';
					$targs = [];
					foreach($newsRecord->getTargets() AS $targ){
						$targs[] = $targ->getUid();
					}
					echo $delimiter.implode(",",$targs).$delimiter.';';
					echo $delimiter.$newsRecord->getExcludeFromRss().$delimiter.';';
					echo $delimiter.$newsRecord->getMooxNewsSeminarBiwecon().$delimiter.';';
					echo $delimiter.$newsRecord->getPushToTwitter().$delimiter.';';
					echo $delimiter.$twittertext.$delimiter.';';
					echo $delimiter.(($newsRecord->getMooxNewsSeminarStartdate()>0)?date("d.m.Y",$newsRecord->getMooxNewsSeminarStartdate()):"").$delimiter.';';
					echo $delimiter.(($newsRecord->getMooxNewsSeminarEnddate()>0)?date("d.m.Y",$newsRecord->getMooxNewsSeminarEnddate()):"").$delimiter.';';
					echo $delimiter.$newsRecord->getMooxNewsSeminarPrice().$delimiter.';';
					echo $delimiter.$newsRecord->getMooxNewsSeminarPrice2().$delimiter.';';
					echo $delimiter.$newsRecord->getMooxNewsSeminarPrice3().$delimiter.';';
					echo $delimiter.$newsRecord->getMooxNewsSeminarPrice4().$delimiter.';';
					echo $delimiter.$newsRecord->getMooxNewsSeminarExternalInvoice().$delimiter.';';
					echo $delimiter.$this->csvTrim($newsRecord->getMooxNewsSeminarPriceInfo()).$delimiter.';';
					echo $delimiter.$newsRecord->getMooxNewsSeminarSubscriberMin().$delimiter.';';
					echo $delimiter.$newsRecord->getMooxNewsSeminarSubscriberMax().$delimiter.';';
					echo $delimiter.(($newsRecord->getMooxNewsSeminarRegisterDeadline()>0)?date("d.m.Y",$newsRecord->getMooxNewsSeminarRegisterDeadline()):"").$delimiter.';';
					echo $delimiter.$newsRecord->getMooxNewsSeminarTime1().$delimiter.';';
					echo $delimiter.$newsRecord->getMooxNewsSeminarTime2().$delimiter.';';
					echo $delimiter.$newsRecord->getMooxNewsSeminarTime3().$delimiter.';';
					echo $delimiter.$this->csvTrim($newsRecord->getMooxNewsSeminarListpoint1()).$delimiter.';';
					echo $delimiter.$this->csvTrim($newsRecord->getMooxNewsSeminarListpoint2()).$delimiter.';';
					echo $delimiter.$this->csvTrim($newsRecord->getMooxNewsSeminarListpoint3()).$delimiter.';';
					echo $delimiter.$this->csvTrim($newsRecord->getMooxNewsSeminarListpoint4()).$delimiter.';';
					echo $delimiter.$this->csvTrim($newsRecord->getMooxNewsSeminarListpoint5()).$delimiter.';';
					echo $delimiter.$this->csvTrim($newsRecord->getMooxNewsSeminarListpoint6()).$delimiter.';';
					echo $delimiter.$newsRecord->getMooxNewsSeminarSpeaker1().$delimiter.';';
					echo $delimiter.$newsRecord->getMooxNewsSeminarSpeaker2().$delimiter.';';
					echo $delimiter.$newsRecord->getMooxNewsSeminarSpeaker3().$delimiter.';';
					echo $delimiter.$newsRecord->getMooxNewsSeminarLimited().$delimiter.';';
					echo $delimiter.$newsRecord->getMooxNewsSeminarOrganizer().$delimiter.';';
					echo $delimiter.$newsRecord->getMooxNewsGeoinfoVenue().$delimiter.';';
					echo $delimiter.$newsRecord->getMooxNewsGeoinfoStreet().$delimiter.';';
					echo $delimiter.$newsRecord->getMooxNewsGeoinfoZip().$delimiter.';';
					echo $delimiter.$newsRecord->getMooxNewsGeoinfoCity().$delimiter.';';
					echo $delimiter.$newsRecord->getMooxNewsGeoinfoCountry().$delimiter.';';
					echo $delimiter.$newsRecord->getMooxNewsGeoinfoUrl().$delimiter.';';
					echo $delimiter.$newsRecord->getCommentActive().$delimiter.';';
					echo "\n";
					//break;
				}
			}
			exit();
		}

		$assignedValues = [
			'news' => $newsRecords,
			'count' => $count,
			'pages' => $pages,
			'overwriteDemand' => $overwriteDemand,
			'demand' => $demand,
			'charList' => $this->charList,
			'extendedFilters' => $extendedFilters,
			'extendedFields' => $this->getExtendedFields("list"),
			'returnUrl' => $this->uriBuilder->getRequest()->getRequestUri()

		];
		if($this->isAjaxRequest)
		{
			$assignedValues['settings'] = $this->settings;
		}

		// TODO: check if needed
		/*	
        if ($demand->getCategories() !== '') {
            $categoriesList = $demand->getCategories();
            if (!is_array($categoriesList)) {
                $categoriesList = GeneralUtility::trimExplode(',', $categoriesList);
            }
            if (!empty($categoriesList)) {
                $assignedValues['categories'] = $this->categoryRepository->findByIdList($categoriesList);
            }
        }

        if ($demand->getTags() !== '') {
            $tagList = $demand->getTags();
            if (!is_array($tagList)) {
                $tagList = GeneralUtility::trimExplode(',', $tagList);
            }
            if (!empty($tagList)) {
                $assignedValues['tags'] = $this->tagRepository->findByIdList($tagList);
            }
        }
		*/

		$assignedValues = $this->emitActionSignal('NewsController', self::SIGNAL_NEWS_LIST_ACTION, $assignedValues);
		$this->view->assignMultiple($assignedValues);

		Cache::addPageCacheTagsByDemandObject($demand);
	}

	/**
	 * Output a calendar view of news
	 *
	 * @param int $pointer
	 * @param array $overwriteDemand
	 */
	public function calendarAction($pointer = NULL, array $overwriteDemand = null)
	{
		setlocale (LC_ALL, 'de_DE');

		if(!$pointer)
		{
			$pointer = date("Ym");
		}

		$calendar = [];

		// Extension manager configuration
		$configuration 	= EmConfiguration::getSettings();

		$demand = $this->createDemandObjectFromSettings($this->settings);
		$demand->setActionAndClass(__METHOD__, __CLASS__);

		if(strlen($pointer)==4)
		{
			$calendarYear = $pointer;
		}
		else
		{
			$calendarYear = substr($pointer,0,4);
			$calendarMonth = substr($pointer,4);
		}

		$prev = [
			'pointer' => date("Ym",mktime(0,0,0,$calendarMonth-1,1,$calendarYear)),
			'name' => strftime("%B %Y",mktime(0,0,0,$calendarMonth-1,1,$calendarYear)),
			'month' => date("m",mktime(0,0,0,$calendarMonth-1,1,$calendarYear)),
			'monthname' => strftime("%B",mktime(0,0,0,$calendarMonth-1,1,$calendarYear)),
			'year' => date("Y",mktime(0,0,0,$calendarMonth-1,1,$calendarYear))
		];
		$current = [
			'pointer' => date("Ym",mktime(0,0,0,$calendarMonth,1,$calendarYear)),
			'name' => strftime("%B %Y",mktime(0,0,0,$calendarMonth,1,$calendarYear)),
			'month' => date("m",mktime(0,0,0,$calendarMonth,1,$calendarYear)),
			'monthname' => strftime("%B",mktime(0,0,0,$calendarMonth,1,$calendarYear)),
			'year' => date("Y",mktime(0,0,0,$calendarMonth,1,$calendarYear))
		];
		$next = [
			'pointer' => date("Ym",mktime(0,0,0,$calendarMonth+1,1,$calendarYear)),
			'name' => strftime("%B %Y",mktime(0,0,0,$calendarMonth+1,1,$calendarYear)),
			'month' => date("m",mktime(0,0,0,$calendarMonth+1,1,$calendarYear)),
			'monthname' => strftime("%B",mktime(0,0,0,$calendarMonth+1,1,$calendarYear)),
			'year' => date("Y",mktime(0,0,0,$calendarMonth+1,1,$calendarYear))
		];

		$firstWeekdayOfMonth = date("N",mktime(0,0,0,$calendarMonth,1,$calendarYear));
		$lastWeekdayOfMonth = date("N",mktime(0,0,0,$calendarMonth+1,0,$calendarYear));
		$lastDayOfMonth = date("d",mktime(0,0,0,$calendarMonth+1,0,$calendarYear));

		for($i=($firstWeekdayOfMonth-1);$i>=1;$i--)
		{
			$timestamp = mktime(0,0,0,$calendarMonth,1-$i,$calendarYear);
			$weekday = date("N",$timestamp);
			$day = date("d",$timestamp);
			$calendar[date('Ymd',$timestamp)] = [
				'timestamp' => $timestamp,
				'day' => $day,
				'weekday' => $weekday,
				'active' => false,
				'items' => []
			];
			if($i==($firstWeekdayOfMonth-1))
			{
				$firstCalendarDay = $timestamp;
			}
		}
		for($i=1;$i<=$lastDayOfMonth;$i++)
		{
			$timestamp = mktime(0,0,0,$calendarMonth,$i,$calendarYear);
			$weekday = date("N",$timestamp);
			$day = date("d",$timestamp);
			$calendar[date('Ymd',$timestamp)] = [
				'timestamp' => $timestamp,
				'day' => $day,
				'weekday' => $weekday,
				'active' => true,
				'items' => []
			];
			if($i==1 && !isset($firstCalendarDay))
			{
				$firstCalendarDay = $timestamp;
			}
		}
		if($lastWeekdayOfMonth<7)
		{
			for($i=1;$i<=(7-$lastWeekdayOfMonth);$i++)
			{
				$timestamp = mktime(0,0,0,$calendarMonth,($lastDayOfMonth+$i),$calendarYear);
				$weekday = date("N",$timestamp);
				$day = date("d",$timestamp);
				$calendar[date('Ymd',$timestamp)] = [
					'timestamp' => $timestamp,
					'day' => $day,
					'weekday' => $weekday,
					'active' => false,
					'items' => []
				];
			}
		}
		$lastCalendarDay = $timestamp+86399;

		// Time restriction greater than or equal
		$timeRestrictionField = $demand->getDateField();
		$timeRestrictionField = (empty($timeRestrictionField)) ? 'datetime' : $timeRestrictionField;
		$timeRestrictionFieldUnderscored = GeneralUtility::camelCaseToLowerCaseUnderscored($timeRestrictionField);

		$demand->setCalendarFrom(date("Y-m-d",$firstCalendarDay));
		$demand->setCalendarTo(date("Y-m-d",$lastCalendarDay));

		// get extended filter
		$extendedFilters = [];
		if(!$this->settings['hideAllFilter'])
		{
			$extendedFilters = $this->getExtendedFilters($demand,$overwriteDemand);
		}

		$years = [];
		if(!$this->settings['hideAllFilter'] && !$this->settings['hideYearFilter'])
		{
			$allyears = $this->newsRepository->findAllYears($demand->getStoragePage());
			$latestYear = 0;
			$newsWithoutDatetime = 0;

			foreach ($allyears as $year) {
				if($year['year']>1970)
				{
					$years[$year['year']] = $year['year'];
					if($year['year']>$latestYear)
					{
						$latestYear = $year['year'];
					}
				}
				else
				{
					$newsWithoutDatetime = 1;
				}
			}
			if($newsWithoutDatetime)
			{
				$years['0'] = "Ohne Jahresangabe";
			}
			if(count($years)>1 && !$overwriteDemand['year'] && $latestYear>0)
			{
				//$overwriteDemand['year'] = $latestYear;
			}
		}

		if ($this->settings['disableOverrideDemand'] != 1 && $overwriteDemand !== null)
		{
			$resetFields = ["type","types","categories","target","targets","years","year","tag","tags","char"];
			foreach($resetFields AS $resetField)
			{
				if(isset($overwriteDemand[$resetField]) && $overwriteDemand[$resetField]=="all")
				{
					unset($overwriteDemand[$resetField]);
				}
			}
			foreach($extendedFilters AS $resetField)
			{
				if($resetField['type']=="select" && isset($overwriteDemand[$resetField['name']]) && $overwriteDemand[$resetField['name']]=="all")
				{
					unset($overwriteDemand[$resetField['name']]);
				}
			}
			$demand = $this->overwriteDemandObject($demand, $overwriteDemand);
		}

		if(!$demand->getCategories() && !$overwriteDemand['categories'] && $this->settings['categories'])
		{
			$demand->setCategories($this->settings['categories']);
		}

		$newsRecords = $this->newsRepository->findDemanded($demand);


		$calendarGetMethodName = "get".GeneralUtility::underscoredToUpperCamelCase($timeRestrictionField);
		foreach($newsRecords AS $newsRecord)
		{
			if(method_exists($newsRecord,$calendarGetMethodName))
			{
				$datetime = $newsRecord->$calendarGetMethodName();
				if(is_object($datetime))
				{
					$timstamp = $datetime->format('U');
				}
				else
				{
					$timstamp = $datetime;
				}

				$start = $timstamp;
				if($start<$firstCalendarDay)
				{
					$start = $firstCalendarDay;
				}
				$end = 0;

				if($GLOBALS['TCA']['tx_mooxnews_domain_model_news']['columns'][$timeRestrictionFieldUnderscored]['moox']['oppositeField'])
				{
					$oppositeField = $GLOBALS['TCA']['tx_mooxnews_domain_model_news']['columns'][$timeRestrictionFieldUnderscored]['moox']['oppositeField'];
				}
				if(!isset($GLOBALS['TCA']['tx_mooxnews_domain_model_news']['columns'][$oppositeField]) || !in_array($GLOBALS['TCA']['tx_mooxnews_domain_model_news']['columns'][$oppositeField]['config']['eval'],['date','datetime']))
				{
					unset($oppositeField);
				}

				if($oppositeField){
					$oppositeFieldGetMethodName = "get".GeneralUtility::underscoredToUpperCamelCase($oppositeField);
					if(method_exists($newsRecord,$oppositeFieldGetMethodName))
					{
						if($newsRecord->$oppositeFieldGetMethodName()>0 && $newsRecord->$oppositeFieldGetMethodName()>$timstamp)
						{
							$end = $newsRecord->$oppositeFieldGetMethodName();
							if($end>$lastCalendarDay)
							{
								$end = $lastCalendarDay;

							}

						}
					}
				}
				$dates = $this->helperService->dateRange(date("Y-m-d",$start),date("Y-m-d",($end>0)?$end:$start),'+1 day','Ymd');
				foreach($dates AS $date)
				{
					$calendar[$date]['items'][] = $newsRecord;
				}
			}
		}

		$categories = [];
		$idList = [];
		if(!$this->settings['hideAllFilter'] && !$this->settings['hideCategoryFilter'])
		{
			$allcategories = $this->categoryRepository->findAllCategories();
			$selectedCategories = explode(",",$this->settings['categories']);

			foreach ($allcategories as $cat)
			{
				if($this->settings['categoryConjunction']=="")
				{
					$categories[$cat->getUid()] = $cat->getTitle();
				}
				elseif(in_array($this->settings['categoryConjunction'],["and","or"]) && in_array($cat->getUid(),$selectedCategories))
				{
					$categories[$cat->getUid()] = $cat->getTitle();
				}
				elseif(in_array($this->settings['categoryConjunction'],["notand","notor"]) && !in_array($cat->getUid(),$selectedCategories))
				{
					$categories[$cat->getUid()] = $cat->getTitle();
				}
			}
			foreach ($allcategories as $c)
			{
				$idList[] = $c->getUid();
			}
		}

		$tags = [];
		if(!$this->settings['hideAllFilter'] && !$this->settings['hideTagFilter'])
		{
			if(is_array($demand->getTags()) && count($demand->getTags()))
			{
				$alltags = $this->tagRepository->findByPidAndUids($demand->getStoragePage(),$demand->getTags);
			}
			else
			{
				$alltags = $this->tagRepository->findByPid($demand->getStoragePage());
			}

			foreach ($alltags as $tag)
			{
				$tags[$tag->getUid()] = $tag->getTitle();
			}
		}

		$targets = [];
		if(!$this->settings['hideAllFilter'] && !$this->settings['hideTargetFilter'])
		{
			if(is_array($demand->getTargets()) && count($demand->getTargets()))
			{
				$alltargets = $this->targetRepository->findByPidAndUids($demand->getStoragePage(),$demand->getTargets);
			}
			else
			{
				$alltargets = $this->targetRepository->findByPid($demand->getStoragePage());
			}

			foreach ($alltargets as $target)
			{
				$targets[$target->getUid()] = $target->getTitle();
			}
		}

		$types = [];
		if(!$this->settings['hideAllFilter'] && !$this->settings['hideTypeFilter'])
		{
			$types = $this->getTypes();
		}

		$count = $newsRecords->count();
		$pages = ceil($count/$this->settings['list']['paginate']['itemsPerPage']);

		$assignedValues = [
			'calendar' => $calendar,
			'prev' => $prev,
			'current' => $current,
			'next' => $next,
			'count' => $count,
			'pages' => $pages,
			'years' => (count($years)>1)?$years:NULL,
			'categories' => (count($categories)>1)?$categories:NULL,
			'categoriesTree' => (count($idList) || $configuration->getCategoryRestriction()!="current_pid")?$this->categoryRepository->findTree($idList):array(),
			'tags' => (count($tags)>1)?$tags:NULL,
			'targets' => (count($targets)>1)?$targets:NULL,
			'showFilter' => (count($categories)>1 || count($types)>1 || count($targets)>1 || count($years)>1)?TRUE:FALSE,
			'overwriteDemand' => $overwriteDemand,
			'demand' => $demand,
			'types' => (count($types)>1)?$types:NULL,
			'charList' => $this->charList,
			'extendedFilters' => $extendedFilters,
			'extendedFields' => $this->getExtendedFields("list"),
			'returnUrl' => $this->uriBuilder->getRequest()->getRequestUri()

		];

		$assignedValues = $this->emitActionSignal('NewsController', self::SIGNAL_NEWS_CALENDAR_ACTION, $assignedValues);
		$this->view->assignMultiple($assignedValues);

		Cache::addPageCacheTagsByDemandObject($demand);
	}

	/**
	 * Single view of a news record
	 *
	 * @param \Neulandlotsen\MooxNews\Domain\Model\News $news news item
	 * @param string $dnews direct news hash
	 * @param int $currentPage current page for optional pagination
	 * @param int $contentObjectUid current content object
	 * @param string $returnUrl optional return url to use in template
	 */
	public function detailAction(
		\Neulandlotsen\MooxNews\Domain\Model\News $news = null,
		$dnews = null,
		$currentPage = 1,
		$contentObjectUid = 0,
		$returnUrl = ''
	){
		if($this->settings['detail']['allowDirectNews'] && $dnews && strlen($dnews)>32)
		{
			$data = json_decode(CoreHelperUtility::decryptText(str_replace(" ","+",urldecode($dnews))),TRUE);

			if(is_array($data) && $data['uid']>0 && $data['sec']!="" && $data['valid']>time())
			{
				$news = $this->newsRepository->findByUid($data['uid'],FALSE);

				if(is_a($news, 'Neulandlotsen\MooxNews\Domain\Model\News') && $data['sec']==$news->getHash())
				{
					if($news->getHidden() || $news->getStarttime()>time() || ($news->getEndtime()>0 && $news->getEndtime()>time()))
					{
						$news = NULL;
					}
					else
					{
						$directNews = true;
						$this->settings['directNews'] = true;
					}
				}
				else
				{
					$news = NULL;
				}

			}
		}
		if (is_null($news))
		{
			$singleNewsId = ((int)$this->settings['singleNews'] > 0) ? $this->settings['singleNews'] : 0;

			if ($singleNewsId > 0)
			{
				if ($this->isPreviewOfHiddenRecordsEnabled())
				{
					$GLOBALS['TSFE']->showHiddenRecords = true;
					$news = $this->newsRepository->findByUid($singleNewsId, false);
				}
				else
				{
					$news = $this->newsRepository->findByUid($singleNewsId);
				}
			}
		}

		// TODO: unclear function
		if ($this->request->hasArgument('overwriteDemand'))
		{
			$overwriteDemand = $this->request->getArgument('overwriteDemand');
		}

		if (is_a($news,'Neulandlotsen\\MooxNews\\Domain\\Model\\News') && $this->settings['detail']['checkPidOfNewsRecord']
		)
		{
			$news = $this->checkPidOfNewsRecord($news);
		}

		if (is_null($news) && isset($this->settings['detail']['errorHandling']))
		{
			$errorContent = $this->handleNoNewsFoundError($this->settings['detail']['errorHandling']);
			if ($errorContent)
			{
				return $errorContent;
			}
		}

		$demand = $this->createDemandObjectFromSettings($this->settings);
		$demand->setActionAndClass(__METHOD__, __CLASS__);

		//'returnUrl' => ($returnUrl)?$returnUrl:$this->uriBuilder->getRequest()->getRequestUri(),

		$assignedValues = [
			'newsItem' => $news,
			'currentPage' => (int)$currentPage,
			'demand' => $demand,
			'returnUrl' => $returnUrl,
			'widgetGetParameter' => "tx_".strtolower($this->extensionName)."_pi1"
		];
		if($this->settings['detail']['loadList'])
		{
			$assignedValues['newsItems'] = $this->newsRepository->findDemanded($demand);
		}

		if($directNews)
		{
			unset($this->settings['listPid']);
			unset($this->settings['backPid']);
			unset($this->settings['historyBack']);
			$assignedValues['settings'] = $this->settings;
		}

		$assignedValues = $this->emitActionSignal('NewsController', self::SIGNAL_NEWS_DETAIL_ACTION, $assignedValues);
		$this->view->assignMultiple($assignedValues);

		Page::setRegisterProperties($this->settings['detail']['registerProperties'], $news);
		if (!is_null($news) && is_a($news, 'Neulandlotsen\\MooxNews\\Domain\\Model\\News')) {
			Cache::addCacheTagsByNewsRecords([$news]);
		}
	}

	/**
	 * preview of a news record
	 *
	 * @param integer $news uid of news item
	 * @param string $hash news hash
	 */
	public function previewAction(
		$news = null,
		$hash = ''
	){
		if($news && $GLOBALS['BE_USER'])
		{
			$GLOBALS['TSFE']->showHiddenRecords = true;
			$news = $this->newsRepository->findByUid($news,FALSE);

			if($news && $hash==$news->getHash())
			{
				$this->view->assign('newsItem', $news);

			}
		}
	}

	/**
	 * pdf view of a news record
	 *
	 * @param integer $news news item
	 * @param string $hash news hash
	 * @param boolean $inline show pdf inline
	 * @param boolean $stream
	 * @param string $generate
	 */
	public function pdfAction(
		$news = null,
		$hash = '',
		$inline = null,
		$stream = true,
		$generate = ''
	){
		if($news)
		{
			if($generate!="" && $generate == md5($GLOBALS['TYPO3_CONF_VARS']['SYS']['encryptionKey']."moox_mailer"))
			{
				$news = $this->newsRepository->findByUid($news,false);
				$stream = false;
			}
			else
			{
				$news = $this->newsRepository->findByUid($news,!$GLOBALS['BE_USER']);
			}

			if($news && $hash==$news->getHash())
			{
				$this->view->assign('newsItem', $news);

				$html = $this->view->render();

				$cached = $this->pdfGenerator->isCached($news->getHash());
				if($cached && $cached['time']>=$news->getTstamp()->format('U'))
				{
					$pdf = $this->pdfGenerator->getCached($news->getHash(), $stream, $news->getTitle(), $inline?false:$this->extConf['pdfForceDownload']);
				}
				else
				{
					$pdf = $this->pdfGenerator->generateFromHtml($html,$inline?false:$this->extConf['pdfForceDownload'],$news->getTitle(),$stream,$news->getHash());
				}
				if(!$stream)
				{
					return $pdf;
				}
			}
		}

		/*
		$sendMail = GeneralUtility::makeInstance('TYPO3\CMS\Core\Mail\MailMessage');
		$sendMail->setFrom(["info@neulandlotsen.de" => "info@neulandlotsen.de"]);
		$sendMail->setTo(["dominic@neulandlotsen.de" => "dominic@neulandlotsen.de"]);
		$sendMail->setSubject("Test PDF-Mail");
		$sendMail->setBody(strip_tags("Test PDF-Mail"));
		$sendMail->addPart("Test PDF-Mail", 'text/html');
		// Create the attachment with your data
		$attachment = new \Swift_Attachment($pdf['data'], $pdf['filename'].".pdf", 'application/pdf');
		// Attach it to the message
		$sendMail->attach($attachment);
		$sendMail->send();
		*/

		exit();
	}

	/**
	 * Get array of allowed types
	 *
	 * @param array $allowedTypes
	 * @return array $types
	 */
	public function getTypes($allowedTypes = [])
	{
		if(!count($allowedTypes))
		{
			$allowedTypes = [];
			if($this->settings['types']!="")
			{
				$allowedTypes = explode(",",$this->settings['types']);
			}
		}

		$types = [];
		foreach($GLOBALS['TCA']['tx_mooxnews_domain_model_news']['columns']['type']['config']['items'] AS $option)
		{
			if($this->settings['types']=="" || in_array($option[1],$allowedTypes))
			{
				$types[$option[1]] = \TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate($option[0], "moox_news");
			}
		}
		return $types;
	}

	/**
	 * Checks if the news pid could be found in the startingpoint settings of the detail plugin and
	 * if the pid could not be found it return NULL instead of the news object.
	 *
	 * @param \Neulandlotsen\MooxNews\Domain\Model\News $news
	 * @return NULL|\Neulandlotsen\MooxNews\Domain\Model\News
	 */
	protected function checkPidOfNewsRecord(\Neulandlotsen\MooxNews\Domain\Model\News $news)
	{
		$allowedStoragePages = GeneralUtility::trimExplode(
			',',
			Page::extendPidListByChildren(
				$this->settings['startingpoint'],
				$this->settings['recursive']
			),
			true
		);
		if (count($allowedStoragePages) > 0 && !in_array($news->getPid(), $allowedStoragePages)) {
			$this->signalSlotDispatcher->dispatch(
				__CLASS__,
				'checkPidOfNewsRecordFailedInDetailAction',
				[
					'news' => $news,
					'newsController' => $this
				]
			);
			$news = null;
		}
		return $news;
	}

	/**
	 * Checks if preview is enabled either in TS or FlexForm
	 *
	 * @return bool
	 */
	protected function isPreviewOfHiddenRecordsEnabled()
	{
		if (!empty($this->settings['previewHiddenRecords']) && $this->settings['previewHiddenRecords'] == 2) {
			$previewEnabled = !empty($this->settings['enablePreviewOfHiddenRecords']);
		} else {
			$previewEnabled = !empty($this->settings['previewHiddenRecords']);
		}
		return $previewEnabled;
	}

	/**
	 * Render a menu by dates, e.g. years, months or dates
	 *
	 * @param array $overwriteDemand
	 */
	public function dateMenuAction(array $overwriteDemand = null)
	{
		$demand = $this->createDemandObjectFromSettings($this->settings);
		$demand->setActionAndClass(__METHOD__, __CLASS__);

		// It might be that those are set, @see http://forge.typo3.org/issues/44759
		$demand->setLimit(0);
		$demand->setOffset(0);
		// @todo: find a better way to do this related to #13856
		if (!$dateField = $this->settings['dateField']) {
			$dateField = 'datetime';
		}
		$demand->setOrder($dateField . ' ' . $this->settings['orderDirection']);
		$newsRecords = $this->newsRepository->findDemanded($demand);

		$demand->setOrder($this->settings['orderDirection']);
		$statistics = $this->newsRepository->countByDate($demand);

		$assignedValues = [
			'listPid' => ($this->settings['listPid'] ? $this->settings['listPid'] : $GLOBALS['TSFE']->id),
			'dateField' => $dateField,
			'data' => $statistics,
			'news' => $newsRecords,
			'overwriteDemand' => $overwriteDemand,
			'demand' => $demand,
		];

		$assignedValues = $this->emitActionSignal('NewsController', self::SIGNAL_NEWS_DATEMENU_ACTION, $assignedValues);
		$this->view->assignMultiple($assignedValues);
	}

	/**
	 * Display the search form
	 *
	 * @param \Neulandlotsen\MooxNews\Domain\Model\Dto\Search $search
	 * @param array $overwriteDemand
	 */
	public function searchFormAction(
		\Neulandlotsen\MooxNews\Domain\Model\Dto\Search $search = null,
		array $overwriteDemand = []
	) {
		$demand = $this->createDemandObjectFromSettings($this->settings);
		$demand->setActionAndClass(__METHOD__, __CLASS__);

		if ($this->settings['disableOverrideDemand'] != 1 && $overwriteDemand !== null) {
			$demand = $this->overwriteDemandObject($demand, $overwriteDemand);
		}

		if (is_null($search)) {
			$search = $this->objectManager->get(\Neulandlotsen\MooxNews\Domain\Model\Dto\Search::class);
		}
		$demand->setSearch($search);

		$assignedValues = [
			'search' => $search,
			'overwriteDemand' => $overwriteDemand,
			'demand' => $demand,
		];

		$assignedValues = $this->emitActionSignal('NewsController', self::SIGNAL_NEWS_SEARCHFORM_ACTION,
			$assignedValues);
		$this->view->assignMultiple($assignedValues);
	}

	/**
	 * Displays the search result
	 *
	 * @param \Neulandlotsen\MooxNews\Domain\Model\Dto\Search $search
	 * @param array $overwriteDemand
	 */
	public function searchResultAction(
		\Neulandlotsen\MooxNews\Domain\Model\Dto\Search $search = null,
		array $overwriteDemand = []
	) {
		$demand = $this->createDemandObjectFromSettings($this->settings);
		$demand->setActionAndClass(__METHOD__, __CLASS__);

		if ($this->settings['disableOverrideDemand'] != 1 && $overwriteDemand !== null) {
			$demand = $this->overwriteDemandObject($demand, $overwriteDemand);
		}

		if (!is_null($search)) {
			$search->setFields($this->settings['search']['fields']);
			$search->setDateField($this->settings['dateField']);
		}
		$demand->setSearch($search);

		$assignedValues = [
			'news' => $this->newsRepository->findDemanded($demand),
			'overwriteDemand' => $overwriteDemand,
			'search' => $search,
			'demand' => $demand,
		];

		$assignedValues = $this->emitActionSignal('NewsController', self::SIGNAL_NEWS_SEARCHRESULT_ACTION,
			$assignedValues);
		$this->view->assignMultiple($assignedValues);
	}

	/**
	 * initialize search result action
	 */
	public function initializeSearchResultAction()
	{
		$this->initializeSearchActions();
	}

	/**
	 * Initialize search form action
	 */
	public function initializeSearchFormAction()
	{
		$this->initializeSearchActions();
	}

	/**
	 * Initialize searchForm and searchResult actions
	 */
	protected function initializeSearchActions()
	{
		if ($this->arguments->hasArgument('search')) {
			$propertyMappingConfiguration = $this->arguments['search']->getPropertyMappingConfiguration();
			$propertyMappingConfiguration->allowAllProperties();
			$propertyMappingConfiguration->setTypeConverterOption('TYPO3\CMS\Extbase\Property\TypeConverter\PersistentObjectConverter', \TYPO3\CMS\Extbase\Property\TypeConverter\PersistentObjectConverter::CONFIGURATION_CREATION_ALLOWED, true);
		}
	}

	/**
	 * action ics
	 *
	 * @param int $newsUid
	 */
	public function icsAction(
		$newsUid = 0
	){
		$this->icsService->writeIcsFile($this->newsRepository->findByUid($newsUid, false), $this->settings);
		exit();
	}

	/***************************************************************************
	 * helper
	 **********************/

	/**
	 * Injects the Configuration Manager and is initializing the framework settings
	 *
	 * @param \TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface $configurationManager Instance of the Configuration Manager
	 */
	public function injectConfigurationManager(
		\TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface $configurationManager
	) {
		$this->configurationManager = $configurationManager;

		$tsSettings = $this->configurationManager->getConfiguration(
			\TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface::CONFIGURATION_TYPE_FRAMEWORK,
			'moox_news',
			'mooxnews_pi1'
		);
		$originalSettings = $this->configurationManager->getConfiguration(
			\TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface::CONFIGURATION_TYPE_SETTINGS
		);

		$propertiesNotAllowedViaFlexForms = ['orderByAllowed'];
		foreach ($propertiesNotAllowedViaFlexForms as $property)
		{
			// TODO: Check if still needed Workaround Dominic Neulandlotsen 2016-04-07
			if($tsSettings['settings'][$property])
			{
				$originalSettings[$property] = $tsSettings['settings'][$property];
			}
		}

		// Use stdWrap for given defined settings
		if (isset($originalSettings['useStdWrap']) && !empty($originalSettings['useStdWrap'])) {
			$typoScriptService = GeneralUtility::makeInstance(TypoScriptService::class);
			$typoScriptArray = $typoScriptService->convertPlainArrayToTypoScriptArray($originalSettings);
			$stdWrapProperties = GeneralUtility::trimExplode(',', $originalSettings['useStdWrap'], true);
			foreach ($stdWrapProperties as $key) {
				if (is_array($typoScriptArray[$key . '.'])) {
					$originalSettings[$key] = $this->configurationManager->getContentObject()->stdWrap(
						$originalSettings[$key],
						$typoScriptArray[$key . '.']
					);
				}
			}
		}

		// start override
		if (isset($tsSettings['settings']['overrideFlexformSettingsIfEmpty'])) {
			$typoScriptUtility = GeneralUtility::makeInstance(TypoScript::class);
			$originalSettings = $typoScriptUtility->override($originalSettings, $tsSettings);
		}

		if (is_array($GLOBALS['TYPO3_CONF_VARS']['EXT']['moox_news']['Controller/NewsController.php']['overrideSettings'])) {
			foreach ($GLOBALS['TYPO3_CONF_VARS']['EXT']['moox_news']['Controller/NewsController.php']['overrideSettings'] as $_funcRef) {
				$_params = [
					'originalSettings' => $originalSettings,
					'tsSettings' => $tsSettings,
				];
				$originalSettings = GeneralUtility::callUserFunction($_funcRef, $_params, $this);
			}
		}

		$this->settings = $originalSettings;
	}

	/**
	 * Get extended fields from external extensions
	 *
	 * @param string $view
	 * @return array $extendedFields
	 */
	public function getExtendedFields($view = 'list')
	{
		$extendedFields = [];
		$lookup = [];

		if($this->settings['addExtendedFields']){

			if($view=="detail"){
				if(is_array($GLOBALS['TCA']['tx_mooxnews_domain_model_news']['moox']['additionalFrontendDetailFields']))
				{
					$lookup = $GLOBALS['TCA']['tx_mooxnews_domain_model_news']['moox']['additionalFrontendDetailFields'];
				}
			} else {
				if(is_array($GLOBALS['TCA']['tx_mooxnews_domain_model_news']['moox']['additionalFrontendListFields']))
				{
					$lookup = $GLOBALS['TCA']['tx_mooxnews_domain_model_news']['moox']['additionalFrontendListFields'];
				}
			}


			foreach($lookup AS $fieldname)
			{
				if(isset($GLOBALS['TCA']['tx_mooxnews_domain_model_news']['columns'][$fieldname]))
				{
					$field = $GLOBALS['TCA']['tx_mooxnews_domain_model_news']['columns'][$fieldname];
					$field['title'] = $fieldname;
					$field['name'] = GeneralUtility::underscoredToLowerCamelCase($fieldname);
					$field['nameEmphasized'] = $fieldname;
					// check this
					/*
					if($field[$key]['type']=="")
					{
						$field[$key]['type'] = "text";
					}
					*/
					$extendedFields[] = $field;
				}
			}
		}

		return $extendedFields;
	}

	/**
	 * Get extended filters from external extensions
	 *
	 * @param Neulandlotsen\MooxNews\Domain\Model\Dto\NewsDemand $demand
	 * @param array $overwriteDemand
	 * @return array extendedFields
	 */
	public function getExtendedFilters($demand,$overwriteDemand = NULL)
	{
		$extendedFilters = [];

		if($this->settings['addExtendedFilter'] && is_array($GLOBALS['TCA']['tx_mooxnews_domain_model_news']['moox']['additionalFrontendListFilter'])){
			foreach($GLOBALS['TCA']['tx_mooxnews_domain_model_news']['moox']['additionalFrontendListFilter'] AS $fieldname){
				if(isset($GLOBALS['TCA']['tx_mooxnews_domain_model_news']['columns'][$fieldname]))
				{
					$field = $GLOBALS['TCA']['tx_mooxnews_domain_model_news']['columns'][$fieldname];

					if($demand->getType()=="" || $demand->getType()==$field['moox']['extkey'])
					{
						$fieldnameLCC = GeneralUtility::underscoredToLowerCamelCase($fieldname);
						$fieldnameUCC = GeneralUtility::underscoredToUpperCamelCase($fieldname);
						$extkey	= GeneralUtility::underscoredToLowerCamelCase($field['moox']['extkey']);
						$label = LocalizationUtility::translate($field['label'], $field['moox']['extkey']);

						if($this->isAjaxRequest)
						{
							if($field['config']['type']=="select")
							{
								$type = "select";
							}
							elseif($field['config']['type']=="input" && $field['moox']['filter']['type']=="select")
							{
								$type = "select";
							}
							else
							{
								$type = "default";
							}
							$extendedFilters[] = [
								"name" => $fieldnameLCC,
								"type" => $type
							];
						}
						else
						{
							if($field['config']['type']=="select")
							{
								$items = [];
								if(is_array($field['config']['items']))
								{
									foreach($field['config']['items'] AS $item)
									{
										if($item[1]=="")
										{
											$item[1] = "all";
										}
										if($item[1]=="all")
										{
											if($field['moox']['filter']['labels']['select_all']!="")
											{
												$item[0] = LocalizationUtility::translate($field['moox']['filter']['labels']['select_all'], $field['moox']['extkey']);
											}
											else
											{
												$item[0] = "Alle";
											}
											$items[$item[1]] = $item[0];
										} else {
											$label = LocalizationUtility::translate($item[0], $field['moox']['extkey']);
											if($label=="")
											{
												$label = $item[0];
											}
											$items[$item[1]] = $label;
										}

									}
								}

								$depends = [];
								$dependFields = [];
								$displayDepends	= [];

								if(is_array($field['moox']['filter']['depends']))
								{
									foreach($field['moox']['filter']['depends'] AS $depend)
									{
										if(isset($GLOBALS['TCA']['tx_mooxnews_domain_model_news']['columns'][$depend]))
										{
											$dependField = $GLOBALS['TCA']['tx_mooxnews_domain_model_news']['columns'][$depend];
											if($dependField['config']['type']=="select" || ($dependField['config']['type']=="input" && $dependField['moox']['filter']['type']=="select"))
											{
												$depends[] = GeneralUtility::underscoredToLowerCamelCase($depend);
												$dependField = GeneralUtility::underscoredToLowerCamelCase($depend);
												if(isset($overwriteDemand[$dependField]) && $overwriteDemand[$dependField]!="all")
												{
													$dependFields[$depend] = $overwriteDemand[$dependField];
												}
											}
										}
									}
								}

								if(is_array($field['moox']['filter']['displayDepends']))
								{
									foreach($field['moox']['filter']['displayDepends'] AS $displayDepend)
									{
										if(isset($GLOBALS['TCA']['tx_mooxnews_domain_model_news']['columns'][$displayDepend]))
										{
											$displayDependField = $GLOBALS['TCA']['tx_mooxnews_domain_model_news']['columns'][$displayDepend];
											if($displayDependField['config']['type']=="select" || ($displayDependField['config']['type']=="input" && $displayDependField['moox']['filter']['type']=="select"))
											{
												$displayDepends[] = GeneralUtility::underscoredToLowerCamelCase($displayDepend);
											}
										}
									}
								}

								if($field['config']['foreign_table']!="")
								{
									$itemsFromDb = $this->newsRepository->findExtendedFilterItems(
										$fieldname,
										$demand->getStoragePage(),
										$field['config']['foreign_table'],
										$field['config']['foreign_table_where'],
										$dependFields
									);
									foreach($itemsFromDb AS $item)
									{
										$items[$item['uid']] = $item['title'];
									}
								}
								if(count($items))
								{
									if(!isset($items['all']))
									{
										if($field['moox']['filter']['labels']['select_all']!="")
										{
											$selectAllLabel = LocalizationUtility::translate($field['moox']['filter']['labels']['select_all'], $field['moox']['extkey']);
										}
										else
										{
											$selectAllLabel = "Alle";
										}
										$itemsTmp = $items;
										$items = [];
										$items["all"] = $selectAllLabel;
										foreach($itemsTmp AS $key => $value)
										{
											$items[$key] = $value;
										}
										unset($itemsTmp);
									}
									$extendedFilters[] = [
										"label" => $label,
										"name" => $fieldnameLCC,
										"type" => 'select',
										"field" => $fieldname,
										"fieldUCC" => $fieldnameUCC,
										"items" => $items,
										"depends" => $depends,
										"displayDepends" => $displayDepends
									];
								}
							}
							elseif($field['config']['type']=="input" && $field['moox']['filter']['type']=="select")
							{
								if(!isset($overwriteDemand[$fieldnameLCC]))
								{
									//$overwriteDemand[$fieldnameLCC] = "all";
								}

								$depends = [];
								$dependFields = [];
								$displayDepends	= [];

								if(is_array($field['moox']['filter']['depends']))
								{
									foreach($field['moox']['filter']['depends'] AS $depend)
									{
										if(isset($GLOBALS['TCA']['tx_mooxnews_domain_model_news']['columns'][$depend]))
										{
											$dependField = $GLOBALS['TCA']['tx_mooxnews_domain_model_news']['columns'][$depend];
											if($dependField['config']['type']=="select" || ($dependField['config']['type']=="input" && $dependField['moox']['filter']['type']=="select"))
											{
												$depends[] = GeneralUtility::underscoredToLowerCamelCase($depend);
												$dependField = GeneralUtility::underscoredToLowerCamelCase($depend);
												if(isset($overwriteDemand[$dependField]) && $overwriteDemand[$dependField]!="all")
												{
													$dependFields[$depend] = $overwriteDemand[$dependField];
												}
											}
										}
									}
								}
								if(is_array($field['moox']['filter']['displayDepends']))
								{
									foreach($field['moox']['filter']['displayDepends'] AS $displayDepend)
									{
										if(isset($GLOBALS['TCA']['tx_mooxnews_domain_model_news']['columns'][$displayDepend]))
										{
											$displayDependField = $GLOBALS['TCA']['tx_mooxnews_domain_model_news']['columns'][$displayDepend];
											if($displayDependField['config']['type']=="select" || ($displayDependField['config']['type']=="input" && $displayDependField['moox']['filter']['type']=="select"))
											{
												$displayDepends[] = GeneralUtility::underscoredToLowerCamelCase($displayDepend);
											}
										}
									}
								}

								$values = [];

								$valuesFromDb = $this->newsRepository->findExtendedFilterUniqueValues($fieldname,$demand->getStoragePage(),$dependFields);

								if(count($valuesFromDb))
								{
									if($field['moox']['filter']['labels']['select_all']!="")
									{
										$values['all'] = LocalizationUtility::translate($field['moox']['filter']['labels']['select_all'], $field['moox']['extkey']);
									}
									else
									{
										$values['all'] = "Alle";
									}

									$valueCnt = 0;

									foreach($valuesFromDb AS $value)
									{
										if($value=="empty"){
											if($field['moox']['filter']['labels']['select_empty']!=""){
												$valueLabel = LocalizationUtility::translate($field['moox']['filter']['labels']['select_empty'], $field['moox']['extkey']);
												$values[$value] = $valueLabel;
											} else {
												$valueLabel = "Ohne Wert";
											}
										} else {
											$values[$value] = $value;
											$valueCnt++;
										}

									}

									if($valueCnt){

										$extendedFilters[] = array(
											"label" => $label,
											"name" => $fieldnameLCC,
											"type" => 'select',
											"field" => $fieldname,
											"fieldUCC" => $fieldnameUCC,
											"items" => $values,
											"depends" => $depends,
											"displayDepends" => $displayDepends
										);
									}
								}
							} else {
								$extendedFilters[] = array(
									"label" => $label,
									"name" => $fieldnameLCC,
									"type" => 'default',
									"field" => $fieldname,
									"fieldUCC" => $fieldnameUCC
								);
							}
						}
					}
				}
			}
		}

		return $extendedFilters;
	}

	/**
	 * Injects a view.
	 * This function is for testing purposes only.
	 *
	 * @param \TYPO3\CMS\Fluid\View\TemplateView $view the view to inject
	 * @return void
	 */
	public function setView(\TYPO3\CMS\Fluid\View\TemplateView $view)
	{
		$this->view = $view;
	}

	/**
	 * @return void
	 */
	public function secureDownloadAction()
	{
		exit("error");
	}

	/**
	 * @param bool $isAjaxRequest
	 */
	public function setIsAjaxRequest($isAjaxRequest)
	{
		$this->isAjaxRequest = $isAjaxRequest;
	}

	/**
	 * @return bool
	 */
	public function getIsAjaxRequest()
	{
		return $this->isAjaxRequest;
	}

	/**
	 * @param array $charList
	 */
	public function setCharList($charList)
	{
		$this->charList = $charList;
	}

	/**
	 * @return array
	 */
	public function getCharList()
	{
		return $this->charList;
	}

	public function csvTrim($in){
		//$in = nl2br(trim(addslashes($in)));
		$in = str_replace("&nbsp;", " ", $in);
		$in = str_replace("<p>", "", $in);
		$in = str_replace("</p>", "\n", $in);
		$in = str_replace("<strong>", "", $in);
		$in = str_replace("</strong>", "", $in);
		$in = str_replace("<ul>", "", $in);
		$in = str_replace("<li>", "- ", $in);
		$in = str_replace("</li>", "\n", $in);
		$in = str_replace("</ul>", "\n", $in);
		$in = str_replace("<br />", "\n", $in);
		$in = str_replace("<br/>", "\n", $in);
		$in = str_replace("<br>", "\n", $in);
		$in = str_replace(array("\r\n", "\n", "\r"),"nnn-",$in);
		return trim($in);
	}

	public function csvTrim2($in){
		//$in = nl2br(trim(addslashes($in)));
		$in = str_replace("\n", "", $in);
		$in = str_replace("\r", "", $in);
		$in = str_replace("&nbsp;", " ", $in);
		$in = str_replace("<p>", "", $in);
		$in = str_replace("</p>", "\n", $in);
		$in = str_replace("<strong>", "", $in);
		$in = str_replace("</strong>", "", $in);
		$in = str_replace("<ul>", "", $in);
		$in = str_replace("<li>", "- ", $in);
		$in = str_replace("</li>", "\n", $in);
		$in = str_replace("</ul>", "\n", $in);
		$in = str_replace("<br />", "\n", $in);
		$in = str_replace("<br/>", "\n", $in);
		$in = str_replace("<br>", "\n", $in);
		$in = str_replace(array("\r\n", "\n", "\r"),"nnn-",$in);
		return trim($in);
	}

	/**
	 * @param array $settings
	 */
	public function setSettings($settings = NULL)
	{
		if(is_array($settings) && count($settings))
		{
			$this->settings = array_merge($this->settings,$settings);
		}
	}
}