define([
	'jquery',
	'TYPO3/CMS/MooxCore/Backend/Module'], function ($, MooxModule) {

	var MooxNewsMod1 = {};

	MooxNewsMod1.init = function () {

		MooxModule.initClearButtons();
		MooxNewsMod1.toggleSelector();

		$(document).on('submit', '#moox-news-module-filter form.use-ajax', function (event) {
			event.preventDefault();

			var url = $(this).attr("action");
			var wrapper = MooxNewsMod1.getWrapper($(this));
			var plugin = $('#'+wrapper).data('plugin');
			var form = "#" + $(this).attr("id");
			var hasCategories = false;

			var data = {};
			data['tx_mooxnews_'+plugin+'[ajax]'] = 1;

			$(form + ' input.is-grouped-filter').each(function () {
				name = $(this).attr('name');

				if ($(this).attr('type') == 'checkbox') {
					if ($(this).is(':checked')) {
						name = name.replace("[]", "[" + $(this).val() + "]");
						if ($(this).hasClass("is-category-filter")) {
							hasCategories = true;
						}
					} else {
						name = '';
					}
				}
				if (name != '') {
					data[name] = $(this).val();
				}
			});
			if (!hasCategories) {
				data['tx_mooxnews_'+plugin+'[filter][categories]'] = 'all';
			}

			$(form + ' select.is-grouped-filter').each(function () {
				data[$(this).attr('name')] = $(this).val();
			});

			$.ajax(url,
				{
					method: "POST",
					cache: false,
					data: data,
					success: function (data) {
						/*mooxNewsMod1Refresh(wrapper);*/
						body = $("#" + wrapper + ".use-ajax #moox-news-module-body").first();
						window.location = body.data('url');
					},
					error: function (data) {

					},
					beforeSend: function () {
						$("body").append("<div id='moox-news-module-overlay'><img src='../typo3conf/ext/moox_news/Resources/Public/Images/Backend/loading.gif' /></div>");
					},
					complete: function () {
						/*$("#moox-news-module-overlay").remove();*/
					},
				});

			return false;
		});

		$(document).on('click', '#moox-news-module-filter a.page-selector', function (event) {
			event.preventDefault();
			$('#moox-news-module-filter input.page-selector').val($(this).data('uid'));
			$('#moox-news-module-filter button.page-selector .current').html($(this).html());
			if ($(this).hasClass('auto-submit')) {
				$('#moox-news-module-filter form').submit();
			}
		});

		$(document).on('click', '#moox-news-module-filter a.language-selector', function (event) {
			event.preventDefault();
			$('#moox-news-module-filter input.language-selector').val($(this).data('id'));
			$('#moox-news-module-filter button.language-selector .current').html($(this).html());
			if ($(this).hasClass('auto-submit')) {
				$('#moox-news-module-filter form').submit();
			}
		});

		$(document).on('click', '#moox-news-module-filter a.type-selector', function (event) {
			event.preventDefault();
			$('#moox-news-module-filter input.type-selector').val($(this).data('id'));
			$('#moox-news-module-filter button.type-selector .current').html($(this).html());
			if ($(this).hasClass('auto-submit')) {
				$('#moox-news-module-filter form').submit();
			}
		});

		$(document).on('click', '#moox-news-module-filter a.group-selector', function (event) {
			event.preventDefault();
			$('#moox-news-module-filter input.group-selector').val($(this).data('uid'));
			$('#moox-news-module-filter button.group-selector .current').html($(this).html());
			if ($(this).hasClass('auto-submit')) {
				$('#moox-news-module-filter form').submit();
			}
		});

		$(document).on('click', '#moox-news-module-filter a.additional-equals-filter', function (event) {
			event.preventDefault();
			button = $(this).parents('.btn-group').find('button .current').first();
			input = $(this).parents('.btn-group').find('input[type=hidden]').first();
			input.val($(this).data('value'));
			button.html($(this).html());
			if ($(this).hasClass('auto-submit')) {
				$('#moox-news-module-filter form').submit();
			}
		});

		$(document).on('click', '#moox-news-module-body a.show-detail', function (event) {
			event.preventDefault();

			visible = false;
			if ($('#moox-news-module-body #news-detail-' + $(this).data('uid')).is(":visible")) {
				visible = true;
			}

			$('#moox-news-module-body .news-detail').hide();
			if (!visible && $('#moox-news-module-body #news-detail-' + $(this).data('uid'))) {
				$('#moox-news-module-body #news-detail-' + $(this).data('uid')).show();
			}
			$('#moox-news-module-body a.show-detail .icon-up').hide();
			$('#moox-news-module-body a.show-detail .icon-down').show();
			if (visible) {
				$(this).find('.icon-down').show();
				$(this).find('.icon-up').hide();
			} else {
				$(this).find('.icon-down').hide();
				$(this).find('.icon-up').show();

			}
		});

		$(document).on('click', '#moox-news-module-filter button.btn-category-selector-clear', function (event) {
			event.preventDefault();
			$('#moox-news-module-filter .is-category-filter').attr('checked', false);
			$('#moox-news-module-filter form').submit();
		});

		$(document).on('show.bs.dropdown', '.btn-group-category-selector', function () {

		});

		$(document).on('hide.bs.dropdown', '.btn-group-category-selector', function () {

		});

		$(document).on('change', '#moox-news-module-filter select.auto-submit', function (event) {
			event.preventDefault();
			$('#moox-news-module-filter form').submit();
		});

		$(document).on('change', '#moox-news-module-filter input.auto-submit', function (event) {
			event.preventDefault();
			$('#moox-news-module-filter form').submit();
		});

		$(document).on('click', '#moox-news-module-filter a.filter-reset', function (event) {
			event.preventDefault();

			var url = $(this).attr("href");
			var wrapper = MooxNewsMod1.getWrapper($(this));
			var plugin = $("#"+wrapper).data('plugin');

			var data = {};
			data['tx_mooxnews_'+plugin+'[ajax]'] = 1;

			$.ajax(url,
				{
					method: "POST",
					cache: false,
					data: data,
					success: function (data) {
						body = $("#" + wrapper + ".use-ajax #moox-news-module-body").first();
						window.location = body.data('url');
					},
					error: function (data) {

					},
					beforeSend: function () {
						$("body").append("<div id='moox-news-module-overlay'><img src='../typo3conf/ext/moox_news/Resources/Public/Images/Backend/loading.gif' /></div>");
					},
					complete: function () {
						/*$("#moox-news-module-overlay").remove();*/
					},
				});

			return false;
		});

		$(document).on('click', '.moox-news-module.use-ajax #moox-news-module-body a.change-ordering', function (event) {
			event.preventDefault();

			var url = $(this).attr("href");
			var wrapper = MooxNewsMod1.getWrapper($(this));
			var plugin = $('#'+wrapper).data('plugin');

			var data = {};
			data['tx_mooxnews_'+plugin+'[ajax]'] = 1;

			$.ajax(url,
				{
					method: "POST",
					cache: false,
					data: data,
					success: function (data) {
						body = $("#" + wrapper + ".use-ajax #moox-news-module-body").first();
						window.location = body.data('url');
					},
					error: function (data) {

					},
					beforeSend: function () {
						$("body").append("<div id='moox-news-module-overlay'><img src='../typo3conf/ext/moox_news/Resources/Public/Images/Backend/loading.gif' /></div>");
					},
					complete: function () {
						/*$("#moox-news-module-overlay").remove();*/
					},
				});

			return false;
		});

		$("#moox-module-multiple-selector-form .selector-checkbox").on("change", function () {
			if ($(this).prop("checked")) {
				$(this).parent().parent().addClass("info");
			} else {
				$(this).parent().parent().removeClass("info");
			}
			checkedCnt = $("#moox-module-multiple-selector-form .selector-checkbox:checked").length;
			allCnt = $("#moox-module-multiple-selector-form .selector-checkbox").length;
			if (checkedCnt == allCnt) {
				$("#moox-module-multiple-selector-form .moox-module-multiple-all").prop("checked", true);
			} else {
				$("#moox-module-multiple-selector-form .moox-module-multiple-all").prop("checked", false);
			}
			MooxNewsMod1.toggleSelector();
		});

		$("#moox-module-multiple-selector-form .moox-module-multiple-all").on("change", function () {
			if ($(this).prop("checked")) {
				$("#moox-module-multiple-selector-form .moox-module-multiple-all").prop("checked", true);
				$("#moox-module-multiple-selector-form .selector-checkbox").each(function () {
					$(this).prop("checked", true)
					$(this).parent().parent().addClass("info");
				});
			} else {
				$("#moox-module-multiple-selector-form .moox-module-multiple-all").prop("checked", false);
				$("#moox-module-multiple-selector-form .selector-checkbox").each(function () {
					$(this).prop("checked", false)
					$(this).parent().parent().removeClass("info");
				});
			}
			MooxNewsMod1.toggleSelector();
		});

		$("#moox-module-multiple-confirmation-form .btn-return").on("click", function (event) {
			event.preventDefault();
			window.history.back();
		});
	};

	MooxNewsMod1.refresh = function (wrapper) {
		var body = $("#" + wrapper + ".use-ajax #moox-news-module-body").first();
		var plugin = $('#'+wrapper).data('plugin');

		var data = {};

		data['tx_mooxnews_'+plugin+'[ajax]'] = 1;

		$.ajax(body.data("url"),
		{
			method: "POST",
			data: data,
			success: function (data) {
				body.html($(data));
			},
			beforeSend: function () {
				$("body").append("<div id='moox-news-module-overlay'><img src='../typo3conf/ext/moox_news/Resources/Public/Images/Backend/loading.gif' /></div>");
			},
			complete: function () {
				$("#moox-news-module-overlay").remove();
			},
		});
	};

	MooxNewsMod1.toggleSelector = function () {
		checkedCnt = $("#moox-module-multiple-selector-form .selector-checkbox:checked").length;
		if (checkedCnt) {
			$(".moox-module-multiple-functions .selector-counter").html("<strong>" + checkedCnt + "</strong> Einträge gewählt");
			$(".moox-module-multiple-functions").show("fast");
		} else {
			$(".moox-module-multiple-functions").hide("fast");
			$(".moox-module-multiple-functions .selector-counter").html("Keine Einträge gewählt");
		}
	};

	MooxNewsMod1.getWrapper = function(object) {
		return object.parents('.moox-news-module-mod1').attr("id");
	};

	MooxNewsMod1.init();

	return MooxNewsMod1;
});