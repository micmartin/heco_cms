<?php
namespace Neulandlotsen\MooxMailer\Domain\Repository;

/**
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */

use TYPO3\CMS\Extbase\Persistence\QueryInterface; 

class NewsletterRepository extends \TYPO3\CMS\Extbase\Persistence\Repository 
{
	
	protected $defaultOrderings = array ('title' => QueryInterface::ORDER_ASCENDING, 'subject' => QueryInterface::ORDER_ASCENDING);	
	
	/**
	 * Finds all newsletters (overwrite)
	 *	
	 * @param array $storagePids
	 * @param array $filter
	 * @return \TYPO3\CMS\Extbase\Persistence\QueryResultInterface The newsletters
	 */
	public function findAll($storagePids = array(), $filter = array())
	{		
		$query = $this->createQuery();
		
		if(is_numeric($storagePids) && $storagePids>0){
			$storagePids = array($storagePids);
		}
		
		if(!is_array($storagePids) || count($storagePids)<1){
			$storagePids = array(0);			
		}
		
		$query->getQuerySettings()->setIgnoreEnableFields(TRUE);
		$query->getQuerySettings()->setStoragePageIds($storagePids);
		
		$constraints = array();
				
		$constraints[] = $query->equals('deleted', 0);
		
		if($filter['state']==1){
			$constraints[] = $query->greaterThan('sent', 0);
		} else {
			$constraints[] = $query->equals('sent', 0);
		}
		
		$filter['query'] = trim($filter['query']);
		if($filter['query']!=""){
			$constraints[] = $query->logicalOr(																
				$query->like('title', "%".$filter['query']."%"),
				$query->like('subject', "%".$filter['query']."%"),
				$query->like('senderName', "%".$filter['query']."%"),
				$query->like('senderEmail', "%".$filter['query']."%")
			);
		}
		
		if($filter['sortDirection']=="DESC"){
			$filter['sortDirection'] = QueryInterface::ORDER_DESCENDING;
		} else {
			$filter['sortDirection'] = QueryInterface::ORDER_ASCENDING;
		}
		
		if($filter['sortField']=="subject"){
			$query->setOrderings (Array('subject' => $filter['sortDirection']));
		} elseif($filter['sortField']=="sent"){
			$query->setOrderings (Array('sent' => $filter['sortDirection']));
		} else {
			$query->setOrderings (Array('title' => $filter['sortDirection'], 'subject' => $filter['sortDirection']));
		}
				
		return $query->matching(
			$query->logicalAnd($constraints))->execute();
	}
	
	/**
	 * Finds all drafts
	 *	
	 * @param int $limit	
	 * @return \TYPO3\CMS\Extbase\Persistence\QueryResultInterface The newsletters
	 */
	public function findDrafts($limit = 0)
	{		
		$query = $this->createQuery();
		
		$query->getQuerySettings()->setRespectStoragePage(FALSE);		
		$query->getQuerySettings()->setIgnoreEnableFields(TRUE);
		$query->setOrderings(Array('tstamp' => QueryInterface::ORDER_DESCENDING));
		
		if($limit>0){
			$query->setLimit($limit);
		}
		
		$constraints = array();
				
		$constraints[] = $query->equals('deleted', 0);
		$constraints[] = $query->equals('sent', 0);
				
		return $query->matching(
			$query->logicalAnd($constraints))->execute();
	}
	
	/**
	 * Finds all sent newsletters
	 *	
	 * @param int $limit	
	 * @return \TYPO3\CMS\Extbase\Persistence\QueryResultInterface The newsletters
	 */
	public function findSentNewsletters($limit = 0)
	{		
		$query = $this->createQuery();
		
		$query->getQuerySettings()->setRespectStoragePage(FALSE);		
		$query->getQuerySettings()->setIgnoreEnableFields(TRUE);
		$query->setOrderings(Array('tstamp' => QueryInterface::ORDER_DESCENDING));
		
		if($limit>0){
			$query->setLimit($limit);
		}
		
		$constraints = array();
				
		$constraints[] = $query->equals('deleted', 0);
		$constraints[] = $query->greaterThan('sent', 0);
				
		return $query->matching(
			$query->logicalAnd($constraints))->execute();
	}
	
	/**
	 * Override default findByUid function to enable also the option to turn of
	 * the enableField setting
	 *
	 * @param int $uid id of record
	 * @param bool $respectEnableFields if set to false, hidden records are shown
	 * @return \Neulandlotsen\MooxMailer\Domain\Model\Newsletter
	 */
	public function findByUid($uid, $respectEnableFields = TRUE)
	{
		$query = $this->createQuery();
		$query->getQuerySettings()->setRespectStoragePage(FALSE);
		$query->getQuerySettings()->setRespectSysLanguage(FALSE);
		$query->getQuerySettings()->setIgnoreEnableFields(!$respectEnableFields);

		return $query->matching(
			$query->logicalAnd(
				$query->equals('uid', $uid),
				$query->equals('deleted', 0)
			))->execute()->getFirst();
	}

	/**
	 * find by job
	 *
	 * @param string $job job
	 * @return \Neulandlotsen\MooxMailer\Domain\Model\Newsletter
	 */
	public function findByJob($job)
	{
		$query = $this->createQuery();
		$query->getQuerySettings()->setRespectStoragePage(FALSE);
		$query->getQuerySettings()->setRespectSysLanguage(FALSE);
		$query->getQuerySettings()->setIgnoreEnableFields(true);

		return $query->matching(
			$query->logicalAnd(
				$query->equals('job', $job),
				$query->equals('deleted', 0)
			))->execute()->getFirst();
	}
}
?>