<?php

namespace Neulandlotsen\MooxNews\ViewHelpers\Widget\Ajax;

/**
 * This file is part of the "moox_news" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

/**
 * Generate additional params required for the pagination
 */
class PaginateAdditionalParamsViewHelper extends \TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper
{

    /**
     * @param int $page
     * @return array
     */
    public function render($page = 0)
    {
        if ($page === 0) {
            return [];
        }
        $params = [
            'tx_mooxnews_pi1' => [
                '@widget_0' => [
                    'currentPage' => $page
                ]
            ]
        ];

        return $params;
    }
}
