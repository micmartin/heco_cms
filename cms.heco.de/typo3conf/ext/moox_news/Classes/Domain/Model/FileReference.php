<?php
namespace Neulandlotsen\MooxNews\Domain\Model;

/**
 * This file is part of the "moox_news" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

/**
 * File Reference
 */
class FileReference extends \TYPO3\CMS\Extbase\Domain\Model\FileReference
{

	/**
	 * @var int
	 */
	protected $uid;

    /**
     * Obsolete when foreign_selector is supported by ExtBase persistence layer
     *
     * @var int
     */
    protected $uidLocal;
		
    /**
     * @var string
     */
	protected $localTable;

	/**
	 * @var string
	 */
    protected $title;

    /**
     * @var string
     */
    protected $description;

    /**
     * @var string
     */
    protected $alternative;

    /**
     * @var string
     */
    protected $link;

    /**
     * @var bool
     */
    protected $showinpreview;
	
	/**
	 * @var bool
	 */
	protected $hideindetail;

	/**
     * @var string
     */
    protected $extension;
	
	/**
     * @var Neulandlotsen\MooxNews\Domain\Model\File
     */
    protected $file;
	
	/**
	 * @var integer
	 */
	protected $cruserId;
	
	/**
     * @var string
     */
    protected $crop;
	
	/**
     * @param Neulandlotsen\MooxNews\Domain\Model\File $file
     */
    public function setFile($file) {
        $this->file = $file;
    }

    /**
     * @return Neulandlotsen\MooxNews\Domain\Model\File $file
     */
    public function getFile() {
        return $this->file;
    }
	
	/**
	 * @return string
	 */
	public function getCruserId() {
		return $this->cruserId;
	}

	/**
	 * @param string $cruserId cruser id	 
	 */
	public function setCruserId($cruserId) {
		$this->cruserId = $cruserId;
	}
	
	/**
	 * @param int $uid	 
	 */
	public function setUid($uid) {
		$this->uid = $uid;
	}

	/**
	 * @return int
	 */
	public function getUid() {
		return $this->uid;
	}

    /**
     * Set File uid
     *
     * @param int $fileUid
     */
    public function setFileUid($fileUid)
    {
        $this->uidLocal = $fileUid;
    }

    /**
     * Get File UID
     *
     * @return int
     */
    public function getFileUid()
    {
        return $this->uidLocal;
    }

    /**
	 * @param string $fileTable
	 */
	public function setFileTable($fileTable)
	{
		$this->tableLocal = $fileTable;
	}

	/**
	 * @return string
	 */
	public function getFileTable()
	{
		return $this->tableLocal;
	}

	/**
     * Set alternative
     *
     * @param string $alternative
     */
    public function setAlternative($alternative)
    {
        $this->alternative = $alternative;
    }

    /**
     * Get alternative
     *
     * @return string
     */
    public function getAlternative()
    {
        return $this->alternative !== null ? $this->alternative : $this->getOriginalResource()->getAlternative();
    }

    /**
     * Set description
     *
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description !== null ? $this->description : $this->getOriginalResource()->getDescription();
    }

    /**
     * Set link
     *
     * @param string $link
     */
    public function setLink($link)
    {
        $this->link = $link;
    }

    /**
     * Get link
     *
     * @return mixed
     */
    public function getLink()
    {
        return $this->link !== null ? $this->link : $this->getOriginalResource()->getLink();
    }

    /**
     * Set title
     *
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title !== null ? $this->title : $this->getOriginalResource()->getTitle();
    }

    /**
     * Get extension
     *
     * @return string
     */
    public function getExtension()
    {
        return "test";
    }

    /**
     * Set showinpreview
     *
     * @param bool $showinpreview
     */
    public function setShowinpreview($showinpreview)
    {
        $this->showinpreview = $showinpreview;
    }

    /**
     * Get showinpreview
     *
     * @return bool
     */
    public function getShowinpreview()
    {
        return $this->showinpreview;
    }
	
	/**
	 * Get hideindetail
	 *
	 * @return bool
	 */
	public function getHideindetail()
	{
		return $this->hideindetail;
	}

	/**
	 * Set hideindetail
	 *
	 * @param bool $hideindetail
	 */
	public function setHideindetail($hideindetail)
	{
		$this->hideindetail = $hideindetail;
	}

	/**
	 * @return string
	 */
	public function getCrop() {
		return $this->crop;
	}

	/**
	 * @param string $crop	 
	 */
	public function setCrop($crop) {
		$this->crop = $crop;
	}
}
