<?php
namespace Neulandlotsen\MooxCore\Controller;

/**
 * This file is part of the "moox_core" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

use Neulandlotsen\MooxCore\Utility\HelperUtility as CoreHelperUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\TypoScript\ExtendedTemplateService;
use TYPO3\CMS\Core\Utility\RootlineUtility;
use TYPO3\CMS\Extbase\Object\ObjectManager;
use Neulandlotsen\MooxNews\Domain\Repository\NewsRepository;
use TYPO3\CMS\Frontend\Page\PageRepository;
use Neulandlotsen\MooxCore\Service\HelperService;
	 
class FileSecurityController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController 
{
	/**
	 * @var ObjectManager
	 */
	protected $objectManager;
	
	/**
	 * @var NewsRepository
	 */
	protected $newsRepository;
	
	/**
	 * @var PageRepository
	 */
	protected $pageRepository;

	/**
	 * @var HelperService
	 */
	protected $helperService;

    /**
     * @param ObjectManager $objectManager
     */
    public function injectObjectManager(ObjectManager $objectManager)
    {
        $this->objectManager = $objectManager;
    }

    /**
     * @param NewsRepository $newsRepository
     */
    public function injectNewsRepository(NewsRepository $newsRepository)
    {
        $this->newsRepository = $newsRepository;
    }

	/**
     * @param PageRepository $pageRepository
     */
    public function injectPageRepository(PageRepository $pageRepository)
    {
        $this->pageRepository = $pageRepository;
    }

    /**
     * @param HelperService $helperService
     */
    public function injectHelperService(HelperService $helperService)
    {
        $this->helperService = $helperService;
    }
	
	/**
	 * get ts settings
	 *
	 * @param int $pageUid page uid
	 * @return array $TSObj->setup
	 */
	public function getTsSetup($pageUid) 
	{
        $rootLine = GeneralUtility::makeInstance(RootlineUtility::class, $pageUid, '', $this->pageRepository)->get();
		$TSObj = GeneralUtility::makeInstance(ExtendedTemplateService::class);             
        $TSObj->tt_track = 0;
        $TSObj->init();
        $TSObj->runThroughTemplates($rootLine);
        $TSObj->generateConfig();		
		
		return $TSObj->setup;
	}
	
	/**
	 * get base url
	 *
	 * @param int $pageUid page uid
	 * @return string $baseURL
	 */
	public function getBaseUrl($pageUid) 
	{
		$TS = $this->getTsSetup($pageUid);
          
		if($_SERVER['HTTPS']=="on")
		{
			$prefix = "https";
		} 
		else 
		{
			$prefix = "http";
		}
		
		if($TS['config.']['baseURL']==$prefix."://")
		{ 
			$baseURL = $prefix."://".$_SERVER['SERVER_NAME']."/";
		}  
		elseif($TS['config.']['baseURL']=="//")
		{ 
			$baseURL = $prefix."://".$_SERVER['SERVER_NAME']."/";
		} 
		elseif($TS['config.']['baseURL']=="/")
		{ 
			$baseURL = $prefix."://".$_SERVER['SERVER_NAME']."/";
		} 
		else 
		{
			$baseURL = $TS['config.']['baseURL'];
			if(substr(strtolower($baseURL),0,2)=="//")
			{				
				$baseURL = $prefix.":".$baseURL;
			}
		}
		
		if(substr($baseURL,(strlen($baseURL)-1))!="/")
		{ 
			$baseURL = $baseURL."/";
		} 

		return $baseURL;
	}
	
	/**
	 * get login redirector pif
	 *
	 * @param int $pageUid page uid
	 * @return string $loginRedirectorPid
	 */
	public function getLoginRedirectorPid($pageUid) 
	{
		$TS = $this->getTsSetup($pageUid);
        
		return $TS['plugin.']['tx_mooxnews.']['settings.']['feLoginPid'];
	}
	
	/**
	 * Get file info	
	 *
	 * @param int $uid
	 * @return	array	file 	
	 */
	public function getFileInfo($filename)
	{
		$seperatorIndex = strrpos ($filename, ".");
		$file['name'] = substr ($filename, 0, $seperatorIndex);
		$file['extension'] = strtolower(substr ($filename, ($seperatorIndex+1)));
		return $file;
	}
	
	/**
	 * action load file
	 *	
	 * @param \string $file
	 * @param \string $hash	
	 * @param \string $pre
	 * @return void
	 */
	public function loadFileAction($file = '', $hash = '', $pre = '')
	{
		$data = json_decode(CoreHelperUtility::decryptText(urldecode($hash)),TRUE);
		$data['file'] = $file;
		$data['prefix'] = $pre;

		if($data['pid']>0)
		{
			$loginRedirectorPid = $this->getLoginRedirectorPid($data['pid']);
			$data['baseUrl'] = $this->getBaseUrl($data['pid']);

			if($loginRedirectorPid>0)
			{
				$cObj = GeneralUtility::makeInstance('tslib_cObj');
				$data['loginurl'] = $cObj->typolink('', ['returnLast' => 'url', 'parameter' => $loginRedirectorPid, 'additionalParams' => '&redirect_url=redirecturi', 'useCacheHash' => 0]);
			}
		}

		if($data['uid']>0 && $data['table']!="" && $data['table']!="tt_content" && $data['table']!="pages" && $data['pid']>0)
		{
			$methodName = "loadFile".GeneralUtility::underscoredToUpperCamelCase($data['table']);
			if(method_exists($this, $methodName))
			{
				$this->$methodName($data);
			}
			elseif($data['table']=="tt_content")
			{
				exit("tt_content");
			}
			elseif($data['table']=="pages")
			{
				exit("pages");
			}
			else
			{
				exit("no access");
			}
		}
		else {
			exit("no access");
		}

	}
}
?>