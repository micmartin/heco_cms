<?php
namespace Neulandlotsen\MooxMailer\Controller;

/**
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */

use TYPO3\CMS\Core\Messaging\FlashMessage;
 
class ScheduleController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController 
{

	/**
	 * scheduleRepository
	 *
	 * @var \Neulandlotsen\MooxMailer\Domain\Repository\ScheduleRepository
	 */
	protected $scheduleRepository;
	
	/**
	 * templateRepository
	 *
	 * @var \Neulandlotsen\MooxMailer\Domain\Repository\TemplateRepository
	 */
	protected $templateRepository;		
	
	/**
	 * extConf
	 *
	 * @var bool
	 */
	protected $extConf;
		
	/**
	 * initialize the controller
	 *
	 * @return void
	 */
	protected function initializeAction() 
	{
		parent::initializeAction();					
		$this->extConf = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Configuration\ExtensionConfiguration::class)->get('moox_mailer');
		$this->scheduleRepository = $this->objectManager->get('Neulandlotsen\\MooxMailer\\Domain\\Repository\\ScheduleRepository');
		$this->templateRepository = $this->objectManager->get('Neulandlotsen\\MooxMailer\\Domain\\Repository\\TemplateRepository');					
	}		
	
	/**
	 * action index
	 *
	 * @return void
	 */
	public function indexAction() 
	{					
		$this->view->assign('schedules', $this->scheduleRepository->findAll(false));
		$this->view->assign('action', 'show');
	}
	
	/**
	 * action add
	 *	
	 * @param array $add
	 * @return void
	 */
	public function addAction($add = array()) 
	{					
		$options = array();		
		$defaultTemplate = $this->templateRepository->findByUid(1);		
		$options[1] = "[Default] ".$defaultTemplate->getTitle();
		$templates = $this->templateRepository->findAll(false);		
		foreach($templates AS $template){
			if($template->getUid()!=1){
				$options[$template->getUid()] = $template->getTitle();			
			}
		}
		
		$years = array();
		$weeks = array();				
		for($i=1;$i<53;$i++){
			$weeks[] = $i;
		}
		if(date("W")<5){
			$startYear = date("Y")-1;
			$yearCount = 4;
		} else {
			$startYear = date("Y");
			$yearCount = 3;
		}
		for($i=$startYear;$i<=($startYear+($yearCount-1));$i++){			
			$years[] = array("year" => $i, "weeks" => $weeks);
		}
		
		if(isset($add['save']) || isset($add['saveAndClose']) ||  isset($add['saveAndNew'])){
			
			$schedule = array();
		
			foreach($add AS $key => $value){
				$key = explode("_",$key);
				if($key[0]=="templates"){
					$schedule[$key[1]][$key[2]] = $value;
				}
			}
			
			$object = $this->objectManager->get('Neulandlotsen\\MooxMailer\\Domain\\Model\\Schedule');
			$object->setTitle($add['title']);
			$object->setSchedule(serialize($schedule));
			
			$this->scheduleRepository->add($object);								
			$this->objectManager->get('TYPO3\CMS\Extbase\Persistence\PersistenceManagerInterface')->persistAll();
			
			$this->addFlashMessage(
				'', 
				'Zeitplan wurde erfolgreich gespeichert.', 
				FlashMessage::OK
			);
		}
		if(isset($add['save'])){
			$this->redirect("edit",NULL,NULL,array('uid' => $object->getUid()));
		} elseif(isset($add['saveAndClose'])){
			$this->redirect("index");
		} elseif(isset($add['saveAndNew'])){			
			$this->redirect("add");
		} else {			
			$this->view->assign('object', $add);
			$this->view->assign('years', $years);
			$this->view->assign('options', $options);
			$this->view->assign('currentYear', date("Y"));
			$this->view->assign('currentWeek', date("W"));
			$this->view->assign('action', 'add');
		}	
	}
	
	/**
	 * action edit
	 *
	 * @param int $uid
	 * @param array $edit
	 * @return void
	 */
	public function editAction($uid = 0, $edit = array())
	{					
		if($uid>0){
			
			$object = $this->scheduleRepository->findByUid($uid);
			
			if(!count($edit)){
				$edit['title'] = $object->getTitle();
				
				foreach(unserialize($object->getSchedule()) AS $year => $weeks){
					foreach($weeks AS $week => $template){
						$edit['templates_'.$year.'_'.$week] = $template;
					}
				}
				
				$edit['uid'] = $object->getUid();				
			}
			
			$options = array();		
			$defaultTemplate = $this->templateRepository->findByUid(1);		
			$options[1] = "[Default] ".$defaultTemplate->getTitle();
			$templates = $this->templateRepository->findAll(false);		
			foreach($templates AS $template){
				if($template->getUid()!=1){
					$options[$template->getUid()] = $template->getTitle();			
				}
			}
			
			$years = array();
			$weeks = array();				
			for($i=1;$i<53;$i++){
				$weeks[] = $i;
			}		
			if(date("W")<5){
				$startYear = date("Y")-1;
				$yearCount = 4;
			} else {
				$startYear = date("Y");
				$yearCount = 3;
			}
			for($i=$startYear;$i<=($startYear+($yearCount-1));$i++){			
				$years[] = array("year" => $i, "weeks" => $weeks);
			}
			
			if(isset($edit['save']) || isset($edit['saveAndClose']) ||  isset($edit['saveAndNew'])){
				
				$schedule = array();
			
				foreach($edit AS $key => $value){
					$key = explode("_",$key);
					if($key[0]=="templates"){
						$schedule[$key[1]][$key[2]] = $value;
					}
				}
				
				$object->setTitle($edit['title']);
				$object->setSchedule(serialize($schedule));
				
				$this->scheduleRepository->update($object);								
				$this->objectManager->get('TYPO3\CMS\Extbase\Persistence\PersistenceManagerInterface')->persistAll();
				
				$this->addFlashMessage(
					'', 
					'Änderungen wurden erfolgreich gespeichert.', 
					FlashMessage::OK
				);
			}
			if(isset($edit['saveAndClose'])){
				$this->redirect("index");
			} elseif(isset($edit['saveAndNew'])){
				$this->redirect("add");
			} else {
				$this->view->assign('object', $edit);
				$this->view->assign('years', $years);
				$this->view->assign('options', $options);
				$this->view->assign('currentYear', date("Y"));
				$this->view->assign('currentWeek', date("W"));
				$this->view->assign('action', 'edit');
				$this->view->assign('uid', $uid);
			}
		} else {
			$this->redirect("index");
		}
	}
	
	/**
	 * action delete
	 *	
	 * @param int $uid
	 * @return void
	 */
	public function deleteAction($uid = 0)
	{					
		if($uid>0){
		
			$object = $this->scheduleRepository->findByUid($uid);
			
			$this->scheduleRepository->remove($object);
			
			$this->objectManager->get('TYPO3\CMS\Extbase\Persistence\PersistenceManagerInterface')->persistAll();
			
			$this->addFlashMessage(
				'', 
				'Zeitplan wurde gelöscht.', 
				FlashMessage::OK
			);
						
		} 
		
		$this->redirect("index");
	}	
		
	/**
	 * Returns ext conf
	 *
	 * @return array
	 */
	public function getExtConf()
	{
		return $this->extConf;
	}

	/**
	 * Set ext conf
	 *
	 * @param array $extConf ext conf
	 * @return void
	 */
	public function setExtConf($extConf)
	{
		$this->extConf = $extConf;
	}
}
?>