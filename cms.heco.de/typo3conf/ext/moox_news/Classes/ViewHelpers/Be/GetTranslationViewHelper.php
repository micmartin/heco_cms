<?php
namespace Neulandlotsen\MooxNews\ViewHelpers\Be;

/**
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */

use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Backend\Utility\BackendUtility as BackendUtilityCore;

/**
 * ViewHelper to get translation of given news record and language
 *
 * # Example: Basic example
 * <code>
 * <n:be.getTranslation as="translation" newsItem="newsItem" language="1"/>
 * </code>
 * <output>
 * Array of translation
 * </output>
 *
 * @package TYPO3
 * @subpackage tx_mooxnews
 */

class GetTranslationViewHelper extends \TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper {

	/**
	 * Render link to news item or internal/external pages
	 *
	 * @param Neulandlotsen\MooxNews\Domain\Model\News $newsItem current news object
	 * @param int $language
	 * @param string $as	 
	 * @param bool $reverse If enabled, the iterator will start with the last element and proceed reversely
	 * @param string $iteration The name of the variable to store iteration information (index, cycle, isFirst, isLast, isEven, isOdd)
	 * @return int translations count
	 */
	public function render(Neulandlotsen\MooxNews\Domain\Model\News $newsItem, $language, $as = "translation", $reverse = FALSE, $iteration = NULL) 
	{	
		return self::renderStatic($this->arguments, $this->buildRenderChildrenClosure(), $this->renderingContext);
	}
	
	/**
	 * @param array $arguments
	 * @param \Closure $renderChildrenClosure
	 * @param \TYPO3\CMS\Fluid\Core\Rendering\RenderingContextInterface $renderingContext
	 * @return string
	 * @throws \TYPO3\CMS\Fluid\Core\ViewHelper\Exception
	 */
	static public function renderStatic(array $arguments, \Closure $renderChildrenClosure, \TYPO3Fluid\Fluid\Core\Rendering\RenderingContextInterface $renderingContext)
	{
		$templateVariableContainer = $renderingContext->getTemplateVariableContainer();
		
		$object = self::getTranslation($arguments['newsItem']->getUid(),$arguments['language']);						
		$templateVariableContainer->add($arguments['as'], $object);
		$output = $renderChildrenClosure();
		$templateVariableContainer->remove($arguments['as']);
		
		return $output;
	}		
	
	/**
	 * Get translation
	 *
	 * @param int $uid
	 * @param int $sysLanguageUid
	 * @return Neulandlotsen\MooxNews\Domain\Model\News
	 */
	public function getTranslation($uid,$sysLanguageUid) 
	{
		
		$translation = BackendUtilityCore::getRecordLocalization('tx_mooxnews_domain_model_news',$uid,$sysLanguageUid);
		if(is_array($translation[0])){
			$return = $translation[0];				
		}
		
		return $return;		
	}
}