<?php
namespace Neulandlotsen\MooxCore\ViewHelpers\Widget;

/**
 * This file is part of the "moox_core" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

use TYPO3Fluid\Fluid\Core\Rendering\RenderingContextInterface;
use TYPO3\CMS\Core\Utility\GeneralUtility;

class UriViewHelper extends \TYPO3\CMS\Fluid\ViewHelpers\Widget\UriViewHelper
{
	/**
	 * Get the URI for a non-AJAX Request.
	 *
	 * @param RenderingContextInterface $renderingContext
	 * @param array $arguments
	 * @return string the Widget URI
	 */
	protected static function getWidgetUri(RenderingContextInterface $renderingContext, array $arguments)
	{
		$excludeFromQuery = ['cHash'];
		$addQueryString = true;
		$controllerContext = $renderingContext->getControllerContext();
		$uriBuilder = $controllerContext->getUriBuilder();
		$argumentPrefix = $controllerContext->getRequest()->getArgumentPrefix();

		if($argumentPrefix)
		{
			$excludeFromQuery[] = $argumentPrefix;
			$argumentPrefixBaseVar = explode("[",$argumentPrefix)[0];
			if($argumentPrefixBaseVar)
			{
				$excludeFromQuery[] = $argumentPrefixBaseVar."[__referrer]";
				$excludeFromQuery[] = $argumentPrefixBaseVar."[__trustedProperties]";
				$overwriteDemands = GeneralUtility::_GP($argumentPrefixBaseVar)['overwriteDemand'];
				if(is_array($overwriteDemands))
				{
					if(!isset($overwriteDemands['reset']))
					{
						foreach ($overwriteDemands AS $overwriteDemand=>$value)
						{
							if($value == "" || $value=="all")
							{
								$excludeFromQuery[] = $argumentPrefixBaseVar."[overwriteDemand][".$overwriteDemand."]";
							}
						}
					}
					else
					{
						$addQueryString = false;
					}
				}
			}
		}

		$parameters = $arguments['arguments'] ?? [];
		if ($arguments['action'] ?? false) {
			$parameters['action'] = $arguments['action'];
		}
		if (($arguments['format'] ?? '') !== '') {
			$parameters['format'] = $arguments['format'];
		}

		$uriBuilder->reset()
			->setArguments([$argumentPrefix => $parameters])
			->setSection($arguments['section'])
			->setUseCacheHash($arguments['useCacheHash'])
			->setFormat($arguments['format']);

		if($addQueryString)
		{
			$uriBuilder->setAddQueryString(true)
				->setAddQueryStringMethod($arguments['addQueryStringMethod'])
				->setArgumentsToBeExcludedFromQueryString($excludeFromQuery);
		}

		return $uriBuilder->build();
	}
}
