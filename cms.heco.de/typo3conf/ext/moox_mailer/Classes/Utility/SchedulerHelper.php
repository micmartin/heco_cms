<?php
namespace Neulandlotsen\MooxMailer\Utility;

/**
 * This file is part of the "moox_mailer" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

use Neulandlotsen\MooxCore\Utility\Backend\HelperUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Persistence\QueryInterface; 

class SchedulerHelper 
{
	/**
	 * Path to the locallang file
	 * @var string
	 */
	const LLPATH = 'LLL:EXT:moox_mailer/Resources/Private/Language/locallang_scheduler.xlf:';
	
	/**
	 * @param string $selectorName selector name
	 * @param int $selected current send mode	
	 * @return	string	$selector
	 */
	public static function getMooxNewsFoldersSelector($selectorName,$selected)
	{						
		$selector = '<select name="'.$selectorName.'[]" id="task_'.$selectorName.'" multiple="multiple" class="form-control" size="5">';
		
		$selected = ($selected)?explode(",",$selected):[];

		$folders = HelperUtility::getFolders('mxnews');
		
		foreach ($folders as $folder)
		{			
			$selector .= '<option '.(in_array($folder['uid'],$selected)?'selected="selected" ':'').'value="'.$folder['uid'].'">'.$folder['title'].' ['.$folder['uid'].']</option>';
		}
								
		$selector .= '</select>';
		
		return $selector;
	}	
	
	/**
	 * @param string $selectorName selector name
	 * @param int $selected current send mode	
	 * @return	string	$selector
	 */
	public static function getFeUsersFoldersSelector($selectorName,$selected)
	{		
		$selector = '<select name="'.$selectorName.'[]" id="task_'.$selectorName.'" multiple="multiple" class="form-control" size="5">';
		
		$selected = ($selected)?explode(",",$selected):[];
		
		$folders = HelperUtility::getFolders('mxfeuser');
				
		foreach ($folders as $folder)
		{			
			$selector .= '<option '.(in_array($folder['uid'],$selected)?'selected="selected" ':'').'value="'.$folder['uid'].'">'.$folder['title'].' ['.$folder['uid'].']</option>';
		}
								
		$selector .= '</select>';
		
		return $selector;
	}
	
	/**
	 * @param string $selectorName selector name
	 * @param int $selected current send mode	
	 * @return	string	$selector
	 */
	public static function getMooxAddressFoldersSelector($selectorName,$selected)
	{						
		$selector = '<select name="'.$selectorName.'[]" id="task_'.$selectorName.'" multiple="multiple" class="form-control" size="5">';
		
		$selected = ($selected)?explode(",",$selected):[];
		
		$selector .= '<option '.(in_array(0,$selected)?'selected="selected" ':'').'value="0">Adressen ohne Ordner [0]</option>';
		
		$folders = HelperUtility::getFolders('mxaddress');
		
		foreach ($folders as $folder)
		{			
			$selector .= '<option '.(in_array($folder['uid'],$selected)?'selected="selected" ':'').'value="'.$folder['uid'].'">'.$folder['title'].' ['.$folder['uid'].']</option>';
		}
								
		$selector .= '</select>';
		
		return $selector;
	}
	
	/**
	 * @param string $selectorName selector name
	 * @param int $selected current send mode	
	 * @return	string	$selector
	 */
	public static function getFeUsersGroupsSelector($selectorName,$selected)
	{		
		$selector = '<select name="'.$selectorName.'[]" id="task_'.$selectorName.'" multiple="multiple" class="form-control" size="5">';
		
		$selected = ($selected)?explode(",",$selected):[];

		$folders = HelperUtility::getFolders('mxfeuser');
		
		if(count($folders))
		{
			$objectManager = GeneralUtility::makeInstance('TYPO3\CMS\Extbase\Object\ObjectManager');
			$frontendUserGroupRepository = $objectManager->get('Neulandlotsen\MooxMailer\Domain\Repository\FrontendUserGroupRepository');
			foreach ($folders as $folder) 
			{					
				$groups = $frontendUserGroupRepository->findByPids($folder['uid']);
				if(count($groups))
				{
					$selector .= '<optgroup label="'.$folder['title'].' ['.$folder['uid'].']">';
					foreach ($groups as $group) 
					{	
						$selector .= '<option '.(in_array($group->getUid(),$selected)?'selected="selected" ':'').'value="'.$group->getUid().'">'.$group->getTitle().' ['.$group->getUid().']</option>';
					}
				}
			}
		}
								
		$selector .= '</select>';
		
		return $selector;
	}
	
	/**
	 * @param string $selectorName selector name
	 * @param int $selected current send mode	
	 * @return	string	$selector
	 */
	public static function getMooxNewsCategorySelector($selectorName,$selected)
	{				
		$selector = '<select name="'.$selectorName.'[]" id="task_'.$selectorName.'" multiple="multiple" class="form-control" size="20">';
		
		$selected = ($selected)?explode(",",$selected):[];

		$folders = HelperUtility::getFolders('mxnews');
		
		if(count($folders))
		{		
			$objectManager = GeneralUtility::makeInstance('TYPO3\CMS\Extbase\Object\ObjectManager');
			$categoryRepository = $objectManager->get('Neulandlotsen\MooxNews\Domain\Repository\CategoryRepository');
			
			foreach ($folders as $folder) 
			{				
				$categories = $categoryRepository->findParentCategoriesByPid(
					$folder['uid'],
					['title' => QueryInterface::ORDER_ASCENDING]
				);
				if(count($categories))
				{
					$selector .= '<optgroup label="'.$folder['title'].' ['.$folder['uid'].']">';
					$selector .= self::getMooxNewsCategorySelectorOptions($categories,$selected,0);
					$selector .= '</optgroup>';
				}
			}
		}
								
		$selector .= '</select>';
		
		return $selector;
	}
	
	public static function getMooxNewsCategorySelectorOptions($categories,$selected = [], $depth = 0)
	{		
		$options = '';
		
		if(count($categories))
		{
			$objectManager = GeneralUtility::makeInstance('TYPO3\CMS\Extbase\Object\ObjectManager');
			$categoryRepository = $objectManager->get('Neulandlotsen\MooxNews\Domain\Repository\CategoryRepository');
		
			foreach($categories AS $category)
			{			
				$options .= '<option '.(in_array($category->getUid(),$selected)?'selected="selected" ':'').'value="'.$category->getUid().'">';
				for($i=0;$i<=$depth;$i++)
				{
					$options .= '&nbsp;';
				}
				$options .= $category->getTitle().' ['.$category->getUid().']';
				$options .= '</option>';			
				$children = $categoryRepository->findChildren(
					$category->getUid(),
					['title' => QueryInterface::ORDER_ASCENDING]
				);
				
				if(count($children)>0)
				{
					$options .= self::getMooxNewsCategorySelectorOptions($children,$selected,($depth+3));
				}
			}
		}

		return $options;
	}
	
	/**
	 * @param string $selectorName selector name
	 * @param int $selected current send mode	
	 * @return	string	$selector
	 */
	public static function getTemplateSelector($selectorName,$selected)
	{		
		$selector = '<select name="'.$selectorName.'" id="task_'.$selectorName.'" class="form-control">';
		
		$objectManager = GeneralUtility::makeInstance('TYPO3\CMS\Extbase\Object\ObjectManager');
		$templateRepository = $objectManager->get('Neulandlotsen\MooxMailer\Domain\Repository\TemplateRepository');	
		$scheduleRepository = $objectManager->get('Neulandlotsen\MooxMailer\Domain\Repository\ScheduleRepository');		
		
		$templates = $templateRepository->findAll(false);
		$schedules = $scheduleRepository->findAll(false);				
		
		foreach ($templates as $template)
		{				
			$selector .= '<option '.(($selected==$template->getUid())?'selected="selected" ':'').'value="'.$template->getUid().'">'.$template->getTitle().' ['.$template->getUid().']</option>';
		}
		
		if(count($schedules))
		{
			$selector .= $templates->count().'<option '.(($selected=="schedule")?'selected="selected" ':'').'value="schedule">[ '.$GLOBALS['LANG']->sL(self::LLPATH.'tx_mooxmailer_tasks_mooxnewssendtask.schedule_option').' ]</option>';
		}
								
		$selector .= '</select>';
		
		return $selector;
	}
	
	/**
	 * @param string $selectorName selector name
	 * @param int $selected current send mode	
	 * @return	string	$selector
	 */
	public static function getScheduleSelector($selectorName,$selected)
	{		
		$selector = '<select name="'.$selectorName.'" id="task_'.$selectorName.'" class="form-control">';
		
		$objectManager = GeneralUtility::makeInstance('TYPO3\CMS\Extbase\Object\ObjectManager');
		$scheduleRepository = $objectManager->get('Neulandlotsen\MooxMailer\Domain\Repository\ScheduleRepository');	
		
		$schedules = $scheduleRepository->findAll(false);
		
		$selector .= '<option value="0">'.$GLOBALS['LANG']->sL(self::LLPATH.'tx_mooxmailer_tasks_mooxnewssendtask.no_schedule_option').'</option>';
		
		foreach ($schedules as $schedule)
		{			
			$selector .= '<option '.(($selected==$schedule->getUid())?'selected="selected" ':'').'value="'.$schedule->getUid().'">'.$schedule->getTitle().' ['.$schedule->getUid().']</option>';
		}
								
		$selector .= '</select>';
		
		return $selector;
	}
	
	/**
	 * @param string $selectorName selector name
	 * @param int $selected current send mode	
	 * @return	string	$selector
	 */
	public static function getFormatSelector($selectorName,$selected)
	{		
		$selector =  '<select name="'.$selectorName.'" id="task_'.$selectorName.'" class="form-control">';
		
		$formats = [];
		$formats[] = [
			"value" => "text/html",
			"title" => "Text/HTML",
		];
		$formats[] = [
			"value" => "text/plain",
			"title" => "Text/Plain",
		];
				
		foreach ($formats as $format)
		{			
			$selector .= '<option '.(($selected==$format['value'])?'selected="selected" ':'').'value="'.$format['value'].'">'.$format['title'].'</option>';
		}
								
		$selector .= '</select>';
		
		return $selector;
	}
	
	/**
	 * @param string $selectorName selector name
	 * @param int $selected current send mode	
	 * @return	string	$selector
	 */
	public static function getModeSelector($selectorName,$selected)
	{		
		$selector =  '<select name="'.$selectorName.'" id="task_'.$selectorName.'" class="form-control">';
		
		$modes = [];
		$modes[] = [
			"value" => "daemon",
			"title" => "Daemon",
		];
		$modes[] = [
			"value" => "direct",
			"title" => "Direct",
		];
		
		foreach ($modes as $mode)
		{			
			$selector .= '<option '.(($selected==$mode['value'])?'selected="selected" ':'').'value="'.$mode['value'].'">'.$mode['title'].'</option>';
		}
								
		$selector .= '</select>';
		
		return $selector;
	}
	
	/**
	 * @param string $selectorName selector name
	 * @param int $selected current send mode	
	 * @return	string	$selector
	 */
	public static function getSendModeSelector($selectorName,$selected)
	{		
		$selector =  '<select name="'.$selectorName.'" id="task_'.$selectorName.'" class="form-control">';
		
		$modes = [];
		$modes[] = [
			"value" => "cumulated",
			"title" => "Kumulierter Versand (Alles in einer Mail)",
		];
		$modes[] = [
			"value" => "single",
			"title" => "Einzelversand (Eine Mail pro News)",
		];
		
		foreach ($modes as $mode) 
		{			
			$selector .= '<option '.(($selected==$mode['value'])?'selected="selected" ':'').'value="'.$mode['value'].'">'.$mode['title'].'</option>';
		}
								
		$selector .= '</select>';
		
		return $selector;
	}
	
	/**
	 * @param string $selectorName selector name
	 * @param int $selected current send mode	
	 * @return	string	$selector
	 */
	public static function getMooxNewsDetailPageSelector($selectorName,$selected)
	{		
		$selector = '<select name="'.$selectorName.'" id="task_'.$selectorName.'" class="form-control">';
		
		$selector .= '<option value="0">[ '.$GLOBALS['LANG']->sL(self::LLPATH.'tx_mooxmailer_tasks_mooxnewssendtask.get_from_category').' ]</option>';

		$pages = HelperUtility::getPluginPages("mooxnews_pi1");
		
		foreach ($pages as $page)
		{	
			$selector .= '<option '.(($page['uid']==$selected)?'selected="selected" ':'').'value="'.$page['uid'].'">'.$page['title'].' ['.$page['uid'].']</option>';
		}
							
		$selector .= '</select>';
		
		return $selector;		
	}
	
	/**
	 * @param string $selectorName selector name
	 * @param int $selected current send mode	
	 * @return	string	$selector
	 */
	public static function getAutoLoginUserSelector($selectorName,$selected)
	{						
		$selector = '<select name="'.$selectorName.'" id="task_'.$selectorName.'" class="form-control">';
		
		$selector .= '<option value="0">[ '.$GLOBALS['LANG']->sL(self::LLPATH.'tx_mooxmailer_tasks_mooxnewssendtask.no_auto_login').' ]</option>';
		
		$selector .= '<option value="-1" '.(($selected==-1)?'selected="selected" ':'').'>[ '.$GLOBALS['LANG']->sL(self::LLPATH.'tx_mooxmailer_tasks_mooxnewssendtask.login_with_current_user').' ]</option>';
		
		$objectManager = GeneralUtility::makeInstance('TYPO3\CMS\Extbase\Object\ObjectManager');
		$frontendUserRepository = $objectManager->get('Neulandlotsen\\MooxMailer\\Domain\\Repository\\FrontendUserRepository');
		
		$results = $frontendUserRepository->findAll(false);
		
		foreach($results AS $user){			
			$selector .= '<option '.(($user->getUid()==$selected)?'selected="selected" ':'').'value="'.$user->getUid().'">'.$user->getFirstName().' '.$user->getLastName().' ('.$user->getUsername().') ['.$user->getUid().']</option>';			
		}
								
		$selector .= '</select>';
		
		return $selector;
	}
	
	/**
	 * @param string $email email
	 * @return string $errormsg
	 */	
	public static function checkMail($email)
	{		
		$error = false;
		$errormsg = "";
		
		if(!GeneralUtility::validEmail($email))
		{
			$error 	= true;
			$errormsg .= "Address in mailbox given [".$email."] does not comply with RFC 3696 (http://tools.ietf.org/html/rfc3696)";
			return $errormsg;
		}

		if (!function_exists('checkdnsrr'))
		{		
			function checkdnsrr($host, $type)
			{				
				@exec('nslookup -type=' . $type . ' ' . $host, $output);

				foreach ($output as $line)
				{
					if (preg_match('/^' . $host . '/i', $line))
					{
						return true;
					}
				}

				return false;
			}
		}

		$host = substr(strrchr($email, '@'), 1);

		if (($checkMX = checkdnsrr($host, 'MX')) or ($checkA = checkdnsrr($host, 'A')))
		{
			return $errormsg;
		} 
		else 
		{			
			if(!$checkMX)
			{
				$errormsg .= "MX-Record check failed";
			}
			if(!$checkA)
			{
				$errormsg .= (($errormsg!="")?", ":"")."A-Record check failed";
			} 
				
			return $errormsg;
		}
	}
}