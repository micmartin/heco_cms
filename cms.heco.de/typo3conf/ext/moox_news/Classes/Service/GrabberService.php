<?php
namespace Neulandlotsen\MooxNews\Service;

/**
 * This file is part of the "moox_news" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

use Neulandlotsen\MooxNews\Service\Transliterator\Transliterator;
use TYPO3\CMS\Core\Core\Environment;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Persistence\ObjectStorage;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Database\Query\Restriction\HiddenRestriction;
use TYPO3\CMS\Core\Database\Query\Restriction\DeletedRestriction;
use TYPO3\CMS\Core\Database\Query\Restriction\BackendWorkspaceRestriction;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
	 
class GrabberService implements \TYPO3\CMS\Core\SingletonInterface
{		
	/**
     * @var string 
     */
	public static $userUploadFolder	= "/user_upload";

	/**
	 * @var string
	 */
	public static $userUploadFolderSecured	= "/user_upload_secured";
	
	/**
     * @var string 
     */	
	public static $newsFolder = "/moox_news";
	
	/**
     * @var string 
     */	
	public static $grabberFolder = "/grabber";

	/**
	 * @var array
	 */
	public static $mime_types = [
		'jpg' => 'image/jpeg',
		'png' => 'image/png',
		'gif' => 'image/gif',
		'bmp' => 'image/bmp',
		'jpeg' => 'image/jpeg',
		'pdf' => 'application/pdf',
		'doc' => 'application/msword',
		'xls' => 'application/msexcel',
		'ppt' => 'application/vnd.ms-powerpoint',
		'docx' => 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
		'xlsx' => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
		'pptx' => 'application/vnd.openxmlformats-officedocument.presentationml.presentation',
		'dot' => 'application/msword',
		'mp3' => 'audio/mpeg',
		'mp4' => 'video/mpeg',
		'mpg' => 'video/mpeg',
		'zip' => 'application/zip'
	];

	/**
	 * @var array
	 */
	public static $types = [
		'jpg' => 2,
		'jpeg' => 2,
		'png' => 2,
		'gif' => 2,
		'bmp' => 2,
		'pdf' => 1,
		'doc' => 1,
		'xls' => 1,
		'ppt' => 1,
		'docx' => 1,
		'xlsx' => 1,
		'pptx' => 1,
		'dot' => 1,
		'mp3' => 3,
		'mp4' => 4,
		'mpg' => 4,
		'zip' => 5
	];

	/**
	 * @var array
	 */
	public static $nonAutoSetFields = [
		'categories',
		'related',
		'targets',
		'images',
		'files',
		'fe_group',
		'l10n_state'
	];

	/**
	 * convert xml to array
	 * @param \SimpleXMLElement $xml
	 * @return array $output
	 */
	public function xmlToArray(\SimpleXMLElement $xml) 
	{
		$json = json_encode($xml);
		$output	= json_decode($json,TRUE);
		return $output;
	}
	
	/**
	 * encode special chars
	 * @param string $input
	 * @return array $output
	 */
	public function encode ($input)
	{	
		$charsArray = ['/ä/','/ö/','/ü/','/Ä/','/Ö/','/Ü/','/ß/','/&/'];
		$replaceArray = ['#auml;','#ouml;','#uuml;','#Auml;','#Ouml;','#Uuml;','#szlig;','#amp;'];
		$output	= preg_replace($charsArray , $replaceArray , $input);
		
		return $output;
	}
	
	/**
	 * get contents from url
	 * @param string $url
	 * @return array $header
	 */
	public function getContentsFromUrl($url)
	{
		$options = [
			CURLOPT_RETURNTRANSFER => true,     // return web page
			CURLOPT_HEADER         => false,    // don't return headers
			CURLOPT_FOLLOWLOCATION => true,     // follow redirects
			CURLOPT_ENCODING       => "",       // handle all encodings
			CURLOPT_USERAGENT      => "spider", // who am i
			CURLOPT_AUTOREFERER    => true,     // set referer on redirect
			CURLOPT_CONNECTTIMEOUT => 120,      // timeout on connect
			CURLOPT_TIMEOUT        => 120,      // timeout on response
			CURLOPT_MAXREDIRS      => 10,       // stop after 10 redirects
		];

		$ch = curl_init( $url );
		curl_setopt_array( $ch, $options );
		$content = curl_exec( $ch );
		$err = curl_errno( $ch );
		$errmsg = curl_error( $ch );
		$header = curl_getinfo( $ch );
		curl_close( $ch );

		$header['errno']   = $err;
		$header['errmsg']  = $errmsg;
		$header['content'] = $content;
		return $header;
	}
	
	/**
	 * get news action
	 * @param string $grabberRemoteUid
	 * @param string $grabberRemoteHash
	 * @param bool $forceUpdate
	 * @return string $action
	 */
	public function getAction($grabberRemoteUid,$grabberRemoteHash,$forceUpdate=false)
	{
		$extConf = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Configuration\ExtensionConfiguration::class)->get('moox_news');

		if($extConf['grabberPublicFolder'])
		{
			$grabberPublicFolder = explode(":",$extConf['grabberPublicFolder']);
			if(count($grabberPublicFolder)==3)
			{
				$storageRootFolder = $grabberPublicFolder[1];
				$grabberPublicFolder = $storageRootFolder."/".$grabberPublicFolder[2];
			}
			elseif(count($grabberPublicFolder)==1)
			{
				$storageRootFolder = "fileadmin";
				$grabberPublicFolder = $storageRootFolder."/".$grabberPublicFolder[0];
			}
		}
		if(!$grabberPublicFolder)
		{
			$grabberPublicFolder = 'fileadmin'.self::$userUploadFolder.self::$newsFolder.self::$grabberFolder;
		}

		if($extConf['grabberPrivateFolder'])
		{
			$grabberPrivateFolder = explode(":",$extConf['grabberPrivateFolder']);
			if(count($grabberPrivateFolder)==3)
			{
				$storageRootFolder = $grabberPrivateFolder[1];
				$grabberPrivateFolder = $storageRootFolder."/".$grabberPrivateFolder[2];
			}
			elseif(count($grabberPrivateFolder)==1)
			{
				$storageRootFolder = "fileadmin";
				$grabberPrivateFolder = $storageRootFolder."/".$grabberPrivateFolder[0];
			}
		}
		if(!$grabberPrivateFolder)
		{
			$grabberPublicFolder = 'fileadmin'.self::$userUploadFolderSecured.self::$newsFolder.self::$grabberFolder;
		}

		@GeneralUtility::mkdir_deep(Environment::getPublicPath() . '/'.$grabberPublicFolder);
		@GeneralUtility::mkdir_deep(Environment::getPublicPath() . '/'.$grabberPrivateFolder);

		/*
		if(is_dir(Environment::getPublicPath() . '/'.'/fileadmin'.self::$userUploadFolder))
		{
			if(!is_dir(Environment::getPublicPath() . '/'.'/fileadmin'.self::$userUploadFolder.self::$newsFolder))
			{				
				mkdir(Environment::getPublicPath() . '/'.'/fileadmin'.self::$userUploadFolder.self::$newsFolder);
			}
			if(is_dir(Environment::getPublicPath() . '/'.'/fileadmin'.self::$userUploadFolder.self::$newsFolder))
			{					
				if(!is_dir(Environment::getPublicPath() . '/'.'/fileadmin'.self::$userUploadFolder.self::$newsFolder.self::$grabberFolder))
				{
					mkdir(Environment::getPublicPath() . '/'.'/fileadmin'.self::$userUploadFolder.self::$newsFolder.self::$grabberFolder);
				}
			}		 
		}

		if(is_dir(Environment::getPublicPath() . '/'.'/fileadmin'.self::$userUploadFolderSecured))
		{
			if(!is_dir(Environment::getPublicPath() . '/'.'/fileadmin'.self::$userUploadFolderSecured.self::$newsFolder))
			{
				mkdir(Environment::getPublicPath() . '/'.'/fileadmin'.self::$userUploadFolderSecured.self::$newsFolder);
			}
			if(is_dir(Environment::getPublicPath() . '/'.'/fileadmin'.self::$userUploadFolderSecured.self::$newsFolder))
			{
				if(!is_dir(Environment::getPublicPath() . '/'.'/fileadmin'.self::$userUploadFolderSecured.self::$newsFolder.self::$grabberFolder))
				{
					mkdir(Environment::getPublicPath() . '/'.'/fileadmin'.self::$userUploadFolderSecured.self::$newsFolder.self::$grabberFolder);
				}
			}
		}*/
		
		$objectManager = GeneralUtility::makeInstance('TYPO3\CMS\Extbase\Object\ObjectManager');
		$newsRepository = $objectManager->get('Neulandlotsen\MooxNews\Domain\Repository\NewsRepository');		
		
		$news = $newsRepository->findOneByGrabberRemoteUid($grabberRemoteUid);
		
		if($news)
		{
			if($news->getGrabberRemoteHash()!="" && ($news->getGrabberRemoteHash()!=$grabberRemoteHash || $forceUpdate))
			{
				$return['action'] = "update";
				$return['uid'] = $news->getUid();
				$return['pid'] = $news->getPid();				
			} 
			elseif($news->getGrabberRemoteHash()!="" && $news->getGrabberRemoteHash()==$grabberRemoteHash)
			{
				$return['action'] 	= "skip";
			} 
			else 
			{
				$return['action'] 	= "insert";
			}
		} 
		else 
		{
			$return['action'] 	= "insert";
		}
		return $return;
	}
	
	/**
	 * copy file to server
	 * @param string $sourcefile
	 * @param string $title
	 * @param string $id
	 * @param bool $secured
	 * @return void
	 */
	public function copyFile($sourcefile,$title,$id,$secured = 0)
	{
		$extConf = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Configuration\ExtensionConfiguration::class)->get('moox_news');

		if($extConf['grabberPublicFolder'])
		{
			$grabberPublicFolder = explode(":",$extConf['grabberPublicFolder']);
			if(count($grabberPublicFolder)==3)
			{
				$storageRootFolder = $grabberPublicFolder[1];
				$grabberPublicFolder = $storageRootFolder."/".$grabberPublicFolder[2];
			}
			elseif(count($grabberPublicFolder)==1)
			{
				$storageRootFolder = "fileadmin";
				$grabberPublicFolder = $storageRootFolder."/".$grabberPublicFolder[0];
			}
		}

		if(!$grabberPublicFolder)
		{
			$grabberPublicFolder = 'fileadmin'.self::$userUploadFolder.self::$newsFolder.self::$grabberFolder;
		}

		if($extConf['grabberPrivateFolder'])
		{
			$grabberPrivateFolder = explode(":",$extConf['grabberPrivateFolder']);
			if(count($grabberPrivateFolder)==3)
			{
				$storageRootFolder = $grabberPrivateFolder[1];
				$grabberPrivateFolder = $storageRootFolder."/".$grabberPrivateFolder[2];
			}
			elseif(count($grabberPrivateFolder)==1)
			{
				$storageRootFolder = "fileadmin";
				$grabberPrivateFolder = $storageRootFolder."/".$grabberPrivateFolder[0];
			}
		}
		if(!$grabberPrivateFolder)
		{
			$grabberPrivateFolder = 'fileadmin'.self::$userUploadFolderSecured.self::$newsFolder.self::$grabberFolder;
		}

		if($secured)
		{
			$destinationfile = Environment::getPublicPath() . '/'.$grabberPrivateFolder."/".$id."_".$title;
		}
		else
		{
			$destinationfile = Environment::getPublicPath() . '/'.$grabberPublicFolder."/".$id."_".$title;
		}

		if(!file_exists($destinationfile))
		{		
			if(strtolower(substr($sourcefile,0, 4)=="http"))
			{
				$ch = curl_init();

				curl_setopt($ch, CURLOPT_POST, 0);
				curl_setopt($ch,CURLOPT_URL,$sourcefile);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

				$file = curl_exec($ch);

				curl_close($ch);

				if($file)
				{
					file_put_contents ($destinationfile, $file);
					return true;
				}
				else
				{
					return false;
				}
			}
			else
			{
				return @copy($sourcefile,$destinationfile);
			}
		}
		return true;
	}
	
	/**
	 * @param string $url
	 * @return string $filename
	 */
	public function getFileName($url)
	{
		$urlParts = explode("/",$url);		
		$filename = $urlParts[count($urlParts)-1];
		return $filename;
	}
	
	/**
	 * @param string $filename
	 * @return string $extension
	 */
	public function getFileExtension($filename)
	{
		$urlParts = explode(".",$filename);
		$extension = $urlParts[count($urlParts)-1];
		return strtolower($extension);
	}
	
	/**
	 * @param string $filename
	 * @return string $imagetitle
	 */
	public function getFileTitle($filename)
	{
		$extension = $this->getFileExtension($filename);
		$imagetitle = str_replace(".".$extension,"",$filename);
		$imagetitle = str_replace("_"," ",$imagetitle);
		return $imagetitle;
	}
	
	/**
	 * make file name
	 * @param string $title
	 * @return string $fileName
	 */
	public function makeFileName($name)
	{
		$name = htmlentities($name, ENT_QUOTES, 'UTF-8');
		$name = preg_replace('~&([a-z]{1,2})(acute|cedil|circ|grave|lig|orn|ring|slash|th|tilde|uml);~i', '$1', $name);
		$name = html_entity_decode($name, ENT_QUOTES, 'UTF-8');
		$name = preg_replace(array('~[^0-9a-z]~i', '~[ -]+~'), ' ', $name);
		
		$fileName = trim($name, ' -');
		
		return $fileName;
	}
	
	/**
	 * write grabbed news to db
	 * @param array $news	
	 * @return string $action
	 */
	public function writeNews($news)
	{
		if(is_array($GLOBALS['TCA']['tx_mooxnews_domain_model_news']['moox']['allowedGrabberFields']) && count($GLOBALS['TCA']['tx_mooxnews_domain_model_news']['moox']['allowedGrabberFields']))
		{
			if($news['title'] && $news['pid'] && $news['grabber_remote_hash'] && $news['grabber_remote_uid'])
			{
				$extConf = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Configuration\ExtensionConfiguration::class)->get('moox_news');
				$allowedFields = $GLOBALS['TCA']['tx_mooxnews_domain_model_news']['moox']['allowedGrabberFields'];

				$objectManager = GeneralUtility::makeInstance('TYPO3\CMS\Extbase\Object\ObjectManager');
				$newsRepository = $objectManager->get('Neulandlotsen\MooxNews\Domain\Repository\NewsRepository');
				$categoryRepository = $objectManager->get('Neulandlotsen\MooxNews\Domain\Repository\CategoryRepository');
				$targetRepository = $objectManager->get('Neulandlotsen\MooxNews\Domain\Repository\TargetRepository');
				$frontendUserGroupRepository = $objectManager->get('TYPO3\CMS\Extbase\Domain\Repository\FrontendUserGroupRepository');

				$timestamp = time();
				$imageErrors = false;
				$filesErrors = false;
				$imageCount = 0;
				$fileCount = 0;
				$images = [];
				$files = [];

				if(!isset($news['crdate']))
				{
					$news['crdate'] = $timestamp;
				}
				if(!isset($news['tstamp']))
				{
					$news['tstamp'] = $timestamp;
				}
				if(!isset($news['path_segment']))
				{
					$news['path_segment'] = Transliterator::urlize($news['title']);
				}
				if(!isset($news['year']) && isset($news['datetime']) && $news['datetime']>0)
				{
					$news['year'] = date("Y",$news['datetime']);
				}

				// prepare images for import
				if (in_array("fal_media",$allowedFields) && is_array($news['images']) && count($news['images']) > 0)
				{
					foreach ($news['images'] AS $grabbedimage)
					{
						$imageCount++;
						$image['sorting'] = $imageCount;
						$image['filename'] = $news['grabber_remote_uid'] . "_" . $this->getFileName($grabbedimage['src']);
						$image['extension'] = $this->getFileExtension($image['filename']);
						$image['mime_type'] = self::$mime_types[$image['extension']];
						//$image['title'] = ($grabbedimage['title'])?$grabbedimage['title']:str_replace("_", " ", $this->getFileTitle($this->getFileName($grabbedimage['src'])));
						$image['title'] = ($grabbedimage['title'])?$grabbedimage['title']:'';
						$image['caption'] = ($grabbedimage['caption'])?$grabbedimage['caption']:$grabbedimage['desc'];
						$image['alt'] = $grabbedimage['alt'];
						$image['secured'] = $grabbedimage['secured'];

						if (isset(self::$types[strtolower($image['extension'])]))
						{
							//$test = get_headers($grabbedimage['src'], 1);
							$ch = curl_init($grabbedimage['src']);
							curl_setopt($ch, CURLOPT_NOBODY, true);
							curl_exec($ch);
							$httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
							if ($httpcode == 200)
							{
								$images[] = $image;
								if (!$this->copyFile($grabbedimage['src'], $this->getFileName($grabbedimage['src']), $news['grabber_remote_uid'], $image['secured']))
								{
									$imageErrors = true;
								}
							}
						}
					}
				}

				// prepare files for import
				if (in_array("fal_related_files",$allowedFields) && is_array($news['files']) && count($news['files']) > 0)
				{
					foreach ($news['files'] AS $grabbedfile)
					{
						$fileCount++;
						$file['sorting'] = $fileCount;
						$file['filename'] = $news['grabber_remote_uid'] . "_" . $this->getFileName($grabbedfile['src']);
						$file['mime_type'] = self::$mime_types[$file['extension']];
						$file['extension'] = $this->getFileExtension($file['filename']);
						//$file['title'] = ($grabbedfile['title'])?$grabbedfile['title']:str_replace("_", " ", $this->getFileTitle($this->getFileName($grabbedfile['src'])));
						$file['title'] = ($grabbedfile['title'])?$grabbedfile['title']:'';
						$file['caption'] = ($grabbedfile['caption'])?$grabbedfile['caption']:$grabbedfile['desc'];
						$file['secured'] = $grabbedfile['secured'];

						if (isset(self::$types[strtolower($file['extension'])]))
						{
							//$test = get_headers($grabbedfile['src'], 1);
							//if ($test[0] == 'HTTP/1.1 200 OK')
							$ch = curl_init($grabbedfile['src']);
							curl_setopt($ch, CURLOPT_NOBODY, true);
							curl_exec($ch);
							$httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
							if ($httpcode == 200)
							{
								$files[] = $file;
								if (!$this->copyFile($grabbedfile['src'], $this->getFileName($grabbedfile['src']), $news['grabber_remote_uid'], $file['secured']))
								{
									$filesErrors = true;
								}
							}
						}
					}
				}

				// force update during next import
				if ($imageErrors || $filesErrors)
				{
					$news['grabber_remote_hash'] = md5($news['grabber_remote_hash'] . $timestamp);
				}

				// make path segment unique
				$queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)
					->getQueryBuilderForTable('tx_mooxnews_domain_model_news');
				$duplicates = $queryBuilder
					->select('uid')
					->from('tx_mooxnews_domain_model_news')
					->where(
						$queryBuilder->expr()->eq('path_segment', $queryBuilder->createNamedParameter($news['path_segment']))
					)
					->execute();

				if ($duplicates->fetch())
				{
					$pathSegmentBase = $news['path_segment'];
					for ($i = 1; $i < 10000; $i++)
					{
						$news['path_segment'] = $pathSegmentBase . "-" . $i;
						$duplicates = $queryBuilder
							->select('uid')
							->from('tx_mooxnews_domain_model_news')
							->where(
								$queryBuilder->expr()->eq('path_segment', $queryBuilder->createNamedParameter($news['path_segment']))
							)
							->execute();
						if (!$duplicates->fetch())
						{
							break;
						}
					}
				}

				$insertNews = $objectManager->get('Neulandlotsen\MooxNews\Domain\Model\News');

				foreach ($news AS $field => $value)
				{
					if(in_array($field,$allowedFields) && !in_array($field,self::$nonAutoSetFields))
					{
						$setMethod = "set".GeneralUtility::underscoredToUpperCamelCase($field);
						if(method_exists($insertNews, $setMethod))
						{
							$prepareMethod = "prepare".GeneralUtility::underscoredToUpperCamelCase($field);
							if(method_exists($this, $prepareMethod))
							{
								$value = $this->$prepareMethod($news);
							}
							$insertNews->$setMethod($value);

						}
					}
					elseif(in_array($field,$allowedFields) && $field=="related")
					{
						if (is_array($news['related']) && count($news['related']) && $news['related'][0] > 0)
						{
							foreach ($news['related'] AS $related)
							{
								$related = (int)trim($related);
								$addRelated = $newsRepository->findOneByImportSourceAndImportId($news['import_source'], $related);
								if ($addRelated)
								{
									$insertNews->addRelated($addRelated);
								}
							}
						}
					}
					elseif(in_array($field,$allowedFields) && $field=="categories")
					{
						if (is_array($news['categories']) && count($news['categories']) && $news['categories'][0] > 0)
						{
							$firstCategory = $categoryRepository->findByUid($news['categories'][0]);
							if ($firstCategory)
							{
								$insertNews->setSinglePid($firstCategory->getSinglePid());
							}

							foreach ($news['categories'] AS $category)
							{
								$category = trim($category);
								$addCategory = $categoryRepository->findByUid($category);
								if ($addCategory)
								{
									$insertNews->addCategory($addCategory);
								}
							}
						}
					}
					elseif(in_array($field,$allowedFields) && $field=="targets")
					{
						if (is_array($news['targets']))
						{
							foreach ($news['targets'] AS $target)
							{
								$target = trim($target);
								$addTarget = $targetRepository->findByUid($target);
								if ($addTarget)
								{
									$insertNews->addTarget($addTarget);
								}
							}
						}
					}
					elseif(in_array($field,$allowedFields) && $field=="fe_group")
					{
						if (is_array($news['fe_group']))
						{
							$news['fe_group'] = implode(",", $news['fe_group']);
							$insertNews->setFeGroup($news['fe_group']);
						}
					}
					elseif(in_array($field,$allowedFields) && $field=="l10n_state")
					{
						if (is_array($news['l10n_state']))
						{
							$l10nState = [];
							foreach($news['l10n_state'] AS $field => $state)
							{
								if(in_array($field,$allowedFields) && in_array($state,['custom','parent']))
								{
									$l10nState[$field] = $state;
								}
							}
							if(count($l10nState))
							{
								$insertNews->setL10nState(json_encode($l10nState));
							}
							
						}
					}
				}

				/*
				if (ExtensionManagementUtility::isLoaded('moox_comment')) {
					$insertNews->setRatingActive(0);
					$insertNews->setReviewActive(0);
					$insertNews->setCommentActive(0);
				}
				if (ExtensionManagementUtility::isLoaded('moox_news_twitter')) {
					$insertNews->setPushToTwitterText($news['push_to_twitter_text']);
				}
				if (ExtensionManagementUtility::isLoaded('moox_news_geoinfo')) {
					$insertNews->setMooxNewsGeoInfoVenue($news['moox_news_geoinfo_venue']);
					$insertNews->setMooxNewsGeoInfoUrl($news['moox_news_geoinfo_url']);
					$insertNews->setMooxNewsGeoInfoStreet($news['moox_news_geoinfo_street']);
					$insertNews->setMooxNewsGeoInfoZip($news['moox_news_geoinfo_zip']);
					$insertNews->setMooxNewsGeoInfoCity($news['moox_news_geoinfo_city']);
				}
				if (ExtensionManagementUtility::isLoaded('moox_mailer')) {
					$insertNews->setMailerTitle($news['mailer_title']);
					$insertNews->setMailerTeaser($news['mailer_teaser']);
				}

				if (ExtensionManagementUtility::isLoaded('moox_news_seminar') && $news['type'] == "moox_news_seminar") {

					$insertNews->setDatetime(($news['moox_news_seminar_startdate'] > 0) ? $news['moox_news_seminar_startdate'] : '');
					$insertNews->setYear(($news['moox_news_seminar_startdate'] > 0) ? date("Y", $news['moox_news_seminar_startdate']) : '');
					$insertNews->setMooxNewsSeminarStartdate($news['moox_news_seminar_startdate']);
					$insertNews->setMooxNewsSeminarEnddate($news['moox_news_seminar_enddate']);
					$insertNews->setMooxNewsSeminarPrice($news['moox_news_seminar_price']);
					$insertNews->setMooxNewsSeminarPriceInfo($news['moox_news_seminar_price_info']);
					$insertNews->setMooxNewsSeminarSubscriberMin($news['moox_news_seminar_subscriber_min']);
					$insertNews->setMooxNewsSeminarSubscriberMax($news['moox_news_seminar_subscriber_max']);
					$insertNews->setMooxNewsSeminarRegisterDeadline($news['moox_news_seminar_register_deadline']);
					$insertNews->setMooxNewsSeminarSpeakers($news['moox_news_seminar_speakers']);

					if ($news['moox_news_seminar_startdate'] > 0) {
						$index = [];
						$index[] = date("d.m.Y", $news['moox_news_seminar_startdate']);
						$index[] = date("j.n.Y", $news['moox_news_seminar_startdate']);
						$index[] = date("d.m.y", $news['moox_news_seminar_startdate']);
						$index[] = date("j.n.y", $news['moox_news_seminar_startdate']);
						$index[] = date("Y-m-d", $news['moox_news_seminar_startdate']);
						$index[] = date("y-m-d", $news['moox_news_seminar_startdate']);
						$index[] = date("Y-n-j", $news['moox_news_seminar_startdate']);
						$index[] = date("y-n-j", $news['moox_news_seminar_startdate']);
						$insertNews->setMooxNewsSeminarSearchIndex(implode(" ", $index));
					}

					if (ExtensionManagementUtility::isLoaded('moox_news_seminar_chemie')) {
						$insertNews->setMooxNewsSeminarSubtitle($news['moox_news_seminar_subtitle']);
						$insertNews->setMooxNewsSeminarInfo($news['moox_news_seminar_info']);
						$insertNews->setMooxNewsSeminarTarget($news['moox_news_seminar_target']);
						$insertNews->setMooxNewsSeminarPrice2($news['moox_news_seminar_price_2']);
						$insertNews->setMooxNewsSeminarPrice3($news['moox_news_seminar_price_3']);
						$insertNews->setMooxNewsSeminarPrice4($news['moox_news_seminar_price_4']);
						$insertNews->setMooxNewsSeminarTime1($news['moox_news_seminar_time_1']);
						$insertNews->setMooxNewsSeminarTime2($news['moox_news_seminar_time_2']);
						$insertNews->setMooxNewsSeminarTime3($news['moox_news_seminar_time_3']);
						$insertNews->setMooxNewsSeminarOrganizer($news['moox_news_seminar_organizer']);
						$insertNews->setMooxNewsSeminarListpoint1($news['moox_news_seminar_listpoint_1']);
						$insertNews->setMooxNewsSeminarListpoint2($news['moox_news_seminar_listpoint_2']);
						$insertNews->setMooxNewsSeminarListpoint3($news['moox_news_seminar_listpoint_3']);
						$insertNews->setMooxNewsSeminarListpoint4($news['moox_news_seminar_listpoint_4']);
						$insertNews->setMooxNewsSeminarListpoint5($news['moox_news_seminar_listpoint_5']);
						$insertNews->setMooxNewsSeminarListpoint6($news['moox_news_seminar_listpoint_6']);
						//$insertNews->setMooxNewsSeminarSpeaker1($news['moox_news_seminar_speaker_1']);
						//$insertNews->setMooxNewsSeminarSpeaker2($news['moox_news_seminar_speaker_2']);
						//$insertNews->setMooxNewsSeminarSpeaker3($news['moox_news_seminar_speaker_3']);
						$insertNews->setMooxNewsSeminarBiwecon($news['moox_news_seminar_biwecon']);
					}

					if (ExtensionManagementUtility::isLoaded('moox_news_seminar_papierzentrum')) {
						$insertNews->setMooxNewsSeminarId($news['moox_news_seminar_id']);
						$insertNews->setMooxNewsSeminarFees($news['moox_news_seminar_fees']);
						$insertNews->setMooxNewsSeminarLead($news['moox_news_seminar_lead']);
						$insertNews->setMooxNewsSeminarAccommodation($news['moox_news_seminar_accommodation']);
						$insertNews->setMooxNewsSeminarType($news['moox_news_seminar_type']);
					}
				}

				*/

				if (is_object($firstCategory)) {

					$categoryFeGroups = $firstCategory->getFeGroup();
					$oldItemGroups = $news['fe_group'];

					if ($categoryFeGroups) {
						if ($extConf['autoSetFeGroupOnSave'] > 0) {
							if (!$extConf['autoSetFeGroupOnSave'] || in_array($oldItemGroups, [-1, -2])) {
								if ($oldItemGroups != $categoryFeGroups) {
									$insertNews->setFeGroup($categoryFeGroups);
								}
							} else {
								$itemFeGroups = [];
								if ($oldItemGroups != "") {
									$itemFeGroups = explode(",", $oldItemGroups);
								}
								if ($categoryFeGroups != "") {
									$categoryFeGroups = explode(",", $categoryFeGroups);
								} else {
									$categoryFeGroups = [];
								}
								$newItemFeGroups = $categoryFeGroups;
								foreach ($itemFeGroups AS $group) {
									if (!in_array($group, $newItemFeGroups)) {
										$newItemFeGroups[] = $group;
									}
								}

								$newItemFeGroups = implode(",", $newItemFeGroups);
								if ($oldItemGroups != $newItemFeGroups) {
									$insertNews->setFeGroup($newItemFeGroups);
								}
							}
						}
					}

					if ($extConf['autoSetSinglePidOnSave'] > 0) {
						if ($insertNews->getSinglePid() != $firstCategory->getSinglePid()) {
							$insertNews->setSinglePid($firstCategory->getSinglePid());
						}
					}
				}

				$newsRepository->add($insertNews);
				$objectManager->get('TYPO3\CMS\Extbase\Persistence\PersistenceManagerInterface')->persistAll();

				$insertNews->setHash($insertNews->getUid().((is_int($insertNews->getCrdate()))?$insertNews->getCrdate():$insertNews->getCrdate()->format("U")).$GLOBALS['TYPO3_CONF_VARS']['SYS']['encryptionKey']);

				$newsRepository->add($insertNews);
				$objectManager->get('TYPO3\CMS\Extbase\Persistence\PersistenceManagerInterface')->persistAll();

				if (in_array("fal_media",$allowedFields))
				{
					$this->writeImagesFal($insertNews, $news, $images);
				}
				if (in_array("fal_related_files",$allowedFields))
				{
					$this->writeFilesFal($insertNews, $news, $files);
				}
			}
		}
	}
	
	/**
	 * update grabbed news in db
	 * @param array $news	
	 * @return string $action
	 */
	public function updateNews($news)
	{
		if(is_array($GLOBALS['TCA']['tx_mooxnews_domain_model_news']['moox']['allowedGrabberFields']) && count($GLOBALS['TCA']['tx_mooxnews_domain_model_news']['moox']['allowedGrabberFields']))
		{
			if ($news['uid'] > 0)
			{
				$extConf = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Configuration\ExtensionConfiguration::class)->get('moox_news');
				$allowedFields = $GLOBALS['TCA']['tx_mooxnews_domain_model_news']['moox']['allowedGrabberFields'];
				unset($allowedFields['crdate']);

				$objectManager = GeneralUtility::makeInstance('TYPO3\CMS\Extbase\Object\ObjectManager');
				$newsRepository = $objectManager->get('Neulandlotsen\MooxNews\Domain\Repository\NewsRepository');
				$categoryRepository = $objectManager->get('Neulandlotsen\MooxNews\Domain\Repository\CategoryRepository');
				$targetRepository = $objectManager->get('Neulandlotsen\MooxNews\Domain\Repository\TargetRepository');
				$frontendUserGroupRepository = $objectManager->get('TYPO3\CMS\Extbase\Domain\Repository\FrontendUserGroupRepository');

				$updateNews = $newsRepository->findByUid($news['uid'], false);

				if ($updateNews)
				{
					$timestamp = time();
					$imageErrors = false;
					$filesErrors = false;
					$imageCount = 0;
					$fileCount = 0;
					$images = [];
					$files = [];

					if(!isset($news['year']) && isset($news['datetime']) && $news['datetime']>0)
					{
						$news['year'] = date("Y",$news['datetime']);
					}

					// prepare images for import
					if (in_array("fal_media",$allowedFields) && is_array($news['images']) && count($news['images']) > 0)
					{
						foreach ($news['images'] AS $grabbedimage)
						{
							$imageCount++;
							$image['sorting'] = $imageCount;
							$image['filename'] = $news['grabber_remote_uid'] . "_" . $this->getFileName($grabbedimage['src']);
							$image['extension'] = $this->getFileExtension($image['filename']);
							$image['mime_type'] = self::$mime_types[$image['extension']];
							//$image['title'] = ($grabbedimage['title'])?$grabbedimage['title']:str_replace("_", " ", $this->getFileTitle($this->getFileName($grabbedimage['src'])));
							$image['title'] = ($grabbedimage['title'])?$grabbedimage['title']:'';
							$image['caption'] = ($grabbedimage['caption'])?$grabbedimage['caption']:$grabbedimage['desc'];
							$image['alt'] = $grabbedimage['alt'];
							$image['secured'] = $grabbedimage['secured'];

							if (isset(self::$types[strtolower($image['extension'])]))
							{
								//$test = get_headers($grabbedimage['src'], 1);
								$ch = curl_init($grabbedimage['src']);
								curl_setopt($ch, CURLOPT_NOBODY, true);
								curl_exec($ch);
								$httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
								if ($httpcode == 200)
								{
									$images[] = $image;
									if (!$this->copyFile($grabbedimage['src'], $this->getFileName($grabbedimage['src']), $news['grabber_remote_uid'], $image['secured']))
									{
										$imageErrors = true;
									}
								}
							}
						}
					}

					// prepare files for import
					if (in_array("fal_related_files",$allowedFields) && is_array($news['files']) && count($news['files']) > 0)
					{
						foreach ($news['files'] AS $grabbedfile)
						{
							$fileCount++;
							$file['sorting'] = $fileCount;
							$file['filename'] = $news['grabber_remote_uid'] . "_" . $this->getFileName($grabbedfile['src']);
							$file['mime_type'] = self::$mime_types[$file['extension']];
							$file['extension'] = $this->getFileExtension($file['filename']);
							//$file['title'] = ($grabbedfile['title'])?$grabbedfile['title']:str_replace("_", " ", $this->getFileTitle($this->getFileName($grabbedfile['src'])));
							$file['title'] = ($grabbedfile['title'])?$grabbedfile['title']:'';
							$file['caption'] = ($grabbedfile['caption'])?$grabbedfile['caption']:$grabbedfile['desc'];
							$file['secured'] = $grabbedfile['secured'];

							if (isset(self::$types[strtolower($file['extension'])]))
							{
								//$test = get_headers($grabbedfile['src'], 1);
								$ch = curl_init($grabbedfile['src']);
								curl_setopt($ch, CURLOPT_NOBODY, true);
								curl_exec($ch);
								$httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
								if ($httpcode == 200)
								{
									$files[] = $file;
									if (!$this->copyFile($grabbedfile['src'], $this->getFileName($grabbedfile['src']), $news['grabber_remote_uid'], $file['secured']))
									{
										$filesErrors = true;
									}
								}
							}
						}
					}

					// force update during next import
					if ($imageErrors || $filesErrors)
					{
						$news['grabber_remote_hash'] = md5($news['grabber_remote_hash'] . $timestamp);
					}

					if ($updateNews->getPathSegment() != '' || is_null($updateNews->getPathSegment()))
					{
						$news['path_segment'] = Transliterator::urlize($news['title']);

						$queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)
							->getQueryBuilderForTable('tx_mooxnews_domain_model_news');

						$duplicates = $queryBuilder
							->select('uid')
							->from('tx_mooxnews_domain_model_news')
							->where(
								$queryBuilder->expr()->eq('path_segment', $queryBuilder->createNamedParameter($news['path_segment']))
							)
							->execute();

						if ($duplicates->fetch())
						{
							$pathSegmentBase = $news['path_segment'];
							for ($i = 1; $i < 10000; $i++)
							{
								$news['path_segment'] = $pathSegmentBase . "-" . $i;
								$duplicates = $queryBuilder
									->select('uid')
									->from('tx_mooxnews_domain_model_news')
									->where(
										$queryBuilder->expr()->eq('path_segment', $queryBuilder->createNamedParameter($news['path_segment']))
									)
									->execute();
								if (!$duplicates->fetch())
								{
									break;
								}

							}
						}
					}

					foreach ($news AS $field => $value)
					{
						if(in_array($field,$allowedFields) && !in_array($field,self::$nonAutoSetFields))
						{
							$setMethod = "set".GeneralUtility::underscoredToUpperCamelCase($field);
							if(method_exists($updateNews, $setMethod))
							{
								$prepareMethod = "prepare".GeneralUtility::underscoredToUpperCamelCase($field);
								if(method_exists($this, $prepareMethod))
								{
									$value = $this->$prepareMethod($news);
								}
								$updateNews->$setMethod($value);

							}
						}
						elseif(in_array($field,$allowedFields) && $field=="related")
						{
							//$removeRelated = $updateNews->getRelated();
							//$updateNews->getRelated()->removeAll($removeRelated);

							$updateNews->setRelated(new \TYPO3\CMS\Extbase\Persistence\ObjectStorage());

							if (is_array($news['related']) && count($news['related']) && $news['related'][0] > 0)
							{
								foreach ($news['related'] AS $related)
								{
									$related = (int)trim($related);
									$addRelated = $newsRepository->findOneByImportSourceAndImportId($news['import_source'], $related);
									if ($addRelated)
									{
										$updateNews->addRelated($addRelated);
									}
								}
							}
						}
						elseif(in_array($field,$allowedFields) && $field=="categories")
						{
							//$removeCategories = $updateNews->getCategories();
							//$updateNews->getCategories()->removeAll($removeCategories);

							$updateNews->setCategories(new \TYPO3\CMS\Extbase\Persistence\ObjectStorage());

							if (is_array($news['categories']) && count($news['categories']) && $news['categories'][0] > 0)
							{
								$firstCategory = $categoryRepository->findByUid($news['categories'][0]);
								if ($firstCategory)
								{
									$updateNews->setSinglePid($firstCategory->getSinglePid());
								}

								foreach ($news['categories'] AS $category)
								{
									$category = trim($category);
									$addCategory = $categoryRepository->findByUid($category);
									if ($addCategory)
									{
										$updateNews->addCategory($addCategory);
									}
								}
							}
						}
						elseif(in_array($field,$allowedFields) && $field=="targets")
						{
							$updateNews->setTargets(new ObjectStorage());

							if (is_array($news['targets']))
							{
								foreach ($news['targets'] AS $target)
								{
									$target = trim($target);
									$addTarget = $targetRepository->findByUid($target);
									if ($addTarget)
									{
										$updateNews->addTarget($addTarget);
									}
								}
							}
						}
						elseif(in_array($field,$allowedFields) && $field=="fe_group")
						{
							$updateNews->setFeGroup("");

							if (is_array($news['fe_group']))
							{
								$news['fe_group'] = implode(",", $news['fe_group']);
								$updateNews->setFeGroup($news['fe_group']);
							}
						}
					}

					/*
					if (ExtensionManagementUtility::isLoaded('moox_news_twitter')) {
						$updateNews->setPushToTwitterText($news['push_to_twitter_text']);
					}

					if (ExtensionManagementUtility::isLoaded('moox_mailer')) {
						$updateNews->setMailerTitle($news['mailer_title']);
						$updateNews->setMailerTeaser($news['mailer_teaser']);
					}

					if (ExtensionManagementUtility::isLoaded('moox_news_seminar') && $news['type'] == "moox_news_seminar") {
						$updateNews->setDatetime(($news['moox_news_seminar_startdate'] > 0) ? $news['moox_news_seminar_startdate'] : '');
						$updateNews->setYear(($news['moox_news_seminar_startdate'] > 0) ? date("Y", $news['moox_news_seminar_startdate']) : '');
						$updateNews->setMooxNewsSeminarStartdate($news['moox_news_seminar_startdate']);
						$updateNews->setMooxNewsSeminarEnddate($news['moox_news_seminar_enddate']);
						$updateNews->setMooxNewsSeminarPrice($news['moox_news_seminar_price']);
						$updateNews->setMooxNewsSeminarPriceInfo($news['moox_news_seminar_price_info']);
						$updateNews->setMooxNewsSeminarSubscriberMin($news['moox_news_seminar_subscriber_min']);
						$updateNews->setMooxNewsSeminarSubscriberMax($news['moox_news_seminar_subscriber_max']);
						$updateNews->setMooxNewsSeminarRegisterDeadline($news['moox_news_seminar_register_deadline']);
						$updateNews->setMooxNewsSeminarSpeakers($news['moox_news_seminar_speakers']);

						if ($news['moox_news_seminar_startdate'] > 0) {
							$index = [];
							$index[] = date("d.m.Y", $news['moox_news_seminar_startdate']);
							$index[] = date("j.n.Y", $news['moox_news_seminar_startdate']);
							$index[] = date("d.m.y", $news['moox_news_seminar_startdate']);
							$index[] = date("j.n.y", $news['moox_news_seminar_startdate']);
							$index[] = date("Y-m-d", $news['moox_news_seminar_startdate']);
							$index[] = date("y-m-d", $news['moox_news_seminar_startdate']);
							$index[] = date("Y-n-j", $news['moox_news_seminar_startdate']);
							$index[] = date("y-n-j", $news['moox_news_seminar_startdate']);
							$updateNews->setMooxNewsSeminarSearchIndex(implode(" ", $index));
						}

						if (ExtensionManagementUtility::isLoaded('moox_news_seminar_chemie')) {
							$updateNews->setMooxNewsSeminarSubtitle($news['moox_news_seminar_subtitle']);
							$updateNews->setMooxNewsSeminarInfo($news['moox_news_seminar_info']);
							$updateNews->setMooxNewsSeminarTarget($news['moox_news_seminar_target']);
							$updateNews->setMooxNewsSeminarPrice2($news['moox_news_seminar_price_2']);
							$updateNews->setMooxNewsSeminarPrice3($news['moox_news_seminar_price_3']);
							$updateNews->setMooxNewsSeminarPrice4($news['moox_news_seminar_price_4']);
							$updateNews->setMooxNewsSeminarTime1($news['moox_news_seminar_time_1']);
							$updateNews->setMooxNewsSeminarTime2($news['moox_news_seminar_time_2']);
							$updateNews->setMooxNewsSeminarTime3($news['moox_news_seminar_time_3']);
							$updateNews->setMooxNewsSeminarOrganizer($news['moox_news_seminar_organizer']);
							$updateNews->setMooxNewsSeminarListpoint1($news['moox_news_seminar_listpoint_1']);
							$updateNews->setMooxNewsSeminarListpoint2($news['moox_news_seminar_listpoint_2']);
							$updateNews->setMooxNewsSeminarListpoint3($news['moox_news_seminar_listpoint_3']);
							$updateNews->setMooxNewsSeminarListpoint4($news['moox_news_seminar_listpoint_4']);
							$updateNews->setMooxNewsSeminarListpoint5($news['moox_news_seminar_listpoint_5']);
							$updateNews->setMooxNewsSeminarListpoint6($news['moox_news_seminar_listpoint_6']);
							$updateNews->setMooxNewsSeminarBiwecon($news['moox_news_seminar_biwecon']);
						}

						if (ExtensionManagementUtility::isLoaded('moox_news_seminar_papierzentrum')) {
							$updateNews->setMooxNewsSeminarId($news['moox_news_seminar_id']);
							$updateNews->setMooxNewsSeminarFees($news['moox_news_seminar_fees']);
							$updateNews->setMooxNewsSeminarLead($news['moox_news_seminar_lead']);
							$updateNews->setMooxNewsSeminarAccommodation($news['moox_news_seminar_accommodation']);
							$updateNews->setMooxNewsSeminarType($news['moox_news_seminar_type']);
						}
					}

					if (ExtensionManagementUtility::isLoaded('moox_news_geoinfo')) {
						$updateNews->setMooxNewsGeoInfoVenue($news['moox_news_geoinfo_venue']);
						$updateNews->setMooxNewsGeoInfoUrl($news['moox_news_geoinfo_url']);
						$updateNews->setMooxNewsGeoInfoStreet($news['moox_news_geoinfo_street']);
						$updateNews->setMooxNewsGeoInfoZip($news['moox_news_geoinfo_zip']);
						$updateNews->setMooxNewsGeoInfoCity($news['moox_news_geoinfo_city']);
					}

					*/

					if (is_object($firstCategory)) {

						$categoryFeGroups = $firstCategory->getFeGroup();
						$oldItemGroups = $news['fe_group'];

						if ($categoryFeGroups) {
							if ($extConf['autoSetFeGroupOnSave'] > 0) {
								if (!$extConf['autoSetFeGroupOnSave'] || in_array($oldItemGroups, [-1, -2])) {
									if ($oldItemGroups != $categoryFeGroups) {
										$updateNews->setFeGroup($categoryFeGroups);
									}
								} else {
									$itemFeGroups = [];
									if ($oldItemGroups != "") {
										$itemFeGroups = explode(",", $oldItemGroups);
									}
									if ($categoryFeGroups != "") {
										$categoryFeGroups = explode(",", $categoryFeGroups);
									} else {
										$categoryFeGroups = [];
									}
									$newItemFeGroups = $categoryFeGroups;
									foreach ($itemFeGroups AS $group) {
										if (!in_array($group, $newItemFeGroups)) {
											$newItemFeGroups[] = $group;
										}
									}

									$newItemFeGroups = implode(",", $newItemFeGroups);
									if ($oldItemGroups != $newItemFeGroups) {
										$updateNews->setFeGroup($newItemFeGroups);
									}
								}
							}
						}

						if ($extConf['autoSetSinglePidOnSave'] > 0) {
							if ($updateNews->getSinglePid() != $firstCategory->getSinglePid()) {
								$updateNews->setSinglePid($firstCategory->getSinglePid());
							}
						}
					}

					$updateNews->setHash($updateNews->getUid().((is_int($updateNews->getCrdate()))?$updateNews->getCrdate():$updateNews->getCrdate()->format("U")).$GLOBALS['TYPO3_CONF_VARS']['SYS']['encryptionKey']);

					$newsRepository->update($updateNews);

					$objectManager->get('TYPO3\CMS\Extbase\Persistence\PersistenceManagerInterface')->persistAll();

					if (in_array("fal_media",$allowedFields))
					{
						$this->writeImagesFal($updateNews, $news, $images);
					}
					if (in_array("fal_related_files",$allowedFields))
					{
						$this->writeFilesFal($updateNews, $news, $files);
					}
				}
			}
		}
	}
	
	/**
	 * write images of grabbed news to db (FAL)
	 * @param Neulandlotsen\MooxNews\Domain\Model\News $newNews	
	 * @param array $news
	 * @param array $images
	 */
	public function writeImagesFal($newsObj,$news,$images)
	{
		$extConf = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Configuration\ExtensionConfiguration::class)->get('moox_news');

		if($extConf['grabberPublicFolder'])
		{
			$grabberPublicFolder = explode(":",$extConf['grabberPublicFolder']);
			if(count($grabberPublicFolder)==3)
			{
				$publicStorageUid = $grabberPublicFolder[0];
				$publicStorageRootFolder = $grabberPublicFolder[1];
				$grabberPublicFolder = $grabberPublicFolder[2];
			}
			elseif(count($grabberPublicFolder)==1)
			{
				$publicStorageUid = 1;
				$publicStorageRootFolder = "fileadmin";
				$grabberPublicFolder = $grabberPublicFolder[0];
			}
		}
		if(!$grabberPublicFolder)
		{
			$publicStorageUid = 1;
			$publicStorageRootFolder = "fileadmin";
			$grabberPublicFolder = self::$userUploadFolder.self::$newsFolder.self::$grabberFolder;
		}

		if($extConf['grabberPrivateFolder'])
		{
			$grabberPrivateFolder = explode(":",$extConf['grabberPrivateFolder']);
			if(count($grabberPrivateFolder)==3)
			{
				$privateStorageUid = $grabberPrivateFolder[0];
				$privateStorageRootFolder = $grabberPrivateFolder[1];
				$grabberPrivateFolder = $privateStorageRootFolder."/".$grabberPrivateFolder[2];
			}
			elseif(count($grabberPrivateFolder)==1)
			{
				$privateStorageUid = 1;
				$privateStorageRootFolder = "fileadmin";
				$grabberPrivateFolder = $grabberPrivateFolder[0];
			}
		}
		if(!$grabberPrivateFolder)
		{
			$privateStorageUid = 1;
			$privateStorageRootFolder = "fileadmin";
			$grabberPrivateFolder = self::$userUploadFolderSecured.self::$newsFolder.self::$grabberFolder;
		}

		$types = self::$types;

		$queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)
			->getQueryBuilderForTable('sys_file_reference');

		$queryBuilder
			->getRestrictions()
			->removeAll();

		/*
		$filesToRemove = $queryBuilder
			->select('uid_local')
			->from('sys_file_reference')
			->where(
				$queryBuilder->expr()->eq('fieldname', $queryBuilder->createNamedParameter('fal_media', \PDO::PARAM_STR)),
				$queryBuilder->expr()->eq('tablenames', $queryBuilder->createNamedParameter('tx_mooxnews_domain_model_news', \PDO::PARAM_STR)),
				$queryBuilder->expr()->eq('import_source', $queryBuilder->createNamedParameter($news['import_source'], \PDO::PARAM_STR)),
				$queryBuilder->expr()->eq('import_id', $queryBuilder->createNamedParameter($news['import_id'], \PDO::PARAM_STR))
			)
			->execute();
		*/

		$queryBuilder
			->delete('sys_file_reference')
			->where(
				$queryBuilder->expr()->eq('fieldname', $queryBuilder->createNamedParameter('fal_media', \PDO::PARAM_STR)),
				$queryBuilder->expr()->eq('tablenames', $queryBuilder->createNamedParameter('tx_mooxnews_domain_model_news', \PDO::PARAM_STR)),
				$queryBuilder->expr()->eq('import_source', $queryBuilder->createNamedParameter($news['import_source'], \PDO::PARAM_STR)),
				$queryBuilder->expr()->eq('import_id', $queryBuilder->createNamedParameter($news['import_id'], \PDO::PARAM_STR))
			)
			->execute();

		/*
		while($file = $filesToRemove->fetch())
		{
			$queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)
				->getQueryBuilderForTable('sys_file');

			$queryBuilder
				->delete('sys_file')
				->where(
					$queryBuilder->expr()->eq('uid', $queryBuilder->createNamedParameter($file['uid_local'], \PDO::PARAM_INT))
				)
				->execute();

			$queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)
				->getQueryBuilderForTable('sys_file_metadata');

			$queryBuilder
				->delete('sys_file_metadata')
				->where(
					$queryBuilder->expr()->eq('uid', $queryBuilder->createNamedParameter($file['uid_local'], \PDO::PARAM_INT))
				)
				->execute();
		}
		*/
		
		$sorting = 1;
		
		$imageCnt = 0;
		
		foreach($images AS $image)
		{
			if($image['secured'])
			{
				$storageRootFolder = $privateStorageRootFolder;
				$storageUid = $privateStorageUid;
				$uploadFolder = "/".$grabberPrivateFolder;
			}
			else
			{
				$storageRootFolder = $publicStorageRootFolder;
				$storageUid = $publicStorageUid;
				$uploadFolder = "/".$grabberPublicFolder;
			}
			$filepath = Environment::getPublicPath() . '/'.$storageRootFolder.$uploadFolder."/".$image['filename'];

			if(file_exists($filepath))
			{
				$image['cruser_id'] = $newsObj->getCruserId();
				$image['sys_language_uid'] = $news['sys_language_uid'];
				$image['import_id'] = $news['import_id'];
				$image['import_source'] = $news['import_source'];
				$image['pid'] = $news['pid'];
				$image['storage'] = ($news['file_storage'])?$news['file_storage']:$storageUid;
				$image['name'] = $image['filename'];
				$image['type'] = $types[strtolower($image['extension'])];
				$image['identifier'] = $uploadFolder."/".$image['name'];
				$image['identifier_hash'] = sha1($image['identifier']);
				$image['folder_hash'] = sha1($uploadFolder);
				$image['extension'] = strtolower($image['extension']);
				$image['mime_type'] = $image['mime_type'];
				$image['name'] = $image['filename'];
				$image['sha1'] = sha1_file(Environment::getPublicPath() . '/'.$storageRootFolder.$image['identifier']);
				$image['size'] = filesize(Environment::getPublicPath() . '/'.$storageRootFolder.$image['identifier']);
				$image['hidden'] = 0;
				
				$imageInfo = getimagesize(Environment::getPublicPath() . '/'.$storageRootFolder.$image['identifier']);
				
				$image['width']	= $imageInfo[0];
				$image['height'] = $imageInfo[1];
				
				$time = time();
				
				$insertFile = [];
				$insertFile['uid'] = NULL;
				$insertFile['pid'] = 0;
				$insertFile['tstamp'] = $time;
				$insertFile['storage'] = $image['storage'];
				$insertFile['type'] = $image['type'];
				$insertFile['identifier'] = $image['identifier'];
				$insertFile['identifier_hash'] = $image['identifier_hash'];
				$insertFile['folder_hash'] = $image['folder_hash'];
				$insertFile['extension'] = $image['extension'];
				$insertFile['mime_type'] = $image['mime_type']?$image['mime_type']:'unknown';
				$insertFile['name'] = $image['name'];
				$insertFile['sha1'] = $image['sha1'];
				$insertFile['size'] = $image['size'];
				$insertFile['creation_date'] = $time;
				$insertFile['modification_date'] = $time;
				$insertFile['metadata'] = 0;

				$queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)
					->getQueryBuilderForTable('sys_file');

				$queryBuilder
					->getRestrictions()
					->removeAll();

				$existingFile = $queryBuilder
					->select('uid')
					->from('sys_file')
					->where(
						$queryBuilder->expr()->eq('identifier', $queryBuilder->createNamedParameter($insertFile['identifier'], \PDO::PARAM_STR))
					)
					->execute();

				if($existingFile = $existingFile->fetch())
				{
					$lastInsertId = $existingFile['uid'];
				}
				else
				{
					$connectionPool = GeneralUtility::makeInstance(ConnectionPool::class);
					$databaseConnectionForSysFile = $connectionPool->getConnectionForTable('sys_file');
					$databaseConnectionForSysFile->insert(
						'sys_file',
						$insertFile
					);

					$lastInsertId = (int)$databaseConnectionForSysFile->lastInsertId('sys_file');
				}

				if($lastInsertId)
				{					
					if($insertFile['metadata'])
					{
						$insertFileMetadata = [];
						$insertFileMetadata['uid'] = NULL;
						$insertFileMetadata['pid'] = 0;
						$insertFileMetadata['tstamp'] = $time;
						$insertFileMetadata['crdate'] = $time;
						$insertFileMetadata['cruser_id'] = ($image['cruser_id'])?$image['cruser_id']:0;
						$insertFileMetadata['file'] = $lastInsertId;
						$insertFileMetadata['title'] = $image['title'];
						$insertFileMetadata['width'] = $image['width'];
						$insertFileMetadata['height'] = $image['height'];
						$insertFileMetadata['description'] = $image['caption'];
						$insertFileMetadata['alternative'] = $image['alt'];

						$queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable('sys_file_metadata');
						$queryBuilder
							->insert('sys_file_metadata')
							->values($insertFileMetadata)
							->execute();
					}
					
					$insertFileReference = [];
					$insertFileReference['uid'] = NULL;
					$insertFileReference['pid'] = $image['pid'];
					$insertFileReference['tstamp'] = $time;					
					$insertFileReference['crdate'] = $time;
					$insertFileReference['cruser_id'] = ($image['cruser_id'])?$image['cruser_id']:0;
					$insertFileReference['hidden'] = $image['hidden'];
					$insertFileReference['uid_local'] = $lastInsertId;
					$insertFileReference['uid_foreign'] = $newsObj->getUid();
					$insertFileReference['tablenames'] = 'tx_mooxnews_domain_model_news';
					$insertFileReference['fieldname'] = 'fal_media';
					$insertFileReference['sorting_foreign'] = $sorting;
					$insertFileReference['table_local'] = 'sys_file';
					$insertFileReference['title'] = $image['title'];
					$insertFileReference['description'] = $image['caption'];
					$insertFileReference['alternative'] = $image['alt'];
					$insertFileReference['import_id'] = $image['import_id'];
					$insertFileReference['import_source'] = $image['import_source'];
					$insertFileReference['showinpreview'] = ($sorting>1)?0:1;
					$insertFileReference['sys_language_uid'] = $image['sys_language_uid'];

					$queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable('sys_file_reference');
					$queryBuilder
						->insert('sys_file_reference')
						->values($insertFileReference)
						->execute();
					
				}

				$sorting++;
				$imageCnt++;
			}

			$queryBuilder
				->update('tx_mooxnews_domain_model_news')
				->where(
					$queryBuilder->expr()->eq('uid', $queryBuilder->createNamedParameter($newsObj->getUid(), \PDO::PARAM_INT))
				)
				->set('fal_media', $imageCnt)
				->execute();
		}
	}

	/**
	 * write files of grabbed news to db (FAL)
	 * @param Neulandlotsen\MooxNews\Domain\Model\News $newNews
	 * @param array $news
	 * @param array $files
	 */
	public function writeFilesFal($newsObj,$news,$files)
	{
		$extConf = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Configuration\ExtensionConfiguration::class)->get('moox_news');

		if($extConf['grabberPublicFolder'])
		{
			$grabberPublicFolder = explode(":",$extConf['grabberPublicFolder']);
			if(count($grabberPublicFolder)==3)
			{
				$publicStorageUid = $grabberPublicFolder[0];
				$publicStorageRootFolder = $grabberPublicFolder[1];
				$grabberPublicFolder = $grabberPublicFolder[2];
			}
			elseif(count($grabberPublicFolder)==1)
			{
				$publicStorageUid = 1;
				$publicStorageRootFolder = "fileadmin";
				$grabberPublicFolder = $grabberPublicFolder[0];
			}
		}
		if(!$grabberPublicFolder)
		{
			$publicStorageUid = 1;
			$publicStorageRootFolder = "fileadmin";
			$grabberPublicFolder = self::$userUploadFolder.self::$newsFolder.self::$grabberFolder;
		}

		if($extConf['grabberPrivateFolder'])
		{
			$grabberPrivateFolder = explode(":",$extConf['grabberPrivateFolder']);
			if(count($grabberPrivateFolder)==3)
			{
				$privateStorageUid = $grabberPrivateFolder[0];
				$privateStorageRootFolder = $grabberPrivateFolder[1];
				$grabberPrivateFolder = $privateStorageRootFolder."/".$grabberPrivateFolder[2];
			}
			elseif(count($grabberPrivateFolder)==1)
			{
				$privateStorageUid = 1;
				$privateStorageRootFolder = "fileadmin";
				$grabberPrivateFolder = $grabberPrivateFolder[0];
			}
		}
		if(!$grabberPrivateFolder)
		{
			$privateStorageUid = 1;
			$privateStorageRootFolder = "fileadmin";
			$grabberPublicFolder = self::$userUploadFolderSecured.self::$newsFolder.self::$grabberFolder;
		}

		$types = self::$types;

		$queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)
			->getQueryBuilderForTable('sys_file_reference');

		$queryBuilder
			->getRestrictions()
			->removeAll();

		/*
		$filesToRemove = $queryBuilder
			->select('uid_local')
			->from('sys_file_reference')
			->where(
				$queryBuilder->expr()->eq('fieldname', $queryBuilder->createNamedParameter('fal_related_files', \PDO::PARAM_STR)),
				$queryBuilder->expr()->eq('tablenames', $queryBuilder->createNamedParameter('tx_mooxnews_domain_model_news', \PDO::PARAM_STR)),
				$queryBuilder->expr()->eq('import_source', $queryBuilder->createNamedParameter($news['import_source'], \PDO::PARAM_STR)),
				$queryBuilder->expr()->eq('import_id', $queryBuilder->createNamedParameter($news['import_id'], \PDO::PARAM_STR))
			)
			->execute();
		*/

		$queryBuilder
			->delete('sys_file_reference')
			->where(
				$queryBuilder->expr()->eq('fieldname', $queryBuilder->createNamedParameter('fal_related_files', \PDO::PARAM_STR)),
				$queryBuilder->expr()->eq('tablenames', $queryBuilder->createNamedParameter('tx_mooxnews_domain_model_news', \PDO::PARAM_STR)),
				$queryBuilder->expr()->eq('import_source', $queryBuilder->createNamedParameter($news['import_source'], \PDO::PARAM_STR)),
				$queryBuilder->expr()->eq('import_id', $queryBuilder->createNamedParameter($news['import_id'], \PDO::PARAM_STR))
			)
			->execute();

		/*
		while($file = $filesToRemove->fetch())
		{
			$queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)
				->getQueryBuilderForTable('sys_file');

			$queryBuilder
				->delete('sys_file')
				->where(
					$queryBuilder->expr()->eq('uid', $queryBuilder->createNamedParameter($file['uid_local'], \PDO::PARAM_INT))
				)
				->execute();

			$queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)
				->getQueryBuilderForTable('sys_file_metadata');

			$queryBuilder
				->delete('sys_file_metadata')
				->where(
					$queryBuilder->expr()->eq('uid', $queryBuilder->createNamedParameter($file['uid_local'], \PDO::PARAM_INT))
				)
				->execute();
		}
		*/

		$sorting = 1;
		$fileCnt = 0;

		foreach($files AS $file)
		{
			if($file['secured'])
			{
				$storageRootFolder = $privateStorageRootFolder;
				$storageUid = $privateStorageUid;
				$uploadFolder = "/".$grabberPrivateFolder;
			}
			else
			{
				$storageRootFolder = $publicStorageRootFolder;
				$storageUid = $publicStorageUid;
				$uploadFolder = "/".$grabberPublicFolder;
			}
			$filepath = Environment::getPublicPath() . '/'.$storageRootFolder.$uploadFolder."/".$file['filename'];

			if(file_exists($filepath))
			{
				$file['cruser_id'] = $newsObj->getCruserId();
				$file['sys_language_uid'] = $news['sys_language_uid'];
				$file['import_id'] = $news['import_id'];
				$file['import_source'] = $news['import_source'];
				$file['pid'] = $news['pid'];
				$file['storage'] = ($news['file_storage'])?$news['file_storage']:$storageUid;
				$file['name'] = $file['filename'];
				$file['type'] = $types[strtolower($file['extension'])];
				$file['identifier'] = $uploadFolder."/".$file['name'];
				$file['identifier_hash'] = sha1($file['identifier']);
				$file['folder_hash'] = sha1($uploadFolder);
				$file['extension'] = strtolower($file['extension']);
				$file['mime_type'] = $file['mime_type'];
				$file['name'] = $file['filename'];
				$file['sha1'] = sha1_file(Environment::getPublicPath() . '/'.$storageRootFolder.$file['identifier']);
				$file['size'] = filesize(Environment::getPublicPath() . '/'.$storageRootFolder.$file['identifier']);
				$file['hidden'] = 0;

				$time = time();

				$insertFile = [];
				$insertFile['uid'] = NULL;
				$insertFile['pid'] = 0;
				$insertFile['tstamp'] = $time;
				$insertFile['storage'] = $file['storage'];
				$insertFile['type'] = $file['type'];
				$insertFile['identifier'] = $file['identifier'];
				$insertFile['identifier_hash'] = $file['identifier_hash'];
				$insertFile['folder_hash'] = $file['folder_hash'];
				$insertFile['extension'] = $file['extension'];
				$insertFile['mime_type'] = $file['mime_type']?$file['mime_type']:'unknown';
				$insertFile['name'] = $file['name'];
				$insertFile['sha1'] = $file['sha1'];
				$insertFile['size'] = $file['size'];
				$insertFile['creation_date'] = $time;
				$insertFile['modification_date'] = $time;
				$insertFile['metadata'] = 0;

				$queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)
					->getQueryBuilderForTable('sys_file');

				$queryBuilder
					->getRestrictions()
					->removeAll();

				$existingFile = $queryBuilder
					->select('uid')
					->from('sys_file')
					->where(
						$queryBuilder->expr()->eq('identifier', $queryBuilder->createNamedParameter($insertFile['identifier'], \PDO::PARAM_STR))
					)
					->execute();

				if($existingFile = $existingFile->fetch())
				{
					$lastInsertId = $existingFile['uid'];
				}
				else
				{
					$connectionPool = GeneralUtility::makeInstance(ConnectionPool::class);
					$databaseConnectionForSysFile = $connectionPool->getConnectionForTable('sys_file');
					$databaseConnectionForSysFile->insert(
						'sys_file',
						$insertFile
					);

					$lastInsertId = (int)$databaseConnectionForSysFile->lastInsertId('sys_file');
				}

				if($lastInsertId)
				{
					if($insertFile['metadata'])
					{
						$insertFileMetadata = [];
						$insertFileMetadata['uid'] = NULL;
						$insertFileMetadata['pid'] = 0;
						$insertFileMetadata['tstamp'] = $time;
						$insertFileMetadata['crdate'] = $time;
						$insertFileMetadata['cruser_id'] = ($file['cruser_id'])?$file['cruser_id']:0;
						$insertFileMetadata['file'] = $lastInsertId;
						$insertFileMetadata['title'] = $file['title'];
						$insertFileMetadata['description'] = $file['caption'];
						$insertFileMetadata['alternative'] = $file['alt'];

						$queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable('sys_file_metadata');
						$queryBuilder
							->insert('sys_file_metadata')
							->values($insertFileMetadata)
							->execute();
					}

					$insertFileReference = [];
					$insertFileReference['uid'] = NULL;
					$insertFileReference['pid'] = $file['pid'];
					$insertFileReference['tstamp'] = $time;
					$insertFileReference['crdate'] = $time;
					$insertFileReference['cruser_id'] = ($file['cruser_id'])?$file['cruser_id']:0;
					$insertFileReference['hidden'] = $file['hidden'];
					$insertFileReference['uid_local'] = $lastInsertId;
					$insertFileReference['uid_foreign'] = $newsObj->getUid();
					$insertFileReference['tablenames'] = 'tx_mooxnews_domain_model_news';
					$insertFileReference['fieldname'] = 'fal_related_files';
					$insertFileReference['sorting_foreign'] = $sorting;
					$insertFileReference['table_local'] = 'sys_file';
					$insertFileReference['title'] = $file['title'];
					$insertFileReference['description'] = $file['caption'];
					$insertFileReference['alternative'] = $file['alt'];
					$insertFileReference['import_id'] = $file['import_id'];
					$insertFileReference['import_source'] = $file['import_source'];
					$insertFileReference['sys_language_uid'] = $file['sys_language_uid'];

					$queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable('sys_file_reference');
					$queryBuilder
						->insert('sys_file_reference')
						->values($insertFileReference)
						->execute();
				}

				$sorting++;
				$fileCnt++;
			}

			$queryBuilder
				->update('tx_mooxnews_domain_model_news')
				->where(
					$queryBuilder->expr()->eq('uid', $queryBuilder->createNamedParameter($newsObj->getUid(), \PDO::PARAM_INT))
				)
				->set('fal_related_files', $fileCnt)
				->execute();
		}
	}

	/**
	 * decode special chars
	 * @param string $input
	 * @return string $output
	 */
	 public function decode($input)
	 {		
		$output = $input;
		
		$replaceArray = ['ä','ö','ü','Ä','Ö','Ü','ß','&'];
		$charsArray = ['/#auml;/','/#ouml;/','/#uuml;/','/#Auml;/','/#Ouml;/','/#Uuml;/','/#szlig;/','/#amp;/'];
		$output = preg_replace($charsArray , $replaceArray , $output);
		$output = trim(str_replace("#strong#","<strong>",$output));
		$output = trim(str_replace("#/strong#","</strong>",$output));
		$output = trim(str_replace("#a1#","<a ",$output));
		$output = trim(str_replace("#a2#"," >",$output));
		$output = trim(str_replace("#/a#","</a>",$output));
		$output = trim(str_replace("#/strong#","</strong>",$output));
		$output = utf8_decode($output);
		
		return $output;
	}
}
?>