<?php
if (!defined('TYPO3_MODE')) {
	die ('Access denied.');
}

// register static typoscript file(s) of extension
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile($_EXTKEY, 'Configuration/TypoScript', 'MOOX Bootstrap 4');

?>
