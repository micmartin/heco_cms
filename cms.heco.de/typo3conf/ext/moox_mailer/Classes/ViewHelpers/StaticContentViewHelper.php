<?php
namespace Neulandlotsen\MooxMailer\ViewHelpers;

/**
 * This file is part of the "moox_mailer" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */
 
use TYPO3\CMS\Backend\Utility\BackendUtility; 
 
class StaticContentViewHelper extends \TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper
{	
	/**
	 * get static content from page
	 *
	 * @param integer $pageUid
	 * @param string $as	 	 
	 * @return string $code
	 */
	public function render($pageUid = NULL, $as = "")
	{		
		$code = '';
		
		if($pageUid>0)
		{
			$contents = $GLOBALS['TYPO3_DB']->exec_SELECTgetRows(
				'header,bodytext',
				'tt_content',
				'pid='.$pageUid.' AND deleted=0 '.BackendUtility::BEenableFields('tt_content')
			);
			
			if(is_array($contents))
			{
				foreach($contents AS $content)
				{
					$value = trim($content['bodytext']);
					if(substr_count($value,"<p>")>1)
					{
						$value = str_replace("</p>","<br />",$value);
					}
					$value = str_replace("<p>","",$value);	
					$value = str_replace("</p>","",$value);
					if(substr($value,-6)=="<br />")
					{
						$value = substr($value,0,strlen($value)-6);
					}
					$code .= "<div>".$value."</div>";
				}
				
			}
		}
		
		if($as!="")
		{
			$this->templateVariableContainer->add($as, $code);
		} 
		else 
		{
			return $code;
		}
	}
}
