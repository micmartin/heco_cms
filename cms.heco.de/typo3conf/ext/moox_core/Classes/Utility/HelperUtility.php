<?php
namespace Neulandlotsen\MooxCore\Utility;

/**
 * This file is part of the "moox_core" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

use TYPO3\CMS\Core\SingletonInterface;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Utility\RootlineUtility;

class HelperUtility implements SingletonInterface
{
	/**
	 * @param array $a
	 * @param array $b
	 * @return bool
	 */
	static public function sortByFolderAndTitle($a, $b)
	{
		return strcmp($a["folder"].$a["title"], $b["folder"].$b["title"]);
	}

	/**
	 * @param string $request
	 * @return string $content
	 */
	static public function request($request)
	{
		$extConf = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Configuration\ExtensionConfiguration::class)->get('moox_core');

		$options = [
			CURLOPT_RETURNTRANSFER => true,     			// return web page
			CURLOPT_HEADER => false,    					// don't return headers
			CURLOPT_FOLLOWLOCATION => true,     			// follow redirects
			CURLOPT_ENCODING => "",       					// handle all encodings
			CURLOPT_USERAGENT => "typo3_moox_core", 		// who am i
			CURLOPT_AUTOREFERER => true,     				// set referer on redirect
			CURLOPT_CONNECTTIMEOUT => 120,      			// timeout on connect
			CURLOPT_TIMEOUT => 120,      					// timeout on response
			CURLOPT_MAXREDIRS => 10,       					// stop after 10 redirects
		];

		if($extConf['curlCredentials']!="")
		{
			$options[CURLOPT_USERPWD] = $extConf['curlCredentials'];
		}

		$ch = curl_init( $request );
		curl_setopt_array( $ch, $options );
		$content = curl_exec( $ch );
		$header = curl_getinfo( $ch );
		if($header['http_code']!=200)
		{
			$content = false;
		}

		curl_close( $ch );

		return $content;
	}

	// copy a directory and all subdirectories and files (recursive)
	// void dircpy( str 'source directory', str 'destination directory' [, bool 'overwrite existing files'] )
	static public function dircpy($basePath, $source, $dest, $overwrite = false)
	{
		if(!is_dir($basePath . $dest)) //Lets just make sure our new folder is already created. Alright so its not efficient to check each time... bite me
			mkdir($basePath . $dest);
		if($handle = opendir($basePath . $source)){        // if the folder exploration is sucsessful, continue
			while(false !== ($file = readdir($handle))){ // as long as storing the next file to $file is successful, continue
				if($file != '.' && $file != '..'){
					$path = $source . '/' . $file;
					if(is_file($basePath . $path)){
						if(!is_file($basePath . $dest . '/' . $file) || $overwrite)
							if(!@copy($basePath . $path, $basePath . $dest . '/' . $file)){
								echo '<font color="red">File ('.$path.') could not be copied, likely a permissions problem.</font>';
							}
					} elseif(is_dir($basePath . $path)){
						if(!is_dir($basePath . $dest . '/' . $file))
							mkdir($basePath . $dest . '/' . $file); // make subdirectory before subdirectory is copied
						self::dircpy($basePath, $path, $dest . '/' . $file, $overwrite); //recurse!
					}
				}
			}
			closedir($handle);
		}
	}

	/**
	 * encrypt text
	 *
	 * @param string $plainText
	 * @return string $encryptedText
	 */
	static public function encryptText($plainText = "")
	{		
		$firstKey = base64_decode($GLOBALS['TYPO3_CONF_VARS']['SYS']['mooxEncryptionKey1']);
		$secondKey = base64_decode($GLOBALS['TYPO3_CONF_VARS']['SYS']['mooxEncryptionKey2']); 
		
		$method = "aes-256-cbc";    
		$iv_length = openssl_cipher_iv_length($method);
		$iv = openssl_random_pseudo_bytes($iv_length);
				
		$firstEncrypted = openssl_encrypt($plainText,$method,$firstKey, OPENSSL_RAW_DATA ,$iv);    
		$secondEncrypted = hash_hmac('sha3-512', $firstEncrypted, $secondKey, TRUE);
		
		$encryptedText = base64_encode($iv.$secondEncrypted.$firstEncrypted);

		$encryptedText = str_replace("/","~slash~",$encryptedText);
		$encryptedText = str_replace("+","~plus~",$encryptedText);

		return $encryptedText;
	}
	
	/**
	 * decrypt text
	 *
	 * @param string $encryptedText
	 * @return string $decryptedText
	 */
	static public function decryptText($encryptedText = "")
	{
		$encryptedText = str_replace("~slash~","/",$encryptedText);
		$encryptedText = str_replace("~plus~","+",$encryptedText);

		$firstKey = base64_decode($GLOBALS['TYPO3_CONF_VARS']['SYS']['mooxEncryptionKey1']);
		$secondKey = base64_decode($GLOBALS['TYPO3_CONF_VARS']['SYS']['mooxEncryptionKey2']);
		$mix = base64_decode($encryptedText);
		
		$method = "aes-256-cbc";    
		$iv_length = openssl_cipher_iv_length($method);
					
		$iv = substr($mix,0,$iv_length);
		$secondEncrypted = substr($mix,$iv_length,64);
		$firstEncrypted = substr($mix,$iv_length+64);
		
		$decryptedText = openssl_decrypt($firstEncrypted,$method,$firstKey,OPENSSL_RAW_DATA,$iv);
		$secondEncryptedNew = hash_hmac('sha3-512', $firstEncrypted, $secondKey, TRUE);

		if (hash_equals($secondEncrypted,$secondEncryptedNew)) {
			return $decryptedText;
		}
	}

	/**
	 * @param string $url
	 * @return string $file
	 */
	static public function getRemoteFile($url = "")
	{
		$file = "";
		if($url)
		{
			$ch = curl_init();

			curl_setopt($ch, CURLOPT_POST, 0);
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

			$file = curl_exec($ch);

			curl_close($ch);
		}
		return $file;
	}

	/**
	 * @param string $html
	 * @return string $title
	 */
	static public function getTitleFromHtml($html = "")
	{
		preg_match("/<title>(.*)<\/title>/siU", $html, $title_matches);

		// Clean up title: remove EOL's and excessive whitespace.
		$title = preg_replace('/\s+/', ' ', $title_matches[1]);
		$title = trim($title);

		return $title;
	}

	/**
	 * @param integer $pageUid page uid
	 * @return array $TSObj->setup
	 */
	protected function getTsSetup($pageUid)
	{
		$pageSelect  = GeneralUtility::makeInstance('TYPO3\CMS\Frontend\Page\PageRepository');
		$pageSelect->init(false);

        $rootLine = GeneralUtility::makeInstance(RootlineUtility::class, $pageUid, '', $pageSelect)->get();
		$templateService = GeneralUtility::makeInstance('TYPO3\CMS\Core\TypoScript\TemplateService');
		$templateService->tt_track = 0;
		$templateService->init();
		$templateService->runThroughTemplates($rootLine);
		$templateService->generateConfig();

		return $templateService->setup;
	}

	/**
	 * @param integer $pageUid page uid
	 * @return string $baseURL
	 */
	public function getBaseUrl($pageUid)
	{
		$config = $this->getTsSetup($pageUid)['config.'];

		if($_SERVER['HTTPS']=="on")
		{
			$prefix = "https";
		}
		else
		{
			$prefix = "http";
		}

		if($config['baseURL']==$prefix."://")
		{
			$baseURL = $prefix."://".$_SERVER['SERVER_NAME']."/";
		}
		elseif($config['baseURL']=="//")
		{
			$baseURL = $prefix."://".$_SERVER['SERVER_NAME']."/";
		}
		elseif($config['baseURL']=="/")
		{
			$baseURL = $prefix."://".$_SERVER['SERVER_NAME']."/";
		}
		else
		{
			$baseURL = $config['baseURL'];
			if(substr(strtolower($baseURL),0,2)=="//")
			{
				$baseURL = $prefix.":".$baseURL;
			}
		}

		if(substr($baseURL,(strlen($baseURL)-1))!="/")
		{
			$baseURL = $baseURL."/";
		}

		return $baseURL;
	}
}
?>