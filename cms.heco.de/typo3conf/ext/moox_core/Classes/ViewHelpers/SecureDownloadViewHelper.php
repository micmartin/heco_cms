<?php
namespace Neulandlotsen\MooxCore\ViewHelpers;

/**
 * This file is part of the "moox_core" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

use Neulandlotsen\MooxCore\Utility\HelperUtility as CoreHelperUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Service\TypoScriptService;
use TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer;
use TYPO3Fluid\Fluid\Core\ViewHelper\Traits\CompileWithRenderStatic;
use Neulandlotsen\MooxCore\Service\HelperService;

class SecureDownloadViewHelper extends \TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper
{
	use CompileWithRenderStatic;

	/**
	 * @var HelperService
	 */
	protected $helperService;

    /**
     * @param HelperService $helperService
     */
    public function injectHelperService(HelperService $helperService)
    {
        $this->helperService = $helperService;
    }

	public function initializeArguments()
	{
		parent::initializeArguments();
		$this->registerArgument('table', 'string', 'Table of secured object', true);
		$this->registerArgument('file', 'string', 'Path to the file', true);
		$this->registerArgument('uid', 'int', 'uid of secured object', false, 0);
	}

	/**
	 * @var bool
	 */
	protected $escapeOutput = false;

	/**
	 * @return string
	 * @throws \TYPO3\CMS\Fluid\Core\ViewHelper\Exception\InvalidVariableException
	 */
	public function render()
	{
		$extConf = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Configuration\ExtensionConfiguration::class)->get('moox_core');

		$table = $this->arguments['table'];
		$file = $this->arguments['file'];
		$uid = $this->arguments['uid'];

		$securityExcludedFileTypes = [];
		if($extConf['securityExcludedFileTypes']!="")
		{
			$securityExcludedFileTypes = explode(",",$extConf['securityExcludedFileTypes']);
		}
		$securityFileLocators = [];
		if($extConf['securityFileLocators']!="")
		{
			$securityFileLocators = explode(",",$extConf['securityFileLocators']);
		}

		if($extConf['securityUrlIndicator']!="")
		{
			$securityUrlIndicator = $extConf['securityUrlIndicator'];
		}
		else
		{
			$securityUrlIndicator = "mxsecured";
		}

		if($uid>0 && $file!="" && $table!="" && !in_array(strtolower(pathinfo($file)['extension']),$securityExcludedFileTypes))
		{
			foreach($securityFileLocators AS $securityFileLocator)
			{
				if(strpos($file,$securityFileLocator) !== FALSE)
				{
					$secureData['uid'] = $uid;
					$secureData['table'] = $table;
					$secureData['pid'] = $GLOBALS["TSFE"]->id;

					$secureDataHash = urlencode(CoreHelperUtility::encryptText(json_encode($secureData)));

					$file = str_replace($securityFileLocator,$securityFileLocator.$securityUrlIndicator.'/'.$secureDataHash.'/',$file);
				}
			}
		}
		return $file;
	}
}
