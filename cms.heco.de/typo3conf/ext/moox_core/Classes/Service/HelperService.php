<?php
namespace Neulandlotsen\MooxCore\Service;

/**
 * This file is part of the "moox_core" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

use TYPO3\CMS\Core\SingletonInterface;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Messaging\FlashMessage;
use TYPO3\CMS\Extbase\Utility\LocalizationUtility; 
 
class HelperService implements SingletonInterface
{
	/**
	 * @var array
	 */
	public $subjectDummyData = [];

	/**
	 * @var array
	 */
	public $templateDummyData = [];

}
?>