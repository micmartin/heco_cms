<?php
namespace Neulandlotsen\MooxNewsSeminar\Controller;

/**
 * This file is part of the "moox_news_seminar" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

use TYPO3\CMS\Core\Messaging\FlashMessage;
 
class Mod1Controller extends \Neulandlotsen\MooxNews\Controller\Mod1Controller
{
	/**
	 * Set Backend Module Templates
	 */
	private function setBackendModuleTemplates(){
		$frameworkConfiguration = $this->configurationManager->getConfiguration(\TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface::CONFIGURATION_TYPE_FRAMEWORK);
		$viewConfiguration = [
			'view' => [
				'templateRootPaths' => [
					0 => 'EXT:moox_news_seminar/Resources/Private/Extensions/moox_news/Backend/Templates/',
					1 => 'EXT:moox_news/Resources/Private/Backend/Templates/',
					
				],
				'partialRootPaths' => [					
					0 => 'EXT:moox_news_seminar/Resources/Private/Extensions/moox_news/Backend/Partials/',
					1 => 'EXT:moox_news/Resources/Private/Backend/Partials/',
				],				
			]
		];
		$this->configurationManager->setConfiguration(array_merge($frameworkConfiguration, $viewConfiguration)); 		
	}
		
	/**
	 * Initializes the controller before invoking an action method.
	 */
	protected function initializeAction() {

		// ToDo: translate type name 2019-07-25
		$this->types = ['moox_news_seminar' => 'Seminar'];
		$this->filter['type'] = 'moox_news_seminar';
		
		parent::initializeAction();
		$this->setBackendModuleTemplates();	
		
		// set types
		if(!isset($this->types['moox_news_seminar']))
		{
			// add message
			$messages[] = [
				"icon" => '<span class="glyphicon glyphicon-warning-sign icon-alert" aria-hidden="true"></span>',
				"text" => 'Im gewählten Ordner ist das Erstellen von Seminaren nicht erlaubt. Wählen Sie einen anderen Ordner aus.',
				"type" => FlashMessage::WARNING,
			];
			
			// set flash messages
			$this->helperService->setFlashMessages($this,$messages,'',FALSE);
		}
		
		// set multiple functions
		$this->multipleFunctions = array_merge($this->multipleFunctions,$this->helperService->getAdditionalMultiple("tx_mooxnewsseminar_domain_model_news",$this->settings));		
	}
			
}
?>