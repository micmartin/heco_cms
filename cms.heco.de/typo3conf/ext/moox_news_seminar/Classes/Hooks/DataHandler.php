<?php
namespace Neulandlotsen\MooxNewsSeminar\Hooks;

/**
 * This file is part of the "moox_news_seminar" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */
 
class DataHandler extends \Neulandlotsen\MooxNews\Hooks\DataHandler 
{

	/**
	 * do change item after saving
	 *
	 * @param object $item
	 * @param array $params
	 * @return void
	 */
	public function external_MooxNewsSeminar($item,$params = []) 
	{
		// get extensions configuration
		$extConf = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Configuration\ExtensionConfiguration::class)->get('moox_news_seminar');
		
		$return = false;
		
		if($item->getType()=="moox_news_seminar")
		{
			$startdate = $item->getMooxNewsSeminarStartdate();
			$enddate = $item->getMooxNewsSeminarEnddate();
			if($startdate>0)
			{
				$item->setDatetime($startdate);
				$item->setEventStarttime($startdate);
				$index = [];
				$index[] = date("d.m.Y",$startdate);
				$index[] = date("j.n.Y",$startdate);
				$index[] = date("d.m.y",$startdate);
				$index[] = date("j.n.y",$startdate);
				$index[] = date("Y-m-d",$startdate);
				$index[] = date("y-m-d",$startdate);
				$index[] = date("Y-n-j",$startdate);
				$index[] = date("y-n-j",$startdate);
				$item->setMooxNewsSeminarSearchIndex(implode(" ",$index));
				$return = true;
			}
			else
			{
				$item->setDatetime('');
				$item->setYear('');
			}
			if($enddate>$startdate)
			{
				$item->setEventEndtime($enddate);
				$return = true;			
			}
			else
			{
				$item->setEventEndtime(0);
				$item->setMooxNewsSeminarEnddate(0);
				$return = true;
			}

			if($item->getMooxNewsSeminarRegisterAllYear())
			{
				$item->setMooxNewsSeminarRegisterDeadline("");
			}
			else
			{
				if((int)$extConf['autoSetArchiveDateOnSave']>0 && !$item->getArchive())
				{
					if($enddate)
					{
						$ts = $enddate;
						$archiveTs = mktime(0,0,0,date('n',$ts),date('j',$ts)+(int)$extConf['autoSetArchiveDateOnSave'],date('Y',$ts));
						$item->setArchive($archiveTs);
						$return = true;
					}
					elseif($startdate)
					{
						$ts = $startdate;
						$archiveTs = mktime(0,0,0,date('n',$ts),date('j',$ts)+(int)$extConf['autoSetArchiveDateOnSave'],date('Y',$ts));
						$item->setArchive($archiveTs);
						$return = true;
					}
				}

				if((int)$extConf['autoSetRegisterDateOnSave']>0 && !$item->getMooxNewsSeminarRegisterDeadline())
				{
					if($startdate)
					{
						$ts = $item->getMooxNewsSeminarStartdate();
						$registerTs = mktime(0,0,0,date('n',$ts),date('j',$ts)-(int)$extConf['autoSetRegisterDateOnSave'],date('Y',$ts));
						$item->setMooxNewsSeminarRegisterDeadline($registerTs);
						$return = true;
					}
				}

				if((int)$extConf['autoSetDisableDateOnSave']>0 && !$item->getEndtime())
				{
					if($enddate)
					{
						$ts = $enddate;
						$disableTs = mktime(0,0,0,date('n',$ts),date('j',$ts)+(int)$extConf['autoSetDisableDateOnSave'],date('Y',$ts));
						$item->setEndtime($disableTs);
						$return = true;
					}
					elseif($startdate)
					{
						$ts = $startdate;
						$disableTs = mktime(0,0,0,date('n',$ts),date('j',$ts)+(int)$extConf['autoSetDisableDateOnSave'],date('Y',$ts));
						$item->setEndtime($disableTs);
						$return = true;
					}
				}
			}
		}
		
		if($return)
		{
			return $item;
		}	
	}
}