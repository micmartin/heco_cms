<?php
if (!defined('TYPO3_MODE'))
{
	die ('Access denied.');
}

$GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['moox_core']['setup'] = unserialize($_EXTCONF);

/***************
 * Use moox_core PAGE & USER TSconfig
 */
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig('<INCLUDE_TYPOSCRIPT: source="FILE:EXT:'.$_EXTKEY.'/Configuration/PageTS/pageTSconfig.typoscript">');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addUserTSConfig('<INCLUDE_TYPOSCRIPT: source="FILE:EXT:'.$_EXTKEY.'/Configuration/UserTS/userTSconfig.typoscript">');

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
	'Neulandlotsen.'.$_EXTKEY,
	'Pi1',
	[
		'FileSecurity' => 'loadFile',
	],
	// non-cacheable actions
	[
		'FileSecurity' => 'loadFile'
	]
);

if($GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['moox_core']['setup']['pdfAllowGenerationFromRemoteSource'])
{
	\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
		'Neulandlotsen.'.$_EXTKEY,
		'Pi2',
		[
			'Pdf' => 'generatePdf',
		],
		// non-cacheable actions
		[
			'Pdf' => 'generatePdf'
		]
	);
}


if (TYPO3_MODE === 'BE')
{
	// enable SignalSlot
	/** @var \TYPO3\CMS\Extbase\SignalSlot\Dispatcher $signalSlotDispatcher */
    $signalSlotDispatcher = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\\CMS\\Extbase\\SignalSlot\\Dispatcher');

    /**
     * Provides an example .htaccess file for Apache after extension is installed and shows a warning if TYPO3 is not running on Apache.
     */
    $signalSlotDispatcher->connect(
        'TYPO3\\CMS\\Extensionmanager\\Service\\ExtensionManagementService',
        'hasInstalledExtensions',
        'Neulandlotsen\\MooxCore\\Service\\InstallService',
        'generateApacheHtaccess'
    );

	/**
	 * Provides an example robots.txt file after extension is installed and shows a warning if TYPO3 is not running with moox_core robots.txt.
	 */
	$signalSlotDispatcher->connect(
			'TYPO3\\CMS\\Extensionmanager\\Service\\ExtensionManagementService',
			'hasInstalledExtensions',
			'Neulandlotsen\\MooxCore\\Service\\InstallService',
			'createDefaultRobots'
	);

	/**
	 * Provides an example AdditionalConfiguration.php file after extension is installed and shows a warning if TYPO3 is not running with moox_core additional configuration.
	 */
	$signalSlotDispatcher->connect(
		'TYPO3\\CMS\\Extensionmanager\\Service\\ExtensionManagementService',
		'hasInstalledExtensions',
		'Neulandlotsen\MooxCore\Service\InstallService',
		'createDefaultAdditionalConfiguration'
	);
}

// TODO: include http://labor.99grad.de/typo3-autoload-cache-leeren-bei-klick-auf-alle-caches-leeren/
//$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['t3lib/class.t3lib_tcemain.php']['clearCachePostProc'][$_EXTKEY] = 'Deine\Extension\Hooks\ClearCacheHook->clearAutoloadCache';

/***************
 * Register hook for processing scss files
 */
if (TYPO3_MODE === 'FE') {
	$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['t3lib/class.t3lib_pagerenderer.php']['render-preProcess'][$_EXTKEY] = 'Neulandlotsen\\MooxCore\\Hooks\\RenderPreProcessorHook->renderPreProcessorProc';
}

// Caching the pages - default expire 3600 seconds
if (!is_array($TYPO3_CONF_VARS['SYS']['caching']['cacheConfigurations'][$_EXTKEY]))
{
	$TYPO3_CONF_VARS['SYS']['caching']['cacheConfigurations'][$_EXTKEY] = [
		'frontend' => 'TYPO3\\CMS\\Core\\Cache\\Frontend\\VariableFrontend',
		'backend' => 'TYPO3\\CMS\\Core\\Cache\\Backend\\FileBackend',
		'options' => [
			'defaultLifetime' => 0,
		]
	];
}

// register custom tca fields
$GLOBALS['TYPO3_CONF_VARS']['SYS']['formEngine']['nodeRegistry']['mooxCoreSelectSingleWithPreviewImage'] = [
	'nodeName' => 'selectSingleWithPreviewImage',
	'priority' => '70',
	'class' => \Neulandlotsen\MooxCore\Form\Element\SelectSingleWithPreviewImageElement::class,
];
$GLOBALS['TYPO3_CONF_VARS']['SYS']['formEngine']['nodeRegistry']['mooxCoreTemplate'] = [
	'nodeName' => 'mooxCoreTemplate',
	'priority' => '71',
	'class' => \Neulandlotsen\MooxCore\Form\Element\TemplateElement::class,
];

/***************
 * Register hook for default upload folders
 */
$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['t3lib/class.t3lib_userauthgroup.php']['getDefaultUploadFolder'][] =
    \Neulandlotsen\MooxCore\Hooks\DefaultUploadFolder::class . '->getDefaultUploadFolder';

if (\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::isLoaded('flux'))
{
	if (FALSE === (boolean)$GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['moox_core']['setup']['disablePageTemplates'])
	{
		\FluidTYPO3\Flux\Core::registerProviderExtensionKey('Neulandlotsen.MooxCore', 'Page');
	}
	if (FALSE === (boolean)$GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['moox_core']['setup']['disableContentTemplates'])
	{
		\FluidTYPO3\Flux\Core::registerProviderExtensionKey('Neulandlotsen.MooxCore', 'Content');
	}
}

// Register cache frontend for proxy class generation
$GLOBALS['TYPO3_CONF_VARS']['SYS']['caching']['cacheConfigurations']['moox_core'] = [
	'frontend' => \TYPO3\CMS\Core\Cache\Frontend\PhpFrontend::class,
	'backend' => \TYPO3\CMS\Core\Cache\Backend\FileBackend::class,
	'groups' => [
		'all',
		'system',
	],
	'options' => [
		'defaultLifetime' => 0,
	]
];
// Caching the pages - default expire 3600 seconds
if (!is_array($TYPO3_CONF_VARS['SYS']['caching']['cacheConfigurations']['moox_core_scss'])) {
	$TYPO3_CONF_VARS['SYS']['caching']['cacheConfigurations']['moox_core_scss'] = array(
		'frontend' => 'TYPO3\\CMS\\Core\\Cache\\Frontend\\VariableFrontend',
		'backend' => 'TYPO3\\CMS\\Core\\Cache\\Backend\\FileBackend',
		'options' => [
			'defaultLifetime' => 0,
		]
	);
}

\Neulandlotsen\MooxCore\Utility\ClassLoader::registerAutoloader();

$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['scheduler']['tasks']['Neulandlotsen\\MooxCore\\Tasks\\SitemapCrawlerTask'] = [
	'extension' => 'moox_core',
	'title' => 'LLL:EXT:moox_core/Resources/Private/Language/locallang_scheduler.xlf:tx_mooxcore_tasks_sitemapcrawlertask.title',
	'description' => 'LLL:EXT:moox_core/Resources/Private/Language/locallang_scheduler.xlf:tx_mooxcore_tasks_sitemapcrawlertask.description',
	'additionalFields' => 'Neulandlotsen\\MooxCore\\Tasks\\SitemapCrawlerTaskAdditionalFieldProvider'
];

/*
// manipulate asset service of extension vhs
if ($GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['vhs']['setup']['disableAssetHandling']) {
	if (is_numeric(array_search("FluidTYPO3\\Vhs\\Service\\AssetService", $GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['tslib/class.tslib_fe.php']['usePageCache']))) {
		$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['tslib/class.tslib_fe.php']['usePageCache'][$key] = 'Neulandlotsen\\MooxCore\\Service\\Vhs\\AssetService';
	}
	if (is_numeric(array_search("FluidTYPO3\\Vhs\\Service\\AssetService->buildAllUncached", $GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['tslib/class.tslib_fe.php']['contentPostProc-output']))) {
		$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['tslib/class.tslib_fe.php']['contentPostProc-output'][$key] = 'Neulandlotsen\\MooxCore\\Service\\Vhs\\AssetService->buildAllUncached';
	}
	if (is_numeric(array_search("FluidTYPO3\\Vhs\\Service\\AssetService->clearCacheCommand", $GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['t3lib/class.t3lib_tcemain.php']['clearCachePostProc']))) {
		$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['t3lib/class.t3lib_tcemain.php']['clearCachePostProc'][$key] = 'Neulandlotsen\\MooxCore\\Service\\Vhs\\AssetService->clearCacheCommand';
	}
}
else
{
	$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['tslib/class.tslib_fe.php']['usePageCache'][] = 'Neulandlotsen\\MooxCore\\Service\\Vhs\\AssetService';
	$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['tslib/class.tslib_fe.php']['contentPostProc-output'][] = 'Neulandlotsen\\MooxCore\\Service\\Vhs\\AssetService->buildAllUncached';
	$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['t3lib/class.t3lib_tcemain.php']['clearCachePostProc'][] = 'Neulandlotsen\\MooxCore\\Service\\Vhs\\AssetService->clearCacheCommand';
}
*/