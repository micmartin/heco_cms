<?php
namespace Neulandlotsen\MooxNewsSeminar\Domain\Session;

/**
 * This file is part of the "moox_news_seminar" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

class BackendSessionHandler extends \Neulandlotsen\MooxNewsSeminar\Domain\Session\SessionHandler 
{	
	/**
     * @var string
     */
    protected $mode = "BE";
}
?>