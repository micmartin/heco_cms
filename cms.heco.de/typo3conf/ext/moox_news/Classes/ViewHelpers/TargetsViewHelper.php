<?php
namespace Neulandlotsen\MooxNews\ViewHelpers;

/**
 * This file is part of the "moox_news" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

use Neulandlotsen\MooxNews\Domain\Repository\TargetRepository;
use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper;

class TargetsViewHelper extends AbstractViewHelper
{
	/**
	 * @var TargetRepository
	 */
	protected $targetRepository;

    /**
     * @param TargetRepository $targetRepository
     */
    public function injectTargetRepository(TargetRepository $targetRepository)
    {
        $this->targetRepository = $targetRepository;
    }

	public function initializeArguments()
	{
		parent::initializeArguments();
		$this->registerArgument('settings', 'array', 'settings of news-plugin', true);
		$this->registerArgument('as', 'string', 'return result as variable with this name', false);
	}

	/**
	 * @return string
	 */
	public function render()
	{
		$settings = $this->arguments['settings'];
		$as = $this->arguments['as'];

		$targets = $this->targetRepository->findByPids($settings['startingpoint']);

		if($as)
		{
			$this->templateVariableContainer->add($this->arguments['as'], $targets);
		}
		else
		{
			return $targets;
		}
	}
}
