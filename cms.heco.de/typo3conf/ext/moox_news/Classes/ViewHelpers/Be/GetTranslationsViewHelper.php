<?php
namespace Neulandlotsen\MooxNews\ViewHelpers\Be;

/**
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */

use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Backend\Utility\BackendUtility as BackendUtilityCore; 
 
/**
 * ViewHelper to get translations of given news record
 *
 * # Example: Basic example
 * <code>
 * <n:be.getTranslations as="translations" newsItem="newsItem" />
 * </code>
 * <output>
 * Array of translations
 * </output>
 *
 * @package TYPO3
 * @subpackage tx_mooxnews
 */

class GetTranslationsViewHelper extends \TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper {

	/**
	 * Render link to news item or internal/external pages
	 *
	 * @param Neulandlotsen\MooxNews\Domain\Model\News $newsItem current news object
	 * @param string $as	 
	 * @param boolean $reverse If enabled, the iterator will start with the last element and proceed reversely
	 * @param string $iteration The name of the variable to store iteration information (index, cycle, isFirst, isLast, isEven, isOdd)
	 * @param array $languages
	 * @param array $settings
	 * @return int translations count
	 */
	public function render(Neulandlotsen\MooxNews\Domain\Model\News $newsItem, $as = "translation", $reverse = FALSE, $iteration = NULL, $languages = array(), $settings = array()) 
	{
		
		return self::renderStatic($this->arguments, $this->buildRenderChildrenClosure(), $this->renderingContext);
	}
	
	/**
	 * @param array $arguments
	 * @param \Closure $renderChildrenClosure
	 * @param \TYPO3\CMS\Fluid\Core\Rendering\RenderingContextInterface $renderingContext
	 * @return string
	 * @throws \TYPO3\CMS\Fluid\Core\ViewHelper\Exception
	 */
	static public function renderStatic(array $arguments, \Closure $renderChildrenClosure, \TYPO3Fluid\Fluid\Core\Rendering\RenderingContextInterface $renderingContext)
	{
		$templateVariableContainer = $renderingContext->getTemplateVariableContainer();
		
		$arguments['each'] = self::getTranslations($arguments['newsItem']->getUid(),$arguments['settings'],$arguments['languages']);
		
		if ($arguments['each'] === NULL) {
			return '';
		}
		if (is_object($arguments['each']) && !$arguments['each'] instanceof \Traversable) {
			throw new \TYPO3Fluid\Fluid\Core\ViewHelper\Exception('ForViewHelper only supports arrays and objects implementing \Traversable interface', 1248728393);
		}

		if ($arguments['reverse'] === TRUE) {
			// array_reverse only supports arrays
			if (is_object($arguments['each'])) {
				$arguments['each'] = iterator_to_array($arguments['each']);
			}
			$arguments['each'] = array_reverse($arguments['each']);
		}
		$iterationData = array(
			'index' => 0,
			'cycle' => 1,
			'total' => count($arguments['each'])
		);

		$output = '';
		foreach ($arguments['each'] as $keyValue => $singleElement) {
			$templateVariableContainer->add($arguments['as'], $singleElement);
			if ($arguments['key'] !== '') {
				$templateVariableContainer->add($arguments['key'], $keyValue);
			}
			if ($arguments['iteration'] !== NULL) {
				$iterationData['isFirst'] = $iterationData['cycle'] === 1;
				$iterationData['isLast'] = $iterationData['cycle'] === $iterationData['total'];
				$iterationData['isEven'] = $iterationData['cycle'] % 2 === 0;
				$iterationData['isOdd'] = !$iterationData['isEven'];
				$templateVariableContainer->add($arguments['iteration'], $iterationData);
				$iterationData['index']++;
				$iterationData['cycle']++;
			}
			$output .= $renderChildrenClosure();
			$templateVariableContainer->remove($arguments['as']);
			if ($arguments['key'] !== '') {
				$templateVariableContainer->remove($arguments['key']);
			}
			if ($arguments['iteration'] !== NULL) {
				$templateVariableContainer->remove($arguments['iteration']);
			}
		}
		return $output;
	}
	
	/**
	 * Get translations
	 *
	 * @param int $l10nParent
	 * @param array $settings
	 * @param array $languages
	 * @return \TYPO3\CMS\Extbase\Persistence\Generic\QueryResult
	 */
	public function getTranslations($l10nParent,$settings = array(), $languages = array()) 
	{		
		$translations = array();
		
		foreach($languages AS $language){
			if($language['uid']>0){
				$translation = BackendUtilityCore::getRecordLocalization('tx_mooxnews_domain_model_news',$l10nParent,$language['uid']);
				$language[0] 		= $language['title'];
				$language[1] 		= $language['uid'];
				$language[2] 		= $language['flagIcon'];
				if(is_array($translation[0])){
					$translation[0]['language'] = $language;
					$translations[] = $translation[0];		
				} else {
					$translations[] = array("notranslation" => true, 'language' => $language);
				}
			}
		}
				
		return $translations;		
	}	
}
