<?php

namespace Neulandlotsen\MooxNews\ViewHelpers\Be;

/**
 * This file is part of the "moox_news" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */
 
use Neulandlotsen\MooxNews\Domain\Model\News;
use TYPO3\CMS\Core\Utility\GeneralUtility;

class PreviewUrlViewHelper extends \TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper
{

    /**
     * @var string
     */
    protected $tagName = 'a';

    /**
     * @var \Neulandlotsen\MooxCore\Utility\HelperUtility
     */
    protected $helperUtility;

    /**
     * @param \Neulandlotsen\MooxCore\Utility\HelperUtility
     */
    public function injectHelperService(\Neulandlotsen\MooxCore\Utility\HelperUtility $helperUtility)
    {
        $this->helperUtility = $helperUtility;
    }

    public function initializeArguments()
    {
        parent::initializeArguments();
        $this->registerArgument('newsItem', News::class, 'news item', true);
		$this->registerArgument('settings', 'array', 'Settings', false, []);           
    }

    /**
     * @return string previewUrl
     */
    public function render()
    {
        $previewUrl = '';

        $newsItem = $this->arguments['newsItem'];
		$settings = $this->arguments['settings'];

		if(is_object($newsItem))
		{
			if($newsItem->getCategories()->count()>0 && is_object($newsItem->getCategories()->current()) && $newsItem->getCategories()->current()->getPreviewPid())
			{
				$previewPid = $newsItem->getCategories()->current()->getPreviewPid();			
			}
			elseif($settings['previewPid'][$newsItem->getType()]>0)
			{
				$previewPid = $settings['previewPid'][$newsItem->getType()];
			}
			elseif($settings['previewPid']['default']>0)
			{
				$previewPid = $settings['previewPid']['default'];
			}
			if($previewPid>0)
			{
				// TODO: check another solution of url generation
				$baseUrl = $this->helperUtility->getBaseUrl($previewPid);
				$previewUrl = $baseUrl."index.php?id=".$previewPid."&no_cache=1&tx_mooxnews_pi1[news]=".$newsItem->getUid()."&tx_mooxnews_pi1[hash]=".$newsItem->getHash()."&tx_mooxnews_pi1[controller]=News&tx_mooxnews_pi1[action]=preview";
			}			
		}
        
        return $previewUrl;
    }
}
