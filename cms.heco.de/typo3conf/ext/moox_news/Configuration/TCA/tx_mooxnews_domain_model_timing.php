<?php
return [
	'ctrl' => [
		'title'	=> 'LLL:EXT:moox_news/Resources/Private/Language/locallang_db.xlf:tx_mooxnews_domain_model_timing',
		'label' => 'starttime',
		'label_alt' => 'endtime.note',
		'label_alt_force' => 1,
		'default_sortby' => 'ORDER BY starttime ASC',
		'tstamp' => 'tstamp',
		'crdate' => 'crdate',
		'cruser_id' => 'cruser_id',
		//'sortby' => 'sorting',
		'delete' => 'deleted',
		'enablecolumns' => [
			'disabled' => 'hidden'
		],
		'searchFields' => 'note',
		'iconfile' => 'EXT:moox_news/Resources/Public/Icons/mooxnews_domain_model_timing.svg',
		'hideTable' => TRUE
	],
	'interface' => [
		'showRecordFieldList' => 'start,end,note',
	],
	'types' => [
		'1' => ['showitem' => '--palette--;;paletteTiming'],
	],
	'columns' => [
		'parent' => [
			'label' => 'LLL:EXT:moox_news/Resources/Private/Language/locallang_db.xlf:tx_mooxnews_domain_model_timing.parent',
			'config' => [
                'type' => 'passthrough',               
            ],
		],
		'date' => [
			'exclude' => 0,
			'label' => 'LLL:EXT:moox_news/Resources/Private/Language/locallang_db.xlf:tx_mooxnews_domain_model_timing.date',
			'config' => [
				'type' => 'input',
                'renderType' => 'inputDateTime',
				'eval' => 'date',
				/*
				'type' => 'select',
                'renderType' => 'selectSingle',
				'itemsProcFunc' => 'Neulandlotsen\MooxNews\Hooks\ItemsProcFunc->user_timing_date',
				*/
            ],
		],
		'start' => [
			'exclude' => 0,
			'label' => 'LLL:EXT:moox_news/Resources/Private/Language/locallang_db.xlf:tx_mooxnews_domain_model_timing.start',
			'config' => [
                'type' => 'input',
                'size' => 16,
                'eval' => 'time',
                'default' => '',
            ],
		],
		'end' => [
			'exclude' => 0,
			'label' => 'LLL:EXT:moox_news/Resources/Private/Language/locallang_db.xlf:tx_mooxnews_domain_model_timing.end',
			'config' => [
                'type' => 'input',
                'size' => 16,
                'eval' => 'time',
                'default' => '',
            ],
		],
		'note' => [
			'exclude' => 0,
			'label' => 'LLL:EXT:moox_news/Resources/Private/Language/locallang_db.xlf:tx_mooxnews_domain_model_timing.note',
			'config' => [
				'type' => 'input',
				'size' => 50,
				'eval' => 'trim'
			],
		],		
	],
	'palettes' => [
        'paletteTiming' => [
            'showitem' => 'date,start,end,note,',
        ],       
    ]
];
?>