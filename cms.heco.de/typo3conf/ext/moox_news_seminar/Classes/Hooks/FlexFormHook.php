<?php
namespace Neulandlotsen\MooxNewsSeminar\Hooks;

use TYPO3\CMS\Core\Utility\GeneralUtility;

class FlexFormHook
{
	/**
	 * @param array $dataStructure
	 * @param array $identifier
	 * @return array
	 */
	public function parseDataStructureByIdentifierPostProcess(array $dataStructure, array $identifier): array
	{
		if ($identifier['type'] === 'tca' && $identifier['tableName'] === 'tt_content' && $identifier['dataStructureKey'] === 'mooxnews_pi1,list')
		{
			$file = PATH_site . 'typo3conf/ext/moox_news_seminar/Configuration/FlexForms/flexform_mooxnews_extend.xml';
			if(file_exists($file))
			{
				$content = file_get_contents($file);
				if ($content)
				{
					$dataStructure['sheets']['moox_news_seminar'] = GeneralUtility::xml2array($content);
				}
			}
		}
		return $dataStructure;
	}
}
?>