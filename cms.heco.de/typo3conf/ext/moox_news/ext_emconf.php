<?php
$EM_CONF[$_EXTKEY] = [
	'title' => 'MOOX News',
	'description' => 'Adaptation of extension "news" (thanks to Georg Ringer) for MOOX.',
	'category' => 'fe',
	'state' => 'beta',
	'uploadfolder' => true,
	'clearCacheOnLoad' => true,
	'author' => 'Dominic Martin (based on Extension:news by Georg Ringer)',
	'author_email' => 'typo3@neulandlotsen.de',
	'author_company' => 'Neulandlotsen',
	'version' => '9.5.3',
	'constraints' => [
		'depends' => [
			'typo3' => '9.5.0-9.5.99',
			'vhs' => '',
		],
		'conflicts' => [],
		'suggests' => [
			'rx_shariff' => '7.0.0-10.99.99',
		],
	],
	'autoload' => [
		'psr-4' => [
			'Neulandlotsen\\MooxNews\\' => 'Classes'
		]
	]
];
?>