<?php

namespace Neulandlotsen\MooxNewsSeminar\Hooks;

/**
 * This file is part of the "moox_news_seminar" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Userfunc to render alternative label for media elements
 */
class ItemsProcFunc
{
	/**
	 * @var \TYPO3\CMS\Extbase\Object\ObjectManager
	 */
	protected $objectManager;
  
	/**
	 * @var \Neulandlotsen\MooxNewsSeminar\Domain\Repository\SpeakerRepository
	 */
	protected $speakerRepository;
	
	/**
	 * @var \Neulandlotsen\MooxNews\Service\HelperService
	 */
	protected $helperService;
	
	/**
	 * @var array
	 */
	protected $filter;
	
	/**
	 * Modifies the select box of speakers-options
	 *
	 * @param array &$config configuration array
	 * @param mixed &$pObj configuration array
	 * @return void
	 */
	public function user_speakers(array &$config, &$pObj)
	{
		// initialize
		$this->objectManager = GeneralUtility::makeInstance('TYPO3\CMS\Extbase\Object\ObjectManager');
		$this->speakerRepository = $this->objectManager->get('Neulandlotsen\MooxNewsSeminar\Domain\Repository\SpeakerRepository');
		$this->helperService = $this->objectManager->get('Neulandlotsen\MooxNews\Service\HelperService');
		
		// get current news filter 
		$this->filter = $this->helperService->buildFilter('Neulandlotsen\MooxNews\Controller\Mod1Controller');
		
		// get speakers for given folder
		$speakers = $this->speakerRepository->findByPid($this->filter['page']);
		
		// init items array
		$config['items'] = [];
		
		foreach($speakers AS $speaker){
			$config['items'][] = [$speaker->getTitle(),$speaker->getUid()];
		}		
	}
}