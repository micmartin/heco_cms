<?php
namespace Neulandlotsen\MooxNews\Domain\Repository;

/**
 * This file is part of the "moox_news" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

use \TYPO3\CMS\Extbase\Persistence\Repository;
use \TYPO3\CMS\Extbase\Persistence\QueryInterface;

class MooxRepository extends Repository
{	
	/**
	 * @var array	
	 */
	protected $settings;
	
	/**
	 * sets query orderings from given array/string
	 *
	 * @param \TYPO3\CMS\Extbase\Persistence\QueryInterface &$query
	 * @param array|string|null	 
	 */
	protected function setQueryOrderings(QueryInterface &$query, $orderings = NULL)
	{		
		$setOrderings = [];
		
		if(!is_null($orderings) && is_string($orderings))
		{			
			$orderings = [$orderings => QueryInterface::ORDER_ASCENDING];			
		}
		
		if(is_array($orderings))
		{						
			foreach($orderings AS $field => $direction){								
				if(strtolower($direction)=="desc")
				{						
					$setOrderings[$field] = QueryInterface::ORDER_DESCENDING;									
				} 
				else
				{						
					$setOrderings[$field] = QueryInterface::ORDER_ASCENDING;	
				}								
			}					
			
			if(count($setOrderings))
			{				
				$query->setOrderings($setOrderings);				
			}
		}		
	}	
	
	/**
	 * sets query limits from given values
	 *
	 * @param \TYPO3\CMS\Extbase\Persistence\QueryInterface &$query
	 * @param int $offset
	 * @param int $limit
	 */
	protected function setQueryLimits(QueryInterface &$query, $offset = NULL, $limit = NULL)
	{	
		if(is_numeric($offset))
		{			
			$query->setOffset($offset);			
		}
		
		if(is_numeric($limit))
		{			
			$query->setLimit($limit);			
		}
	}
	
	/**
	 * sets query storage page(s)
	 *
	 * @param \TYPO3\CMS\Extbase\Persistence\QueryInterface &$query
	 * @param array|int|string $storagePages
	 */
	protected function setQueryStoragePages(QueryInterface &$query, $storagePages = NULL)
	{		
		$query->getQuerySettings()->setRespectStoragePage(TRUE);
		
		if(is_string($storagePages))
		{
			if($storagePages=="all")
			{
				$query->getQuerySettings()->setRespectStoragePage(FALSE);
			} 
			elseif(strpos($storagePages, ",")!==false)
			{
				$query->getQuerySettings()->setStoragePageIds(explode(",",$storagePages));
			}
		} 
		elseif(is_array($storagePages))
		{
			$setStoragePages = [];
			
			foreach($storagePages AS $storagePage)
			{				
				if(is_numeric($storagePage))
				{					
					$setStoragePages[] = $storagePage;					
				}
			}
			if(count($setStoragePages))
			{
				$query->getQuerySettings()->setStoragePageIds($setStoragePages);
			}					
		} 
		elseif(is_numeric($storagePages))
		{			
			$query->getQuerySettings()->setStoragePageIds([$storagePages]);			
		}				
	}
	
	/**
	 * Finds all by filter (ordered)
	 *	
	 * @param array $filter
	 * @param array $orderings
	 * @param int $offset
	 * @param int $limit
	 * @param array|int $storagePages
	 * @param array|bool $enableFieldsToBeIgnored
	 * @param bool $rawMode if set to true, return is as an array
	 * @return \TYPO3\CMS\Extbase\Persistence\QueryResultInterface
	 */
	public function findByFilter($filter = NULL, $orderings = NULL, $offset = NULL, $limit = NULL, $storagePages = NULL, $enableFieldsToBeIgnored = NULL, $rawMode = FALSE, $includeDeleted = FALSE)
	{		
		$query = $this->createQuery();

		$this->setQueryStoragePages($query,$storagePages);
		$this->setQueryOrderings($query,$orderings);
		$this->setQueryLimits($query,$offset,$limit);
		
		if(is_array($enableFieldsToBeIgnored))
		{			
			$query->getQuerySettings()->setIgnoreEnableFields(TRUE);
			$query->getQuerySettings()->setEnableFieldsToBeIgnored($enableFieldsToBeIgnored);
		} 
		elseif(!is_null($enableFieldsToBeIgnored) && $enableFieldsToBeIgnored)
		{			
			$query->getQuerySettings()->setIgnoreEnableFields(TRUE);
			$query->getQuerySettings()->setEnableFieldsToBeIgnored(["disabled","starttime","endtime"]);
		}
		
		if($includeDeleted)
		{
			$query->getQuerySettings()->setIncludeDeleted(TRUE);
		}

		if(isset($filter['language']) && is_numeric($filter['language']))
		{
			$query->getQuerySettings()->setLanguageUid($filter['language']);
		}

		$constraints = $this->createFilterConstraints($query,$filter);

		// TODO: Variable für excludeRootLevel ergänzen - 2019-05-07
		if(true)
		{
			$constraints[] = $query->logicalNot($query->equals('pid',0));
		}

		if(is_array($constraints))
		{			
			$result = $query->matching(
				$query->logicalAnd($constraints)
			);

			return $result->execute($rawMode);
		} 
		else 
		{			
			return $query->execute($rawMode);				
		}
	}
	
	/**
	 * @return array
	 */
	public function getSettings()
	{
		return $this->settings;
	}

	/**
	 * @param array $settings
	 */
	public function setSettings($settings)
	{
		$this->settings = $settings;
	}

	/**
	 * Debugs a SQL query from a QueryResult
	 *
	 * @param mixed $queryResult
	 */
	public function debugQuery($queryResult)
	{
		$queryParser = $this->objectManager->get(\TYPO3\CMS\Extbase\Persistence\Generic\Storage\Typo3DbQueryParser::class);
		\TYPO3\CMS\Extbase\Utility\DebuggerUtility::var_dump($queryParser->convertQueryToDoctrineQueryBuilder($queryResult)->getSQL());
		\TYPO3\CMS\Extbase\Utility\DebuggerUtility::var_dump($queryParser->convertQueryToDoctrineQueryBuilder($queryResult)->getParameters());
	}
}
