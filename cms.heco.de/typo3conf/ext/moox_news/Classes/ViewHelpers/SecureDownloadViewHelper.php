<?php
namespace Neulandlotsen\MooxNews\ViewHelpers;

/**
 * This file is part of the "moox_core" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Service\TypoScriptService;
use TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer;
use TYPO3Fluid\Fluid\Core\ViewHelper\Traits\CompileWithRenderStatic;

class SecureDownloadViewHelper extends \TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper
{
	use CompileWithRenderStatic;

	public function initializeArguments()
	{
		parent::initializeArguments();
		$this->registerArgument('file', 'string', 'Path to the file', true);
		$this->registerArgument('configuration', 'array', 'Path to the file', false, []);
		$this->registerArgument('secure', 'int', 'news uid', false, 0);
	}

	/**
	 * @var bool
	 */
	protected $escapeOutput = false;

	/**
	 * @return string
	 * @throws \TYPO3\CMS\Fluid\Core\ViewHelper\Exception\InvalidVariableException
	 */
	public function render()
	{
		$extConf = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Configuration\ExtensionConfiguration::class)->get('moox_news');

		$file = $this->arguments['file'];
		$configuration = $this->arguments['configuration'];
		$secure = $this->arguments['secure'];

		$securityExcludedFileTypes = [];
		if($extConf['securityExcludedFileTypes']!="")
		{
			$securityExcludedFileTypes = explode(",",$extConf['securityExcludedFileTypes']);
		}
		$securityFileLocators = [];
		if($extConf['securityFileLocators']!="")
		{
			$securityFileLocators = explode(",",$extConf['securityFileLocators']);
		}

		if($extConf['securityUrlIndicator']!="")
		{
			$securityUrlIndicator = $extConf['securityUrlIndicator'];
		}
		else
		{
			$securityUrlIndicator = "mxsecured";
		}
		if($secure>0 && !in_array($fileInformation['extension'],$securityExcludedFileTypes))
		{
			foreach($securityFileLocators AS $securityFileLocator)
			{
				if(strpos($file,$securityFileLocator) !== FALSE)
				{
					$file = str_replace($securityFileLocator,$securityFileLocator.$securityUrlIndicator.'/news/'.base_convert($secure+999999,10,21).'/'.$GLOBALS["TSFE"]->id.'/',$file);
				}
			}
		}
		return $file;
	}
}
