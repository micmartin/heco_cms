<?php
namespace Neulandlotsen\MooxMailer\ViewHelpers;

/**
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */
 
use TYPO3\CMS\Core\Utility\GeneralUtility;

class GetFoldersViewHelper extends \TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper {

    public function initializeArguments()
    {
        parent::initializeArguments();
        $this->registerArgument('serializedFolders', 'string', 'serializedFolders', true);
    }

    /**
	 * Get folders of given serialized folder array
	 *
	 * @return array
	 */
	public function render()
	{
        $serializedFolders = $this->arguments['serializedFolders'];
	    $folders = array();
		
		if ($serializedFolders === NULL) {
			$serializedFolders = $this->renderChildren();
		}
		
		$unserializedFolders = unserialize($serializedFolders);
		
		if(is_array($unserializedFolders) && count($unserializedFolders)>0){
			$objectManager = GeneralUtility::makeInstance('TYPO3\CMS\Extbase\Object\ObjectManager');		
			$pageRepository = $objectManager->get('TYPO3\\CMS\\Frontend\\Page\\PageRepository');
			foreach($unserializedFolders AS $folder){
				if(is_numeric($folder)){
					$folders[] = $pageRepository->getPage($folder);
				}
			}			
		} 
		return $folders;
	}
}
