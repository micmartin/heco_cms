<?php
namespace Neulandlotsen\MooxMailer\Domain\Repository;

/**
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */

use TYPO3\CMS\Extbase\Persistence\QueryInterface; 

class OpeningRepository extends \TYPO3\CMS\Extbase\Persistence\Repository
{
	
	protected $defaultOrderings = array ('firstOpening' => QueryInterface::ORDER_ASCENDING);	

	/**
	 * find by job and fe user uid
	 *
	 * @param string $job job
	 * @param int $feUserUid fe user uid
	 * @return \Neulandlotsen\MooxMailer\Domain\Model\Opening
	 */
	public function findByJobAndFeUser($job,$feUserUid)
	{
		$query = $this->createQuery();
		$query->getQuerySettings()->setRespectStoragePage(FALSE);
		$query->getQuerySettings()->setRespectSysLanguage(FALSE);
		$query->getQuerySettings()->setIgnoreEnableFields(TRUE);

		return $query->matching(
			$query->logicalAnd(
				$query->equals('job', $job),
				$query->equals('feUser', $feUserUid),
				$query->equals('deleted', 0)
			))->execute()->getFirst();
	}
	
	/**
	 * find by job and address uid
	 *
	 * @param string $job job
	 * @param int $addressUid address uid
	 * @return \Neulandlotsen\MooxMailer\Domain\Model\Opening
	 */
	public function findByJobAndAddress($job,$addressUid)
	{
		$query = $this->createQuery();
		$query->getQuerySettings()->setRespectStoragePage(FALSE);
		$query->getQuerySettings()->setRespectSysLanguage(FALSE);
		$query->getQuerySettings()->setIgnoreEnableFields(TRUE);

		return $query->matching(
			$query->logicalAnd(
				$query->equals('job', $job),
				$query->equals('address', $addressUid),
				$query->equals('deleted', 0)
			))->execute()->getFirst();
	}
}
?>