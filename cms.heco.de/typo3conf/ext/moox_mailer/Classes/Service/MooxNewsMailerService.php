<?php
namespace Neulandlotsen\MooxMailer\Service;

/**
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */

use TYPO3\CMS\Core\Database\Query\Restriction\DeletedRestriction;
use TYPO3\CMS\Extbase\Persistence\QueryInterface;
use TYPO3\CMS\Core\Database\Connection;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface;

class MooxNewsMailerService extends \Neulandlotsen\MooxMailer\Service\MailerService {

	/**
	 * @var \TYPO3\CMS\Extbase\Object\ObjectManager
	 */
	protected $objectManager;

	/**
	 * @var \Neulandlotsen\MooxNews\Domain\Repository\NewsRepository
	 */
	protected $newsRepository;

	/**
	 * @var \Neulandlotsen\MooxNews\Domain\Repository\CategoryRepository
	 */
	protected $categoryRepository;

	/**
	 * @var \Neulandlotsen\MooxMailer\Domain\Repository\FrontendUserRepository
	 */
	protected $frontendUserRepository;

	/**
	 * @var \Neulandlotsen\MooxMailer\Domain\Repository\AddressRepository
	 */
	protected $addressRepository;

	/**
	 * @var \Neulandlotsen\MooxMailer\Service\HelperService
	 */
	protected $helperService;

	/**
	 * @var \Neulandlotsen\MooxCore\Utility\PdfGenerator
	 */
	protected $pdfGenerator;

	/**
	 * @var \TYPO3\CMS\Frontend\Page\PageRepository
	 */
	protected $pageRepository;

	/**
	 * @var \TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface
	 */
	protected $configurationManager;

	/**
	 * @var array
	 */
	protected $categoriesWithoutPermissions;

	/**
	 * @var string
	 */
	protected $sendMode;

	/**
	 * @var bool
	 */
	protected $sendSingleMails;

	/**
	 * @var bool
	 */
	protected $forceSubject;

	/**
	 * @var bool
	 */
	protected $forceTemplateInSingleMailing;

	/**
	 * @var bool
	 */
	protected $forceScheduledTemplateInSingleMailing;

	/**
	 * @var array
	 */
	protected $newsPids;

	/**
	 * @var array
	 */
	protected $userPids;

	/**
	 * @var string
	 */
	protected $addressesPids;

	/**
	 * @var string
	 */
	protected $addressesCategories;

	/**
	 * @var array
	 */
	protected $excludedUserGroups;

	/**
	 * @var array
	 */
	protected $categoriesUids;

	/**
	 * @var string
	 */
	protected $simulatedUserGroups;

	/**
	 * @var bool
	 */
	protected $sendRestrictedNewsOnly;

	/**
	 * @var array
	 */
	protected $previewMaildata;

	/**
	 * @var int
	 */
	protected $previewMaildataUid;

	/**
	 * @var array
	 */
	protected $affectedUsers;

	/**
	 * @var array
	 */
	protected $affectedUsersEmails;

	/**
	 * @var array
	 */
	protected $affectedNews;

	/**
	 * @var array
	 */
	protected $affectedNewsUids;

	/**
	 * @var array
	 */
	protected $affectedNewsUidsByEmail;

	/**
	 * @var int
	 */
	protected $detailPid;

	/**
	 * @var int
	 */
	protected $loginPid;

	/**
	 * @var int
	 */
	protected $autoLoginUser;

	/**
	 * @var string
	 */
	protected $redirectorShortLink;

	/**
	 * @var bool
	 */
	protected $respectTags;

	/**
	 * @var bool
	 */
	protected $respectZipregions;

	/**
	 * @var bool
	 */
	protected $attachRelated;

	/**
	 * @var bool
	 */
	protected $attachSelf;

	/**
	 * @var bool
	 */
	protected $directNews;

	/**
	 * @var int
	 */
	protected $newsLimit;

	/**
	 * @var array
	 */
	protected $processedNews = [];

	/**
	 * @var array
	 */
	protected $attachableFiles = [];

	/**
	 * @var int
	 */
	public static $redirectorType = 9001;

	/**
	 * @var string
	 */
	public static $trackingSalt = "Sf0Cn3OpNFelyH";

	/**
	 * @var string
	 */
	public static $taskName = "mooxnewssendtask";

	/**
	 * initialize moox news mailer
	 *
	 * @param array|object $task task
	 */
	public function initialize($task,$job = '')
	{
		parent::initialize($task);

		$this->objectManager = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\CMS\Extbase\Object\ObjectManager');
		$this->newsRepository = $this->objectManager->get('Neulandlotsen\MooxNews\Domain\Repository\NewsRepository');
		$this->categoryRepository = $this->objectManager->get('Neulandlotsen\MooxNews\Domain\Repository\CategoryRepository');
		$this->frontendUserRepository = $this->objectManager->get('Neulandlotsen\\MooxMailer\\Domain\\Repository\\FrontendUserRepository');
		$this->frontendUserGroupRepository = $this->objectManager->get('Neulandlotsen\\MooxMailer\\Domain\\Repository\\FrontendUserGroupRepository');
		$this->pageRepository = $this->objectManager->get('TYPO3\\CMS\\Frontend\\Page\\PageRepository');
		$this->helperService = $this->objectManager->get('Neulandlotsen\\MooxMailer\Service\\HelperService');
		if(\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::isLoaded('moox_address')){
			$this->addressRepository = $this->objectManager->get('Neulandlotsen\\MooxMailer\\Domain\\Repository\\AddressRepository');
		}
		$this->templateRepository = $this->objectManager->get('Neulandlotsen\\MooxMailer\\Domain\\Repository\\TemplateRepository');
		$this->zipregionRepository = $this->objectManager->get('Neulandlotsen\\MooxMailer\\Domain\\Repository\\ZipregionRepository');
		$this->configurationManager = $this->objectManager->get('TYPO3\\CMS\\Extbase\\Configuration\\ConfigurationManagerInterface');

		$this->setSendMode($task->getSendMode());
		if($task->getSendMode()=="single"){
			$this->setSendSingleMails(true);
		} else {
			$this->setSendSingleMails($task->getSendSingleMails());
		}
		$this->setForceSubject($task->getForceSubject());
		$this->setForceTemplateInSingleMailing($task->getForceTemplateInSingleMailing());
		$this->setForceScheduledTemplateInSingleMailing($task->getForceScheduledTemplateInSingleMailing());
		$this->setDetailPid($task->getDetailPid());
		$this->setLoginPid($task->getLoginPid());
		if (!empty($this->extConf['enableAutoLogin'])){
			$this->setAutoLoginUser($task->getAutoLoginUser());
		}

		if($task->getNewsPids()!=""){
			$this->setNewsPids(explode(",",$task->getNewsPids()));
		} else {
			$this->setNewsPids([]);
		}

		if($task->getUserPids()!=""){
			$this->setUserPids(explode(",",$task->getUserPids()));
		} else {
			$this->setUserPids([]);
		}

		if($task->getAddressesPids()!=""){
			$this->setAddressesPids(explode(",",$task->getAddressesPids()));
		} else {
			$this->setAddressesPids([]);
		}

		if($task->getAddressesCategories()!=""){
			$this->setAddressesCategories(explode(",",$task->getAddressesCategories()));
		} else {
			$this->setAddressesCategories([]);
		}

		if($task->getExcludedUserGroups()!=""){
			$this->setExcludedUserGroups(explode(",",$task->getExcludedUserGroups()));
		} else {
			$this->setExcludedUserGroups([]);
		}

		$categories = [];

		if($task->getIncludedNewsCategoriesUids()==""){
			$includedCategories = $this->getCategories();
		} else {
			$includedCategories = explode(",",$task->getIncludedNewsCategoriesUids());
		}

		if($task->getExcludedNewsCategoriesUids()!=""){
			$excludedCategories = explode(",",$task->getExcludedNewsCategoriesUids());
			foreach($includedCategories AS $includedCategory){
				if(!in_array($includedCategory,$excludedCategories)){
					$categories[] = $includedCategory;
				}
			}
		} else {
			$categories = $includedCategories;
		}

		$this->setCategoriesUids($categories);

		$rightsMapping = $task->getRightsMapping();

		if($rightsMapping!=""){
			$categoriesWithoutPermissions = [];
			$rules = explode("\r\n",$rightsMapping);
			foreach($rules AS $rule){
				$ruleParts 	= explode(":",$rule);
				$uid = $ruleParts[0];
				$fe_groups = $ruleParts[1];
				$categoriesWithoutPermissions[] = array (
					"uid" => trim($uid),
					"fe_groups" => trim($fe_groups)
				);
			}
			if(count($categoriesWithoutPermissions)>0){
				$this->setCategoriesWithoutPermissions($categoriesWithoutPermissions);
			}
		}

		if($task->getSimulatedUserGroups()!=""){
			$this->setSimulatedUserGroups(explode(",",$task->getSimulatedUserGroups()));
		} else {
			$this->setSimulatedUserGroups([]);
		}

		$this->setSendRestrictedNewsOnly($task->getSendRestrictedNewsOnly());
		$this->setSendToUsersWithGroupsOnly($task->getSendToUsersWithGroupsOnly());
		$this->setRespectTags($task->getRespectTags());
		$this->setRespectZipregions($task->getRespectZipregions());
		$this->setAttachRelated($task->getAttachRelated());
		$this->setAttachSelf($task->getAttachSelf());
		$this->setDirectNews($task->getDirectNews());
		$this->setNewsLimit($task->getNewsLimit());

		if($this->getAttachRelated() || $this->getAttachSelf())
		{
			$this->pdfGenerator = $this->objectManager->get('Neulandlotsen\\MooxCore\\Utility\\PdfGenerator');
		}

		$this->previewMaildata = [];
		$this->affectedUsers = [];
		$this->affectedUsersEmails = [];
		$this->affectedNews	= [];
		$this->affectedNewsUids = [];
		$this->affectedNewsUidsByEmail = [];

		$this->configuration = $this->configurationManager->getConfiguration(ConfigurationManagerInterface::CONFIGURATION_TYPE_FRAMEWORK,"MooxMailer");
	}

	/**
	 * get category options
	 *
	 * @param array $categories categories
	 * @param array $selected selected categories
	 * @param int $depth depth
	 * @return string $options
	 */
	public function getCategoryOptions($categories,$selected = [], $depth = 0)
	{
		$ordering = array('title' => QueryInterface::ORDER_ASCENDING);

		$options = '';

		$objectManager = GeneralUtility::makeInstance('TYPO3\CMS\Extbase\Object\ObjectManager');
		$categoryRepository = $objectManager->get('Neulandlotsen\MooxNews\Domain\Repository\CategoryRepository');

		foreach($categories AS $category){
			$options .= '<option '.(in_array($category->getUid(),$selected)?'selected="selected" ':'').'value="'.$category->getUid().'">';
			for($i=0;$i<=$depth;$i++){
				$options .= '&nbsp;';
			}
			$options .= $category->getTitle().' ['.$category->getUid().']';
			$options .= '</option>';
			$children = $categoryRepository->findChildren($category->getUid(),$ordering);

			if(count($children)>0){
				$options .= self::getCategoryOptions($children,$selected,($depth+3));
			}
		}

		return $options;
	}

	/**
	 * Get array of folders with news module
	 *
	 * @return	array	folders with news module
	 */
	public function getMooxNewsFolders()
	{
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable('pages');
        $queryBuilder
            ->getRestrictions()
            ->removeAll()
            ->add(GeneralUtility::makeInstance(DeletedRestriction::class));

        $query = $queryBuilder
            ->select('*')
            ->from('pages')
            ->where(
                $queryBuilder->expr()->andx(
                    $queryBuilder->expr()->in('doktype', $queryBuilder->createNamedParameter(1, \PDO::PARAM_INT), $queryBuilder->createNamedParameter(254, \PDO::PARAM_INT)),
                    $queryBuilder->expr()->eq('module', $queryBuilder->createNamedParameter('mxnews', \PDO::PARAM_STR))
                )
            )
            ->execute();

        $pages = $query->fetch();

		return $pages;
	}

	/**
	 * Get array of pages with news plugin
	 *
	 * @return	array	pages with news plugin
	 */
	public function getMooxNewsPluginPages()
	{
		$pages = [];

		$this->objectManager = GeneralUtility::makeInstance('TYPO3\CMS\Extbase\Object\ObjectManager');
		$this->pageRepository = $this->objectManager->get('TYPO3\\CMS\\Frontend\\Page\\PageRepository');
		$this->pageRepository->init(true);

        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable('tt_content');
        $queryBuilder
            ->getRestrictions()
            ->removeAll()
            ->add(GeneralUtility::makeInstance(DeletedRestriction::class));

        $query = $queryBuilder
            ->select('DISTINCT pid')
            ->from('tt_content')
            ->where(
                $queryBuilder->expr()->andx(
                    $queryBuilder->expr()->in('CType', $queryBuilder->createNamedParameter('list', \PDO::PARAM_STR)),
                    $queryBuilder->expr()->eq('list_type', $queryBuilder->createNamedParameter('mooxnews_pi1', \PDO::PARAM_STR))
                )
            )
            ->execute();

		$res = $query->fetch();

		foreach ($res as $page) {
			$pageInfo 	= $this->pageRepository->getPage((int)$page['pid']);
			if(is_array($pageInfo)){
				$pages[] 	= array("uid" => $pageInfo['uid'], "title" => $pageInfo['title']);
			}
		}

		return $pages;
	}

	/**
	 * Returns news categories without permission mapping
	 *
	 * @return array
	 */
	public function getCategoriesWithoutPermissions()
	{
		return $this->categoriesWithoutPermissions;
	}

	/**
	 * Set news categories without permission mapping
	 *
	 * @param array $categoriesWithoutPermissions news categories without permission mapping
	 * @return void
	 */
	public function setCategoriesWithoutPermissions($categoriesWithoutPermissions)
	{
		$this->categoriesWithoutPermissions = $categoriesWithoutPermissions;
	}

	/**
	 * get news categories of current preparation
	 *
	 * @return array $news
	 */
	public function getCategories()
	{
		$categories = [];

		$categoriesFromRep = $this->categoryRepository->findByPids($this->newsPids);

		foreach($categoriesFromRep AS $category){
			$categories[] = $category->getUid();
		}

		return $categories;
	}

	/**
	 * get news of current preparation
	 *
	 * @return \Neulandlotsen\MooxMailer\Domain\Model\News $news
	 */
	public function getNews()
	{
		$filter = [];
		$filter['mailer'] = true;
		$filter['newsPids'] = $this->newsPids;
		$filter['categoriesUids'] = $this->categoriesUids;

		if($this->sendRestrictedNewsOnly){
			$filter['sendRestrictedNewsOnly'] = 1;
		}

		if($this->sendMode == "cumulated") {
			$filter['mailerFrequenzy'] 	= 2;
		} else {
			$filter['mailerFrequenzy'] 	= 1;
		}

		$news = $this->newsRepository->findNewsByFilter($filter);

		return $news;
	}

	/**
	 * get affected fe user groups
	 *
	 * @param \Neulandlotsen\MooxMailer\Domain\Model\News $news affected news
	 * @return array $feGroups
	 */
	public function getAffectedFeGroups($news)
	{
		$feGroups = [];

		foreach($news AS $item){
			if($item->getFeGroup()!=""){

				$itemGroups = explode(",",$item->getFeGroup());

				foreach($itemGroups AS $itemGroup){
					if($itemGroup>0 && !in_array($itemGroup,$feGroups) && !in_array($itemGroup,$this->getExcludedUserGroups())){
						$feGroups[] = $itemGroup;
					}
				}
			} elseif(!isset($feGroups['nogroup']) && !$this->sendRestrictedNewsOnly) {
				$feGroups['nogroup'] = "";
			}
		}

		return $feGroups;
	}

	/**
	 * get affected tags
	 *
	 * @param mixed $news affected news
	 * @return array $feGroups
	 */
	public function getAffectedTags($news)
	{
		$tags = [];

		foreach($news AS $item){
			if($item->getMailerTags()!=""){

				$itemTags = explode(",",$item->getMailerTags());

				foreach($itemTags AS $itemTag){
					if($itemTag>0 && !in_array($itemTag,$tags)){
						$tags[] = $itemTag;
					}
				}
			} else {
				$tags['notag'] = "";
			}
		}

		return $tags;
	}

	/**
	 * get affected fe users
	 *
	 * @param array $feGroups affected fe groups
	 * @param array $tags affected mailer tags
	 * @return array $feUsers
	 */
	public function getFeUsers($feGroups,$tags = [])
	{
		if(isset($feGroups['all'])){
			return $this->frontendUserRepository->findByPids($this->userPids);
			//return $this->frontendUserRepository->findByPidsAndTags($this->userPids,$tags);
		} else {
			if(count($feGroups)>0){
				return $this->frontendUserRepository->findByPidsAndGroups($this->userPids,$feGroups);
				//return $this->frontendUserRepository->findByPidsGroupsAndTags($this->userPids,$feGroups,$tags);
			} else {
				return [];
			}
		}
	}

	/**
	 * get affected moox adresses
	 *
	 * @return array $mooxAdresses
	 */
	public function getMooxAddress()
	{
		return $this->addressRepository->findByPids($this->addressesPids);
	}

	/**
	 * strip special tags/elements from body
	 *
	 * @param string $body body
	 * @return string $body
	 */
	private function stripMail($body)
	{
		$body = preg_replace("~<(/)?div>~", "<\\1p>", $body);
		$body = strip_tags($body,"<br><strong><ul><li><p><a>");
		$body = htmlspecialchars_decode($body);
		return $body;
	}

	/**
	 * check teaser or extract short description from bodytext if needed
	 *
	 * @param string $teaser teaser
	 * @param string $bodytext bodytext
	 * @return string $body
	 */
	private function getShortDescription($teaser, $bodytext = '')
	{
		if(strlen($teaser) < 1) {
			$bodytext = preg_replace("/\r|\n/s", "", $bodytext);
			$matches = [];
			preg_match("/^.*?[a-z|A-Z][\.|\?|\!]/",$bodytext,$matches);
			return $matches[0];
		}
		else
		{
			return $teaser;
		}
	}

	/**
	 * get email template and render email body
	 *
	 * @param int $templateUid template uid
	 * @param array $variables variables
	 * @return string $emailBody email body
	 */
	private function getEmailBody($templateUid, $variables)
	{
		$template = $this->templateRepository->findByUid($templateUid);

		$emailView = $this->objectManager->get('TYPO3\\CMS\\Fluid\\View\StandaloneView');
		$emailView->setFormat('html');
		// TODO: Local bugfix, need to be checked
		$partialRootPaths[1000] = "EXT:moox_template_chemiebw/Resources/Private/Extensions/moox_mailer/Backend/Partials/";
		$partialRootPaths[0] = $this->configuration['view']['partialRootPaths'][0];
		$emailView->setPartialRootPaths($partialRootPaths);
		$emailView->setTemplateSource(trim($template->getTemplate()));
		$emailView->assignMultiple($variables);
		$emailBody = $emailView->render();

		return $emailBody;
	}

	public function affectedUsersSort($a, $b)
	{
		return (strtolower((string)$a["sort"])>strtolower((string)$b["sort"]));
	}

	/**
	 * process moox news mailer job
	 *
	 * @param array $maildata maildata
	 * @return array $state
	 */
	public function prepareMaildata()
	{
		$this->previewMaildata = [];
		$this->baseUrls	= [];
		$this->affectedUsers = [];
		$this->affectedUsersEmails = [];
		$this->affectedNews = [];
		$this->affectedNewsUids = [];
		$this->affectedNewsUidsByEmail = [];
		$this->attachableFiles = [];

		// get news of current preparation
		$newsItems = $this->getNews();

		if($newsItems->count()<1)
		{
			return [];
		}
		elseif($this->newsLimit>0 && $newsItems->count()<$this->newsLimit)
		{
			return [
				'newsLimit' => $this->newsLimit,
				'newsCount' => $newsItems->count()
			];
		}

		foreach($newsItems AS $news)
		{
			$this->processedNews[] = $news->getUid();
		}


		$feGroups = [];
		$users = [];

		if(count($this->userPids)){
			if($this->sendRestrictedNewsOnly){
				// get affected fe groups
				$feGroups = $this->getAffectedFeGroups($newsItems);
				if(is_array($this->excludedUserGroups) && count($this->excludedUserGroups))
				{
					$feGroups = array_diff($feGroups,$this->excludedUserGroups);
				}
			} else {
				if($this->sendToUsersWithGroupsOnly){
					$feGroupsTmp = $this->getFeUsersGroups($this->userPids);
					foreach($feGroupsTmp AS $feGroupTmp){
						$feGroups[] = $feGroupTmp->getUid();
					}
					if(is_array($this->excludedUserGroups) && count($this->excludedUserGroups))
					{
						$feGroups = array_diff($feGroups,$this->excludedUserGroups);
					}
				} else {
					if(is_array($this->excludedUserGroups) && count($this->excludedUserGroups))
					{
						$feGroupsTmp = $this->getFeUsersGroups($this->userPids);
						foreach($feGroupsTmp AS $feGroupTmp){
							$feGroups[] = $feGroupTmp->getUid();
						}
						$feGroups = array_diff($feGroups,$this->excludedUserGroups);
					}
					else
					{
						$feGroups['all'] = true;
					}
				}
			}

			// get user of current preparation
			$feUsers = $this->getFeUsers($feGroups,$this->getAffectedTags($newsItems));

			$users = [];

			foreach($feUsers AS $feUser){
				if(true || in_array($feUser->getUid(),[2,20])){
					$users[] = array("type" => "feuser", "object" => $feUser);
				}
			}
		}

		// get addresses of current preparation
		if(!$this->sendToUsersWithGroupsOnly){
			if($this->addressRepository && count($this->addressesPids)){
				$mooxAddresses 	= $this->getMooxAddress();
				$mooxAddressesCnt = $mooxAddresses->count();
				foreach($mooxAddresses AS $mooxAddress){
					if(true || in_array($mooxAddress->getUid(),[740,38])){
						$users[] = array("type" => "address", "object" => $mooxAddress);
					}
				}
			}
		}

		// init mail arrays
		$mails = [];
		$previewmails = [];
		$maildata = [];
		$zipregions = $this->zipregionRepository->findAll(FALSE);

		// go through all affected users
		foreach($users AS $user){

			$type = $user['type'];
			$user = $user['object'];

			if(!$this->previewMaildataUser || (strpos($this->previewMaildataUser,"@")!== FALSE && $this->previewMaildataUser==$user->getEmail())){

				if($user->getEmail()!=""){

					$userFeGroups = [];
					$userMailerTags = [];
					$userMailerZipregions = [];

					if($type=="feuser"){
						if($user->getUsergroup()!=NULL){
							foreach($user->getUsergroup() AS $feGroup){
								$userFeGroups[] = $feGroup->getUid();
							}
						}
						if($user->getMailerTags()!=''){
							foreach(explode(',',$user->getMailerTags()) AS $mailerTag){
								if($mailerTag!=''){
									$userMailerTags[] = $mailerTag;
								}
							}
						}
						if($this->respectZipregions && $user->getZip()!=''){
							$userZipregions = [];
							foreach($zipregions AS $zipregion){
								$ziplist = [];
								if($zipregion->getMode()=='list' && $zipregion->getList()!=''){
									$ziplist_tmp = explode(",",$zipregion->getList());
									foreach($ziplist_tmp AS $zip){
										if(is_numeric(trim($zip))){
											$ziplist[] = (int)trim($zip);
										}
									}
								}
								if(($zipregion->getMode()=='area' && (int)$zipregion->getLower()>=0 && (int)$zipregion->getUpper()>0) || ($zipregion->getMode()=='list' AND count($ziplist))){
									if(($zipregion->getMode()=='area' && (int)$user->getZip()>=(int)$zipregion->getLower() && (int)$user->getZip()<=(int)$zipregion->getUpper()) || ($zipregion->getMode()=='list' && in_array((int)$user->getZip(),$ziplist))){
										if(!in_array($zipregion->getUid(),$userZipregions)){
											$userZipregions[] = $zipregion->getUid();
										}
										if(!in_array($zipregion->getUid(),$userMailerZipregions)){
											$userMailerZipregions[] = $zipregion->getUid();
										}
									}
								}
							}
							if(count($userZipregions)){
								$user->setMailerZipregions(implode(",",$userZipregions));
							}
						}
					}
					if(!count($userFeGroups) && count($this->simulatedUserGroups)){
						$userFeGroups = $this->simulatedUserGroups;
					}

					// go through all affected news
					foreach($newsItems AS $news){

						if(!$this->previewMaildataUid || ($this->previewMaildataUid>0 && $this->previewMaildataUid==$news->getUid())){

							if($news->getFeGroup()!=""){
								$newsFeGroups = [];
								$newsFeGroupsTmp = explode(",",$news->getFeGroup());
								foreach($newsFeGroupsTmp AS $newsFeGroupTmp){
									if($newsFeGroupTmp!=""){
										$newsFeGroups[] = $newsFeGroupTmp;
									}
								}

							} else {
								$newsFeGroups = [];
							}
							if($news->getMailerTags()!=""){
								$newsMailerTags = [];
								$newsMailerTagsTmp = explode(",",$news->getMailerTags());
								foreach($newsMailerTagsTmp AS $newsMailerTagTmp){
									if($newsMailerTagTmp!=""){
										$newsMailerTags[] = $newsMailerTagTmp;
									}
								}

							} else {
								$newsMailerTags = [];
							}
							if($this->respectZipregions && $news->getMailerZipregions()!=""){
								$newsMailerZipregions = [];
								$newsMailerZipregionsTmp = explode(",",$news->getMailerZipregions());
								foreach($newsMailerZipregionsTmp AS $newsMailerZipregionTmp){
									if($newsMailerZipregionTmp!=""){
										$newsMailerZipregions[] = $newsMailerZipregionTmp;
									}
								}

							} else {
								$newsMailerZipregions = [];
							}
							$newsFirstCategory 	= $news->getCategories()->current();

							$sendNews = false;



							if(!count($newsFeGroups) && !$this->sendRestrictedNewsOnly && !$this->sendToUsersWithGroupsOnly)
							{
								$sendNews = true;
							}
							elseif(count($userFeGroups) && count($newsFeGroups))
							{
								foreach($userFeGroups AS $userFeGroup)
								{
									if(in_array($userFeGroup,$newsFeGroups))
									{
										$sendNews = true;
										break;
									}
								}
							}
							elseif(count($userFeGroups) || (!count($newsFeGroups) && isset($feGroups['nogroup'])))
							{
								$sendNews = true;
							}

							if($sendNews && $this->respectTags && count($userMailerTags) && count($newsMailerTags)){
								if(count(array_intersect($userMailerTags,$newsMailerTags))<1){
									$sendNews = false;
								}
							}
							if($sendNews && $this->respectZipregions && count($userMailerZipregions) && count($newsMailerZipregions)){
								if(count(array_intersect($userMailerZipregions,$newsMailerZipregions))<1){
									$sendNews = false;
								}
							}
							// ToDo: check this!!!! 2019-08-06
							/*
							if(!$sendNews && $mooxAddressesCnt>0){
								$sendNews = true;
							}
							*/

							if($sendNews){

								if($this->sendSingleMails || $newsFirstCategory->getMailerForcesinglemail()){
									$singleMail = true;
								} else {
									$singleMail = false;
								}

								$mailTemplateUid = 0;
								$mailSubject = "";

								if($singleMail){

									if($this->scheduleUid>0 && $this->forceScheduledTemplateInSingleMailing && $this->schedule['template']['uid']>0){
										$mailTemplateUid = $this->schedule['template']['uid'];
									} elseif($this->templateUid>0 && $this->forceTemplateInSingleMailing){
										$mailTemplateUid = $this->templateUid;
									} elseif($newsFirstCategory->getMailerTemplate()>0){
										$mailTemplateUid = $newsFirstCategory->getMailerTemplate();
									} else {
										$mailTemplateUid = $this->templateUid;
									}

								} else {

									if($this->scheduleUid>0 && $this->schedule['template']['uid']>0){
										$mailTemplateUid = $this->schedule['template']['uid'];
									} else {
										$mailTemplateUid = $this->templateUid;
									}

								}

								$templateInfo = $this->loadTemplate($mailTemplateUid);

								if($singleMail){
									if(!$this->forceSubject && $newsFirstCategory->getMailerSubject()!=""){
										$mailSubject = $newsFirstCategory->getMailerSubject();
									} elseif (!$this->forceSubject && $templateInfo['subject']!=""){
										$mailSubject = $templateInfo['subject'];
									} else {
										$mailSubject = $this->subject;
									}
								} else {
									if (!$this->forceSubject && $templateInfo['subject']!=""){
										$mailSubject = $templateInfo['subject'];
									} else {
										$mailSubject = $this->subject;
									}
								}

								$from = [];
								if ($templateInfo['senderAddress']!=""){
									$from = $this->getFrom();
									$from['address'] = $templateInfo['senderAddress'];
								}
								if ($templateInfo['senderName']!=""){
									$from = $this->getFrom();
									$from['name'] = $templateInfo['senderName'];
								}

								$replyTo = [];
								if ($templateInfo['replyToAddress']!=""){
									$replyTo['address'] = $templateInfo['replyToAddress'];
								}
								elseif($this->getReplyTo()!="")
								{
									$replyTo['address'] = $this->getReplyTo();
								}

								if($singleMail){
									$key = $user->getEmail()."|".$news->getUid();
								} else {
									$key = $user->getEmail();
								}

								if($singleMail || (!$singleMail && !isset($mails[$key]))){
									$mails[$key]['uid'] = $user->getUid();
									$mails[$key]['type'] = $type;
									$mails[$key]['crdate'] = $user->getCrdate();
									$mails[$key]['gender'] = $user->getGender();
									$mails[$key]['title'] = $user->getTitle();
									$mails[$key]['email'] = $user->getEmail();
									$mails[$key]['isSingle'] = $singleMail;
									$mails[$key]['templateUid'] = $mailTemplateUid;
									$mails[$key]['redirectorType'] = MooxNewsMailerService::$redirectorType;
									$mails[$key]['newsCount'] = 0;
									$mails[$key]['attachments'] = [];

									if($type=="feuser"){
										$mails[$key]['name'] = trim((string)$user->getName());
										$mails[$key]['firstName'] = trim((string)$user->getFirstName());
										$mails[$key]['middleName'] = trim((string)$user->getMiddleName());
										$mails[$key]['lastName'] = trim((string)$user->getLastName());
										if($this->autoLoginUser==-1){
											$mails[$key]['autoLoginUser']['uid'] = $user->getUid();
											$mails[$key]['autoLoginUser']['hash'] = $user->getAutoHash();
										} elseif($this->autoLoginUser>0){
											$autoLoginUser = $this->frontendUserRepository->findByUid($this->autoLoginUser);
											$mails[$key]['autoLoginUser']['uid'] = $autoLoginUser->getUid();
											$mails[$key]['autoLoginUser']['hash'] = $autoLoginUser->getAutoHash();
										}
									} else {
										$mails[$key]['name'] = trim($user->getForename().(($user->getSurname())?" ".$user->getSurname():""));
										$mails[$key]['firstName'] = trim($user->getForename());
										$mails[$key]['middleName'] = "";
										$mails[$key]['lastName'] = trim($user->getSurname());
										if($this->autoLoginUser>0){
											$autoLoginUser = $this->frontendUserRepository->findByUid($this->autoLoginUser);
											if($autoLoginUser)
											{
												$mails[$key]['autoLoginUser']['uid'] = $autoLoginUser->getUid();
												$mails[$key]['autoLoginUser']['hash'] = $autoLoginUser->getAutoHash();
											}
										}
									}

									$mails[$key]['bounceInfo']['receiverUid'] = $user->getUid();
									$mails[$key]['bounceInfo']['receiverType'] = $type;
									$mails[$key]['bounceInfo']['task'] = self::$taskName;

									$infoWritten = true;
								} else {
									$infoWritten = false;
								}

								$key2 = $newsFirstCategory->getUid();

								if($this->detailPid>0){
									$singlePid = $this->detailPid;
								} else {
									$singlePid = $newsFirstCategory->getSinglePid();
								}

								if(isset($this->baseUrls[$singlePid])){
									$baseUrl = $this->baseUrls[$singlePid];
								} else {
									$baseUrl = self::getBaseUrl($singlePid);
									if (substr($baseUrl,(strlen($baseUrl)-1)) == "/"){
										$baseUrl = substr($baseUrl,0,(strlen($baseUrl)-1));
									}
									$this->baseUrls[$singlePid] = $baseUrl;
								}

								if($singleMail || (!$singleMail && !isset($mails[$key]['news'][$key2]))){
									$mails[$key]['news'][$key2]['category'] = $newsFirstCategory->getTitle();
									foreach($newsFirstCategory->getImages() AS $image){
										$mails[$key]['news'][$key2]['categoryImage'] 	= $image;
										$mails[$key]['news'][$key2]['baseUrl'] 			= $baseUrl;
										break;
									}
								}

								$key3 = $news->getUid();

								$newsItem = [];

								$newsItem['baseUrl'] = $baseUrl;
								$newsItem['singlePid'] = $singlePid;

								$url = $newsItem['baseUrl']."/";
								$url .= "?type=".MooxNewsMailerService::$redirectorType;
								$url .= "&tx_mooxmailer_pi1[mode]=news";
								$url .= "&tx_mooxmailer_pi1[id]=".$newsItem['singlePid'];
								$url .= "&tx_mooxmailer_pi1[itemUid]=".$news->getUid();
								if(isset($mails[$key]['autoLoginUser'])){
									$url .= "&tx_mooxmailer_pi1[autoLogin][id]=".$mails[$key]['autoLoginUser']['uid'];
									$url .= "&tx_mooxmailer_pi1[autoLogin][hash]=".$mails[$key]['autoLoginUser']['hash'];
								}
								$newsItem['url'] = $url;
								if($mails[$key]['firstTitle']==""){
									$mails[$key]['firstTitle'] = $news->getTitle();
								}

								if($singleMail){
									$news->setBodytext($this->stripMail($news->getBodytext()));
								} else {
									$news->setBodytext($this->stripMail($this->getShortDescription($news->getTeaser(), $news->getBodytext())));
								}
								$newsItem['fulltext'] = $this->stripMail($news->getBodytext());

								$newsItem['data']  = $news;

								if($infoWritten){
									$mails[$key]['subject'] = $this->helperService->prepareMailSubject($mailSubject,$newsItem['data']);
								}

								$mails[$key]['news'][$key2]['items'][$key3] = $newsItem;
								$mails[$key]['newsCount'] = $mails[$key]['newsCount']+1;

								if($this->attachSelf)
								{
									if(isset($this->attachableFiles[$news->getUid()])){
										$mails[$key]['attachments']["MooxNews".$news->getUid()] = $this->attachableFiles[$news->getUid()];
									}
									else
									{
										$cached = $this->pdfGenerator->isCached($news->getHash());
										if(!($cached && $cached['time']>=$news->getTstamp()->format('U')))
										{
											$pdfUrl = $newsItem['baseUrl']."/";
											$pdfUrl .= "?type=66667";
											$pdfUrl .= "&tx_mooxnews_pi2[action]=pdf";
											$pdfUrl .= "&tx_mooxnews_pi2[controller]=News";
											$pdfUrl .= "&tx_mooxnews_pi2[news]=".$news->getUid();
											$pdfUrl .= "&tx_mooxnews_pi2[hash]=".$news->getHash();
											$pdfUrl .= "&tx_mooxnews_pi2[generate]=".md5($GLOBALS['TYPO3_CONF_VARS']['SYS']['encryptionKey']."moox_mailer");

											$this->helperService->getContentsFromUrl($pdfUrl);

											$pdf = $this->pdfGenerator->getCached($news->getHash(), false, $news->getTitle(), false);
											$attachment = [];
											$attachment['type'] = "MooxNews";
											$attachment['uid'] = $news->getUid();
											$attachment['mime'] = "application/pdf";
											$attachment['name'] = $pdf['filename'].".pdf";
											$attachment['path'] = $pdf['filepath'];
											$this->attachableFiles["MooxNews".$news->getUid()] = $attachment;
											$mails[$key]['attachments'][$news->getUid()] = $attachment;
										}
										else
										{
											$pdf = $this->pdfGenerator->getCached($news->getHash(), false, $news->getTitle(), false);
											$attachment = [];
											$attachment['type'] = "MooxNews";
											$attachment['uid'] = $news->getUid();
											$attachment['mime'] = "application/pdf";
											$attachment['name'] = $pdf['filename'].".pdf";
											$attachment['path'] = $pdf['filepath'];
											$this->attachableFiles["MooxNews".$news->getUid()] = $attachment;
											$mails[$key]['attachments'][$news->getUid()] = $attachment;
										}
									}
								}

								if(!in_array($news->getUid(),$this->affectedNewsUids)){

									$this->affectedNewsUids[] 	= $news->getUid();

									$affectedNews = [];

									$affectedNews['uid'] = $newsItem['uid'];
									$affectedNews['dateTime'] = $newsItem['dateTime'];
									$affectedNews['title'] = $newsItem['title'];
									$affectedNews['teaser'] = $newsItem['teaser'];
									$affectedNews['bodytext'] = $newsItem['bodytext'];
									$affectedNews['fulltext'] = $newsItem['fulltext'];
									$affectedNews['feGroups'] = $newsFeGroups;
									foreach($affectedNews['feGroups'] AS $groupId){
										$group = [];
										$group['title'] = $this->frontendUserGroupRepository->findByUid($groupId)->getTitle();
										$group['uid'] = $groupId;
										$affectedNews['feGroupsExtended'][] = $group;
									}
									$affectedNews['singlePid'] = $newsItem['singlePid'];
									$affectedNews['category'] = $newsFirstCategory;
									$affectedNews['categories'] = $news->getCategories();
									$affectedNews['mooxNewsSeminarStartdate'] = $newsItem['mooxNewsSeminarStartdate'];
									$affectedNews['mooxNewsSeminarEnddate'] = $newsItem['mooxNewsSeminarEnddate'];

									$this->affectedNews[$newsItem['uid']]= $affectedNews;
								}

								if(!in_array($news->getUid(),$this->affectedNewsUidsByEmail)){
									$this->affectedNewsUidsByEmail[$user->getEmail()][] = $news->getUid();
								}
							}
						}
					}

					if(!in_array($user->getEmail(),$this->affectedUsersEmails)){

						$this->affectedUsersEmails[] = $user->getEmail();

						$affectedUser = [];

						$affectedUser['uid'] = $user->getUid();
						$affectedUser['email'] = $user->getEmail();
						$affectedUser['type'] = $type;

						if($type=="feuser"){
							$affectedUser['username'] = trim($user->getUsername());
							$affectedUser['firstName'] = trim(ucfirst($user->getFirstName()));
							$affectedUser['lastName'] = trim(ucfirst($user->getLastName()));
							$affectedUser['feGroups'] = $user->getUsergroup();
							if($affectedUser['feGroups']!=NULL){
								foreach($affectedUser['feGroups'] AS $group){
									$affectedUser['feGroupsUids'][] = $group->getUid();
								}
							}
						} else {
							$affectedUser['username'] = "";
							$affectedUser['firstName'] = trim(ucfirst($user->getForename()));
							$affectedUser['lastName'] = trim(ucfirst($user->getSurname()));
						}
						$affectedUser['sort'] = $affectedUser['lastName'].$affectedUser['firstName'].$affectedUser['username'].$affectedUser['email'];
						$this->affectedUsers[$user->getEmail()]	= $affectedUser;
					}
				}
			}
		}

		usort($this->affectedUsers, array("\Neulandlotsen\MooxMailer\Service\MooxNewsMailerService", "affectedUsersSort"));

		$affectedUsersTmp = $this->affectedUsers;
		$this->affectedUsers = [];
		foreach($affectedUsersTmp AS $affectedUserTmp){
			$this->affectedUsers[$affectedUserTmp['email']] = $affectedUserTmp;
		}
		$affectedUsersTmp = NULL;

		// go through all affected news
		foreach($this->affectedNews AS $news){

			if(!$this->previewMaildataUid || ($this->previewMaildataUid>0 && $this->previewMaildataUid==$news['uid'])){

				if($this->sendSingleMails || $news['category']->getMailerForcesinglemail()){
					$singleMail = true;
				} else {
					$singleMail = false;
				}

				if($singleMail){

					if($this->scheduleUid>0 && $this->forceScheduledTemplateInSingleMailing && $this->schedule['template']['uid']>0){
						$mailTemplateUid = $this->schedule['template']['uid'];
					} elseif($this->templateUid>0 && $this->forceTemplateInSingleMailing){
						$mailTemplateUid = $this->templateUid;
					} elseif($news['category']->getMailerTemplate()>0){
						$mailTemplateUid = $news['category']->getMailerTemplate();
					} else {
						$mailTemplateUid = $this->templateUid;
					}

				} else {

					if($this->scheduleUid>0 && $this->schedule['template']['uid']>0){
						$mailTemplateUid = $this->schedule['template']['uid'];
					} else {
						$mailTemplateUid = $this->templateUid;
					}
				}

				$templateInfo = $this->loadTemplate($mailTemplateUid);

				if($singleMail){
					if(!$this->forceSubject && $news['category']->getMailerSubject()!=""){
						$mailSubject = $news['category']->getMailerSubject();
					} elseif (!$this->forceSubject && $templateInfo['subject']!=""){
						$mailSubject = $templateInfo['subject'];
					} else {
						$mailSubject = $this->subject;
					}
				} else {
					if (!$this->forceSubject && $templateInfo['subject']!=""){
						$mailSubject = $templateInfo['subject'];
					} else {
						$mailSubject = $this->subject;
					}
				}

				$from = [];
				if ($templateInfo['senderAddress']!=""){
					$from = $this->getFrom();
					$from['address'] = $templateInfo['senderAddress'];
				}
				if ($templateInfo['senderName']!=""){
					$from = $this->getFrom();
					$from['name'] = $templateInfo['senderName'];
				}

				$replyTo = [];
				if ($templateInfo['replyToAddress']!=""){
					$replyTo['address'] = $templateInfo['replyToAddress'];
				}
				elseif($this->getReplyTo()!="")
				{
					$replyTo['address'] = $this->getReplyTo();
				}

				if($singleMail){
					$key = "0|".$news['uid'];
				} else {
					$key = "0";
				}

				if($singleMail || (!$singleMail && !isset($previewmails[$key])))
				{
					$previewmails[$key]['gender'] = 0;
					$previewmails[$key]['title'] = "Dr.";
					$previewmails[$key]['name'] = "Hans Mustermann";
					$previewmails[$key]['firstName'] = "Hans";
					$previewmails[$key]['middleName'] = "Jürgen";
					$previewmails[$key]['lastName'] = "Mustermann";
					$previewmails[$key]['email'] = "email@example.net";
					$previewmails[$key]['isSingle'] = $singleMail;
					$previewmails[$key]['templateUid'] = $mailTemplateUid;
					$previewmails[$key]['redirectorType'] = MooxNewsMailerService::$redirectorType;
					$previewmails[$key]['newsCount'] = 0;

					if($type=="feuser" && $this->autoLoginUser==-1)
					{
						$previewmails[$key]['autoLoginUser']['uid'] = $user->getUid();
						$previewmails[$key]['autoLoginUser']['hash'] = $user->getAutoHash();
					}
					elseif($this->autoLoginUser>0)
					{
						$autoLoginUser = $this->frontendUserRepository->findByUid($this->autoLoginUser);
						if($autoLoginUser)
						{
							$previewmails[$key]['autoLoginUser']['uid'] = $autoLoginUser->getUid();
							$previewmails[$key]['autoLoginUser']['hash'] = $autoLoginUser->getAutoHash();
						}
					}
					$infoWritten = true;
				}
				else
				{
					$infoWritten = false;
				}

				$key2 = $news['category']->getUid();

				if(isset($this->baseUrls[$news['singlePid']])){
					$baseUrl = $this->baseUrls[$news['singlePid']];
				} else {
					$baseUrl = self::getBaseUrl($news['singlePid']);
					if (substr($baseUrl,(strlen($baseUrl)-1)) == "/"){
						$baseUrl = substr($baseUrl,0,(strlen($baseUrl)-1));
					}
					$this->baseUrls[$news['singlePid']] = $baseUrl;
				}

				if($singleMail || (!$singleMail && !isset($previewmails[$key]['news'][$key2]))){
					$previewmails[$key]['news'][$key2]['category'] = $news['category']->getTitle();
					foreach($news['category']->getImages() AS $image){
						$previewmails[$key]['news'][$key2]['categoryImage'] = $image;
						$previewmails[$key]['news'][$key2]['baseUrl'] = $baseUrl;
						break;
					}
				}

				$key3 = $news['uid'];

				$newsItem = [];

				$newsItem['mooxNewsSeminarStartdate'] = $news['mooxNewsSeminarStartdate'];
				$newsItem['mooxNewsSeminarEnddate'] = $news['mooxNewsSeminarEnddate'];
				$newsItem['uid'] = $news['uid'];
				$newsItem['singlePid'] = $singlePid;
				$newsItem['baseUrl'] = $baseUrl;
				$url = $newsItem['baseUrl']."/";
				$url .= "?type=".MooxNewsMailerService::$redirectorType;
				$url .= "&tx_mooxmailer_pi1[mode]=news";
				if($newsItem['singlePid']>0){
					$url .= "&tx_mooxmailer_pi1[id]=".$newsItem['singlePid'];
				}
				$url .= "&tx_mooxmailer_pi1[itemUid]=".$newsItem['uid'];
				if(isset($previewmails[$key]['autoLoginUser'])){
					$url .= "&tx_mooxmailer_pi1[autoLogin][id]=".$previewmails[$key]['autoLoginUser']['uid'];
					$url .= "&tx_mooxmailer_pi1[autoLogin][hash]=".$previewmails[$key]['autoLoginUser']['hash'];
				}
				$newsItem['url'] = $url;
				$newsItem['dateTime'] = $news['dateTime'];
				$newsItem['title'] = $news['title'];
				if($previewmails[$key]['firstTitle']==""){
					$previewmails[$key]['firstTitle'] = $newsItem['title'];
				}
				$newsItem['teaser'] = $news['teaser'];

				if($singleMail){
					$newsItem['bodytext'] = $this->stripMail($news['bodytext']);
				} else {
					$newsItem['bodytext'] = $this->stripMail($this->getShortDescription($news['teaser'], $news['bodytext']));
				}

				$newsItem['fulltext'] = $news['fulltext'];

				if($infoWritten){
					$previewmails[$key]['subject'] = $this->helperService->prepareMailSubject($mailSubject,$newsItem);
				}

				$previewmails[$key]['news'][$key2]['items'][$key3] = $newsItem;
				$previewmails[$key]['newsCount'] = $previewmails[$key]['newsCount']+1;
			}
		}

		$index = 1;

		foreach($previewmails AS $mail){

			$this->previewMaildata[] = [
				"to" => [
					"address" => $mail['email'],
					"name" 		=> $mail['name'],
					"uid" 		=> $mail['uid'],
					"type" 		=> $mail['type'],
					"hash" 		=> md5($mail['uid'].(is_object($mail['crdate'])?$mail['crdate']->format("U"):$mail['crdate']).$mail['type'].self::$trackingSalt)
				],
				"from" => $from,
				"reply-to" => $replyTo,
				"subject" => $mail['subject'],
				"body" => $this->getEmailBody($mail["templateUid"],$mail),
				"attachments" => $mail['attachments'],
				"index"	=> $index,
				//"mail" => $mail

			];

			$index++;

			$this->resetTimeout();
		}

		$index = 1;

		foreach($mails AS $mail)
		{
			$maildata[] = [
				"to" => [
					"address" => $mail['email'],
					"name" => $mail['name'],
					"uid" => $mail['uid'],
					"type" => $mail['type'],
					"hash" => md5($mail['uid'].(is_object($mail['crdate'])?$mail['crdate']->format("U"):$mail['crdate']).$mail['type'].self::$trackingSalt)
				],
				"from" => $from,
				"reply-to" => $replyTo,
				"bounceInfo" => $mail['bounceInfo'],
				"subject" => $mail['subject'],
				"body" => $this->getEmailBody($mail["templateUid"],$mail),
				"attachments" => $mail['attachments'],
				"index"	=> $index,
				"jobType" => self::$taskName,
				//"mail" => $mail
			];

			$index++;

			$this->resetTimeout();
		}

		return $maildata;
	}

	/**
	 * mark all processed mails as "do not send" (mailer_frequency = 0)
	 */
	private function setMailsAsProcessed()
	{
		// skip this action needed
		if($this->skipMailActions){
			return;
		}

		$queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable('tx_mooxnews_domain_model_news');
		$queryBuilder
			->update('tx_mooxnews_domain_model_news')
			->where(
				$queryBuilder->expr()->in('uid', $queryBuilder->createNamedParameter($this->processedNews, Connection::PARAM_INT_ARRAY))
			)
			->set('mailer_frequency', 0)
			->execute();

	}

	/**
	 * process moox news mailer job
	 *
	 * @return array $state
	 */
	public function process($preparedMaildata = [])
	{
		if((!is_array($preparedMaildata) || count($preparedMaildata)<1)){
			$preparedMaildata = $this->prepareMaildata();
		}

		if($preparedMaildata["newsLimit"]){
			$newsLimit = $preparedMaildata["newsLimit"];
			$newsCount = $preparedMaildata["newsCount"];
			$preparedMaildata = [];
		}

		$process = parent::process($preparedMaildata);

		$this->setMailsAsProcessed();

		return array (
			"successCnt" 	=> $process['successCnt'],
			"errorCnt" 		=> $process['errorCnt'],
			"processedCnt" 	=> $process['processedCnt'],
			"preparedCnt" 	=> $process['preparedCnt'],
			"newsLimit" 	=> $newsLimit,
			"newsCount" 	=> $newsCount,
		);
	}

	/**
	 * @return array $sendModes
	 */
	public function getSendModes()
	{
		$modes = [];
		$modes[0]['value'] = "cumulated";
		$modes[0]['title'] = "Kumulierter Versand (Alles in einer Mail)";
		$modes[1]['value'] = "single";
		$modes[1]['title'] = "Einzelversand (Eine Mail pro News)";

		return $modes;
	}

	/**
	 * @return string $sendMode
	 */
	public function getSendMode()
	{
		return $this->sendMode;
	}

	/**
	 * @param string $sendMode send mode
	 */
	public function setSendMode($sendMode)
	{
		$this->sendMode = $sendMode;
	}

	/**
	 * @return bool
	 */
	public function getSendSingleMails()
	{
		return $this->sendSingleMails;
	}

	/**
	 * @param bool $sendSingleMails send single mails
	 */
	public function setSendSingleMails($sendSingleMails)
	{
		$this->sendSingleMails = $sendSingleMails;
	}

	/**
	 * @return bool
	 */
	public function getForceSubject()
	{
		return $this->forceSubject;
	}

	/**
	 * @param bool $forceSubject force subject
	 */
	public function setForceSubject($forceSubject)
	{
		$this->forceSubject = $forceSubject;
	}

	/**
	 * @return bool
	 */
	public function getForceTemplateInSingleMailing()
	{
		return $this->forceTemplateInSingleMailing;
	}

	/**
	 * @param bool $forceTemplateInSingleMailing force template in single mailing
	 */
	public function setForceTemplateInSingleMailing($forceTemplateInSingleMailing)
	{
		$this->forceTemplateInSingleMailing = $forceTemplateInSingleMailing;
	}

	/**
	 * @return bool
	 */
	public function getForceScheduledTemplateInSingleMailing()
	{
		return $this->forceScheduledTemplateInSingleMailing;
	}

	/**
	 * @param bool $forceScheduledTemplateInSingleMailing force template in single mailing
	 */
	public function setForceScheduledTemplateInSingleMailing($forceScheduledTemplateInSingleMailing)
	{
		$this->forceScheduledTemplateInSingleMailing = $forceScheduledTemplateInSingleMailing;
	}

	/**
	 * Returns news pids
	 *
	 * @return array $newsPids
	 */
	public function getNewsPids()
	{
		return $this->newsPids;
	}

	/**
	 * @param array $newsPids news pids
	 */
	public function setNewsPids($newsPids)
	{
		$this->newsPids = $newsPids;
	}

	/**
	 * @return array $userPids
	 */
	public function getUserPids()
	{
		return $this->userPids;
	}

	/**
	 * @param array $userPids user pids
	 */
	public function setUserPids($userPids)
	{
		$this->userPids = $userPids;
	}

	/**
	 * @return array
	 */
	public function getAddressesPids()
	{
		return $this->addressesPids;
	}

	/**
	 * @param array $addressesPids addresses pids
	 */
	public function setAddressesPids($addressesPids)
	{
		$this->addressesPids = $addressesPids;
	}

	/**
	 * @return array
	 */
	public function getAddressesCategories()
	{
		return $this->addressesCategories;
	}

	/**
	 * @param array $addressesCategories
	 */
	public function setAddressesCategories($addressesCategories)
	{
		$this->addressesCategories = $addressesCategories;
	}

	/**
	 * @return array $excludedUserGroups
	 */
	public function getExcludedUserGroups()
	{
		return $this->excludedUserGroups;
	}

	/**
	 * @param array $excludedUserGroups excluded user groups
	 */
	public function setExcludedUserGroups($excludedUserGroups)
	{
		$this->excludedUserGroups = $excludedUserGroups;
	}

	/**
	 * @return array $categoriesUids
	 */
	public function getCategoriesUids()
	{
		return $this->categoriesUids;
	}

	/**
	 * @param array $categoriesUids categories uids
	 * @return void
	 */
	public function setCategoriesUids($categoriesUids)
	{
		$this->categoriesUids = $categoriesUids;
	}

	/**
	 * @return string
	 */
	public function getSimulatedUserGroups()
	{
		return $this->simulatedUserGroups;
	}

	/**
	 * Set the simulated user groups
	 *
	 * @param string $simulatedUserGroups simulated user groups
	 * @return void
	 */
	public function setSimulatedUserGroups($simulatedUserGroups)
	{
		$this->simulatedUserGroups = $simulatedUserGroups;
	}

	/**
	 * @return bool
	 */
	public function getSendRestrictedNewsOnly()
	{
		return $this->sendRestrictedNewsOnly;
	}

	/**
	 * Set send restricted news only
	 *
	 * @param bool $sendRestrictedNewsOnly send restricted news only
	 * @return void
	 */
	public function setSendRestrictedNewsOnly($sendRestrictedNewsOnly)
	{
		$this->sendRestrictedNewsOnly = $sendRestrictedNewsOnly;
	}

	/**
	 * @return bool
	 */
	public function getSendToUsersWithGroupsOnly()
	{
		return $this->sendToUsersWithGroupsOnly;
	}

	/**
	 * @param bool $sendToUsersWithGroupsOnly send to users with groups only
	 */
	public function setSendToUsersWithGroupsOnly($sendToUsersWithGroupsOnly)
	{
		$this->sendToUsersWithGroupsOnly = $sendToUsersWithGroupsOnly;
	}

	/**
	 * @return array $previewMaildata
	 */
	public function getPreviewMaildata()
	{
		return $this->previewMaildata;
	}

	/**
	 * @param array $previewMaildata preview maildata
	 */
	public function setPreviewMaildata($previewMaildata)
	{
		$this->previewMaildata = $previewMaildata;
	}

	/**
	 * @return int $previewMaildataUid
	 */
	public function getPreviewMaildataUid()
	{
		return $this->previewMaildataUid;
	}

	/**
	 * @param int $previewMaildataUid preview maildata uid
	 */
	public function setPreviewMaildataUid($previewMaildataUid)
	{
		$this->previewMaildataUid = $previewMaildataUid;
	}

	/**
	 * @return int $previewMaildataUser
	 */
	public function getPreviewMaildataUser()
	{
		return $this->previewMaildataUser;
	}

	/**
	 * @param int $previewMaildataUser preview maildata user
	 */
	public function setPreviewMaildataUser($previewMaildataUser)
	{
		$this->previewMaildataUser = $previewMaildataUser;
	}

	/**
	 * @return array $affectedUsers
	 */
	public function getAffectedUsers()
	{
		return $this->affectedUsers;
	}

	/**
	 * @param array $affectedUsers affected users
	 */
	public function setAffectedUsers($affectedUsers)
	{
		$this->affectedUsers = $affectedUsers;
	}

	/**
	 * @return array $affectedUsersEmails
	 */
	public function getAffectedUsersEmails()
	{
		return $this->affectedUsersEmails;
	}

	/**
	 * Set affected users uids
	 *
	 * @param array $affectedUsersEmails affected users
	 */
	public function setAffectedUsersEmails($affectedUsersEmails)
	{
		$this->affectedUsersEmails = $affectedUsersEmails;
	}

	/**
	 * @return array $affectedNews
	 */
	public function getAffectedNews()
	{
		return $this->affectedNews;
	}

	/**
	 * Set affected news
	 *
	 * @param array $affectedNews affected news
	 */
	public function setAffectedNews($affectedNews)
	{
		$this->affectedNews = $affectedNews;
	}

	/**
	 * @return array $affectedNewsUids
	 */
	public function getAffectedNewsUids()
	{
		return $this->affectedNewsUids;
	}

	/**
	 * @param array $affectedNewsUids affected news
	 */
	public function setAffectedNewsUids($affectedNewsUids)
	{
		$this->affectedNewsUids = $affectedNewsUids;
	}

	/**
	 * Returns affected news uids grouped by fe-user
	 *
	 * @return array $affectedNewsUidsByEmail
	 */
	public function getAffectedNewsUidsByEmail()
	{
		return $this->affectedNewsUidsByEmail;
	}

	/**
	 * Set affected news uids grouped by fe-user
	 *
	 * @param array $affectedNewsUidsByEmail affected news
	 */
	public function setAffectedNewsUidsByEmail($affectedNewsUidsByEmail)
	{
		$this->affectedNewsUidsByEmail = $affectedNewsUidsByEmail;
	}

	/**
	 * @return int
	 */
	public function getDetailPid()
	{
		return $this->detailPid;
	}

	/**
	 * @param int $detailPid
	 */
	public function setDetailPid($detailPid)
	{
		$this->detailPid = $detailPid;
	}

	/**
	 * @return int
	 */
	public function getLoginPid()
	{
		return $this->loginPid;
	}

	/**
	 * @param int $loginPid
	 */
	public function setLoginPid($loginPid)
	{
		$this->loginPid = $loginPid;
	}

	/**
	 * @return int
	 */
	public function getAutoLoginUser()
	{
		return $this->autoLoginUser;
	}

	/**
	 * @param bool $autoLoginUser auto login user
	 */
	public function setAutoLoginUser($autoLoginUser)
	{
		$this->autoLoginUser = $autoLoginUser;
	}

	/**
	 * @return bool
	 */
	public function getRespectTags()
	{
		return $this->respectTags;
	}

	/**
	 * @param string $respectTags
	 */
	public function setRespectTags($respectTags)
	{
		$this->respectTags = $respectTags;
	}

	/**
	 * @return bool
	 */
	public function getRespectZipregions()
	{
		return $this->respectZipregions;
	}

	/**
	 * @param bool $respectZipregions
	 */
	public function setRespectZipregions($respectZipregions)
	{
		$this->respectZipregions = $respectZipregions;
	}

	/**
	 * @return bool
	 */
	public function getAttachRelated()
	{
		return $this->attachRelated;
	}

	/**
	 * @param bool $attachRelated
	 */
	public function setAttachRelated($attachRelated)
	{
		$this->attachRelated = $attachRelated;
	}

	/**
	 * @return bool
	 */
	public function getAttachSelf()
	{
		return $this->attachSelf;
	}

	/**
	 * @param bool $attachSelf
	 */
	public function setAttachSelf($attachSelf)
	{
		$this->attachSelf = $attachSelf;
	}

	/**
	 * @return bool
	 */
	public function getDirectNews()
	{
		return $this->directNews;
	}

	/**
	 * @param bool $directNews
	 */
	public function setDirectNews($directNews)
	{
		$this->directNews = $directNews;
	}

	/**
	 * @return int
	 */
	public function getNewsLimit()
	{
		return $this->newsLimit;
	}

	/**
	 * @param int $directNews
	 */
	public function setNewsLimit($newsLimit)
	{
		$this->newsLimit = $newsLimit;
	}
}
?>