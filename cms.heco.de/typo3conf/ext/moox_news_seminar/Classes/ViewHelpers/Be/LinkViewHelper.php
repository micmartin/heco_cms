<?php
namespace Neulandlotsen\MooxNewsSeminar\ViewHelpers\Be;

/**
 * This file is part of the "moox_news_seminar" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

use TYPO3\CMS\Backend\Routing\UriBuilder;
use TYPO3\CMS\Backend\Utility\BackendUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Utility\LocalizationUtility;
use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper;
use TYPO3Fluid\Fluid\Core\Rendering\RenderingContextInterface;
use TYPO3Fluid\Fluid\Core\ViewHelper\Traits\CompileWithRenderStatic;

/**
 * Displays 'Edit link' with sprite icon to add or edit category
 *
 * @internal
 */
class LinkViewHelper extends AbstractViewHelper
{
    use CompileWithRenderStatic;

    /**
     * As this ViewHelper renders HTML, the output must not be escaped.
     *
     * @var bool
     */
    protected $escapeOutput = false;

    /**
     * Initializes the arguments
     */
    public function initializeArguments()
    {
        parent::initializeArguments();
		$this->registerArgument('action', 'string', 'action to perform record to', true);
        $this->registerArgument('table', 'string', 'table to add record to', true);
		$this->registerArgument('variant', 'string', 'table to add record to', false);
		$this->registerArgument('storagePage', 'integer', 'page/folder to add record to', false);
		$this->registerArgument('uid', 'integer', 'uid of record', false);		
		$this->registerArgument('returnUrl', 'string', 'return to this url after operation', true);
    }

    /**
     * Render link with sprite icon to remove user
     *
     * @param array $arguments
     * @param \Closure $renderChildrenClosure
     * @param RenderingContextInterface $renderingContext
     *
     * @return string
     */
    public static function renderStatic(array $arguments, \Closure $renderChildrenClosure, RenderingContextInterface $renderingContext)
    {
		$urlParameters = [];
		
		if(in_array($arguments['action'],['add','edit']))
		{
			if($arguments['action'] == 'add')
			{
				$urlParameters['edit['.$arguments['table'].']['.$arguments['storagePage'].']'] = 'new';				
			}
			elseif($arguments['action'] == 'edit')
			{
				$urlParameters['edit['.$arguments['table'].']['.$arguments['uid'].']'] = 'edit';				
			} 
						
			if($arguments['variant']!="")
			{
				$urlParameters['defVals['.$arguments['table'].'][variant]'] = $arguments['variant'];
			}		
			$urlParameters['returnUrl'] = $arguments['returnUrl']?$arguments['returnUrl']:GeneralUtility::getIndpEnv('REQUEST_URI');

            $uriBuilder = GeneralUtility::makeInstance(UriBuilder::class);
			
			return htmlspecialchars(
                $uriBuilder->buildUriFromRoute('record_edit', $urlParameters)
			);
		}
		
		return "#";
		
    }
}