<?php
namespace Neulandlotsen\MooxMailer\Domain\Model;

/**
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */

use TYPO3\CMS\Extbase\DomainObject\AbstractEntity;
use TYPO3\CMS\Extbase\Persistence\ObjectStorage; 
 
class Bounce extends AbstractEntity
{
	
	/**
	 * @var DateTime
	 */
	protected $crdate;

	/**
	 * @var DateTime
	 */
	protected $tstamp;
	
	/**
	 * Sichtbarkeit
	 *
	 * @var int
	 */
	protected $hidden;		
	
	/**
	 * fe user
	 *
	 * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Neulandlotsen\MooxMailer\Domain\Model\FrontendUser>
	 */
	protected $feUser = NULL;
	
	/**
	 * address
	 *
	 * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Neulandlotsen\MooxMailer\Domain\Model\Address>
	 */
	protected $address = NULL;
	
	/**
	 * Titel
	 *
	 * @var string
	 * @TYPO3\CMS\Extbase\Annotation\Validate("NotEmpty")
	 */
	protected $title;
	
	/**
	 * action
	 *
	 * @var string
	 */
	protected $action;	
	
	/**
	 * code
	 *
	 * @var string
	 */
	protected $code;	
	
	/**
	 * type
	 *
	 * @var string
	 */
	protected $type;
	
	/**
	 * type description
	 *
	 * @var string
	 */
	protected $typeDescription;
	
	/**
	 * status
	 *
	 * @var string
	 */
	protected $status;
	
	/**
	 * status description
	 *
	 * @var string
	 */
	protected $statusDescription;
	
	/**
	 * recipient
	 *
	 * @var string
	 */
	protected $recipient;
	
	/**
	 * mail
	 *
	 * @var string
	 */
	protected $mail;
	
	/**
	 * job
	 *
	 * @var string
	 */
	protected $job;
	
	/**
	 * job type
	 *
	 * @var string
	 */
	protected $jobType;
	
	/**
	 * receiver type
	 *
	 * @var string
	 */
	protected $receiverType;
	
	/**
	 * receiver uid
	 *
	 * @var int
	 */
	protected $receiverUid;
	
	/**
	 * sent
	 *
	 * @var int
	 */
	protected $sent;
	
	/**
	 * bounced
	 *
	 * @var int
	 */
	protected $bounced;
	
	/**
	 * __construct
	 */
	public function __construct()
	{
		//Do not remove the next line: It would break the functionality
		$this->initStorageObjects();
	}

	/**
	 * Initializes all ObjectStorage properties
	 * Do not modify this method!
	 *
	 * @return void
	 */
	protected function initStorageObjects()
	{	
		$this->feUser = new ObjectStorage();
		$this->address = new ObjectStorage();
	}
	
	/**
	 * Get creation date
	 *
	 * @return int
	 */
	public function getCrdate()
	{
		return $this->crdate;
	}

	/**
	 * Set creation date
	 *
	 * @param int $crdate
	 * @return void
	 */
	public function setCrdate($crdate)
	{
		$this->crdate = $crdate;
	}

	/**
	 * Get year of crdate
	 *
	 * @return int
	 */
	public function getYearOfCrdate()
	{
		return $this->getCrdate()->format('Y');
	}

	/**
	 * Get month of crdate
	 *
	 * @return int
	 */
	public function getMonthOfCrdate()
	{
		return $this->getCrdate()->format('m');
	}

	/**
	 * Get day of crdate
	 *
	 * @return int
	 */
	public function getDayOfCrdate()
	{
		return (int)$this->crdate->format('d');
	}

	/**
	 * Get timestamp
	 *
	 * @return int
	 */
	public function getTstamp()
	{
		return $this->tstamp;
	}

	/**
	 * Set time stamp
	 *
	 * @param int $tstamp time stamp
	 * @return void
	 */
	public function setTstamp($tstamp)
	{
		$this->tstamp = $tstamp;
	}
	
	/**
	 * Returns the hidden
	 *
	 * @return int $hidden
	 */
	public function getHidden()
	{
		return $this->hidden;
	}

	/**
	 * Sets the hidden
	 *
	 * @param int $hidden
	 * @return void
	 */
	public function setHidden($hidden)
	{
		$this->hidden = $hidden;
	}
	
	/**
	 * Adds a fe user
	 *
	 * @param \Neulandlotsen\MooxMailer\Domain\Model\FrontendUser $feUser
	 * @return void
	 */
	public function addFeUser(\Neulandlotsen\MooxMailer\Domain\Model\FrontendUser $feUser)
	{
		$this->feUser->attach($feUser);
	}

	/**
	 * Removes a fe user
	 *
	 * @param \Neulandlotsen\MooxMailer\Domain\Model\FrontendUser $feUserToRemove The fe user to be removed
	 * @return void
	 */
	public function removeFeUser(\Neulandlotsen\MooxMailer\Domain\Model\FrontendUser $feUserToRemove)
	{
		$this->feUser->detach($feUserToRemove);
	}

	/**
	 * Returns the fe user
	 *
	 * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Neulandlotsen\MooxMailer\Domain\Model\FrontendUser> $feUser
	 */
	public function getFeUser()
	{
		return $this->feUser;
	}

	/**
	 * Sets the fe user
	 *
	 * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Neulandlotsen\MooxMailer\Domain\Model\FrontendUser> $feUser
	 * @return void
	 */
	public function setFeUser(ObjectStorage $feUser)
	{
		$this->feUser = $feUser;
	}
	
	/**
	 * Adds a address
	 *
	 * @param \Neulandlotsen\MooxMailer\Domain\Model\Address $address
	 * @return void
	 */
	public function addAddress(\Neulandlotsen\MooxMailer\Domain\Model\Address $address)
	{
		$this->address->attach($address);
	}

	/**
	 * Removes a address
	 *
	 * @param \Neulandlotsen\MooxMailer\Domain\Model\Address $addressToRemove The address to be removed
	 * @return void
	 */
	public function removeAddress(\Neulandlotsen\MooxMailer\Domain\Model\Address $addressToRemove)
	{
		$this->address->detach($addressToRemove);
	}

	/**
	 * Returns the address
	 *
	 * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Neulandlotsen\MooxMailer\Domain\Model\Address> $address
	 */
	public function getAddress()
	{
		return $this->address;
	}

	/**
	 * Sets the address
	 *
	 * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Neulandlotsen\MooxMailer\Domain\Model\Address> $address
	 * @return void
	 */
	public function setAddress(ObjectStorage $address)
	{
		$this->address = $address;
	}
	
	/**
	 * Returns the title
	 *
	 * @return string $title
	 */
	public function getTitle()
	{
		return $this->title;
	}
	
	/**
	 * Sets the title
	 *
	 * @param string $title
	 * @return void
	 */
	public function setTitle($title)
	{
		$this->title = $title;
	}	

	/**
	 * Returns the action
	 *
	 * @return string $action
	 */
	public function getAction()
	{
		return $this->action;
	}

	/**
	 * Sets the action
	 *
	 * @param string $action
	 * @return void
	 */
	public function setAction($action)
	{
		$this->action = $action;
	}
	
	/**
	 * Returns the code
	 *
	 * @return string $code
	 */
	public function getCode()
	{
		return $this->code;
	}

	/**
	 * Sets the code
	 *
	 * @param string $code
	 * @return void
	 */
	public function setCode($code)
	{
		$this->code = $code;
	}
	
	/**
	 * Returns the major code
	 *
	 * @return string $majorCode
	 */
	public function getMajorCode()
	{
		return $this->majorCode;
	}

	/**
	 * Sets the major code
	 *
	 * @param string $majorCode
	 * @return void
	 */
	public function setMajorCode($majorCode)
	{
		$this->majorCode = $majorCode;
	}
	
	/**
	 * Returns the sub code
	 *
	 * @return string $subCode
	 */
	public function getSubCode()
	{
		return $this->subCode;
	}

	/**
	 * Sets the sub code
	 *
	 * @param string $subCode
	 * @return void
	 */
	public function setSubCode($subCode)
	{
		$this->subCode = $subCode;
	}
	
	/**
	 * Returns the type
	 *
	 * @return string $type
	 */
	public function getType()
	{
		return $this->type;
	}

	/**
	 * Sets the type
	 *
	 * @param string $type
	 * @return void
	 */
	public function setType($type)
	{
		$this->type = $type;
	}
	
	/**
	 * Returns the type description
	 *
	 * @return string $typeDescription
	 */
	public function getTypeDescription()
	{
		return $this->typeDescription;
	}

	/**
	 * Sets the typeDescription
	 *
	 * @param string $typeDescription
	 * @return void
	 */
	public function setTypeDescription($typeDescription)
	{
		$this->typeDescription = $typeDescription;
	}
	
	/**
	 * Returns the status
	 *
	 * @return string $status
	 */
	public function getStatus()
	{
		return $this->status;
	}

	/**
	 * Sets the status
	 *
	 * @param string $status
	 * @return void
	 */
	public function setStatus($status)
	{
		$this->status = $status;
	}
	
	/**
	 * Returns the status description
	 *
	 * @return string $statusDescription
	 */
	public function getStatusDescription()
	{
		return $this->statusDescription;
	}

	/**
	 * Sets the statusDescription
	 *
	 * @param string $statusDescription
	 * @return void
	 */
	public function setStatusDescription($statusDescription)
	{
		$this->statusDescription = $statusDescription;
	}
	
	/**
	 * Returns the recipient
	 *
	 * @return string $recipient
	 */
	public function getRecipient()
	{
		return $this->recipient;
	}

	/**
	 * Sets the recipient
	 *
	 * @param string $recipient
	 * @return void
	 */
	public function setRecipient($recipient)
	{
		$this->recipient = $recipient;
	}
	
	/**
	 * Returns the mail
	 *
	 * @return string $mail
	 */
	public function getMail()
	{
		return $this->mail;
	}

	/**
	 * Sets the mail
	 *
	 * @param string $mail
	 * @return void
	 */
	public function setMail($mail)
	{
		$this->mail = $mail;
	}
	
	/**
	 * Returns the job
	 *
	 * @return string $job
	 */
	public function getJob()
	{
		return $this->job;
	}

	/**
	 * Sets the job
	 *
	 * @param string $job
	 * @return void
	 */
	public function setJob($job)
	{
		$this->job = $job;
	}
	
	/**
	 * Returns the job type
	 *
	 * @return string $jobType
	 */
	public function getJobType()
	{
		return $this->jobType;
	}

	/**
	 * Sets the job type
	 *
	 * @param string $jobType
	 * @return void
	 */
	public function setJobType($jobType)
	{
		$this->jobType = $jobType;
	}
	
	/**
	 * Returns the receiver type
	 *
	 * @return string $receiverType
	 */
	public function getReceiverType()
	{
		return $this->receiverType;
	}

	/**
	 * Sets the receiver type
	 *
	 * @param string $receiverType
	 * @return void
	 */
	public function setReceiverType($receiverType)
	{
		$this->receiverType = $receiverType;
	}
	
	/**
	 * Returns the receiver uid
	 *
	 * @return int $receiverUid
	 */
	public function getReceiverUid()
	{
		return $this->receiverUid;
	}

	/**
	 * Sets the receiver uid
	 *
	 * @param int $receiverUid
	 * @return void
	 */
	public function setReceiverUid($receiverUid)
	{
		$this->receiverUid = $receiverUid;
	}
	
	/**
	 * Returns the sent
	 *
	 * @return int $sent
	 */
	public function getSent()
	{
		return $this->sent;
	}

	/**
	 * Sets the sent
	 *
	 * @param int $sent
	 * @return void
	 */
	public function setSent($sent)
	{
		$this->sent = $sent;
	}
	
	/**
	 * Returns the bounced
	 *
	 * @return int $bounced
	 */
	public function getBounced()
	{
		return $this->bounced;
	}

	/**
	 * Sets the bounced
	 *
	 * @param int $bounced
	 * @return void
	 */
	public function setBounced($bounced)
	{
		$this->bounced = $bounced;
	}
}
?>