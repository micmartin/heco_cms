<?php
$EM_CONF[$_EXTKEY] = [
	'title' => 'MOOX Seminar for MOOX News',
	'description' => 'Erweiterung von MOOX-News mit seminarspezifischen Feldern und Funktionen',
	'category' => 'plugin',
	'state' => 'beta',
	'uploadfolder' => false,
	'clearCacheOnLoad' => true,
	'author' => 'Dominic Martin',
	'author_email' => 'typo3@neulandlotsen.de',
	'author_company' => 'Neulandlotsen',
	'version' => '9.5.2',
	'constraints' => [
		'depends' => [
			'moox_news' => '9.5.0-9.5.99',
		],
		'conflicts' => [],
		'suggests' => [],
	],
	'autoload' => [
		'psr-4' => [
			'Neulandlotsen\\MooxNewsSeminar\\' => 'Classes'
		]
	]
];
?>