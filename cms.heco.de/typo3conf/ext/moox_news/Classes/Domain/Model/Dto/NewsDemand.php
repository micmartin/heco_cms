<?php
namespace Neulandlotsen\MooxNews\Domain\Model\Dto;

/**
 * This file is part of the "moox_news" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */
use Neulandlotsen\MooxNews\Domain\Model\DemandInterface;
use TYPO3\CMS\Extbase\DomainObject\AbstractEntity;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * News Demand object which holds all information to get the correct news records.
 */
class NewsDemand extends AbstractEntity implements DemandInterface
{

    /**
     * @var array
     */
    protected $categories;

    /**
     * @var string
     */
    protected $categoryConjunction;

    /**
     * @var bool
     */
    protected $includeSubCategories = false;

    /**
     * @var string
     */
    protected $author;

    /**
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage
     */
	protected $tags;

    /**
     * @var string
     */
    protected $archiveRestriction;

    /**
     * @var string
     */
    protected $timeRestriction = null;

    /**
     * @var string
     */
    protected $timeRestrictionHigh = null;

    /**
     * @var int
     */
    protected $topNewsRestriction;

    /**
     * @var string
     */
    protected $dateField;

    /**
     * @var int
     */
    protected $month;

    /**
     * @var int
     */
    protected $year;

    /**
     * @var int
     */
    protected $day;

    /**
     * @var string
     */
    protected $calendar;
	
	/**
     * @var string
     */
    protected $calendarFrom;
	
	/**
     * @var string
     */
    protected $calendarTo;

    /**
     * @var string
     */
    protected $searchFields;

    /**
     * @var \Neulandlotsen\MooxNews\Domain\Model\Dto\Search
     */
	protected $search;

   	/**
     * @var string
     */
    protected $order;

    /**
     * @var string
     */
    protected $orderByAllowed;

    /**
     * @var bool
     */
    protected $topNewsFirst;

    /**
     * @var int
     */
    protected $storagePage;

    /**
	 * @var array
	 */
	protected $uids;

    /**
     * @var int
     */
    protected $limit;

    /**
     * @var int
     */
    protected $offset;

	/**
     * @var string
     */
	protected $query;
	
	/**
     * @var bool
     */
	protected $includeOnlySelectedNewsInRss;

    /**
     * @var bool
     */
    protected $excludeAlreadyDisplayedNews;

	/**
     * @var bool
     */
	protected $excludeFromRss;

	/**
     * @var string
     */
	protected $tag;
	
	/**
	 * @var array
	 */
	protected $targets;
	
	/**
     * @var string
     */
	protected $target;
	
	/**
     * @var int
     */
	protected $sysLanguageUid;

    /**
     * @var array
     */
	protected $types = [];
	
	/**
     * @var string
     */
	protected $type;
	
	/**
     * @var string
     */
	protected $char;
	
	/**
     * @var int
     */
	protected $privacy;
	
	/**
     * @var array
     */
	public $charList;
	
	/**
     * @var string
     */
    protected $hideIdList;

    /**
     * @var string
     */
    protected $action;

    /**
     * @var string
     */
    protected $class;

    /**
     * Set archive settings
     *
     * @param string $archiveRestriction archive setting
     * @return NewsDemand
     */
    public function setArchiveRestriction($archiveRestriction)
    {
        $this->archiveRestriction = $archiveRestriction;
        return $this;
    }

    /**
     * Get archive setting
     *
     * @return string
     */
    public function getArchiveRestriction()
    {
        return $this->archiveRestriction;
    }

    /**
     * List of allowed categories
     *
     * @param array $categories categories
     * @return NewsDemand
     */
    public function setCategories($categories)
    {
        $this->categories = $categories;
        return $this;
    }

    /**
     * Get allowed categories
     *
     * @return array
     */
    public function getCategories()
    {
        return $this->categories;
    }

    /**
     * Set category mode
     *
     * @param string $categoryConjunction
     * @return NewsDemand
     */
    public function setCategoryConjunction($categoryConjunction)
    {
        $this->categoryConjunction = $categoryConjunction;
        return $this;
    }

    /**
     * Get category mode
     *
     * @return string
     */
    public function getCategoryConjunction()
    {
        return $this->categoryConjunction;
    }

    /**
     * Get include sub categories
     * @return bool
     */
    public function getIncludeSubCategories()
    {
        return (boolean)$this->includeSubCategories;
    }

    /**
     * @param bool $includeSubCategories
     * @return NewsDemand
     */
    public function setIncludeSubCategories($includeSubCategories)
    {
        $this->includeSubCategories = $includeSubCategories;
        return $this;
    }

    /**
     * Set author
     *
     * @param string $author
     * @return NewsDemand
     */
    public function setAuthor($author)
    {
        $this->author = $author;
    }

    /**
     * Get author
     *
     * @return string
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * Set time limit low, either integer or string
     *
     * @param mixed $timeRestriction
     * @return NewsDemand
     */
    public function setTimeRestriction($timeRestriction)
    {
        $this->timeRestriction = $timeRestriction;
        return $this;
    }

    /**
     * Get time limit low
     *
     * @return mixed
     */
    public function getTimeRestriction()
    {
        return $this->timeRestriction;
    }

    /**
     * Get time limit high
     *
     * @return mixed
     */
    public function getTimeRestrictionHigh()
    {
        return $this->timeRestrictionHigh;
    }

    /**
     * Set time limit high
     *
     * @param mixed $timeRestrictionHigh
     * @return NewsDemand
     */
    public function setTimeRestrictionHigh($timeRestrictionHigh)
    {
        $this->timeRestrictionHigh = $timeRestrictionHigh;
        return $this;
    }

    /**
     * Set order
     *
     * @param string $order order
     * @return NewsDemand
     */
    public function setOrder($order)
    {
        $this->order = $order;
        return $this;
    }

    /**
     * Get order
     *
     * @return string
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * Set order allowed
     *
     * @param string $orderByAllowed allowed fields for ordering
     * @return NewsDemand
     */
    public function setOrderByAllowed($orderByAllowed)
    {
        $this->orderByAllowed = $orderByAllowed;
        return $this;
    }

    /**
     * Get allowed order fields
     *
     * @return string
     */
    public function getOrderByAllowed()
    {
        return $this->orderByAllowed;
    }

    /**
     * Set order respect top news flag
     *
     * @param bool $topNewsFirst respect top news flag
     * @return NewsDemand
     */
    public function setTopNewsFirst($topNewsFirst)
    {
        $this->topNewsFirst = $topNewsFirst;
        return $this;
    }

    /**
     * Get order respect top news flag
     *
     * @return int
     */
    public function getTopNewsFirst()
    {
        return $this->topNewsFirst;
    }

    /**
     * Set search fields
     *
     * @param string $searchFields search fields
     */
    public function setSearchFields($searchFields)
    {
        $this->searchFields = $searchFields;
        return $this;
    }

    /**
     * Get search fields
     *
     * @return string
     */
    public function getSearchFields()
    {
        return $this->searchFields;
    }

    /**
     * Set top news setting
     *
     * @param string $topNewsRestriction top news settings
     * @return NewsDemand
     */
    public function setTopNewsRestriction($topNewsRestriction)
    {
        $this->topNewsRestriction = $topNewsRestriction;
        return $this;
    }

    /**
     * Get top news setting
     *
     * @return string
     */
    public function getTopNewsRestriction()
    {
        return $this->topNewsRestriction;
    }

    /**
     * Set list of storage pages
     *
     * @param string $storagePage storage page list
     * @return NewsDemand
     */
    public function setStoragePage($storagePage)
    {
        $this->storagePage = $storagePage;
        return $this;
    }

    /**
     * Get list of storage pages
     *
     * @return string
     */
    public function getStoragePage()
    {
        return $this->storagePage;
    }
	
	/**
	 * @return array
	 */
	public function getUids()
	{
		return $this->uids;
	}

	/**
	 * @param array $uids
	 */
	public function setUids($uids)
	{
		$this->uids = $uids;
	}

	/**
	 * Get date from
	 *
	 * @return integer
	 */
	public function getDateFrom() 
	{
		return $this->dateFrom;
	}

	/**
	 * Set date from
	 *
	 * @param integer $dateFrom
	 */
	public function setDateFrom($dateFrom) 
	{
		$this->dateFrom = $dateFrom;
	}
	
	/**
	 * Get date to
	 *
	 * @return integer
	 */
	public function getDateTo() 
	{
		return $this->dateTo;
	}

	/**
	 * Set date to
	 *
	 * @param integer $dateTo
	 * @return void
	 */
	public function setDateTo($dateTo) 
	{
		$this->dateTo = $dateTo;
	}

    /**
     * Get day restriction
     *
     * @return int
     */
    public function getDay()
    {
        return $this->day;
    }

    /**
     * Set day restriction
     *
     * @param int $day
     * @return NewsDemand
     */
    public function setDay($day)
    {
        $this->day = $day;
        return $this;
    }

    /**
     * Get month restriction
     *
     * @return int
     */
    public function getMonth()
    {
        return $this->month;
    }

    /**
     * Set month restriction
     *
     * @param int $month month
     * @return NewsDemand
     */
    public function setMonth($month)
    {
        $this->month = $month;
        return $this;
    }

    /**
     * Get year restriction
     *
     * @return int
     */
    public function getYear()
    {
        return $this->year;
    }

    /**
     * Set year restriction
     *
     * @param int $year year
     * @return NewsDemand
     */
    public function setYear($year)
    {
        $this->year = $year;
        return $this;
    }

    /**
     * Get calendar restriction
     *
     * @return string
     */
    public function getCalendar()
    {
        return $this->calendar;
    }

    /**
     * Set calendar restriction
     *
     * @param int $calendar calendar
     * @return NewsDemand
     */
    public function setCalendar($calendar)
    {
        $this->calendar = $calendar;
        return $this;
    }
	
	 /**
     * Get calendar from restriction
     *
     * @return string
     */
    public function getCalendarFrom()
    {
        return $this->calendarFrom;
    }

    /**
     * Set calendar from restriction
     *
     * @param int $calendarFrom calendarFrom
     * @return NewsDemand
     */
    public function setCalendarFrom($calendarFrom)
    {
        $this->calendarFrom = $calendarFrom;
        return $this;
    }
	
	/**
     * Get calendar to restriction
     *
     * @return string
     */
    public function getCalendarTo()
    {
        return $this->calendarTo;
    }

    /**
     * Set calendar to restriction
     *
     * @param int $calendarTo calendarTo
     * @return NewsDemand
     */
    public function setCalendarTo($calendarTo)
    {
        $this->calendarTo = $calendarTo;
        return $this;
    }

    /**
     * Set limit
     *
     * @param int $limit limit
     * @return NewsDemand
     */
    public function setLimit($limit)
    {
        $this->limit = (int)$limit;
        return $this;
    }

    /**
     * Get limit
     *
     * @return int
     */
    public function getLimit()
    {
        return $this->limit;
    }

    /**
     * Set offset
     *
     * @param int $offset offset
     * @return NewsDemand
     */
    public function setOffset($offset)
    {
        $this->offset = (int)$offset;
        return $this;
    }

    /**
     * Get offset
     *
     * @return int
     */
    public function getOffset()
    {
        return $this->offset;
    }
	
	/**
	 * Set per page
	 *
	 * @param integer $perPage per page
	 */
	public function setPerPage($perPage) 
	{
		$this->perPage = (int)$perPage;
	}

	/**
	 * Get per page
	 *
	 * @return integer
	 */
	public function getPerPage() 
	{
		return $this->perPage;
	}

    /**
     * Set date field which is used for datemenu
     *
     * @param string $dateField datefield
     * @return NewsDemand
     */
    public function setDateField($dateField)
    {
        $this->dateField = $dateField;
        return $this;
    }

    /**
     * Get datefield which is used for datemenu
     *
     * @return string
     */
    public function getDateField()
    {
		$allowedFields = ['datetime', 'archive', 'crdate'];
    	if(is_array($GLOBALS['TCA']['tx_mooxnews_domain_model_news']['moox']['additionalFrontendDateFields']))
		{
			$allowedFields = array_merge($allowedFields,$GLOBALS['TCA']['tx_mooxnews_domain_model_news']['moox']['additionalFrontendDateFields']);
		}
		if (empty($this->dateField) || in_array(GeneralUtility::camelCaseToLowerCaseUnderscored($this->dateField), $allowedFields)) {
            return $this->dateField;
        } else {
            return '';
        }
    }

    /**
     * Get search object
     *
     * @return \Neulandlotsen\MooxNews\Domain\Model\Dto\Search
     */
    public function getSearch()
    {
        return $this->search;
    }

    /**
     * Set search object
     *
     * @param \Neulandlotsen\MooxNews\Domain\Model\Dto\Search $search search object
     * @return NewsDemand
     */
    public function setSearch($search = null)
    {
        $this->search = $search;
        return $this;
    }

    /**
     * Set flag if displayed news records should be excluded
     *
     * @param bool $excludeAlreadyDisplayedNews
     * @return NewsDemand
     */
    public function setExcludeAlreadyDisplayedNews($excludeAlreadyDisplayedNews)
    {
        $this->excludeAlreadyDisplayedNews = (bool)$excludeAlreadyDisplayedNews;
        return $this;
    }

    /**
     * Get flag if displayed news records should be excluded
     *
     * @return bool
     */
    public function getExcludeAlreadyDisplayedNews()
    {
        return $this->excludeAlreadyDisplayedNews;
    }
	
	/**
	 * @param string $tag
	 * @return void
	 */
	public function setTag($tag) 
	{
		$this->tag = $tag;
	}

	/**
	 * @return string
	 */
	public function getTag() 
	{
		return $this->tag;
	}
	
	/**
	 * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage
	 */
	public function getTags() 
	{
		return $this->tags;
	}
	

	/**
	 * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage $tags tags
	 * @return void
	 */
	public function setTags($tags) 
	{
		$this->tags = $tags;
	}
	
	
	/**
	 * @param array $targets targets
	 * @return void
	 */
	public function setTargets($targets) 
	{
		$this->targets = $targets;
	}

	/**
	 * @return array
	 */
	public function getTargets() 
	{
		return $this->targets;
	}
	
	/**
	 * @param string $target
	 * @return void
	 */
	public function setTarget($target) 
	{
		$this->target = $target;
	}

	/**
	 * @return string
	 */
	public function getTarget() 
	{
		return $this->target;
	}
	
	/**
	 * @param integer $sysLanguageUid sys language uid
	 * @return void
	 */
	public function setSysLanguageUid($sysLanguageUid) 
	{
		$this->sysLanguageUid = $sysLanguageUid;
	}

	/**
	 * @return integer
	 */
	public function getSysLanguageUid() 
	{
		return $this->sysLanguageUid;
	}
	
	/**
	 * Set flag if only selected news should be shown in rss feeds
	 *
	 * @param boolean $includeOnlySelectedNewsInRss
	 * @return void
	 */
	public function setIncludeOnlySelectedNewsInRss($includeOnlySelectedNewsInRss) 
	{
		$this->includeOnlySelectedNewsInRss = (bool)$includeOnlySelectedNewsInRss;
	}
	
	/**
	 * Get flag if only selected news should be shown in rss feeds
	 *
	 * @return boolean
	 */
	public function getIncludeOnlySelectedNewsInRss() 
	{
		return $this->includeOnlySelectedNewsInRss;
	}
		
	/**
	 * @param boolean $excludeFromRss	 
	 */
	public function setExcludeFromRss($excludeFromRss) 
	{
		$this->excludeFromRss = $excludeFromRss;
	}

	/**
	 * @return boolean
	 */
	public function getExcludeFromRss() 
	{
		return $this->excludeFromRss;
	}
	
	/**
	 * Set types
	 *
	 * @param array $types	 
	 */
	public function setTypes($types) 
	{
		$this->types = $types;
	}

	/**
	 * @return array
	 */
	public function getTypes() 
	{
		return $this->types;
	}
	
	/**
	 * @param string $type
	 * @return void
	 */
	public function setType($type) 
	{
		$this->type = $type;
	}

	/**
	 * @return string
	 */
	public function getType() 
	{
		return $this->type;
	}
	
	/**
	 * @param string $char	 
	 */
	public function setChar($char) 
	{
		$this->char = $char;
	}

	/**
	 * @return string
	 */
	public function getChar() 
	{
		return $this->char;
	}
	
	/**
	 * @param array $charList
	 * @return void
	 */
	public function setCharList($charList) 
	{
		$this->charList = $charList;
	}

	/**
	 * @return array
	 */
	public function getCharList() 
	{
		return $this->charList;
	}
	
	/**
	 * @param integer $privacy
	 * @return void
	 */
	public function setPrivacy($privacy) 
	{
		$this->privacy = $privacy;
	}
	
	/**
	 * @return integer
	 */
	public function getPrivacy() {
		return $this->privacy;
	}

	/**
	 * @return string
	 */
	public function getQuery() 
	{
		return $this->query;
	}

	/**
	 * @param string $query	 
	 */
	public function setQuery($query) 
	{
		$this->query = $query;
	}

    /**
     * @return string
     */
    public function getHideIdList()
    {
        return $this->hideIdList;
    }

    /**
     * @param string $hideIdList
     * @return NewsDemand
     */
    public function setHideIdList($hideIdList)
    {
        $this->hideIdList = $hideIdList;
        return $this;
    }

    /**
     * @return string
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * @param string $action
     * @return NewsDemand
     */
    public function setAction($action)
    {
        $this->action = $action;
        return $this;
    }

    /**
     * @return string
     */
    public function getClass()
    {
        return $this->class;
    }

    /**
     * @param string $class
     * @return NewsDemand
     */
    public function setClass($class)
    {
        $this->class = $class;
        return $this;
    }

    /**
     * @param string $action
     * @param string $controller
     * @return NewsDemand
     */
    public function setActionAndClass($action, $controller)
    {
        $this->action = $action;
        $this->class = $controller;
        return $this;
    }
}