<?php
namespace Neulandlotsen\MooxMailer\Controller;

/**
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */
 
class HtmlViewController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController 
{
	
	/**
	 * newsletterRepository
	 *
	 * @var \Neulandlotsen\MooxMailer\Domain\Repository\NewsletterRepository
	 */
	protected $newsletterRepository;
	
	/**
	 * initialize the controller
	 *
	 * @return void
	 */
	protected function initializeAction() 
	{
		parent::initializeAction();		
		$this->newsletterRepository = $this->objectManager->get('Neulandlotsen\\MooxMailer\\Domain\\Repository\\NewsletterRepository');			
	}
	
	/**
	 * action show newsletter
	 *
	 * @param int $uid
	 * @param string $hash
	 * @return void
	 */
	public function showNewsletterAction($uid = 0, $hash = "") 
	{		
		if($uid>0 && $hash!=""){
			$object = $this->newsletterRepository->findByUid($uid,TRUE);
			if(is_object($object)){
				if($object->getSent()>0 && $hash == md5($object->getCrdate()->format("YmdHis")."mOOx".$object->getUid())){
					$html = $object->getHtml();
				}
			}
		}
		
		if($html!=''){
			$this->view->setTemplatePathAndFilename('typo3conf/ext/moox_mailer/Resources/Private/Templates/HtmlView/ShowNewsletter.html');
			$this->view->assign('html', $html);
		} else {
			exit("Newsletter not found or not sent yet...");
		}
		
	}	
	
	/**
	 * action error
	 *	
	 * @return void
	 */
	public function errorAction() 
	{		
		$this->view->setTemplatePathAndFilename('typo3conf/ext/moox_mailer/Resources/Private/Templates/HtmlView/Error.html');
		$this->view->assign('error', "Newsletter not found or not sent yet...");
	}
}
?>