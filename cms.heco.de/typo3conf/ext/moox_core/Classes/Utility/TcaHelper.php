<?php

namespace Neulandlotsen\MooxCore\Utility;

/**
 * This file is part of the "moox_core" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

/**
 * Helper class for tca manipulation
 */
class TcaHelper
{
    /**
	 * @param string $table
     * @param string $arrayName
	 * @param array $fields
     */
    public static function addFieldsToArray($table,$arrayName,$fields = [])
    {
        if(is_array($GLOBALS['TCA'][$table]['moox'][$arrayName])){
			foreach($fields AS $key => $field){
				if(!in_array($field,$GLOBALS['TCA'][$table]['moox'][$arrayName])){
					if(is_string($key)){
						$GLOBALS['TCA'][$table]['moox'][$arrayName][$key] = $field;	
					} else {
						$GLOBALS['TCA'][$table]['moox'][$arrayName][] = $field;
					}
				}
			}
		}
    }
	
	/**
	 * @param string $table
     * @param string $listName
	 * @param array $fields
     */
    public static function addFieldsToList($table,$listName,$fields = [])
    {
        if($listName=="requestUpdate")
		{
			if($GLOBALS['TCA'][$table]['ctrl']['requestUpdate']!=''){
				$listedFields = explode(",",$GLOBALS['TCA'][$table]['ctrl']['requestUpdate']);
			}
			else
			{
				$listedFields = [];
			}
			foreach($fields AS $field){
				if(!in_array($field,$listedFields)){
					if($GLOBALS['TCA'][$table]['ctrl']['requestUpdate']!=""){
						$GLOBALS['TCA'][$table]['ctrl']['requestUpdate'] .= ",";
					}
					$GLOBALS['TCA'][$table]['ctrl']['requestUpdate'] .= $field;			
				}
			}			
		}
		else
		{
			if($GLOBALS['TCA'][$table]['moox'][$listName]!=''){
				$listedFields = explode(",",$GLOBALS['TCA'][$table]['moox'][$listName]);
			}
			else
			{
				$listedFields = [];
			}
			foreach($fields AS $field){
				if(!in_array($field,$listedFields)){
					if($GLOBALS['TCA'][$table]['moox'][$listName]!=""){
						$GLOBALS['TCA'][$table]['moox'][$listName] .= ",";
					}
					$GLOBALS['TCA'][$table]['moox'][$listName] .= $field;			
				}
			}
		}
    }
	
	/**
	 * @param string $table
     * @param string $extkey
	 * @param string $fieldList
     */
    /*
	public static function addListViewFieldSet($table,$extkey,$fieldList="")
    {
        if(is_array($GLOBALS['TCA'][$table]['moox']['listViewFields']) && $fieldList!="")
		{
			if(!isset($GLOBALS['TCA'][$table]['moox']['listViewFields'][$extkey]))
			{
				$GLOBALS['TCA'][$table]['moox']['listViewFields'][$extkey] = $fieldList;
			} 
			else 
			{
				$GLOBALS['TCA'][$table]['moox']['listViewFields'][$extkey] .= $fieldList;
			}
		}
    }
	*/
	
	/**
	 * @param string $table
     * @param string $extkey
	 * @param array $fields
	 * @param string $inherit
	 * @param array $inheritExcludes
     */
    public static function addBackendPreviewFieldsSet($table,$extkey,$fields = [],$inherit = "", $inheritExclude = [])
    {
        if($inherit!="" && is_array($GLOBALS['TCA'][$table]['moox']['backendPreviewFields'][$inherit]))
		{
			$inheritFields = $GLOBALS['TCA'][$table]['moox']['backendPreviewFields'][$inherit];
			if(is_array($inheritExclude))
			{
				foreach($inheritExclude AS $excludeField)
				{
					if($index=array_search($excludeField, $inheritFields))
					{ 
						unset($inheritFields[$index]);
					}
				}
			}
			$fields = array_merge($inheritFields,$fields);			
		}
		
		if(!is_array($GLOBALS['TCA'][$table]['moox']['backendPreviewFields'][$extkey]))
		{
			$GLOBALS['TCA'][$table]['moox']['backendPreviewFields'][$extkey] = [];
		}
		foreach($fields AS $field)
		{
			if(!in_array($field,$GLOBALS['TCA'][$table]['moox']['backendPreviewFields'][$extkey]))
			{
				if(!in_array($field,$GLOBALS['TCA'][$table]['moox']['backendPreviewFields'][$extkey])){
					$GLOBALS['TCA'][$table]['moox']['backendPreviewFields'][$extkey][] = $field;
				}
			}
		}
		$GLOBALS['TCA'][$table]['moox']['backendPreviewFields'][$extkey] = array_unique($GLOBALS['TCA'][$table]['moox']['backendPreviewFields'][$extkey]);
    }

    /**
	 * @param string $table
	 * @param string $extkey
	 * @param array $fields
	 * @param string $inherit
	 * @param array $inheritExcludes
	 */
	public static function addSearchFieldsSet($table,$extkey,$fields = [],$inherit = "", $inheritExclude = [])
	{
		if($inherit!="" && is_array($GLOBALS['TCA'][$table]['moox']['searchFields'][$inherit]))
		{
			$inheritFields = $GLOBALS['TCA'][$table]['moox']['searchFields'][$inherit];
			if(is_array($inheritExclude))
			{
				foreach($inheritExclude AS $excludeField)
				{
					if($index=array_search($excludeField, $inheritFields))
					{
						unset($inheritFields[$index]);
					}
				}
			}
			$fields = array_merge($inheritFields,$fields);
		}

		if(!is_array($GLOBALS['TCA'][$table]['moox']['searchFields'][$extkey]))
		{
			$GLOBALS['TCA'][$table]['moox']['searchFields'][$extkey] = [];
		}
		foreach($fields AS $field)
		{
			if(!in_array($field,$GLOBALS['TCA'][$table]['moox']['searchFields'][$extkey]))
			{
				if(!in_array($field,$GLOBALS['TCA'][$table]['moox']['searchFields'][$extkey])){
					$GLOBALS['TCA'][$table]['moox']['searchFields'][$extkey][] = $field;
				}
			}
		}
		$GLOBALS['TCA'][$table]['moox']['searchFields'][$extkey] = array_unique($GLOBALS['TCA'][$table]['moox']['searchFields'][$extkey]);
	}
	
	/**
	 * @param string $table
     * @param string $extkey
	 * @param array $fields
     */
    public static function addSearchFieldSet($table,$extkey,$fields = [])
    {
        if(!is_array($GLOBALS['TCA'][$table]['moox']['searchFields'][$extkey]))
		{
			$GLOBALS['TCA'][$table]['moox']['searchFields'][$extkey] = [];
		}
		foreach($fields AS $field)
		{
			if(!in_array($field,$GLOBALS['TCA'][$table]['moox']['searchFields'][$extkey]))
			{
				$GLOBALS['TCA'][$table]['moox']['searchFields'][$extkey][] = $field;					
			}
		}
    }
	
	/**
	 * @param string $table
     * @param string $extkey
	 * @param string $field
     */
    public static function addDateTimeDefaultField($table,$extkey,$field = "")
    {
        if(is_array($GLOBALS['TCA'][$table]['moox']['dateTimeDefaultField']))
		{
			if(!isset($GLOBALS['TCA'][$table]['moox']['dateTimeDefaultField'][$extkey]))
			{
				$GLOBALS['TCA'][$table]['moox']['dateTimeDefaultField'][$extkey] = $field;
			} 
			else 
			{
				$GLOBALS['TCA'][$table]['moox']['dateTimeDefaultField'][$extkey] = $field;
			}
		}
    }
	
	/**
	 * hide tca fields off parent type or other extenders
	 *
	 * @param string $hideFields
	 * @param string $table
	 * @param string $type
	 * @param string $operator
	 */
	public static function hideTcaFields($hideFields = "", $table = "", $type = "", $operator = "!=")
	{		
		// hide classified default fields defined in hide fields string 
		if($hideFields!="")
		{
			if($operator=="=")
			{
				$logicalOperator = "OR";
			}
			else
			{
				$logicalOperator = "AND";
			}
			$hideFields = array_unique(explode(",",$hideFields));
			foreach($hideFields AS $hideField)
			{
				if(isset($GLOBALS['TCA'][$table]['columns'][$hideField]))
				{
					if(!isset($GLOBALS['TCA'][$table]['columns'][$hideField]['displayCond']))
					{
						$GLOBALS['TCA'][$table]['columns'][$hideField]['displayCond'] = "";
					}
					if(is_string($GLOBALS['TCA'][$table]['columns'][$hideField]['displayCond']) && $GLOBALS['TCA'][$table]['columns'][$hideField]['displayCond']=="")
					{
						$GLOBALS['TCA'][$table]['columns'][$hideField]['displayCond'] = 'FIELD:type:'.$operator.':'.$type;						
					} 
					elseif(is_string($GLOBALS['TCA'][$table]['columns'][$hideField]['displayCond']) && $GLOBALS['TCA'][$table]['columns'][$hideField]['displayCond']!="")
					{
						$displayCond = $GLOBALS['TCA'][$table]['columns'][$hideField]['displayCond'];
						$GLOBALS['TCA'][$table]['columns'][$hideField]['displayCond'] = [];
						$GLOBALS['TCA'][$table]['columns'][$hideField]['displayCond'][$logicalOperator] = [];
						$GLOBALS['TCA'][$table]['columns'][$hideField]['displayCond'][$logicalOperator][] = $displayCond;
						$GLOBALS['TCA'][$table]['columns'][$hideField]['displayCond'][$logicalOperator][] = 'FIELD:type:'.$operator.':'.$type;
						$GLOBALS['TCA'][$table]['columns'][$hideField]['displayCond'][$logicalOperator] = array_unique($GLOBALS['TCA'][$table]['columns'][$hideField]['displayCond'][$logicalOperator]);
					} 
					elseif(is_array($GLOBALS['TCA'][$table]['columns'][$hideField]['displayCond']) && is_array($GLOBALS['TCA'][$table]['columns'][$hideField]['displayCond'][$logicalOperator]))
					{					
						$GLOBALS['TCA'][$table]['columns'][$hideField]['displayCond'][$logicalOperator][] = 'FIELD:type:'.$operator.':'.$type;
						$GLOBALS['TCA'][$table]['columns'][$hideField]['displayCond'][$logicalOperator] = array_unique($GLOBALS['TCA'][$table]['columns'][$hideField]['displayCond'][$logicalOperator]);
					}						
				}				
			}
		}
	}
}
