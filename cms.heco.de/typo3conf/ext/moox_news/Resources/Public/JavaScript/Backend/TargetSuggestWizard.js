define(['jquery', 'TYPO3/CMS/Backend/FormEngine'], function($, FormEngine) {
    'use strict';

    var TargetSuggestWizard = {};

    TargetSuggestWizard.initialize = function() {
        $('.news-targetable').closest('.formengine-field-item').find('.t3-form-suggest').filter(function() {
            return !$(this).data('news-targetable-initialized');
        }).each(function() {
            $(this).data('news-targetable-initialized', true);
            var autocomplete = $(this).autocomplete();
            var onSelectCallback = autocomplete.options.onSelect;
            var transformResultCallback = autocomplete.options.transformResult;
            autocomplete.setOptions({
                transformResult: function(response) {
                    response = transformResultCallback(response);

                    var $form = FormEngine.getFormElement();
                    var targetData = $form.data('tx_mooxnews_domain_model_target') || {};

                    response.suggestions = $.map(response.suggestions, function(dataItem) {
                        if (typeof dataItem.data.uid == 'string' && dataItem.data.uid.indexOf('NEW') === 0) {
                            var key = dataItem.data.label.toLowerCase();
                            if (key in targetData) {
                                dataItem.data.uid = targetData[key];
                            } else {
                                targetData[key] = dataItem.data.uid;
                            }
                        }
                        return dataItem;
                    });

                    $form.data('tx_mooxnews_domain_model_target', targetData);

                    return response;
                },
                onSelect: function(suggestion) {
                    if ($.isFunction(onSelectCallback)) {
                        onSelectCallback.call(this, suggestion);
                    }
                    var $this = $(this);
                    if (suggestion.data.table === 'tx_mooxnews_domain_model_target'
                        && typeof suggestion.data.uid == 'string'
                        && suggestion.data.uid.indexOf('NEW') === 0
                    ) {
                        var $parent = $this.parent();
                        var pid = (typeof suggestion.data.pid !== 'undefined') ? suggestion.data.pid : $this.data('pid');
                        $('<input>').attr({
                            type: 'hidden',
                            name: 'data[tx_mooxnews_domain_model_target][' + suggestion.data.uid + '][pid]',
                            value: pid
                        }).appendTo($parent);
                        $('<input>').attr({
                            type: 'hidden',
                            name: 'data[tx_mooxnews_domain_model_target][' + suggestion.data.uid + '][title]',
                            value: suggestion.data.label
                        }).appendTo($parent);
                    }
                    $this.focus();
                    $this.val('');
                    $this.autocomplete().hide();
                }
            })
        });
    };

    TargetSuggestWizard.reinitialize = FormEngine.reinitialize;

    FormEngine.reinitialize = function() {
        TargetSuggestWizard.reinitialize();

        if ($('.t3-form-suggest').length) {
            require(['TYPO3/CMS/Backend/FormEngineSuggest'], function() {
                $(function() {
                    TargetSuggestWizard.initialize();
                })
            });
        }
    };

    return TargetSuggestWizard;
});
