define([
	'jquery',
	'TYPO3/CMS/MooxCore/Backend/Module'], function ($, MooxModule) {

	var MooxNewsMod4 = {};

	MooxNewsMod4.init = function () {

		$(document).on('submit', '#moox-news-module-filter form.use-ajax', function (event) {

			event.preventDefault();

			var url = $(this).attr("action");
			var wrapper = MooxNewsMod4.getWrapper($(this));
			var form = "#" + $(this).attr("id");

			var data = {};

			$(form + ' input.is-grouped-filter').each(function () {
				data[$(this).attr('name')] = $(this).val();
			});

			$(form + ' select.is-grouped-filter').each(function () {
				data[$(this).attr('name')] = $(this).val();
			});

			$.ajax(url, {
				method: "POST",
				cache: false,
				data: data,
				success: function (data) {
					/*mooxNewsMod4Refresh(wrapper);*/
					body = $("#" + wrapper + ".use-ajax #moox-news-module-body").first();
					window.location = body.data('url');
				},
				error: function (data) {

				},
				beforeSend: function () {
					$("body").append("<div id='moox-news-module-overlay'><img src='../typo3conf/ext/moox_news/Resources/Public/Images/Backend/loading.gif' /></div>");
				},
				complete: function () {
					/*$("#moox-news-module-overlay").remove();*/
				},
			});

			return false;
		});

		$(document).on('click', '#moox-news-module-filter a.page-selector', function (event) {
			event.preventDefault();
			$('#moox-news-module-filter input.page-selector').val($(this).data('uid'));
			$('#moox-news-module-filter button.page-selector .current').html($(this).html());
			if ($(this).hasClass('auto-submit')) {
				$('#moox-news-module-filter form').submit();
			}
		});

		$(document).on('click', '#moox-news-module-body a.show-detail', function (event) {
			event.preventDefault();

			visible = false;
			if ($('#moox-news-module-body #category-detail-' + $(this).data('uid')).is(":visible")) {
				visible = true;
			}

			$('#moox-news-module-body .category-detail').hide();
			if (!visible && $('#moox-news-module-body #category-detail-' + $(this).data('uid'))) {
				$('#moox-news-module-body #category-detail-' + $(this).data('uid')).show();
			}
			$('#moox-news-module-body a.show-detail .icon-up').hide();
			$('#moox-news-module-body a.show-detail .icon-down').show();
			if (visible) {
				$(this).find('.icon-down').show();
				$(this).find('.icon-up').hide();
			} else {
				$(this).find('.icon-down').hide();
				$(this).find('.icon-up').show();

			}
		});

		$(document).on('change', '#moox-news-module-filter select.auto-submit', function (event) {
			event.preventDefault();
			$('#moox-news-module-filter form').submit();
		});

		$(document).on('change', '#moox-news-module-filter input.auto-submit', function (event) {
			event.preventDefault();
			$('#moox-news-module-filter form').submit();
		});

		$(document).on('click', '#moox-news-module-filter a.filter-reset', function (event) {
			event.preventDefault();

			var url = $(this).attr("href");
			var wrapper = MooxNewsMod4.getWrapper($(this));

			var data = {};
			data['tx_mooxnews_mooxnewsmooxdb_mooxnewscategorymanagement[ajax]'] = 1;

			$.ajax(url, {
				method: "POST",
				cache: false,
				data: data,
				success: function (data) {
					body = $("#" + wrapper + ".use-ajax #moox-news-module-body").first();
					window.location = body.data('url');
				},
				error: function (data) {

				},
				beforeSend: function () {
					$("body").append("<div id='moox-news-module-overlay'><img src='../typo3conf/ext/moox_news/Resources/Public/Images/Backend/loading.gif' /></div>");
				},
				complete: function () {
					/*$("#moox-news-module-overlay").remove();*/
				},
			});

			return false;
		});

		$(document).on('click', '.moox-news-module.use-ajax #moox-news-module-body a.change-ordering', function (event) {
			event.preventDefault();

			var url = $(this).attr("href");
			var wrapper = MooxNewsMod4.getWrapper($(this));

			var data = {};
			data['tx_mooxnews_mooxnewsmooxdb_mooxnewscategorymanagement[ajax]'] = 1;

			$.ajax(url, {
				method: "POST",
				cache: false,
				data: data,
				success: function (data) {
					body = $("#" + wrapper + ".use-ajax #moox-news-module-body").first();
					window.location = body.data('url');
				},
				error: function (data) {

				},
				beforeSend: function () {
					$("body").append("<div id='moox-news-module-overlay'><img src='../typo3conf/ext/moox_news/Resources/Public/Images/Backend/loading.gif' /></div>");
				},
				complete: function () {
					/*$("#moox-news-module-overlay").remove();*/
				},
			});

			return false;
		});
	}

	MooxNewsMod4.refresh = function(wrapper)
	{
		var body = $("#"+wrapper+".use-ajax #moox-news-module-body").first();

		var data = { };

		data['tx_mooxnews_mooxnewsmooxdb_mooxnewscategorymanagement[ajax]'] = 1;

		$.ajax(body.data("url"), {
			method: "POST",
			data: data,
			success: function(data) {
				body.html($(data));
			},
			beforeSend: function(){
				$("body").append("<div id='moox-news-module-overlay'><img src='../typo3conf/ext/moox_news/Resources/Public/Images/Backend/loading.gif' /></div>");
			},
			complete: function(){
				$("#moox-news-module-overlay").remove();
			},
		});
	}

	MooxNewsMod4.getWrapper = function(object) {
		return object.parents('.moox-news-module-mod4').attr("id");
	}


	MooxNewsMod4.init();

	return MooxNewsMod4;
});