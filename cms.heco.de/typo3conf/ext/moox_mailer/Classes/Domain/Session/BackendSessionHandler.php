<?php
namespace Neulandlotsen\MooxMailer\Domain\Session;

/**
 * This file is part of the "moox_mailer" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

class BackendSessionHandler extends \Neulandlotsen\MooxCore\Domain\Session\SessionHandler
{	
	/**
     * @var string
     */
    protected $mode = "BE";
}
?>