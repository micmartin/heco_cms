<?php
namespace Neulandlotsen\MooxNews\Controller;

/**
 * This file is part of the "moox_news" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

use Neulandlotsen\MooxCore\Utility\PdfGenerator;
use Neulandlotsen\MooxNews\Service\Transliterator\Transliterator;
use Neulandlotsen\MooxNews\Domain\Repository\Backend\NewsRepository;
use Neulandlotsen\MooxNews\Domain\Repository\CategoryRepository;
use Neulandlotsen\MooxNews\Domain\Session\BackendSessionHandler;
use Neulandlotsen\MooxNews\Service\HelperService;
use Neulandlotsen\MooxNews\Service\IcsService;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Messaging\FlashMessage;
use TYPO3\CMS\Backend\Utility\BackendUtility;
use TYPO3\CMS\Extbase\Reflection\ObjectAccess;
use TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface;
use TYPO3\CMS\Extbase\Persistence\PersistenceManagerInterface;
use TYPO3\CMS\Frontend\Page\PageRepository;
use TYPO3\CMS\Core\Site\SiteFinder;
 
class Mod1Controller extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController 
{
	/**
	 * @var PersistenceManagerInterface
	 */
	protected $persistenceManager;		
	
	/**
	 * @var NewsRepository
	 */
	protected $newsRepository;
	
	/**
     * @var CategoryRepository
	 */
    protected $categoryRepository;

	/**
	 * @var PageRepository
	 */
	protected $pageRepository;		
	
	/**
     * @var BackendSessionHandler
     */
	protected $backendSession;
	
	/**
	 * @var HelperService
	 */
	protected $helperService;

	/**
	 * @var PdfGenerator
	 */
	protected $pdfGenerator;
	
	/**
	 * @var IcsService
	 */
	protected $icsService;

	/**
	 * @var SiteFinder\
	 */
	protected $siteFinder;
	
	/**
     * TsConfig configuration
     *
     * @var array
     */
    protected $tsConfiguration = [];
	
	
	/**
	 * @var int
	 */
	protected $storagePage;
	
	/**
	 * @var array
	 */
	protected $storagePageInfo = [];
	
	/**
	 * @var bool
	 */
	protected $extConf;				
	
	/**
	 * @var array
	 */
	protected $groupedFolders = [];

	/**
	 * @var array
	 */
	protected $folders = [];
	
	/**
	 * @var array
	 */
	protected $types;

	/**
	 * @var array
	 */
	protected $years;

	/**
	 * @var array
	 */
	protected $languages;
	
	/**
	 * @var array
	 */
	protected $filter;
	
	/**
	 * @var array
	 */
	protected $ordering;

    /**
     * @param PersistenceManagerInterface $persistenceManager
     */
    public function injectPersistenceManagerInterface(PersistenceManagerInterface $persistenceManager)
    {
        $this->persistenceManager = $persistenceManager;
    }

    /**
     * @param NewsRepository $newsRepository
     */
    public function injectNewsRepository(NewsRepository $newsRepository)
    {
        $this->newsRepository = $newsRepository;
    }

    /**
     * @param CategoryRepository $categoryRepository
     */
    public function injectCategoryRepository(CategoryRepository $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
    }

    /**
     * @param PageRepository $pageRepository
     */
    public function injectPageRepository(PageRepository $pageRepository)
    {
        $this->pageRepository = $pageRepository;
    }

    /**
     * @param BackendSessionHandler $backendSession
     */
    public function injectBackendSessionHandler(BackendSessionHandler $backendSession)
    {
        $this->backendSession = $backendSession;
    }

    /**
     * @param HelperService $helperService
     */
    public function injectHelperService(HelperService $helperService)
    {
        $this->helperService = $helperService;
    }

    /**
     * @param PdfGenerator $pdfGenerator
     */
    public function injectPdfGenerator(PdfGenerator $pdfGenerator)
    {
        $this->pdfGenerator = $pdfGenerator;
    }

    /**
     * @param IcsService $icsService
     */
    public function injectIcsService(IcsService $icsService)
    {
        $this->icsService = $icsService;
    }

	/**
	 * @param SiteFinder $siteFinder
	 */
	public function injectSiteFinder(SiteFinder $siteFinder)
	{
		$this->siteFinder = $siteFinder;
	}
	
	/**
	 * @var array
	 */
	protected $multipleFunctions = [
		"show" => "Einträge aktivieren",
		"hide" => "Einträge deaktivieren",
		"remove" => "Einträge löschen",
		/*"duplicate" => "Einträge duplizieren",*/
		"copy" => "Einträge kopieren",
		"move" => "Einträge verschieben",
		"istopnews" => "Einträge als Top-Eintrag markieren",
		"notopnews" => "Einträge von Top-Einträgen entfernen"		
	];
	
	/**
	 * initialize the controller
	 */
	protected function initializeAction()
	{
	    // parent initialization
		parent::initializeAction();

		// set ext conf
		$this->extConf = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Configuration\ExtensionConfiguration::class)->get('moox_news');

		// get news folders
		$this->groupedFolders = $this->helperService->getFolders();
		foreach ($this->groupedFolders AS $groupedFolder)
		{
			foreach ($groupedFolder AS $folder)
			{
				$this->folders[] = $folder;
			}
		}

		// set filter
		$this->filter = $this->helperService->buildFilter(self::class,$this->settings);
		
		// set ordering
		$this->ordering = $this->helperService->buildOrdering(self::class,$this->settings);
		
		// set storage page
		$this->setStoragePage(
			$this->helperService->redirectToFolder($this->groupedFolders,$this->filter,$this->settings,self::class)
		);

		if($this->extConf['translationHandling'] && $this->getStoragePage()>0)
		{
			$currentSite = $this->siteFinder->getSiteByPageId($this->getStoragePage());
			if(count($currentSite->getLanguages())>1)
			{
				$this->languages = $currentSite->getLanguages();
			}
		}

		// merge typoscript settings
		$this->settings = $this->helperService->mergeSettingsOfCurrentPid($this->settings,$this->getStoragePage());
		
		// get storage page informatione
		$this->storagePageInfo = BackendUtility::readPageAccess($this->getStoragePage(), '');
		
		// set types
		$this->types = $this->helperService->getTypes($this->storagePageInfo['news_types']);	
		
		// force type-filter
		if(count($this->types)==1)
		{
			$this->filter['type'] = key($this->types);
			$this->helperService->updateFilter($this->filter,self::class);
		}
		elseif(count($this->types)<1)
		{
			// add message
			$messages[] = [
				"icon" => '<span class="glyphicon glyphicon-warning-sign icon-alert" aria-hidden="true"></span>',
				"text" => 'Im gewählten Ordner ist das Erstellen von Einträgen nicht erlaubt. Wählen Sie einen anderen Ordner aus.',
				"type" => FlashMessage::WARNING,
			];
			
			// set flash messages
			$this->helperService->setFlashMessages($this,$messages,'',FALSE);
		}

		// set years
		$this->years = $this->helperService->getYears($this->settings,$this->filter,true);

		if(isset($this->filter['year']) && !in_array($this->filter['year'], $this->years))
		{
			unset($this->filter['year']);
			$this->helperService->updateFilter($this->filter,self::class);
		}
		
		// set multiple functions
		$this->multipleFunctions = array_merge($this->multipleFunctions,$this->helperService->getAdditionalMultiple("tx_mooxnews_domain_model_news",$this->settings));
		
		// initialize storage settings
		$this->initializeStorageSettings();
	}
	
	/**
	 * initialize storage settings
	 */
	protected function initializeStorageSettings()
	{				
		//fallback to current pid if no storagePid is defined
		$configuration = $this->configurationManager->getConfiguration(ConfigurationManagerInterface::CONFIGURATION_TYPE_FRAMEWORK);
		$currentPid['persistence']['storagePid'] = $this->getStoragePage();
		$this->configurationManager->setConfiguration(array_merge($configuration, $currentPid));		
	}
	
	/**
	 * action list
	 *
	 * @param bool $refresh refresh
	 * @param bool $ajax ajax	
	 * @param string $returnUrl return url
	 * @param array $excludeFields
	 */
	public function listAction($refresh = FALSE, $ajax = FALSE, $returnUrl = NULL, $excludeFields = [])
	{
		// init action arrays and booleans
		$messages = [];
		$errors = [];	

		// get categories
		$categories = $this->categoryRepository->findParentCategoriesByPid($this->getStoragePage());
        $idList = [];
        foreach ($categories as $c)
		{
			if($c->getNewsTypes()=="" || $this->filter['type']=="" || ($c->getNewsTypes()!="" && $this->filter['type']!="" && in_array($this->filter['type'],explode(",",$c->getNewsTypes()))))
			{
				$idList[] = $c->getUid();
			} elseif(is_array($this->filter['categories']) && in_array($c->getUid(),$this->filter['categories'])){
				if(($key = array_search($c->getUid(), $this->filter)) !== false) {
					unset($this->filter[$key]);
				}
			}
        }
        if (empty($idList) && !$this->getBackendUser()->isAdmin())
		{
            $idList = $this->getBackendUser()->getCategoryMountPoints();
        }

		//$this->filter = [];

		// get news
		$this->newsRepository->setSettings($this->settings);
		$news = $this->newsRepository->findByFilter(
			$this->filter,
			[$this->ordering['field']=>$this->ordering['direction']],
			NULL,
			NULL,
			[$this->getStoragePage()],
			['hidden','starttime','endtime']
		);	
		
		
		if(false && $this->getStoragePage()==755)
		{
			$removeCategory = $this->categoryRepository->findByUid(88);
			foreach($news AS $newsToUpdate)
			{
				if($newsToUpdate->getType()=="moox_news_seminar")
				{
					if($newsToUpdate->getUid()==10058 || true)
					{
						if($newsToUpdate->getCategories()->contains($removeCategory))
						{
							$newsToUpdate->getCategories()->detach($removeCategory);
							$this->newsRepository->update($newsToUpdate);															
						}						
					}
				}
			}
			$this->persistenceManager->persistAll();
		}
		if(false)
		{
			foreach($news AS $newsToUpdate)
			{
				$firstCategory = $newsToUpdate->getFirstCategory();
				if(is_object($firstCategory))
				{
					$newsToUpdate->setFeGroup($firstCategory->getFeGroup());
				}
				$newsToUpdate->setHash('');
				$this->newsRepository->update($newsToUpdate);
			}
			$this->persistenceManager->persistAll();
		}
		if(false)
		{
			$updCnt = 0;
			echo "Update Date<br>\n";
			foreach($news AS $newsToUpdate)
			{
				echo $updCnt." - ".$newsToUpdate->getTitle()."<br>\n";
				if($newsToUpdate->getType()=="moox_news_seminar") {
					if ($newsToUpdate->getMooxNewsSeminarStartdate() > 0)
					{
						$newsToUpdate->setDatetime($newsToUpdate->getMooxNewsSeminarStartdate());
						$newsToUpdate->setYear(date("Y",$newsToUpdate->getMooxNewsSeminarStartdate()));
					}
					else
					{
						$newsToUpdate->setDatetime("");
						$newsToUpdate->setYear("");
					}
				}
				else
				{
					if ($newsToUpdate->getDatetime() > 0)
					{
						$newsToUpdate->setYear(date("Y",$newsToUpdate->getDatetime()->format('U')));
					}
				}
				$this->newsRepository->update($newsToUpdate);
				$this->persistenceManager->persistAll();
				$updCnt++;
			}

			exit();
		}
		if(false)
		{
			$updCnt = 0;
			echo "Update Path Segment<br>\n";
			foreach($news AS $newsToUpdate)
			{
				$newsToUpdate->setPathSegment('');
				$this->newsRepository->update($newsToUpdate);
				$this->persistenceManager->persistAll();
			}
			foreach($news AS $newsToUpdate)
			{
				$pathSegment = Transliterator::urlize($newsToUpdate->getTitle());

				$queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)
					->getQueryBuilderForTable('tx_mooxnews_domain_model_news');

				$duplicates = $queryBuilder
					->select('uid')
					->from('tx_mooxnews_domain_model_news')
					->where(
						$queryBuilder->expr()->eq('path_segment', $queryBuilder->createNamedParameter($pathSegment))
					)
					->execute();

				if($duplicates->fetch())
				{
					echo "Duplicate found: ".$pathSegment."<br>\n";
					$pathSegmentBase = $pathSegment;
					for($i=1;$i<1000;$i++)
					{
						$pathSegment = $pathSegmentBase."-".$i;
						$duplicates = $queryBuilder
							->select('uid')
							->from('tx_mooxnews_domain_model_news')
							->where(
								$queryBuilder->expr()->eq('path_segment', $queryBuilder->createNamedParameter($pathSegment))
							)
							->execute();
						if(!$duplicates->fetch())
						{
							break;
						}
						else
						{
							echo "Duplicate found: ".$pathSegment."<br>\n";
						}
					}
				}

				echo $updCnt." - ".$newsToUpdate->getTitle()."<br>\n";

				$newsToUpdate->setPathSegment($pathSegment);
				$this->newsRepository->update($newsToUpdate);
				$this->persistenceManager->persistAll();
				$updCnt++;
			}


			exit($updCnt." updated");
		}
		if(false)
		{
			$updCnt = 0;
			echo "Update Index<br>\n";
			foreach($news AS $newsToUpdate)
			{
				if($newsToUpdate->getType()=="moox_news_seminar")
				{
					echo $updCnt." - ".$newsToUpdate->getTitle()."<br>\n";
					$startdate = $newsToUpdate->getMooxNewsSeminarStartdate();
					if($startdate>0){
						$index = [];
						$index[] = date("d.m.Y",$startdate);
						$index[] = date("j.n.Y",$startdate);
						$index[] = date("d.m.y",$startdate);
						$index[] = date("j.n.y",$startdate);
						$index[] = date("Y-m-d",$startdate);
						$index[] = date("y-m-d",$startdate);
						$index[] = date("Y-n-j",$startdate);
						$index[] = date("y-n-j",$startdate);
						$newsToUpdate->setMooxNewsSeminarSearchIndex(implode(" ",$index));
						$this->newsRepository->update($newsToUpdate);								
						$this->persistenceManager->persistAll();
						$updCnt++;
					}
				}
			}
			
			exit();
		}

		// set template variables	
		$this->view->assign('table', 'tx_mooxnews_domain_model_news');	
		$this->view->assign('news', $news);	
		$this->view->assign('categories', $this->categoryRepository->findTree($idList));
		$this->view->assign('fields', $this->helperService->getListViewFields("tx_mooxnews_domain_model_news",$this->settings,$this->filter,$excludeFields));
		$this->view->assign('fieldsSeparator', $this->helperService->getFieldsSeparator($this->settings));	
		$this->view->assign('previewFields', $this->helperService->getPreviewFields("tx_mooxnews_domain_model_news",$this->settings,$this->filter));
		$this->view->assign('buttons', $this->helperService->getAdditionalButtons("tx_mooxnews_domain_model_news",$this->settings));
		$this->view->assign('addItems', $this->helperService->getAdditionalAddButtonItems("tx_mooxnews_domain_model_news",$this->settings));
		$this->view->assign('moduleItems', $this->helperService->getAdditionalModuleSelectItems($this->settings));
		$this->view->assign('controls', $this->helperService->getAdditionalControls("tx_mooxnews_domain_model_news",$this->settings));
		$this->view->assign('filters', $this->helperService->getAdditionalFilters("tx_mooxnews_domain_model_news",$this->settings));
		$this->view->assign('previewPartials', $this->helperService->getAdditionalBackendPreviewPartials("tx_mooxnews_domain_model_news",$this->settings));
		$this->view->assign('filter', $this->filter);	
		$this->view->assign('ordering', $this->ordering);
		$this->view->assign('groupedFolders', $this->groupedFolders);
		$this->view->assign('folders', $this->folders);
		$this->view->assign('types', $this->types);
		$this->view->assign('languages', $this->languages);
		$this->view->assign('years', $this->years);
		$this->view->assign('multipleFunctions', array_merge($this->multipleFunctions,$this->helperService->getAdditionalMultiple("tx_mooxnews_domain_model_news",$this->settings)));
		$this->view->assign('storagePage', $this->storagePage);
		$this->view->assign('replacements', $this->helperService->getQueryReplacements($this->filter['query']));
		$this->view->assign('conf', $this->extConf);
		$this->view->assign('returnUrl', $this->uriBuilder->getRequest()->getRequestUri());
		$this->view->assign('refresh', $refresh);
		$this->view->assign('settings',$this->settings);		
		$this->view->assign('action', 'list');
		$this->view->assign('module', 'mod1');
		$this->view->assign('plugin', $this->request->getPluginName());
		$this->view->assign('icon', 'ext-news-wizard-icon');
	}		
	
	/**
	 * action filter
	 *
	 * @param array $filter filter
	 * @param bool $ajax ajax
	 * @param bool $reset reset
	 * @param string $returnUrl return url
	 * @return void
	 */
	public function filterAction($filter = NULL, $ajax = FALSE, $reset = FALSE, $returnUrl = NULL)
	{
		if($reset)
		{			
			$filter['page'] = $this->filter['page'];
			$this->helperService->resetFilter(self::class);
			$this->filter = $this->helperService->buildFilter(self::class,$this->settings);
			$this->filter['page'] = $filter['page'];
			$this->helperService->updateFilter($this->filter,self::class);
		} 
		else 
		{			
			if(is_array($filter))
			{
				if($filter['page'] != $this->filter['page']){
					unset($this->filter['type']);
					unset($filter['type']);
				}
				if(!isset($filter['categories']) || (isset($filter['categories']) && (!is_array($filter['categories']) || count($filter['categories'])<1))){
					unset($this->filter['categories']);
				}
				if(isset($filter['page']) && $this->filter['page']!=$filter['page'])
				{
					unset($this->filter['categories']);
					unset($filter['categories']);					
					if($filter['page']>0 && $this->filter['type']!='')
					{
						$pageInfo = BackendUtility::readPageAccess($filter['page'], '');
						if($pageInfo['news_types']!="")
						{
							if(!in_array($this->filter['type'],explode(",",$pageInfo['news_types'])))
							{
								unset($this->filter['type']);
								unset($filter['type']);								
							}
						}
					}
				}
				foreach($filter AS $key => $value)
				{
					if($value!="" && $value!="all")
					{						
						$this->filter[$key] = $value;
					} 
					elseif(isset($this->filter[$key]))
					{
						unset($this->filter[$key]);
					}
				}
				$this->helperService->updateFilter($this->filter,self::class);
			} 			
		}
		
		if(!$ajax)
		{			
			if($returnUrl)
			{
				$this->redirectToUri($returnUrl);
			}			
		} 
		else 
		{
			header ("HTTP/1.0 200 Ok");
			exit();
		}
	}
	
	/**
	 * action ordering
	 *
	 * @param array $ordering ordering
	 * @param bool $ajax ajax
	 * @param bool $reset reset
	 * @param string $returnUrl return url
	 * @return void
	 */
	public function orderingAction($ordering = NULL, $ajax = FALSE, $reset = FALSE, $returnUrl = NULL)
	{
		if($reset)
		{			
			$this->helperService->resetOrdering(self::class);	
			$this->ordering = $this->helperService->buildOrdering(self::class,$this->settings);
		} 
		else 
		{			
			if(is_array($ordering))
			{
				$this->ordering = $ordering;				
				$this->helperService->updateOrdering($this->ordering,self::class);
			} 			
		}
		
		if(!$this->ajax)
		{
			if($returnUrl)
			{
				$this->redirectToUri($returnUrl);
			}
		} 
		else 
		{
			header ("HTTP/1.0 200 Ok");
			exit();
		}
	}
	
	/**
	 * action toggle visibility
	 *
	 * @param int $uid	
	 * @param string $returnUrl return url
	 * @return void
	 */
	public function toggleVisibiltyAction($uid = 0, $returnUrl = NULL)
	{					
		if($uid>0)
		{									
			$item = $this->newsRepository->findByUid($uid,FALSE);
			
			if($item->getHidden()==1)
			{
				$item->setHidden(0);				
			} 
			else 
			{
				$item->setHidden(1);				
			}			
			
			$this->newsRepository->update($item);								
			$this->persistenceManager->persistAll();
								
		} 
		
		if($returnUrl)
		{
			$this->redirectToUri($returnUrl);
		} 
		else 
		{
			$this->redirect("list");
		}		
	}		
	
	/**
	 * action multiple
	 *
	 * @param array $selection	
	 * @param string $function
	 * @param bool $confirmed
	 * @param array $settings
	 * @param string $returnUrl
	 */
	public function multipleAction($selection = [], $function = NULL, $confirmed = false, $settings = [], $returnUrl = NULL)
	{							
		$cnt = 0;
		$messages = [];
		$method_exists = false;
		$listMode = false;
		$list = [];
		
		if(strpos($function, '::')!==false)
		{
			$method = explode("::",$function);
			if($method[2]=='list')
			{
				$listMode = true;
			}
			if(count($method)==2 || count($method)==3)
			{
				if(class_exists($method[0]))
				{
					$methodClass = GeneralUtility::makeInstance($method[0]);
					$method = $method[1];
					if(method_exists($methodClass,$method))
					{
						$method_exists = true;							
					} 
				}
			}			
		} 
		else
		{
			$method = "multiple_".$function;
			if(method_exists($this,$method))
			{
				$method_exists = true;				
			}
		}		
		
		if(is_array($selection) && count($selection) && $function!="" && isset($this->multipleFunctions[$function])&& $method_exists)
		{									
			if($confirmed)
			{
				foreach($selection AS $uid)
				{					
					$item = $this->newsRepository->findByUid($uid,FALSE);						
					
					if(is_object($item))
					{
						if(is_object($methodClass))
						{
							if($listMode)
							{
								$list[] = $item;
							} 
							else
							{
								if($methodClass->$method($item,$this->newsRepository,array_merge($this->settings,$settings)))
								{
									$this->persistenceManager->persistAll();									
								}
							}
						} 
						else
						{
							if($listMode)
							{
								$list[] = $item;
							} 
							else
							{
								$this->$method($item,array_merge($this->settings,$settings));								
							}								
						}
						$cnt++;
					}					
				}
				if($listMode && count($list))
				{
					if(is_object($methodClass))
					{
						if($methodClass->$method($list,$this->newsRepository,array_merge($this->settings,$settings)))
						{
							$this->persistenceManager->persistAll();
						}
					} 
					else
					{
						$this->$method($list,array_merge($this->settings,$settings));
					}
				}
				
				// add message
				$messages[] = [
					"icon" => '<span class="glyphicon glyphicon-warning-sign icon-alert" aria-hidden="true"></span>',
					"title" => "Auswahl",
					"text" => 'Die Funktion "'.$this->multipleFunctions[$function].'" wurde auf '.$cnt.' Einträge angewendet',
					"type" => FlashMessage::OK,
				];
				
				// set flash messages
				$this->helperService->setFlashMessages($this,$messages);
				
				if($returnUrl)
				{
					$this->redirectToUri($returnUrl);
				} 
				else 
				{
					$this->redirect("list");
				}
			}
			else 
			{
				$items = [];
				foreach($selection AS $uid)
				{					
					$item = $this->newsRepository->findByUid($uid,FALSE);						
					if(is_object($item))
					{
						$items[] = $item;
						$cnt++;
					}
				}
				$this->view->assign('items', $items);
				$this->view->assign('function', $function);
				$this->view->assign('groupedFolders', $this->groupedFolders);
				$this->view->assign('message', $this->multipleFunctions[$function]);
				$this->view->assign('returnUrl', $returnUrl);
				$this->view->assign('action', 'multiple');
				$this->view->assign('icon', 'ext-news-wizard-icon');
			}
		} 
		elseif($returnUrl)
		{
			$this->redirectToUri($returnUrl);
		} 
		else 
		{
			$this->redirect("list");
		}		
	}		
	
	/**
	 * multiple show
	 *
	 * @param \Neulandlotsen\MooxNews\Domain\Model\News $item
	 * @param array $settings
	 */
	public function multiple_show(\Neulandlotsen\MooxNews\Domain\Model\News $item = NULL, $settings = [])
	{					
		if(is_object($item))
		{									
			$item->setHidden(0);							
			$this->newsRepository->update($item);								
			$this->persistenceManager->persistAll();						
		} 		
	}
	
	/**
	 * multiple hide
	 *
	 * @param \Neulandlotsen\MooxNews\Domain\Model\News $item
	 * @param array $settings
	 */
	public function multiple_hide(\Neulandlotsen\MooxNews\Domain\Model\News $item = NULL, $settings = [])
	{					
		if(is_object($item))
		{									
			$item->setHidden(1);							
			$this->newsRepository->update($item);								
			$this->persistenceManager->persistAll();						
		} 		
	}
	
	/**
	 * multiple remove
	 *
	 * @param \Neulandlotsen\MooxNews\Domain\Model\News $item
	 * @param array $settings
	 */
	public function multiple_remove(\Neulandlotsen\MooxNews\Domain\Model\News $item = NULL, $settings = [])
	{					
		if(is_object($item))
		{									
			$this->newsRepository->remove($item);								
			$this->persistenceManager->persistAll();						
		} 		
	}
	
	/**
	 * multiple duplicate
	 *
	 * @param \Neulandlotsen\MooxNews\Domain\Model\News $item
	* @param array $settings	 
	 */
	public function multiple_duplicate(\Neulandlotsen\MooxNews\Domain\Model\News $item = NULL, $settings = [])
	{					
		if(is_object($item))
		{									
			$this->duplicateAction($item->getUid(),NULL,TRUE,$settings);									
		} 		
	}
	
	/**
	 * multiple copy
	 *
	 * @param \Neulandlotsen\MooxNews\Domain\Model\News $item
	 * @param array $settings	 
	 */
	public function multiple_copy(\Neulandlotsen\MooxNews\Domain\Model\News $item = NULL, $settings = [])
	{					
		if(is_object($item))
		{									
			$this->copyAction($item->getUid(), ($settings['pid']>0)?$settings['pid']:$item->getPid(), NULL, TRUE, $settings);
		} 		
	}
	
	/**
	 * multiple move
	 *
	 * @param \Neulandlotsen\MooxNews\Domain\Model\News $item
	 * @param array $settings	 
	 */
	public function multiple_move(\Neulandlotsen\MooxNews\Domain\Model\News $item = NULL, $settings = [])
	{					
		if(is_object($item))
		{									
			if($settings['pid']>0)
			{
				$this->moveAction($item->getUid(), $settings['pid'], NULL, TRUE, $settings);
			}
		} 		
	}
	
	/**
	 * multiple istopnews
	 *
	 * @param \Neulandlotsen\MooxNews\Domain\Model\News $item
	 * @param array $settings
	 */
	public function multiple_istopnews(\Neulandlotsen\MooxNews\Domain\Model\News $item = NULL, $settings = [])
	{					
		if(is_object($item))
		{									
			$item->setIstopnews(1);							
			$this->newsRepository->update($item);								
			$this->persistenceManager->persistAll();						
		} 		
	}
	
	/**
	 * multiple notopnews
	 *
	 * @param \Neulandlotsen\MooxNews\Domain\Model\News $item	
	 * @param array $settings
	 */
	public function multiple_notopnews(\Neulandlotsen\MooxNews\Domain\Model\News $item = NULL, $settings = [])
	{					
		if(is_object($item))
		{									
			$item->setIstopnews(0);							
			$this->newsRepository->update($item);								
			$this->persistenceManager->persistAll();						
		} 		
	}
	
	/**
     * action add news by tca
     *
	 * @param string $type
     */
    public function addNewsByTcaAction($type = "")
    {
        $this->helperService->redirectToCreateNewRecord('tx_mooxnews_domain_model_news',$this->storagePage,$type);
    }
	
	/**
	 * action duplicate
	 *
	 * @param int $uid	
	 * @param string $returnUrl
	 * @param boolean $directCall 
	 * @param array $settings 
	 * @return void
	 */
	public function duplicateAction($uid = 0, $returnUrl = NULL, $directCall = FALSE, $settings = [])
	{					
		exit("out of order");		
		
		$messages = [];	

		$noCopyFields = [
			"l10nParent",
			"uid",
			"tstamp",
			"crdate",
			"cruserId",
			"isTopnews"
		];
		
		if($uid>0){

			if($settings['prefix']==""){
				$settings['prefix'] = "Kopie";
			}
		
			$defaultStorageFields = [
				"categories",
				"contentElements",
				"relatedFiles",
				"relatedLinks",
				"media",
				"falMedia",
				"falRelatedFiles"
			];
											
			$testedStorageFields = [
				"categories",
				"falMedia"
			];
			
			$storageClasses = ["TYPO3\CMS\Extbase\Persistence\Generic\LazyObjectStorage"];
			
			$original = $this->newsRepository->findByUid($uid,FALSE);
			
			$clone = $this->objectManager->get('Neulandlotsen\MooxNews\Domain\Model\Backend\News');

			// $product = source object
			$properties = ObjectAccess::getSettablePropertyNames($original);
			foreach ($properties as $name)
			{
				$value = ObjectAccess::getProperty($original, $name);
				if(!in_array($name,$defaultStorageFields) && (!is_object($value) || (is_object($value) && !in_array(get_class($value),$storageClasses))))
				{					
					if(!in_array($name,$noCopyFields)){
						ObjectAccess::setProperty($clone, $name, $value);
					}
				} 				
			}
			
			foreach ($properties as $name)
			{
				$value = ObjectAccess::getProperty($original, $name);
				if((in_array($name,$defaultStorageFields) && in_array($name,$testedStorageFields)) || (!in_array($name,$defaultStorageFields) && is_object($value) && in_array(get_class($value),$storageClasses)))
				{										
					if(!in_array($name,$noCopyFields)){
						$getCall = "get".ucfirst($name);
						$setCall = "get".ucfirst($name);
						if(method_exists($original, $getCall))
						{
							$objects = $original->$getCall();
							if(method_exists($clone->$getCall(),"attach"))
							{
								if(count($objects))
								{
									foreach ($objects as $object)
									{
										if(ObjectAccess::isPropertySettable($object, "uid"))
										{
											//$object->setUid(NULL);
										}
										$clone->$getCall()->attach($object);								
									}
								}
							} 
							elseif(method_exists($clone,$setCall))
							{
								$clone->$setCall($objects);
							}
						}
					}
				} 				
			}
			
			$clone->setTitle($original->getTitle()." [".$settings['prefix']."]");
			$clone->setHidden(1);
			
			$this->newsRepository->add($clone);
												
			$this->persistenceManager->persistAll();
			
			if(!$directCall)
			{
				// add message
				$messages[] = [				
					"text" => 'Eintrag wurde dupliziert',
					"type" => FlashMessage::OK,
				];
					
				// set flash messages
				$this->helperService->setFlashMessages($this,$messages);
			}
		}
		
		if(!$directCall)
		{
			if($returnUrl)
			{
				$this->redirectToUri($returnUrl);
			} 
			else 
			{
				$this->redirect("list");
			}
		}
	}
	
	/**
	 * action move
	 *
	 * @param int $uid
	 * @param int $pid	
	 * @param string $returnUrl
	 * @param boolean $directCall 
	 * @param array $settings 
	 * @param array $data	 
	 */
	public function moveAction($uid = 0,$pid = 0, $returnUrl = NULL, $directCall = FALSE, $settings = [])
	{					
		$messages = [];
		
		if(is_numeric($pid) && $uid>0)
		{									
			
			$folder = $this->pageRepository->getPage($pid);
			
			$item = $this->newsRepository->findByUid($uid,FALSE);
			
			if($folder['news_types']=="" || in_array($item->getType(),explode(",",$folder['news_types'])))
			{
				$item->setPid($pid);	
			
				$this->newsRepository->update($item);														
				$this->persistenceManager->persistAll();
				
				if(!$directCall)
				{
					// add message
					$messages[] = [				
						"text" => 'Eintrag wurde verschoben',
						"type" => FlashMessage::OK,
					];
						
					// set flash messages
					$this->helperService->setFlashMessages($this,$messages);
				}
			} 
			else 
			{
				
				if(!$directCall)
				{
					// add message
					$messages[] = [
						"icon" => '<span class="glyphicon glyphicon-warning-sign icon-alert" aria-hidden="true"></span>',
						"title" => "Verschieben",
						"text" => 'Der Eintrag kann nicht in den Ordner "'.$folder['title'].'" vorschoben werden (nur folgende Eintragstypen erlaubt: '.$folder['news_types'].')',
						"type" => FlashMessage::ERROR,
					];
					
					// set flash messages
					$this->helperService->setFlashMessages($this,$messages);
				}
			}
									
		} 
		
		if(!$directCall)
		{
			if($pid>0 && $this->filter['page']!=$pid)
			{
				$this->filter['page'] = $pid;
				unset($this->filter['categories']);
				if($this->filter['type']!='')
				{
					$pageInfo = BackendUtility::readPageAccess($pid, '');
					if($pageInfo['news_types']!="")
					{
						if(!in_array($this->filter['type'],explode(",",$pageInfo['news_types'])))
						{
							unset($this->filter['type']);
						}
					}
				}

				$this->helperService->updateFilter($this->filter,self::class);
				$this->redirect("list");
			}
			else
			{
				if($returnUrl)
				{
					$this->redirectToUri($returnUrl);
				}
				else
				{
					$this->redirect("list");
				}
			}

		}			
	}
	
	/**
	 * action copy
	 *
	 * @param int $uid
	 * @param int $pid
	 * @param string $returnUrl
	 * @param boolean $directCall 
	 * @param array $settings 
	 * @param array $data
	 * @return void
	 */
	public function copyAction($uid = 0, $pid = 0, $returnUrl = NULL, $directCall = FALSE, $settings = [], $data = [])
	{										
		$messages = [];	
		
		if($uid>0){
			
			$item = $this->newsRepository->findByUid($uid,FALSE);
			
			if(is_object($item))
			{			
				$settings = array_merge($this->settings,$settings);
				
				$resetFields = $this->helperService->getResetFields("tx_mooxnews_domain_model_news",$item->getType(),$settings);
				
				if($settings['prefix']!="")
				{
					$data['title'] = "[".$settings['prefix']."] ".$item->getTitle();
				}

				$pathSegment = Transliterator::urlize($item->getTitle());

				$queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)
					->getQueryBuilderForTable('tx_mooxnews_domain_model_news');

				$duplicates = $queryBuilder
					->select('uid')
					->from('tx_mooxnews_domain_model_news')
					->where(
						$queryBuilder->expr()->eq('path_segment', $queryBuilder->createNamedParameter($pathSegment))
					)
					->execute();

				if($duplicates->fetch())
				{
					$pathSegmentBase = $pathSegment;
					for($i=1;$i<1000;$i++)
					{
						$pathSegment = $pathSegmentBase."-".$i;
						$duplicates = $queryBuilder
							->select('uid')
							->from('tx_mooxnews_domain_model_news')
							->where(
								$queryBuilder->expr()->eq('path_segment', $queryBuilder->createNamedParameter($pathSegment))
							)
							->execute();
						if(!$duplicates->fetch())
						{
							break;
						}
					}
				}
				$data['path_segment'] = $pathSegment;
				
				$data = array_merge($resetFields,$data);
				
				$copy = $this->helperService->copyNewsRecord("tx_mooxnews_domain_model_news",$item->getUid(),($pid>0)?$pid:$item->getPid(),$data);
				
				if(!$directCall)
				{
					// add message
					$messages[] = [				
						"text" => 'Eintrag wurde kopiert',
						"type" => FlashMessage::OK,
					];
						
					// set flash messages
					$this->helperService->setFlashMessages($this,$messages);
				}
			}
		}
		
		if(!$directCall)
		{
			if($pid>0 && $this->filter['page']!=$pid)
			{
				$this->filter['page'] = $pid;
				unset($this->filter['categories']);
				if($this->filter['type']!='')
				{
					$pageInfo = BackendUtility::readPageAccess($pid, '');
					if($pageInfo['news_types']!="")
					{
						if(!in_array($this->filter['type'],explode(",",$pageInfo['news_types'])))
						{
							unset($this->filter['type']);
						}
					}
				}

				$this->helperService->updateFilter($this->filter,self::class);
				$this->redirect("list");
			}
			else
			{
				if($returnUrl)
				{
					$this->redirectToUri($returnUrl);
				}
				else
				{
					$this->redirect("list");
				}
			}

		}
	}
	
	/**
	 * action import
	 */
	public function importAction()
	{					
		exit();
		
		$grabberService = $this->objectManager->get('Neulandlotsen\MooxNews\Service\GrabberService');
		
		$mode = "update";
		
		if($mode=="insert"){
		
			$importfile = "Seminare_2018.csv";
			
			$speakerLookup = [
				104 => 112,
				105 => 113,
				106 => 114,
				107 => 115,
				108 => 116,
				109 => 117,
				110 => 118,
				111 => 119,
				112 => 120,
				113 => 121,
				114 => 122,
				115 => 123,
				116 => 124,
				117 => 125,
				118 => 126,
				119 => 127,
				120 => 128,
				121 => 129,
				122 => 130,
				123 => 131,
				124 => 132,
				125 => 133,
				126 => 134,
				127 => 135
			];
			
			//header("Content-Type: text/html; charset=utf-8");
			$row = 0;
			$items = [];
			if (($handle = fopen($_SERVER["DOCUMENT_ROOT"]."/".$importfile, "r")) !== FALSE) {			
				while (($data = fgetcsv($handle, 10000, ";")) !== FALSE) {
					if($row==0){
						//print_r($data);
					}
					if($row>0 && $data[0]!=''){
						$news = [];
						$news['pid'] = 755;
						$news['hidden'] = 1;
						$news['remoteuid'] = $data[0];
						$news['importsource'] = "Seminar_Import_2017_08_28";					
						$news['starttime'] = strtotime(trim($data[3]));					
						$news['endtime'] = strtotime(trim($data[4]));
						$news['datetime'] = trim($data[19]);
						$news['title'] = str_replace("\n","",trim($data[5]));
						$news['title'] = str_replace("\r","",$news['title']);
						$news['title'] = trim($news['title']);
						$news['moox_news_seminar_subtitle'] = trim($data[6]);
						$news['moox_news_seminar_info'] = trim($data[7]);					
						$news['teaser'] = trim($data[8]);					
						$news['bodytext'] = trim($data[9]);
						$news['categories'] = explode(",",trim($data[12]));
						$news['targets'] = explode(",",trim($data[14]));
						$news['moox_news_seminar_target'] = trim($data[13]);
						$news['moox_news_seminar_startdate'] = strtotime(trim($data[19]));
						$news['datetime'] = $news['moox_news_seminar_startdate']?$news['moox_news_seminar_startdate']:time();
						$news['moox_news_seminar_enddate'] = strtotime(trim($data[20]));
						if($news['moox_news_seminar_startdate'] == $news['moox_news_seminar_enddate']){
							$news['moox_news_seminar_enddate'] = "";
						}
						$news['moox_news_seminar_price'] = trim(str_replace(",",".",$data[21]));
						$news['moox_news_seminar_price_2'] = trim(str_replace(",",".",$data[22]));
						$news['moox_news_seminar_price_3'] = trim(str_replace(",",".",$data[23]));
						$news['moox_news_seminar_price_4'] = trim(str_replace(",",".",$data[24]));
						$news['moox_news_seminar_price_info'] = trim($data[26]);
						$news['moox_news_seminar_subscriber_min'] = trim($data[27]);
						$news['moox_news_seminar_subscriber_max'] = trim($data[28]);
						$news['moox_news_seminar_register_deadline'] = strtotime(trim($data[29]));
						$news['moox_news_seminar_time_1'] = trim($data[30]);
						$news['moox_news_seminar_time_2'] = trim($data[31]);
						$news['moox_news_seminar_time_3'] = trim($data[32]);
						$news['moox_news_seminar_organizer'] = trim($data[43]);					
						$news['moox_news_seminar_listpoint_1'] = trim($data[33]);
						$news['moox_news_seminar_listpoint_2'] = trim($data[34]);
						$news['moox_news_seminar_listpoint_3'] = trim($data[35]);
						$news['moox_news_seminar_listpoint_4'] = trim($data[36]);
						$news['moox_news_seminar_listpoint_5'] = trim($data[37]);
						$news['moox_news_seminar_listpoint_6'] = trim($data[38]);					
						$news['moox_news_seminar_speaker_1'] = trim($data[39]);
						$news['moox_news_seminar_speaker_2'] = trim($data[40]);
						$news['moox_news_seminar_speaker_3'] = trim($data[41]);
						$speakers = [];
						if($news['moox_news_seminar_speaker_1'])
						{
							$speakers[] = (isset($speakerLookup[$news['moox_news_seminar_speaker_1']]))?$speakerLookup[$news['moox_news_seminar_speaker_1']]:$news['moox_news_seminar_speaker_1'];
						}
						if($news['moox_news_seminar_speaker_2'])
						{
							$speakers[] = (isset($speakerLookup[$news['moox_news_seminar_speaker_2']]))?$speakerLookup[$news['moox_news_seminar_speaker_2']]:$news['moox_news_seminar_speaker_2'];
						}
						if($news['moox_news_seminar_speaker_3'])
						{
							$speakers[] = (isset($speakerLookup[$news['moox_news_seminar_speaker_3']]))?$speakerLookup[$news['moox_news_seminar_speaker_3']]:$news['moox_news_seminar_speaker_3'];
						}
						if(count($speakers)){
							$news['moox_news_seminar_speakers'] = implode(",",$speakers);
						}
						$news['moox_news_geoinfo_venue'] = trim($data[44]);
						$news['moox_news_geoinfo_url'] = trim($data[49]);
						$news['moox_news_geoinfo_street'] = trim($data[45]);
						$news['moox_news_geoinfo_zip'] = trim($data[46]);
						$news['moox_news_geoinfo_city'] = trim($data[47]);
						$news['moox_news_seminar_biwecon'] = trim($data[16]);
						$news['push_to_twitter_text'] = trim($data[18]);					
						
						//$grabberService->writeNews($news);
						
						$items[] = $news;
						
						//break;
					}
					$row++;
				}
			}			
		} elseif($mode=="update"){
		
			$importfile = "Seminare_2018_V2.csv";
			
			$speakerLookup = [
				104 => 112,
				105 => 113,
				106 => 114,
				107 => 115,
				108 => 116,
				109 => 117,
				110 => 118,
				111 => 119,
				112 => 120,
				113 => 121,
				114 => 122,
				115 => 123,
				116 => 124,
				117 => 125,
				118 => 126,
				119 => 127,
				120 => 128,
				121 => 129,
				122 => 130,
				123 => 131,
				124 => 132,
				125 => 133,
				126 => 134,
				127 => 135
			];
			
			//header("Content-Type: text/html; charset=utf-8");
			$row = 0;
			$items = [];
			if (($handle = fopen($_SERVER["DOCUMENT_ROOT"]."/".$importfile, "r")) !== FALSE) {			
				while (($data = fgetcsv($handle, 10000, ";")) !== FALSE) {
					if($row==0){
						//print_r($data);
					}
					if($row>0 && $data[2]!=''){
						$news = [];
						$news['pid'] = 755;
						$news['uid'] = $data[2];
						$news['remoteuid'] = $data[0];
						$news['importsource'] = "Seminar_Import_2017_08_28";					
						$news['starttime'] = strtotime(trim($data[3]));					
						$news['endtime'] = strtotime(trim($data[4]));
						$news['datetime'] = trim($data[19]);
						$news['title'] = str_replace("\n","",trim($data[5]));
						$news['title'] = str_replace("\r","",$news['title']);
						$news['title'] = trim($news['title']);
						$news['moox_news_seminar_subtitle'] = trim($data[6]);
						$news['moox_news_seminar_info'] = trim($data[7]);					
						$news['teaser'] = trim($data[8]);					
						$news['bodytext'] = trim($data[9]);
						$news['categories'] = explode(",",trim($data[12]));
						$news['targets'] = explode(",",trim($data[14]));
						$news['moox_news_seminar_target'] = trim($data[13]);
						$news['moox_news_seminar_startdate'] = strtotime(trim($data[19]));
						$news['datetime'] = $news['moox_news_seminar_startdate']?$news['moox_news_seminar_startdate']:time();
						$news['moox_news_seminar_enddate'] = strtotime(trim($data[20]));
						if($news['moox_news_seminar_startdate'] == $news['moox_news_seminar_enddate']){
							$news['moox_news_seminar_enddate'] = "";
						}
						$news['moox_news_seminar_price'] = trim(str_replace(",",".",$data[21]));
						$news['moox_news_seminar_price_2'] = trim(str_replace(",",".",$data[22]));
						$news['moox_news_seminar_price_3'] = trim(str_replace(",",".",$data[23]));
						$news['moox_news_seminar_price_4'] = trim(str_replace(",",".",$data[24]));
						$news['moox_news_seminar_price_info'] = trim($data[26]);
						$news['moox_news_seminar_subscriber_min'] = trim($data[27]);
						$news['moox_news_seminar_subscriber_max'] = trim($data[28]);
						$news['moox_news_seminar_register_deadline'] = strtotime(trim($data[29]));
						$news['moox_news_seminar_time_1'] = trim($data[30]);
						$news['moox_news_seminar_time_2'] = trim($data[31]);
						$news['moox_news_seminar_time_3'] = trim($data[32]);
						$news['moox_news_seminar_organizer'] = trim($data[43]);					
						$news['moox_news_seminar_listpoint_1'] = trim($data[33]);
						$news['moox_news_seminar_listpoint_2'] = trim($data[34]);
						$news['moox_news_seminar_listpoint_3'] = trim($data[35]);
						$news['moox_news_seminar_listpoint_4'] = trim($data[36]);
						$news['moox_news_seminar_listpoint_5'] = trim($data[37]);
						$news['moox_news_seminar_listpoint_6'] = trim($data[38]);					
						$news['moox_news_seminar_speaker_1'] = trim($data[39]);
						$news['moox_news_seminar_speaker_2'] = trim($data[40]);
						$news['moox_news_seminar_speaker_3'] = trim($data[41]);
						$speakers = [];
						if($news['moox_news_seminar_speaker_1'])
						{
							$speakers[] = (isset($speakerLookup[$news['moox_news_seminar_speaker_1']]))?$speakerLookup[$news['moox_news_seminar_speaker_1']]:$news['moox_news_seminar_speaker_1'];
						}
						if($news['moox_news_seminar_speaker_2'])
						{
							$speakers[] = (isset($speakerLookup[$news['moox_news_seminar_speaker_2']]))?$speakerLookup[$news['moox_news_seminar_speaker_2']]:$news['moox_news_seminar_speaker_2'];
						}
						if($news['moox_news_seminar_speaker_3'])
						{
							$speakers[] = (isset($speakerLookup[$news['moox_news_seminar_speaker_3']]))?$speakerLookup[$news['moox_news_seminar_speaker_3']]:$news['moox_news_seminar_speaker_3'];
						}
						if(count($speakers)){
							$news['moox_news_seminar_speakers'] = implode(",",$speakers);
						}
						$news['moox_news_geoinfo_venue'] = trim($data[44]);
						$news['moox_news_geoinfo_url'] = trim($data[49]);
						$news['moox_news_geoinfo_street'] = trim($data[45]);
						$news['moox_news_geoinfo_zip'] = trim($data[46]);
						$news['moox_news_geoinfo_city'] = trim($data[47]);
						$news['moox_news_seminar_biwecon'] = trim($data[16]);
						$news['push_to_twitter_text'] = trim($data[18]);					
						
						//$grabberService->updateNews($news);
						
						$items[] = $news;
						
						//break;
					}
					$row++;
				}
			}
			
			print_r($items);
		}
		
		exit("finished ".$row."-".count($items));
	}
	
	/**
	 * action populate
	 */
	public function populateAction()
	{
		/*
		$objectManager = GeneralUtility::makeInstance('TYPO3\CMS\Extbase\Object\ObjectManager');
		$cacheService = $objectManager->get('TYPO3\\CMS\\Extbase\\Service\\CacheService');
		$cacheService->clearPageCache(133);
		*/
		
		$this->view->assign('action', 'populate');
		$this->view->assign('module', 'mod1');
		$this->view->assign('icon', 'actions-system-help-open');
	}
	
	/**
	 * action ics
	 *
 	 * @param int $newsUid
	 */
	public function icsAction(
		$newsUid = 0
	){
		$this->settings['forceIcsDownload'] = true;
		$this->icsService->writeIcsFile($this->newsRepository->findByUid($newsUid, false), $this->settings);
		exit();
	}

	/**
	 * pdf view of a news record
	 *
	 * @param integer $uid news item uid
	 * @param string $hash news hash
	 */
	public function pdfAction(
		$uid = 0
	){
		if($uid)
		{
			$news = $this->newsRepository->findByUid($uid,FALSE);

			if($news)
			{
				$this->view->assign('newsItem', $news);

				$html = $this->view->render();

				$cached = $this->pdfGenerator->isCached(md5($news->getHash()));
				if($cached && $cached['time']>=$news->getTstamp()->format('U'))
				{
					$this->pdfGenerator->getCached(md5($news->getHash()), TRUE, $news->getTitle(), FALSE);
				}
				else
				{
					$pdf = $this->pdfGenerator->generateFromHtml($html,FALSE,$news->getTitle());
					$this->pdfGenerator->writeCache(md5($news->getHash()), $pdf['data']);
				}
			}
		}
		exit();
	}
	
	/**
     * Set the TsConfig configuration for the extension
     *
     */
    protected function setTsConfig()
    {
        $tsConfig = BackendUtility($this->pageUid);
        if (isset($tsConfig['tx_mooxnews.']['module.']) && is_array($tsConfig['tx_mooxnews.']['module.']))
		{
            $this->tsConfiguration = $tsConfig['tx_mooxnews.']['module.'];
        }
    }
	
	/**
     * Get a CSRF token
     *
     * @param bool $tokenOnly Set it to TRUE to get only the token, otherwise including the &moduleToken= as prefix
     * @return string
     */
    protected function getToken($tokenOnly = false)
    {
        $token = FormProtectionFactory::get()->generateToken('moduleCall', 'moox_MooxNewsMod1');
        if ($tokenOnly)
		{
            return $token;
        } 
		else 
		{
            return '&moduleToken=' . $token;
        }
    }
	
    /**
     * @return LanguageService
     */
    protected function getLanguageService()
    {
        return $GLOBALS['LANG'];
    }

    /**
     * @return \TYPO3\CMS\Core\Authentication\BackendUserAuthentication
     */
    protected function getBackendUser()
    {
        return $GLOBALS['BE_USER'];
    }
	
	/**
	 * @return int
	 */
	public function getStoragePage()
	{
		return $this->storagePage;
	}

	/**
	 * @param int $storagePage
	 */
	public function setStoragePage($storagePage)
	{
		$this->storagePage = $storagePage;
	}
	
	/**
	 * @return array
	 */
	public function getExtConf()
	{
		return $this->extConf;
	}

	/**
	 * @param array $extConf
	 */
	public function setExtConf($extConf)
	{
		$this->extConf = $extConf;
	}		
	
	/**
	 * @return array
	 */
	public function getTypes()
	{
		return $this->types;
	}

	/**
	 * @param array $types
	 */
	public function setTypes($types)
	{
		$this->types = $types;
	}

	/**
	 * @return array
	 */
	public function getYears()
	{
		return $this->years;
	}

	/**
	 * @param array $years
	 */
	public function setYears($years)
	{
		$this->years = $years;
	}

	/**
	 * @return array
	 */
	public function getGroupedFolders()
	{
		return $this->groupedFolders;
	}

	/**
	 * @param array $groupedFolders
	 */
	public function setGroupedFolders($groupedFolders)
	{
		$this->groupedFolders = $groupedFolders;
	}

	/**
	 * @return array
	 */
	public function getFolders()
	{
		return $this->folders;
	}

	/**
	 * @param array $folders
	 */
	public function setFolders($folders)
	{
		$this->folders = $folders;
	}
	
	/**
	 * @return array
	 */
	public function getFilter()
	{
		return $this->filter;
	}

	/**
	 * @param array $filter
	 */
	public function setFilter($filter)
	{
		$this->filter = $filter;
	}
	
	/**
	 * @return array
	 */
	public function getOrdering()
	{
		return $this->ordering;
	}

	/**
	 * @param array $ordering
	*/
	public function setOrdering($ordering)
	{
		$this->ordering = $ordering;
	}

	/**
	 * @return array
	 */
	public function getLanguages()
	{
		return $this->languages;
	}

	/**
	 * @param array $languages
	 */
	public function setLanguages($languages)
	{
		$this->languages = $languages;
	}
}
?>