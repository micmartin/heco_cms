<?php
namespace Neulandlotsen\MooxMailer\Controller;

/**
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */

use Neulandlotsen\MooxMailer\Domain\Repository\NewsletterRepository;
use Neulandlotsen\MooxMailer\Domain\Repository\FrontendUserRepository;
use Neulandlotsen\MooxMailer\Domain\Repository\AddressRepository;
use Neulandlotsen\MooxMailer\Domain\Repository\ClickRepository;
use Neulandlotsen\MooxMailer\Domain\Repository\ZipregionRepository;
use Neulandlotsen\MooxMailer\Service\BackendSessionService;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Database\Query\Restriction\DeletedRestriction;
use TYPO3\CMS\Core\Database\Query\Restriction\HiddenRestriction;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Utility\HttpUtility;
use TYPO3\CMS\Core\Messaging\FlashMessage;
use TYPO3\CMS\Core\TypoScript\ExtendedTemplateService;
use TYPO3\CMS\Core\Utility\RootlineUtility;
use TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface;
use TYPO3\CMS\Frontend\Page\PageRepository;

// reference the Dompdf namespace
use Dompdf\Dompdf;
use Dompdf\Options;
 
class NewsletterController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController 
{
	
	/**
	 * page type of redirector
	 *
	 * @var int
	 */	
	public static $redirectorType = 9001;
	
	/**
	 * page type of unregister
	 *
	 * @var int
	 */	
	public static $unregisterType = 9002;
	
	/**
	 * page type of html view
	 *
	 * @var int
	 */	
	public static $htmlViewType = 9003;
	
	/**
	 * subject prefix of test sending
	 *
	 * @var string
	 */	
	public static $subjectPrefix = "[Testversand] ";
	
	/**
	 * directory of mailer service
	 *
	 * @var string
	 */	
	public static $mailDir = '/maildir';
	
	/**
	 * mode of mailer service
	 *
	 * @var string
	 */	
	public static $mode = 'daemon';
	
	/**
	 * format of mailer service
	 *
	 * @var string
	 */	
	public static $format = 'text/html';
	
	/**
	 * count mode of mailer service
	 *
	 * @var string
	 */	
	public static $countMode = 'live';
	
	/**
	 * script timeout of mailer service
	 *
	 * @var int
	 */	
	public static $scriptTimeout = 90;
	
	/**
	 * memory limit of mailer service
	 *
	 * @var int
	 */	
	public static $memoryLimit = 1024;
	
	/**
	 * no log option of mailer service
	 *
	 * @var bool
	 */	
	public static $noLog = false;
		
	/**
	 * indexUrl
	 *
	 * @var string
	 */
	public static $indexUrl = "index.php?id=##id##&type=##type##&tx_mooxmailer_pi1[id]=##id##";
	
	/**
	 * indexHtmlViewUrl
	 *
	 * @var string
	 */
	public static $indexHtmlViewUrl = "index.php?id=##id##&type=##type##&tx_mooxmailer_pi3[uid]=##uid##&tx_mooxmailer_pi3[hash]=##hash##";
	
	/**
	 * trackingSalt
	 *
	 * @var string
	 */
	public static $trackingSalt = "Sf0Cn3OpNFelyH";	
	
	/**
	 * task name
	 *
	 * @var string
	 */
	public static $taskName = "newsletter";
	
	/**
	 * @var NewsletterRepository
	 */
	protected $newsletterRepository;
	
	/**
	 * @var PageRepository
	 */
	protected $pageRepository;
	
	/**
	 * @var FrontendUserRepository
	 */
	protected $frontendUserRepository;
	
	/**
	 * @var AddressRepository
	 */
	protected $addressRepository;
	
	/**
	 * @var ClickRepository
	 */
	protected $clickRepository;
	
	/**
	 * @var ZipregionRepository
	 */
	protected $zipregionRepository;
	
	/**
	 * @var BackendSessionService
	 */
	protected $backendSession;
	
	/**
	 * page
	 *
	 * @var int
	 */
	protected $page;
	
	/**
	 * extConf
	 *
	 * @var bool
	 */
	protected $extConf;
		
	/**
	 * affected users
	 *
	 * @var array
	 */
	protected $affectedUsers;
	
	/**
	 * storage page
	 *
	 * @var int
	 */
	protected $storagePage;

    /**
     * @param NewsletterRepository $newsletterRepository
     */
    public function injectNewsletterRepository(NewsletterRepository $newsletterRepository)
    {
        $this->newsletterRepository = $newsletterRepository;
    }

    /**
     * @param PageRepository $pageRepository
     */
    public function injectPageRepository(PageRepository $pageRepository)
    {
        $this->pageRepository = $pageRepository;
    }

    /**
     * @param FrontendUserRepository $frontendUserRepository
     */
    public function injectFrontendUserRepository(FrontendUserRepository $frontendUserRepository)
    {
        $this->frontendUserRepository = $frontendUserRepository;
    }

    /**
     * @param AddressRepository $addressRepository
     */
    public function injectAddressRepository(AddressRepository $addressRepository)
    {
        $this->addressRepository = $addressRepository;
    }

    /**
     * @param ClickRepository $clickRepository
     */
    public function injectClickRepository(ClickRepository $clickRepository)
    {
        $this->clickRepository = $clickRepository;
    }

    /**
     * @param ZipregionRepository $zipregionRepository
     */
    public function injectZipregionRepository(ZipregionRepository $zipregionRepository)
    {
        $this->zipregionRepository = $zipregionRepository;
    }

    /**
     * @param BackendSessionService $backendSession
     */
    public function injectBackendSessionService(BackendSessionService $backendSession)
    {
        $this->backendSession = $backendSession;
    }
	
	/**
	 * Replace markers with real content
	 *
	 * @param string $content
	 * @param object $object
	 * @return void
	 */
	public static function replaceMarkers($content = "", $object = NULL) 
	{		
		$content = str_replace("#KW#",date("W"),$content);
		$content = str_replace("#YEAR#",date("Y"),$content);
		$content = str_replace("#MONTH#",date("m"),$content);
		$content = str_replace("#DAY#",date("d"),$content);
		$content = str_replace("#HOUR#",date("H"),$content);
		$content = str_replace("#MINUTE#",date("i"),$content);
		$content = str_replace("#SECOND#",date("s"),$content);
		if(is_object($object)){
			if($object->getPage()>0){
				$objectManager = GeneralUtility::makeInstance('TYPO3\CMS\Extbase\Object\ObjectManager');		
				$pageRepository = $objectManager->get('TYPO3\\CMS\\Frontend\\Page\\PageRepository');
				$pageInfo = $pageRepository->getPage($object->getPage());			
			}
			$content = str_replace("#PAGETITLE#",$pageInfo['title'],$content);
			$content = str_replace("#TITLE#",$object->getTitle(),$content);
			$content = str_replace("#SENDERNAME#",$object->getSenderName(),$content);
			$content = str_replace("#SENDEREMAIL#",$object->getSenderEmail(),$content);
		}
		
		return $content;
	}
	
	/**
	 * Clean up ziplist
	 *
	 * @param string $list
	 * @return void
	 */
	public static function cleanUpZiplist($list) 
	{		
		$cleaned = "";
		$zipcodes = [];
		$list = trim($list);
		if($list != ''){
			$list = str_replace(";",",",$list);
			$zipcodes_tmp = explode(",",$list);
			foreach($zipcodes_tmp AS $zipcode){
				if(is_numeric(trim($zipcode))){
					$zipcodes[] = trim($zipcode);
				}
			}
			if(count($zipcodes)){
				sort($zipcodes,SORT_STRING);
				$cleaned = implode(",",$zipcodes);
			}
		}
		return $cleaned;
	}
	
	/**
	 * sort helper function
	 *
	 * @param array $a
	 * @param array $b
	 * @return void
	 */
	static public function sortByFolderAndTitle($a, $b)
	{
		return strcmp($a["folder"].$a["title"], $b["folder"].$b["title"]);
	}
	
	/**
	 * initialize the controller
	 *
	 * @return void
	 */
	protected function initializeAction() 
	{
		parent::initializeAction();
		
		$this->setPage((int)GeneralUtility::_GET('id'));
		
		$this->extConf = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Configuration\ExtensionConfiguration::class)->get('moox_mailer');
		
		$this->initializeStorageSettings();
					
		$this->affectedUsers = [];
	}
	
	/**
	 * initialize storage settings
	 *	 
	 * @return void
	 */
	protected function initializeStorageSettings()
	{				
		//fallback to current pid if no storagePid is defined
		if (version_compare(TYPO3_version, '6.0.0', '>=')) {
			$configuration = $this->configurationManager->getConfiguration(ConfigurationManagerInterface::CONFIGURATION_TYPE_FRAMEWORK);
		} else {
			$configuration = $this->configurationManager->getConfiguration(Tx_Extbase_Configuration_ConfigurationManagerInterface::CONFIGURATION_TYPE_FRAMEWORK);
		}
		if (empty($configuration['persistence']['storagePid'])) {
			$currentPid['persistence']['storagePid'] = $this->page;
			$this->storagePage = $currentPid['persistence']['storagePid'];
			$this->configurationManager->setConfiguration(array_merge($configuration, $currentPid));
		} else {
			$this->storagePage = $configuration['persistence']['storagePid'];
		}
	}
	
	/**
	 * action index
	 *
	 * @param array $filter	
	 * @return void
	 */
	public function indexAction($filter = [])
	{				
		$folders = $this->getFolders();
		
		$this->redirectToFolder($folders);
		
		$filter = $this->processFilter($filter);
		
		if($this->request->hasArgument('@widget_0')){
			$pagination = $this->request->getArgument('@widget_0');			
		}
		
		$rootline = GeneralUtility::makeInstance(RootlineUtility::class, $this->page, '', $this->pageRepository)->get();
		
		foreach($rootline AS $rootlinepage){
			if($rootlinepage['is_siteroot']){
				$rootpage = $rootlinepage;
				break;
			}
		}
		
		if(!$rootpage){
			$rootpage = $rootline[0];
		}
		
		$rootfound = false;
		for($i=0;$i<count($rootline);$i++){
			if($rootfound){
				unset($rootline[$i]);
			} else {
				if($rootline[$i]['is_siteroot']){
					$rootfound = true;
				}
			}
		}
		
		$rootline = array_reverse($rootline);
		
		if(isset($rootline[count($rootline)-2])){			
			$pageInfo = $this->pageRepository->getPage((int)$rootline[count($rootline)-2]['uid']);		
			if(in_array($pageInfo['module'],array("mxmailer"))){
				$folder = $pageInfo['uid'];				
			}
			
		}			
		
		$newsletters = $this->newsletterRepository->findAll($this->page,$filter);
		
		$this->view->assign('newsletters', $newsletters);
		$this->view->assign('filter', $filter);
		$this->view->assign('action', 'index');
		$this->view->assign('page', $this->page);
		$this->view->assign('folder', $folder);
		$this->view->assign('rootpage', $rootpage);
		$this->view->assign('rootline', $rootline);
		$this->view->assign('folders', (count($folders)>0)?$folders:false);
		$this->view->assign('pagination', $pagination);
		$this->view->assign('conf', $this->extConf);
	}
	
	/**
	 * action add
	 *	
	 * @param int $uid
	 * @param array $add
	 * @param string $redirectorController
	 * @param string $redirectorAction
	 * @return void
	 */
	public function addAction($uid = 0, $add = [], $redirectorController = "Newsletter", $redirectorAction = "index")
	{			
		$addCount = count($add);
		
		if($add['redirectorController']!=""){
			$redirectorController = $add['redirectorController'];
		} else {
			$add['redirectorController'] = $redirectorController;
		}
		if($add['redirectorAction']!=""){
			$redirectorAction = $add['redirectorAction'];
		} else {
			$add['redirectorAction'] = $redirectorAction;
		}
		
		$addressFolders = array();
		$testAddressFolders = array();
		foreach($this->getFolders("mxaddress") AS $addressFolder){
			$addressFolders[$addressFolder['uid']] = $addressFolder['title']. " [UID: ".$addressFolder['uid']."]";
			$testAddressFolders[$addressFolder['uid']] = $addressFolder['title']. " [UID: ".$addressFolder['uid']."]";
		}
		
		$userFolders = array();
		foreach($this->getFolders("mxfeuser") AS $userFolder){
			$userFolders[$userFolder['uid']] = $userFolder['title']. " [UID: ".$userFolder['uid']."]";
		}
		
		$moveToFolders = array();
		$moveToFolders[0] = "Nach dem Senden nicht verschieben";
		foreach($this->getFolders("mxsent") AS $moveToFolder){
			$moveToFolders[$moveToFolder['uid']] = $moveToFolder['title']. " [UID: ".$moveToFolder['uid']."]";
		}
		
		$draftFolders = $this->getFolders("mxdrafts");
		if(!count($draftFolders)){
			$noDraftFolders = true;
		}
		
		$zipmodes = [];
		//$zipmodes[] = array("none" => "Keine Einschränkung nach Postleitzahlen");
		$zipmodes["list"] = "Freie Postleitzahlenliste";
		$zipregions = $this->zipregionRepository->findAll(FALSE);
		foreach($zipregions AS $zipregion){
			$zipmodes[$zipregion->getUid()] = "[Vordefiniert]: ".$zipregion->getTitle();
		}
		
		if($uid>0){
			$object = $this->newsletterRepository->findByUid($uid,FALSE);
			
			$add['title'] = "Kopie von ".$object->getTitle();
			$add['subject'] = $object->getSubject();
			$add['senderName'] = $object->getSenderName();
			$add['senderEmail'] = $object->getSenderEmail();
			$add['replyToEmail'] = $object->getReplyToEmail();
			if($object->getUserFolders()!=""){
				$add['userFolders']	= unserialize($object->getUserFolders());
			}
			if($object->getAddressFolders()!=""){
				$add['addressFolders'] = unserialize($object->getAddressFolders());
			}
			if($object->getTestAddressFolders()!=""){
				$add['testAddressFolders'] = unserialize($object->getTestAddressFolders());
			}
			$add['moveToFolder'] = $object->getMoveToFolder();
			$add['autologin'] = $object->getAutologin();
			$add['zipmode'] = $object->getZipmode();
			$add['ziplist'] = $object->getZiplist();
		} elseif(!$addCount){
			
			$add['title'] = ($this->settings['newsletterTitle'])?self::replaceMarkers($this->settings['newsletterTitle']):"";
			$add['subject'] = ($this->settings['newsletterSubject'])?$this->settings['newsletterSubject']:"Newsletter ##KW##";
			$add['senderEmail'] = ($this->settings['newsletterSenderEmail'])?$this->settings['newsletterSenderEmail']:"";
			$add['replyToEmail'] = ($this->settings['newsletterReplyToEmail'])?$this->settings['newsletterReplyToEmail']:"";
			$add['senderName'] = ($this->settings['newsletterSenderName'])?$this->settings['newsletterSenderName']:"";
			$add['userFolders'] = ($this->settings['newsletterUserFolders'])?explode(",",$this->settings['newsletterUserFolders']):array();
			$add['addressFolders'] = ($this->settings['newsletterAddressFolders'])?explode(",",$this->settings['newsletterAddressFolders']):array();
			$add['testAddressFolders'] = ($this->settings['newsletterTestAddressFolders'])?explode(",",$this->settings['newsletterTestAddressFolders']):array();
			$add['moveToFolder'] = ($this->settings['newsletterMoveToFolder'])?$this->settings['newsletterMoveToFolder']:"";
			$add['autologin'] = ($this->settings['newsletterAutologin'])?1:0;
			$add['zipmode'] = ($this->settings['zipmode'])?$this->settings['zipmode']:"";
			$add['ziplist'] = ($this->settings['ziplist'])?$this->settings['ziplist']:"";
		}
		$add['ziplist'] = self::cleanUpZiplist($add['ziplist']);
				
		if($add['autologin'] && is_array($add['userFolders']) && count($add['userFolders'])>0){
			$this->affectedUsers = $this->getFeUsers($add['userFolders']);				
		}
		if(strpos($add['currentUserEmail'],"@")!==FALSE){
			$currentUser = $this->affectedUsers[$add['currentUserEmail']];
		}
		
		if(isset($add['save']) || isset($add['saveAndClose']) || isset($add['saveAndNew']) || isset($add['saveAndSend'])){
			
			$hasErrors = false;			
			$errors = array();
			$errorMessages = array();						
			
			if($add['page']<1 && FALSE){					
				$errorMessages[] = 	array( 
					"title" => "Entwurf",
					"message" => "Bitte wählen Sie die Seite aus die Sie verschicken möchten"
				);					
				$allErrors['page'] = true;				
				$hasErrors = true;
			}
			if($add['title']==""){					
				$errorMessages[] = 	array( 
					"title" => "Titel",
					"message" => "Bitte geben Sie einen Titel für den Newsletter ein. Dieser wird für die interne Verwaltung benötigt"
				);					
				$allErrors['title']	= true;			
				$hasErrors = true;
			}
			if($add['subject']==""){					
				$errorMessages[] = 	array( 
					"title" => "Betreff",
					"message" => "Bitte geben Sie einen Betreff für den Newsletter ein"
				);					
				$allErrors['subject'] = true;			
				$hasErrors = true;
			}
			if(trim($add['senderEmail'])==""){					
				$errorMessages[] = 	array( 
					"title" => "Absender-Adresse",
					"message" => "Bitte geben Sie eine Email-Adresse ein."
				);					
				$allErrors['senderEmail'] = true;				
				$hasErrors = true;
			} elseif(!GeneralUtility::validEmail(trim($add['senderEmail']))){					
				$errorMessages[] = 	array( 
					"title" => "Absender-Adresse",
					"message" => "Bitte geben Sie eine korrekte Email-Adresse ein."
				);					
				$allErrors['senderEmail'] = true;				
				$hasErrors = true;
			}
			if(trim($add['replyToEmail'])!="" && !GeneralUtility::validEmail(trim($add['replyToEmail']))){					
				$errorMessages[] = 	array( 
					"title" => "Antwort-Adresse",
					"message" => "Bitte geben Sie eine korrekte Email-Adresse ein."
				);					
				$allErrors['replyToEmail'] = true;				
				$hasErrors = true;
			}
			if($add['senderName']==""){					
				$errorMessages[] = 	array( 
					"title" => "Absender-Name",
					"message" => "Bitte geben Sie einen Absender-Namen ein"
				);					
				$allErrors['senderName'] = true;			
				$hasErrors = true;
			}
			if($add['userFolders']=="" && $add['addressFolders']==""){					
				$errorMessages[] = 	array( 
					"title" => "Empfänger-Auswahl",
					"message" => "Bitte wählen Sie mindestens einen Benutzer-Ordner oder einen Adress-Ordner aus"
				);					
				$allErrors['userFolders'] = true;		
				$allErrors['addressFolders'] = true;	
				$hasErrors = true;
			}
			if($add['zipmode']=="list" && $add['ziplist']==""){					
				$errorMessages[] = 	array( 
					"title" => "Postleitzahlenliste",
					"message" => "Bitte gebe Sie mindestens eine Postleitzahl ein oder wählen Sie einen anderen Modus aus"
				);					
				$allErrors['zipmode'] = true;		
				$allErrors['ziplist'] = true;	
				$hasErrors = true;
			}
			
			if(!$hasErrors){
				
				$object = $this->objectManager->get('Neulandlotsen\\MooxMailer\\Domain\\Model\\Newsletter');
								
				$object->setPage($add['page']);			
				$object->setTitle($add['title']);
				$object->setSubject($add['subject']);
				$object->setSenderName($add['senderName']);
				$object->setSenderEmail($add['senderEmail']);
				$object->setReplyToEmail($add['replyToEmail']);
				
				if(is_array($add['userFolders']) && count($add['userFolders'])){
					$object->setUserFolders(serialize($add['userFolders']));
				} else {
					$object->setUserFolders("");
				}
				if(is_array($add['addressFolders']) && count($add['addressFolders'])){
					$object->setAddressFolders(serialize($add['addressFolders']));
				} else {
					$object->setAddressFolders("");
				}
				if(is_array($add['testAddressFolders']) && count($add['testAddressFolders'])){
					$object->setTestAddressFolders(serialize($add['testAddressFolders']));
				} else {
					$object->setTestAddressFolders("");
				}
				$object->setMoveToFolder($add['moveToFolder']);
				$object->setAutologin(($add['autologin'])?1:0);
				$object->setZipmode($add['zipmode']);	
				$object->setZiplist($add['ziplist']);
				
				$this->newsletterRepository->add($object);								
				$this->objectManager->get('TYPO3\CMS\Extbase\Persistence\PersistenceManagerInterface')->persistAll();
				
				$this->addFlashMessage(
					'', 
					'Entwurf wurde erfolgreich gespeichert.', 
					FlashMessage::OK
				);
			
				if(isset($add['save'])){
					$params['uid'] = $object->getUid();
					if($redirectorController!=""){
						$params['redirectorController'] = $redirectorController;
					}
					if($redirectorAction!=""){
						$params['redirectorAction'] = $redirectorAction;
					}
					$this->redirect("edit",NULL,NULL,$params);
				} elseif(isset($add['saveAndClose'])){
					$this->redirect($redirectorAction,$redirectorController);
				} elseif(isset($add['saveAndNew'])){
					if($redirectorController!=""){
						$params['redirectorController'] = $redirectorController;
					}
					if($redirectorAction!=""){
						$params['redirectorAction'] = $redirectorAction;
					}
					$this->redirect("add",NULL,NULL,$params);
				} elseif(isset($add['saveAndSend'])){			
					$params['uid'] = $object->getUid();
					if($redirectorController!=""){
						$params['redirectorController'] = $redirectorController;
					}
					if($redirectorAction!=""){
						$params['redirectorAction'] = $redirectorAction;
					}
					$this->redirect("send",NULL,NULL,$params);
				} else {			
					$this->view->assign('object', $add);			
					$this->view->assign('action', 'add');
					$this->view->assign('page', $this->page);
					$this->view->assign('noDraftFolders', $noDraftFolders);					
					$this->view->assign('drafts', $this->getDrafts());
					$this->view->assign('userFolders', $userFolders);
					$this->view->assign('addressFolders', $addressFolders);
					$this->view->assign('testAddressFolders', $testAddressFolders);
					$this->view->assign('moveToFolders', $moveToFolders);					
					$this->view->assign('zipmodes', $zipmodes);					
					$this->view->assign('users', $this->affectedUsers);
					$this->view->assign('currentUser', $currentUser);
				}
				
			} else {					
					
				foreach($errorMessages AS $errorMessage){
					$this->addFlashMessage($errorMessage['message'], ($errorMessage['title']!="")?$errorMessage['title'].": ":"", FlashMessage::ERROR);
				}
					
				$this->view->assign('allErrors', $allErrors);									
				$this->view->assign('object', $add);			
				$this->view->assign('action', 'add');
				$this->view->assign('page', $this->page);
				$this->view->assign('drafts', $this->getDrafts());
				$this->view->assign('noDraftFolders', $noDraftFolders);
				$this->view->assign('userFolders', $userFolders);
				$this->view->assign('addressFolders', $addressFolders);
				$this->view->assign('moveToFolders', $moveToFolders);
				$this->view->assign('zipmodes', $zipmodes);
				$this->view->assign('users', $this->affectedUsers);
				$this->view->assign('currentUser', $currentUser);
			}
			
		} else {
			$this->view->assign('allErrors', $allErrors);
			$this->view->assign('object', $add);			
			$this->view->assign('action', 'add');
			$this->view->assign('page', $this->page);
			$this->view->assign('drafts', $this->getDrafts());
			$this->view->assign('noDraftFolders', $noDraftFolders);
			$this->view->assign('userFolders', $userFolders);
			$this->view->assign('addressFolders', $addressFolders);
			$this->view->assign('testAddressFolders', $testAddressFolders);
			$this->view->assign('moveToFolders', $moveToFolders);
			$this->view->assign('zipmodes', $zipmodes);
			$this->view->assign('users', $this->affectedUsers);
			$this->view->assign('currentUser', $currentUser);
		}				
	}
	
	/**
	 * action edit
	 *
	 * @param int $uid
	 * @param array $edit
	 * @param array $pagination
	 * @param string $redirectorController
	 * @param string $redirectorAction
	 * @return void
	 */
	public function editAction($uid = 0, $edit = array(), $pagination = array(), $redirectorController = "Newsletter", $redirectorAction = "index")
	{			
		if($uid>0){						
		
			$addressFolders = array();
			$testAddressFolders = array();
			foreach($this->getFolders("mxaddress") AS $addressFolder){
				$addressFolders[$addressFolder['uid']] = $addressFolder['title']. " [UID: ".$addressFolder['uid']."]";
				$testAddressFolders[$addressFolder['uid']] = $addressFolder['title']. " [UID: ".$addressFolder['uid']."]";
			}
			
			$userFolders = array();
			foreach($this->getFolders("mxfeuser") AS $userFolder){
				$userFolders[$userFolder['uid']] = $userFolder['title']. " [UID: ".$userFolder['uid']."]";
			}
			
			$moveToFolders = array();
			$moveToFolders[0] = "Nach dem Senden nicht verschieben";
			foreach($this->getFolders("mxsent") AS $moveToFolder){
				$moveToFolders[$moveToFolder['uid']] = $moveToFolder['title']. " [UID: ".$moveToFolder['uid']."]";
			}	

			$draftFolders = $this->getFolders("mxdrafts");
			if(!count($draftFolders)){
				$noDraftFolders = true;
			}
			
			$zipmodes = [];
			//$zipmodes[] = array("none" => "Keine Einschränkung nach Postleitzahlen");
			$zipmodes["list"] = "Freie Postleitzahlenliste";
			$zipregions = $this->zipregionRepository->findAll(FALSE);
			foreach($zipregions AS $zipregion){
				$zipmodes[$zipregion->getUid()] = "[Vordefiniert]: ".$zipregion->getTitle();
			}
					
			$object = $this->newsletterRepository->findByUid($uid,FALSE);
			
			if(!count($edit)){
				$edit['page'] = $object->getPage();
				$edit['title'] = $object->getTitle();
				$edit['subject'] = $object->getSubject();
				$edit['senderName'] = $object->getSenderName();
				$edit['senderEmail'] = $object->getSenderEmail();
				$edit['replyToEmail'] = $object->getReplyToEmail();
				if($object->getUserFolders()!=""){
					$edit['userFolders'] = unserialize($object->getUserFolders());
				}
				if($object->getAddressFolders()!=""){
					$edit['addressFolders'] = unserialize($object->getAddressFolders());
				}
				if($object->getTestAddressFolders()!=""){
					$edit['testAddressFolders'] = unserialize($object->getTestAddressFolders());
				}
				$edit['moveToFolder'] = $object->getMoveToFolder();
				$edit['autologin'] = $object->getAutologin();	
				$edit['zipmode'] = $object->getZipmode();
				$edit['ziplist'] = $object->getZiplist();				
				$edit['uid'] = $object->getUid();				
			} else {				
				$object->setPage($edit['page']);			
				$object->setTitle($edit['title']);
				$object->setSubject($edit['subject']);
				$object->setSenderName($edit['senderName']);
				$object->setSenderEmail($edit['senderEmail']);	
				$object->setReplyToEmail($edit['replyToEmail']);	
				if(is_array($edit['userFolders'])){
					$object->setUserFolders(serialize($edit['userFolders']));
				} else {
					$object->setUserFolders("");
				}
				if(is_array($edit['addressFolders'])){
					$object->setAddressFolders(serialize($edit['addressFolders']));
				} else {
					$object->setAddressFolders("");
				}	
				if(is_array($edit['testAddressFolders'])){
					$object->setTestAddressFolders(serialize($edit['testAddressFolders']));
				} else {
					$object->setTestAddressFolders("");
				}				
				$object->setMoveToFolder($edit['moveToFolder']);
				$object->setAutologin($edit['autologin']);
				$object->setZipmode($edit['zipmode']);
				$edit['ziplist'] = self::cleanUpZiplist($edit['ziplist']);
				$object->setZiplist($edit['ziplist']);
			}
			
			if($edit['redirectorController']!=""){
				$redirectorController = $edit['redirectorController'];
			} else {
				$edit['redirectorController'] = $redirectorController;
			}
			if($edit['redirectorAction']!=""){
				$redirectorAction = $edit['redirectorAction'];
			} else {
				$edit['redirectorAction'] = $redirectorAction;
			}
						
			if($edit['autologin'] && count($edit['userFolders'])>0){
				$this->affectedUsers = $this->getFeUsers($edit['userFolders']);				
			}
			if(strpos($edit['currentUserEmail'],"@")!==FALSE){
				$currentUser = $this->affectedUsers[$edit['currentUserEmail']];
			}
						
			if(isset($edit['save']) || isset($edit['saveAndClose']) || isset($edit['saveAndNew']) || isset($edit['saveAndSend'])){
				
				$hasErrors = false;			
				$errors = array();
				$errorMessages = array();								
				
				if($edit['page']<1 && FALSE){					
					$errorMessages[] = array( 
						"title" => "Entwurf",
						"message" => "Bitte wählen Sie die Seite aus die Sie verschicken möchten"
					);					
					$allErrors['page'] = true;				
					$hasErrors = true;
				}
				if($edit['title']==""){					
					$errorMessages[] = array( 
						"title" => "Titel",
						"message" => "Bitte geben Sie einen Titel für den Newsletter ein. Dieser wird für die interne Verwaltung benötigt"
					);					
					$allErrors['title']	= true;			
					$hasErrors = true;
				}
				if($edit['subject']==""){					
					$errorMessages[] = array( 
						"title" => "Betreff",
						"message" => "Bitte geben Sie einen Betreff für den Newsletter ein"
					);					
					$allErrors['subject'] = true;			
					$hasErrors = true;
				}
				if(trim($edit['senderEmail'])==""){					
					$errorMessages[] = array( 
						"title" => "Absender-Adresse",
						"message" => "Bitte geben Sie eine Email-Adresse ein."
					);					
					$allErrors['senderEmail'] = true;				
					$hasErrors = true;
				} elseif(!GeneralUtility::validEmail(trim($edit['senderEmail']))){					
					$errorMessages[] = array( 
						"title" => "Absender-Adresse",
						"message" => "Bitte geben Sie eine korrekte Email-Adresse ein."
					);					
					$allErrors['senderEmail'] = true;				
					$hasErrors = true;
				}
				if(trim($edit['replyToEmail'])!="" && !GeneralUtility::validEmail(trim($edit['replyToEmail']))){					
					$errorMessages[] = array( 
						"title" => "Antwort-Adresse",
						"message" => "Bitte geben Sie eine korrekte Email-Adresse ein."
					);					
					$allErrors['replyToEmail'] = true;				
					$hasErrors = true;
				}
				if($edit['senderName']==""){					
					$errorMessages[] = array( 
						"title" => "Absender-Name",
						"message" => "Bitte geben Sie einen Absender-Namen ein"
					);					
					$allErrors['senderName'] = true;			
					$hasErrors = true;
				}
				if($edit['userFolders']=="" && $edit['addressFolders']==""){					
					$errorMessages[] = array( 
						"title" => "Empfänger-Auswahl",
						"message" => "Bitte wählen Sie mindestens einen Benutzer-Ordner oder einen Adress-Ordner aus"
					);					
					$allErrors['userFolders'] = true;		
					$allErrors['addressFolders'] = true;	
					$hasErrors = true;
				}
				if($edit['zipmode']=="list" && $edit['ziplist']==""){					
					$errorMessages[] = 	array( 
						"title" => "Postleitzahlenliste",
						"message" => "Bitte gebe Sie mindestens eine Postleitzahl ein oder wählen Sie einen anderen Modus aus"
					);					
					$allErrors['zipmode'] = true;		
					$allErrors['ziplist'] = true;	
					$hasErrors = true;
				}
				
				if(!$hasErrors){					
										
					$object->setPage($edit['page']);			
					$object->setTitle($edit['title']);
					$object->setSubject($edit['subject']);
					$object->setSenderName($edit['senderName']);
					$object->setSenderEmail($edit['senderEmail']);
					$object->setReplyToEmail($edit['replyToEmail']);
					if(is_array($edit['userFolders']) && count($edit['userFolders'])){
						$object->setUserFolders(serialize($edit['userFolders']));
					} else {
						$object->setUserFolders("");
					}
					if(is_array($edit['addressFolders']) && count($edit['addressFolders'])){
						$object->setAddressFolders(serialize($edit['addressFolders']));
					} else {
						$object->setAddressFolders("");
					}	
					if(is_array($edit['testAddressFolders']) && count($edit['testAddressFolders'])){
						$object->setTestAddressFolders(serialize($edit['testAddressFolders']));
					} else {
						$object->setTestAddressFolders("");
					}
					$object->setMoveToFolder($edit['moveToFolder']);
					$object->setAutologin($edit['autologin']);
										
					$this->newsletterRepository->update($object);								
					$this->objectManager->get('TYPO3\CMS\Extbase\Persistence\PersistenceManagerInterface')->persistAll();
					
					$this->addFlashMessage(
						'', 
						'Änderungen wurden erfolgreich gespeichert.', 
						FlashMessage::OK);
				
					if(isset($edit['saveAndClose'])){
						if($pagination['currentPage']>1){
							$this->redirect("index","Newsletter","MooxMailer",array('@widget_0' => $pagination));
						} else {
							$this->redirect($redirectorAction,$redirectorController);
						}
					} elseif(isset($edit['saveAndNew'])){
						if($redirectorController!=""){
							$params['redirectorController'] = $redirectorController;
						}
						if($redirectorAction!=""){
							$params['redirectorAction'] 	= $redirectorAction;
						}
						$this->redirect("add",NULL,NULL,$params);
					} elseif(isset($edit['saveAndSend'])){
						$params['uid'] 						= $object->getUid();
						if($redirectorController!=""){
							$params['redirectorController'] = $redirectorController;
						}
						if($redirectorAction!=""){
							$params['redirectorAction'] 	= $redirectorAction;
						}
						$this->redirect("send",NULL,NULL,$params);
					} else {
						$this->view->assign('object', $edit);				
						$this->view->assign('action', 'edit');
						$this->view->assign('uid', $uid);
						$this->view->assign('pagination', $pagination);
						$this->view->assign('page', $this->page);
						$this->view->assign('noDraftFolders', $noDraftFolders);
						$this->view->assign('drafts', $this->getDrafts());
						$this->view->assign('userFolders', $userFolders);
						$this->view->assign('addressFolders', $addressFolders);
						$this->view->assign('testAddressFolders', $testAddressFolders);
						$this->view->assign('moveToFolders', $moveToFolders);
						$this->view->assign('zipmodes', $zipmodes);
						$this->view->assign('users', $this->affectedUsers);
						$this->view->assign('currentUser', $currentUser);							
					}
					
				} else {					
					
					foreach($errorMessages AS $errorMessage){
						$this->addFlashMessage($errorMessage['message'], ($errorMessage['title']!="")?$errorMessage['title'].": ":"", FlashMessage::ERROR);
					}
					
					$this->view->assign('allErrors', $allErrors);										
					$this->view->assign('object', $edit);				
					$this->view->assign('action', 'edit');
					$this->view->assign('uid', $uid);
					$this->view->assign('pagination', $pagination);
					$this->view->assign('page', $this->page);
					$this->view->assign('noDraftFolders', $noDraftFolders);
					$this->view->assign('drafts', $this->getDrafts());
					$this->view->assign('userFolders', $userFolders);
					$this->view->assign('addressFolders', $addressFolders);
					$this->view->assign('moveToFolders', $moveToFolders);
					$this->view->assign('zipmodes', $zipmodes);
					$this->view->assign('users', $this->affectedUsers);
					$this->view->assign('currentUser', $currentUser);
				}
				
			} else {
				
				$this->view->assign('object', $edit);				
				$this->view->assign('action', 'edit');
				$this->view->assign('uid', $uid);
				$this->view->assign('pagination', $pagination);
				$this->view->assign('page', $this->page);
				$this->view->assign('noDraftFolders', $noDraftFolders);
				$this->view->assign('drafts', $this->getDrafts());
				$this->view->assign('userFolders', $userFolders);
				$this->view->assign('addressFolders', $addressFolders);
				$this->view->assign('testAddressFolders', $testAddressFolders);
				$this->view->assign('moveToFolders', $moveToFolders);
				$this->view->assign('zipmodes', $zipmodes);
				$this->view->assign('users', $this->affectedUsers);
				$this->view->assign('currentUser', $currentUser);
			}
			
		} else {
			if($pagination['currentPage']>1){
				$this->redirect($redirectorAction,$redirectorController,"MooxMailer",array('@widget_0' => $pagination));
			} else {
				$this->redirect($redirectorAction,$redirectorController);
			}
		}
	}
	
	/**
	 * action delete
	 *	
	 * @param int $uid	
	 * @param array $pagination
	 * @param string $redirectorController
	 * @param string $redirectorAction
	 * @return void
	 */
	public function deleteAction($uid = 0, $pagination = array(), $redirectorController = "Newsletter", $redirectorAction = "index") 
	{		
		if($uid>0){
		
			$object = $this->newsletterRepository->findByUid($uid,FALSE);
			
			$this->newsletterRepository->remove($object);
			
			$this->objectManager->get('TYPO3\CMS\Extbase\Persistence\PersistenceManagerInterface')->persistAll();
			
			$this->addFlashMessage(
				'', 
				'Newsletter wurde gelöscht.', 
				FlashMessage::OK
			);
						
		} 
		
		if($pagination['currentPage']>1){
			$this->redirect("index","Newsletter","MooxMailer",array('@widget_0' => $pagination));
		} else {
			$this->redirect($redirectorAction,$redirectorController);
		}
	}
	
	/**
	 * action show
	 *	
	 * @param int $uid	
	 * @param array $pagination
	 * @param string $redirectorController
	 * @param string $redirectorAction
	 * @return void
	 */
	public function showAction($uid = 0, $pagination = array(), $redirectorController = "Newsletter", $redirectorAction = "index") 
	{		
		if($uid>0){
		
			$object = $this->newsletterRepository->findByUid($uid,FALSE);
			
			$jobPath = $GLOBALS["_SERVER"]["DOCUMENT_ROOT"].self::$mailDir."/jobs/".$object->getJob();
			if(file_exists($jobPath)){
				$state = "In Bearbeitung";
				$completed = false;
			} else {
				$state = "Vollständig versandt";
				$completed = true;
			}
			
			$logPathRunning = $GLOBALS["_SERVER"]["DOCUMENT_ROOT"].self::$mailDir."/logs/".$object->getJob().".log";
			$logPath = $GLOBALS["_SERVER"]["DOCUMENT_ROOT"].self::$mailDir."/logs/__".$object->getJob().".log";
			
			if(file_exists($logPath)){
				$log = file_get_contents($logPath);
			} elseif(file_exists($logPathRunning)){				
				$log = file_get_contents($logPathRunning);
			}
			
			$mailerServiceConfig = 	array(
				"mailDir" 			=> $GLOBALS["_SERVER"]["DOCUMENT_ROOT"].self::$mailDir,
				"mode"				=> self::$mode,
				"format"			=> self::$format,
				"uid" 				=> $object->getUid(),
				"timestamp" 		=> time(),
				"countMode" 		=> self::$countMode,
				"scriptTimeout" 	=> self::$scriptTimeout,
				"memoryLimit" 		=> self::$memoryLimit,
				"noLog" 			=> self::$noLog,
				"senderAddress"		=> $object->getSenderEmail(),
				"senderName"		=> $object->getSenderName(),
				"replyToAddress"	=> $object->getReplyToEmail()
			);
							
			// initiate mailer
			$mailer = $this->objectManager->get('Neulandlotsen\MooxMailer\Service\MailerService');
			$mailer->initialize($mailerServiceConfig,$object->getJob());
			
			if(!$completed){
				$processed = (int)$mailer->getMailsProcessed();
			} else {
				$processed = $object->getSentCount();
			}
			
			$this->view->assign('object', $object);
			$this->view->assign('state', $state);
			$this->view->assign('log', nl2br($log));
			$this->view->assign('processed', $processed);
			$this->view->assign('prepared', (int)$mailer->getMailsPrepared());
			$this->view->assign('completed', $completed);
			$this->view->assign('action', "show");
						
		} else {
			if($pagination['currentPage']>1){
			$this->redirect("index","Newsletter","MooxMailer",array('@widget_0' => $pagination));
			} else {
				$this->redirect($redirectorAction,$redirectorController);
			}
		}		
	}
	
	/**
	 * action stats
	 *	
	 * @param int $uid	
	 * @param array $pagination
	 * @param string $redirectorController
	 * @param string $redirectorAction
	 * @return void
	 */
	public function statsAction($uid = 0, $pagination = array(), $redirectorController = "Newsletter", $redirectorAction = "index")
	{	
		if($uid>0){
		
			$newsletter = $this->newsletterRepository->findByUid($uid,FALSE);
						
			$this->view->assign('object', $newsletter);
			$this->view->assign('clicks', $this->clickRepository->findTopXByJob($newsletter->getJob(),true,10));
			$this->view->assign('action', "stats");
						
		} else {
			if($pagination['currentPage']>1){
			$this->redirect("index","Newsletter","MooxMailer",array('@widget_0' => $pagination));
			} else {
				$this->redirect($redirectorAction,$redirectorController);
			}
		}		
	}
	
	/**
	 * action stats pdf
	 *	
	 * @param int $uid		
	 * @return void
	 */
	public function statsPdfAction($uid = 0)
	{		
		if($uid>0){
			
			ini_set('memory_limit', '2048M');
			
			$newsletter = $this->newsletterRepository->findByUid($uid,FALSE);
			$clicks = $this->clickRepository->findTopXByJob($newsletter->getJob(),10);
			
			$dompdfPath = str_replace("Classes/Controller","Resources/Private/Php/Dompdf",dirname(__FILE__));
			$templatePath = str_replace("Classes/Controller","Resources/Private/Backend/Templates/Newsletter",dirname(__FILE__));
			
			$dompdfTempPath = $_SERVER['DOCUMENT_ROOT']."/typo3temp/mooxmailer";
			
			// include autoloader
			require_once $dompdfPath.'/autoload.inc.php';
			
			if(!is_dir($dompdfTempPath)){
				mkdir($dompdfTempPath);
			}
			
			$headerHtml = '<div style="background-color:#666;color:#FFF;padding:5px">';
			$headerHtml .= '<table cellspacing="0" cellpadding="0" border="0" width="100%"><tr>';
			$headerHtml .= '<td align="left"><strong>'.$newsletter->getSubject().'</strong></td>';
			$headerHtml .= '<td align="right">Stand: '.date("d.m.Y H:i:s").'</td>';
			$headerHtml .= '</tr></table></div>';			
			$infoHtml = '<div style="background-color:#AAA;color:#FFF;padding:5px">Gesendet am: '.date("d.m.Y H:i:s",$newsletter->getSent()).'</div>';
			//$subheaderHtml .= '<div style="margin: 20px;margin-bottom: 0px;background-color:#666;color:#FFF;padding:5px">Mail</div>';
			
			$html = '
				<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
				<html>
					<head>
						<meta http-equiv="content-type" content="text/html; charset=utf-8" />
						<title>Statistik</title>		
					</head>
					<body style="font-family: Arial,helvetica,sans-serif"><!--BODY-->
					</body>
				</html>
			';
			
			//$html = $newsletter->getHtml();
			
			$statsHtml = $this->objectManager->get('TYPO3\\CMS\\Fluid\\View\StandaloneView');			
			$statsHtml->setTemplatePathAndFilename($templatePath."/StatsPdf.html");
			$statsHtml->setFormat('html');			
			$statsHtml->assignMultiple(array("object" => $newsletter, "clicks" => $clicks));
			$statsHtml = $statsHtml->render();
			
			//$html = str_replace("<body>","<body>".$headerHtml.$infoHtml.$subheaderHtml,$html);
			$html = str_replace("<!--BODY-->",$headerHtml.$infoHtml.$subheaderHtml,$html);
			$html = str_replace("</body>",$statsHtml."</body>",$html);
			
			$options = new Options();
			$options->set('tempDir', $dompdfTempPath);
			$options->set('logOutputFile', $dompdfTempPath."/dompdf.log");
			//$options->set('isHtml5ParserEnabled', true);
			//$options->set('debugPng', true);
			//$options->set('debugKeepTemp', true);
			//$options->set('debugCss', true);
			//$options->set('debugLayout', true);			
			
			$dompdf = new Dompdf($options);			
			$dompdf->load_html($html);			
			//$dompdf->setPaper('A4', 'landscape');
			$dompdf->render();
			$dompdf->stream($newsletter->getJob().'_stats_'.date("YmdHis"),array("Attachment" => 1));
			
			exit();	
		} 	
	}
	
	/**
	 * action preview iframe
	 *
	 * @param int $page
	 * @param int $autoLoginUser
	 * @param int $uid
	 * @param string $hash
	 * @return void
	 */
	public function previewIframeAction($page = 0, $autoLoginUser = 0, $uid = 0, $hash = "")
	{							
		if($page>0){				
			$content = $this->getPageContent($page,$autoLoginUser);
			if(in_array($content['http_code'],array(200))){
				
				$baseUrl = $this->getBaseUrl($page);				
				$url = $this->preparePreviewUrl($baseUrl.self::$indexUrl,array("pid" => $page));
				$htmlViewUrl = $this->preparePreviewUrl($baseUrl.self::$indexHtmlViewUrl,array("pid" => $page,"type" => self::$htmlViewType,"uid" => $uid,"hash" => $hash));
				
				$data = array();
				
				if($autoLoginUser>0){
					$autoLoginUser = $this->frontendUserRepository->findByUid($autoLoginUser);
					$data['uid'] = $autoLoginUser->getUid();
					$data['gender']	= $autoLoginUser->getGender();
					$data['title'] = $autoLoginUser->getTitle();
					$data['username'] = $autoLoginUser->getUsername();
					$data['autoLogin']['uid'] = $data['uid'];
					$data['autoLogin']['hash'] = md5($data['username'].$autoLoginUser->getPassword());;
					$data['firstName'] = $autoLoginUser->getFirstName();
					$data['lastName'] = $autoLoginUser->getLastName();
					if($data['firstName']!=""){
						$data['fullName'] = $data['firstName']." ".$data['fullName'];
					}
					$data['email'] = $autoLoginUser->getEmail();
					$url .= "&tx_mooxmailer_pi1[autoLogin][id]=".$data['autoLogin']['uid'];
					$url .= "&tx_mooxmailer_pi1[autoLogin][hash]=".$data['autoLogin']['hash'];
					$data['isAddress'] = false;
				} else {
					$data['gender']	= 1;
					$data['title'] = "Dipl. Ing.";							
					$data['firstName'] = "Hans";
					$data['lastName'] = "Mustermann";
					$data['fullName'] = $data['lastName'];
					if($data['firstName']!=""){
						$data['fullName'] = $data['firstName']." ".$data['fullName'];
					}
					$data['isAddress'] = true;
					$data['email'] = "hans@mustermann.de";
					if($this->settings['addressUnregisterPid']>0){
						$data['unregisterUrl'] = $this->prepareUnregisterUrl($this->getBaseUrl($this->settings['addressUnregisterPid']).self::$indexUrl,array("pid" => $this->settings['addressUnregisterPid']));					
					}
				}
				$data['viewUrl'] = $url;
				$data['htmlViewUrl'] = $htmlViewUrl;
				
				$preview = $this->parsePageContent($content['content'],$data);
				
				$this->view->assign('preview', $preview);
			}
		} 
	}
	
	/**
	 * action show sent newsletter iframe
	 *
	 * @param int $uid	
	 * @return void
	 */
	public function showSentNewsletterIframeAction($uid = 0)
	{							
		if($uid>0){				
			
			$object = $this->newsletterRepository->findByUid($uid,FALSE);
			
			$this->view->assign('newsletter', $object->getHtml());
			
		} 
	}		
	
	/**
	 * action send
	 *	
	 * @param int $uid	
	 * @param string $redirectorController
	 * @param string $redirectorAction
	 * @return void
	 */
	public function sendAction($uid = 0, $redirectorController = "Newsletter", $redirectorAction = "index")
	{		
	
		if($uid>0){
		
			$object = $this->newsletterRepository->findByUid($uid,FALSE);
			
			$isSendable = true;
			
			if($object->getPage()<1){
				$baseUrl = $this->getBaseUrl($object->getPage());
				$isSendable = false;				
			}
			if($object->getSubject()==""){
				$isSendable = false;				
			}
			if($object->getSenderName()==""){
				$isSendable = false;				
			}
			if($object->getSenderEmail()==""){
				$isSendable = false;				
			}			
			if($object->getAddressFolders()=="" && $object->getUserFolders()==""){
				$isSendable = false;				
			}
			
			if($isSendable){
				$html = $this->getPageContent($object->getPage(),0,$baseUrl);				
				if(!in_array($html['http_code'],array(200))){
					$isSendable = false;
					$errorMessage = "Die zu versendenen Seite konnten nicht geladen werden. Versuchen Sie es in einigen Minuten nochmals oder wenden Sie sich an Ihren Administrator";
				}
			} else {
				$errorMessage = "Newsletter unvollständig. Vervollständigen Sie den Newsletter und versuchen Sie ihn dann erneut zu versenden";
			}
			
			if($isSendable){
				
				$mailerServiceConfig = 	array(
					"mailDir" 			=> $GLOBALS["_SERVER"]["DOCUMENT_ROOT"].self::$mailDir,
					"mode"				=> self::$mode,
					"format"			=> self::$format,
					"uid" 				=> $object->getUid(),
					"timestamp" 		=> time(),
					"countMode" 		=> self::$countMode,
					"scriptTimeout" 	=> self::$scriptTimeout,
					"memoryLimit" 		=> self::$memoryLimit,
					"noLog" 			=> self::$noLog,
					"senderAddress"		=> $object->getSenderEmail(),
					"senderName"		=> $object->getSenderName(),
					"replyToAddress"	=> $object->getReplyToEmail()
				);
					
				// initiate daemon
				$daemon = $this->objectManager->get('Neulandlotsen\MooxMailer\Service\DaemonService');
				$daemon->initialize($mailerServiceConfig);						
			
				// initiate mailer
				$mailer = $this->objectManager->get('Neulandlotsen\MooxMailer\Service\MailerService');
				$mailer->initialize($mailerServiceConfig);
				
				$raw = true;
				$baseUrl = $this->getBaseUrl($object->getPage());
				$viewUrl = $this->preparePreviewUrl($baseUrl.self::$indexUrl,array("pid" => $object->getPage()));
				$htmlViewUrl = $this->preparePreviewUrl($baseUrl.self::$indexHtmlViewUrl,array("pid" => $object->getPage(),"type" => self::$htmlViewType,"uid" => $object->getUid(),"hash" => $object->getHash()));				
				if($this->settings['addressUnregisterPid']>0){
					$unregisterUrl 	= $this->prepareUnregisterUrl($this->getBaseUrl($this->settings['addressUnregisterPid']).self::$indexUrl,array("pid" => $this->settings['addressUnregisterPid']));					
				}
				$maildata = array();
				$userHtml = array();				
				$errors = 0;
				$duplicates	= 0;
				
				if($object->getZipmode()=='list' && $object->getZiplist()!=''){
					$zipmode = "list";
					$ziplist = explode(",",$object->getZiplist());
				} elseif($object->getZipmode()!='' && is_numeric($object->getZipmode())){
					$ziplist = [];
					$zipregion = $this->zipregionRepository->findByUid($object->getZipmode());
					if(is_object($zipregion)){
						$ziplist_cleaned = self::cleanUpZiplist($zipregion->getList());						
						if($zipregion->getMode()=='list' && $ziplist_cleaned!=''){
							$ziplist_tmp = explode(",",$ziplist_cleaned);
							foreach($ziplist_tmp AS $zip){
								if(is_numeric(trim($zip))){
									$ziplist[] = trim($zip);
								}
							}
						} elseif($zipregion->getMode()=='area' && (int)$zipregion->getLower()>=0 && (int)$zipregion->getUpper()>0){
							$zipmode = "area";
							$ziplist_lower = (int)$zipregion->getLower();
							$ziplist_upper = (int)$zipregion->getUpper();
						}
					}
					if(count($ziplist)){
						$zipmode = "list";
					}
				}
				
				if($object->getUserFolders()!=""){
					
					$userFolders = unserialize($object->getUserFolders());
					
					if(count($userFolders)){
						
						$users = $this->frontendUserRepository->findByPids($userFolders,$raw);
						$usersCount = 0;						
						
						foreach($users AS $user){							
							
							if(!$zipmode || ($user['zip']!='' && ($zipmode=='area' && $user['zip']>=$ziplist_lower && $user['zip']<=$ziplist_upper) || ($zipmode=='list' && in_array($user['zip'],$ziplist)))){
							
								$data = array();
								
								$data['isAddress'] = false;
								$data['baseUrl'] = $baseUrl;
								$data['viewUrl'] = $viewUrl;
								$data['htmlViewUrl'] = $htmlViewUrl;
								
								if($raw){
																	
									$data['uid'] = $user['uid'];
									$data['crdate']	= $user['crdate'];
									$data['type'] = "feuser";
									$data['gender']	= $user['gender'];
									$data['title'] = $user['title'];
									$data['username'] = $user['username'];					
									$data['firstName'] = $user['first_name'];
									$data['lastName'] = $user['last_name'];
									$data['fullName'] = $data['lastName'];
									if($data['firstName']!=""){
										$data['fullName'] = $data['firstName']." ".$data['fullName'];
									}
									$data['email'] = $user['email'];
									$data['password'] = $user['password'];
									
								} else {
								
									$data['uid'] = $user->getUid();
									$data['crdate']	= $user->getCrdate();
									$data['type'] = "feuser";
									$data['gender'] = $user->getGender();
									$data['title'] = $user->getTitle();
									$data['username'] = $user->getUsername();					
									$data['firstName'] = $user->getFirstName();
									$data['lastName'] = $user->getLastName();
									$data['fullName'] = $data['lastName'];
									if($data['firstName']!=""){
										$data['fullName'] = $data['firstName']." ".$data['fullName'];
									}
									$data['email'] = $user->getEmail();
									$data['password'] = $user->getPassword();
								}
								
								$data['bounceInfo']['receiverType']	= $data['type'];
								$data['bounceInfo']['receiverUid'] = $data['uid'];
								$data['bounceInfo']['task']	= "newsletter";
								
								$currentHtml = "";
								
								if($object->getAutologin()){
									
									$data['autoLogin']['uid'] = $data['uid'];
									$data['autoLogin']['hash'] = md5($data['username'].$data['password']);
									$data['viewUrl'] .= "&tx_mooxmailer_pi1[autoLogin][id]=".$data['autoLogin']['uid'];
									$data['viewUrl'] .= "&tx_mooxmailer_pi1[autoLogin][hash]=".$data['autoLogin']['hash'];
									
									if($raw){
										$userFeGroups = explode(",",$user['usergroup']);
									} else {
										$userFeGroups = array();
										foreach($user->getUsergroup() AS $feGroup){
											$userFeGroups[] = $feGroup->getUid();
										}
									}
									
									if(count($userFeGroups)){
										$identifier = "#".implode("|",$userFeGroups)."#";
									} else {
										$identifier	= "nogroup";
									}							
									
									$requestValid = false;
									
									if(!isset($userHtml[$identifier])){
										
										$userHtml[$identifier] = array();
										$userHtml[$identifier] = $this->getPageContent($object->getPage(),$data['uid'],$baseUrl);
										
										if(in_array($userHtml[$identifier]['http_code'],array(200))){
											$requestValid = true;
										}
										
									} else {
									
										$requestValid = true;
									}
									
									if($requestValid){
										$currentHtml = $this->parsePageContent($userHtml[$identifier]['content'],$data);
									} else {
										$currentHtml = $this->parsePageContent($html['content'],$data);
										$errors++;									
									}	

								} else {
									$currentHtml = $this->parsePageContent($html['content'],$data);
								}
								
								if($currentHtml==""){
									$errors++;
									continue;
								}
								
								if($data['firstName']!=""){
									$data['name'] = $data['firstName'];
								}
								if($data['lastName']!=""){
									if($data['name']!=""){
										$data['name'] .= " ".$data['lastName'];
									} else {
										$data['name'] = $data['lastName'];
									}
								}
								if($data['name']==""){
									$data['name'] = $data['email'];
								}
								
								if(false)
								{
                                    $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable('tx_mooxmailer_domain_model_job');
                                    $queryBuilder
                                        ->getRestrictions()
                                        ->removeAll()
                                        ->add(GeneralUtility::makeInstance(DeletedRestriction::class));

                                    $query = $queryBuilder
                                        ->select('uid')
                                        ->from('tx_mooxmailer_domain_model_job')
                                        ->where(
                                            $queryBuilder->expr()->andx(
                                                $queryBuilder->expr()->eq('job', $queryBuilder->createNamedParameter($mailer->getIdentifier().'_'.$mailer->getDescription(), \PDO::PARAM_STR)),
                                                $queryBuilder->expr()->eq('email', $queryBuilder->createNamedParameter($data['email'], \PDO::PARAM_STR))
                                            )
                                        )
                                        ->setMaxResults(1)
                                        ->execute();
									
									$job = $query->fetch();
									if(!$job['uid'])
									{
										$jobInsert = $GLOBALS['TYPO3_DB']->exec_INSERTQuery(
											'tx_mooxmailer_domain_model_job',
											array(
												"job" => $mailer->getIdentifier().'_'.$mailer->getDescription(),
												"email" => $data['email'],
												"data" => serialize(
													array(
														"to" => array(
															"address" 	=> $data['email'],
															"name" 		=> $data['name'],
															"uid" 		=> $data['uid'],
															"type" 		=> $data['type'],
															"hash" 		=> md5($data['uid'].$data['crdate'].$data['type'].self::$trackingSalt)
														),
														"bounceInfo" 	=> $data['bounceInfo'],
														"subject" 		=> self::replaceMarkers($object->getSubject(),$object),
														"body"			=> $currentHtml,
														"baseUrl"		=> $data['baseUrl'],
														"jobType"		=> self::$taskName,
													)
												),
												"tstamp" => time()
												)
										);	
									}
								} else {
									$maildata[$data['email']] = array(
										"to" => array(
											"address" 	=> $data['email'],
											"name" 		=> $data['name'],
											"uid" 		=> $data['uid'],
											"type" 		=> $data['type'],
											"hash" 		=> md5($data['uid'].$data['crdate'].$data['type'].self::$trackingSalt)
										),
										"bounceInfo" 	=> $data['bounceInfo'],
										"subject" 		=> self::replaceMarkers($object->getSubject(),$object),
										"body"			=> $currentHtml,
										"baseUrl"		=> $data['baseUrl'],
										"jobType"		=> self::$taskName,
									);
								}
								
								$usersCount++;
								//break;
							}
						}
					}
				}
				
				unset($users);
				unset($userHtml);
				unset($currentHtml);
				
				if($object->getAddressFolders()!=""){
					
					$addressFolders = unserialize($object->getAddressFolders());					
					
					if(count($addressFolders)){
						
						$addresses = $this->addressRepository->findByPids($addressFolders,true);
						$addressesCount = 0;
						
						foreach($addresses AS $address)
						{
							if(!$zipmode || ($address['zip']!='' && ($zipmode=='area' && $address['zip']>=$ziplist_lower && $address['zip']<=$ziplist_upper) || ($zipmode=='list' && in_array($address['zip'],$ziplist))))
							{
								$data = array();
								$data['isAddress'] = true;
								$data['baseUrl'] = $baseUrl;
								$data['viewUrl'] = $viewUrl;
								$data['htmlViewUrl'] = $htmlViewUrl;
								
								if($raw)
								{
									$data['uid'] = $address['uid'];
									$data['crdate']	= $address['crdate'];
									$data['type'] = "address";
									$data['gender']	= $address['gender'];
									$data['title'] = $address['title'];											
									$data['firstName'] = $address['forename'];
									$data['lastName'] = $address['surname'];
									$data['fullName'] = $data['lastName'];
									if($data['firstName']!=""){
										$data['fullName'] = $data['firstName']." ".$data['fullName'];
									}
									$data['email'] = $address['email'];
									$data['hash'] = ($address['hash'])?$address['hash']:$address['register_hash'];
								}
								else
								{
									$data['uid'] = $address->getUid();
									$data['crdate'] = $address->getCrdate();
									$data['type'] = "address";
									$data['gender']	= $address->getGender();
									$data['title'] = $address->getTitle();												
									$data['firstName'] = $address->getForename();
									$data['lastName'] = $address->getSurname();
									$data['fullName'] = $data['lastName'];
									if($data['firstName']!=""){
										$data['fullName'] = $data['firstName']." ".$data['fullName'];
									}
									$data['email'] = $address->getEmail();
									$data['hash'] = (method_exists($address,"getHash"))?$address->getHash():$address->getRegisterHash();
								}
								
								$data['bounceInfo']['receiverType']	= $data['type'];
								$data['bounceInfo']['receiverUid'] = $data['uid'];
								$data['bounceInfo']['task'] = "newsletter";

								if($unregisterUrl!="" && $data['email']!="" && $data['hash'])
								{
									$data['unregisterUrl'] = $unregisterUrl;
									$data['unregisterUrl'] .= "&tx_mooxmailer_pi2[email]=".$data['email'];
									$data['unregisterUrl'] .= "&tx_mooxmailer_pi2[hash]=".$data['hash'];
								}
								
								if(!isset($maildata[$data['email']])){
															
									$currentHtml = $this->parsePageContent($html['content'],$data);
									
									if($data['firstName']!="")
									{
										$data['name'] = $data['firstName'];
									}
									if($data['lastName']!="")
									{
										if($data['name']!="")
										{
											$data['name'] .= " ".$data['lastName'];
										}
										else
										{
											$data['name'] = $data['lastName'];
										}
									}
									if($data['name']=="")
									{
										$data['name'] = $data['email'];
									}
									
									if(false)
									{
                                        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable('tx_mooxmailer_domain_model_job');
                                        $queryBuilder
                                            ->getRestrictions()
                                            ->removeAll()
                                            ->add(GeneralUtility::makeInstance(DeletedRestriction::class));

                                        $query = $queryBuilder
                                            ->select('uid')
                                            ->from('tx_mooxmailer_domain_model_job')
                                            ->where(
                                                $queryBuilder->expr()->andx(
                                                    $queryBuilder->expr()->eq('job', $queryBuilder->createNamedParameter($mailer->getIdentifier().'_'.$mailer->getDescription(), \PDO::PARAM_STR)),
                                                    $queryBuilder->expr()->eq('email', $queryBuilder->createNamedParameter($data['email'], \PDO::PARAM_STR))
                                                )
                                            )
                                            ->setMaxResults(1)
                                            ->execute();

										$job = $query->fetch();
										if(!$job['uid'])
										{
											$jobInsert = $GLOBALS['TYPO3_DB']->exec_INSERTQuery(
												'tx_mooxmailer_domain_model_job',
												array(
													"job" => $mailer->getIdentifier().'_'.$mailer->getDescription(),
													"email" => $data['email'],
													"data" => serialize(
														array(
															"to" => array(
																"address" 	=> $data['email'],
																"name" 		=> $data['name'],
																"uid" 		=> $data['uid'],
																"type" 		=> $data['type'],
																"hash" 		=> md5($data['uid'].$data['crdate'].$data['type'].self::$trackingSalt)
															),
															"bounceInfo" 	=> $data['bounceInfo'],
															"subject" 		=> self::replaceMarkers($object->getSubject(),$object),
															"body"			=> $currentHtml,
															"baseUrl"		=> $data['baseUrl'],
															"jobType"		=> self::$taskName,
														)
													),
													"tstamp" => time()
												)
											);	
										}																	
									}
									else
									{
										$maildata[] = array(
											"to" => array(
												"address" 	=> $data['email'],
												"name" 		=> $data['name'],
												"uid" 		=> $data['uid'],
												"type" 		=> $data['type'],
												"hash" 		=> md5($data['uid'].$data['crdate'].$data['type'].self::$trackingSalt)
											),
											"bounceInfo" 	=> $data['bounceInfo'],
											"subject" 		=> self::replaceMarkers($object->getSubject(),$object),
											"body"			=> $currentHtml,
											"baseUrl"		=> $data['baseUrl'],
											"jobType"		=> self::$taskName,
										);
									}
									
									$addressesCount++;
								}
								else
								{
									$duplicates++;
								}
							}
						}
					}
				}
				
				unset($addresses);
				unset($currentHtml);
				
				$overallCount = $addressesCount+$usersCount;
				
				if($overallCount>0){										
					
					// if mailer ready, set mailer lock
					if($mailer->getMode()=="daemon" && $daemon->mailerReady()){
						$daemon->lockMailer("[".date("Y-m-d H:i:s")."] locked by ".$mailer->getDescription());
						$lock = true;				
					}
					
					if($lock){
						
						$mailer->setStoragePage($this->getStoragePage());
						
						if(false){
							$process = $mailer->process(array("loadFromDB" => true));
						} else {
							$process = $mailer->process($maildata);
						}
						
						// remove mailer lock
						$daemon->removeLock();
						
						$object->setHtml($this->parsePageContent($html['content'],array("settings" => array("isArchiv" => 1))));
						$object->setSent(time());
						$object->setSentSubject(self::replaceMarkers($object->getSubject(),$object));						
						$object->setSentCount($overallCount);
						$object->setJob($mailer->getIdentifier()."_".$mailer->getDescription());						
												
						$this->newsletterRepository->update($object);
						$this->objectManager->get('TYPO3\CMS\Extbase\Persistence\PersistenceManagerInterface')->persistAll();			
						
						$filter = unserialize($this->backendSession->get("filter"));			
						$filter['state'] = 1;
						$filter['sortField'] = "sent";				
						$filter['sortDirection'] = "DESC";
						$this->backendSession->store("filter",serialize($filter));	
						
						if($object->getMoveToFolder()>0){
							$this->moveDraft($object->getPage(),$object->getMoveToFolder());
						}
						
						$successTxt = 'Newsletter wurde an '.$usersCount.' Benutzer und '.$addressesCount.' Adresse(n) versendet.';
						
						$this->addFlashMessage(
							'', 
							$successTxt, 
							FlashMessage::OK
						);
						
						if($duplicates>0){
							$this->addFlashMessage(
								'', 
								$duplicates.' Email-Adresse(n) wurden doppelt innerhalb der gewählten Empfänger gefunden und daher nicht mehrfach versendet', 
								FlashMessage::WARNING
							);
						}
						
					} else {
				
						$lockInfo = $daemon->lockInfo();
						$lockInfoParts = explode("]",$lockInfo);
						$lockInfoText = trim($lockInfoParts[1]);
						$lockInfoDate = trim($lockInfoParts[0])."]";
						
						// log to console
						$text = "[".date("Y-m-d H:i:s")."] TASK [".(($this->description)?$this->description:$this->taskUid)."] says: mailer ".$lockInfoText." ".$lockInfoDate."...\n";
						
						$this->addFlashMessage(
							$text, 
							'Der Newsletter kann derzeit nicht versendet werden, da der Mail-Service beschäftigt ist. Bitte versuchen Sie es in einigen Minuten erneut.', 
							FlashMessage::WARNING
						);						
					}	

					$mailer = NULL;
					$daemon = NULL;
					
				} else {
					$this->addFlashMessage(
						'', 
						'Keine Empfänger für den Versand gefunden.', 
						FlashMessage::WARNING
					);
				}												
				
				$this->redirect($redirectorAction,$redirectorController);
				
			} else {
				$this->addFlashMessage(
					'', 
					$errorMessage, 
					FlashMessage::ERROR
				);
					
				$this->redirect("edit","Newsletter",NULL,array('uid' => $object->getUid()));
			}
						
		} else {
			if($pagination['currentPage']>1){
				$this->redirect($redirectorAction,$redirectorController,"MooxMailer",array('@widget_0' => $pagination));
			} else {
				$this->redirect($redirectorAction,$redirectorController);
			}
		}
	}
	
	/**
	 * action test send
	 *	
	 * @param int $uid	
	 * @param string $redirectorController
	 * @param string $redirectorAction
	 * @return void
	 */
	public function testSendAction($uid = 0, $redirectorController = "Newsletter", $redirectorAction = "index")
	{					
		if($uid>0){
		
			$object = $this->newsletterRepository->findByUid($uid,FALSE);
			
			$isSendable = true;
			
			if($object->getPage()<1){
				$baseUrl = $this->getBaseUrl($object->getPage());
				$isSendable = false;
			}
			if($object->getSubject()==""){
				$isSendable = false;
			}
			if($object->getSenderName()==""){
				$isSendable = false;
			}
			if($object->getSenderEmail()==""){
				$isSendable = false;
			}			
			if($object->getTestAddressFolders()==""){
				$isSendable = false;
			}
			
			if($isSendable){
				$html = $this->getPageContent($object->getPage(),0,$baseUrl);
				if(!in_array($html['http_code'],array(200))){										
					$isSendable = false;					
					$errorMessage = "Die zu versendenen Seite konnten nicht geladen werden. Versuchen Sie es in einigen Minuten nochmals oder wenden Sie sich an Ihren Administrator";
				}
			} else {
				$errorMessage = "Newsletter unvollständig. Vervollständigen Sie den Newsletter und versuchen Sie ihn dann erneut zu versenden";
			}
			
			if($isSendable){
				
				$raw = true;
				$baseUrl = $this->getBaseUrl($object->getPage());
				$viewUrl = $this->preparePreviewUrl($baseUrl.self::$indexUrl,array("pid" => $object->getPage()));
				$htmlViewUrl = $this->preparePreviewUrl($baseUrl.self::$indexHtmlViewUrl,array("pid" => $object->getPage(),"type" => self::$htmlViewType,"uid" => $object->getUid(),"hash" => $object->getHash()));
				if($this->settings['addressUnregisterPid']>0){
					$unregisterUrl 	= $this->prepareUnregisterUrl($this->getBaseUrl($this->settings['addressUnregisterPid']).self::$indexUrl,array("pid" => $this->settings['addressUnregisterPid']));					
				}				
				$maildata = array();
				$userHtml = array();				
				$errors = 0;
				$duplicates	= 0;				
				
				if($object->getTestAddressFolders()!=""){
					
					$testAddressFolders = unserialize($object->getTestAddressFolders());					
					
					if(count($testAddressFolders)){
						
						$addresses = $this->addressRepository->findByPids($testAddressFolders,true);
						$addressesCount = 0;
						
						foreach($addresses AS $address){														
							
							$data = array();
							$data['isAddress'] = true;
							$data['baseUrl'] = $baseUrl;
							$data['viewUrl'] = $viewUrl;
							$data['htmlViewUrl'] = $htmlViewUrl;
							
							if($raw){
								
								$data['uid'] = $address['uid'];
								$data['crdate']	= $address['crdate'];
								$data['type'] = "address";
								$data['gender'] = $address['gender'];
								$data['title'] = $address['title'];											
								$data['firstName'] = $address['forename'];
								$data['lastName'] = $address['surname'];
								$data['fullName'] = $data['lastName'];
								if($data['firstName']!=""){
									$data['fullName'] = $data['firstName']." ".$data['fullName'];
								}
								$data['email'] = $address['email'];
								$data['hash'] = ($address['hash'])?$address['hash']:$address['register_hash'];
								
							} else {
							
								$data['uid'] = $address->getUid();
								$data['crdate']	= $address->getCrdate();
								$data['type'] = "address";
								$data['gender'] = $address->getGender();
								$data['title'] = $address->getTitle();												
								$data['firstName'] = $address->getForename();
								$data['lastName'] = $address->getSurname();
								$data['fullName'] = $data['lastName'];
								if($data['firstName']!=""){
									$data['fullName'] = $data['firstName']." ".$data['fullName'];
								}
								$data['email'] = $address->getEmail();
								$data['hash'] = (method_exists($address,"getHash"))?$address->getHash():$address->getRegisterHash();
							}
							
							$data['bounceInfo']['receiverType']	= $data['type'];
							$data['bounceInfo']['receiverUid'] = $data['uid'];
							$data['bounceInfo']['task']	= "newsletter";
							
							if($unregisterUrl!="" && $data['email']!="" && $data['hash']){
								$data['unregisterUrl'] = $unregisterUrl;
								$data['unregisterUrl'] .= "&tx_mooxmailer_pi2[email]=".$data['email'];
								$data['unregisterUrl'] .= "&tx_mooxmailer_pi2[hash]=".$data['hash'];
							}							
							
							if(!isset($maildata[$data['email']])){
														
								$currentHtml = $this->parsePageContent($html['content'],$data);
								
								if($data['firstName']!=""){
									$data['name'] = $data['firstName'];
								}
								if($data['lastName']!=""){
									if($data['name']!=""){
										$data['name'] .= " ".$data['lastName'];
									} else {
										$data['name'] = $data['lastName'];
									}
								}
								if($data['name']==""){
									$data['name'] = $data['email'];
								}
								
								$maildata[] = array(
									"to" => array(
										"address" 	=> $data['email'],
										"name" 		=> $data['name'],
										"uid" 		=> $data['uid'],
										"type" 		=> $data['type'],
										"hash" 		=> md5($data['uid'].$data['crdate'].$data['type'].self::$trackingSalt)
									),
									/*"bounceInfo" 	=> $data['bounceInfo'],*/
									"subject" 		=> self::replaceMarkers(self::$subjectPrefix.$object->getSubject(),$object),
									"body"			=> $currentHtml,
									"baseUrl"		=> $data['baseUrl'],
									"isTest"		=> true,
								);
								
								$addressesCount++;
							} else {
								$duplicates++;
							}							
						}
					}
				}
				
				unset($addresses);
				unset($currentHtml);
				
				$overallCount = $addressesCount;
				
				if($overallCount>0){
					
					$mailerServiceConfig = 	array(
						"mailDir" 			=> $GLOBALS["_SERVER"]["DOCUMENT_ROOT"].self::$mailDir,
						"mode"				=> self::$mode,
						"format"			=> self::$format,
						"uid" 				=> $object->getUid(),
						"timestamp" 		=> time(),
						"countMode" 		=> self::$countMode,
						"scriptTimeout" 	=> self::$scriptTimeout,
						"memoryLimit" 		=> self::$memoryLimit,
						"noLog" 			=> self::$noLog,
						"senderAddress"		=> $object->getSenderEmail(),
						"senderName"		=> $object->getSenderName(),
						"replyToAddress"	=> $object->getReplyToEmail()
					);
					
					// initiate daemon
					$daemon = $this->objectManager->get('Neulandlotsen\MooxMailer\Service\DaemonService');
					$daemon->initialize($mailerServiceConfig);						
			
					// initiate mailer
					$mailer = $this->objectManager->get('Neulandlotsen\MooxMailer\Service\MailerService');
					$mailer->initialize($mailerServiceConfig);
					$mailer->setIsTest(true);
					
					// if mailer ready, set mailer lock
					if($mailer->getMode()=="daemon" && $daemon->mailerReady()){
						$daemon->lockMailer("[".date("Y-m-d H:i:s")."] locked by ".$mailer->getDescription());
						$lock = true;				
					}
					
					if($lock){
						
						$process = $mailer->process($maildata);
						
						// remove mailer lock
						$daemon->removeLock();												
						
						$successTxt = self::$subjectPrefix.'Newsletter wurde an '.$addressesCount.' Adresse(n) versendet.';
						
						$this->addFlashMessage(
							'', 
							$successTxt, 
							FlashMessage::OK
						);
						
						if($duplicates>0){
							$this->addFlashMessage(
								'', 
								$duplicates.' Email-Adresse(n) wurden doppelt innerhalb der gewählten Empfänger gefunden und daher nicht mehrfach versendet', 
								FlashMessage::WARNING
							);
						}
						
					} else {
				
						$lockInfo = $daemon->lockInfo();
						$lockInfoParts = explode("]",$lockInfo);
						$lockInfoText = trim($lockInfoParts[1]);
						$lockInfoDate = trim($lockInfoParts[0])."]";
						
						// log to console
						$text = "[".date("Y-m-d H:i:s")."] TASK [".(($this->description)?$this->description:$this->taskUid)."] says: mailer ".$lockInfoText." ".$lockInfoDate."...\n";
						
						$this->addFlashMessage(
							$text, 
							'Der Newsletter kann derzeit nicht versendet werden, da der Mail-Service beschäftigt ist. Bitte versuchen Sie es in einigen Minuten erneut.', 
							FlashMessage::WARNING
						);						
					}	

					$mailer = NULL;
					$daemon = NULL;
					
				} else {
					$this->addFlashMessage(
						'', 
						'Keine Empfänger für den Test-Versand gefunden.', 
						FlashMessage::WARNING
					);
				}												
				
				$this->redirect($redirectorAction,$redirectorController);
				
			} else {
				$this->addFlashMessage(
					'', 
					$errorMessage, 
					FlashMessage::ERROR
				);
					
				$this->redirect("edit","Newsletter",NULL,array('uid' => $object->getUid()));
			}
						
		} else {
			if($pagination['currentPage']>1){
				$this->redirect($redirectorAction,$redirectorController,"MooxMailer",array('@widget_0' => $pagination));
			} else {
				$this->redirect($redirectorAction,$redirectorController);
			}
		}
	}
	
	/**
	 * action add folder select
	 *	
	 * @param int $uid	
	 * @param string $redirectorController
	 * @param string $redirectorAction
	 * @return void
	 */
	public function addFolderSelectAction($uid = 0, $redirectorController = "Newsletter", $redirectorAction = "index")
	{		
		$reset 	= (int)GeneralUtility::_GET('reset');
		
		$folders = $this->getFolders();
		
		$this->redirectToFolder($folders);
		
		$this->addFlashMessage(
			'',
			'Wählen Sie den Ordner in dem Sie den neuen Entwurf anlegen wollen',
			FlashMessage::INFO
		);
		
		$mailerFolders = $this->getFolders("mxmailer");
		if(!count($mailerFolders)){
			$this->addFlashMessage(
				'Es empfiehlt sich einen gesonderten Mailer-Ordner über den Seitenbaum anzulegen. Mailer-Ordner dienen zur gruppierten Ablage von Newsletter, Vorlagen und Zeitpl�nen. Außerdem k�nnen Sie innerhalb eines Mailer-Ordners Entwurfs- und Archiv-Ordner anlegen. Sie k�nnen verschiedene Voreinstellungen pro Mailer-Ordner festlegen und somit �ber das Anlegen verschiedener Mailer-Ordner, verschiedene Varianten von Newslettern verwalten.',
				'Keine Mailer-Ordner gefunden', 					 
				FlashMessage::WARNING
			);					
		}

		$rootline = GeneralUtility::makeInstance(RootlineUtility::class, $this->page, '', $this->pageRepository)->get();
		
		foreach($rootline AS $rootlinepage){
			if($rootlinepage['is_siteroot']){
				$rootpage = $rootlinepage;
				break;
			}
		}
		
		if(!$rootpage){
			$rootpage = $rootline[0];
		}
		
		$rootfound = false;
		for($i=0;$i<count($rootline);$i++){
			if($rootfound){
				unset($rootline[$i]);
			} else {
				if($rootline[$i]['is_siteroot']){
					$rootfound = true;
				}
			}
		}
		
		$rootline = array_reverse($rootline);
		
		if(isset($rootline[count($rootline)-2])){			
			$pageInfo = $this->pageRepository->getPage((int)$rootline[count($rootline)-2]['uid']);		
			if(in_array($pageInfo['module'],array("mxmailer"))){
				$folder = $pageInfo['uid'];				
			}
			
		}
		
		$folders = $this->getFolders();				
		
		$this->view->assign('folders', (count($folders)>0)?$folders:false);
		$this->view->assign('uid', $uid);
		$this->view->assign('page', $this->page);
		$this->view->assign('folder', $folder);
		$this->view->assign('tasks', NULL);
		$this->view->assign('rootpage', $rootpage);
		$this->view->assign('rootline', $rootline);
		$this->view->assign('noMailerFolders', $noMailerFolders);
		$this->view->assign('action', "folderSelect");
		$this->view->assign('redirectorController', $redirectorController);
		$this->view->assign('redirectorAction', $redirectorAction);
	}
	
	/**
	 * get ts settings
	 *
	 * @param int $pageUid page uid
	 * @return array $TSObj->setup
	 */
	public function getTsSetup($pageUid)
	{
		$rootLine = GeneralUtility::makeInstance(RootlineUtility::class, $pageUid, '', $this->pageRepository)->get();
		$TSObj = GeneralUtility::makeInstance(ExtendedTemplateService::class);             
        $TSObj->tt_track = 0;
        $TSObj->init();
        $TSObj->runThroughTemplates($rootLine);
        $TSObj->generateConfig();		
		
		return $TSObj->setup;
	}
	
	/**
	 * get base url
	 *
	 * @param int $pageUid page uid
	 * @return string $baseURL
	 */
	public function getBaseUrl($pageUid)
	{
		$TS = self::getTsSetup($pageUid);
        
		if($_SERVER['HTTPS']=="on"){
			$prefix = "https";
		} else {
			$prefix = "http";
		}
		$baseUrl = $TS['config.']['baseURL'];
		if($baseUrl==$prefix."://")
		{ 
			$baseUrl = $prefix."://".$_SERVER['SERVER_NAME']."/";
		}
		elseif($baseUrl=="//")
		{
			$baseUrl = $prefix."://".$_SERVER['SERVER_NAME']."/";
		}
		elseif($baseUrl=="/")
		{ 
			$baseUrl = $prefix."://".$_SERVER['SERVER_NAME']."/";
		} 
		else
		{
			if(substr(strtolower($baseUrl),0,2)=="//"){				
				$baseUrl = $prefix.":".$baseURL;
			}
		} 
		
		if($baseUrl=="")
		{
			$baseUrl = $prefix."://".$_SERVER['SERVER_NAME']."/";
		}		
		
		if(substr($baseUrl,(strlen($baseUrl)-1))!="/"){ 
			$baseUrl = $baseUrl."/";
		}
		
		return $baseUrl;
	}
	
	/**
	 * get contents from url
	 * @param string $url
	 * @return array $header
	 */
	public function getContentsFromUrl($url)
	{		
		$url = $url."&tx_mooxmailer_pi1[key]=f907a83d1cce65747f92162859ce016b";
		
		$options =[
			CURLOPT_RETURNTRANSFER => true,     // return web page
			CURLOPT_HEADER         => false,    // don't return headers
			CURLOPT_FOLLOWLOCATION => true,     // follow redirects
			CURLOPT_ENCODING       => "",       // handle all encodings
			CURLOPT_USERAGENT      => "typo3_moox_mailer", // who am i
			CURLOPT_AUTOREFERER    => true,     // set referer on redirect
			CURLOPT_CONNECTTIMEOUT => 120,      // timeout on connect
			CURLOPT_TIMEOUT        => 120,      // timeout on response
			CURLOPT_MAXREDIRS      => 10,       // stop after 10 redirects
			CURLOPT_COOKIESESSION  => true,
			CURLOPT_COOKIEJAR      => COOKIE_FILE,
			CURLOPT_COOKIEFILE     => COOKIE_FILE
		];
		if($this->extConf['curlCredentials']!="")
		{
			$options[CURLOPT_USERPWD] = $this->extConf['curlCredentials'];
		}
		
		$ch = curl_init( $url );
		@curl_setopt_array( $ch, $options );
		$content = curl_exec( $ch );
		$err = curl_errno( $ch );
		$errmsg = curl_error( $ch );
		$header = curl_getinfo( $ch );
		curl_close( $ch );

		$header['errno'] = $err;
		$header['errmsg'] = $errmsg;
		$header['content'] = trim($content);
		
		if($header['http_code']=="303"){
			if(strpos($header['content'],'meta http-equiv="refresh"')){							
				$html = json_decode(json_encode(simplexml_load_string($header['content'])),TRUE);
				foreach($html['head']['meta'] AS $meta){
					if($meta['http-equiv']=="refresh"){
						$redirect = substr($meta['content'],((int)strpos($meta['content'],"url=")+4));
						break;
					}
				}
			}
			if($redirect!=""){
				$header = $this->getContentsFromUrl($redirect);
			}
		}
		
		return $header;
	}
	
	/**
	 * get page content
	 *
	 * @param int $uid
	 * @param int $autoLoginUser
	 * @param string $baseUrl
	 * @return arrayobject $newsletter
	 */
	public function getPageContent($uid,$autoLoginUser = 0, $baseUrl = "")
	{		
		if($baseUrl!=""){
			$url = $this->preparePreviewUrl($baseUrl.self::$indexUrl,array("pid" => $uid));								
		} else {
			$url = $this->preparePreviewUrl($this->getBaseUrl($uid).self::$indexUrl,array("pid" => $uid));			
		}
		
		if($autoLoginUser>0){
			$autoLoginUser = $this->frontendUserRepository->findByUid($autoLoginUser);
			$autoLoginUserUid = $autoLoginUser->getUid();
			$autoLoginUserHash = md5($autoLoginUser->getUsername().$autoLoginUser->getPassword());
			$url .= "&tx_mooxmailer_pi1[autoLogin][id]=".$autoLoginUserUid;
			$url .= "&tx_mooxmailer_pi1[autoLogin][hash]=".$autoLoginUserHash;
		}
		
		$return = $this->getContentsFromUrl($url);
		
		$return['content'] = preg_replace('/<script\b[^>]*>(.*?)<\/script>/is', "", $return['content']);
		
		$endOfHtml = strpos($return['content'],"</html>");
		
		if($endOfHtml>0){
			$return['content'] = substr($return['content'],0,($endOfHtml+7));
		}
		
		$return['content'] = trim($return['content']);
				
		return $return;
		
	}
	
	/**
	 * parse page content
	 *
	 * @param string $content
	 * @param array $data
	 * @return string $content
	 */
	public function parsePageContent($content,$data = array()) 
	{		
		$content = str_replace("[#f:","<f:",$content);
		$content = str_replace("[#/f:","</f:",$content);
		$content = str_replace("[#v:","<v:",$content);
		$content = str_replace("[#/v:","</v:",$content);
		$content = str_replace("#/]",">",$content);
		$content = str_replace("#//]","/>",$content);
		$content = str_replace("[#","{",$content);
		$content = str_replace("#]","}",$content);
		$content = str_replace("&quot;",'"',$content);
		
		$data['timestamp'] = time();
		
		$newsletterView = $this->objectManager->get('TYPO3\\CMS\\Fluid\\View\StandaloneView');
        $newsletterView->setFormat('html');
        $newsletterView->setTemplateSource($content);
        $newsletterView->assignMultiple($data);
        $content = $newsletterView->render();
		
		return $content;
	}
	
	/**
	 * get newsletter
	 *
	 * @param int $uid
	 * @return array|object $newsletter
	 */
	public function getNewsletter($uid) 
	{	
		$newsletter = array();
		
		if($uid){				
			$newsletter	= $this->newsletterRepository->findByUid($uid,FALSE);
		}
		
		return $newsletter;
	}

	/**
	 * Get array of folders with newsletter module	
	 *
	 * @param string $module
	 * @return	array	folders with newsletter module	
	 */
	public function getFolders($module = "mxmailer") 
	{		
		global $BE_USER;
		
		$folders = array();

        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable('pages');
        $queryBuilder
            ->getRestrictions()
            ->removeAll()
            ->add(GeneralUtility::makeInstance(DeletedRestriction::class))
            ->add(GeneralUtility::makeInstance(HiddenRestriction::class));

        $query = $queryBuilder
            ->select('*')
            ->from('pages')
            ->where(
                $queryBuilder->expr()->in('doktype', $queryBuilder->createNamedParameter(1, \PDO::PARAM_INT), $queryBuilder->createNamedParameter(254, \PDO::PARAM_INT)),
                $queryBuilder->expr()->eq('module', $queryBuilder->createNamedParameter($module, \PDO::PARAM_STR))
            )
            ->execute();
		
		$folderCnt = 0;
		while($row = $query->fetch())
        {
			$folders[$folderCnt] = $row;
			$rootline = GeneralUtility::makeInstance(RootlineUtility::class, $row['uid'], '', $this->pageRepository)->get();
			
			foreach($rootline AS $rootlinepage){
				if($rootlinepage['is_siteroot']){
					$folders[$folderCnt]['rootpage'] = $rootlinepage;
					break;
				}
			}
			
			if(!$folders[$folderCnt]['rootpage']){
				$folders[$folderCnt]['rootpage'] = $rootline[0];
			}
					
			$rootline = array_reverse($rootline);
			
			if(isset($rootline[count($rootline)-2])){			
				$pageInfo = $this->pageRepository->getPage((int)$rootline[count($rootline)-2]['uid']);		
				if($pageInfo['module']==$module){
					$folders[$folderCnt]['folder'] = $pageInfo['uid'];				
				}
				
			}
			
			$folders[$folderCnt]['rootline'] = $rootline;
			$folderCnt++;
		}
				
		usort($folders, array("\Neulandlotsen\MooxMailer\Controller\NewsletterController", "sortByFolderAndTitle"));
		
		$folders = array_reverse($folders);
		
		return $folders;		
	}
	
	/**
	 * Get first folder with newsletter module	
	 *
	 * @param string $module
	 * @return	array	folder 	
	 */
	public function getFirstFolder($module = "mxmailer")
	{		
		global $BE_USER;

        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable('pages');
        $queryBuilder
            ->getRestrictions()
            ->removeAll()
            ->add(GeneralUtility::makeInstance(DeletedRestriction::class));

        $query = $queryBuilder
            ->select('*')
            ->from('pages')
            ->where(
                $queryBuilder->expr()->in('doktype', $queryBuilder->createNamedParameter(1, \PDO::PARAM_INT), $queryBuilder->createNamedParameter(254, \PDO::PARAM_INT)),
                $queryBuilder->expr()->eq('module', $queryBuilder->createNamedParameter($module, \PDO::PARAM_STR))
            )
            ->setMaxResults(1)
            ->execute();

		return $query->fetch();
	}	
		
	/**
	 * Get newsletter drafts	
	 *	
	 * @return	array	drafts 	
	 */
	public function getDrafts()
	{		
		$draftFolders = $this->getFolders("mxdrafts");
		
		$drafts = array();
		foreach($draftFolders AS $draftFolder){
			foreach($this->pageRepository->getMenu($draftFolder['uid'], "uid,title","title","AND hidden=0") as $page)
			{
				$drafts[$draftFolder['uid']]['items'][] = array("uid" => $page['uid'], "title" => $page['title']);
			}
			if(isset($drafts[$draftFolder['uid']])){
				$drafts[$draftFolder['uid']]['title'] = $draftFolder['title'];
			}
		}
		
		return $drafts;		
	}
	
	/**
	 * get affected fe users
	 *
	 * @param array $pids pids
	 * @return array $feUsers
	 */
	public function getFeUsers($pids = array())
	{        
		if(!is_array($pids)){
			$pids = array($pids);
		}
		
		$feUsers = array();
		
		if(!$this->frontendUserRepository){
			$this->frontendUserRepository = $this->objectManager->get('Neulandlotsen\\MooxMailer\\Domain\\Repository\\FrontendUserRepository');
		}
		
		foreach($this->frontendUserRepository->findByPids($pids) AS $feUser){
			$feUsers[$feUser->getEmail()]['uid'] = $feUser->getUid();
			$feUsers[$feUser->getEmail()]['username'] = $feUser->getUsername();
			$feUsers[$feUser->getEmail()]['firstName'] = ucfirst($feUser->getFirstName());
			$feUsers[$feUser->getEmail()]['lastName'] = ucfirst($feUser->getLastName());
			$feUsers[$feUser->getEmail()]['email'] = $feUser->getEmail();
			$feUsers[$feUser->getEmail()]['type'] = "feuser";
		}
		
		return $feUsers;
	}
	
	/**
	 * move draft to given pid
	 *
	 * @param int $uid uid
	 * @param int $pid pid
	 * @return array $feUsers
	 */
	public function moveDraft($uid = 0,$pid = 0)
	{        
		if($uid>0 && $pid>0){
			$res = $GLOBALS['TYPO3_DB']->exec_UPDATEquery('pages','uid='.$uid,array('tstamp' => time(),'pid' => $pid));			
		}
	}

	/**
	 * action toggle newsletter state
	 *
	 * @param int $uid
	 * @param array $pagination
	 * @param string $redirectorController
	 * @param string $redirectorAction
	 * @return void
	 */
	public function toggleNewsletterStateAction($uid = 0, $pagination = array(), $redirectorController = "Newsletter", $redirectorAction = "index")
	{			
		if($uid>0){						
			
			$object = $this->newsletterRepository->findByUid($uid,FALSE);
			
			if($object->getHidden()==1){
				$object->setHidden(0);
				$action = "aktiviert";
			} else {
				$object->setHidden(1);
				$action = "deaktiviert";
			}			
			
			$this->newsletterRepository->update($object);								
			$this->objectManager->get('TYPO3\CMS\Extbase\Persistence\PersistenceManagerInterface')->persistAll();
			
			$this->addFlashMessage(
				'', 
				'Newsletter wurde erfolgreich '.$action, 
				FlashMessage::OK
			);						
		} 
		
		if($pagination['currentPage']>1){
			$this->redirect($redirectorAction,$redirectorController,"MooxMailer",array('@widget_0' => $pagination));
		} else {
			$this->redirect($redirectorAction,$redirectorController);
		}
		
	}
	
	/**
	 * prepare preview url
	 *
	 * @param string $url url
	 * @param array $params params
	 * @return string $url
	 */
	public function preparePreviewUrl($url,$params)
	{
		if($params['type']!=""){
			$url = str_replace("##type##",$params['type'],$url);
		} else {
			$url = str_replace("##type##",self::$redirectorType,$url);
		}
		$url = str_replace("##id##",$params['pid'],$url);
		$url = str_replace("##uid##",$params['uid'],$url);
		$url = str_replace("##hash##",$params['hash'],$url);
		return $url;
	}
	
	/**
	 * prepare unregister url
	 *
	 * @param string $url url
	 * @param array $params params
	 * @return string $url
	 */
	public function prepareUnregisterUrl($url,$params)
	{ 
		$url = str_replace("_pi1","_pi2",$url);
		$url = str_replace("##type##",self::$unregisterType,$url);
		$url = str_replace("##id##",$params['pid'],$url);
		return $url;
	}

	/**
	 * set page or redirect to default/last folder
	 *
	 * @param array $folders
	 * @return void
	 */
	public function redirectToFolder($folders = [])
	{		
		$id = GeneralUtility::_GET('id');
		$id	= ($id!="")?(int)$id:NULL;
		
		if(is_null($this->backendSession->get("id")) && (int)$this->settings['newsletterStartPid']>0)
		{
			$this->setPage((int)$this->settings['newsletterStartPid']);
			$this->backendSession->store("id",$this->page);
			$this->initializeStorageSettings();	
			HttpUtility::redirect($_SERVER['REQUEST_URI']."&id=".(int)$this->settings['newsletterStartPid']);
		} 
		elseif(is_null($this->backendSession->get("id")) && (int)$this->settings['newsletterStartPid']<1) 
		{			
			$this->setPage(0);
			$this->backendSession->store("id",$this->page);
			$this->initializeStorageSettings();
		} 
		elseif(!is_null($id))
		{
			$this->setPage($id);
			$this->backendSession->store("id",$this->page);
			$this->initializeStorageSettings();
		} 
		else 
		{
			$this->setPage($this->backendSession->get("id"));
			$this->backendSession->store("id",$this->page);
			$this->initializeStorageSettings();			
		}
				
		if($this->page>0)
		{
			$isMailerFolder = false;
			foreach($folders AS $mailerFolder)
			{
				if($mailerFolder['uid']==$this->page)
				{
					$isMailerFolder = true;
					break;
				}
			}
			if(!$isMailerFolder)
			{
				$this->backendSession->delete("id");
				HttpUtility::redirect($_SERVER['REQUEST_URI']);
			} 
		}
	}
	
	/**
	 * process filter
	 *
	 * @param array $filter
	 * @return array $filter
	 */
	public function processFilter($filter = array())
	{		
		if($this->backendSession->get("filter")){
			$filter_tmp = unserialize($this->backendSession->get("filter"));			
			if(isset($filter['state'])){
				$filter_tmp['state'] = $filter['state'];
			} 
			if(isset($filter['perPage'])){
				$filter_tmp['perPage'] = $filter['perPage'];
			}
			if(isset($filter['sortField'])){
				$filter_tmp['sortField'] = $filter['sortField'];
			}
			if($filter_tmp['state']==0 && in_array($filter_tmp['sortField'],array('sent'))){
				$filter_tmp['sortField'] = "title";
			} elseif($filter_tmp['state']==1 && in_array($filter_tmp['sortField'],array('subject'))){
				$filter_tmp['sortField'] = "title";
			}
			if($filter_tmp['sortField']==""){
				$filter_tmp['sortField'] = "title";
			}
			if(isset($filter['sortDirection'])){
				$filter_tmp['sortDirection'] = $filter['sortDirection'];
			}
			if(isset($filter['query'])){
				$filter_tmp['query'] = $filter['query'];
			}
			$filter = $filter_tmp;	
		} else {									
			$filter['state'] 	= 0;
			if($this->settings['itemsPerPage']>0){
				$filter['perPage'] 	= $this->settings['itemsPerPage'];
			} else {
				$filter['perPage'] 	= 25;
			}
			if($this->settings['itemsPerPage']>0){
				$filter['perPage'] 	= $this->settings['itemsPerPage'];
			} else {
				$filter['perPage'] 	= 25;
			}			
			if(in_array($this->settings['defaultSortField'],array("title"))){
				$filter['sortField'] = $this->settings['defaultSortField'];
			} else {
				$filter['sortField'] = "title";
			}							
			if(in_array($this->settings['defaultSortDirection'],array("ASC","DESC"))){
				$filter['sortDirection'] = $this->settings['defaultSortDirection'];
			} else {
				$filter['sortDirection'] = "ASC";
			}						
		}
		if($filter['perPage']<1){
			$filter['perPage'] = 25;
		}
		$this->backendSession->store("filter",serialize($filter));
		return $filter;
	}	
	
	/**
	 * Returns page
	 *
	 * @return int
	 */
	public function getPage()
	{
		return $this->page;
	}

	/**
	 * Set page
	 *
	 * @param int $page page
	 * @return void
	 */
	public function setPage($page)
	{
		$this->page = $page;
	}
	
	/**
	 * Returns ext conf
	 *
	 * @return array
	 */
	public function getExtConf()
	{
		return $this->extConf;
	}

	/**
	 * Set ext conf
	 *
	 * @param array $extConf ext conf
	 * @return void
	 */
	public function setExtConf($extConf)
	{
		$this->extConf = $extConf;
	}
	
	/**
	 * Returns affected users
	 *
	 * @return array $affectedUsers
	 */
	public function getAffectedUsers()
	{
		return $this->affectedUsers;
	}

	/**
	 * Set affected users
	 *
	 * @param array $affectedUsers affected users
	 * @return void
	 */
	public function setAffectedUsers($affectedUsers)
	{
		$this->affectedUsers = $affectedUsers;
	}
	
	/**
	 * Returns storage page
	 *
	 * @return int
	 */
	public function getStoragePage()
	{
		return $this->storagePage;
	}

	/**
	 * Set storage page
	 *
	 * @param int $storagePage storage page
	 * @return void
	 */
	public function setStoragePage($storagePage)
	{
		$this->storagePage = $storagePage;
	}
}
?>