<?php
namespace Neulandlotsen\MooxNews\Controller;

/**
 * This file is part of the "moox_news" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */
 
class TargetController extends NewsController
{

    const SIGNAL_TAG_LIST_ACTION = 'listAction';

    /**
     * @var \Neulandlotsen\MooxNews\Domain\Repository\TargetRepository
     */
    protected $targetRepository;

    /**
     * Inject a target repository to enable DI
     *
     * @param \Neulandlotsen\MooxNews\Domain\Repository\TargetRepository $targetRepository
     * @return void
     */
    public function injectTargetRepository(\Neulandlotsen\MooxNews\Domain\Repository\TargetRepository $targetRepository)
    {
        $this->targetRepository = $targetRepository;
    }

    /**
     * List categories
     *
     * @param array $overwriteDemand
     * @return void
     */
    public function listAction(array $overwriteDemand = null)
    {
        // Default value is wrong for tags
        if ($this->settings['orderBy'] === 'datetime') {
            unset($this->settings['orderBy']);
        }

        $demand = $this->createDemandObjectFromSettings($this->settings);
        $demand->setActionAndClass(__METHOD__, __CLASS__);

        if ($this->settings['disableOverrideDemand'] != 1 && $overwriteDemand !== null) {
            $demand = $this->overwriteDemandObject($demand, $overwriteDemand);
        }

        $assignedValues = [
            'targets' => $this->targetRepository->findDemanded($demand),
            'overwriteDemand' => $overwriteDemand,
            'demand' => $demand,
        ];

        $assignedValues = $this->emitActionSignal('TargetController', self::SIGNAL_TAG_LIST_ACTION, $assignedValues);
        $this->view->assignMultiple($assignedValues);
    }

}
