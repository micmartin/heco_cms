<?php
namespace Neulandlotsen\MooxNewsSeminar\Service;

/**
 * This file is part of the "moox_news_seminar" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

use Neulandlotsen\MooxNews\Domain\Session\BackendSessionHandler;
use Neulandlotsen\MooxNewsSeminar\Domain\Repository\SpeakerRepository;
use TYPO3\CMS\Backend\Routing\UriBuilder;
use TYPO3\CMS\Core\SingletonInterface;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Utility\HttpUtility;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Database\Query\Restriction\DeletedRestriction;
use TYPO3\CMS\Core\Database\Query\Restriction\HiddenRestriction;
use TYPO3\CMS\Core\FormProtection\FormProtectionFactory;
use TYPO3\CMS\Core\Messaging\FlashMessageService;
use TYPO3\CMS\Backend\Utility\BackendUtility;
use TYPO3\CMS\Core\Utility\RootlineUtility;
use TYPO3\CMS\Extbase\Object\ObjectManager;
use TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface;
use TYPO3\CMS\Frontend\Page\PageRepository;

class HelperService implements SingletonInterface
{	
	/**
	 * @var ObjectManager
	 */
	protected $objectManager;
	
	/**
	 * @var ConfigurationManagerInterface
	 */
	protected $configurationManager;		
	
	/**
	 * @var SpeakerRepository
	 */
	protected $speakerRepository;
	
	/**
	 * @var PageRepository
	 */
	protected $pageRepository;
		
	/**
     * @var BackendSessionHandler
	 */
    protected $backendSession;
	
	/**
	 * @var FlashMessageService
	 */
	protected $flashMessageService;
	
	/**
	 * @var array	
	 */
	protected $configuration;
	
	/**
	 * @var bool
	 */
	protected $extConf;
	
	/**
	 * @var array 	
	 */
	protected $storagePids;
	
	/**
     * @var string 
     */
	protected $storageKey = 'tx_mooxnews';
	
	/**
	 * Path to the locallang file
	 * @var string
	 */
	const LLPATH = 'LLL:EXT:moox_news_seminar/Resources/Private/Language/locallang.xlf:';

    /**
     * @param ObjectManager $objectManager
     */
    public function injectObjectManager(ObjectManager $objectManager)
    {
        $this->objectManager = $objectManager;
    }

    /**
     * @param ConfigurationManagerInterface $configurationManager
     */
    public function injectConfigurationManagerInterface(ConfigurationManagerInterface $configurationManager)
    {
        $this->configurationManager = $configurationManager;
    }

    /**
     * @param SpeakerRepository $speakerRepository
     */
    public function injectSpeakerRepository(SpeakerRepository $speakerRepository)
    {
        $this->speakerRepository = $speakerRepository;
    }

    /**
     * @param PageRepository $pageRepository
     */
    public function injectPageRepository(PageRepository $pageRepository)
    {
        $this->pageRepository = $pageRepository;
    }

    /**
     * @param BackendSessionHandler $backendSession
     */
    public function injectBackendSessionHandler(BackendSessionHandler $backendSession)
    {
        $this->backendSession = $backendSession;
    }

    /**
     * @param FlashMessageService $flashMessageService
     */
    public function injectFlashMessageService(FlashMessageService $flashMessageService)
    {
        $this->flashMessageService = $flashMessageService;
    }
	
	/**
     *
     * @return void
     */
    public function initialize()
	{												
		// set backend session key 
		//$this->backendSession->setStorageKey($this->storageKey);
		
		// get typoscript configuration
		$this->configuration = $this->configurationManager->getConfiguration(ConfigurationManagerInterface::CONFIGURATION_TYPE_FRAMEWORK,"MooxNewsSeminar");						
		
		// get extensions's configuration
		$this->extConf = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Configuration\ExtensionConfiguration::class)->get('moox_news_seminar');
    }
	
	/**
	 * sort helper function
	 *
	 * @param array $a
	 * @param array $b
	 * @return bool
	 */
	public function sortByFolderAndTitle($a, $b)
	{
		return strcmp($a["folder"].$a["title"], $b["folder"].$b["title"]);
	}		
	
	/**
	 * set flash messages
	 *
	 * @param mixed &$parent
	 * @param array $messages
	 * @param string $identifier
	 */
	public function setFlashMessages(&$parent = NULL, $messages = [], $identifier = 'extbase.flashmessages.tx_mooxnewsseminar', $storeInSession = true) 
	{						
		if($parent)
		{						
			// overwrite message queue
			$flashMessageQueue = $this->flashMessageService->getMessageQueueByIdentifier($identifier);	

			// set flash messages
			foreach($messages AS $message)
			{
				if(!is_array($message))
				{
					$message = [];
				}
				if($message['text']=="")
				{
					$message['text'] = "Unbekannter Fehler / Unknown error";
				}				
				if(false && $message['icon']!="" && $message['title']!="")
				{
					$message['title'] = $message['icon'].$message['title'];
				}
				
				$flashMessageQueue->addMessage(
					GeneralUtility::makeInstance('TYPO3\\CMS\\Core\\Messaging\\FlashMessage',
						$message['text'],
						($message['title']!="")?$message['title'].": ":"",
						$message['type'],
						$storeInSession
					)						
				);
			}
		}
	}
	
	/**
	 * get speakers
	 *	
	 * @return array $categories
	 */
	public function getSpeakers()
	{
		$speakers = [];
		
		$speakers = $this->speakerRepository->findByFilter(NULL,['title'=>'ASC'],NULL,NULL,$this->storagePids);
				
		return $speakers;
	}
	
	/**
	 * get fields separator
	 *
	 * @param array $settings
	 * @return string $listViewFieldSeparator
	 */
	public function getFieldsSeparator($settings = [])
	{
		// get list view field separator
		if($settings['listViewFieldSeparator']!="")
		{
			$listViewFieldSeparator = $settings['listViewFieldSeparator'];
		} 
		else 
		{
			$listViewFieldSeparator = "&nbsp;|&nbsp;";
		}
		
		return $listViewFieldSeparator;
	}
	
	/**
	 * get additional listfields
	 *
	 * @return array $fields
	 */
	public function getAdditionalListFields($filter = [])
	{
		global $BE_USER;
		
		$fields = [];
				
		return $fields;
	}			
	
	/**
	 * @param string $table
	 * @param string|array $fieldList
	 * @param array $excludeFields
	 * @param boolean $forceFieldList
	 * @return array list view fields 	
	 */
	public function getListViewFields($table = "tx_mooxnewsseminar_domain_model_speaker", $fieldList = [], $excludeFields = [], $forceFieldList = false)
	{				
		global $BE_USER;
		
		$this->initialize();
		
		$dateTimeFields = ["tstamp","starttime","endtime","crdate"];
		
		$listViewFields = [];
		
		if($table=='tx_mooxnewsseminar_domain_model_speaker')
		{
			if(!in_array("title",$excludeFields))
			{
				$listViewFields[0] = ["name" => "title", "length" => 10000, "type" => "string"];			
			}
		}
		
		return $listViewFields;
	}
	
	/**
	 * Get array of folders with frontend users module	
	 *	
	 * @return array
	 */
	public function getFolders()
	{		
		global $BE_USER;
		
		$folders = [];

        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)
            ->getQueryBuilderForTable('pages');
        $queryBuilder
            ->getRestrictions()
            ->removeAll()
            ->add(GeneralUtility::makeInstance(DeletedRestriction::class))
            ->add(GeneralUtility::makeInstance(HiddenRestriction::class));
// TODO: Where-Statement noch nicht fertig! 2019-05-29
        $query = $queryBuilder
            ->select('*')
            ->from('pages')
            ->where(
                $queryBuilder->expr()->in('module', $queryBuilder->createNamedParameter('mxnews', \PDO::PARAM_STR), $queryBuilder->createNamedParameter('mxspeaker', \PDO::PARAM_STR))
            )
            ->execute();
//        \TYPO3\CMS\Extbase\Utility\DebuggerUtility::var_dump($query);
/*
		$query = [
			'SELECT' => '*',
			'FROM' => 'pages',
			'WHERE' => $BE_USER->getPagePermsClause(1)." AND deleted=0 AND hidden=0 AND doktype=254 AND (module='mxnews' OR module='mxspeaker')"
		];
		$pages = $GLOBALS['TYPO3_DB']->exec_SELECT_queryArray($query);
*/

		$folderCnt = 0;
		while($row = $query->fetch())
		{
			$folders[$folderCnt] = $row;
			$rootline = GeneralUtility::makeInstance(RootlineUtility::class, $row['uid'], '', $this->pageRepository)->get();
			
			foreach($rootline AS $rootlinepage)
			{
				if($rootlinepage['is_siteroot'])
				{
					$folders[$folderCnt]['rootpage'] = $rootlinepage;
					break;
				}
			}
			
			if(!$folders[$folderCnt]['rootpage'])
			{
				$folders[$folderCnt]['rootpage'] = $rootline[0];
			}
					
			$rootline = array_reverse($rootline);
			
			if(isset($rootline[count($rootline)-2]))
			{			
				$pageInfo = $this->pageRepository->getPage((int)$rootline[count($rootline)-2]['uid']);		
				if(in_array($pageInfo['module'],['mxnews','mxspeaker']))
				{
					$folders[$folderCnt]['folder'] = $pageInfo['uid'];				
				}				
			}
			
			$folders[$folderCnt]['rootline'] = $rootline;
			$folderCnt++;
		}
				
		usort($folders, ["\Neulandlotsen\MooxNewsSeminar\Service\HelperService", "sortByFolderAndTitle"]);
		
		$folders = array_reverse($folders);
		
		$groupedFolders = [];
		
		foreach($folders AS $folder)
		{
			$groupedFolders[($folder['rootpage']['nav_title'])?$folder['rootpage']['nav_title']:$folder['rootpage']['title']][] = $folder; 
		}
		
		return $groupedFolders;		
	}
	
	/**
	 * set page or redirect to default/last folder
	 *
	 * @param array $folders
	 * @param array $filter
	 * @param array $settings
	 * @param string $identifier
	 */
	public function redirectToFolder($folders = [], $filter = [], $settings = [], $identifier = NULL)
	{		
		if(isset($filter['page']) && $filter['page']>0)
		{
			$storagePage = $filter['page'];			
		} 
		
		$readPageAccess = BackendUtility::readPageAccess($storagePage, '');
		
		if((!$readPageAccess && $storagePage!=0) || !is_numeric($storagePage)){
			$storagePage = 0;
			$filter['page'] = 0;
			$this->updateFilter($filter,$identifier);			
		}
		
		if($storagePage>0)
		{
			$isFeUsersFolder = false;
			foreach($folders AS $groupeFeUserFolders)
			{
				foreach($groupeFeUserFolders AS $feUserFolder)
				{
					if($feUserFolder['uid']==$storagePage)
					{
						$isFeUsersFolder = true;
						break;
					}
				}
			}
			if(!$isFeUsersFolder)
			{
				if($identifier!="")
				{
					$filter['page'] = 0;
					$this->updateFilter($filter,$identifier);
				}				
				HttpUtility::redirect($_SERVER['REQUEST_URI']);				
			}
		}
		
		return $storagePage;
	}
	
	/**
	 * @param string $query
	 * @return array 	
	 */
	public function getQueryReplacements($query = "")
	{		
		$replacements = [];
		
		if($query!="")
		{
			$replacements[] = ["search" => $query, "replace" => '<span class="bg-info">'.$query.'</span>'];
			$replacements[] = ["search" => strtolower($query), "replace" => '<span class="bg-info">'.strtolower($query).'</span>'];
			$replacements[] = ["search" => strtoupper($query), "replace" => '<span class="bg-info">'.strtoupper($query).'</span>'];
			$replacements[] = ["search" => ucfirst($query), "replace" => '<span class="bg-info">'.ucfirst($query).'</span>'];
			$replacements[] = ["search" => '<span class="bg-info"><span class="bg-info">', "replace" => '<span class="bg-info">'];
			$replacements[] = ["search" => '</span></span>', "replace" => '</span>'];
		}
		
		return $replacements;
	}
	
	/**
     * Redirect to tceform creating a new record
     *
	 * @param string $table table name
	 * @param string $storagePage storage page
	 * @param string $returnUrl return url
	 * @param string $variant variant
     */
    public function redirectToCreateNewRecord($table,$storagePage = 0, $returnUrl = '', $variant = '')
    {                
		$storagePage = $storagePage+0;
		
		exit($returnUrl);

        $uriBuilder = GeneralUtility::makeInstance(UriBuilder::class);
		
		if($variant!="")
		{
			$url = $uriBuilder->buildUriFromRoute('record_edit', [
                'edit[' . $table . '][' . $storagePage . ']' => 'new',
                'defVals[' . $table . '][variant]' => $variant,
                'returnUrl' => $returnUrl
            ]);
		} 
		else 
		{
			$url = $uriBuilder->buildUriFromRoute('record_edit', [
                'edit[' . $table . '][' . $storagePage . ']' => 'new',
                'returnUrl' => $returnUrl
            ]);
		}
        HttpUtility::redirect($url);
    }
	
	/**
     * Get a CSRF token
     *
     * @param bool $tokenOnly Set it to TRUE to get only the token, otherwise including the &moduleToken= as prefix
     * @return string
     */
    protected function getToken($tokenOnly = false)
    {
        $token = FormProtectionFactory::get()->generateToken('moduleCall', 'moox_MooxNewsSeminarMod1');
        if($tokenOnly)
		{
            return $token;
        } 
		else 
		{
            return '&moduleToken=' . $token;
        }
    }
	
	/**
	 * @param string $identifier
	 * @param array $settings
	 * @return array
	 */
	public function buildFilter($identifier = NULL, $settings = [])
	{				
		global $BE_USER;
		
		$filter = NULL;
		
		if($identifier!="")
		{			
			$identifier = $identifier."Filter";
			
			if($this->backendSession->stored($identifier))
			{	
				if($this->backendSession->get($identifier)!="")
				{				
					$filter = unserialize($this->backendSession->get($identifier));							
				} 
			} 
			
			if(!is_array($filter))
			{
				
				if((int)$settings['itemsPerPage']>0)
				{
					$filter['perPage'] = (int)$settings['itemsPerPage'];					
				} 
				else 
				{
					$filter['perPage'] = 10;
				}
				
				if($BE_USER->user['mooxnewsseminar_listview_fields']!="")
				{
					$fieldList = explode(",",$BE_USER->user['mooxnewsseminar_listview_fields']);
				} 
				elseif($settings['listViewFields']!="")
				{
					$fieldList = explode(",",$settings['listViewFields']);
				}
				$fieldList = [];
				if(count($fieldList))
				{
					$filter['fields'] = [];
					foreach($fieldList AS $field)
					{
						$filter['fields'][] = $field;
					}
				}
                $pageTsModuleStartPid = $BE_USER->getTSConfig()['tx_mooxnewsseminar.']['moduleStartPid'];
				if((int)$pageTsModuleStartPid>0)
				{
					$pid = $pageTsModuleStartPid;					
				} 
				elseif((int)$settings['moduleStartPid']>0)
				{
					$pid = (int)$settings['moduleStartPid'];					
				}
				$readPageAccess = BackendUtility::readPageAccess($pid, '');
				if(!is_array($readPageAccess) || !$pid)
				{
					$pid = 0;
				}
				$filter['page'] = $pid;
				$this->backendSession->store($identifier,serialize($filter));						
			}			
		}
		
		if(!isset($filter['fields']) || !is_array($filter['fields']) || !count($filter['fields'])){
			$filter['fields'] = ['title'];
		}
		
		return $filter;
	}
	
	/**
	 * update filter
	 *
	 * @param array $filter
	 * @param string $identifier
	 */
	public function updateFilter($filter = NULL, $identifier = NULL)
	{				
		if(is_array($filter) && $identifier!="")
		{						
			$identifier = $identifier."Filter";			
			$this->backendSession->store($identifier,serialize($filter));			
		}
		return $filter;
	}
	
	/**
	 * @param string $identifier
	 */
	public function resetFilter($identifier = NULL)
	{				
		if($identifier!="")
		{	
			$identifier = $identifier."Filter";
			$this->backendSession->delete($identifier);
		}
	}
	
	/**
	 * @param string $identifier
	 * @param array $settings
	 * @return array
	 */
	public function buildOrdering($identifier = NULL, $settings = [])
	{				
		global $BE_USER;
		
		$ordering = NULL;
		
		if($identifier!="")
		{			
			$identifier = $identifier."Ordering";
			
			if($this->backendSession->stored($identifier)){	
				if($this->backendSession->get($identifier)!=""){				
					$ordering = unserialize($this->backendSession->get($identifier));				
				} 
			}
			
			if(!is_array($ordering))
			{
				$ordering['field'] = "title";
				$ordering['direction'] = "asc";
				
				$this->backendSession->store($identifier,serialize($ordering));
			}
		}
		
		return $ordering;
	}
	
	/**
	 * @param array $ordering
	 * @param string $identifier
	 */
	public function updateOrdering($ordering = NULL, $identifier = NULL)
	{				
		if(is_array($ordering) && $identifier!="")
		{						
			$identifier = $identifier."Ordering";			
			$this->backendSession->store($identifier,serialize($ordering));			
		}
		return $ordering;
	}
	
	/**
	 * @param string $identifier
	 */
	public function resetOrdering($identifier = NULL)
	{				
		if($identifier!="")
		{	
			$identifier = $identifier."Ordering";
			$this->backendSession->delete($identifier);
		}
	}
	
	/**
	 * hide tca fields off parent type
	 *
	 * @param string $hideFields
	 * @param string $table
	 * @param string $variant
	 * @param string $operator
	 */
	public static function tcaHideDefaultFields($hideFields = "", $table = "", $variant = "", $operator = "=")
	{		
		// hide classified default fields defined in hide fields string 
		if($hideFields!="")
		{
			$hideFields = explode(",",$hideFields);
			foreach($hideFields AS $hideField)
			{
				if(isset($GLOBALS['TCA'][$table]['columns'][$hideField]))
				{
					if(!isset($GLOBALS['TCA'][$table]['columns'][$hideField]['displayCond']))
					{
						$GLOBALS['TCA'][$table]['columns'][$hideField]['displayCond'] = "";
					}
					if(is_string($GLOBALS['TCA'][$table]['columns'][$hideField]['displayCond']) && $GLOBALS['TCA'][$table]['columns'][$hideField]['displayCond']=="")
					{
						$GLOBALS['TCA'][$table]['columns'][$hideField]['displayCond'] = 'FIELD:variant:'.$operator.':'.$variant;						
					} 
					elseif(is_string($GLOBALS['TCA'][$table]['columns'][$hideField]['displayCond']) && $GLOBALS['TCA'][$table]['columns'][$hideField]['displayCond']!="")
					{
						$displayCond = $GLOBALS['TCA'][$table]['columns'][$hideField]['displayCond'];
						$GLOBALS['TCA'][$table]['columns'][$hideField]['displayCond'] = [];
						$GLOBALS['TCA'][$table]['columns'][$hideField]['displayCond']['OR'] = [];
						$GLOBALS['TCA'][$table]['columns'][$hideField]['displayCond']['OR'][] = $displayCond;
						$GLOBALS['TCA'][$table]['columns'][$hideField]['displayCond']['OR'][] = 'FIELD:variant:'.$operator.':'.$variant;
						$GLOBALS['TCA'][$table]['columns'][$hideField]['displayCond']['OR'] = array_unique($GLOBALS['TCA'][$table]['columns'][$hideField]['displayCond']['OR']);
					} 
					elseif(is_array($GLOBALS['TCA'][$table]['columns'][$hideField]['displayCond']) && is_array($GLOBALS['TCA'][$table]['columns'][$hideField]['displayCond']['OR']))
					{					
						$GLOBALS['TCA'][$table]['columns'][$hideField]['displayCond']['OR'][] = $displayCond;
						$GLOBALS['TCA'][$table]['columns'][$hideField]['displayCond']['OR'][] = 'FIELD:variant:'.$operator.':'.$variant;
						$GLOBALS['TCA'][$table]['columns'][$hideField]['displayCond']['OR'] = array_unique($GLOBALS['TCA'][$table]['columns'][$hideField]['displayCond']['OR']);
					} 
					elseif(is_array($GLOBALS['TCA'][$table]['columns'][$hideField]['displayCond']) && is_array($GLOBALS['TCA'][$table]['columns'][$hideField]['displayCond']['AND']))
					{					
						if(!is_array($GLOBALS['TCA'][$table]['columns'][$hideField]['displayCond']['AND']['OR']))
						{
							$GLOBALS['TCA'][$table]['columns'][$hideField]['displayCond']['AND']['OR'] = [];
						}
						$GLOBALS['TCA'][$table]['columns'][$hideField]['displayCond']['AND']['OR'][] = 'FIELD:variant:'.$operator.':'.$variant;
						$GLOBALS['TCA'][$table]['columns'][$hideField]['displayCond']['AND']['OR'] = array_unique($GLOBALS['TCA'][$table]['columns'][$hideField]['displayCond']['AND']['OR']);
					}						
				}
			}
		}		
	}	
	
	/**
	 * @return array
	 */
	public function getStoragePids()
	{
		return $this->storagePids;
	}

	/**
	 * @param array $storagePids storage pids
	 */
	public function setStoragePids($storagePids)
	{
		$this->storagePids = $storagePids;
	}
	
	/**
	 * @return string
	 */
	public function getStorageKey()
	{
		return $this->storageKey;
	}
	
	/**
      * @param string $storageKey 
	  */
	public function setStorageKey($storageKey)
	{ 
		$this->storageKey = $storageKey; 
	}
}
?>