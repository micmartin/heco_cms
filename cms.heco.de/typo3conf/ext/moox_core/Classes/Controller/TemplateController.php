<?php
namespace Neulandlotsen\MooxCore\Controller;

/**
 * This file is part of the "moox_core" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */
 
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Utility\HttpUtility;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Core\Messaging\FlashMessage;
use TYPO3\CMS\Backend\Utility\BackendUtility;
use TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface;
use TYPO3\CMS\Extbase\Persistence\PersistenceManagerInterface;
use Neulandlotsen\MooxCore\Domain\Repository\TemplateRepository;
use Neulandlotsen\MooxCore\Service\HelperService;
use Neulandlotsen\MooxCore\Utility\Backend\HelperUtility;
use Neulandlotsen\MooxCore\Utility\HelperUtility as FrontendHelperUtility;

class TemplateController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{
	/**
	 * @var PersistenceManagerInterface
	 */
	protected $persistenceManager;

	/**
	 * @var TemplateRepository
	 */
	protected $templateRepository;

	/**
	 * @var HelperService
	 */
	protected $helperService;

	/**
	 * @var BackendSessionHandler
	 */
	protected $backendSession;

	/**
	 * @var HelperUtility
	 */
	protected $helperUtility;
	
	/**
	 * @var int
	 */
	protected $storagePid;
	
	/**
	 * @var bool
	 */
	protected $extConf;	

	/**
	 * @var array	
	 */
	protected $configuration;
	
	/**
	 * @var array
	 */
	protected $folders;
	
	/**
	 * @var array
	 */
	protected $filter;
	
	/**
	 * @var array
	 */
	protected $ordering;
	
	/**
	 * @var array
	 */
	protected $errors = [];
	
	/**
	 * @var array
	 */
	protected $messages = [];

	/**
	 * @var string
	 */
	protected $table = 'tx_mooxcore_domain_model_template';

	/**
	 * @var string
	 */
	protected $sessionStorageKey = 'tx_mooxcore_template';

	/**
	 * @var string
	 */
	protected $controllerClass = 'Neulandlotsen\MooxCore\Controller\TemplateController';

    /**
     * @param PersistenceManagerInterface $persistenceManager
     */
    public function injectPersistenceManagerInterface(PersistenceManagerInterface $persistenceManager)
    {
        $this->persistenceManager = $persistenceManager;
    }
//ToDo prüfen 2019-08-28
    /**
     * @param TemplateRepository $templateRepository
     */
/*    public function injectTemplateRepository(TemplateRepository $templateRepository)
    {
        $this->templateRepository = $templateRepository;
    }
*/
    /**
     * @param HelperService $helperService
     */
/*    public function injectHelperService(HelperService $helperService)
    {
        $this->helperService = $helperService;
    }
*/

    /**
     * @param BackendSessionHandler $backendSession
     */
    /*  public function injectBackendSessionHandler(BackendSessionHandler $backendSession)
    {
        $this->backendSession = $backendSession;
    }
*/
    /**
     * @param HelperUtility $helperUtility
     */
    public function injectHelperUtility(HelperUtility $helperUtility)
    {
        $this->helperUtility = $helperUtility;
    }

	/**
	 * initialize the controller
	 */
	protected function initializeAction()
	{
		// parent initialization
		parent::initializeAction();

		// set backend session key 
		$this->backendSession->setStorageKey($this->sessionStorageKey);

		// set filter
		$this->filter = $this->helperUtility->buildFilter($this->controllerClass,$this->settings);

		// set ordering
		$this->ordering = $this->helperUtility->buildOrdering($this->controllerClass,$this->settings);

		// initialize storage settings
		$this->initializeStorageSettings();

		// set ext conf
		$this->extConf = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Configuration\ExtensionConfiguration::class)->get(GeneralUtility::camelCaseToLowerCaseUnderscored($this->extensionName));

		// get typoscript configuration
		$this->configuration = $this->configurationManager->getConfiguration(
			ConfigurationManagerInterface::CONFIGURATION_TYPE_FRAMEWORK,
			$this->extensionName
		);
	}
	
	/**
	 * initialize storage settings
	 */
	protected function initializeStorageSettings()
	{								
		// get configuration
		$configuration = $this->configurationManager->getConfiguration(ConfigurationManagerInterface::CONFIGURATION_TYPE_FRAMEWORK);
				
		// force to save elements on root level
		$currentPid['persistence']['storagePid'] = $configuration['persistence']['templateStoragePid'];
		$this->configurationManager->setConfiguration(array_merge($configuration, $currentPid));
		
		// set helper storage page
		$this->helperService->setStoragePage($currentPid['persistence']['storagePid']);
	}
	
	/**
	 * @param bool $refresh
	 * @param bool $ajax	
	 * @param string $returnUrl
	 */
	public function listAction($refresh = FALSE, $ajax = FALSE, $returnUrl = NULL)
	{
	    // get templates
		$templates = $this->templateRepository->findByFilter($this->filter,[$this->ordering['field']=>$this->ordering['direction']],NULL,NULL,'all',['disabled','starttime','endtime']);
		
		// set template variables		
		$this->view->assign('templates', $templates);	
		$this->view->assign('fields', $this->helperService->getListViewFields($this->table,[],[],[],true));
		$this->view->assign('fieldsSeparator', $this->helperService->getFieldsSeparator($this->settings));
		$this->view->assign('filter', $this->filter);	
		$this->view->assign('ordering', $this->ordering);		
		$this->view->assign('storagePage', $this->storagePage);
		$this->view->assign('replacements', $this->helperUtility->getQueryReplacements($this->filter['query']));
		$this->view->assign('conf', $this->extConf);
		$this->view->assign('returnUrl', $this->uriBuilder->getRequest()->getRequestUri());
		$this->view->assign('refresh', $refresh);
		$this->view->assign('table', $this->table);
		$this->view->assign('action', 'list');
	}		
	
	/**
	 * @param array $filter
	 * @param bool $ajax
	 * @param bool $reset
	 * @param string $returnUrl
	 */
	public function filterAction($filter = NULL, $ajax = FALSE, $reset = FALSE, $returnUrl = NULL)
	{
		if($reset)
		{			
			$this->helperUtility->resetFilter($this->controllerClass);
		} 
		else 
		{			
			if(is_array($filter))
			{
				foreach($filter AS $key => $value)
				{
					if($value!="" && $value!="all")
					{
						$this->filter[$key] = $value;
					} 
					elseif(isset($this->filter[$key]))
					{
						unset($this->filter[$key]);
					}
				}
				$this->helperUtility->updateFilter($this->filter,$this->controllerClass);
			} 			
		}
		
		if(!$ajax)
		{			
			if($returnUrl)
			{
				$this->redirectToUri($returnUrl);
			}			
		} 
		else 
		{
			header ("HTTP/1.0 200 Ok");
			exit();
		}
	}
	
	/**
	 * @param array $ordering
	 * @param bool $ajax
	 * @param bool $reset
	 * @param string $returnUrl 
	 */
	public function orderingAction($ordering = NULL, $ajax = FALSE, $reset = FALSE, $returnUrl = NULL)
	{
		if($reset)
		{			
			$this->helperUtility->resetOrdering($this->controllerClass);
			$this->ordering = $this->helperUtility->buildOrdering($this->controllerClass,$this->settings);
		} 
		else 
		{			
			if(is_array($ordering))
			{
				$this->ordering = $ordering;				
				$this->helperUtility->updateOrdering($this->controllerClass);
			}
		}
		
		if(!$this->ajax)
		{
			if($returnUrl)
			{
				$this->redirectToUri($returnUrl);
			}
		} 
		else 
		{
			header ("HTTP/1.0 200 Ok");
			exit();
		}
	}
	
	/**
	 * @param int $uid
	 * @param string $returnUrl
	 */
	public function hiddenAction($uid = 0, $returnUrl = NULL)
	{					
		if($uid>0)
		{									
			$item = $this->templateRepository->findByUid($uid,FALSE);
			
			if($item->getHidden()==1)
			{
				$item->setHidden(0);				
			} 
			else 
			{
				$item->setHidden(1);				
			}			
			
			$this->templateRepository->update($item);								
			$this->persistenceManager->persistAll();
								
		} 
		
		if($returnUrl)
		{
			$this->redirectToUri($returnUrl);
		} 
		else 
		{
			$this->redirect("list");
		}		
	}
	
	/**
	 * @param int $uid
	 * @param string $returnUrl
	 * @param boolean $directCall 
	 * @param array $settings 
	 * @param array $data
	 * @return void
	 */
	public function copyAction($uid = 0, $returnUrl = NULL, $directCall = FALSE, $settings = [], $data = [])
	{										
		if($uid>0)
		{
			$item = $this->templateRepository->findByUid($uid,FALSE);
			
			if(is_object($item))
			{			
				$settings = array_merge($this->settings,$settings);
				
				$settings['prefix'] = "Kopie";
				
				if($settings['prefix']!="")
				{
					$data['title'] = "[".$settings['prefix']."] ".$item->getTitle();
				}
				
				$this->helperUtility->copyBackendRecord($this->table,$item->getUid(),$item->getPid(),$data);
				
				if(!$directCall)
				{
					// add message
					$this->messages[] = [				
						"text" => 'Template wurde kopiert',
						"type" => FlashMessage::OK,
					];
						
					// set flash messages
					$this->helperUtility->setFlashMessages($this, $this->extensionName);
				}
			}						
		}
		
		if(!$directCall)
		{
			if($returnUrl)
			{
				$this->redirectToUri($returnUrl);
			} 
			else 
			{
				$this->redirect("list");
			}
		}
	}
	
	/**
	 * @param int $uid
	 * @param string $field
	 * @param string $previewFormat
	 */
	public function previewIframeAction($uid = 0, $field = 'template', $previewFormat = "html")
	{							
		if($uid>0)
		{		
			$template = $this->templateRepository->findByUid($uid,FALSE);

			$partialRootPaths = [GeneralUtility::getFileAbsFileName("EXT:".GeneralUtility::camelCaseToLowerCaseUnderscored($this->extensionName)."/Resources/Private/Partials/Mail/")];

			//ToDo: check this, remove or correct 2019-07-30
			if($partialRootPath=="")
			{
				$partialRootPath = GeneralUtility::getFileAbsFileName(str_replace("Backend/","",array_shift($this->configuration['view']['partialRootPaths']))."Mail/");
				if(is_dir($partialRootPath))
				{
					$partialRootPaths[] = GeneralUtility::getFileAbsFileName("EXT:".GeneralUtility::camelCaseToLowerCaseUnderscored($this->extensionName)."/Resources/Private/Extensions/".GeneralUtility::camelCaseToLowerCaseUnderscored($this->extensionName)."/Partials/Mail");
				}
			}
			
			if (!empty($this->extConf['mailRenderingPartialRoot']))
			{
				$partialRootPath = GeneralUtility::getFileAbsFileName($this->extConf['mailRenderingPartialRoot']);
				if (substr($partialRootPath, -1) != '/')
				{
					$partialRootPath = $partialRootPath."/";
				}
				if(is_dir($partialRootPath))
				{
					$partialRootPaths[] = $partialRootPath;	
				} 
			}
			
			$getMethodName = 'get'.GeneralUtility::underscoredToUpperCamelCase($field);
			$previewView = $this->objectManager->get('TYPO3\\CMS\\Fluid\\View\StandaloneView');
			$previewView->setFormat('html');
			$templateCode = $template->$getMethodName();
			if($this->extConf['showTemplateVariables'])
			{
				$templateCode .= '<f:debug title="Variablen" inline="1">{_all}</f:debug>';
			}
			if($previewFormat=="text")
			{
				$templateCode = nl2br($templateCode);
			}
			$previewView->setTemplateSource($templateCode);
			$previewView->setPartialRootPaths($partialRootPaths);
			$previewView->assignMultiple($this->helperService->templateDummyData);
			$preview = $previewView->render();
			
			$this->view->assign('preview', $preview);
		} 
		else 
		{
			$this->view->assign('preview', "Vorschau kann nicht angezeigt werden.");
		}
	}
	
	/**
	 * @return int
	 */
	public function getStoragePid()
	{
		return $this->storagePid;
	}

	/**
	 * @param int $storagePid
	 */
	public function setStoragePid($storagePid)
	{
		$this->storagePid = $storagePid;
	}
	
	/**
	 * @return array
	 */
	public function getExtConf()
	{
		return $this->extConf;
	}

	/**
	 * @param array $extConf
	 */
	public function setExtConf($extConf)
	{
		$this->extConf = $extConf;
	}

	/**
	 * @return array
	 */
	public function getControllerClass()
	{
		return $this->controllerClass;
	}

	/**
	 * @param array $controllerClass
	 */
	public function setControllerClass($controllerClass)
	{
		$this->controllerClass = $controllerClass;
	}

	/**
	 * @return array
	 */
	public function getFolders()
	{
		return $this->folders;
	}

	/**
	 * @param array $folders
	 */
	public function setFolders($folders)
	{
		$this->folders = $folders;
	}
	
	/**
	 * @return array
	 */
	public function getFilter()
	{
		return $this->filter;
	}

	/**
	 * @param array $filter
	 */
	public function setFilter($filter)
	{
		$this->filter = $filter;
	}
	
	/**
	 * @return array
	 */
	public function getOrdering()
	{
		return $this->ordering;
	}

	/**
	 * @param array $ordering
	*/
	public function setOrdering($ordering)
	{
		$this->ordering = $ordering;
	}	
	
	/**
	 * @return array
	 */
	public function getErrors()
	{
		return $this->errors;
	}

	/**
	 * @param array $errors
	 */
	public function setErrors($errors)
	{
		$this->errors = $errors;
	}
	
	/**
	 * @return array
	 */
	public function getMessages()
	{
		return $this->messages;
	}

	/**
	 * @param array $messages
	 */
	public function setMessages($messages)
	{
		$this->messages = $messages;
	}
}
?>