<?php
if (!defined('TYPO3_MODE'))
{
	die ('Access denied.');
}

$ll = 'LLL:EXT:moox_news/Resources/Private/Language/locallang_db.xlf:';

/**
 * Add extra fields to the sys_category record
 */
$newSysCategoryColumns = [
    'pid' => [
        'label' => 'pid',
        'config' => [
            'type' => 'passthrough'
        ]
    ],
    'sorting' => [
        'label' => 'sorting',
        'config' => [
            'type' => 'passthrough'
        ]
    ],
    'crdate' => [
        'label' => 'crdate',
        'config' => [
            'type' => 'passthrough',
        ]
    ],
    'tstamp' => [
        'label' => 'tstamp',
        'config' => [
            'type' => 'passthrough',
        ]
    ],
	'fe_group' => [
		'exclude' => 1,
		'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.fe_group',
		'config' => [
			'type' => 'select',
			'size' => 6,
			'maxitems' => 99,
			'items' => [
				[
					'LLL:EXT:lang/locallang_general.xlf:LGL.hide_at_login',
					-1,
				],
				[
					'LLL:EXT:lang/locallang_general.xlf:LGL.any_login',
					-2,
				],
				[
					'LLL:EXT:lang/locallang_general.xlf:LGL.usergroups',
					'--div--',
				],
			],
			'exclusiveKeys' => '-1,-2',
			'foreign_table' => 'fe_groups',
			'foreign_table_where' => 'ORDER BY fe_groups.title',
		],
	],
    'images' => [
        'exclude' => true,
        'l10n_mode' => 'mergeIfNotBlank',
        'label' => $ll . 'tx_mooxnews_domain_model_category.image',
        'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig(
            'images',
            [
                'appearance' => [
                    'createNewRelationLinkTitle' => 'LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:images.addFileReference',
                    'showPossibleLocalizationRecords' => true,
                    'showRemovedLocalizationRecords' => true,
                    'showAllLocalizationLink' => true,
                    'showSynchronizationLink' => true
                ],
                'foreign_match_fields' => [
                    'fieldname' => 'images',
                    'tablenames' => 'sys_category',
                    'table_local' => 'sys_file',
                ],
            ],
            $GLOBALS['TYPO3_CONF_VARS']['GFX']['imagefile_ext']
        )
    ],
    'single_pid' => [
        'exclude' => true,
        'l10n_mode' => 'mergeIfNotBlank',
        'label' => $ll . 'tx_mooxnews_domain_model_category.single_pid',
        'config' => [
            'type' => 'group',
            'internal_type' => 'db',
            'allowed' => 'pages',
            'size' => 1,
            'maxitems' => 1,
            'show_thumbs' => 1,
            'default' => 0,
            'wizards' => [
                'suggest' => [
                    'type' => 'suggest',
                    'default' => [
                        'searchWholePhrase' => true
                    ]
                ],
            ],
        ]
    ],
	'preview_pid' => [
		'exclude' => true,
		'l10n_mode' => 'mergeIfNotBlank',
		'label' => $ll . 'tx_mooxnews_domain_model_category.preview_pid',
		'config' => [
            'type' => 'group',
            'internal_type' => 'db',
            'allowed' => 'pages',
            'size' => 1,
            'maxitems' => 1,
            'minitems' => 0,
            'show_thumbs' => 1,
            'wizards' => [
                'suggest' => [
                    'type' => 'suggest',
                    'default' => [
                        'searchWholePhrase' => true
                    ]
                ],
            ],
        ]
	],
    'shortcut' => [
        'exclude' => true,
        'l10n_mode' => 'mergeIfNotBlank',
        'label' => $ll . 'tx_mooxnews_domain_model_category.shortcut',
        'config' => [
            'type' => 'group',
            'internal_type' => 'db',
            'allowed' => 'pages',
            'size' => 1,
            'maxitems' => 1,
            'show_thumbs' => true,
            'default' => 0,
            'wizards' => [
                'suggest' => [
                    'type' => 'suggest',
                    'default' => [
                        'searchWholePhrase' => true
                    ]
                ],
            ],
        ]
    ],
	'news_types' => [
		'exclude' => true,
		'l10n_mode' => 'mergeIfNotBlank',
		'label' => $ll . 'tx_mooxnews_domain_model_category.news_types',
		'config' => [
			'type' => 'select',			
			'size' => 6,
			'maxitems' => 999,
			'minitems' => 0,
			'allowNonIdValues' => 1,			
			'itemsProcFunc' => 'Neulandlotsen\MooxNews\Hooks\ItemsProcFunc->user_types',
			'default' => ''
		]
	],
    'import_id' => [
        'label' => $ll . 'tx_mooxnews_domain_model_news.import_id',
        'config' => [
            'type' => 'passthrough'
        ]
    ],
    'import_source' => [
        'label' => $ll . 'tx_mooxnews_domain_model_news.import_source',
        'config' => [
            'type' => 'passthrough'
        ]
    ],
    'seo_headline' => [
        'exclude' => true,
        'label' => $ll . 'tx_mooxnews_domain_model_category.seo.seo_headline',
        'config' => [
            'type' => 'input',
        ],
    ],
    'seo_title' => [
        'exclude' => true,
        'label' => $ll . 'tx_mooxnews_domain_model_category.seo.seo_title',
        'config' => [
            'type' => 'input',
        ],
    ],
    'seo_description' => [
        'exclude' => true,
        'label' => $ll . 'tx_mooxnews_domain_model_category.seo.seo_description',
        'config' => [
            'type' => 'text',
        ],
    ],
    'seo_text' => [
        'exclude' => true,
        'label' => $ll . 'tx_mooxnews_domain_model_category.seo.seo_text',
        'config' => [
            'type' => 'text',
        ],
        'defaultExtras' => 'richtext:rte_transform[mode=ts_css]',
    ],
	'slug' => [
		'exclude' => true,
		'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_tca.xlf:pages.slug',
		'displayCond' => 'USER:' . \TYPO3\CMS\Core\Compatibility\PseudoSiteTcaDisplayCondition::class . '->isInPseudoSite:pages:false',
		'config' => [
			'type' => 'slug',
			'size' => 50,
			'generatorOptions' => [
				'fields' => ['title'],
				'replacements' => [
					'/' => '-'
				],
			],
			'fallbackCharacter' => '-',
			'eval' => 'uniqueInSite',
			'default' => ''
		]
	],
	'filter_title' => [
		'exclude' => true,
		'label' => $ll . 'tx_mooxnews_domain_model_category.filter_title',
		'config' => [
			'type' => 'input',
		],
	],
];

//TODO: check this
//$tx_mooxnews_domain_model_news['columns']['categories']['config']['foreign_table_where'] = ' AND pid=###CURRENT_PID### AND (sys_category.sys_language_uid = 0 OR sys_category.l10n_parent = 0) ORDER BY sys_category.sorting';

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('sys_category', $newSysCategoryColumns,1);
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes('sys_category','--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.options, images', '', 'before:description');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes('sys_category','filter_title', '', 'after:title');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes('sys_category','--div--;News, single_pid,preview_pid,shortcut', '', 'after:description');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes('sys_category','fe_group,news_types');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes('sys_category','--div--;' . $ll . 'tx_mooxnews_domain_model_category.tabs.seo, seo_title, seo_description, seo_headline, seo_text', '', 'after:endtime');

//TODO: check this
/*
$GLOBALS['TCA']['sys_category']['columns']['items']['config']['MM_oppositeUsage']['tx_mooxnews_domain_model_news']
    = [0 => 'categories'];
*/

$GLOBALS['TCA']['sys_category']['ctrl']['label_userFunc'] = \Neulandlotsen\MooxNews\Hooks\Labels::class . '->getUserLabelCategory';

// add fe_group as enable field
//TODO: nicht sicher ob korrekt, war in einem System einkommentiert
//$GLOBALS['TCA']['sys_category']['ctrl']['enablecolumns']['fe_group'] = 'fe_group';
//TODO: check this
/*$GLOBALS['TCA']['sys_category']['ctrl']['columns']['items']['config']['MM_oppositeUsage'] = array(
        'pages' => array('categories'),
        'sys_file_metadata' => array('categories'),
        'tt_content' => array('categories'),
		'tx_mooxnews_domain_model_news' => array('categories'),
);*/
$GLOBALS['TCA']['sys_category']['ctrl']['columns']['items']['config'] = [
	'type' => 'passthrough'
];
$GLOBALS['TCA']['sys_category']['types'][1]['showitem'] = str_replace("--div--;LLL:EXT:lang/locallang_tca.xlf:sys_category.tabs.items,items,","",$GLOBALS['TCA']['sys_category']['types'][1]['showitem']);

// Extension manager configuration
$configuration = \Neulandlotsen\MooxNews\Utility\EmConfiguration::getSettings();

if($configuration->getCategoryRestriction()=="current_pid")
{
	$GLOBALS['TCA']['sys_category']['columns']['parent']['config']['foreign_table_where'] = ' AND pid=###CURRENT_PID###'.$GLOBALS['TCA']['sys_category']['columns']['parent']['config']['foreign_table_where'];
}
