<?php
namespace Neulandlotsen\MooxNews\Service;

/**
 * This file is part of the "moox_news" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

use Neulandlotsen\MooxNews\Domain\Repository\NewsRepository;
use Neulandlotsen\MooxNews\Domain\Session\BackendSessionHandler;
use TYPO3\CMS\Backend\Routing\UriBuilder;
use TYPO3\CMS\Core\DataHandling\DataHandler;
use TYPO3\CMS\Core\SingletonInterface;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Utility\HttpUtility;
use TYPO3\CMS\Core\FormProtection\FormProtectionFactory;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Database\Query\Restriction\HiddenRestriction;
use TYPO3\CMS\Core\Database\Query\Restriction\DeletedRestriction;
use TYPO3\CMS\Core\Messaging\FlashMessageService;
use TYPO3\CMS\Backend\Utility\BackendUtility;
use TYPO3\CMS\Core\Utility\RootlineUtility;
use TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface;
use TYPO3\CMS\Extbase\Configuration\BackendConfigurationManager;
use TYPO3\CMS\Extbase\Object\ObjectManager;
use TYPO3\CMS\Frontend\Page\PageRepository;

class HelperService implements SingletonInterface
{	
	/**
	 * @var ObjectManager
	 */
	protected $objectManager;
	
	/**
	 * @var ConfigurationManagerInterface
	 */
	protected $configurationManager;
	
	/**
	 * @var BackendConfigurationManager
	 */
	protected $backendConfigurationManager;
	
	/**
     * @var \TYPO3\CMS\Core\DataHandling\DataHandler
	 */
    protected $dataHandler;
	
	/**
	 * @var NewsRepository
	 */
	protected $newsRepository;
	
	/**
	 * @var PageRepository
	 */
	protected $pageRepository;
		
	/**
     * @var BackendSessionHandler
	 */
    protected $backendSession;
	
	/**
	 * @var FlashMessageService
	 */
	protected $flashMessageService;
	
	/**
	 * @var array	
	 */
	protected $configuration;
	
	/**
	 * @var bool
	 */
	protected $extConf;
	
	/**
	 * @var array 	
	 */
	protected $storagePids;
	
	/**
	 * @var array
	 */
	protected $allowedFields = [
		"uid",
		"title",
		"crdate",
		"tstamp",
		"starttime",
		"endtime",
		"datetime",
		"event_starttime",
		"event_endtime",
	];

	/**
	 * @var array
	 */
	public $excludeFieldsFromFilterSession = [];
	
	/**
	 * Path to the locallang file
	 * @var string
	 */
	const LLPATH = 'LLL:EXT:moox_news/Resources/Private/Language/locallang.xlf:';

    /**
     * @param ObjectManager $objectManager
     */
    public function injectObjectManager(ObjectManager $objectManager)
    {
        $this->objectManager = $objectManager;
    }

    /**
     * @param ConfigurationManagerInterface $configurationManager
     */
    public function injectConfigurationManagerInterface(ConfigurationManagerInterface $configurationManager)
    {
        $this->configurationManager = $configurationManager;
    }

    /**
     * @param BackendConfigurationManager $backendConfigurationManager
     */
    public function injectBackendConfigurationManager(BackendConfigurationManager $backendConfigurationManager)
    {
        $this->backendConfigurationManager = $backendConfigurationManager;
    }

    /**
     * @param NewsRepository $newsRepository
     */
    public function injectNewsRepository(NewsRepository $newsRepository)
    {
        $this->newsRepository = $newsRepository;
    }

    /**
     * @param PageRepository $pageRepository
     */
    public function injectPageRepository(PageRepository $pageRepository)
    {
        $this->pageRepository = $pageRepository;
    }

    /**
     * @param BackendSessionHandler $backendSession
     */
    public function injectBackendSessionHandler(BackendSessionHandler $backendSession)
    {
        $this->backendSession = $backendSession;
    }

    /**
     * @param FlashMessageService $flashMessageService
     */
    public function injectFlashMessageService(FlashMessageService $flashMessageService)
    {
        $this->flashMessageService = $flashMessageService;
    }
	
	/**
     *
     * @return void
     */
    public function initialize()
	{												
		// get typoscript configuration
		$this->configuration = $this->configurationManager->getConfiguration(ConfigurationManagerInterface::CONFIGURATION_TYPE_FRAMEWORK,"MooxNews");				
		
		// get extensions's configuration
		$this->extConf = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Configuration\ExtensionConfiguration::class)->get('moox_news');
    }
	
	/**
	 * sort helper function
	 *
	 * @param array $a
	 * @param array $b
	 * @return bool
	 */
	public function sortByFolderAndTitle($a, $b)
	{
		$search = array("Ä", "Ö", "Ü", "ä", "ö", "ü", "ß", "´");
		$replace = array("Ae", "Oe", "Ue", "ae", "oe", "ue", "ss", "");

		return str_replace($search, $replace,($a['rootpage']['title'].$a['title'])) < str_replace($search, $replace,($b['rootpage']['title'].$b['title']));
	}

	/**
	 * @param integer $pageUid page uid
	 * @return array $TSObj->setup
	 */
	public function getTsSetup($pageUid)
	{		
		$pageSelect  = GeneralUtility::makeInstance('TYPO3\CMS\Frontend\Page\PageRepository');   
		$pageSelect->init(false);
        $rootLine = GeneralUtility::makeInstance(RootlineUtility::class, $pageUid, '', $pageSelect)->get();
		$templateService = GeneralUtility::makeInstance('TYPO3\CMS\Core\TypoScript\TemplateService');             
        $templateService->tt_track = 0;
        $templateService->init();
        $templateService->runThroughTemplates($rootLine);
        $templateService->generateConfig();		
		
		return $templateService->setup;
	}
	
	/**
	 * @param integer $pageUid page uid
	 * @return string $baseURL
	 */
	public function getBaseUrl($pageUid)
	{		
		$config = $this->getTsSetup($pageUid)['config.'];
         
		if($_SERVER['HTTPS']=="on")
		{
			$prefix = "https";
		} 
		else 
		{
			$prefix = "http";
		}
		
		if($config['baseURL']==$prefix."://")
		{ 
			$baseURL = $prefix."://".$_SERVER['SERVER_NAME']."/";
		}  
		elseif($config['baseURL']=="//")
		{ 
			$baseURL = $prefix."://".$_SERVER['SERVER_NAME']."/";
		} 
		elseif($config['baseURL']=="/")
		{ 
			$baseURL = $prefix."://".$_SERVER['SERVER_NAME']."/";
		} 
		else 
		{
			$baseURL = $config['baseURL'];
			if(substr(strtolower($baseURL),0,2)=="//")
			{				
				$baseURL = $prefix.":".$baseURL;
			}			
		} 
		
		if(substr($baseURL,(strlen($baseURL)-1))!="/")
		{ 
			$baseURL = $baseURL."/";
		}
		
		return $baseURL;
	}
	
	/**
	 * set flash messages
	 *
	 * @param mixed &$parent
	 * @param array $messages
	 * @param string $identifier
	 * @param boolean $storeInSession
	 */
	public function setFlashMessages(&$parent = NULL, $messages = [], $identifier = 'extbase.flashmessages.tx_mooxnews.backend', $storeInSession = TRUE) 
	{						
		if($identifier=='')
		{
			$identifier = 'extbase.flashmessages.tx_mooxnews.backend';
		}
		if($parent)
		{									
			// overwrite message queue
			$flashMessageQueue = $this->flashMessageService->getMessageQueueByIdentifier($identifier);	

			// set flash messages
			foreach($messages AS $message)
			{
				if(!is_array($message))
				{
					$message = [];
				}
				if($message['text']=="")
				{
					$message['text'] = "Unbekannter Fehler / Unknown error";
				}				
				if(false && $message['icon']!="" && $message['title']!="")
				{
					$message['title'] = $message['icon'].$message['title'];
				}
				
				$flashMessageQueue->addMessage(
					GeneralUtility::makeInstance('TYPO3\\CMS\\Core\\Messaging\\FlashMessage',
						$message['text'],
						($message['title']!="")?$message['title'].": ":"",
						$message['type'],
						$storeInSession
					)						
				);
			}
		}
	}
	
	/**
	 * @param string $table
	 * @param array $settings
	 * @param array $filter
	 * @param array $excludeFields
	 * @return array list view fields 	
	 */
	public function getListViewFields($table = "tx_mooxnews_domain_model_news", $settings = [], $filter = [], $excludeFields = [])
	{				
		$this->initialize();
		
		$listViewFieldsList = "";		
		$listViewFields = [];		
		$allowedFormatTypes = [
			'text',
			'date',
			'time',
			'datetime',
			'email',
			'url',
			'plain',
			'html',
			'currency',
			'boolean',
			'default',
			'translate'
		];
		
		if(in_array($table,['tx_mooxnews_domain_model_news']))
		{
			$allowedListViewFields = (is_array($GLOBALS['TCA'][$table]['moox']['allowedBackendListViewFields']))?$GLOBALS['TCA'][$table]['moox']['allowedBackendListViewFields']:[];

			if(isset($filter['type']) && $filter['type']!="" && isset($settings['backendListViewFields'][$table][$filter['type']]) && $settings['backendListViewFields'][$table][$filter['type']]!="")
			{
				$listViewFieldsList = $settings['backendListViewFields'][$table][$filter['type']];				
			}
			elseif(isset($settings['backendListViewFields'][$table]['moox_news']) && $settings['backendListViewFields'][$table]['moox_news']!="")
			{
				$listViewFieldsList = $settings['backendListViewFields'][$table]['moox_news'];				
			} 
			else 
			{
				$listViewFieldsList = "title:75,datetime,crdate,tstamp";				
			}

			$listViewFieldsTmp = explode(",",$listViewFieldsList);
			foreach($listViewFieldsTmp AS $listViewField)
			{
				$listViewField = explode(":",$listViewField);
				
				if(in_array(trim($listViewField[0]),$allowedListViewFields))
				{
					if(!in_array(trim($listViewField[0]),$excludeFields))
					{
						$listViewFieldInfo = $GLOBALS['TCA'][$table]['columns'][trim($listViewField[0])];
						if ($listViewFieldInfo['moox']['backend']['type'] == "" || !in_array($listViewFieldInfo['moox']['backend']['type'], $allowedFormatTypes)) {
							$listViewFieldInfo['moox']['backend']['type'] = 'default';
						}

						$fieldArray = [
							"name" => trim($listViewField[0]),
							"length" => (trim($listViewField[1]) > 0) ? trim($listViewField[1]) : 10000,
							"label" => ($listViewFieldInfo['moox']['backend']['label'])?$listViewFieldInfo['moox']['backend']['label']:$listViewFieldInfo['label'],
							"extkey" => $listViewFieldInfo['moox']['extkey'],
						];
						// TODO: doppelt, label overwrite aufgrund von merge eigentlich nicht nötig!
						$listViewFields[] = array_merge($fieldArray, $listViewFieldInfo['moox']['backend']);
					}
				} 
				else 
				{
					$additionalFieldsAccepted = true;
					$listViewAdditionalFields = [];
					$listViewAdditionalFieldsTmp = explode("|",$listViewField[0]);					
					if(count($listViewAdditionalFieldsTmp)>1)
					{
						foreach($listViewAdditionalFieldsTmp AS $listViewAdditionalField)
						{
							if(in_array(trim($listViewAdditionalField),$allowedListViewFields))
							{
								if(!in_array(trim($listViewField[0]),$excludeFields))
								{
									$listViewFieldInfo = $GLOBALS['TCA'][$table]['columns'][trim($listViewAdditionalField)];
									if ($listViewFieldInfo['moox']['backend']['type'] == "" || !in_array($listViewFieldInfo['moox']['backend']['type'], $allowedFormatTypes)) {
										$listViewFieldInfo['moox']['backend']['type'] = 'default';
									}

									$fieldArray = [
										"name" 	=> trim($listViewAdditionalField)
									];

									// TODO: doppelt, label overwrite aufgrund von merge eigentlich nicht nötig!
									$listViewAdditionalFields[] = array_merge($fieldArray, $listViewFieldInfo['moox']['backend']);
								}
							} 
							else 
							{
								$additionalFieldsAccepted = false;
								break;
							}							
						}
					} 
					else 
					{
						$additionalFieldsAccepted = false;
					}
					if($additionalFieldsAccepted)
					{
						$mainViewField = $listViewAdditionalFields[0];
						$mainViewFieldInfo = $GLOBALS['TCA'][$table]['columns'][trim($listViewAdditionalField)];
						if ($mainViewFieldInfo['moox']['backend']['type'] == "" || !in_array($mainViewFieldInfo['moox']['backend']['type'], $allowedFormatTypes)) {
							$mainViewFieldInfo['moox']['backend']['type'] = 'default';
						}
						unset($listViewAdditionalFields[0]);
						$fieldArray = [
							"name" => $mainViewField,
							"length" => (trim($listViewField[1])>0)?trim($listViewField[1]):10000,
							"type" => $mainViewFieldInfo['moox']['backend']['type'],
							"label" => ($mainViewFieldInfo['moox']['backend']['label'])?$mainViewFieldInfo['moox']['backend']['label']:$mainViewFieldInfo['label'],
							"additional" => $listViewAdditionalFields
						];

						// TODO: doppelt, label overwrite aufgrund von merge eigentlich nicht nötig!
						$listViewFields[] = array_merge($fieldArray, $mainViewFieldInfo['moox']['backend']);
					}
				}
			}
		}
		elseif($table=='tx_mooxnews_domain_model_target')
		{
			$listViewFields[0] = ["name" => "title", "length" => 10000, "type" => "string", "extkey" => "moox_news"];
			$listViewFields[1] = ["name" => "uid", "length" => 10000, "type" => "string", "extkey" => "moox_news"];
		}
		elseif($table=='tx_mooxnews_domain_model_tag')
		{
			$listViewFields[0] = ["name" => "title", "length" => 10000, "type" => "string", "extkey" => "moox_news"];
			$listViewFields[1] = ["name" => "uid", "length" => 10000, "type" => "string", "extkey" => "moox_news"];
		}
		elseif($table=='sys_category')
		{
			$listViewFields[0] = ["name" => "title", "length" => 10000, "type" => "string", "extkey" => "moox_news"];
			$listViewFields[1] = ["name" => "uid", "length" => 10000, "type" => "string", "extkey" => "moox_news"];
		}
		
		return $listViewFields;
	}
	
	/**
	 * @param string $table
	 * @param array $settings
	 * @param array $filter
	 * @return array list view fields 	
	 */
	public function getPreviewFields($table = "tx_mooxnews_domain_model_news", $settings = [], $filter = [])
	{				
		$backendPreviewFieldsList = [];		
		$previewFields = [];
		$allowedFormatTypes = [
			'text',
			'date',
			'time',
			'datetime',
			'email',
			'url',
			'plain',
			'html',
			'currency',
			'boolean',
			'default',
			'images',
			'list',
			'translate'
		];
		
		if(in_array($table,['tx_mooxnews_domain_model_news']))
		{			
			if(isset($filter['type']) && $filter['type']!="" && isset($GLOBALS['TCA'][$table]['moox']['backendPreviewFields'][$filter['type']]) && $GLOBALS['TCA'][$table]['moox']['backendPreviewFields'][$filter['type']]!="")
			{
				$backendPreviewFieldsList = $GLOBALS['TCA'][$table]['moox']['backendPreviewFields'][$filter['type']];				
			}
			elseif(false && is_array($GLOBALS['TCA'][$table]['moox']['backendPreviewFields']))
			{
				foreach($GLOBALS['TCA'][$table]['moox']['backendPreviewFields'] AS $set){
					$backendPreviewFieldsList = array_merge($backendPreviewFieldsList,$set);
				}				
			} 
			else 
			{
				$backendPreviewFieldsList = $GLOBALS['TCA'][$table]['moox']['backendPreviewFields']['moox_news'];				
			}
						
			foreach($backendPreviewFieldsList AS $previewField)
			{
				if(isset($GLOBALS['TCA'][$table]['columns'][$previewField])){
					$previewFieldInfo = $GLOBALS['TCA'][$table]['columns'][$previewField];
					
					if($previewFieldInfo['moox']['backend']['type']=="" || !in_array($previewFieldInfo['moox']['backend']['type'],$allowedFormatTypes))
					{
						$previewFieldInfo['moox']['backend']['type'] = 'default';
					}
					
					$fieldArray = [	
						"name" => trim($previewField),					
						"label" => $previewFieldInfo['label']
					];
					if($previewFieldInfo['moox']['backend']['type']=="translate")
					{
						if(is_array($previewFieldInfo['config']['items']) && count($previewFieldInfo['config']['items']))
						{
							$items = [];
							foreach($previewFieldInfo['config']['items'] AS $item)
							{
								$items[$item[1]] = [
									'label' => $item[0],
									'icon' => $item[2]
								];
							}
							$fieldArray['items'] = $items;
						}
					}
					unset($previewFieldInfo['moox']['backend']['label']);
					$previewFields[] = array_merge($fieldArray,$previewFieldInfo['moox']['backend']);					
				}
			}			
		}
		
		return $previewFields;
	}
	
	/**
	 * @param string $table
	 * @param string $type
	 * @param array $settings
	 * @return array list view fields 	
	 */
	public function getSearchFields($table = "tx_mooxnews_domain_model_news", $type="moox_news", $settings = [])
	{				
		$searchFields = [];		
				
		if(in_array($table,['tx_mooxnews_domain_model_news']))
		{			
			if(is_array($settings['searchFields'][$table]))
			{
				if(isset($settings['searchFields'][$table][$type]) && $settings['searchFields'][$table][$type]!=''){
					$searchFields = explode(",",$settings['searchFields'][$table][$type]);
				}			
			}				
		}
		
		if(!count($searchFields)){
			$searchFields = ['title','teaser','bodytext'];
		}
		
		return $searchFields;
	}
	
	/**
	 * @param string $table
	 * @param string $type
	 * @param array $settings
	 * @return array reset fields 	
	 */
	public function getResetFields($table = "tx_mooxnews_domain_model_news", $type="moox_news", $settings = [])
	{				
		$resetFields = [];		
		
		if(in_array($table,['tx_mooxnews_domain_model_news']))
		{			
			if(is_array($settings['resetFieldsOnDuplicate'][$table]))
			{
				if(isset($settings['resetFieldsOnDuplicate'][$table][$type]) && $settings['resetFieldsOnDuplicate'][$table][$type]!='')
				{
					$resetFieldsTmp = explode(",",$settings['resetFieldsOnDuplicate'][$table][$type]);
					foreach($resetFieldsTmp AS $resetFieldTmp)
					{
						$resetField = explode(":",$resetFieldTmp);
						if(count($resetField)>1)
						{
							if($resetField[1]=="now()")
							{
								$resetFields[$resetField[0]] = time();
							}
							elseif($resetField[1]=="now(Y)")
							{
								$resetFields[$resetField[0]] = date('Y');
							}
							else
							{
								$resetFields[$resetField[0]] = "";
							}
						} 
						else 
						{
							$resetFields[$resetField[0]] = "";
						}	
					}
				}			
			}				
		}
		
		$defaultResetFields = ['datetime' => "",'starttime' => "",'endtime' => "",'archive' => "",'event_starttime' => "",'event_endtime' => "",'related' => "",'grabber_remote_uid' => "",'grabber_remote_hash' => "",'import_source' => "",'import_id' => ""];
		
		if(!count($resetFields))
		{
			$resetFields = $defaultResetFields;
		}
		else
		{
			$resetFields = array_merge($defaultResetFields,$resetFields);
		}
		
		return $resetFields;
	}
	
	/**
	 * @param string $table
	 * @param array $settings
	 * @return array list view fields 	
	 */
	public function getAdditionalControls($table = "tx_mooxnews_domain_model_news", $settings = [])
	{				
		$additionalControls = [];		
				
		if(in_array($table,['tx_mooxnews_domain_model_news']))
		{			
			if(is_array($settings['backendAdditionalControls'][$table]))
			{
				$additionalControls = $settings['backendAdditionalControls'][$table];				
			}				
		}
		
		return $additionalControls;
	}
	
	/**
	 * @param string $table
	 * @param array $settings
	 * @return array list view fields 	
	 */
	public function getAdditionalFilters($table = "tx_mooxnews_domain_model_news", $settings = [])
	{				
		$additionalFilters = [];		
				
		if(in_array($table,['tx_mooxnews_domain_model_news']))
		{			
			if(is_array($settings['backendAdditionalFilters'][$table]))
			{
				$additionalFilters = $settings['backendAdditionalFilters'][$table];				
			}				
		}
		
		return $additionalFilters;
	}
	
	/**
	 * @param string $table
	 * @param array $settings
	 * @return array list view fields 	
	 */
	public function getAdditionalButtons($table = "tx_mooxnews_domain_model_news", $settings = [])
	{				
		$additionalButtons = [];		
				
		if(in_array($table,['tx_mooxnews_domain_model_news']))
		{			
			if(is_array($settings['backendAdditionalButtons'][$table]))
			{
				$additionalButtons = $settings['backendAdditionalButtons'][$table];				
			}				
		}
		
		return $additionalButtons;
	}
	
	/**
	 * @param string $table
	 * @param array $settings
	 * @return array list view fields 	
	 */
	public function getAdditionalAddButtonItems($table = "tx_mooxnews_domain_model_news", $settings = [])
	{				
		$additionalAddButtonItems = [];		
				
		if(in_array($table,['tx_mooxnews_domain_model_news']))
		{			
			if(is_array($settings['backendAdditionalAddButtonItems'][$table]))
			{
				$additionalAddButtonItems = $settings['backendAdditionalAddButtonItems'][$table];				
			}				
		}
		
		return $additionalAddButtonItems;
	}
	
	/**
	 * @param array $settings
	 * @return array list view fields 	
	 */
	public function getAdditionalModuleSelectItems($settings = [])
	{				
		$additionalModuleSelectItems = [];		
				
		if(is_array($settings['backendAdditionalModuleSelectItems']))
		{
			$additionalModuleSelectItems = $settings['backendAdditionalModuleSelectItems'];				
		}				
		
		return $additionalModuleSelectItems;
	}
	
	/**
	 * @param string $table
	 * @param array $settings
	 * @return array list view fields 	
	 */
	public function getAdditionalMultiple($table = "tx_mooxnews_domain_model_news", $settings = [])
	{				
		$additionalMultiple = [];		
		
		/*		
		if($table=="all")
		{
			if(is_array($settings['backendAdditionalMultiple']))
			{
				foreach($settings['backendAdditionalMultiple'] AS $array)
				{
					if(is_array($array))
					{
						$additionalMultiple = array_merge($additionalMultiple,$array);
					}
				}
			}
		}
		else 
		{
			if(is_array($settings['backendAdditionalMultiple'][$table]))
			{
				$additionalMultiple = $settings['backendAdditionalMultiple'][$table];				
			}				
		}
		*/
		if(is_array($settings['backendAdditionalMultiple']['all']))
		{
			$additionalMultiple = $settings['backendAdditionalMultiple']['all'];			
		}	
		if(is_array($settings['backendAdditionalMultiple'][$table]))
		{
			$additionalMultiple = array_merge($additionalMultiple,$settings['backendAdditionalMultiple'][$table]);		
		}
		
		return $additionalMultiple;
	}
	
	/**
	 * @param string $allowedTypes
	 * @return array	
	 */
	public function getTypes($allowedTypes = "")
	{		
		$types 	= [];	
			
		if($allowedTypes!="")
		{
			$allowedTypes = explode(",",$allowedTypes);
		} 
		else 
		{
			$allowedTypes = [];
		}
				
		foreach($GLOBALS['TCA']['tx_mooxnews_domain_model_news']['columns']['type']['config']['items'] AS $option)
		{			
			if((count($allowedTypes)<1 || in_array($option[1],$allowedTypes)) && !($this->extConf['hideDefaultNewsType'] && $option[1]=="moox_news"))
			{
				$types[$option[1]] = $GLOBALS['LANG']->sL($option[0]);
			}
		}
		return $types;
	}
	
	/**	 
	 * @return array	
	 */
	public function getAdditionalBackendMultipleFunctions()
	{
		
		$additionalBackendMultipleFunctions = [];
		
		if(is_array($GLOBALS['TCA']['tx_mooxnews_domain_model_news']['moox']['additionalBackendMultipleFunctions']))
		{
			$additionalBackendMultipleFunctions = $GLOBALS['TCA']['tx_mooxnews_domain_model_news']['moox']['additionalBackendMultipleFunctions'];
		}
		
		return $additionalBackendMultipleFunctions;
	}
	
	/**	 
	 * @param string $table
	 * @param array $settings
	 * @return array	
	 */
	public function getAdditionalBackendPreviewPartials($table = "tx_mooxnews_domain_model_news", $settings = [])
	{
		
		$additionalBackendPreviewPartials = [];
		
		if(in_array($table,['tx_mooxnews_domain_model_news']))
		{			
			if(is_array($settings['backendAdditionalPreviewPartials'][$table]))
			{
				foreach($settings['backendAdditionalPreviewPartials'][$table] AS $type => $partialList)
				{
					$additionalBackendPreviewPartials[$type] = explode(",",$partialList);
				}
			}				
		}
		
		return $additionalBackendPreviewPartials;
	}
	
	/**
	 * get fields separator
	 *
	 * @param array $settings
	 * @return string $listViewFieldSeparator
	 */
	public function getFieldsSeparator($settings = [])
	{
		// get list view field separator
		if($settings['listViewFieldSeparator']!="")
		{
			$listViewFieldSeparator = $settings['listViewFieldSeparator'];
		} 
		else 
		{
			$listViewFieldSeparator = "&nbsp;|&nbsp;";
		}
		
		return $listViewFieldSeparator;
	}

	/**
	 * get years from db
	 *
	 * @param array $settings
	 * @param array $filter
	 * @param bool $excludeEmptyYear
	 * @return string $years
	 */
	public function getYears($settings = [], $filter = [], $excludeEmptyYear = false)
	{
		$years = [];
		$queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)
			->getQueryBuilderForTable('tx_mooxnews_domain_model_news');

		$queryBuilder
			->select('year')
			->from('tx_mooxnews_domain_model_news')
			->orderBy('year','DESC')
			->groupBy('year');

		$foundYears = $queryBuilder->execute();

		while($year = $foundYears->fetch())
		{
			if(!$excludeEmptyYear || $year['year']>1)
			{
				$years[$year['year']] = $year['year'];
			}
		}

		return $years;
	}
	
	/**
	 * Get array of folders with frontend users module	
	 *	
	 * @return array
	 */
	public function getFolders()
	{
		global $BE_USER;
		
		$folders = [];

		$queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)
			->getQueryBuilderForTable('pages');
		$queryBuilder
			->getRestrictions()
			->removeAll()
			->add(GeneralUtility::makeInstance(DeletedRestriction::class))
			->add(GeneralUtility::makeInstance(HiddenRestriction::class));

		$res = $queryBuilder
			->select('*')
			->from('pages')
			->where(
				$queryBuilder->expr()->eq('module', $queryBuilder->createNamedParameter('mxnews', \PDO::PARAM_STR)),
				$queryBuilder->expr()->eq('l10n_parent', $queryBuilder->createNamedParameter(0, \PDO::PARAM_INT))
			)
			->orderBy('title','ASC')
			->execute();

		$folderCnt = 0;
		while($row = $res->fetch())
		{
			if(BackendUtility::readPageAccess($row['uid'], $BE_USER->getPagePermsClause(1)))
			{
				$folders[$folderCnt] = $row;
				$rootline = GeneralUtility::makeInstance(RootlineUtility::class, $row['uid'], '', $this->pageRepository)->get();
				
				foreach($rootline AS $rootlinepage)
				{
					if($rootlinepage['is_siteroot'])
					{
						$folders[$folderCnt]['rootpage'] = $rootlinepage;
						break;
					}
				}
				
				if(!$folders[$folderCnt]['rootpage'])
				{
					$folders[$folderCnt]['rootpage'] = $rootline[0];
				}
						
				$rootline = array_reverse($rootline);
				
				if(isset($rootline[count($rootline)-2]))
				{			
					$pageInfo = $this->pageRepository->getPage((int)$rootline[count($rootline)-2]['uid']);		
					if($pageInfo['module']=='mxnews')
					{
						$folders[$folderCnt]['folder'] = $pageInfo['uid'];				
					}
					
				}
				
				$folders[$folderCnt]['rootline'] = $rootline;
				$folderCnt++;
			}
		}

		usort($folders, ["\Neulandlotsen\MooxNews\Service\HelperService", "sortByFolderAndTitle"]);
		
		$folders = array_reverse($folders);
		
		$groupedFolders = [];
		
		foreach($folders AS $folder)
		{
			$groupedFolders[($folder['rootpage']['nav_title'])?$folder['rootpage']['nav_title']:$folder['rootpage']['title']][] = $folder; 
		}
		
		return $groupedFolders;		
	}
	
	/**
	 * set page or redirect to default/last folder
	 *
	 * @param array $folders
	 * @param array $filter
	 * @param array $settings
	 * @param string $identifier
	 */
	public function redirectToFolder($folders = [], $filter = [], $settings = [], $identifier = NULL)
	{						
		if(isset($filter['page']) && $filter['page']>0)
		{
			$storagePage = $filter['page'];			
		} 
		
		$readPageAccess = BackendUtility::readPageAccess($storagePage, '');
		
		if((!$readPageAccess && $storagePage!=0) || !is_numeric($storagePage)){
			$storagePage = 0;
			$filter['page'] = 0;
			//$filterTmp = $this->buildFilter($identifier,$settings);
			//print_r($filterTmp);
			$this->updateFilter($filter,$identifier);
			//exit();
			//HttpUtility::redirect($_SERVER['REQUEST_URI']);
		}
		
		if($storagePage>0)
		{
			$isNewsFolder = false;
			foreach($folders AS $groupedNewsFolders)
			{
				foreach($groupedNewsFolders AS $newsFolder)
				{
					if($newsFolder['uid']==$storagePage)
					{
						$isNewsFolder = true;
						break;
					}
				}
			}
			if(!$isNewsFolder)
			{
				if($identifier!="")
				{
					$filter['page'] = 0;
					$this->updateFilter($filter,$identifier);
				}				
				HttpUtility::redirect($_SERVER['REQUEST_URI']);				
			}
		}
		
		return $storagePage;
	}
	
	/**
	 * @param array $settings
	 * @param int $pid
	 * @return array 	
	 */
	public function mergeSettingsOfCurrentPid($settings = [],$pid = 0)
	{		
		if($pid>0)
		{
			$_POST['id'] = $pid;
			$settingsByPid = $this->backendConfigurationManager->getTypoScriptSetup();	
			if(is_array($settingsByPid) && is_array($settingsByPid['module.']['tx_mooxnews.']['settings.']))
			{
				$settings = array_merge($settings,$settingsByPid['module.']['tx_mooxnews.']['settings.']);
			}
			unset($_POST['id']);
		}
		
		return $settings;
	}
	
	/**
	 * @param string $query
	 * @return array 	
	 */
	public function getQueryReplacements($query = "")
	{		
		$replacements = [];
		
		if($query!="")
		{
			$replacements[] = ["search" => $query, "replace" => '<span class="bg-info">'.$query.'</span>'];
			$replacements[] = ["search" => strtolower($query), "replace" => '<span class="bg-info">'.strtolower($query).'</span>'];
			$replacements[] = ["search" => strtoupper($query), "replace" => '<span class="bg-info">'.strtoupper($query).'</span>'];
			$replacements[] = ["search" => ucfirst($query), "replace" => '<span class="bg-info">'.ucfirst($query).'</span>'];
			$replacements[] = ["search" => '<span class="bg-info"><span class="bg-info">', "replace" => '<span class="bg-info">'];
			$replacements[] = ["search" => '</span></span>', "replace" => '</span>'];
		}
		
		return $replacements;
	}
	
	/**
     * Redirect to tceform creating a new record
     *
	 * @param string $table
	 * @param string $storagePage
	 * @param string $type
     */
    public function redirectToCreateNewRecord($table,$storagePage = 0,$type = '')
    {                
		$storagePage = $storagePage+0;
		$returnUrl = 'index.php?M=moox_MooxNewsMod1&id=' . $storagePage . $this->getToken();
        $uriBuilder = GeneralUtility::makeInstance(UriBuilder::class);
        if($type!="")
		{
			$url = $uriBuilder->buildUriFromRoute('record_edit', [
                'edit[' . $table . '][' . $storagePage . ']' => 'new',
                'defVals[' . $table . '][type]' => $type,
                'returnUrl' => $returnUrl
            ]);
		} 
		else 
		{
			$url = $uriBuilder->buildUriFromRoute('record_edit', [
                'edit[' . $table . '][' . $storagePage . ']' => 'new',
                'returnUrl' => $returnUrl
	        ]);
		}
        HttpUtility::redirect($url);
    }
	
	/**
     * Get a CSRF token
     *
     * @param bool $tokenOnly Set it to TRUE to get only the token, otherwise including the &moduleToken= as prefix
     * @return string
     */
    protected function getToken($tokenOnly = false)
    {
        $token = FormProtectionFactory::get()->generateToken('moduleCall', 'moox_MooxNewsMod1');
        if($tokenOnly)
		{
            return $token;
        } 
		else 
		{
            return '&moduleToken=' . $token;
        }
    }
			
	/**
	 * @param string $identifier
	 * @param array $settings
	 * @return array
	 */
	public function buildFilter($identifier = NULL, $settings = [])
	{				
		global $BE_USER;
		
		$filter = NULL;
		
		// TODO: Workaround for new extensions with new list view concept
		if(is_array($settings['itemsPerPage']))
		{
			$settings['itemsPerPage'] = $settings['itemsPerPage']['_typoScriptNodeValue'];
		}
		if(is_array($settings['moduleStartPid']))
		{
			$settings['moduleStartPid'] = $settings['moduleStartPid']['_typoScriptNodeValue'];
		}
		// TODO END

		if($identifier!="")
		{			
			$identifier = $identifier."Filter";
			
			if($this->backendSession->stored($identifier))
			{	
				if($this->backendSession->get($identifier)!="")
				{				
					$filter = unserialize($this->backendSession->get($identifier));				
				} 
			}

			if(!is_array($filter))
			{
				if((int)$settings['itemsPerPage']>0)
				{
					$filter['perPage'] = (int)$settings['itemsPerPage'];					
				} 
				else 
				{
					$filter['perPage'] = 10;
				}
				$pageTsModuleStartPid = $BE_USER->getTSConfig()['tx_mooxnews.']['moduleStartPid'];

				if((int)$pageTsModuleStartPid>0)
				{
					$pid = $pageTsModuleStartPid;					
				} 
				elseif((int)$settings['moduleStartPid']>0)
				{
					$pid = (int)$settings['moduleStartPid'];					
				}
				$readPageAccess = BackendUtility::readPageAccess($pid, '');
				if(!is_array($readPageAccess) || !$pid)
				{
					$pid = 0;
				}
				$filter['page'] = $pid;
				$this->backendSession->store($identifier,serialize($filter));
			}			
		}
		
		return $filter;
	}
	
	/**
	 * update filter
	 *
	 * @param array $filter
	 * @param string $identifier
	 */
	public function updateFilter($filter = NULL, $identifier = NULL)
	{				
		if(is_array($filter) && $identifier!="")
		{						
			$identifier = $identifier."Filter";
			foreach ($this->excludeFieldsFromFilterSession AS $excludeFilter)
			{
				unset($filter[$excludeFilter]);
			}
			$this->backendSession->store($identifier,serialize($filter));			
		}
		return $filter;
	}
	
	/**
	 * @param string $identifier
	 */
	public function resetFilter($identifier = NULL)
	{				
		if($identifier!="")
		{	
			$identifier = $identifier."Filter";
			$this->backendSession->delete($identifier);
		}
	}
	
	/**
	 * @param string $identifier
	 * @param array $settings
	 * @return array
	 */
	public function buildOrdering($identifier = NULL, $settings = [])
	{				
		$ordering = NULL;
		
		// TODO: Workaround for new extensions with new list view concept
		if(is_array($settings['defaultSortField']))
		{
			$settings['defaultSortField'] = $settings['defaultSortField']['_typoScriptNodeValue'];
		}
		if(is_array($settings['defaultSortDirection']))
		{
			$settings['defaultSortDirection'] = $settings['defaultSortDirection']['_typoScriptNodeValue'];
		}
		// TODO END
		
		if($identifier!="")
		{			
			$identifier = $identifier."Ordering";
			
			if($this->backendSession->stored($identifier))
			{	
				if($this->backendSession->get($identifier)!="")
				{				
					$ordering = unserialize($this->backendSession->get($identifier));					
				} 
			}

			if(!is_array($ordering))
			{
				if($settings['defaultSortField']!='' && $GLOBALS['TCA']['tx_mooxnews_domain_model_news']['columns'][$settings['defaultSortField']]['moox']['sortable'])
				{
					$ordering['field'] = $settings['defaultSortField'];					
				} 
				else 
				{
					$ordering['field'] = 'title';					
				}
				if($settings['defaultSortDirection']!='' && in_array($settings['defaultSortDirection'],['asc','desc']))
				{
					$ordering['direction'] = $settings['defaultSortDirection'];
				} 
				else 
				{
					$ordering['direction'] = 'asc';
				}				
				$this->backendSession->store($identifier,serialize($ordering));
			}
		}
		
		return $ordering;
	}
	
	/**
	 * @param array $ordering
	 * @param string $identifier
	 */
	public function updateOrdering($ordering = NULL, $identifier = NULL)
	{				
		if(is_array($ordering) && $identifier!="")
		{						
			$identifier = $identifier."Ordering";			
			$this->backendSession->store($identifier,serialize($ordering));			
		}
		return $ordering;
	}
	
	/**
	 * @param string $identifier
	 */
	public function resetOrdering($identifier = NULL)
	{				
		if($identifier!="")
		{	
			$identifier = $identifier."Ordering";
			$this->backendSession->delete($identifier);
		}
	}
	
	/**
	 * Creating date collection between two dates
	 *
	 * <code>
	 * <?php
	 * # Example 1
	 * date_range("2014-01-01", "2014-01-20", "+1 day", "m/d/Y");
	 *
	 * # Example 2. you can use even time
	 * date_range("01:00:00", "23:00:00", "+1 hour", "H:i:s");
	 * </code>
	 *
	 * @author Ali OYGUR <alioygur@gmail.com>
	 * @param string since any date, time or datetime format
	 * @param string until any date, time or datetime format
	 * @param string step
	 * @param string date of output format
	 * @return array
	 */
	public function dateRange($first, $last, $step = '+1 day', $output_format = 'd/m/Y' ) {

		$dates = array();
		$current = strtotime($first);
		$last = strtotime($last);

		while( $current <= $last ) {

			$dates[] = date($output_format, $current);
			$current = strtotime($step, $current);
		}

		return $dates;
	}
	
	/**
	 * hide tca fields off parent type
	 *
	 * @param string $hideFields
	 * @param string $table
	 * @param string $variant
	 * @param string $operator
	 */
	public static function tcaHideDefaultFields($hideFields = "", $table = "", $variant = "", $operator = "=")
	{		
		// hide classified default fields defined in hide fields string 
		if($hideFields!="")
		{
			$hideFields = explode(",",$hideFields);
			foreach($hideFields AS $hideField)
			{
				if(isset($GLOBALS['TCA'][$table]['columns'][$hideField]))
				{
					if(!isset($GLOBALS['TCA'][$table]['columns'][$hideField]['displayCond']))
					{
						$GLOBALS['TCA'][$table]['columns'][$hideField]['displayCond'] = "";
					}
					if(is_string($GLOBALS['TCA'][$table]['columns'][$hideField]['displayCond']) && $GLOBALS['TCA'][$table]['columns'][$hideField]['displayCond']=="")
					{
						$GLOBALS['TCA'][$table]['columns'][$hideField]['displayCond'] = 'FIELD:type:'.$operator.':'.$variant;						
					} 
					elseif(is_string($GLOBALS['TCA'][$table]['columns'][$hideField]['displayCond']) && $GLOBALS['TCA'][$table]['columns'][$hideField]['displayCond']!="")
					{
						$displayCond = $GLOBALS['TCA'][$table]['columns'][$hideField]['displayCond'];
						$GLOBALS['TCA'][$table]['columns'][$hideField]['displayCond'] = [];
						$GLOBALS['TCA'][$table]['columns'][$hideField]['displayCond']['OR'] = [];
						$GLOBALS['TCA'][$table]['columns'][$hideField]['displayCond']['OR'][] = $displayCond;
						$GLOBALS['TCA'][$table]['columns'][$hideField]['displayCond']['OR'][] = 'type'.$operator.':'.$variant;
						$GLOBALS['TCA'][$table]['columns'][$hideField]['displayCond']['OR'] = array_unique($GLOBALS['TCA'][$table]['columns'][$hideField]['displayCond']['OR']);
					} 
					elseif(is_array($GLOBALS['TCA'][$table]['columns'][$hideField]['displayCond']) && is_array($GLOBALS['TCA'][$table]['columns'][$hideField]['displayCond']['OR']))
					{					
						$GLOBALS['TCA'][$table]['columns'][$hideField]['displayCond']['OR'][] = $displayCond;
						$GLOBALS['TCA'][$table]['columns'][$hideField]['displayCond']['OR'][] = 'FIELD:type:'.$operator.':'.$variant;
						$GLOBALS['TCA'][$table]['columns'][$hideField]['displayCond']['OR'] = array_unique($GLOBALS['TCA'][$table]['columns'][$hideField]['displayCond']['OR']);
					} 
					elseif(is_array($GLOBALS['TCA'][$table]['columns'][$hideField]['displayCond']) && is_array($GLOBALS['TCA'][$table]['columns'][$hideField]['displayCond']['AND']))
					{					
						if(!is_array($GLOBALS['TCA'][$table]['columns'][$hideField]['displayCond']['AND']['OR']))
						{
							$GLOBALS['TCA'][$table]['columns'][$hideField]['displayCond']['AND']['OR'] = [];
						}
						$GLOBALS['TCA'][$table]['columns'][$hideField]['displayCond']['AND']['OR'][] = 'FIELD:type:'.$operator.':'.$variant;
						$GLOBALS['TCA'][$table]['columns'][$hideField]['displayCond']['AND']['OR'] = array_unique($GLOBALS['TCA'][$table]['columns'][$hideField]['displayCond']['AND']['OR']);
					}						
				}
			}
		}		
	}
	
	/**
	 * copy 
	 *
     * @return $uid
     */
	public function copyNewsRecord($tableName, $uid, $pageId, array $recordData = null)
	{
		$commandMap = [
			$tableName => [
				$uid => [
					'copy' => $pageId,
				],
			],
		];
		if ($recordData !== null) {
			$commandMap[$tableName][$uid]['copy'] = [
				'action' => 'paste',
				'target' => $pageId,
				'update' => $recordData,
			];
		}
		$this->createDataHandler();
		$this->dataHandler->start([], $commandMap);
		$this->dataHandler->process_cmdmap();
		return $this->dataHandler->copyMappingArray;
	}
	
	/**
	 * initialize and get DataHandler
	 *
     * @return DataHandler
     */
	protected function createDataHandler()
	{
		$this->dataHandler = GeneralUtility::makeInstance(DataHandler::class);
		$backendUser = $this->getBackendUser();
		if (isset($backendUser->uc['copyLevels']))
		{
			$this->dataHandler->copyTree = $backendUser->uc['copyLevels'];
		}
		return $this->dataHandler;
	}
	
	/**
     * @return DataHandler
     */
	public function getDataHandler()
    {
		return $this->dataHandler;
    }
	
	/**
     * @return LanguageService
     */
    protected function getLanguageService()
    {
        return $GLOBALS['LANG'];
    }

    /**
     * @return \TYPO3\CMS\Core\Authentication\BackendUserAuthentication
     */
    protected function getBackendUser()
    {
        return $GLOBALS['BE_USER'];
    }
	
	/**
	 * @return array
	 */
	public function getStoragePids()
	{
		return $this->storagePids;
	}

	/**
	 * @param array $storagePids storage pids
	 */
	public function setStoragePids($storagePids)
	{
		$this->storagePids = $storagePids;
	}
	
	/**
	 * @return array
	 */
	public function getAllowedFields()
	{
		return $this->allowedFields;
	}

	/**
	 * @param array $allowedFields allowed fields
	 */
	public function setAllowedFields($allowedFields)
	{
		$this->allowedFields = $allowedFields;
	}
}
?>