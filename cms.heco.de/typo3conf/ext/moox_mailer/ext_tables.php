<?php
if (!defined('TYPO3_MODE')) {
	die ('Access denied.');
}

/* ===========================================================================
 	Register BE-Module for Administration
=========================================================================== */

if (TYPO3_MODE === 'BE') {

	// Get the extensions's configuration
	$extConf = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Configuration\ExtensionConfiguration::class)->get($_EXTKEY);

	if(!is_dir($GLOBALS["_SERVER"]["DOCUMENT_ROOT"].'/maildir'))
	{
		$extConf['showJobManagement'] = false;
	}
	// register backend main module
	if($extConf['showMailerManagement'] || $extConf['showJobManagement'] || $extConf['showTemplateManagement'])
	{
		\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerModule(
			'Neulandlotsen.' . $_EXTKEY . '',
			"mooxmailer",
			'',
			'',
			[],
			[
				'access' => 'user,group',
				'icon'   => 'EXT:' . $_EXTKEY . '/Resources/Public/Icons/module_main.svg',
				'labels' => 'LLL:EXT:' . $_EXTKEY . '/Resources/Private/Language/locallang_modmain.xlf',
			]
		);

		// reorder main module
		if (isset($TBE_MODULES['MooxMailerMooxmailer'])){
			$temp_TBE_MODULES = [];
			$mooxMailerMooxmailerCfg = $TBE_MODULES['MooxMailerMooxmailer'];
			unset($TBE_MODULES['MooxMailerMooxmailer']);
			if (isset($TBE_MODULES['moox']))
			{
				$pos = "moox";
			}
			else
			{
				$pos = "web";
			}
			foreach ($TBE_MODULES as $key => $val)
			{
				if ($key == $pos)
				{
					if($pos == 'moox')
					{
						$temp_TBE_MODULES['MooxMailerMooxmailer'] = $mooxMailerMooxmailerCfg;
					}
					$temp_TBE_MODULES[$key] = $val;
					if($pos == 'web')
					{
						$temp_TBE_MODULES['MooxMailerMooxmailer'] = $mooxMailerMooxmailerCfg;
					}

				}
				else
				{
					$temp_TBE_MODULES[$key] = $val;
				}
			}
			$TBE_MODULES = $temp_TBE_MODULES;
		}
	}

	// register backend submodule(s)
	if($extConf['showJobManagement'])
	{
		\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerModule(
			'Neulandlotsen.' . $_EXTKEY,
			'mooxmailer',	// Make module a submodule of 'mooxmailer'
			'JobManagement',	// Submodule key
			'',	// Position
			[
				'Mod1' => 'list,executeTask',
			],
			[
				'access' => 'user,group',
				'icon'   => 'EXT:' . $_EXTKEY . '/Resources/Public/Icons/module_mod1.svg',
				'labels' => 'LLL:EXT:' . $_EXTKEY . '/Resources/Private/Language/locallang_mod1.xlf',
			]
		);
	}
	if($extConf['showTemplateManagement'])
	{
		\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerModule(
			'Neulandlotsen.' . $_EXTKEY,
			'mooxmailer',	// Make module a submodule of 'mooxmailer'
			'TemplateManagement',	// Submodule key
			'',	// Position
			[
				'Mod9' => 'list,refresh,filter,ordering,copy,previewIframe',
			],
			[
				'access' => 'user,group',
				'icon'   => 'EXT:' . $_EXTKEY . '/Resources/Public/Icons/module_mod9.svg',
				'labels' => 'LLL:EXT:' . $_EXTKEY . '/Resources/Private/Language/locallang_mod9.xlf',
			]
		);
	}

	/*
	if(\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::isLoaded('moox_news'))
	{
		$controllerActions = [
			'Mailer' => 'index,showTask,setCurrentTask,showNews,setCurrentUser,previewNews,previewNewsIframe,removeNews,executeTask,previewTask,showLog,generateFolders,getUserListAjax',
			'Template' => 'index,add,edit,delete,previewIframe',
			'Schedule' => 'index,add,edit,delete',
			'Newsletter' => 'index,add,edit,delete,previewIframe,send,testSend,show,stats,statsPdf,showSentNewsletterIframe,addFolderSelect,toggleNewsletterState'
		];
	} 
	else 
	{
		$controllerActions = [	
			'Mailer' => 'index,generateFolders',
			'Newsletter' => 'index,add,edit,delete,previewIframe,send,show,stats,statsPdf,testSend,showSentNewsletterIframe,addFolderSelect,toggleNewsletterState'
		];
	}
	\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerModule(
		'Neulandlotsen.' . $_EXTKEY,
		$mainModuleName,	 // Make module a submodule of 'moox'
		'mailermanagement',	// Submodule key
		'',	// Position
		$controllerActions,
		[
			'access' => 'user,group',
			'icon'   => 'EXT:' . $_EXTKEY . '/Resources/Public/Icons/module-mooxmailer.svg',
			'labels' => 'LLL:EXT:' . $_EXTKEY . '/Resources/Private/Language/locallang_init.xlf',
		]
	);
	*/
} 

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile(
	$_EXTKEY, 
	'Configuration/TypoScript/Default', 
	'MOOX Mailer (Default)'
);
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile(
	$_EXTKEY, 
	'Configuration/TypoScript/Newsletter', 
	'MOOX Mailer (Newsletter)'
);
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile(
	$_EXTKEY, 
	'Configuration/TypoScript/Tracking', 
	'MOOX Mailer (Tracking)'
);

$GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['moox_mailer']['setup'] = unserialize($_EXTCONF);

if (FALSE === (boolean) $GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['moox_mailer']['setup']['disablePageTemplates'])
{
	\FluidTYPO3\Flux\Core::registerProviderExtensionKey('Neulandlotsen.MooxMailer', 'Page');
}

?>