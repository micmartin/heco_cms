<?php
namespace Neulandlotsen\MooxMailer\Tasks;

/**
 * This file is part of the "moox_mailer" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

use TYPO3\CMS\Extbase\Utility\LocalizationUtility;
use TYPO3\CMS\Core\Messaging\FlashMessage;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Database\Connection;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Scheduler\AbstractAdditionalFieldProvider;
use TYPO3\CMS\Scheduler\Task\Enumeration\Action;

class DaemonSendTaskAdditionalFieldProvider extends AbstractAdditionalFieldProvider
{
	/**
	 * This method is used to define new fields for adding or editing a task
	 * In this case, it adds an pid field
	 *
	 * @param array $taskInfo Reference to the array containing the info used in the add/edit form
	 * @param object $task When editing, reference to the current task object. Null when adding.
	 * @param \TYPO3\CMS\Scheduler\Controller\SchedulerModuleController $parentObject Reference to the calling object (Scheduler's BE module)
	 * @return array	Array containing all the information pertaining to the additional fields
	 */
	public function getAdditionalFields(array &$taskInfo, $task, \TYPO3\CMS\Scheduler\Controller\SchedulerModuleController $parentObject)
	{
        $currentSchedulerModuleAction = $parentObject->getCurrentAction();

	    // Initialize extra field value
		if (empty($taskInfo['mailsPerProcess']))
		{
			if ($currentSchedulerModuleAction->equals(Action::EDIT))
			{
				// In case of edit, set to internal value if no data was submitted already
				$taskInfo['mailsPerProcess'] = $task->mailsPerProcess;
			} 
			else 
			{
				// Otherwise set an empty value, as it will not be used anyway
				$taskInfo['mailsPerProcess'] = 500;
			}
		}	

		if (empty($taskInfo['noLog']))
		{
			if ($currentSchedulerModuleAction->equals(Action::EDIT))
			{
				// In case of edit, set to internal value if no data was submitted already
				$taskInfo['noLog'] = $task->noLog;
			} 
			else 
			{
				// Otherwise set an empty value, as it will not be used anyway
				$taskInfo['noLog'] = 0;
			}
		}
		
		$additionalFields = [];						
		
		// Write the code for the field
		$fieldID = 'task_mailsPerProcess';
		$fieldCode = '<input type="text" size=5" name="tx_scheduler[mailsPerProcess]" class="form-control" id="' . $fieldID . '" value="' . $taskInfo['mailsPerProcess'] . '" />';		
		$additionalFields[$fieldID] = [
			'code' => $fieldCode,
			'label' => LocalizationUtility::translate( 'LLL:EXT:moox_mailer/Resources/Private/Language/locallang_scheduler.xlf:tx_mooxmailer_tasks_daemonsendtask.mails_per_process_label', 'moox_mailer' ),
			'cshKey' => '_MOD_tools_txschedulerM1',
			'cshLabel' => $fieldID
		];	
		
		// Write the code for the field
		$fieldID = 'task_noLog';		
		$fieldCode = '<input type="checkbox" name="tx_scheduler[noLog]" id="' . $fieldID . '" value="1" '.($taskInfo['noLog']?'checked="checked" ':'').'/>';	
		$additionalFields[$fieldID] = [
			'code' => $fieldCode,
			'label' => LocalizationUtility::translate( 'LLL:EXT:moox_mailer/Resources/Private/Language/locallang_scheduler.xlf:tx_mooxmailer_tasks_daemonsendtask.no_log_label', 'moox_mailer' ),
			'cshKey' => '_MOD_tools_txschedulerM1',
			'cshLabel' => $fieldID
		];
		
		return $additionalFields;
	}

	/**
	 * This method checks any additional data that is relevant to the specific task
	 * If the task class is not relevant, the method is expected to return TRUE
	 *
	 * @param array $submittedData Reference to the array containing the data submitted by the user
	 * @param \TYPO3\CMS\Scheduler\Controller\SchedulerModuleController $parentObject Reference to the calling object (Scheduler's BE module)
	 * @return bool TRUE if validation was ok (or selected class is not relevant), FALSE otherwise
	 */
	public function validateAdditionalFields(array &$submittedData, \TYPO3\CMS\Scheduler\Controller\SchedulerModuleController $parentObject)
	{	
		$result = TRUE;
		
		$submittedData['mailsPerProcess'] = trim($submittedData['mailsPerProcess']);		
		$submittedData['noLog'] = isset($submittedData['noLog'])?1:0;
		
		if(!preg_match('/^([\d])+$/',$submittedData['mailsPerProcess']) || (int)$submittedData['mailsPerProcess']<1)
		{
			$this->addMessage($GLOBALS['LANG']->sL('LLL:EXT:moox_mailer/Resources/Private/Language/locallang_scheduler.xlf:tx_mooxmailer_tasks_daemonsendtask.mails_per_process_error').": ".$submittedData['mailsPerProcess'], FlashMessage::ERROR);
			$result = FALSE;
		}
		
		if ($submittedData['uid']==0)
		{			
			$queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)
				->getQueryBuilderForTable('tx_scheduler_task');

			$res = $queryBuilder
				->select('*')
				->from('tx_scheduler_task')
				->execute();

			while($task = $res->fetch())
			{				
				$daemontask = unserialize($task['serialized_task_object']);
				if($daemontask instanceof \Neulandlotsen\MooxMailer\Tasks\DaemonSendTask)
				{
					$this->addMessage($GLOBALS['LANG']->sL('LLL:EXT:moox_mailer/Resources/Private/Language/locallang_scheduler.xlf:tx_mooxmailer_tasks_daemonsendtask.only_one_error'), FlashMessage::ERROR);
					$result = FALSE;
					break;
				}
			}
		}
		
		return $result;
	}

	/**
	 * This method is used to save any additional input into the current task object
	 * if the task class matches
	 *
	 * @param array $submittedData Array containing the data submitted by the user
	 * @param \TYPO3\CMS\Scheduler\Task\AbstractTask $task Reference to the current task object
	 * @return void
	 */
	public function saveAdditionalFields(array $submittedData, \TYPO3\CMS\Scheduler\Task\AbstractTask $task)
	{		
		$task->mailsPerProcess = $submittedData['mailsPerProcess'];		
		$task->noLog = $submittedData['noLog'];	
	}	
}

?>