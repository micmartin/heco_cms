<?php
namespace Neulandlotsen\MooxMailer\ViewHelpers;

/**
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */

use TYPO3\CMS\Backend\Utility\BackendUtility;
use TYPO3\CMS\Frontend\Page\PageRepository;
use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper;

class GetPageInfoViewHelper extends AbstractViewHelper
{
	/**
	 * @var PageRepository
	 */
	protected $pageRepository;

    /**
     * @param PageRepository $pageRepository
     */
    public function injectPageRepository(PageRepository $pageRepository)
    {
        $this->pageRepository = $pageRepository;
    }

    public function initializeArguments()
    {
        parent::initializeArguments();
        $this->registerArgument('uid', 'int', 'uid', true);
        $this->registerArgument('field', 'string', 'field', true);
    }
	
	/**
	 * @return mixed
	 */
	public function render()
	{
        $uid = $this->arguments['uid'];
        $field = $this->arguments['field'];
        $return = "";

        if($uid && $field)
        {
            $page = BackendUtility::getRecord('pages',$uid,'*');
            if(isset($page[$field]))
            {
                $return = $page[$field];
            }
        }

        return $return;
	}
}
