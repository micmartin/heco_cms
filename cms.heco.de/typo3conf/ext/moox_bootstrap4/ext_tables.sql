#
# Table structure for table 'tt_content'
#
CREATE TABLE tt_content (
	tx_mooxbootstrap4_visibility tinytext,
	tx_mooxbootstrap4_container varchar(100) NOT NULL DEFAULT '',
	tx_mooxbootstrap4_card varchar(100) NOT NULL DEFAULT '',
	tx_mooxbootstrap4_card_include_header tinyint(1) NOT NULL,
	tx_mooxbootstrap4_shadow varchar(100) NOT NULL DEFAULT '',
);
