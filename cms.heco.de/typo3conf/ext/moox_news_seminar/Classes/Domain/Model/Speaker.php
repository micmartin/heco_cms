<?php
namespace Neulandlotsen\MooxNewsSeminar\Domain\Model;

/**
 * This file is part of the "moox_news_seminar" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

use TYPO3\CMS\Extbase\DomainObject\AbstractEntity;
use TYPO3\CMS\Extbase\Persistence\ObjectStorage;

class Speaker extends AbstractEntity {
	
	/**
	 * @var int
	 */
    protected $uid;
	
	/**
	 * @var int
	 */
    protected $pid;
	
	/**
	 * @var int
	 */
    protected $tstamp;
	
	/**
	 * @var bool
	 */
    protected $hidden;		
	
	/**
	 * @var int
	 */
    protected $crdate;
	
	/**
	 * @var string
	 */
	protected $title;	
	
	/**
	 * @var string
	 */
	protected $position;
	
	/**
	 * @var string
	 */
	protected $description;
	
	/**
	 * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\CMS\Extbase\Domain\Model\FileReference>
	 * @TYPO3\CMS\Extbase\Annotation\ORM\Lazy
	 */
	protected $image;
	
	/**
	 * @var string
	 */
    protected $autoName;
	
	/**
	 * initialize object
	 */
	public function initializeObject()
	{
		//Do not remove the next line: It would break the functionality
		$this->initStorageObjects();
	}
  
	/**
	 * Initializes all ObjectStorage properties
	 * Do not modify this method!
	 * It will be rewritten on each save in the extension builder
	 * You may modify the constructor of this class instead
	 */
	protected function initStorageObjects()
	{
		$this->image = new ObjectStorage();
	}
	
	/**
     * @return int $uid uid
     */
    public function getUid()
	{
       return $this->uid;
    }
     
    /**
     * @param int $uid uid
	 */
    public function setUid($uid)
	{
        $this->uid = $uid;
    }
	
	/**
     * @return int $pid pid
     */
    public function getPid()
	{
       return $this->pid;
    }
     
    /**
     * @param int $pid pid
	 */
    public function setPid($pid)
	{
        $this->pid = $pid;
    }
	
	/**
     * @return int $tstamp tstamp
     */
    public function getTstamp()
	{
       return $this->tstamp;
    }
     
    /**
     * @param int $tstamp tstamp
	 */
    public function setTstamp($tstamp)
	{
        $this->tstamp = $tstamp;
    }		
	
	/**
     * @return bool $hidden hidden
     */
    public function getHidden()
	{
       return $this->hidden;
    }
     
    /**
     * @param bool $hidden hidden
	 */
    public function setHidden($hidden)
	{
        $this->hidden = $hidden;
    }		
     
	/**
     * @return int $crdate crdate
     */
    public function getCrdate()
	{
       return $this->crdate;
    }
     
    /**
     * @param int $crdate crdate
	 */
    public function setCrdate($crdate)
	{
        $this->crdate = $crdate;
    }
	
	/**
	 * @return int
	 */
	public function getYearOfCrdate()
	{
		return $this->getCrdate()->format('Y');
	}

	/**
	 * @return int
	 */
	public function getMonthOfCrdate()
	{
		return $this->getCrdate()->format('m');
	}

	/**
	 * @return int
	 */
	public function getDayOfCrdate()
	{
		return (int)$this->crdate->format('d');
	}
	
	/**
	 * @return string
	 */
	public function getTitle()
	{
		return $this->title;
	}

	/**
	 * @param string $title
	 */
	public function setTitle($title)
	{
		$this->title = $title;
	}
	
	/**
	 * @return string
	 */
	public function getPosition()
	{
		return $this->position;
	}

	/**
	 * @param string $position
	 */
	public function setPosition($position)
	{
		$this->position = $position;
	}
	
	/**
	 * @return string
	 */
	public function getDescription()
	{
		return $this->description;
	}

	/**
	 * @param string $description
	 */
	public function setDescription($description)
	{
		$this->description = $description;
	}
	
	/**
	 * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage $image
	 */
	public function setImage($image)
	{
		$this->image = $image;
	}
	 
	/**
	 * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage
	 */
	public function getImage()
	{
		return $this->image;
	}

	/**
     * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $image
     *
     */
    public function addImage(\TYPO3\CMS\Extbase\Domain\Model\FileReference $image)
	{
        $this->image->attach($image);
    }	
	
	/**
     * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $image
     *
     */
    public function removeImage(\TYPO3\CMS\Extbase\Domain\Model\FileReference $image)
	{
        $this->image->detach($image);
    }
	
	/**
	 * @return string $autoName
	 */
	public function getAutoName()
	{
	   $autoName = $this->title;
	   return $autoName;
	}
}