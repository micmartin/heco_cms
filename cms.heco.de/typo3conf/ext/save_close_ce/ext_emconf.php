<?php

/***************************************************************
 * Extension Manager/Repository config file for ext "save_close_ce".
 *
 * Auto generated 17-07-2019 11:48
 *
 * Manual updates:
 * Only the data in the array - everything else is removed by next
 * writing. "version" and "dependencies" must not be touched!
 ***************************************************************/

$EM_CONF[$_EXTKEY] = array (
  'title' => 'Save and close button',
  'description' => 'Adds save and close button to all the content elements',
  'category' => 'be',
  'author' => 'Goran Medakovic',
  'author_email' => 'avion.bg@gmail.com',
  'state' => 'stable',
  'uploadfolder' => false,
  'createDirs' => '',
  'clearCacheOnLoad' => 0,
  'version' => '1.0.3',
  'constraints' => 
  array (
    'depends' => 
    array (
      'typo3' => '9.5.0-9.6.0',
    ),
    'conflicts' => 
    array (
    ),
    'suggests' => 
    array (
    ),
  ),
  'clearcacheonload' => false,
  'author_company' => NULL,
);

