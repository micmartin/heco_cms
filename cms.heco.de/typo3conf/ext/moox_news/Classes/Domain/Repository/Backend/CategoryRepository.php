<?php
namespace Neulandlotsen\MooxNews\Domain\Repository\Backend;

/**
 * This file is part of the "moox_news" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

use TYPO3\CMS\Extbase\Persistence\QueryInterface; 
use Neulandlotsen\MooxNews\Service\CategoryService;
use Neulandlotsen\MooxNews\Domain\Repository\MooxRepository;

class CategoryRepository extends MooxRepository
{	
	protected $defaultOrderings = ['title' => QueryInterface::ORDER_ASCENDING];
	
	/**
	 * Returns a constraint array created by a given filter array
	 *
	 * @param \TYPO3\CMS\Extbase\Persistence\QueryInterface $query
	 * @param array $filter
	 * @param array $constraints	
	 * @return \TYPO3\CMS\Extbase\Persistence\Generic\Qom\ConstraintInterface|null
	 */
	protected function createFilterConstraints(\TYPO3\CMS\Extbase\Persistence\QueryInterface $query,$filter = NULL,$constraints = NULL)
	{				
		if(is_null($constraints))
		{			
			$constraints = [];			
		}
		
		if(isset($filter['parent']) && is_int($filter['parent']))
		{			
			$constraints[] = $query->equals('parentcategory', $filter['parent']);			
		}
		
		if(isset($filter['hidden']) && in_array($filter['hidden'],[0,1]))
		{			
			$constraints[] = $query->equals('hidden', $filter['hidden']);			
		}
		
		$searchFields = ['title'];
		if(isset($filter['query']) && $filter['query']!="")
		{			
			$searchConstraints = [];
			foreach($searchFields AS $searchField)
			{
				$searchConstraints[] =  $query->like($searchField, "%".$filter['query']."%");
			}
			
			if(count($searchConstraints)>0)
			{
				if(count($searchConstraints)==1)
				{
					$constraints[] = $searchConstraints[0];
				} 
				else 
				{
					$constraints[] = $query->logicalOr($searchConstraints);
				}
			}			
		}
		
		if(count($constraints)<1)
		{			
			$constraints = NULL;			
		}
		
		return $constraints;
	}
	
	/**
     * Find categories by a given pid
     *
     * @param int $pid pid
	 * @param boolean $respectEnableFields if set to false, hidden records are shown
     * @return QueryInterface
     */
    public function findByPid($pid,$respectEnableFields = TRUE)
    {
        $query = $this->createQuery();
        $query->getQuerySettings()->setRespectStoragePage(false);
		$query->getQuerySettings()->setIgnoreEnableFields(!$respectEnableFields);
		$query->matching(
			$query->equals('pid', (int)$pid)
		);
		$query->setOrderings(
			[
				'title' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING
			]
		);
		return $query->execute();
    }
	
	/**
     * Find categories by a given parent uid
     *
     * @param int $parentUid parent uid
	 * @param boolean $respectEnableFields if set to false, hidden records are shown
     * @return QueryInterface
     */
    public function findByParent($parentUid,$respectEnableFields = TRUE)
    {
		if($parentUid>0)
		{
			$query = $this->createQuery();
			$query->getQuerySettings()->setRespectStoragePage(false);
			$query->getQuerySettings()->setIgnoreEnableFields(!$respectEnableFields);
			$query->matching(
				$query->logicalAnd(
					$query->equals('deleted', 0),
					$query->equals('parentcategory', $parentUid)
				)
			);		
			return $query->execute();
		}
    }
}
?>