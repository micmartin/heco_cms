define([
	'jquery',
	'TYPO3/CMS/MooxCore/Backend/Module'], function ($, MooxModule) {

	var MooxNewsSeminarMod2 = {};

	MooxNewsSeminarMod2.init = function () {

		MooxModule.initClearButtons();
		
		$(document).on('submit', '#moox-news-seminar-module-filter form.use-ajax', function(event) {
		
			event.preventDefault();
			
			var url = $(this).attr("action");		
			var wrapper = MooxNewsSeminarMod2.getWrapper($(this));
			var form = "#" + $(this).attr("id");
			
			var data = { };
				
			$(form + ' input.is-grouped-filter').each(function() {
				data[$(this).attr('name')] = $(this).val();		
			});
			
			$(form + ' select.is-grouped-filter').each(function() {
				data[$(this).attr('name')] = $(this).val();		
			});
			
			$.ajax(url, {
				method: "POST",
				cache: false,
				data: data,
				success: function(data) {				
					/*mooxNewsSeminarMod2Refresh(wrapper);*/
					body = $("#"+wrapper+".use-ajax #moox-news-seminar-module-body").first();								
					window.location = body.data('url');
				},
				error: function(data) {				
					
				},
				beforeSend: function(){
					$("body").append("<div id='moox-news-seminar-module-overlay'><img src='../typo3conf/ext/moox_news_seminar/Resources/Public/Images/Backend/loading.gif' /></div>");
				},
				complete: function(){
					/*$("#moox-news-seminar-module-overlay").remove();*/
				},
			});
			
			return false;
		});
	
		$(document).on('click', '#moox-news-seminar-module-filter a.page-selector', function(event) {
			event.preventDefault();
			$('#moox-news-seminar-module-filter input.page-selector').val($(this).data('uid'));
			$('#moox-news-seminar-module-filter button.page-selector .current').html($(this).html());		
			if($(this).hasClass('auto-submit')){
				$('#moox-news-seminar-module-filter form').submit();
			}
		});
	
		$(document).on('click', '#moox-news-seminar-module-body a.show-detail', function(event) {
			event.preventDefault();
			
			visible = false;
			if($('#moox-news-seminar-module-body #speaker-detail-'+$(this).data('uid')).is(":visible")){
				visible = true;			
			} 
			
			$('#moox-news-seminar-module-body .speaker-detail').hide();
			if(!visible && $('#moox-news-seminar-module-body #speaker-detail-'+$(this).data('uid'))){
				$('#moox-news-seminar-module-body #speaker-detail-'+$(this).data('uid')).show();
			}
			$('#moox-news-seminar-module-body a.show-detail .icon-up').hide();
			$('#moox-news-seminar-module-body a.show-detail .icon-down').show();
			if(visible){
				$(this).find('.icon-down').show();
				$(this).find('.icon-up').hide();
			} else {
				$(this).find('.icon-down').hide();
				$(this).find('.icon-up').show();
				
			}
		});
	
		$(document).on('change', '#moox-news-seminar-module-filter select.auto-submit', function(event) {
			event.preventDefault();
			$('#moox-news-seminar-module-filter form').submit();		
		});
		
		$(document).on('change', '#moox-news-seminar-module-filter input.auto-submit', function(event) {
			event.preventDefault();
			$('#moox-news-seminar-module-filter form').submit();		
		});
		
		$(document).on('click', '#moox-news-seminar-module-filter a.filter-reset', function(event) {
			event.preventDefault();
			
			var url = $(this).attr("href");
			var wrapper = MooxNewsSeminarMod2.getWrapper($(this));
			
			var data = { };
			data['tx_mooxnewsseminar_mooxnewsmooxdb_mooxnewsseminarspeakermanagement[ajax]'] = 1;
			
			$.ajax(url, {
				method: "POST",
				cache: false,
				data: data,
				success: function(data) {				
					body = $("#"+wrapper+".use-ajax #moox-news-seminar-module-body").first();								
					window.location = body.data('url');
				},
				error: function(data) {				
					
				},
				beforeSend: function(){
					$("body").append("<div id='moox-news-seminar-module-overlay'><img src='../typo3conf/ext/moox_news_seminar/Resources/Public/Images/Backend/loading.gif' /></div>");
				},
				complete: function(){
					/*$("#moox-news-seminar-module-overlay").remove();*/
				},
			});
			
			return false;		
		});
		
		$(document).on('click', '.moox-news-seminar-module.use-ajax #moox-news-seminar-module-body a.change-ordering', function(event) {
			event.preventDefault();
			
			var url = $(this).attr("href");
			var wrapper = MooxNewsSeminarMod2.getWrapper($(this));
			
			var data = { };
			data['tx_mooxnewsseminar_mooxnewsmooxdb_mooxnewsseminarspeakermanagement[ajax]'] = 1;
			
			$.ajax(url, {
				method: "POST",
				cache: false,
				data: data,
				success: function(data) {				
					body = $("#"+wrapper+".use-ajax #moox-news-seminar-module-body").first();								
					window.location = body.data('url');
				},
				error: function(data) {				
					
				},
				beforeSend: function(){
					$("body").append("<div id='moox-news-seminar-module-overlay'><img src='../typo3conf/ext/moox_news_seminar/Resources/Public/Images/Backend/loading.gif' /></div>");
				},
				complete: function(){
					/*$("#moox-news-seminar-module-overlay").remove();*/
				},
			});
			
			return false;		
		});
	};

	MooxNewsSeminarMod2.refresh = function (wrapper)
	{			
		var body = $("#"+wrapper+".use-ajax #moox-news-seminar-module-body").first();		
		
		var data = { };
		
		data['tx_mooxnewsseminar_mooxnewsmooxdb_mooxnewsseminarspeakermanagement[ajax]'] = 1;
		
		$.ajax(body.data("url"), {
			method: "POST",
			data: data,
			success: function(data) {			
				body.html($(data));				
			},
			beforeSend: function(){
				$("body").append("<div id='moox-news-seminar-module-overlay'><img src='../typo3conf/ext/moox_news_seminar/Resources/Public/Images/Backend/loading.gif' /></div>");
			},
			complete: function(){
				$("#moox-news-seminar-module-overlay").remove();
			},
		});
	};
	
	MooxNewsSeminarMod2.getWrapper = function (object) {
		return object.parents('.moox-news-seminar-module-mod2').attr("id");
	};

	MooxNewsSeminarMod2.init();

	return MooxNewsSeminarMod2;
});