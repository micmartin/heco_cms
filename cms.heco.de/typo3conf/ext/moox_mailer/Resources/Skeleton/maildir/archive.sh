#!/bin/bash

#settings
ARCHIVE_OLDER_THAN_X_DAYS=7
DELETE_OLDER_THAN_X_DAYS=31
NOTHING_TO_DO=1;

clear

WORKDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

cd $WORKDIR

for AFILE in $(find ./logs/ -type f -name '__*.log' -mtime +$ARCHIVE_OLDER_THAN_X_DAYS); do
	DATETIME=`date +"%Y-%m-%d %H:%M:%S"`
	AFILENAMEBASE=`basename $AFILE`;
	AFILENAME=${AFILENAMEBASE%.log}
	AFILENAMENOPREFIX=${AFILENAME#__}	
	cp logs/$AFILENAMEBASE mails/$AFILENAMEBASE;
	cd mails
	tar cfvj ./../archive/$AFILENAMENOPREFIX.tar.bz2 ./$AFILENAME*.* #>> /dev/null
	rm ./$AFILENAME*.*
	cd ..
	rm logs/$AFILENAMEBASE
	clear
	echo "["$DATETIME"] CRON: Job "$AFILENAMENOPREFIX" archived after "$ARCHIVE_OLDER_THAN_X_DAYS" days..."
	echo " ";
	echo "["$DATETIME"] CRON: Job "$AFILENAMENOPREFIX" archived after "$ARCHIVE_OLDER_THAN_X_DAYS" days..." >> logs/archive.log
	echo "["$DATETIME"] CRON: Job "$AFILENAMENOPREFIX" archived after "$ARCHIVE_OLDER_THAN_X_DAYS" days..." >> logs/global.log
	NOTHING_TO_DO=0
done

echo " ";

for DFILE in $(find ./archive/ -type f -name '*.tar.bz2' -mtime +$DELETE_OLDER_THAN_X_DAYS); do
	DATETIME=`date +"%Y-%m-%d %H:%M:%S"`
	DFILENAMEBASE=`basename $DFILE`;
	DFILENAME=${DFILENAMEBASE%.tar.bz2}
	rm ./archive/$DFILENAMEBASE;
	echo "["$DATETIME"] CRON: Archive file of job "$DFILENAME" deleted after "$DELETE_OLDER_THAN_X_DAYS" days..."
	echo " ";
	echo "["$DATETIME"] CRON: Archive file of job "$DFILENAME" deleted after "$DELETE_OLDER_THAN_X_DAYS" days..." >> logs/archive.log
	echo "["$DATETIME"] CRON: Archive file of job "$DFILENAME" deleted after "$DELETE_OLDER_THAN_X_DAYS" days..." >> logs/global.log
	NOTHING_TO_DO=0
done

if [ $NOTHING_TO_DO -eq 1 ]
then
	DATETIME=`date +"%Y-%m-%d %H:%M:%S"`
	echo "["$DATETIME"] CRON: Nothing to do, nothing to archive, nothing to delete..."
	echo " ";
fi
