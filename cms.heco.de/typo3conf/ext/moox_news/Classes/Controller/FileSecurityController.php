<?php
namespace Neulandlotsen\MooxNews\Controller;

/**
 * This file is part of the "moox_news" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\TypoScript\ExtendedTemplateService;
	 
class FileSecurityController extends \Neulandlotsen\MooxCore\Controller\FileSecurityController
{
	/**
	 * @var string
	 */
	public static $mooxNewsFileRoot = 'fileadmin/user_upload/moox_news/';

	/**
	 * @param array $data
	 */
	public function loadFileTxMooxnewsDomainModelNews($data = [])
	{
		$fileAccess = false;

		$news = $this->newsRepository->findByUid($data['uid'],FALSE);

		if(is_a($news, 'Neulandlotsen\MooxNews\Domain\Model\News'))
		{
			if(!($news->getHidden() || $news->getStarttime()>time() || ($news->getEndtime()>0 && $news->getEndtime()>time())))
			{
				if($news->getFeGroup()!=0)
				{
					if(is_array($GLOBALS["TSFE"]->fe_user->user))
					{
						if($GLOBALS['TSFE']->fe_user->user['usergroup']!=0)
						{
							$hasNewsAccess = false;
							$usergroups = explode(",",$GLOBALS['TSFE']->fe_user->user['usergroup']);
							$newsusergroups = explode(",",$news->getFeGroup());
							foreach($newsusergroups AS $newsusergroup)
							{
								if(in_array($newsusergroup,$usergroups))
								{
									$hasNewsAccess 	= true;
									break;
								}
							}
							if($hasNewsAccess)
							{
								$fileAccess = true;
							}
						}
					}
					else
					{
						$loginurl = str_replace("redirecturi",urlencode(substr(urldecode($_SERVER['REQUEST_URI']),1)),$data['loginurl']);
						if($loginurl!="")
						{
							$this->redirectToURI($data['baseUrl'].$loginurl);
						}
					}
				}
				else
				{
					$fileAccess = true;
				}
			}
		}


		$filepath = $GLOBALS["_SERVER"]["DOCUMENT_ROOT"]."/".(($data['prefix']!="")?$data['prefix']."/": \Neulandlotsen\MooxNews\Controller\FileSecurityController::$mooxNewsFileRoot).$data['file'];
		$filearray = explode("/",$data['file']);
		$filearray = array_reverse($filearray);
		$filename = $filearray[0];

		if(!file_exists($filepath))
		{
			$fileAccess = false;
		}

		if($fileAccess)
		{
			$fileInfo = $this->getFileInfo($filename);
			$fileSize = (int)filesize($filepath);
			//header("Content-type: application/octet-stream");
			//header("Content-Length: " .(int)(filesize($filepath)) );
			//header('Content-Disposition: attachment; filename=\"".(($filename)?$filename:"unknown")."\";");
			//header('Content-type: application/'.$fileInfo['extension']);
			//readfile($filepath);
			$file=fopen($filepath,'r');
			set_time_limit(0);
			if ($file)
			{
				header("Pragma: public");
				header("Expires: 0");
				header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
				header("Cache-Control: private",false);
				//header("Content-Type: application/force-download");
				//header("Content-Type: application/octet-stream");
				//header("Content-Type: application/download");
				header("Content-Type: application/pdf");
				header("Content-Disposition: attachment; filename=\"".(($filename)?$filename:"unknown")."\";");
				header("Content-Description: File Transfer");
				header("Content-Transfer-Encoding: binary");
				header('Content-Length: '.$fileSize );
				flush();
				while (!feof($file)) {
					print(fread($file,4096));
					flush();
				}
				fclose($file);
				die();
			}
			else
			{
				$this->redirectToURI($data['baseUrl']);
			}

		}
		else
		{
			$this->redirectToURI($data['baseUrl']);
		}
	}
}
?>