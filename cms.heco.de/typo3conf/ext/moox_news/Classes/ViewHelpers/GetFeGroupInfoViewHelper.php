<?php
namespace Neulandlotsen\MooxNews\ViewHelpers;

/**
 * This file is part of the "moox_news" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

use TYPO3\CMS\Extbase\Domain\Repository\FrontendUserGroupRepository;
use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper;

class GetFeGroupInfoViewHelper extends AbstractViewHelper
{	
	//ToDo Funktion überprüfen 2019-05-06

    /**
	 * @var FrontendUserGroupRepository
	 */
	protected $frontendUserGroupRepository;

    /**
     * @param FrontendUserGroupRepository $frontendUserGroupRepository
     */
    public function injectFrontendUserGroupRepository(FrontendUserGroupRepository $frontendUserGroupRepository)
    {
        $this->frontendUserGroupRepository = $frontendUserGroupRepository;
    }

    public function initializeArguments()
    {
        parent::initializeArguments();
        $this->registerArgument('uid', 'int', 'uid', true);
        $this->registerArgument('field', 'string', 'field', true);
    }
	
	/**
	 * @return string
	 */
	public function render()
	{
        $uid = $this->arguments['uid'];
        $field = $this->arguments['field'];
		$group = "";
		
		if($uid && $field)
		{
			$group = $this->frontendUserGroupRepository->findByUid($uid);
			if(is_object($group))
			{
				$group = $group->getTitle();
			}
		}
		
		return $group;
	}
}
