<?php
namespace Neulandlotsen\MooxNews\Controller;

    /**
     * This file is part of the TYPO3 CMS project.
     *
     * It is free software; you can redistribute it and/or modify it under
     * the terms of the GNU General Public License, either version 2
     * of the License, or any later version.
     *
     * For the full copyright and license information, please read the
     * LICENSE.txt file that was distributed with this source code.
     *
     * The TYPO3 project - inspiring people to share!
     */

use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Utility\LocalizationUtility;	 
	 
class AjaxController extends NewsController {	
	
	/**
	 * default action
	 *
	 * @param array request
	 * @return void
	 */
	public function defaultAction($request = array())
	{
		exit($this->actionMethodName);
	}
		
	/**
	 * get list code
	 *
	 * @param array $overwriteDemand
	 * @param int $page
	 * @param string $settings
	 * @return void
	 */
	public function fetchListAction(array $overwriteDemand = NULL,$page = 0,$settings = "")
	{		
		if($settings!=""){
		
			$settings = json_decode($settings,true);			
			
			if(!is_array($this->settings)){
				$this->settings = array();
			}
			
			$this->settings = array_merge($this->settings,$settings);
			
			if($page>1){
				$overwriteDemand['limit'] = $this->settings['list']['paginate']['itemsPerPage'];
				$overwriteDemand['offset'] = ($overwriteDemand['limit']*($page-1));				
			} elseif($page==1){
				$overwriteDemand['limit'] = $this->settings['list']['paginate']['itemsPerPage'];
				$overwriteDemand['offset'] = 0;
			}
			
			$this->isAjaxRequest = true;
			
			parent::listAction($overwriteDemand);
		
		} else {
			exit("not allowed");
		}
	}
	
	/**
	 * get code for single news element
	 *
	 * @param \Neulandlotsen\MooxNews\Domain\Model\News $news news item
	 * @param string $settings
	 * @return void
	 */
	public function fetchDetailAction(\Neulandlotsen\MooxNews\Domain\Model\News $news = NULL,$settings = "") 
	{				
		if(!is_null($news) && $settings!=""){
		
			$settings = json_decode($settings,true);			
			
			if(!is_array($this->settings)){
				$this->settings = array();
			}
			
			$this->settings = array_merge($this->settings,$settings);
			
			parent::detailAction($news);
		
		} else {
			exit("not allowed");
		}
	}
	
	/**
	 * get filter code
	 *
	 * @param string $filter
	 * @param array $dependDemand	
	 * @param string $settings
	 * @return void
	 */
	public function fetchFilterAction($filter = "",array $dependDemand = NULL,$settings = "")
	{		
		$this->isAjaxRequest = true;
		
		if($filter!="" && $settings!=""){
		
			$dependFields = array();
			
			if(count($dependDemand)){
				foreach($dependDemand AS $dependField => $dependFieldValue){
					$dependFields[GeneralUtility::camelCaseToLowerCaseUnderscored($dependField)] = $dependFieldValue;
				}
			}
			
			$settings = json_decode($settings,true);			
			
			if(!is_array($this->settings)){
				$this->settings = array();
			}
			
			$this->settings = array_merge($this->settings,$settings);
			
			$demand = NewsController::createDemandObjectFromSettings($this->settings);			
			$fieldname = GeneralUtility::camelCaseToLowerCaseUnderscored($filter);			
			$field = $GLOBALS['TCA']['tx_mooxnews_domain_model_news']['columns'][$fieldname];			
			$items = array();
			
			if($field['config']['type']=='select'){							
				if($field['config']['foreign_table']!=""){
					$itemsFromDb = $this->newsRepository->findExtendedFilterItems($fieldname,$demand->getStoragePage(),$field['config']['foreign_table'],$field['config']['foreign_table_where'],$dependFields);					
					foreach($itemsFromDb AS $item){
						$items[$item['uid']] = $item['title'];
					}
				}
			} else {
				$valuesFromDb = $this->newsRepository->findExtendedFilterUniqueValues($fieldname,$demand->getStoragePage(),$dependFields);
			}
			
			if($field['config']['type']=='select'){
				if(count($items)){
					if(!isset($items['all'])){
						if($field['addToMooxNewsFrontendFilter']['selectAllLabel']!=""){
							$selectAllLabel = LocalizationUtility::translate($field['addToMooxNewsFrontendFilter']['selectAllLabel'], $field['extkey']);
						} else {
							$selectAllLabel = "Alle";
						}
						$itemsTmp 		= $items;
						$items 			= array();
						$items["all"] 	= $selectAllLabel;
						foreach($itemsTmp AS $key => $value){
							$items[$key] = $value;
						}
						unset($itemsTmp);
					}
				}
				$this->view->assign('options', $items);
			} else {
				
				if($field['addToMooxNewsFrontendFilter']['selectAllLabel']!=""){
					$values['all'] = LocalizationUtility::translate($field['addToMooxNewsFrontendFilter']['selectAllLabel'], $field['extkey']);
				} else {
					$values['all'] = "Alle";
				}
				
				if(count($valuesFromDb)){
				
					$valueCnt 	= 0;
								
					foreach($valuesFromDb AS $value){
						if($value=="empty"){
							if($field['addToMooxNewsFrontendFilter']['selectEmptyLabel']!=""){
								$valueLabel = LocalizationUtility::translate($field['addToMooxNewsFrontendFilter']['selectEmptyLabel'], $field['extkey']);
								$values[$value] = $valueLabel;
							} else {
								$valueLabel = "Ohne Wert";
							}
						} else {
							$values[$value] = $value;
							$valueCnt++;
						}				
					}
					$this->view->assign('options', $values);					
				} 
			}
			
		} else {
			exit("not allowed");
		}
	}
}