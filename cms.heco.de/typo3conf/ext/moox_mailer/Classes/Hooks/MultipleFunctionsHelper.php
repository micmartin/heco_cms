<?php
namespace Neulandlotsen\MooxMailer\Hooks;

/**
 * This file is part of the "moox_news" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

use TYPO3\CMS\Core\Utility\GeneralUtility; 
 
class MultipleFunctionsHelper 
{	
	/**
	 * @param \Neulandlotsen\MooxNews\Domain\Model\News $item
	 * @param \Neulandlotsen\MooxNews\Domain\Repository\Backend\NewsRepository $repository
	 * @return bool persist?
	 */
	public function multiple_setFrequencyToCumulated($item = NULL, $repository = NULL)
	{
		if(is_object($item) && is_object($repository))
		{
			$item->setMailerFrequency(2);							
			$repository->update($item);								
			return true;
		}
	}	
	
	/**
	 * @param \Neulandlotsen\MooxNews\Domain\Model\News $item
	 * @param \Neulandlotsen\MooxNews\Domain\Repository\Backend\NewsRepository $repository
	 * @return bool persist?
	 */
	public function multiple_setFrequencyToDirect($item = NULL, $repository = NULL)
	{
		if(is_object($item) && is_object($repository))
		{
			$item->setMailerFrequency(1);							
			$repository->update($item);								
			return true;
		}
	}
	
	/**
	 * @param \Neulandlotsen\MooxNews\Domain\Model\News $item
	 * @param \Neulandlotsen\MooxNews\Domain\Repository\Backend\NewsRepository $repository
	 * @return bool persist?
	 */
	public function multiple_setFrequencyToNone($item = NULL, $repository = NULL)
	{
		if(is_object($item) && is_object($repository))
		{
			$item->setMailerFrequency(0);							
			$repository->update($item);								
			return true;
		}
	}
}