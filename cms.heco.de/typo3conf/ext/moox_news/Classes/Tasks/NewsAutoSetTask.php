<?php
namespace Neulandlotsen\MooxNews\Tasks;

/**
 * This file is part of the "moox_news" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Database\Query\Restriction\DeletedRestriction;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Utility\LocalizationUtility;
	 
class NewsAutoSetTask extends \TYPO3\CMS\Scheduler\Task\AbstractTask
{			
	/**
	 * @var string
	 */
	public $newsPids;
	
	/**
	 * @var bool
	 */
	public $setAccess;
	
	/**
	 * @var bool
	 */
	public $ignore;
	
	/**
	 * @var bool
	 */
	public $setSinglePid;
	
	/**
	 * Prepares the mailer job.
	 *
	 * @return	bool	Returns TRUE on success, FALSE if no items were indexed or none were found.
	 * @see	typo3/sysext/scheduler/tx_scheduler_Task#execute()
	 */
	public function execute()
	{											
		$executionSucceeded = true;
		
		$objectManager 	= GeneralUtility::makeInstance('TYPO3\CMS\Extbase\Object\ObjectManager');		
		$newsRepository = $objectManager->get('Neulandlotsen\MooxNews\Domain\Repository\NewsRepository');
		
		$newsItems = $newsRepository->findNewsByPidList(explode(",",$this->newsPids));
		
		$doPersistAll = false;
		
		$categories = [];
		
		// process query
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable('tx_mooxnews_domain_model_news');
        $queryBuilder
            ->getRestrictions()
            ->removeAll()
            ->add(GeneralUtility::makeInstance(DeletedRestriction::class));

        $query = $queryBuilder
            ->select('*')
            ->from('tx_mooxnews_domain_model_news')
            ->where(
                $queryBuilder->expr()->in('pid', $queryBuilder->createNamedParameter($this->newsPids, \PDO::PARAM_STR))
            )
            ->execute();
		
		while( $news = $query->fetch())
		{
            $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable('sys_category_record_mm');
            $queryBuilder
                ->getRestrictions()
                ->removeAll()
                ->add(GeneralUtility::makeInstance(DeletedRestriction::class));

            $catRefRes = $queryBuilder
                ->select('uid_local')
                ->from('sys_category_record_mm')
                ->where(
                    $queryBuilder->expr()->andx(
                        $queryBuilder->expr()->eq('tablenames', $queryBuilder->createNamedParameter('tx_mooxnews_domain_model_news', \PDO::PARAM_STR)),
                        $queryBuilder->expr()->eq('uid_foreign', $queryBuilder->createNamedParameter($news['uid'], \PDO::PARAM_INT))
                    )
                )
                ->orderBy('sorting_foreign', 'ASC')
                ->setMaxResults(1)
                ->execute();
			$catRef = $catRefRes->fetch();
			$catUid = $catRef['uid_local'];
			if($catUid>0)
			{
				if(!isset($categories[$catUid]))
				{
                    $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable('sys_category');

                    $catRes = $queryBuilder
                        ->select('*')
                        ->from('sys_category')
                        ->where(
                            $queryBuilder->expr()->eq('uid', $queryBuilder->createNamedParameter($catUid, \PDO::PARAM_INT))
                        )
                        ->setMaxResults(1)
                        ->execute();

					$cat = $catRes->fetch();
					$categories[$catUid] = $cat;
				} 
				else 
				{
					$cat = $categories[$catUid];
				}
					
				$doUpdateFeGroup = false;
				$doUpdateSinglePid = false;
				
				$categoryFeGroups = $cat['fe_group'];	
				$oldNewsGroups = $news['fe_group'];
					
				if($categoryFeGroups)
				{
					if($this->setAccess)
					{
						if($this->ignore || in_array($news['fe_group'],[-1,-2]))
						{
							if($oldNewsGroups!=$categoryFeGroups)
							{
								$newNewsFeGroups = $categoryFeGroups;
								$doUpdateFeGroup = true;							
							}
						} 
						else 
						{
							$newsFeGroups = [];
							if($news['fe_group']!="")
							{
								$newsFeGroups = explode(",",$news['fe_group']);
							}								
							if($categoryFeGroups!="")
							{
								$categoryFeGroups = explode(",",$categoryFeGroups);
							} 
							else 
							{
								$categoryFeGroups = [];
							}
							$newNewsFeGroups	= $categoryFeGroups;
							foreach($newsFeGroups AS $group)
							{
								if(!in_array($group,$newNewsFeGroups))
								{
									$newNewsFeGroups[] = $group;
								}
							}
								
							$newNewsFeGroups = implode(",",$newNewsFeGroups);
							if($oldNewsGroups!=$newNewsFeGroups)
							{
								$doUpdateFeGroup = true;								
							}														
						}
					}
						
					if($this->setSinglePid)
					{
						if($news['single_pid']!=$cat['single_pid'])
						{
							$newNewsSinglePid = $cat['single_pid'];
							$doUpdateSinglePid = true;							
						}
					}
					if($doUpdateFeGroup || $doUpdateSinglePid){
						
						$updateFields = [];
						
						if($doUpdateFeGroup)
						{
							$updateFields['fe_group'] = $newNewsFeGroups;
						}
						if($doUpdateSinglePid)
						{
							$updateFields['single_pid'] = $newNewsSinglePid;
						}
						
						$result = $GLOBALS['TYPO3_DB']->exec_UPDATEquery(
							'tx_mooxnews_domain_model_news',
							'uid='.$news['uid'],
							$updateFields
						);							
					}				
				}
			}			
		}
		
		return $executionSucceeded;
	}
	
	/**
	 * This method returns the sleep duration as additional information
	 *
	 * @return string Information to display
	 */
	public function getAdditionalInformation() 
	{		
		$info = LocalizationUtility::translate( 'LLL:EXT:moox_news/Resources/Private/Language/locallang_scheduler.xlf:tx_mooxnews_tasks_setnewsaccesstask.news_pids_label', 'moox_news' ).": ".$this->getNewsPids();
				
		return $info;
	}
	
	/**
	 * @return string
	 */
	public function getNewsPids() 
	{
		return $this->newsPids;
	}

	/**
	 * @param string $newsPids
	 */
	public function setNewsPids($newsPids) 
	{
		$this->newsPids = $newsPids;
	}
	
	/**
	 * @return bool
	 */
	public function getSetAccess() 
	{
		return $this->setAccess;
	}

	/**
	 * @param bool $setAccess
	 */
	public function setSetAccess($setAccess) 
	{
		$this->setAccess = $setAccess;
	}
	
	/**
	 * @return bool
	 */
	public function getIgnore() 
	{
		return $this->ignore;
	}

	/**
	 * @param bool $ignore
	 */
	public function setIgnore($ignore) 
	{
		$this->ignore = $ignore;
	}
	
	/**
	 * @return bool
	 */
	public function getSetSinglePid() 
	{
		return $this->setSinglePid;
	}

	/**
	 * @param bool $setSinglePid
	 */
	public function setSetSinglePid($setSinglePid) 
	{
		$this->setSinglePid = $setSinglePid;
	}
}
?>