<?php
$EM_CONF[$_EXTKEY] = [
	'title' => 'MOOX Mailer',
	'description' => 'Send info mails to feusers or addresses, depending on user rights, schedule etc.',
	'category' => 'service',
	'state' => 'beta',
	'uploadfolder' => false,
	'clearCacheOnLoad' => true,
	'author' => 'Dominic Martin',
	'author_email' => 'typo3@neulandlotsen.de',
	'author_company' => 'Neulandlotsen',
	'version' => '9.5.2',
	'constraints' => [
		'depends' => [
			'moox_core' => '9.5.0-9.5.99',
		],
		'conflicts' =>[],
		'suggests' => [],
	],
	'autoload' => [
		'psr-4' => [
			'Neulandlotsen\\MooxMailer\\' => 'Classes'
		]
	]
];
?>
