<?php
namespace Neulandlotsen\MooxNews\Service;

/**
 * This file is part of the "moox_news" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

use TYPO3\CMS\Core\SingletonInterface;
use TYPO3\CMS\Core\Utility\GeneralUtility;

class IcsService implements SingletonInterface
{	
	/**
     * @param \Neulandlotsen\MooxNews\Domain\Model\News $news
     * @param array $settings
     */
	public function writeIcsFile(
		\Neulandlotsen\MooxNews\Domain\Model\News $news = null,
		array $settings = []
	){
		if(is_object($news))
		{
			if($news->getIcsDownloadAllowed() || $settings['forceIcsDownload'])
			{
				$values = [];
				if($news->getEventStarttime())
				{
					if($news->getEventStarttime()->format("Hi")=="0000")
					{
						$values['start'] = ";VALUE=DATE:".$news->getEventStarttime()->format("Ymd");
					}
					else
					{
						$values['start'] = ";TZID=W. Europe Standard Time:".$news->getEventStarttime()->format("Ymd")."T".$news->getEventStarttime()->format("Hi")."00";
					}
					if($news->getEventEndtime())
					{
						if($news->getEventEndtime()->format("Hi")=="2359"){
							$values['end'] = ";VALUE=DATE:".date("Ymd",($news->getEventEndtime()->format("U")+(24*60*60)));
						}
						else
						{
							$values['end'] = ";TZID=W. Europe Standard Time:".$news->getEventEndtime()->format("Ymd")."T".$news->getEventEndtime()->format("Hi")."00";
						}												
					}
				}
				else
				{
					$values['start'] = ";VALUE=DATE:".$news->getDatetime()->format("Ymd");//."T".$news->getDatetime()->format("His");
					$values['end'] = "";
				}
				
				$values['location'] = "";
				$values['dtstamp'] = date("Ymd",time()-3600)."T".date("His",time()-3600)."Z";
				$values['title'] = $news->getTitle();				
				$values['description'] = $news->getTeaser();
				$values['uid'] = md5($news->getUid().$values['dtstamp']);
				$values['id'] = $news->getUid();
				$values['filename'] = "event";
				
				$externalFunctions = [];
				$functions = get_class_methods(get_class($this));
				foreach($functions AS $function)
				{
					if(substr($function,0,13)=="setIcsValues_")
					{
						$externalFunctions[] = $function;
					}
				}										
				
				foreach($externalFunctions AS $externalFunction)
				{
					$values = array_merge($values, $this->$externalFunction($news, $settings));
				}

				$eol = "\r\n";
				$ics_content =
				'BEGIN:VCALENDAR'.$eol.
				'VERSION:2.0'.$eol.
				'PRODID:-//'.$settings['icsNamespace'].'//'.$settings['icsDomain'].'//'.$settings['icsLanguage'].$eol.
				'CALSCALE:GREGORIAN'.$eol.
				'BEGIN:VEVENT'.$eol.
				'DTSTART'.$values['start'].$eol.
				(($values['end']!='')?'DTEND'.$values['end'].$eol:'').
				'LOCATION:'.$values['location'].$eol.
				'DTSTAMP:'.$values['dtstamp'].$eol.
				'SUMMARY:'.$values['title'].$eol;
				if($values['url'])
				{
					$ics_content .=	'URL;VALUE=URI:'.$values['url'].$eol;
				}
				$ics_content .=	'DESCRIPTION:'.$values['description'].$eol.
				'UID:'.$values['uid'].$eol.
				'END:VEVENT'.$eol.
				'END:VCALENDAR';

				header('Content-Type: text/calendar; charset=utf-8');
				header('Content-Disposition: attachment; filename='.$values['filename'].'_'.$values['id'].'.ics');
				
				echo $ics_content;
				
				exit();
			}
			else
			{
				exit("not allowed");
			}
		}
		else
		{
			exit("not found");
		}
	}
}
?>