<?php
namespace Neulandlotsen\MooxTemplateHeco\Domain\Model;

/**
 * This file is part of the "moox_template_heco" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

class News extends \Neulandlotsen\MooxNews\Domain\Model\News {
	/**
	 * @var string
	 */
	protected $mooxTemplateHecoLink;
	

	/**
	 * @return string
	 */
	public function getMooxTemplateHecoLink()
	{
		return $this->mooxTemplateHecoLink;
	}

	/**
	 * @param string $mooxTemplateHecoLink
	 */
	public function setMooxTemplateHecoLink($mooxTemplateHecoLink)
	{
		$this->mooxTemplateHecoLink = $mooxTemplateHecoLink;
	}
}
