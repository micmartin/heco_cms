<?php
if (!defined('TYPO3_MODE')) {
	die ('Access denied.');
}

// Override folder icons
$GLOBALS['TCA']['pages']['columns']['module']['config']['items'][] = [
    0 => 'LLL:EXT:moox_mailer/Resources/Private/Language/locallang_be.xlf:folder-name',
    1 => 'mxmailer',
    2 => 'apps-pagetree-folder-contains-mxmailer'
];
$GLOBALS['TCA']['pages']['ctrl']['typeicon_classes']['contains-mxmailer'] = 'apps-pagetree-folder-contains-mxmailer';

$GLOBALS['TCA']['pages']['columns']['module']['config']['items'][] = [
    0 => 'LLL:EXT:moox_mailer/Resources/Private/Language/locallang_be.xlf:folder-name.drafts',
    1 => 'mxdrafts',
    2 => 'apps-pagetree-folder-contains-mxdrafts'
];
$GLOBALS['TCA']['pages']['ctrl']['typeicon_classes']['contains-mxdrafts'] = 'apps-pagetree-folder-contains-mxdrafts';

$GLOBALS['TCA']['pages']['columns']['module']['config']['items'][] = [
    0 => 'LLL:EXT:moox_mailer/Resources/Private/Language/locallang_be.xlf:folder-name.sent',
    1 => 'mxsent',
    2 => 'apps-pagetree-folder-contains-mxsent'
];
$GLOBALS['TCA']['pages']['ctrl']['typeicon_classes']['contains-mxsent'] = 'apps-pagetree-folder-contains-mxsent';
