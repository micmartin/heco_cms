<?php
namespace Neulandlotsen\MooxNews\Domain\Model\Dto;

/**
 * This file is part of the "moox_news" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

/**
 * Extension Manager configuration
 */
class EmConfiguration
{

    /**
     * Fill the properties properly
     *
     * @param array $configuration em configuration
     */
    public function __construct(array $configuration)
    {
        foreach ($configuration as $key => $value) {
            if (property_exists(__CLASS__, $key)) {
                $this->$key = $value;
            }
        }
    }

    /**
     * @var int
     */
    protected $tagPid = 0;

	/**
	 * @var int
	 */
	protected $targetPid = 0;

    /**
     * @var boolean;
     */
    protected $prependAtCopy = true;

    /**
     * @var string;
     */
    protected $categoryRestriction = '';

    /**
     * @var bool
     */
    protected $categoryBeGroupTceFormsRestriction = false;

    /**
     * @var bool
     */
    protected $contentElementRelation = true;	
	
	/**
	 * @var bool
	 */
	protected $contentPageRelation = false;

    /**
     * @var bool
     */
    protected $manualSorting = false;

    /**
     * @var string
     */
    protected $archiveDate = 'date';

    /**
     * @var bool
     */
    protected $dateTimeNotRequired = false;

    /**
     * @var bool
     */
    protected $showImporter = false;

    /**
     * @var bool
     */
    protected $rteForTeaser = false;

    /**
     * @var bool
     */
    protected $showAdministrationModule = true;

    /**
     * @var bool
     */
    protected $hidePageTreeForAdministrationModule = true;

    /**
     * @var int
     */
    protected $storageUidImporter = 1;

    /**
     * @var string
     */
    protected $resourceFolderImporter = '/mooxnews_import';

	/**
	 * @var bool
	 */
	protected $excludeFromRssDefault = false;	
	
	/**
	 * @var bool
	 */
	protected $hiddenByDefault = true;
	
	/**
	 * @var bool
	 */
	protected $forceReloadByTitleChanges = false;
		
	/**
	 * @var int
	 */
	protected $minCategoryCount = 0;
	
	/**
	 * @var int
	 */
	protected $maxCategoryCount = 0;


    /**
     * @var bool
     */
    protected $showPageTemplates = false;
	
    /**
     * @return int
     */
    public function getTagPid()
    {
        return (int)$this->tagPid;
    }
	
	/**
	 * @return int
	 */
	public function getTargetPid() 
	{
		return (int)$this->targetPid;
	}

    /**
     *
     * @return bool
     */
    public function getPrependAtCopy()
    {
        return (boolean)$this->prependAtCopy;
    }

    /**
     * @return string
     */
    public function getCategoryRestriction()
    {
        return $this->categoryRestriction;
    }

    /**
     * Get categoryBeGroupTceFormsRestriction
     *
     * @return bool
     */
    public function getCategoryBeGroupTceFormsRestriction()
    {
        return (bool)$this->categoryBeGroupTceFormsRestriction;
    }

    /**
     * @return bool
     */
    public function getContentElementRelation()
    {
        return (boolean)$this->contentElementRelation;
    }

    /**
     * @return bool
     */
    public function getContentElementPreview()
    {
        return (bool)$this->contentElementPreview;
    }
	
	/**
	 * @return bool
	 */
	public function getContentPageRelation() 
	{
		return (boolean)$this->contentPageRelation;
	}

    /**
     * @return bool
     */
    public function getManualSorting()
    {
        return (boolean)$this->manualSorting;
    }

    /**
     * @return string
     */
    public function getArchiveDate()
    {
        return $this->archiveDate;
    }

    /**
     * @return bool
     */
    public function getShowImporter()
    {
        return (boolean)$this->showImporter;
    }

    /**
     * @param bool $showAdministrationModule
     */
    public function setShowAdministrationModule($showAdministrationModule)
    {
        $this->showAdministrationModule = $showAdministrationModule;
    }

    /**
     * @return bool
     */
    public function getShowAdministrationModule()
    {
        return $this->showAdministrationModule;
    }

    /**
     * @return bool
     */
    public function getRteForTeaser()
    {
        return $this->rteForTeaser;
    }

    /**
     * @return string
     */
    public function getResourceFolderImporter()
    {
        return $this->resourceFolderImporter;
    }

    /**
     * @return int
     */
    public function getStorageUidImporter()
    {
        return $this->storageUidImporter;
    }

    /**
     * @return bool
     */
    public function getDateTimeNotRequired()
    {
        return (bool)$this->dateTimeNotRequired;
    }

    /**
     * @return bool
     */
    public function getDateTimeRequired()
    {
        return !(bool)$this->dateTimeNotRequired;
    }

    /**
     * @return bool
     */
    public function getHidePageTreeForAdministrationModule()
    {
        return (bool)$this->hidePageTreeForAdministrationModule;
    }
	
	/**
	 * @return bool
	 */
	public function getExcludeFromRssDefault() 
	{
		return $this->excludeFromRssDefault;
	}
		
	/**
	 * @return bool
	 */
	public function getForceReloadByTitleChanges() 
	{
		return $this->forceReloadByTitleChanges;
	}
	
	/**
	 * @return bool
	 */
	public function getHiddenByDefault() 
	{
		return $this->hiddenByDefault;
	}

	/**
	 * @return int
	 */
	public function getMinCategoryCount() 
	{
		return $this->minCategoryCount;
	}
	
	/**
	 * @return int
	 */
	public function getMaxCategoryCount() 
	{
		return $this->maxCategoryCount;
	}
	
	 /**
     * @return bool
     */
    public function getShowPageTemplates()
    {
        return $this->showPageTemplates;
    }
}
