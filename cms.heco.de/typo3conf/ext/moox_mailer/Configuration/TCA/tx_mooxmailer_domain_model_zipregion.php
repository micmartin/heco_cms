<?php
return [
	'ctrl' => [
		'title'	=> 'LLL:EXT:moox_mailer/Resources/Private/Language/locallang_db.xlf:tx_mooxmailer_domain_model_zipregion',
		'label' => 'title',
		'default_sortby' => 'ORDER BY sorting ASC',
		'tstamp' => 'tstamp',
		'crdate' => 'crdate',
		'cruser_id' => 'cruser_id',
		'dividers2tabs' => TRUE,
		'sortby' => 'sorting',
		'versioningWS' => 2,
		'versioning_followPages' => TRUE,
		'origUid' => 't3_origuid',
		'languageField' => 'sys_language_uid',
		'transOrigPointerField' => 'l10n_parent',
		'transOrigDiffSourceField' => 'l10n_diffsource',
		'delete' => 'deleted',
		'enablecolumns' => [
			'disabled' => 'hidden',
			'starttime' => 'starttime',
			'endtime' => 'endtime',
		],		
		'searchFields' => 'title',	
		'requestUpdate' => 'mode',
		'iconfile' => 'EXT:moox_mailer/Resources/Public/Icons/tx_mooxmailer_domain_model_zipregion.gif',
	],
	'interface' => [
		'showRecordFieldList' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, title, mode, lower, upper, list',
	],
	'types' => [
		'1' => ['showitem' => 'sys_language_uid;;;;1-1-1, l10n_parent, l10n_diffsource, title, mode, lower, upper, list,--div--;LLL:EXT:cms/locallang_ttc.xlf:tabs.access,hidden, starttime, endtime'],
	],
	'palettes' => [
		'1' => ['showitem' => ''],
	],
	'columns' => [
		'sys_language_uid' => [
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.language',
			'config' => [
				'type' => 'select',
				'foreign_table' => 'sys_language',
				'foreign_table_where' => 'ORDER BY sys_language.title',
				'items' => [
					['LLL:EXT:lang/locallang_general.xlf:LGL.allLanguages', -1],
					['LLL:EXT:lang/locallang_general.xlf:LGL.default_value', 0]
				],
			],
		],
		'l10n_parent' => [
			'displayCond' => 'FIELD:sys_language_uid:>:0',
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.l18n_parent',
			'config' => [
				'type' => 'select',
				'items' => [
					['', 0],
				],
				'foreign_table' => 'tx_mooxmailer_domain_model_tag',
				'foreign_table_where' => 'AND tx_mooxmailer_domain_model_tag.pid=###CURRENT_PID### AND tx_mooxmailer_domain_model_tag.sys_language_uid IN (-1,0)',
			],
		],
		'l10n_diffsource' => [
			'config' => [
				'type' => 'passthrough',
			],
		],
		't3ver_label' => [
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.versionLabel',
			'config' => [
				'type' => 'input',
				'size' => 30,
				'max' => 255,
			],
		],
		'hidden' => [
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.hidden',
			'config' => [
				'type' => 'check',
			],
		],
		'starttime' => [
			'exclude' => 1,
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.starttime',
			'config' => [
				'type' => 'input',
				'size' => 13,
				'max' => 20,
				'eval' => 'datetime',
				'checkbox' => 0,
				'default' => 0,
				'range' => [
					'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
				],
			],
		],
		'endtime' => [
			'exclude' => 1,
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.endtime',
			'config' => [
				'type' => 'input',
				'size' => 13,
				'max' => 20,
				'eval' => 'datetime',
				'checkbox' => 0,
				'default' => 0,
				'range' => [
					'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
				],
			],
		],
		'title' => [
			'exclude' => 0,
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => 'LLL:EXT:moox_mailer/Resources/Private/Language/locallang_db.xlf:tx_mooxmailer_domain_model_zipregion.title',
			'config' => [
				'type' => 'input',
				'size' => 40,
				'eval' => 'trim,required'
			],
		],
		'mode' => [
			'exclude' => 0,
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => 'LLL:EXT:moox_mailer/Resources/Private/Language/locallang_db.xlf:tx_mooxmailer_domain_model_zipregion.mode',
			'config' => [
				'type' => 'select',
				'items' => array(
				   array('LLL:EXT:moox_mailer/Resources/Private/Language/locallang_db.xlf:tx_mooxmailer_domain_model_zipregion.mode.area', 'area'),
				   array('LLL:EXT:moox_mailer/Resources/Private/Language/locallang_db.xlf:tx_mooxmailer_domain_model_zipregion.mode.list', 'list'),
				),
				'default' => 'area',
			],
		],
		'lower' => [
			'exclude' => 0,
			'displayCond' => 'FIELD:mode:=:area',
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => 'LLL:EXT:moox_mailer/Resources/Private/Language/locallang_db.xlf:tx_mooxmailer_domain_model_zipregion.lower',
			'config' => [
				'type' => 'input',
				'size' => 5,
				'eval' => 'trim,required'
			],			
		],
		'upper' => [
			'exclude' => 0,
			'displayCond' => 'FIELD:mode:=:area',
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => 'LLL:EXT:moox_mailer/Resources/Private/Language/locallang_db.xlf:tx_mooxmailer_domain_model_zipregion.upper',
			'config' => [
				'type' => 'input',
				'size' => 5,
				'eval' => 'trim,required'
			],			
		],
		'list' => array(
			'exclude' => 0,
			'displayCond' => 'FIELD:mode:=:list',
			'label' => 'LLL:EXT:moox_mailer/Resources/Private/Language/locallang_db.xlf:tx_mooxmailer_domain_model_zipregion.list',
			'config' => array(
				'type' => 'text',
				'cols' => 40,
				'rows' => 20,
				'eval' => 'trim,required'
			),
		),
	],
];
?>