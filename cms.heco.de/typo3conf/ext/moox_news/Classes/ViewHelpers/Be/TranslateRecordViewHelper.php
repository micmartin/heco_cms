<?php
namespace Neulandlotsen\MooxNews\ViewHelpers\Be;

/**
 * This file is part of the "moox_news" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

use TYPO3\CMS\Backend\Routing\UriBuilder;
use TYPO3\CMS\Backend\Utility\BackendUtility;
use TYPO3\CMS\Core\FormProtection\FormProtectionFactory;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3Fluid\Fluid\Core\Rendering\RenderingContextInterface;
use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper;
use TYPO3Fluid\Fluid\Core\ViewHelper\ViewHelperInterface;

/**
 * Translate Record ViewHelper, see FormEngine logic
 */
class TranslateRecordViewHelper extends AbstractViewHelper implements ViewHelperInterface
{

    public function initializeArguments()
    {
        parent::initializeArguments();
		$this->registerArgument('table', 'string', 'table', true);
        $this->registerArgument('recordId', 'int', 'recordId', true);
		$this->registerArgument('recordPid', 'int', 'recordPid', true);
		$this->registerArgument('languageId', 'int', 'languageId', true);
    }

    /**
     * Returns a URL to link to FormEngine
     *
     * @return string URL to FormEngine module + parameters
     * @see \TYPO3\CMS\Backend\Utility\BackendUtility::getModuleUrl()
     */
    public function render()
    {
        return static::renderStatic(
            [
                'table' => $this->arguments['table'],
				'recordId' => $this->arguments['recordId'],
				'recordPid' => $this->arguments['recordPid'],
				'languageId' => $this->arguments['languageId']
            ],
            $this->buildRenderChildrenClosure(),
            $this->renderingContext
        );
    }

    /**
     * @param array $arguments
     * @param callable $renderChildrenClosure
     * @param RenderingContextInterface $renderingContext
     * @return string
     */
    public static function renderStatic(
        array $arguments,
        \Closure $renderChildrenClosure,
        RenderingContextInterface $renderingContext
    ) 
	{
		$uriBuilder = GeneralUtility::makeInstance(UriBuilder::class);

		$listUrl = [];
		$listUrl['returnUrl'] = GeneralUtility::getIndpEnv('REQUEST_URI');
		$listUrl['id'] = $arguments['recordPid'];
		$listUrl['table'] = '';
		$listUrl['imagemode'] = 1;
		$listUrl['justLocalized'] = $arguments['table'].":".$arguments['recordId'].":".$arguments['languageId'];
		$listUrl = (string)$uriBuilder->buildUriFromRoute('record_edit', $listUrl);

		$localizeUrl = [];
		$localizeUrl['cmd['.$arguments['table'].']['.$arguments['recordId'].'][localize]'] = $arguments['languageId'];
		$localizeUrl['redirect'] = $listUrl;


		return $uriBuilder->buildUriFromRoute('tce_db', $localizeUrl);
    }
}