<?php
namespace Neulandlotsen\MooxNewsSeminar\Domain\Model;

/**
 * This file is part of the "moox_news_seminar" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

class News extends \Neulandlotsen\MooxNews\Domain\Model\News {
	/**
	 * @var integer
	 */
	protected $mooxNewsSeminarStartdate;	
	
	/**
	 * @var integer
	 */
	protected $mooxNewsSeminarEnddate;
	
	/**
	 * @var integer
	 */
	protected $mooxNewsSeminarPrice;
	
	/**
	 * @var string
	 */
	protected $mooxNewsSeminarPriceInfo;

	/**
	 * @var integer
	 */
	protected $mooxNewsSeminarSubscriberMin;

	/**
	 * @var integer
	 */
	protected $mooxNewsSeminarSubscriberMax;
	
	/**
	 * @var integer
	 */
	protected $mooxNewsSeminarRegisterCountdown;
	
	/**
	 * @var integer
	 */
	protected $mooxNewsSeminarRegisterUntil;	
	
	/**
	 * @var integer
	 */
	protected $mooxNewsSeminarRegisterDaysLeft;
	
	/**
	 * @var integer
	 */
	protected $mooxNewsSeminarRegisterDeadline;

	/**
	 * @var bool
	 */
	protected $mooxNewsSeminarRegisterAllYear;

	/**
	 * @var bool
	 */
	protected $mooxNewsSeminarRegisterPossible;
	
	/**
	 * @var string
	 */
	protected $mooxNewsSeminarSearchIndex;
	
	/**
	 * @var array
	 */
 	 protected $mooxNewsSeminarActiveRelated;
	
	/**
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Neulandlotsen\MooxNewsSeminar\Domain\Model\Speaker>
     * @TYPO3\CMS\Extbase\Annotation\ORM\Lazy
     */
    protected $mooxNewsSeminarSpeakers;
	
	/**
	 * @return integer
	 */
	public function getMooxNewsSeminarStartdate()
	{
		return $this->mooxNewsSeminarStartdate;
	}

	/**
	 * @param integer $mooxNewsSeminarStartdate
	 */
	public function setMooxNewsSeminarStartdate($mooxNewsSeminarStartdate)
	{
		$this->mooxNewsSeminarStartdate = $mooxNewsSeminarStartdate;
	}
	
	/**
	 * @return integer
	 */
	public function getMooxNewsSeminarEnddate()
	{
		return $this->mooxNewsSeminarEnddate;
	}

	/**
	 * @param integer $mooxNewsSeminarEnddate
	 */
	public function setMooxNewsSeminarEnddate($mooxNewsSeminarEnddate)
	{
		$this->mooxNewsSeminarEnddate = $mooxNewsSeminarEnddate;
	}
	
	/**
	 * @return double
	 */
	public function getMooxNewsSeminarPrice()
	{
		return $this->mooxNewsSeminarPrice;
	}

	/**
	 * @param double $mooxNewsSeminarPrice seminar price
	 */
	public function setMooxNewsSeminarPrice($mooxNewsSeminarPrice)
	{
		$this->mooxNewsSeminarPrice = $mooxNewsSeminarPrice;
	}
	
	/**
	 * @return string
	 */
	public function getMooxNewsSeminarPriceInfo()
	{
		return $this->mooxNewsSeminarPriceInfo;
	}

	/**
	 * @param string $mooxNewsSeminarPriceInfo
	 */
	public function setMooxNewsSeminarPriceInfo($mooxNewsSeminarPriceInfo)
	{
		$this->mooxNewsSeminarPriceInfo = $mooxNewsSeminarPriceInfo;
	}

	/**
	 * @return integer
	 */
	public function getMooxNewsSeminarSubscriberMin()
	{
		return $this->mooxNewsSeminarSubscriberMin;
	}

	/**
	 * @param integer $mooxNewsSeminarSubscriberMin
	 */
	public function setMooxNewsSeminarSubscriberMin($mooxNewsSeminarSubscriberMin)
	{
		$this->mooxNewsSeminarSubscriberMin = $mooxNewsSeminarSubscriberMin;
	}

	/**
	 * @return integer
	 */
	public function getMooxNewsSeminarSubscriberMax()
	{
		return $this->mooxNewsSeminarSubscriberMax;
	}

	/**
	 * @param integer $mooxNewsSeminarSubscriberMax	 
	 */
	public function setMooxNewsSeminarSubscriberMax($mooxNewsSeminarSubscriberMax)
	{
		$this->mooxNewsSeminarSubscriberMax = $mooxNewsSeminarSubscriberMax;
	}
	
	/**
	 * @return integer
	 */
	public function getMooxNewsSeminarRegisterCountdown()
	{
		return $this->mooxNewsSeminarRegisterCountdown;
	}

	/**
	 * @param integer $mooxNewsSeminarRegisterCountdown 
	 */
	public function setMooxNewsSeminarRegisterCountdown($mooxNewsSeminarRegisterCountdown)
	{
		$this->mooxNewsSeminarRegisterCountdown = $mooxNewsSeminarRegisterCountdown;
	}
	
	/**
	 * @return integer
	 */
	public function getMooxNewsSeminarRegisterUntil()
	{		
		$mooxNewsSeminarRegisterUntil = 0;
		
		$extConf = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Configuration\ExtensionConfiguration::class)->get('moox_news_seminar');
		
		if($this->mooxNewsSeminarRegisterDeadline>0)
		{				
			$mooxNewsSeminarRegisterUntil = $this->mooxNewsSeminarRegisterDeadline;				
		} 
		elseif($this->mooxNewsSeminarStartdate>0 && $extConf['seminarRegisterCountdown']>0)
		{
			$mooxNewsSeminarRegisterUntil = mktime(
				0,
				0,
				0,
				date("n",$this->mooxNewsSeminarStartdate),
				date("j",$this->mooxNewsSeminarStartdate)-$extConf['seminarRegisterCountdown'],
				date("Y",$this->mooxNewsSeminarStartdate)
			);			
		} 
		
		return $mooxNewsSeminarRegisterUntil;
	}	
		
	/**
	 * @return integer
	 */
	public function getMooxNewsSeminarRegisterDaysLeft()
	{		
		$mooxNewsSeminarRegisterDaysLeft = 0;
		
		if($this->getMooxNewsSeminarRegisterUntil()>0)
		{
			$diff = $this->getMooxNewsSeminarRegisterUntil()+86400-time();
			$mooxNewsSeminarRegisterDaysLeft = ceil($diff/(60*60*24));
			if($mooxNewsSeminarRegisterDaysLeft==0)
			{
				$mooxNewsSeminarRegisterDaysLeft = 0;
			}
		}		
		
		return $mooxNewsSeminarRegisterDaysLeft;
	}
	
	/**	 
	 * @return boolean
	 */
	public function getMooxNewsSeminarRegisterPossible()
	{		
		$mooxNewsSeminarRegisterPossible = false;
		
		if($this->getMooxNewsSeminarRegisterUntil()==0 || ($this->getMooxNewsSeminarRegisterUntil()>0 && ($this->getMooxNewsSeminarRegisterUntil()+86400)>time()))
		{
			$mooxNewsSeminarRegisterPossible = true;
		} 
		
		return $mooxNewsSeminarRegisterPossible;
	}
	
	/**
	 * @return integer
	 */
	public function getMooxNewsSeminarRegisterDeadline()
	{
		return $this->mooxNewsSeminarRegisterDeadline;
	}

	/**
	 * @param integer $mooxNewsSeminarRegisterDeadline	 
	 */
	public function setMooxNewsSeminarRegisterDeadline($mooxNewsSeminarRegisterDeadline)
	{
		$this->mooxNewsSeminarRegisterDeadline = $mooxNewsSeminarRegisterDeadline;
	}

	/**
	 * @return boolean
	 */
	public function getMooxNewsSeminarRegisterAllYear()
	{
		return $this->mooxNewsSeminarRegisterAllYear;
	}

	/**
	 * @param boolean $mooxNewsSeminarRegisterAllYear
	 */
	public function setMooxNewsSeminarRegisterAllYear($mooxNewsSeminarRegisterAllYear)
	{
		$this->mooxNewsSeminarRegisterAllYear = $mooxNewsSeminarRegisterAllYear;
	}
	
	/**
	 * @return string
	 */
	public function getMooxNewsSeminarSearchIndex()
	{
		return $this->mooxNewsSeminarSearchIndex;
	}

	/**
	 * @param string $mooxNewsSeminarSearchIndex	 
	 */
	public function setMooxNewsSeminarSearchIndex($mooxNewsSeminarSearchIndex)
	{
		$this->mooxNewsSeminarSearchIndex = $mooxNewsSeminarSearchIndex;
	}
	
	/**
	 * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage|string
     */
	public function getMooxNewsSeminarSpeakers()
	{
		return $this->mooxNewsSeminarSpeakers;
	}

	/**
	 * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage $mooxNewsSeminarSpeakers
	 */
	public function setMooxNewsSeminarSpeakers($mooxNewsSeminarSpeakers)
	{
		$this->mooxNewsSeminarSpeakers = $mooxNewsSeminarSpeakers;
	}

	/**
	 * Return related items sorted by datetime
	 *
	 * @return array
	 */
	public function getMooxNewsSeminarActiveRelated()
	{
		$related = [];
		$items = $this->getRelated();
		foreach ($items AS $item)
		{
			if($item->getMooxNewsSeminarStartdate()>time())
			{
				$related[] = $item;
			}
		}
		if(count($related))
		{
			return $related;
		}
		return NULL;
	}
}
