<?php

namespace Neulandlotsen\MooxNews\Domain\Repository;

/**
 * This file is part of the "moox_news" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

/**
 * Repository for link objects
 *
 */
class LinkRepository extends \TYPO3\CMS\Extbase\Persistence\Repository
{
}
