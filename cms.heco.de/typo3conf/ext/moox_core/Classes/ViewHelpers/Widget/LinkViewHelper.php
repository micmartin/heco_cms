<?php
namespace Neulandlotsen\MooxCore\ViewHelpers\Widget;

/**
 * This file is part of the "moox_core" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

use TYPO3\CMS\Core\Utility\GeneralUtility;

class LinkViewHelper extends \TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper
{
	/**
	 * Get the URI for a non-AJAX Request.
	 *
	 * @return string the Widget URI
	 */
	protected function getWidgetUri()
	{
		$excludeFromQuery = ['cHash'];
		$addQueryString = true;
		$uriBuilder = $this->renderingContext->getControllerContext()->getUriBuilder();
		$argumentPrefix = $this->renderingContext->getControllerContext()->getRequest()->getArgumentPrefix();

		if($argumentPrefix)
		{
			$excludeFromQuery[] = $argumentPrefix;
			$argumentPrefixBaseVar = explode("[",$argumentPrefix)[0];
			if($argumentPrefixBaseVar)
			{
				$excludeFromQuery[] = $argumentPrefixBaseVar."[__referrer]";
				$excludeFromQuery[] = $argumentPrefixBaseVar."[__trustedProperties]";
				$overwriteDemands = GeneralUtility::_GP($argumentPrefixBaseVar)['overwriteDemand'];
				if(is_array($overwriteDemands))
				{
					if(!isset($overwriteDemands['reset']))
					{
						foreach ($overwriteDemands AS $overwriteDemand=>$value)
						{
							if($value == "" || $value=="all")
							{
								$excludeFromQuery[] = $argumentPrefixBaseVar."[overwriteDemand][".$overwriteDemand."]";
							}
						}
					}
					else
					{
						$addQueryString = false;
					}
				}
			}
		}

		$arguments = $this->hasArgument('arguments') ? $this->arguments['arguments'] : [];
		if ($this->hasArgument('action')) {
			$arguments['action'] = $this->arguments['action'];
		}
		if ($this->hasArgument('format') && $this->arguments['format'] !== '') {
			$arguments['format'] = $this->arguments['format'];
		}

		$uriBuilder->reset()
			->setArguments([$argumentPrefix => $arguments])
			->setSection($this->arguments['section'])
			->setUseCacheHash($this->arguments['useCacheHash'])
			->setFormat($this->arguments['format']);

		if($addQueryString)
		{
			$uriBuilder->setAddQueryString(true)
				->setAddQueryStringMethod($this->arguments['addQueryStringMethod'])
				->setArgumentsToBeExcludedFromQueryString($excludeFromQuery);
		}

		return $uriBuilder->build();
	}
}
