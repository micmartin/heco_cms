<?php
namespace Neulandlotsen\MooxMailer\Service;

use Neulandlotsen\MooxCore\Utility\HelperUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Core\Messaging\FlashMessage;
use TYPO3\CMS\Core\Utility\VersionNumberUtility;

class InstallService 
{
    /**
    * @var string
    */
    protected $extKey = 'moox_mailer';
    
    /**
     * @var string
     */
    protected $messageQueueByIdentifier = '';

    /**
     * Initializes the install service
     */
    public function __construct()
	{
		$this->messageQueueByIdentifier = 'extbase.flashmessages.tx_extensionmanager_tools_extensionmanagerextensionmanager';
	}

	/**
	 * Creates maildir in webroot
	 */
    public function createMaildir()
	{
		$maildirSkeleton = ExtensionManagementUtility::extPath("moox_mailer")."Resources/Skeleton/maildir";
		$maildirLocation = $GLOBALS["_SERVER"]["DOCUMENT_ROOT"]."/maildir";
		if(!is_dir($maildirLocation))
		{
			HelperUtility::dircpy("", $maildirSkeleton, $maildirLocation);
			if(file_exists($maildirLocation."/archive.sh")){
				chmod($maildirLocation."/archive.sh",0760);
			}
			/**
			 * Add Flashmessage that there is robots_allowed.txt file placed in the website root
			 */
			$flashMessage = GeneralUtility::makeInstance('TYPO3\\CMS\\Core\\Messaging\\FlashMessage',
				'',
				'Maildir was placed in the root directory.',
				FlashMessage::OK,
				TRUE
			);
			$this->addFlashMessage($flashMessage);
		}
    }


    /**
     * Adds a Flash Message to the Flash Message Queue
     *
     * @param FlashMessage $flashMessage
     */
    public function addFlashMessage(FlashMessage $flashMessage)
	{
	    if($flashMessage)
		{
		    /** @var $flashMessageService \TYPO3\CMS\Core\Messaging\FlashMessageService */
		    $flashMessageService = GeneralUtility::makeInstance('TYPO3\\CMS\\Core\\Messaging\\FlashMessageService');
		    /** @var $flashMessageQueue \TYPO3\CMS\Core\Messaging\FlashMessageQueue */
		    $flashMessageQueue = $flashMessageService->getMessageQueueByIdentifier($this->messageQueueByIdentifier);
		    $flashMessageQueue->enqueue($flashMessage);
	    }
    }

}
