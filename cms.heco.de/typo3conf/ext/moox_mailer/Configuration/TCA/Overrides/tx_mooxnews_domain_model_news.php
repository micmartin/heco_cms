<?php
if (!defined('TYPO3_MODE')) {
	die('Access denied.');
}

$ll = 'LLL:EXT:moox_mailer/Resources/Private/Language/locallang_db.xlf:';
$extConf = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(
    \TYPO3\CMS\Core\Configuration\ExtensionConfiguration::class
)->get('moox_mailer');

// add new news fields to field arrays
\Neulandlotsen\MooxCore\Utility\TcaHelper::addBackendPreviewFieldsSet(
	"tx_mooxnews_domain_model_news",
	"moox_news",[
		'mailer_title',
		'mailer_teaser'
	]
);

\Neulandlotsen\MooxCore\Utility\TcaHelper::addFieldsToArray(
	"tx_mooxnews_domain_model_news",
	"allowedGrabberFields", [
		'mailer_title',
		'mailer_teaser',
	]
);

/***************
 * Add extra fields to the moox_news record
 */
$newTxMooxnewsDomainModelNewsColumns = [	
	'mailer_frequency' => [		
		'exclude' => 1,	
		'l10n_mode' => 'exclude',
		'label' => $ll . 'tx_mooxmailer_domain_model_news.mailer_frequency',		
		'config' => [
			'type' => 'select',
			'items' => [
				[$ll . 'tx_mooxmailer_domain_model_news.mailer_frequency.I.0', '0'],
				[$ll . 'tx_mooxmailer_domain_model_news.mailer_frequency.I.1', '1'],
				[$ll . 'tx_mooxmailer_domain_model_news.mailer_frequency.I.2', '2'],
			],
			'size' => 1,	
			'maxitems' => 1,
		]
	],
	'mailer_tags' => [		
		'exclude' => 1,		
		'label' => $ll . 'tx_mooxmailer_domain_model_news.mailer_tags',
		'config' => [
			'type' => 'select',
			'allowNonIdValues' => 1,				
			'size' => 6,
			'minitems' => 0,
			'maxitems' => 99,
			'multiple' => 1,
			'itemsProcFunc' => 'Neulandlotsen\MooxMailer\Hooks\TcaFormHelper->tags',			
		],		
	],
	'mailer_zipregions' => [		
		'exclude' => 1,		
		'label' => $ll . 'tx_mooxmailer_domain_model_news.mailer_zipregions',
		'config' => [
			'type' => 'select',
			'allowNonIdValues' => 1,				
			'size' => 6,
			'minitems' => 0,
			'maxitems' => 99,
			'multiple' => 1,
			'itemsProcFunc' => 'Neulandlotsen\MooxMailer\Hooks\TcaFormHelper->zipregions',			
		],		
	],
    'mailer_title' => [
        'exclude' => 1,
        'label' => $ll . 'tx_mooxmailer_domain_model_news.mailer_title',
        'config' => [
            'type' => 'input',
            'size' => 60
        ],
    ],
    'mailer_teaser' => [
        'exclude' => 1,
        'label' => $ll . 'tx_mooxmailer_domain_model_news.mailer_teaser',
        'config' => [
            'type' => 'text',
            'cols' => 50,
			'rows' => 5
        ],
    ],
    'mailer_files' => [
        'exclude' => 1,
        'l10n_mode' => 'mergeIfNotBlank',
        'label' => $ll . 'tx_mooxmailer_domain_model_news.mailer_files',
        'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig(
            'mailer_files',
            [
                'appearance' => [
                    'createNewRelationLinkTitle' => $ll . 'tx_mooxmailer_domain_model_news.mailer_files.add',
                    'showPossibleLocalizationRecords' => true,
                    'showRemovedLocalizationRecords' => true,
                    'showAllLocalizationLink' => true,
                    'showSynchronizationLink' => true
                ],
                'inline' => [
                    'inlineOnlineMediaAddButtonStyle' => 'display:none'
                ],
                'foreign_match_fields' => [
                    'fieldname' => 'mailer_files',
                    'tablenames' => 'tx_mooxnews_domain_model_news',
                    'table_local' => 'sys_file',
                ],
            ],
           $extConf['allowedFileTypes']
        ),
        // special moox configuration
        'moox' => [
            'extkey' => 'moox_news',
        ],
    ],
];

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('tx_mooxnews_domain_model_news', $newTxMooxnewsDomainModelNewsColumns,1);
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes('tx_mooxnews_domain_model_news', '--div--;' . $ll . 'tx_mooxmailer_domain_model_news.tabs.mailer,mailer_frequency,mailer_tags,mailer_zipregions,mailer_title,mailer_teaser,mailer_files', '', '');
?>