<?php
defined('TYPO3_MODE') or die();

$boot = function () {
    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
        'Neulandlotsen.moox_news',
        'Pi1',
        [
            'News' => 'list,detail,preview,dateMenu,searchForm,searchResult,secureDownload,ics',
            'Category' => 'list',
            'Tag' => 'list',
			'Target' => 'list',
        ],
        [
			'News' => 'preview,secureDownload,ics'
		]
    );

	\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
		'Neulandlotsen.moox_news',
		'Pi2',
		[
			'News' => 'pdf',
		],
		// non-cacheable actions
		[
			'News' => 'pdf,'
		]
	);

	\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
		'Neulandlotsen.moox_news',
		'Pi3',
		[
			'Redirector' => 'redirect',

		],
		// non-cacheable actions
		[
			'Redirector' => 'redirect'
		]
	);

	\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
		'Neulandlotsen.moox_news',
		'Pi4',
		[
			'Ajax' => 'default,fetchList,fetchDetail,fetchFilter',
		]
	);
	
	$GLOBALS['TYPO3_CONF_VARS']['SYS']['formEngine']['nodeRegistry']['mooxNewsSelectSortableCategoryTree'] = [
		'nodeName' => 'selectSortableCategoryTree',
		'priority' => '70',
		'class' => \Neulandlotsen\MooxNews\Form\Element\SelectSortableCategoryTreeElement::class,
    ];
	
    // Page module hook
    $GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['cms/layout/class.tx_cms_layout.php']['list_type_Info']['mooxnews' . '_pi1']['moox_news'] =
        \Neulandlotsen\MooxNews\Hooks\PageLayoutView::class . '->getExtensionSummary';

    $GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['t3lib/class.t3lib_tcemain.php']['clearCachePostProc']['moox_news_clearcache'] =
        \Neulandlotsen\MooxNews\Hooks\DataHandler::class . '->clearCachePostProc';
	
	// Edit restriction for news records
    $GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['t3lib/class.t3lib_tcemain.php']['processCmdmapClass']['moox_news'] =
        \Neulandlotsen\MooxNews\Hooks\DataHandler::class;
		
	// Post save Handler
    $GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['t3lib/class.t3lib_tcemain.php']['processDatamapClass']['moox_news'] =
        \Neulandlotsen\MooxNews\Hooks\DataHandler::class;

    // FormEngine: Rendering of fields
    $GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['t3lib/class.t3lib_tceforms.php']['getSingleFieldClass']['moox_news'] =
        \Neulandlotsen\MooxNews\Hooks\FormEngine::class;

    // FormEngine: Rendering of the whole FormEngine
    $GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['t3lib/class.t3lib_tceforms.php']['getMainFieldsClass']['moox_news'] =
        \Neulandlotsen\MooxNews\Hooks\FormEngine::class;

    // Modify flexform values
	$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['t3lib/class.t3lib_befunc.php']['getFlexFormDSClass']['moox_news'] =
		\Neulandlotsen\MooxNews\Hooks\BackendUtility::class;

	// Modify flexform fields since core 8.5 via formEngine: Inject a data provider between TcaFlexPrepare and TcaFlexProcess
	if (\TYPO3\CMS\Core\Utility\VersionNumberUtility::convertVersionNumberToInteger(TYPO3_version) >= 8005000) {
		$GLOBALS['TYPO3_CONF_VARS']['SYS']['formEngine']['formDataGroup']['tcaDatabaseRecord'][\Neulandlotsen\MooxNews\Backend\FormDataProvider\NewsFlexFormManipulation::class] = [
			'depends' => [
				\TYPO3\CMS\Backend\Form\FormDataProvider\TcaFlexPrepare::class,
			],
			'before' => [
				\TYPO3\CMS\Backend\Form\FormDataProvider\TcaFlexProcess::class,
			],
		];
	}

	// Modify flexform values
	$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['t3lib/class.t3lib_befunc.php']['getFlexFormDSClass']['news'] =
		\Neulandlotsen\MooxNews\Hooks\BackendUtility::class;
	$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS'][\TYPO3\CMS\Core\Configuration\FlexForm\FlexFormTools::class]['flexParsing']['news']
		= \Neulandlotsen\MooxNews\Hooks\BackendUtility::class;
	// Modify flexform fields since core 8.5 via formEngine: Inject a data provider between TcaFlexPrepare and TcaFlexProcess
	$GLOBALS['TYPO3_CONF_VARS']['SYS']['formEngine']['formDataGroup']['tcaDatabaseRecord'][\Neulandlotsen\MooxNews\Backend\FormDataProvider\NewsFlexFormManipulation::class] = [
		'depends' => [
			\TYPO3\CMS\Backend\Form\FormDataProvider\TcaFlexPrepare::class,
		],
		'before' => [
			\TYPO3\CMS\Backend\Form\FormDataProvider\TcaFlexProcess::class,
		],
	];

    // Inline records hook
    $GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['t3lib/class.t3lib_tceforms_inline.php']['tceformsInlineHook']['moox_news'] =
        \Neulandlotsen\MooxNews\Hooks\InlineElementHook::class;

    // Xclass InlineRecordContainer
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects'][\TYPO3\CMS\Backend\Form\Container\InlineRecordContainer::class] = [
        'className' => \Neulandlotsen\MooxNews\Xclass\InlineRecordContainerForNews::class,
    ];

    /* ===========================================================================
        Custom cache, done with the caching framework
    =========================================================================== */
    if (!is_array($GLOBALS['TYPO3_CONF_VARS']['SYS']['caching']['cacheConfigurations']['cache_news_category'])) {
        $GLOBALS['TYPO3_CONF_VARS']['SYS']['caching']['cacheConfigurations']['cache_news_category'] = [];
    }
    // Define string frontend as default frontend, this must be set with TYPO3 4.5 and below
    // and overrides the default variable frontend of 4.6
    if (!isset($GLOBALS['TYPO3_CONF_VARS']['SYS']['caching']['cacheConfigurations']['cache_news_category']['frontend'])) {
        $GLOBALS['TYPO3_CONF_VARS']['SYS']['caching']['cacheConfigurations']['cache_news_category']['frontend'] = \TYPO3\CMS\Core\Cache\Frontend\StringFrontend::class;
    }

    /* ===========================================================================
        Add TSconfig
    =========================================================================== */
    // For linkvalidator
    if (\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::isLoaded('linkvalidator')) {
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig('<INCLUDE_TYPOSCRIPT: source="FILE:EXT:moox_news/Configuration/TSconfig/Page/mod.linkvalidator.typoscript">');
    }
    if (\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::isLoaded('guide')) {
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig('  <INCLUDE_TYPOSCRIPT: source="DIR:EXT:moox_news/Configuration/TSconfig/Tours" extensions="ts">');
    }
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig('<INCLUDE_TYPOSCRIPT: source="FILE:EXT:moox_news/Configuration/TSconfig/ContentElementWizard.typoscript">');
	
	/* ===========================================================================
		Scheduler tasks
	=========================================================================== */

	if(false)
	{
		$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['scheduler']['tasks']['Neulandlotsen\\MooxNews\\Tasks\\NewsAutoSetTask'] = [
			'extension' => 'moox_news',
			'title' => 'LLL:EXT:moox_news/Resources/Private/Language/locallang_scheduler.xlf:tx_mooxnews_tasks_newsautosettask.title',
			'description' => 'LLL:EXT:moox_news/Resources/Private/Language/locallang_scheduler.xlf:tx_mooxnews_tasks_newsautosettask.description',
			'additionalFields' => 'Neulandlotsen\\MooxNews\\Tasks\\NewsAutoSetTaskAdditionalFieldProvider'
		];
	}
	
    /* ===========================================================================
        Hooks
    =========================================================================== */
    if (\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::isLoaded('realurl')) {
        $GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['ext/realurl/class.tx_realurl_autoconfgen.php']['extensionConfiguration']['moox_news'] =
            \Neulandlotsen\MooxNews\Hooks\RealUrlAutoConfiguration::class . '->addNewsConfig';
    }

	// Register cache frontend for proxy class generation
	$GLOBALS['TYPO3_CONF_VARS']['SYS']['caching']['cacheConfigurations']['moox_news'] = [
		'frontend' => \TYPO3\CMS\Core\Cache\Frontend\PhpFrontend::class,
		'backend' => \TYPO3\CMS\Core\Cache\Backend\FileBackend::class,
		'groups' => [
			'all',
			'system',
		],
		'options' => [
			'defaultLifetime' => 0,
		]
	];


    $GLOBALS['TYPO3_CONF_VARS']['SYS']['formEngine']['formDataGroup']['tcaDatabaseRecord'][\Neulandlotsen\MooxNews\Backend\FormDataProvider\NewsRowInitializeNew::class] = [
        'depends' => [
            \TYPO3\CMS\Backend\Form\FormDataProvider\DatabaseRowInitializeNew::class,
        ]
    ];
    \Neulandlotsen\MooxNews\Utility\ClassLoader::registerAutoloader();

    if (TYPO3_MODE === 'BE') {
        $icons = [
            //'apps-pagetree-folder-contains-news' => 'ext-news-folder-tree.svg',
			'apps-pagetree-folder-contains-news' => 'mooxnews_folder.svg',
            'ext-news-wizard-icon' => 'plugin_wizard.svg',			
            'ext-news-type-default' => 'mooxnews_domain_model_news.svg',
            'ext-news-type-internal' => 'mooxnews_domain_model_news_internal.svg',
            'ext-news-type-external' => 'mooxnews_domain_model_news_external.svg',
			'ext-news-category' => 'mooxnews_domain_model_category.svg',
            'ext-news-tag' => 'mooxnews_domain_model_tag.svg',
			'ext-news-target' => 'mooxnews_domain_model_target.svg',
            'ext-news-link' => 'mooxnews_domain_model_link.svg',
			'list_icon_moox_news' => 'mooxnews_domain_model_news.svg',
			'mooxnews_actions-sort-down' => 'mooxnews_actions-sort-down.svg',
			'mooxnews_actions-sort-down-active' => 'mooxnews_actions-sort-down-active.svg',
			'mooxnews_actions-sort-up' => 'mooxnews_actions-sort-up.svg',
			'mooxnews_actions-sort-up-active' => 'mooxnews_actions-sort-up-active.svg',
			'mooxnews_actions-ics' => 'mooxnews_actions-ics.svg',
        ];
        $iconRegistry = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Imaging\IconRegistry::class);
        foreach ($icons as $identifier => $path) {
            $iconRegistry->registerIcon(
                $identifier,
                \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
                ['source' => 'EXT:moox_news/Resources/Public/Icons/' . $path]
            );
        }
    }

    if (\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::isLoaded('dd_googlesitemap')) {
        $GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['dd_googlesitemap']['sitemap']['txmooxnews']
            = \Neulandlotsen\MooxNews\Hooks\TxNewsSitemapGenerator::class . '->main';
    }

	$GLOBALS['TYPO3_CONF_VARS']['SYS']['formEngine']['nodeRegistry'][1552726986] = [
		'nodeName' => 'NewsStaticText',
		'priority' => 70,
		'class' => \Neulandlotsen\MooxNews\Backend\FieldInformation\StaticText::class
	];

    $GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['extbase']['commandControllers'][] = \Neulandlotsen\MooxNews\Command\NewsImportCommandController::class;

	// extend moox_core classes
	$GLOBALS['TYPO3_CONF_VARS']['EXT']['moox_core']['classes']['Controller/FileSecurityController'][] = 'moox_news';
	
	// remove from chash
	// ToDo: Find solution for filter form, remove workaround
	/*
	$GLOBALS['TYPO3_CONF_VARS']['FE']['cacheHash']['excludedParameters'][] = 'tx_mooxnews_pi1[][referrer]';
	$GLOBALS['TYPO3_CONF_VARS']['FE']['cacheHash']['excludedParameters'][] = 'tx_mooxnews_pi1[][referrer][@extension]';
	$GLOBALS['TYPO3_CONF_VARS']['FE']['cacheHash']['excludedParameters'][] = 'tx_mooxnews_pi1[][referrer][@vendor]';
	$GLOBALS['TYPO3_CONF_VARS']['FE']['cacheHash']['excludedParameters'][] = 'tx_mooxnews_pi1[][referrer][@controller]';
	$GLOBALS['TYPO3_CONF_VARS']['FE']['cacheHash']['excludedParameters'][] = 'tx_mooxnews_pi1[][referrer][@action]';
	$GLOBALS['TYPO3_CONF_VARS']['FE']['cacheHash']['excludedParameters'][] = 'tx_mooxnews_pi1[][referrer][arguments]';
	$GLOBALS['TYPO3_CONF_VARS']['FE']['cacheHash']['excludedParameters'][] = 'tx_mooxnews_pi1[][referrer][@request]';
	$GLOBALS['TYPO3_CONF_VARS']['FE']['cacheHash']['excludedParameters'][] = 'tx_mooxnews_pi1[][trustedProperties]';
	$GLOBALS['TYPO3_CONF_VARS']['FE']['cacheHash']['excludedParameters'][] = 'tx_mooxnews_pi1[action]';
	$GLOBALS['TYPO3_CONF_VARS']['FE']['cacheHash']['excludedParameters'][] = 'tx_mooxnews_pi1[controller]';
	$GLOBALS['TYPO3_CONF_VARS']['FE']['cacheHash']['excludedParameters'][] = 'tx_mooxnews_pi1[news]';
	$GLOBALS['TYPO3_CONF_VARS']['FE']['cacheHash']['excludedParameters'][] = 'tx_mooxnews_pi1[currentPage]';
	$GLOBALS['TYPO3_CONF_VARS']['FE']['cacheHash']['excludedParameters'][] = 'tx_mooxnews_pi1[contentObjectUid]';
	$GLOBALS['TYPO3_CONF_VARS']['FE']['cacheHash']['excludedParameters'][] = 'tx_mooxnews_pi1[news_preview]';
	$GLOBALS['TYPO3_CONF_VARS']['FE']['cacheHash']['excludedParameters'][] = 'tx_mooxnews_pi1[news_preview_hash]';
	$GLOBALS['TYPO3_CONF_VARS']['FE']['cacheHash']['excludedParameters'][] = 'tx_mooxnews_pi1[overwriteDemand]';
	$GLOBALS['TYPO3_CONF_VARS']['FE']['cacheHash']['excludedParameters'][] = 'tx_mooxnews_pi1[overwriteDemand][dateFrom]';
	$GLOBALS['TYPO3_CONF_VARS']['FE']['cacheHash']['excludedParameters'][] = 'tx_mooxnews_pi1[overwriteDemand][dateTo]';
	$GLOBALS['TYPO3_CONF_VARS']['FE']['cacheHash']['excludedParameters'][] = 'tx_mooxnews_pi1[overwriteDemand][includeSubCategories]';
	$GLOBALS['TYPO3_CONF_VARS']['FE']['cacheHash']['excludedParameters'][] = 'tx_mooxnews_pi1[overwriteDemand][categoryConjunction]';
	$GLOBALS['TYPO3_CONF_VARS']['FE']['cacheHash']['excludedParameters'][] = 'tx_mooxnews_pi1[overwriteDemand][categories]';
	$GLOBALS['TYPO3_CONF_VARS']['FE']['cacheHash']['excludedParameters'][] = 'tx_mooxnews_pi1[overwriteDemand][year]';
	$GLOBALS['TYPO3_CONF_VARS']['FE']['cacheHash']['excludedParameters'][] = 'tx_mooxnews_pi1[overwriteDemand][month]';
	$GLOBALS['TYPO3_CONF_VARS']['FE']['cacheHash']['excludedParameters'][] = 'tx_mooxnews_pi1[@widget_0][currentPage]';
	$GLOBALS['TYPO3_CONF_VARS']['FE']['cacheHash']['excludedParameters'][] = 'tx_mooxnews_pi2[action]';
	$GLOBALS['TYPO3_CONF_VARS']['FE']['cacheHash']['excludedParameters'][] = 'tx_mooxnews_pi2[controller]';
	$GLOBALS['TYPO3_CONF_VARS']['FE']['cacheHash']['excludedParameters'][] = 'tx_mooxnews_pi2[file]';
	$GLOBALS['TYPO3_CONF_VARS']['FE']['cacheHash']['excludedParameters'][] = 'tx_mooxnews_pi2[page]';
	$GLOBALS['TYPO3_CONF_VARS']['FE']['cacheHash']['excludedParameters'][] = 'tx_mooxnews_pi2[hash]';
	$GLOBALS['TYPO3_CONF_VARS']['FE']['cacheHash']['excludedParameters'][] = 'tx_mooxnews_pi2[pre]';
	$GLOBALS['TYPO3_CONF_VARS']['FE']['cacheHash']['excludeAllEmptyParameters'] = true;
	*/

	// include pageTS
	\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig('<INCLUDE_TYPOSCRIPT: source="FILE:EXT:moox_news/Configuration/TSconfig/moox_news.typoscript">');
};

$boot();
unset($boot);
