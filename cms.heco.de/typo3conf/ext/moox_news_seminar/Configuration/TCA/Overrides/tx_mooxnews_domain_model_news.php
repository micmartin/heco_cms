<?php
defined('TYPO3_MODE') or die();

// set default language file as ll-reference
$extConf = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Configuration\ExtensionConfiguration::class)->get('moox_news_seminar');

// set default language file as ll-reference
$ll = 'LLL:EXT:moox_news_seminar/Resources/Private/Language/locallang_db.xlf:';

// add new news type to type selector
$GLOBALS['TCA']['tx_mooxnews_domain_model_news']['columns']['type']['config']['items'][] = [
	$ll.'tx_mooxnewsseminar_domain_model_news',
	'moox_news_seminar',
	'ext-news-type-mooxnewsseminar'
];

// add new news fields to field arrays
//\Neulandlotsen\MooxCore\Utility\TcaHelper::addFieldsToArray("tx_mooxnews_domain_model_news","additionalFrontendDemandFields",[]);
//\Neulandlotsen\MooxCore\Utility\TcaHelper::addFieldsToArray("tx_mooxnews_domain_model_news","additionalFrontendDetailFields",[]);
//\Neulandlotsen\MooxCore\Utility\TcaHelper::addFieldsToArray("tx_mooxnews_domain_model_news","additionalFrontendSortingFields",[]);
\Neulandlotsen\MooxCore\Utility\TcaHelper::addFieldsToArray(
	"tx_mooxnews_domain_model_news",
	"additionalFrontendSortingFields", [
		'moox_news_seminar_startdate',
		'moox_news_seminar_enddate',
	]
);
\Neulandlotsen\MooxCore\Utility\TcaHelper::addFieldsToArray(
	"tx_mooxnews_domain_model_news",
	"additionalFrontendDateFields",[
		'moox_news_seminar_startdate',
		'moox_news_seminar_enddate',
	]
);
\Neulandlotsen\MooxCore\Utility\TcaHelper::addFieldsToArray(
	"tx_mooxnews_domain_model_news",
	"allowedBackendListViewFields",[
		'moox_news_seminar_startdate',
		'moox_news_seminar_enddate',
		'moox_news_seminar_register_deadline'
	]
);
\Neulandlotsen\MooxCore\Utility\TcaHelper::addBackendPreviewFieldsSet(
	"tx_mooxnews_domain_model_news",
	"moox_news_seminar",[
		'moox_news_seminar_startdate',
		'moox_news_seminar_enddate',
		'moox_news_seminar_register_deadline',
		'moox_news_seminar_register_all_year',
		'moox_news_seminar_price',
		'moox_news_seminar_price_info',
		'moox_news_seminar_speakers'
	],
	'moox_news',
	[
		'datetime',
		'event_starttime',
		'event_endtime',
	]
);
\Neulandlotsen\MooxCore\Utility\TcaHelper::addSearchFieldsSet(
	"tx_mooxnews_domain_model_news",
	"moox_news_seminar",[
		'moox_news_seminar_price_info'
	],
	'moox_news'
);
\Neulandlotsen\MooxCore\Utility\TcaHelper::addFieldsToArray(
	"tx_mooxnews_domain_model_news",
	"allowedGrabberFields", [
		'moox_news_seminar_startdate',
		'moox_news_seminar_enddate',
		'moox_news_seminar_register_deadline',
		'moox_news_seminar_price',
		'moox_news_seminar_price_info',
		'moox_news_seminar_subscriber_min',
		'moox_news_seminar_subscriber_max',
		'moox_news_seminar_search_index',
		'moox_news_seminar_register_deadline',
	]
);
//\Neulandlotsen\MooxCore\Utility\TcaHelper::addSearchFieldSet("tx_mooxnews_domain_model_news","moox_news_seminar",[]);
//\Neulandlotsen\MooxCore\Utility\TcaHelper::addDateTimeDefaultField("tx_mooxnews_domain_model_news","moox_news_seminar","");
//\Neulandlotsen\MooxCore\Utility\TcaHelper::addFieldsToArray("tx_mooxnews_domain_model_news","additionalFrontendDemandFields",[]);
/*
\Neulandlotsen\MooxCore\Utility\TcaHelper::addFieldsToList(
	"tx_mooxnews_domain_model_news",
	"requestUpdate",[
		'moox_news_seminar_startdate',
		'moox_news_seminar_enddate'
	]
);
*/

$tx_mooxnewsseminar_domain_model_news = [	
	'moox_news_seminar_startdate' => [
		'exclude' => true,
		// show field only for new news type
		'displayCond' => 'FIELD:type:=:moox_news_seminar',
		/*'onChange' => 'reload',*/
		'label' => $ll.'tx_mooxnewsseminar_domain_model_news.startdate',
		'config' => [
			'type' => 'input',
			'eval' => 'date',
			'checkbox' => 1,
		],
		// special moox configuration
		'moox' => [
			'extkey' => 'moox_news_seminar',
			'backend' => [
				'type' => 'date',
			],
			'sortable' => 1,
			'oppositeField' => 'moox_news_seminar_enddate'
		],
	],
	'moox_news_seminar_enddate' => [
		'exclude' => true,
		// show field only for new news type
		'displayCond' => 'FIELD:type:=:moox_news_seminar',
		/*'onChange' => 'reload',*/
		'label' => $ll.'tx_mooxnewsseminar_domain_model_news.enddate',
		'config' => [
			'type' => 'input',
			'eval' => 'date',
			'checkbox' => 1,
		],
		// special moox configuration
		'moox' => [
			'extkey' => 'moox_news_seminar',
			'backend' => [
				'type' => 'date',
			],
			'sortable' => 1,
		],
	],
	/*
	'moox_news_seminar_timing' => [
		'exclude' => 1,
		// show field only for new news type
		'displayCond' => 'FIELD:type:=:moox_news_seminar',
		'label' => $ll.'tx_mooxnewsseminar_domain_model_news.timing',
		'config' => [
			'type' => 'inline',
			'foreign_table' => 'tx_mooxnews_domain_model_timing',
			'foreign_field' => 'parent',			
			'appearance' => [
				'collapseAll' = TRUE,
				'newRecordLinkTitle' => ' Uhrzeit hinzufügen',
				'useSortable' => FALSE,				
				'showPossibleLocalizationRecords' => FALSE,
				'showRemovedLocalizationRecords' => FALSE,
				'showSynchronizationLink' => FALSE,
				'showAllLocalizationLink' => FALSE,
				'enabledControls' => [
					'info' => FALSE,
					'new' => TRUE,
					'dragdrop' => FALSE,
					'sort' => FALSE,
					'hide' => FALSE,
					'delete' => TRUE,
					'localize' => FALSE
				],
			],
		],
		// special moox configuration
		'moox' => [
			'extkey' => 'moox_news_seminar',
			'backend' => [
				'type' => 'list',
				'icons' => [
					'default' => 'list_icon_moox_news_seminar'
				]
			],
		],
	],
	*/
	'moox_news_seminar_price' => [
		'exclude' => true,
		// show field only for new news type
		'displayCond' => 'FIELD:type:=:moox_news_seminar',
		'label' => $ll.'tx_mooxnewsseminar_domain_model_news.price',
		'config' => [
			'type'     => 'input',
			'size'     => '8',
			'max'      => '13',
			'eval'     => 'double2',
			'checkbox' => '0',
			'default'  => '0'
		],
		// special moox configuration
		'moox' => [
			'extkey' => 'moox_news_seminar',
			'backend' => [
				'type' => 'currency',
			],
		],
	],
	'moox_news_seminar_price_info' => [		
		'exclude' => true,
		// show field only for new news type
		'displayCond' => 'FIELD:type:=:moox_news_seminar',
		'label' => $ll.'tx_mooxnewsseminar_domain_model_news.price_info',		
		'config' => [
			'type'     => 'text',
			'cols'     => '40',
			'rows'     => '15'
		],
		// special moox configuration
		'moox' => [
			'extkey' => 'moox_news_seminar',
			'backend' => [
				'type' => 'text',
			],
		],
	],
	'moox_news_seminar_subscriber_min' => [
		'exclude' => true,
		// show field only for new news type
		'displayCond' => 'FIELD:type:=:moox_news_seminar',
		'label' => $ll.'tx_mooxnewsseminar_domain_model_news.subscriber_min',
		'config' => [
			'type' => 'input',
			'size' => 8,
			'eval' => 'int'
		],
		// special moox configuration
		'moox' => [
			'extkey' => 'moox_news_seminar',			
		],
	],
	'moox_news_seminar_subscriber_max' => [
		'exclude' => true,
		// show field only for new news type
		'displayCond' => 'FIELD:type:=:moox_news_seminar',
		'label' => $ll.'tx_mooxnewsseminar_domain_model_news.subscriber_max',
		'config' => [
			'type' => 'input',
			'size' => 8,
			'eval' => 'int'
		],
		// special moox configuration
		'moox' => [
			'extkey' => 'moox_news_seminar',			
		],
	],
	'moox_news_seminar_speakers' => [
		'exclude' => true,
		// show field only for new news type
		'displayCond' => 'FIELD:type:=:moox_news_seminar',
		'label' => $ll.'tx_mooxnewsseminar_domain_model_news.speakers',
		/*'onChange' => 'reload',*/
		'config' => [
			'type' => 'select',
			'renderType' => 'selectMultipleSideBySide',
			'internal_type' => 'db',
			'allowed' => 'tx_mooxnewsseminar_domain_model_speaker',	
			'foreign_table' => 'tx_mooxnewsseminar_domain_model_speaker',
			'foreign_table_where' => 'AND tx_mooxnewsseminar_domain_model_speaker.deleted=0 AND tx_mooxnewsseminar_domain_model_speaker.hidden=0 ORDER BY tx_mooxnewsseminar_domain_model_speaker.title',
			'itemsProcFunc' => 'Neulandlotsen\MooxNewsSeminar\Hooks\ItemsProcFunc->user_speakers',
			'size' => 5,
			'maxitems' => 10,
			'multiple' => 1,
			'enableMultiSelectFilterTextfield' => true,
			'fieldControl' => [
				'editPopup' => [
					'disabled' => true,
					'options' => [
						'title' => 'Referent bearbeiten',
					],
				],
				'addRecord' => [
					'disabled' => false,
					'options' => [
						'title' => 'Referent hinzufügen',
						'setValue' => 'prepend',
					],
				],
				'listModule' => [
					'disabled' => false,
					'options' => [
						'title' => 'Referenten anzeigen',
					],
				],
			],
		],
		// special moox configuration
		'moox' => [
			'extkey' => 'moox_news_seminar',
			'backend' => [
				'type' => 'list',
				'icons' => [
					'default' => 'list_icon_moox_news_seminar'
				]
			],
		],
	],
	'moox_news_seminar_register_countdown' => [
		'exclude' => true,
		// show field only for new news type
		'displayCond' => 'FIELD:type:=:moox_news_seminar',
		'label' => $ll.'tx_mooxnewsseminar_domain_model_news.register_countdown',
		'config' => [
			'type' => 'input',
			'size' => 1,
			'eval' => 'int'
		],
		// special moox configuration
		'moox' => [
			'extkey' => 'moox_news_seminar',			
		],
	],
	'moox_news_seminar_register_deadline' => [
		'exclude' => true,
		// show field only for new news type
		'displayCond' => [
			'AND' => [
				'FIELD:type:=:moox_news_seminar',
				'FIELD:moox_news_seminar_register_all_year:REQ:false',
			],
		],
		'l10n_mode' => 'mergeIfNotBlank',
		'label' => $ll.'tx_mooxnewsseminar_domain_model_news.register_deadline',
		'config' => [
			'type' => 'input',
			'eval' => 'date',
			'checkbox' => 1,
		],		
		// special moox configuration
		'moox' => [
			'extkey' => 'moox_news_seminar',
			'backend' => [
				'type' => 'date',
			],
		],
	],
	'moox_news_seminar_register_all_year' => [
		'exclude' => 0,
		'displayCond' => 'FIELD:type:=:moox_news_seminar',
		'label' => $ll.'tx_mooxnewsseminar_domain_model_news.register_all_year',
		'onChange' => 'reload',
		'config' => [
			'type' => 'check',
			'default' => 0
		],
		// special moox configuration
		'moox' => [
			'extkey' => 'moox_news_seminar',
			'backend' => [
				'type' => 'boolean',
			],
		],
	],
	'moox_news_seminar_search_index' => [
		'label' => $ll.'tx_mooxnewsseminar_domain_model_news.search_index',
		'config' => [
			'type' => 'passthrough'
		],
		// special moox configuration
		'moox' => [
			'extkey' => 'moox_news_seminar',				
		],
	],
];

// extend moox_news tca with new fields from this extension
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns(
	'tx_mooxnews_domain_model_news',
	$tx_mooxnewsseminar_domain_model_news,
	1
);

// extend moox_news tca with new palette fields from this extension
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addFieldsToPalette(
	'tx_mooxnews_domain_model_news',
	'paletteSeminarDate',	'moox_news_seminar_startdate,moox_news_seminar_register_deadline,--linebreak--,moox_news_seminar_enddate,moox_news_seminar_register_all_year,',
	'after:archive'
);

// extend moox_news tca with new palette fields from this extension
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addFieldsToPalette(
	'tx_mooxnews_domain_model_news',
	'paletteSeminarTime',
	'moox_news_seminar_timing'
);

// extend moox_news tca with new palette fields from this extension
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addFieldsToPalette(
	'tx_mooxnews_domain_model_news',
	'paletteSeminarLimits',
	'moox_news_seminar_subscriber_min,moox_news_seminar_subscriber_max'
);

// extend moox_news tca with new palette fields from this extension
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addFieldsToPalette(
	'tx_mooxnews_domain_model_news',
	'palettePrice',
	'moox_news_seminar_price,--linebreak--,moox_news_seminar_price_info',
	'before:archive'
);

// place new fields in backend form - optionally generate new tab for extended fields
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes(
	'tx_mooxnews_domain_model_news',
	'--div--;' . $ll.'tx_mooxnewsseminar_domain_model_news,moox_news_seminar_speakers,--palette--;Preisinformationen;palettePrice,--palette--;Teilnehmereinstellungen;paletteSeminarLimits',
	'',
	'after:tags'
);

// place new fields in backend form - optionally generate new tab for extended fields
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes(
	'tx_mooxnews_domain_model_news',
	'--palette--;Uhrzeiten des Seminars;paletteSeminarTime',
	'',
	'after:moox_news_seminar_price'
);
// place new fields in backend form - optionally generate new tab for extended fields
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes(
	'tx_mooxnews_domain_model_news',
	'--palette--;Datumsinformationen des Seminars;paletteSeminarDate',
	'',
	'after:archive'
);

// hide news default fields for new news type 
\Neulandlotsen\MooxCore\Utility\TcaHelper::hideTcaFields(
	"datetime,event_starttime,event_endtime", 
	"tx_mooxnews_domain_model_news", 
	"moox_news_seminar"
);