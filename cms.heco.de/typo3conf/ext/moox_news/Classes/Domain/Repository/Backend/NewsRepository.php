<?php
namespace Neulandlotsen\MooxNews\Domain\Repository\Backend;

/**
 * This file is part of the "moox_news" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

use TYPO3\CMS\Extbase\Persistence\QueryInterface; 
use Neulandlotsen\MooxNews\Domain\Repository\MooxRepository;

class NewsRepository extends MooxRepository
{	
	/**
	 * Returns a constraint array created by a given filter array
	 *
	 * @param \TYPO3\CMS\Extbase\Persistence\QueryInterface $query
	 * @param array $filter
	 * @param array $constraints	
	 * @return \TYPO3\CMS\Extbase\Persistence\Generic\Qom\ConstraintInterface|null
	 */
	protected function createFilterConstraints(\TYPO3\CMS\Extbase\Persistence\QueryInterface $query,$filter = NULL,$constraints = NULL)
	{				
		if(is_null($constraints))
		{			
			$constraints = [];			
		}

		if(isset($filter['uids']) && is_array($filter['uids']))
		{			
			$constraints[] = $query->in('uid', $filter['uids']);			
		}
		
		if(isset($filter['hidden']) && in_array($filter['hidden'],[0,1]))
		{			
			$constraints[] = $query->equals('hidden', $filter['hidden']);			
		}
		
		if(isset($filter['istopnews']) && in_array($filter['istopnews'],[0,1]))
		{			
			$constraints[] = $query->equals('istopnews', $filter['istopnews']);			
		}
		
		if(isset($filter['type']) && $filter['type']!="")
		{			
			$constraints[] = $query->equals('type', $filter['type']);			
		}
				
		if(isset($filter['group']) && $filter['group']!="")
		{			
			if($filter['group']=='none')
			{
				$constraints[] = $query->equals('usergroup', '');
			} 
			else 
			{
				$constraints[] = $query->contains('usergroup', $filter['group']);
			}
		}
		
		if(isset($filter['categories']) && is_array($filter['categories']) && count($filter['categories']))
		{						
			$categoryConstraints = [];
			
			foreach($filter['categories'] AS $category){
				$categoryConstraints[] = $query->contains('categories', $category);
			}		
			
			if(count($categoryConstraints)>0)
			{
				if(count($categoryConstraints)==1)
				{
					$constraints[] = $categoryConstraints[0];
				} 
				else 
				{
					$constraints[] = $query->logicalOr($categoryConstraints);
				}
			}
		}
		
		if(isset($filter['year']) && is_numeric($filter['year']) && $filter['year']>0)
		{						
			$constraints[] = $query->greaterThanOrEqual('datetime', mktime(0,0,0,1,1,$filter['year']));	
			$constraints[] = $query->lessThan('datetime', mktime(0,0,0,1,1,($filter['year'])+1));
		}
				
		if(isset($filter['query']) && $filter['query']!="")
		{						
			$searchConstraints = [];
			
			if(substr($filter['query'],0,1)=="#" && is_numeric(substr($filter['query'],1))){
				
				$constraints[] =  $query->equals('uid', (int)substr($filter['query'],1));
				
			} else {
			
				$helperService = $this->objectManager->get('Neulandlotsen\MooxNews\Service\HelperService');
				$searchFields = $helperService->getSearchFields("tx_mooxnews_domain_model_news",$filter['type'],$this->settings);
				
				foreach($searchFields AS $searchField)
				{
					$searchConstraints[] =  $query->like($searchField, "%".$filter['query']."%");
				}
				
				if(count($searchConstraints)>0)
				{
					if(count($searchConstraints)==1)
					{
						$constraints[] = $searchConstraints[0];
					} 
					else 
					{
						$constraints[] = $query->logicalOr($searchConstraints);
					}
				}	
			}
		}
		
		// get post save functions from external extensions
		$additionalFilterFunctions = [];
		$functions = get_class_methods(get_class($this));
		foreach($functions AS $function)
		{
			if(substr($function,0,17)=="additionalFilter_")
			{
				$additionalFilterFunctions[] = $function;
			}
		}

		foreach($additionalFilterFunctions AS $additionalFilterFunction)
		{
			if($additionalFilter = $this->$additionalFilterFunction($query,$filter))
			{
				if($additionalFilter){
					$constraints[] = $additionalFilter;
				}
			}
		}
		
		if(count($constraints)<1)
		{			
			$constraints = NULL;			
		}
		
		return $constraints;
	}
	
	/**
	 * Finds all news (overwrite)
	 *	
	 * @param array $storagePids
	 * @param array $filter
	 * @return \TYPO3\CMS\Extbase\Persistence\QueryResultInterface
	 */
	public function findAll($storagePids = [], $filter = [])
	{		
		$query = $this->createQuery();
		
		if(is_numeric($storagePids) && $storagePids>0)
		{
			$storagePids = [$storagePids];
		}
		
		if(!is_array($storagePids) || count($storagePids)<1)
		{
			$storagePids = [0];			
		}
		
		$query->getQuerySettings()->setIgnoreEnableFields(TRUE);
		$query->getQuerySettings()->setStoragePageIds($storagePids);

		if(isset($filter['language']) && is_numeric($filter['language']))
		{
			$query->getQuerySettings()->setLanguageUid($filter['language']);
		}
		
		$constraints = [];
				
		$constraints[] = $query->equals('deleted', 0);
				
		$filter['query'] = trim($filter['query']);
		if($filter['query']!="")
		{
			$constraints[] = $query->logicalOr(	
				$query->like('title', "%".$filter['query']."%"),
				$query->like('teaser', "%".$filter['query']."%"),
				$query->like('bodytext', "%".$filter['query']."%")
			);
		}
		
		if($filter['sortDirection']=="DESC")
		{
			$filter['sortDirection'] = QueryInterface::ORDER_DESCENDING;
		} 
		else 
		{
			$filter['sortDirection'] = QueryInterface::ORDER_ASCENDING;
		}
		
		$sortFields = explode("|",$filter['sortField']);
		if(count($sortFields)>1)
		{
			$sortOrderings = [];
			foreach($sortFields AS $sortField)
			{
				$sortOrderings = array_merge($sortOrderings,[$sortField => $filter['sortDirection']]);				
			}
			$query->setOrderings ($sortOrderings);
		} 
		else 
		{
			$query->setOrderings ([$filter['sortField'] => $filter['sortDirection']]);
		}
				
		return $query->matching(
			$query->logicalAnd($constraints)
		)->execute();
	}
	
	/**
	 * Override default findByUid function to enable also the option to turn of
	 * the enableField setting
	 *
	 * @param int $uid
	 * @param bool $respectEnableFields if set to false, hidden records are shown
	 * @return \Neulandlotsen\MooxNews\Domain\Model\News
	 */
	public function findByUid($uid, $respectEnableFields = TRUE)
	{
		$query = $this->createQuery();
		$query->getQuerySettings()->setRespectStoragePage(FALSE);
		$query->getQuerySettings()->setRespectSysLanguage(FALSE);
		$query->getQuerySettings()->setIgnoreEnableFields(!$respectEnableFields);

		return $query->matching(
			$query->logicalAnd(
				$query->equals('uid', $uid),
				$query->equals('deleted', 0)
			)
		)->execute()->getFirst();
	}
	
	/**
	 * @return \TYPO3\CMS\Extbase\Persistence\QueryResultInterface
	 */
	public function findReallyAll()
	{
		$query = $this->createQuery();
		$query->getQuerySettings()->setRespectStoragePage(FALSE);
		$query->getQuerySettings()->setRespectSysLanguage(FALSE);
		$query->getQuerySettings()->setIgnoreEnableFields(TRUE);

		return $query->execute();
	}
}
?>