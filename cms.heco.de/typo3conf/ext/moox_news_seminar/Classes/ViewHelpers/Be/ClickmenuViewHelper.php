<?php
namespace Neulandlotsen\MooxNewsSeminar\ViewHelpers\Be;

/**
 * This file is part of the "moox_news_seminar" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */
 
class ClickmenuViewHelper extends \TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper
{
    /**
     * @var bool
     */
    protected $escapeOutput = false;
    
    /**
     * Render the clickmenu
     *
     * @param string $table Name of the table
     * @param int $uid uid of the record
     * @return string
     */
    public function render($table, $uid)
    {
        return $this->wrapClickMenuOnIcon($this->renderChildren(), $table, $uid);
    }

    /**
     * @param $str
     * @param $table
     * @param string $uid
     * @return string
     */
    private function wrapClickMenuOnIcon($str, $table, $uid = '')
    {
        $listFr = 1;
        $addParams = '';
        $enDisItems = '';
        $returnOnClick = false;

        $backPath = rawurlencode($GLOBALS['BACK_PATH']) . '|' . \TYPO3\CMS\Core\Utility\GeneralUtility::shortMD5($GLOBALS['BACK_PATH'] . '|' . $GLOBALS['TYPO3_CONF_VARS']['SYS']['encryptionKey']);
        $onClick = 'Clickmenu.show("' . $table . '","' . $uid . '","' . $listFr . '","' . str_replace('+', '%2B',
                $enDisItems) . '","' . str_replace('&', '&amp;', addcslashes($backPath, '"')) . '","' . str_replace('&',
                '&amp;', addcslashes($addParams, '"')) . '");return false;';
        return $returnOnClick ? $onClick : '<a href="#" onclick="' . htmlspecialchars($onClick) . '" oncontextmenu="' . htmlspecialchars($onClick) . '">' . $str . '</a>';
    }
}
