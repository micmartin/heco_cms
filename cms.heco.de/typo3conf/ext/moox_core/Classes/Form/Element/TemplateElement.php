<?php
namespace Neulandlotsen\MooxCore\Form\Element;

/**
 * This file is part of the "moox_core" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

use http\Env\Request;
use TYPO3\CMS\Backend\Utility\BackendUtility;
use TYPO3\CMS\T3editor\Form\Element\T3editorElement;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface;

/**
 * template FormEngine widget
 */
class TemplateElement extends T3editorElement
{
	/**
	 * @var \TYPO3\CMS\Extbase\Object\ObjectManager
	 */
	protected $objectManager;
	
	/**
	 * @var \TYPO3\CMS\Extbase\Mvc\Web\Routing\UriBuilder
	 */
	protected $uriBuilder;
	
	/**
	 * @var mixed
	 */
	protected $helperService;
	
	/**
	 * @var \TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface
	 */
	protected $configurationManager;

	 /**
     * Render template element
     *
     * @return array As defined in initializeResultArray() of AbstractNode
     */
    public function render():array
    {
		$this->resultArray = parent::render();
		
		if($this->data['databaseRow'][$this->data['fieldName']] != "" && is_array($this->data['parameterArray']['fieldConf']['config']['preview']) && is_numeric($this->data['databaseRow']['uid']) && $this->data['databaseRow']['uid']>0)
		{
			$previewSettings = $this->data['parameterArray']['fieldConf']['config']['preview'];
			
			$this->objectManager = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\CMS\Extbase\Object\ObjectManager');
			$this->uriBuilder = $this->objectManager->get('TYPO3\CMS\Extbase\Mvc\Web\Routing\UriBuilder');
			$this->uriBuilder->initializeObject(); 
			$this->helper = $this->objectManager->get($previewSettings['helperClass']);

			$previewUri = $this->uriBuilder->setTargetPageUid(1)->setArguments(
				['route'=>$previewSettings['M']]
			)->uriFor(
				$previewSettings['action'],
				[
					'uid'=>$this->data['databaseRow']['uid'],
					'field'=>$this->data['fieldName'],
					'previewFormat'=>$previewSettings['format']
				],
				$previewSettings['controller'],
				$previewSettings['extension'],
				strtolower($previewSettings['M'])
			);

			$html = [];
			$html[] = '<div class="form-group" style="margin-top:15px">';
			$html[] = 	'<label for="preview">Vorschau der Vorlage</label>';
			$html[] = 	'<div class="form-control-static">';
			$html[] = 		'<div class="panel panel-default">';
			$html[] = 			'<div class="panel-heading">'.$this->helper->prepareMailSubject($this->data['databaseRow']['subject'],$this->helper->subjectDummyData).'</div>';
			$html[] = 			'<div class="panel-body">';
			$html[] = 				'<iframe style="min-height:800px;width:100%" src="'.$previewUri.'" marginheight="0" marginwidth="0" frameborder="0"></iframe>';
			$html[] = 			'</div>';
			$html[] = 		'</div>';
			$html[] = 	'</div>';		
			$html[] = '</div>';

			$this->resultArray['html'] .= implode(LF, $html);
		}
		
		return $this->resultArray;
	}
}
