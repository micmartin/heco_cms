<?php
namespace Neulandlotsen\MooxMailer\Domain\Model;

/**
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */
 
class News extends \Neulandlotsen\MooxNews\Domain\Model\News
{
	
	/**
	 * @var int
	 */
	protected $mailerFrequency;
	
	/**
	 * @var string
	 */
    protected $mailerTags;
	
	/**
	 * @var string
	 */
    protected $mailerZipregions;

    /**
     * @var string
     */
    protected $mailerTitle;

    /**
     * @var string
     */
    protected $mailerTeaser;

    /**
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Neulandlotsen\MooxNews\Domain\Model\FileReference>
     * @TYPO3\CMS\Extbase\Annotation\ORM\Lazy
     */
    protected $mailerFiles;
	
	/**
	 * @return int
	 */
	public function getMailerFrequency()
	{
		return $this->mailerFrequency;
	}

	/**
	 * @param int $mailerFrequency mailer frequency
	 * @return void
	 */
	public function setMailerFrequency($mailerFrequency)
	{
		$this->mailerFrequency = $mailerFrequency;
	}
	
	/**
	 * @return string $mailerTags
	 */
	public function getMailerTags()
	{
		return $this->mailerTags;
	}

	/**
	 * @param string $mailerTags
	 * @return void
	 */
	public function setMailerTags($mailerTags)
	{
		$this->mailerTags = $mailerTags;
	}
	
	/**
	 * @return string $mailerZipregions
	 */
	public function getMailerZipregions()
	{
		return $this->mailerZipregions;
	}

	/**
	 * @param string $mailerZipregions
	 * @return void
	 */
	public function setMailerZipregions($mailerZipregions)
	{
		$this->mailerZipregions = $mailerZipregions;
	}

    /**
     * @return string $mailerTitle
     */
    public function getMailerTitle()
    {
        return $this->mailerTitle;
    }

    /**
     * @param string $mailerTitle
     * @return void
     */
    public function setMailerTitle($mailerTitle)
    {
        $this->mailerTitle = $mailerTitle;
    }

    /**
     * @return string $mailerTeaser
     */
    public function getMailerTeaser()
    {
        return $this->mailerTeaser;
    }

    /**
     * @param string $mailerTeaser
     * @return void
     */
    public function setMailerTeaser($mailerTeaser)
    {
        $this->mailerTeaser = $mailerTeaser;
    }

    /**
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Neulandlotsen\MooxNews\Domain\Model\FileReference>
     */
    public function getMailerFiles()
    {
        return $this->mailerFiles;
    }

    /**
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage $mailerFiles
     */
    public function setMailerFiles($mailerFiles)
    {
        $this->mailerFiles = $mailerFiles;
    }
}
