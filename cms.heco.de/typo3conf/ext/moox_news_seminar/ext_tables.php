<?php
if (!defined('TYPO3_MODE')) {
	die ('Access denied.');
}

if (TYPO3_MODE === 'BE')
{
	/**
	 * Registers a Backend Module
	 */
	\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerModule(
		'Neulandlotsen.' . $_EXTKEY,
		'MooxNewsMooxdb',	// Make module a submodule of 'moox'
		'SeminarManagement',	// Submodule key
		'after:MooxNewsNewsmanagement',	// Position
		[
			'Mod1' => 'list,refresh,filter,ordering,move,multiple,duplicate,copy,addNewsByTca,import,populate,ics',		
		],
		[
			'access' => 'user,group',
			'icon'   => 'EXT:' . $_EXTKEY . '/Resources/Public/Icons/module_seminarmanagement.svg',
			'labels' => 'LLL:EXT:' . $_EXTKEY . '/Resources/Private/Language/locallang_mod1.xlf',
		]
	);	
	
	/**
	 * Registers a Backend Module
	 */
	\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerModule(
		'Neulandlotsen.' . $_EXTKEY,
		'MooxNewsMooxdb',	// Make module a submodule of 'moox'
		'SpeakerManagement',	// Submodule key
		'after:MooxNewsTagmanagement',	// Position
		[
			'Mod2' => 'list,refresh,filter,ordering,move',		
		],
		[
			'access' => 'user,group',
			'icon'   => 'EXT:' . $_EXTKEY . '/Resources/Public/Icons/module_speakermanagement.svg',
			'labels' => 'LLL:EXT:' . $_EXTKEY . '/Resources/Private/Language/locallang_mod2.xlf',
		]
	);	
	
}

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile($_EXTKEY, 'Configuration/TypoScript', 'MOOX Seminars for MOOX News');

?>