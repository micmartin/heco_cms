<?php
namespace Neulandlotsen\MooxNewsSeminar\ViewHelpers;

/**
 * This file is part of the "moox_news_seminar" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper;
use TYPO3\CMS\Core\Utility\GeneralUtility; 

class GetValueViewHelper extends AbstractViewHelper
{
	/**
	 * @param object $item current item object
	 * @param string $field
	 * @param string $transform	
	 * @param string $unit	
	 * @param bool $raw
	 * @return string
	 */
	public function render($item, $field, $transform = NULL, $unit = NULL, $raw = false)
	{
		$value = "";
		if($field != "")
		{
			$getCall = "get".GeneralUtility::underscoredToUpperCamelCase($field);
			if(method_exists($item, $getCall))
			{
				if($raw)
				{
					$value = $item->$getCall();
				} 
				else 
				{
					$value = $item->$getCall(true);
				}
			} 
			elseif(is_array($item))
			{
				$value = $item[$field];
			} 
		}
		
		if($transform=="currency")
		{
			$value = number_format($value+0, 2, ',', '.');
		} 
		elseif($transform=="float")
		{
			$value = number_format($value+0, 4, ',', '');
		}
		
		if($unit!="")
		{
			$value .= " ".$unit;
		}
		
		return $value;
	}
}