# create databases
CREATE DATABASE IF NOT EXISTS `typo3_cms` CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

# create root user and grant rights
#CREATE USER 'root'@'localhost' IDENTIFIED BY 'local';
GRANT ALL PRIVILEGES ON *.* TO 'root'@'%';