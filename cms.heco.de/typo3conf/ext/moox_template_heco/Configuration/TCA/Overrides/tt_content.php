<?php
$mooxTemplateHecoElements = [
	'mooxtemplateheco_backlinktile',
	'mooxtemplateheco_contactbox',
	'mooxtemplateheco_contactboxslides',
	'mooxtemplateheco_exhibitioncalendar',
	'mooxtemplateheco_hecocolumns',
	'mooxtemplateheco_locationbox',
	'mooxtemplateheco_socialtile',
	'mooxtemplateheco_teaser',
	'mooxtemplateheco_tile'
];
$fieldsToHideFromMooxTemplateHecoElements = [
	'subheader',
	'header_link',
	'date',
	'categories'
];
foreach ($fieldsToHideFromMooxTemplateHecoElements AS $field)
{
	if(!is_array($GLOBALS['TCA']['tt_content']['columns'][$field]['displayCond']['AND']))
	{
		$GLOBALS['TCA']['tt_content']['columns'][$field]['displayCond']['AND'] = [];
	}
	foreach ($mooxTemplateHecoElements AS $element)
	{
		$GLOBALS['TCA']['tt_content']['columns'][$field]['displayCond']['AND'][] = 'FIELD:CType:!=:'.$element;
	}
}
?>