/*
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */

/**
 * Initialization of the selectSortableCategoryTreeElement js component used e.g. for category tree rendering
 */
define(['jquery', 'TYPO3/CMS/MooxNews/FormEngine/Element/SelectSortableCategoryTreeElement'], function ($, SelectSortableCategoryTreeElement) {
    'use strict';

    var SelectSortableCategoryTreeElement = {
    };
    SelectSortableCategoryTreeElement.initialize = function (target) {
        
		var selectSortableCategoryTreeElementTarget = $("#"+target);
		
		mooxNewsSelectSortableCategoryTreeElementUpdateList(selectSortableCategoryTreeElementTarget);
		
		$(document).on("click","li.tree-selector-item span.do-add",function() {			
			mooxNewsSelectSortableCategoryTreeElementAdd(selectSortableCategoryTreeElementTarget,$(this).parent("li").data('value'),$(this).parent("li").data("title"));
		});
		$(document).on("click","li.tree-selector-item span.not-main",function() {			
			mooxNewsSelectSortableCategoryTreeElementAddAsMain(selectSortableCategoryTreeElementTarget,$(this).parent("li").data('value'),$(this).parent("li").data("title"));
			$("li.tree-selector-item span.is-main").addClass('hidden');
			$("li.tree-selector-item span.not-main").removeClass('hidden');
			$(this).parent("li").find("span.is-main").removeClass('hidden');
			$(this).parent("li").find("span.not-main").addClass('hidden');
		});
		$(document).on("click",".buttons-"+target,function() {			
			mooxNewsSelectSortableCategoryTreeElementUpdateList(selectSortableCategoryTreeElementTarget);
			mooxNewsSelectSortableCategoryTreeElementUpdateTree(selectSortableCategoryTreeElementTarget);
		});
    };
    return SelectSortableCategoryTreeElement;
});

function mooxNewsSelectSortableCategoryTreeElementAdd(target,value,label)
{	
	alreadyExisting = false;
	
	target.find("option").each(function(){
		if($(this).attr('value')==value){
			alreadyExisting = true;			
		}	
	});
	
	target.val([]);	
	
	if(!alreadyExisting){
		target.append( new Option(label,value,false,true) );	
	}

	mooxNewsSelectSortableCategoryTreeElementUpdateList(target);
	mooxNewsSelectSortableCategoryTreeElementUpdateHidden(target);
}

function mooxNewsSelectSortableCategoryTreeElementAddAsMain(target,value,label)
{	
	mooxNewsSelectSortableCategoryTreeElementAdd(target,value,label);
	$(".buttons-"+target.attr('id')+" .t3js-btn-moveoption-top").click();
}

function mooxNewsSelectSortableCategoryTreeElementUpdateList(target)
{	
	if(!target.hasClass('is-flexform')){
	target.find("option").each(function(){
		$(this).text($(this).text().replace(/ \(Hauptkategorie\)/g,''));
	});
	target.find("option").first().each(function(){
		$(this).text($(this).text()+ " (Hauptkategorie)");
	});
	}
}

function mooxNewsSelectSortableCategoryTreeElementUpdateTree(target)
{	
	$("li.tree-selector-item span.is-main").addClass('hidden');
	$("li.tree-selector-item span.not-main").removeClass('hidden');
	target.find("option").first().each(function(){
		$("li.tree-selector-item-"+$(this).val()+" span.is-main").removeClass('hidden');
		$("li.tree-selector-item-"+$(this).val()+" span.not-main").addClass('hidden');
		
	});
}

function mooxNewsSelectSortableCategoryTreeElementUpdateHidden(target)
{	
	var selectSortableCategoryTreeElementHidden = $("input#"+target.attr('id')+"-hidden");
	var selectSortableCategoryTreeElementHiddenValue = '';
	
	target.find("option").each(function(){
		if(selectSortableCategoryTreeElementHiddenValue!=''){
			selectSortableCategoryTreeElementHiddenValue = selectSortableCategoryTreeElementHiddenValue + ",";
		}
		selectSortableCategoryTreeElementHiddenValue = selectSortableCategoryTreeElementHiddenValue + $(this).val();
	});
	
	selectSortableCategoryTreeElementHidden.val(selectSortableCategoryTreeElementHiddenValue);
	
}