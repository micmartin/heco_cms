<?php

/**
 * Definitions for routes provided by EXT:moox_news
 */
return [
    'news_tag' => [
        'path' => '/news/tag',
        'target' => \Neulandlotsen\MooxNews\Backend\TagEndPoint::class . '::create'
    ],
	'news_target' => [
        'path' => '/news/target',
        'target' => \Neulandlotsen\MooxNews\Backend\TargetEndPoint::class . '::create'
    ]
];
