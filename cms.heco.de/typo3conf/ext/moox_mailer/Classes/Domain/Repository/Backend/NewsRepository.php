<?php
namespace Neulandlotsen\MooxMailer\Domain\Repository\Backend;

/**
 * This file is part of the "moox_mailer" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

class NewsRepository extends \Neulandlotsen\MooxNews\Domain\Repository\Backend\NewsRepository
{	
	/**
	 * Returns additional filter constraint(s)
	 *
	 * @param \TYPO3\CMS\Extbase\Persistence\QueryInterface $query
	 * @param array $filter
	 * @return \TYPO3\CMS\Extbase\Persistence\Generic\Qom\ConstraintInterface|null
	 */
	protected function additionalFilter_mooxNewsSeminar(\TYPO3\CMS\Extbase\Persistence\QueryInterface $query,$filter = NULL,$constraints = NULL)
	{
		if(isset($filter['mailerFrequency']) && $filter['mailerFrequency']!="")
		{			
			return $query->equals('mailerFrequency', $filter['mailerFrequency']);			
		}
	}
}
?>