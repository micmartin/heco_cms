<?php
namespace Neulandlotsen\MooxMailer\Domain\Repository;

/**
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */

use TYPO3\CMS\Extbase\Persistence\QueryInterface;

class ClickRepository extends \TYPO3\CMS\Extbase\Persistence\Repository
{
	
	protected $defaultOrderings = array ('firstClick' => QueryInterface::ORDER_ASCENDING);

	/**
	 * find by job, id, url and fe user uid
	 *
	 * @param string $job job
	 * @param string $id id
	 * @param string $url url
	 * @param int $feUserUid fe user uid
	 * @return \Neulandlotsen\MooxMailer\Domain\Model\Click
	 */
	public function findByJobAndIdAndUrlAndFeUser($job,$id,$url,$feUserUid) 
	{
		$query = $this->createQuery();
		$query->getQuerySettings()->setRespectStoragePage(FALSE);
		$query->getQuerySettings()->setRespectSysLanguage(FALSE);
		$query->getQuerySettings()->setIgnoreEnableFields(TRUE);

		return $query->matching(
			$query->logicalAnd(
				$query->equals('job', $job),
				$query->equals('id', $id),
				$query->equals('url', $url),
				$query->equals('feUser', $feUserUid),
				$query->equals('deleted', 0)
			))->execute()->getFirst();
	}
	
	/**
	 * find by job, id, url and address uid
	 *
	 * @param string $job job
	 * @param string $id id
	 * @param string $url url
	 * @param int $addressUid address uid
	 * @return \Neulandlotsen\MooxMailer\Domain\Model\Click
	 */
	public function findByJobAndIdAndUrlAndAddress($job,$id,$url,$addressUid)
	{
		$query = $this->createQuery();
		$query->getQuerySettings()->setRespectStoragePage(FALSE);
		$query->getQuerySettings()->setRespectSysLanguage(FALSE);
		$query->getQuerySettings()->setIgnoreEnableFields(TRUE);

		return $query->matching(
			$query->logicalAnd(
				$query->equals('job', $job),
				$query->equals('id', $id),
				$query->equals('url', $url),
				$query->equals('address', $addressUid),
				$query->equals('deleted', 0)
			))->execute()->getFirst();
	}
	
	/**
	 * find by job
	 *
	 * @param string $job job
	 * @param bool $raw raw
	 * @param int $limit limit	
	 * @return \TYPO3\CMS\Extbase\Persistence\QueryResultInterface The clicks
	 */
	public function findTopXByJob($job,$raw = false,$limit = 10)
	{
		$query = $this->createQuery();
		$query->getQuerySettings()->setRespectStoragePage(FALSE);
		$query->getQuerySettings()->setRespectSysLanguage(FALSE);

		$stmt = '	SELECT *,SUM(clicks) AS clicks FROM tx_mooxmailer_domain_model_click
					WHERE deleted=0 AND job="'.$job.'"
					GROUP BY job, url, id ORDER BY clicks DESC, url ASC LIMIT '.$limit;
				
		return $query
		  ->statement($stmt)
		  ->execute($raw);
		
	}
	
	/**
	 * find by job
	 *
	 * @param string $job job	
	 * @return \TYPO3\CMS\Extbase\Persistence\QueryResultInterface The clicks
	 */
	public function findAllByJob($job) 
	{
		$query = $this->createQuery();
		$query->getQuerySettings()->setRespectStoragePage(FALSE);
		$query->getQuerySettings()->setRespectSysLanguage(FALSE);
		$query->setOrderings(
			array(
				'clicks' => QueryInterface::ORDER_DESCENDING,
				'url' => QueryInterface::ORDER_ASCENDING
			)
		);
		return $query->matching(
			$query->logicalAnd(
				$query->equals('job', $job),				
				$query->equals('deleted', 0)
			))->execute();
	}
}
?>