<?php
if (!defined('TYPO3_MODE'))
{
	die ('Access denied.');
}

$ll = 'LLL:EXT:moox_news/Resources/Private/Language/locallang_db.xlf:';

/**
 * Add extra fields to the pages record
 */
$newPagesColumns = [
	'news_types' => [
		'exclude' => 1,
		// show field only for new news type
		'displayCond' => 'FIELD:doktype:=:254',
		'l10n_mode' => 'mergeIfNotBlank',
		'label' => $ll . 'pages.news_types',
		'config' => [
			'type' => 'select',			
			'size' => 6,			
			'maxitems' => 999,
			'minitems' => 0,
			'allowNonIdValues' => 1,			
			'itemsProcFunc' => 'Neulandlotsen\MooxNews\Hooks\ItemsProcFunc->user_types',
			'default' => ''
		]
	]
];

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('pages', $newPagesColumns,1);
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes('pages', 'news_types', '', 'after:hidden');

// Override news icon
$GLOBALS['TCA']['pages']['columns']['module']['config']['items'][] = [
    0 => 'LLL:EXT:moox_news/Resources/Private/Language/locallang_be.xlf:news-folder',
    1 => 'mxnews',
    2 => 'apps-pagetree-folder-contains-news'
];
$GLOBALS['TCA']['pages']['ctrl']['typeicon_classes']['contains-mxnews'] = 'apps-pagetree-folder-contains-news';

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::registerPageTSConfigFile(
    'moox_news',
    'Configuration/TSconfig/Page/news_folder.typoscript',
    'Page is a news folder'
);