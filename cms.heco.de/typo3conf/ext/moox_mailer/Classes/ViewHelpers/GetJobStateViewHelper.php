<?php
namespace Neulandlotsen\MooxMailer\ViewHelpers;

/**
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */
 
class GetJobStateViewHelper extends \TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper {

    public function initializeArguments()
    {
        parent::initializeArguments();
        $this->registerArgument('type', 'string', 'type', true);
        $this->registerArgument('object', 'mixed', 'object', true);
    }

    /**
	 * Get folders of given serialized folder array
	 *
	 * @param string $job job
	 * @param string $as as
	 * @return array
	 */
	public function render($as = "")
	{
        $type = $this->arguments['type'];
        $object = $this->arguments['object'];

	    $mailDir = $GLOBALS["_SERVER"]["DOCUMENT_ROOT"]."/maildir";
		
		$state = array();
		
		
		if(is_object($object) && $object->getJob()!=""){
			
			$jobPath = $mailDir."/jobs/".$object->getJob();
			
			if($type=="newsletter"){
				
				$state['sent'] = $object->getSentCount();
				
				if(file_exists($jobPath)){
			
					$mailerServiceConfig = 	array(
						"mailDir" 		=> $mailDir,
						"mode"			=> 'daemon',
						"format"		=> 'text/html',
						"uid" 			=> $object->getUid(),
						"timestamp" 	=> time(),
						"countMode" 	=> 'live',
						"scriptTimeout" => 90,
						"memoryLimit" 	=> 1024,
						"noLog" 		=> true,
						"senderAddress"	=> $object->getSenderName(),
						"senderName"	=> $object->getSenderEmail(),
					);
								
					// initiate mailer
					$mailer = $this->objectManager->get('Neulandlotsen\MooxMailer\Service\MailerService');
					$mailer->initialize($mailerServiceConfig,$object->getJob());
				
					$state['description'] = "In Bearbeitung";
					$state['processed'] = (int)$mailer->getMailsProcessed();
					$state['left'] = $state['sent']-$state['processed'];
					$state['percentage'] = floor(($state['processed']*100)/$state['sent']);
					$state['percentageOpposite'] = 100 - $state['percentage'];
					$state['completed'] = 0;
					
					$mailer = NULL;
					
				} else {
					$state['description'] = "Vollständig versendet";
					$state['processed'] = $state['sent'];					
					$state['left'] = 0;
					$state['percentage'] = 100;
					$state['percentageOpposite'] = 0;
					$state['completed'] = 1;
				}				
			}
		}
		
		if($as!=""){
			$this->templateVariableContainer->add($as, $state);
		} else {
			return $state;
		}
	}
}
