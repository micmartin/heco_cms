<?php
defined('TYPO3_MODE') or die();

// set default language file as ll-reference
//$extConf = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Configuration\ExtensionConfiguration::class)->get('moox_template_heco');

// set default language file as ll-reference
$ll = 'LLL:EXT:moox_template_heco/Resources/Private/Language/locallang_db.xlf:';

// add new news fields to field arrays
//\Neulandlotsen\MooxCore\Utility\TcaHelper::addFieldsToArray("tx_mooxnews_domain_model_news","additionalFrontendDemandFields",[]);
//\Neulandlotsen\MooxCore\Utility\TcaHelper::addFieldsToArray("tx_mooxnews_domain_model_news","additionalFrontendDetailFields",[]);
//\Neulandlotsen\MooxCore\Utility\TcaHelper::addFieldsToArray("tx_mooxnews_domain_model_news","additionalFrontendSortingFields",[]);
//\Neulandlotsen\MooxCore\Utility\TcaHelper::addFieldsToArray("tx_mooxnews_domain_model_news","allowedBackendListViewFields",[]);
\Neulandlotsen\MooxCore\Utility\TcaHelper::addBackendPreviewFieldsSet(
	"tx_mooxnews_domain_model_news",
	"moox_news",[
		'moox_template_heco_link'
	],
	'moox_news',
	[]
);

\Neulandlotsen\MooxCore\Utility\TcaHelper::addFieldsToArray(
	"tx_mooxnews_domain_model_news",
	"allowedGrabberFields", [
		'moox_template_heco_link'
	]
);
//\Neulandlotsen\MooxCore\Utility\TcaHelper::addSearchFieldsSet("tx_mooxnews_domain_model_news","moox_template_heco",[],'moox_news');
//\Neulandlotsen\MooxCore\Utility\TcaHelper::addSearchFieldSet("tx_mooxnews_domain_model_news","moox_template_heco",[]);
//\Neulandlotsen\MooxCore\Utility\TcaHelper::addDateTimeDefaultField("tx_mooxnews_domain_model_news","moox_template_heco","");
//\Neulandlotsen\MooxCore\Utility\TcaHelper::addFieldsToArray("tx_mooxnews_domain_model_news","additionalFrontendDemandFields",[]);
//\Neulandlotsen\MooxCore\Utility\TcaHelper::addFieldsToList("tx_mooxnews_domain_model_news","requestUpdate",[]);

$tx_mooxtemplateheco_domain_model_news = [
	'moox_template_heco_link' => [
		'exclude' => true,
		'label' => $ll.'tx_mooxtemplateheco_domain_model_news.link',
		'config' => [
			'type' => 'input',
			'renderType' => 'inputLink',
		],
		// special moox configuration
		'moox' => [
			'extkey' => 'moox_template_heco',
			'backend' => [
				'type' => 'link',
			],
			'sortable' => 1
		],
	]
];

// extend moox_news tca with new fields from this extension
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns(
	'tx_mooxnews_domain_model_news',
	$tx_mooxtemplateheco_domain_model_news,
	1
);

// place new fields in backend form - optionally generate new tab for extended fields
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes(
	'tx_mooxnews_domain_model_news',
	'moox_template_heco_link',
	'',
	'after:bodytext'
);