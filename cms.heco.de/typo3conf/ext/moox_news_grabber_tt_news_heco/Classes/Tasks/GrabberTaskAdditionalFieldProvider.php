<?php
namespace Neulandlotsen\MooxNewsGrabberTtNewsHeco\Tasks;

/**
 * This file is part of the "moox_news_grabber_tt_news_heco" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

use Neulandlotsen\MooxNews\Hooks\SchedulerHelper;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Utility\LocalizationUtility;
use TYPO3\CMS\Core\Messaging\FlashMessage;
use TYPO3\CMS\Core\Messaging\FlashMessageService;
 
class GrabberTaskAdditionalFieldProvider implements \TYPO3\CMS\Scheduler\AdditionalFieldProviderInterface {

	/**
	 * This method is used to define new fields for adding or editing a task
	 * In this case, it adds an pid field
	 *
	 * @param array $taskInfo Reference to the array containing the info used in the add/edit form
	 * @param object $task When editing, reference to the current task object. Null when adding.
	 * @param \TYPO3\CMS\Scheduler\Controller\SchedulerModuleController $parentObject Reference to the calling object (Scheduler's BE module)
	 * @return array	Array containing all the information pertaining to the additional fields
	 */
	public function getAdditionalFields(array &$taskInfo, $task, \TYPO3\CMS\Scheduler\Controller\SchedulerModuleController $parentObject)
	{		
		// Initialize extra field value
		if (empty($taskInfo['newsPids']))
		{
			if ($parentObject->CMD == 'edit')
			{
				// In case of edit, set to internal value if no data was submitted already
				$taskInfo['newsPids'] = $task->newsPids;
			} 
			else 
			{
				// Otherwise set an empty value, as it will not be used anyway
				$taskInfo['newsPids'] = '';
			}
		}
		
		$additionalFields = [];
				
		// Write the code for the field
		$fieldID = 'task_newsPids';		
		$fieldCode = SchedulerHelper::getMooxNewsFoldersSelector('tx_scheduler[newsPids]',$taskInfo['newsPids']);		
		$additionalFields[$fieldID] = [
			'code' => $fieldCode,
			'label' => LocalizationUtility::translate('LLL:EXT:moox_news_grabber_tt_news_heco/Resources/Private/Language/locallang_scheduler.xlf:tx_mooxnewsgrabberttnewsheco_tasks_grabbertask.news_pids_label', 'moox_news_grabber_tt_news'),
			'cshKey' => '_MOD_tools_txschedulerM1',
			'cshLabel' => $fieldID
		];
		return $additionalFields;
	}

	/**
	 * This method checks any additional data that is relevant to the specific task
	 * If the task class is not relevant, the method is expected to return TRUE
	 *
	 * @param array $submittedData Reference to the array containing the data submitted by the user
	 * @param \TYPO3\CMS\Scheduler\Controller\SchedulerModuleController $parentObject Reference to the calling object (Scheduler's BE module)
	 * @return boolean TRUE if validation was ok (or selected class is not relevant), FALSE otherwise
	 */
	public function validateAdditionalFields(array &$submittedData, \TYPO3\CMS\Scheduler\Controller\SchedulerModuleController $parentObject)
	{				
		$submittedData['newsPids'] 	= (is_array($submittedData['newsPids']))?implode(",",$submittedData['newsPids']):$submittedData['newsPids'];
		
		if ($submittedData['newsPids']=="")
		{
			$parentObject->addMessage($GLOBALS['LANG']->sL('LLL:EXT:moox_news_grabber_tt_news_heco/Resources/Private/Language/locallang_scheduler.xlf:tx_mooxnewsgrabberttnewsheco_tasks_grabbertask.news_pids_error'), \TYPO3\CMS\Core\Messaging\FlashMessage::ERROR);
			$result = FALSE;
		} 
		else 
		{
			$result = true;
		}				
		
		return $result;
	}

	/**
	 * This method is used to save any additional input into the current task object
	 * if the task class matches
	 *
	 * @param array $submittedData Array containing the data submitted by the user
	 * @param \TYPO3\CMS\Scheduler\Task\AbstractTask $task Reference to the current task object
	 * @return void
	 */
	public function saveAdditionalFields(array $submittedData, \TYPO3\CMS\Scheduler\Task\AbstractTask $task)
	{				
		$task->newsPids = $submittedData['newsPids'];		
	}		
}

?>