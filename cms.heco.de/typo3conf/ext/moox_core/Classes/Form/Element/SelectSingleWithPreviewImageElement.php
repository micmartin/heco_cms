<?php
namespace Neulandlotsen\MooxCore\Form\Element;

/**
 * This file is part of the "moox_core" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

use TYPO3\CMS\Backend\Form\Element\SelectSingleElement;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Utility\ArrayUtility;
use TYPO3\CMS\Backend\Form\Utility\FormEngineUtility;
use TYPO3\CMS\Core\Imaging\Icon;
use TYPO3\CMS\Core\Imaging\IconFactory;

/**
 * Render a widget with select box and preview images.
 *
 * This is rendered for config type=select, renderType=selectSingleWithPreviewImage set
 */
class SelectSingleWithPreviewImageElement extends SelectSingleElement
{
	/**
	 * Render single element
	 *
	 * @return array As defined in initializeResultArray() of AbstractNode
	 */
	public function render()
	{
		$resultArray = parent::render();

		return $resultArray;
	}

	/**
	 * Merge field wizard configuration with default wizards and render them.
	 *
	 * @return array Result array
	 */
	protected function renderFieldWizard(): array
	{
		$options = $this->data;
		$fieldWizard = $this->defaultFieldWizard;
		$fieldWizardFromTca = $options['parameterArray']['fieldConf']['config']['fieldWizard'] ?? [];
		ArrayUtility::mergeRecursiveWithOverrule($fieldWizard, $fieldWizardFromTca);
		$options['renderType'] = 'fieldWizard';
		$options['renderData']['fieldWizard'] = $fieldWizard;
		return $this->renderPreviewImages();//$this->nodeFactory->create($options)->render();
	}

	/**
	 * Render thumbnails of selected files
	 *
	 * @return array
	 */
	public function renderPreviewImages(): array
	{

		$size = 192;

		$iconFactory = GeneralUtility::makeInstance(IconFactory::class);

		$result = $this->initializeResultArray();

		$parameterArray = $this->data['parameterArray'];
		$selectItems = $parameterArray['fieldConf']['config']['items'];

		$selectItemCounter = 0;
		foreach ($selectItems as $item) {
			if ($item[1] === '--div--') {
				continue;
			}

			$icon = !empty($item[2]) ? $iconFactory->getIcon(
				$item[2],
				Icon::SIZE_LARGE
			)->render() : '';

			if ($icon) {
				$icon = str_replace('width="48" height="48"','width="'.$size.'" height="'.$size.'"',$icon);
				$icon = str_replace('class="t3js-icon','style="border:1px solid #ccc;height:'.$size.'px;width:'.$size.'px;line-height:'.$size.'px;" class="t3js-icon',$icon);
				$icon = str_replace('<img','<img style="max-width:'.$size.'px;max-height:'.$size.'px"',$icon);
				$fieldValue = $this->data['databaseRow'][$this->data['fieldName']];
				$selectIcons[] = [
					'title' => $item[0],
					'active' => ($fieldValue[0] === (string)$item[1]) ? true : false,
					'icon' => $icon,
					'index' => $selectItemCounter,
				];
			}
			$selectItemCounter++;
		}

		$html = [];
		$html[] = "<style>.t3js-forms-select-single-icons.icon-list .row .item:hover{background-color:#ededed}</style>";
		if (!empty($selectIcons)) {
			$html[] = '<div class="t3js-forms-select-single-icons icon-list">';
			$html[] =    '<div class="row">';
			foreach ($selectIcons as $i => $selectIcon) {
				$active = $selectIcon['active'] ?  ' active' : '';
				$html[] =   '<div class="item' . $active . '" style="max-width:'.($size+12).'px">';
				if (is_array($selectIcon)) {
					$html[] = '<a href="#" title="' . htmlspecialchars($selectIcon['title'], ENT_COMPAT, 'UTF-8', false) . '" data-select-index="' . htmlspecialchars((string)$selectIcon['index']) . '">';
					$html[] =   $selectIcon['icon'];
					$html[] = '</a>';
				}
				$html[] =   '</div>';
			}
			$html[] =    '</div>';
			$html[] = '</div>';
		}

		$result['html'] = implode(LF, $html);
		return $result;
	}
}
