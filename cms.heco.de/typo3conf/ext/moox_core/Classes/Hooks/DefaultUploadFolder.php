<?php
namespace Neulandlotsen\MooxCore\Hooks;
/*
 * This source file is proprietary property of Beech Applications B.V.
 * Date: 06-04-2016
 * All code (c) Beech Applications B.V. all rights reserved
 */
use TYPO3\CMS\Backend\Utility\BackendUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Authentication\BackendUserAuthentication;
use TYPO3\CMS\Core\Resource\Exception\FolderDoesNotExistException;
use TYPO3\CMS\Core\Resource\Folder;
use TYPO3\CMS\Core\Resource\ResourceFactory;
use TYPO3\CMS\Core\Messaging\FlashMessage; 
use TYPO3\CMS\Core\Messaging\FlashMessageService; 
use TYPO3\CMS\Extbase\Utility\DebuggerUtility;

/**
 * Class DefaultUploadFolder
 */
class DefaultUploadFolder
{
	/**
     * Get default upload folder
     *
     * @param array $params
     * @param BackendUserAuthentication $backendUserAuthentication
     * @return Folder
     */
    public function getDefaultUploadFolder($params, BackendUserAuthentication $backendUserAuthentication)
    {
		/** @var Folder $uploadFolder */
        $uploadFolder = $params['uploadFolder'];

        $table = $params['table'];
        $field = $params['field'];
		$pid = $params['pid'];
        
		$pageTs = BackendUtility::getPagesTSconfig($pid);
        $subFolder = $pageTs['default_upload_folders.'][$pid.'.'][$table.'.'][$field];

		if ($subFolder == '')
		{
            $subFolder = $pageTs['default_upload_folders.'][$pid.'.'][$table];
		}
		if ($subFolder == '')
		{
            $subFolder = $pageTs['default_upload_folders.'][$pid];
		}		
		if ($subFolder == '')
		{
            $subFolder = $pageTs['default_upload_folders.'][$table.'.'][$field];
		}
        if ($subFolder == '')
        {
            $subFolder = $pageTs['default_upload_folders.'][$table];
        }
		if ($subFolder == '')
		{
            $subFolder = $pageTs['default_upload_folder'];
		}

		// Folder by combined identifier
        if (preg_match('/[0-9]+:/', $subFolder))
		{            
			try
			{
                $uploadFolder = ResourceFactory::getInstance()->getFolderObjectFromCombinedIdentifier(
                    $subFolder
                );

				if(is_object($uploadFolder))
				{
					$path = $uploadFolder->getIdentifier().$subFolder;
				}
            } 
			catch (FolderDoesNotExistException $e) 
			{
                // todo: try to create the folder
				/** @var FlashMessage $message */ 
				/*
				$message = GeneralUtility::makeInstance(FlashMessage::class, 
					'Der derzeit konfigurierten Upload-Ordner "'.$path.'" für das Feld "'.$field.'" in Tabelle "'.$table.'" (Pid: '.$pid.'")', 
					'', // header is optional 
					FlashMessage::ERROR, 
					true // whether message should be stored in session 
				);
				*/
				/** @var $flashMessageService FlashMessageService */ 
				//$flashMessageService = GeneralUtility::makeInstance(FlashMessageService::class); 
				//$flashMessageService->getMessageQueueByIdentifier()->enqueue($message); 
			}
		}

        if (
            $uploadFolder instanceof Folder
            &&
            $subFolder != ''
            &&
            $uploadFolder->hasFolder($subFolder)
        ) 
		{
            $uploadFolder = $uploadFolder->getSubfolder($subFolder);
        }
		else
		{
			//\TYPO3\CMS\Core\Utility\DebugUtility::debug($subFolder);
		}
		
		//\TYPO3\CMS\Core\Utility\DebugUtility::debug($subFolder);
		
        return $uploadFolder;
    }
}
