<?php
namespace Neulandlotsen\MooxNews\ViewHelpers;

/**
 * This file is part of the "moox_news" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper;
use TYPO3\CMS\Core\Database\Connection;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Utility\GeneralUtility;

class MonthsViewHelper extends AbstractViewHelper
{
	public function initializeArguments()
	{
		parent::initializeArguments();
		$this->registerArgument('year', 'mixed', 'year', false);
		$this->registerArgument('hidePast', 'bool', 'year', false, FALSE);
		$this->registerArgument('as', 'string', 'return result as variable with this name', false);
	}

	/**
	 * @return string
	 */
	public function render()
	{
		$year = $this->arguments['year'];
		$hidePast = $this->arguments['hidePast'];
		$as = $this->arguments['as'];

		$months = [];

		if($hidePast && $year==date("Y"))
		{
			$start = date("n");
		}
		else
		{
			$start = 1;
		}
		for($i=$start;$i<=12;$i++)
		{
			$months[$i] = strftime ("%B",mktime(0,0,0,$i+1,0,0));
		}

		if($as)
		{
			$this->templateVariableContainer->add($this->arguments['as'], $months);
		}
		else
		{
			return $months;
		}
	}
}
