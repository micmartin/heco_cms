<?php
namespace Neulandlotsen\MooxMailer\Domain\Model;

/**
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */

use TYPO3\CMS\Extbase\DomainObject\AbstractEntity;
 
class Schedule extends AbstractEntity
{
	
	/**
	 * Titel
	 *
	 * @var string
	 * @TYPO3\CMS\Extbase\Annotation\Validate("NotEmpty")
	 */
	protected $title;
		
	/**
	 * Zeitplan
	 *
	 * @var string
	 */
	protected $schedule;	
	
	/**
	 * Returns the title
	 *
	 * @return string $title
	 */
	public function getTitle()
	{
		return $this->title;
	}

	/**
	 * Sets the title
	 *
	 * @param string $title
	 * @return void
	 */
	public function setTitle($title)
	{
		$this->title = $title;
	}		
	
	/**
	 * Returns the schedule
	 *
	 * @return string $schedule
	 */
	public function getSchedule()
	{
		return $this->schedule;
	}

	/**
	 * Sets the schedule
	 *
	 * @param string $schedule
	 * @return void
	 */
	public function setSchedule($schedule)
	{
		$this->schedule = $schedule;
	}
}
?>