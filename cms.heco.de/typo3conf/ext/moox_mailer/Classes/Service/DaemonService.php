<?php
namespace Neulandlotsen\MooxMailer\Service;

/**
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */

class DaemonService implements \TYPO3\CMS\Core\SingletonInterface {	
	
	/**
	 * name of global daemon logfile
	 *
	 * @var string
	 */
	public static $globalDaemonLogfile = "daemon.log";
	
	/**
	 * time after existing locks will be deleted (caused by server fails)
	 *
	 * @var int
	 */
	static public $maxLockLifetime = "30"; // minutes
	
	/**
	 * Anzahl Mails pro Durchlauf
	 *
	 * @var int
	 */
	protected $mailsPerProcess;

	/**
	 * Hauptverzeichnis
	 *
	 * @var string
	 */
	protected $dir;
	
	/**
	 * Mail-Verzeichnis
	 *
	 * @var string
	 */
	protected $mailDir;
	
	/**
	 * Job-Verzeichnis
	 *
	 * @var string
	 */
	protected $jobDir;
	
	/**
	 * Log-Verzeichnis
	 *
	 * @var string
	 */
	protected $logDir;
	
	/**
	 * Logging deaktivieren
	 *
	 * @var bool
	 */
	protected $noLog;
	
	/**
	 * initialize daemon
	 *
	 * @param array|object $object object
	 * @return void
	 */
	public function initialize($object)
	{				
		if(is_object($object)){
			$this->setDir($object->getMailerDir());
		} elseif(is_array($object)) {
			$this->setDir($object['mailDir']);
		}
		
		if($object instanceof \Neulandlotsen\MooxMailer\Tasks\DaemonSendTask){
		
			if($object->getMailsPerProcess()<1){
				$this->setMailsPerProcess(500);
			} else {
				$this->setMailsPerProcess($object->getMailsPerProcess());
			}
			
			if($object->getNoLog()){
				$this->setNoLog(1);
			}
		}
		
	}	
	
	/**
	 * checks if mailer is currently locked
	 *	
	 * @return bool $ready
	 */
	public function mailerReady()
	{
		
		if(file_exists($this->getJobDir().".lock")){
			
			// log daemon info
			$this->log("DAEMON: lock-file exisiting...");
			
			$check_ts 	= mktime ((date("H")),date("i")-DaemonService::$maxLockLifetime,date("s"),date("m"),date("d"),date("Y"));
			$this_ts	= filemtime($this->getJobDir().".lock");
			
			if($this_ts<$check_ts){
				
				// log daemon info
				$this->log("DAEMON: lock-file outdated...");
				
				unlink($this->getJobDir().".lock");
				return true;
				
			} else {
				
				// log daemon info
				$this->log("DAEMON: lock-file up-to-date (generated: ".date("Y-m-d H:i:s",$this_ts).")...");
				
				return false;
			}
			
		} else {
			
			// log daemon info
			$this->log("DAEMON: lock-file is absent...");
		
			return true;
		}
	}

	/**
	 * lock mailer
	 *
	 * @return void
	 */
	public function lockMailer($comment = "locked")
	{
		if(!file_exists($this->getJobDir().".lock")){
			// log daemon info
			$this->log("DAEMON: lock-file generated...");
			file_put_contents($this->getJobDir().".lock",$comment);
		} else {
			// log daemon info
			$this->log("DAEMON: lock-file already present...");
		}
	}
		
	/**
	 * remove mailer lock
	 *
	 * @return void
	 */
	public function removeLock()
	{		
		if(file_exists($this->getJobDir().".lock")){
			unlink($this->getJobDir().".lock");
		}
	}
		
	/**
	 * get lock info
	 *
	 * @return string $lockInfo
	 */
	public function lockInfo()
	{
		if(file_exists($this->getJobDir().".lock")){
			return file_get_contents($this->getJobDir().".lock");
		}
	}
		
	/**
	 * get next job to handle
	 *
	 * @return string $job
	 */
	public function getNextJob()
	{		
		$jobfile = "";
		
		$handle = opendir($this->getJobDir());
		
		while ($file = readdir ($handle)) {			
			
			if(!in_array($file,array(".",".."))){
				if(substr(strtolower($file),0,2)!="__"){
					$thisFilemtime = filemtime ($this->getJobDir().$file);
					if($jobfile=="" || $thisFilemtime<$lastFilemtime){
						$lastFilemtime 	= $thisFilemtime;
						$jobfile 		= $file;
					}
				} 
			}			
		}

		if($jobfile!=""){
			return $jobfile;
		} else {
			return false;
		}
	}

	/**
	 * mark mailfile as processed
	 *
	 * @param string $mailfile mailfile
	 * @return void
	 */
	public function markMailfileAsProcessed($mailfile)
	{
		rename($this->getMailDir().$mailfile,$this->getMailDir()."__".$mailfile);
	}	
	
	/**
	 * log daemon info
	 *
	 * @param string $logtext logtext	
	 * @return void
	 */
	public function log($logtext)
	{		
		$logdate = "[".date("Y-m-d H:i:s")."] ";
		
		if(!$this->noLog){
			file_put_contents($this->getLogDir().DaemonService::$globalDaemonLogfile,$logdate.$logtext."\n",FILE_APPEND | LOCK_EX);
		}
    } 
	
	/**
	 * Returns mailer per process
	 *
	 * @return string $mailsPerProcess
	 */
	public function getMailsPerProcess()
	{
		return $this->mailsPerProcess;
	}

	/**
	 * Set mails per process
	 *
	 * @param string $mailsPerProcess mails per process
	 * @return void
	 */
	public function setMailsPerProcess($mailsPerProcess)
	{
		$this->mailsPerProcess = $mailsPerProcess;
	}
	
	/**
	 * Returns dir
	 *
	 * @return string $dir
	 */
	public function getDir()
	{
		return $this->dir;
	}

	/**
	 * Set dir
	 *
	 * @param string $dir dir
	 * @return void
	 */
	public function setDir($dir)
	{
		$this->dir 		= $dir;
		$this->mailDir 	= $this->dir."/mails/";
		$this->jobDir 	= $this->dir."/jobs/";
		$this->logDir 	= $this->dir."/logs/";
	}
	
	/**
	 * Returns mail dir
	 *
	 * @return string $mailDir
	 */
	public function getMailDir()
	{
		return $this->dir."/mails/";
	}

	/**
	 * Returns job dir
	 *
	 * @return string $jobDir
	 */
	public function getJobDir()
	{
		return $this->dir."/jobs/";
	}
	
	/**
	 * Returns log dir
	 *
	 * @return string $logDir
	 */
	public function getLogDir()
	{
		return $this->dir."/logs/";
	}
	
	/**
	 * Returns no log
	 *
	 * @return boolean $noLog
	 */
	public function getNoLog()
	{
		return $this->noLog;
	}

	/**
	 * Set no log
	 *
	 * @param bool $noLog no log
	 * @return void
	 */
	public function setNoLog($noLog)
	{
		$this->noLog = $noLog;
	}
}
?>