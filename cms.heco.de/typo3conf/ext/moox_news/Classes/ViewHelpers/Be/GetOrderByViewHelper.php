<?php
namespace Neulandlotsen\MooxNews\ViewHelpers\Be;

/**
 * This file is part of the "moox_news" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Utility\StringUtility;
use TYPO3\CMS\Extbase\Utility\LocalizationUtility;

class GetOrderByViewHelper extends AbstractViewHelper
{
	public function initializeArguments()
	{
		parent::initializeArguments();
		$this->registerArgument('as', 'string', 'return result as variable with this name', false);
	}

	/**
	 * @return string
	 */
	public function render()
	{
		$items = [];

		$as = $this->arguments['as'];

		$newItems = $GLOBALS['TYPO3_CONF_VARS']['EXT']['moox_news']['orderByNews'];

		// if a override configuration is found
		if (!empty($newItems)) {
			// remove default configuration
			$items = [];
			// empty default line
			array_push($items, ['', '']);

			$newItemArray = GeneralUtility::trimExplode(',', $newItems, true);
			$languageKey = 'LLL:EXT:moox_news/Resources/Private/Language/locallang_be.xlf:flexforms_general.orderBy.';
			foreach ($newItemArray as $item) {
				// label: if empty, key (=field) is used
				$label = LocalizationUtility::translate($languageKey . $item);
				if (empty($label)) {
					$label = $item;
				}
				array_push($items, [htmlspecialchars($label), $item]);
			}
		}

		if(is_array($GLOBALS['TCA']['tx_mooxnews_domain_model_news']['moox']['additionalFrontendSortingFields']))
		{
			foreach($GLOBALS['TCA']['tx_mooxnews_domain_model_news']['moox']['additionalFrontendSortingFields'] AS $fieldname)
			{
				if(isset($GLOBALS['TCA']['tx_mooxnews_domain_model_news']['columns'][$fieldname]))
				{
					$field = $GLOBALS['TCA']['tx_mooxnews_domain_model_news']['columns'][$fieldname];
					$ll = 'LLL:EXT:'.$field['moox']['extkey'].'/Resources/Private/Language/locallang_db.xml:';

					// TODO: was sollte diese Abfrage
					// if($field['moox']['additionalSorting'])
					if($field['moox']['sortable'])
					{
						$prefix = $ll.'tx_'.str_replace("_","",$field['moox']['extkey']).'_domain_model_news';
						$prefix = LocalizationUtility::translate($prefix).": ";
						$label = LocalizationUtility::translate($field['label']);
						array_push($items, array($prefix.$label, GeneralUtility::underscoredToLowerCamelCase($fieldname)));
					}
				}
			}
		}

		if($as)
		{
			$this->templateVariableContainer->add($this->arguments['as'], $items);
		}
		else
		{
			return $items;
		}
	}
}
