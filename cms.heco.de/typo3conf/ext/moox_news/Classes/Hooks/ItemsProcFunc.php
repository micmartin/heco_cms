<?php

namespace Neulandlotsen\MooxNews\Hooks;

/**
 * This file is part of the "moox_news" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */
use Neulandlotsen\MooxNews\Utility\TemplateLayout;
use TYPO3\CMS\Backend\Utility\BackendUtility;
use TYPO3\CMS\Backend\Configuration\TranslationConfigurationProvider;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Utility\StringUtility;
use TYPO3\CMS\Frontend\Page\PageRepository;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Database\Query\Restriction\DeletedRestriction;

/**
 * Userfunc to render alternative label for media elements
 */
class ItemsProcFunc
{
	/**
	 * @var \TYPO3\CMS\Extbase\Object\ObjectManager
	 */
	protected $objectManager;
  
	/**
	 * @var \Neulandlotsen\MooxNews\Domain\Repository\TagRepository
	 */
	protected $tagRepository;
	
	/**
	 * @var \Neulandlotsen\MooxNews\Domain\Repository\TargetRepository
	 */
	protected $targetRepository;
	
	/**
	 * @var \Neulandlotsen\MooxNews\Service\HelperService
	 */
	protected $helperService;
	
	/**
	 * @var array
	 */
	protected $filter;
	
    /** @var TemplateLayout $templateLayoutsUtility */
    protected $templateLayoutsUtility;

    public function __construct()
    {
        $this->templateLayoutsUtility = GeneralUtility::makeInstance(TemplateLayout::class);
    }

    /**
     * Itemsproc function to extend the selection of templateLayouts in the plugin
     *
     * @param array &$config configuration array
     */
    public function user_templateLayout(array &$config)
    {
        $pageId = 0;
        if (ExtensionManagementUtility::isLoaded('compatibility6')) {
            if (StringUtility::beginsWith($config['row']['uid'], 'NEW')) {
                $getVars = GeneralUtility::_GET('edit');
                if (is_array($getVars) && isset($getVars['tt_content']) && is_array($getVars['tt_content'])) {
                    $keys = array_keys($getVars['tt_content']);
                    $firstKey = (int)$keys[0];
                    if ($firstKey > 0) {
                        $pageId = $firstKey;
                    } else {
                        $row = $this->getContentElementRow(abs($firstKey));
                        $pageId = $row['pid'];
                    }
                }
            } else {
                $row = $this->getContentElementRow($config['row']['uid']);
                $pageId = $row['pid'];
            }
        } else {
            $pageId = $this->getPageId($config['flexParentDatabaseRow']['pid']);
        }

        if ($pageId > 0) {
            $templateLayouts = $this->templateLayoutsUtility->getAvailableTemplateLayouts($pageId);
            foreach ($templateLayouts as $layout) {
                $additionalLayout = [
                    htmlspecialchars($this->getLanguageService()->sL($layout[0])),
                    $layout[1]
                ];
                array_push($config['items'], $additionalLayout);
            }
        }
    }

    /**
     * Modifies the select box of orderBy-options as a category menu
     * needs different ones then a news action
     *
     * @param array &$config configuration array
     */
    public function user_orderBy(array &$config)
    {
        $row = $this->getContentElementRow($config['row']['uid']);

        // check if the record has been saved once
        if (is_array($row) && !empty($row['pi_flexform'])) {
            $flexformConfig = GeneralUtility::xml2array($row['pi_flexform']);

            // check if there is a flexform configuration
            if (isset($flexformConfig['data']['sDEF']['lDEF']['switchableControllerActions']['vDEF'])) {
                $selectedActionList = $flexformConfig['data']['sDEF']['lDEF']['switchableControllerActions']['vDEF'];
                // check for selected action
                if (GeneralUtility::isFirstPartOfStr($selectedActionList, 'Category')) {
                    $newItems = $GLOBALS['TYPO3_CONF_VARS']['EXT']['moox_news']['orderByCategory'];
                } elseif (GeneralUtility::isFirstPartOfStr($selectedActionList, 'Tag')) {
                    $this->removeNonValidOrderFields($config, 'tx_mooxnews_domain_model_tag');
                    $newItems = $GLOBALS['TYPO3_CONF_VARS']['EXT']['moox_news']['orderByTag'];
                } else {
                    $newItems = $GLOBALS['TYPO3_CONF_VARS']['EXT']['moox_news']['orderByNews'];
                }
            }
        }

        // if a override configuration is found
        if (!empty($newItems)) {
            // remove default configuration
            $config['items'] = [];
            // empty default line
            array_push($config['items'], ['', '']);

            $newItemArray = GeneralUtility::trimExplode(',', $newItems, true);
            $languageKey = 'LLL:EXT:moox_news/Resources/Private/Language/locallang_be.xlf:flexforms_general.orderBy.';
            foreach ($newItemArray as $item) {
                // label: if empty, key (=field) is used
                $label = $this->getLanguageService()->sL($languageKey . $item);
                if (empty($label)) {
                    $label = $item;
                }
                array_push($config['items'], [htmlspecialchars($label), $item]);
            }
        }

		if(is_array($GLOBALS['TCA']['tx_mooxnews_domain_model_news']['moox']['additionalFrontendSortingFields']))
		{
			foreach($GLOBALS['TCA']['tx_mooxnews_domain_model_news']['moox']['additionalFrontendSortingFields'] AS $fieldname)
			{
				if(isset($GLOBALS['TCA']['tx_mooxnews_domain_model_news']['columns'][$fieldname]))
				{
					$field = $GLOBALS['TCA']['tx_mooxnews_domain_model_news']['columns'][$fieldname];
					$ll = 'LLL:EXT:'.$field['moox']['extkey'].'/Resources/Private/Language/locallang_db.xml:';
					
					// TODO: was sollte diese Abfrage
					// if($field['moox']['additionalSorting'])
					if($field['moox']['sortable'])
					{
						$prefix = $ll.'tx_'.str_replace("_","",$field['moox']['extkey']).'_domain_model_news';
						$prefix = $GLOBALS['LANG']->sL($prefix).": ";
						$label = $GLOBALS['LANG']->sL($field['label']);
						array_push($config['items'], array($prefix.$label, GeneralUtility::underscoredToLowerCamelCase($fieldname)));
					}
				}
			}
		}
    }

	/**
	 * Modifies the select box of types-options
	 *
	 * @param array &$config configuration array
	 * @param mixed &$pObj configuration array
	 */
	public function user_types(array &$config, &$pObj)
	{					
		// init items array
		$config['items'] = [];
		
		$extConf = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Configuration\ExtensionConfiguration::class)->get('moox_news');
		
		foreach($GLOBALS['TCA']['tx_mooxnews_domain_model_news']['columns']['type']['config']['items'] AS $option){
			if(!($extConf['hideDefaultNewsType'] && $option[1]=="moox_news")){
				$config['items'][] =  [$GLOBALS['LANG']->sL($option[0]),$option[1]];
			}
		}		
	}

	/**
	 * Modifies the select box of years-options
	 *
	 * @param array &$config configuration array
	 * @param mixed &$pObj configuration array
	 */
	public function user_dateYear(array &$config, &$pObj)
	{
		// initialize
		$this->objectManager = GeneralUtility::makeInstance('TYPO3\CMS\Extbase\Object\ObjectManager');
		$this->helperService = $this->objectManager->get('Neulandlotsen\MooxNews\Service\HelperService');

		// init items array
		$config['items'] = [];

		$years = $this->helperService->getYears([],[],true);

		$yearMax = array_shift($years);
		$yearMin = end($years);
		if($yearMax<date('Y')+5)
		{
			$yearMax = date('Y')+5;
		}
		if($yearMin>date('Y')-5)
		{
			$yearMin = date('Y')-5;
		}

		$config['items'][] =  [$GLOBALS['LANG']->sL('LLL:EXT:moox_news/Resources/Private/Language/locallang_be.xlf:flexforms_general.dateYear.all'),''];
		for($i=$yearMax;$i>=$yearMin;$i--){
			$config['items'][] = [$i,$i];
		}
	}
	
	/**
	 * Modifies the select box of tag-options
	 *
	 * @param array &$config configuration array
	 * @param mixed &$pObj configuration array
	 * @return void
	 */
	public function user_tags(array &$config, &$pObj)
	{
		// initialize
		$this->objectManager = GeneralUtility::makeInstance('TYPO3\CMS\Extbase\Object\ObjectManager');
		$this->tagRepository = $this->objectManager->get('Neulandlotsen\MooxNews\Domain\Repository\TagRepository');
		$this->helperService = $this->objectManager->get('Neulandlotsen\MooxNews\Service\HelperService');
		
		// set filter
		$this->filter = $this->helperService->buildFilter('Neulandlotsen\MooxNews\Controller\Mod1Controller');
		
		// get tags for given folder
		$tags = $this->tagRepository->findByPid($this->filter['page']);
		
		// init items array
		$config['items'] = [];
		
		foreach($tags AS $tag){
			$config['items'][] = [$tag->getTitle(),$tag->getUid()];
		}		
	}
	
	/**
	 * Modifies the select box of target-options
	 *
	 * @param array &$config configuration array
	 * @param mixed &$pObj configuration array
	 * @return void
	 */
	public function user_targets(array &$config, &$pObj)
	{
		// initialize
		$this->objectManager = GeneralUtility::makeInstance('TYPO3\CMS\Extbase\Object\ObjectManager');
		$this->targetRepository = $this->objectManager->get('Neulandlotsen\MooxNews\Domain\Repository\TargetRepository');
		$this->helperService = $this->objectManager->get('Neulandlotsen\MooxNews\Service\HelperService');
		
		// set filter
		$this->filter = $this->helperService->buildFilter('Neulandlotsen\MooxNews\Controller\Mod1Controller');
		
		// get targets for given folder
		$targets = $this->targetRepository->findByPid($this->filter['page']);
		
		// init items array
		$config['items'] = [];
		
		foreach($targets AS $target){
			$config['items'][] = [$target->getTitle(),$target->getUid()];
		}		
	}
	
	/**
	 * Modifies the select box of timing date options
	 *
	 * @param array &$config configuration array
	 * @param mixed &$pObj configuration array
	 */
	public function user_timing_date(array &$config, &$pObj)
	{		
		// init items array
		$config['items'] = [];
		
		$extConf = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Configuration\ExtensionConfiguration::class)->get('moox_news');
		
		foreach($GLOBALS['TCA']['tx_mooxnews_domain_model_news']['columns']['type']['config']['items'] AS $option){
			if(!($extConf['hideDefaultNewsType'] && $option[1]=="moox_news")){
				$config['items'][] = array($GLOBALS['LANG']->sL($option[0]),$option[1]);
			}
		}		
	}
	
	/**
	 * Modifies the select box of privacy-options
	 *
	 * @param array &$config configuration array
	 * @param mixed &$pObj configuration array
	 */
	public function privacy(array &$config, &$pObj)
	{
		
		if(!ExtensionManagementUtility::isLoaded('moox_news_frontend')){
			
			// set local language path
			$llpath = 'LLL:EXT:moox_news/Resources/Private/Language/locallang_be.xlf:';
			
			// init items array
			$config['items'] = [];
			
			// add item
			$config['items'][] = array($GLOBALS['LANG']->sL($llpath."flexforms_general.privacy.disabled"),"");
		}
	}
	
	/**
	 * Modifies the select box of date fields
	 *
	 * @param array &$config configuration array
	 * @return void
	 */
	public function user_dateField(array &$config)
	{
		
		if(is_array($GLOBALS['TCA']['tx_mooxnews_domain_model_news']['moox']['additionalFrontendDateFields']))
		{
			foreach($GLOBALS['TCA']['tx_mooxnews_domain_model_news']['moox']['additionalFrontendDateFields'] AS $fieldname)
			{
				if(isset($GLOBALS['TCA']['tx_mooxnews_domain_model_news']['columns'][$fieldname]))
				{
					$field = $GLOBALS['TCA']['tx_mooxnews_domain_model_news']['columns'][$fieldname];
					$ll = 'LLL:EXT:'.$field['moox']['extkey'].'/Resources/Private/Language/locallang_db.xml:';
					$prefix = $ll.'tx_'.str_replace("_","",$field['moox']['extkey']).'_domain_model_news';
					$prefix = $GLOBALS['LANG']->sL($prefix).": ";
					$label = $GLOBALS['LANG']->sL($field['label']);
					array_push($config['items'], array($prefix.$label, GeneralUtility::underscoredToLowerCamelCase($fieldname)));
				}
			}
		}	
	}
	
	/**
	 * Modifies the select box of types-options with folder restrictions
	 *
	 * @param array &$config configuration array
	 * @return void
	 */
	public function user_types_restricted(array &$config) {
		
		$extConf = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Configuration\ExtensionConfiguration::class)->get('moox_news');
		$items = $config['items'];
		$config['items'] = [];
		
		if($config['row']['pid']>0)
		{
			$pageInfo = BackendUtility::readPageAccess($config['row']['pid'], '');
			if($pageInfo['news_types']!="")
			{
				$allowedTypes = explode(",",$pageInfo['news_types']);
				foreach($items AS $item)
				{
					if(in_array($item[1],$allowedTypes) && !($extConf['hideDefaultNewsType'] && $item[1]=="moox_news"))
					{
						$config['items'][] = $item;
					}
				}
			} 
			else 
			{
				foreach($items AS $item)
				{
					if(!($extConf['hideDefaultNewsType'] && $item[1]=="moox_news"))
					{
						$config['items'][] = $item;
					}
				}
			}
		} 
		else 
		{
			foreach($items AS $item)
			{
				if(!($extConf['hideDefaultNewsType'] && $item[1]=="moox_news"))
				{
					$config['items'][] = $item;
				}
			}
		}
	}
	
	/**
	 * Modifies the select box of languages
	 *
	 * @param array &$config configuration array
	 * @return void
	 */
	public function user_language(array &$config) 
	{
		$config['items'] = [];
		$languages = [];
		$sysLanguages = GeneralUtility::makeInstance(TranslationConfigurationProvider::class)->getSystemLanguages();
		
		$languages[] = ["Standard-Verhalten",""];
		$languages[] = ["Aktuelle Seitenspache","page"];
		$languages[] = ["Artikel für alle Sprachen anzeigen","-1"];
		
		foreach($sysLanguages AS $language)
		{
			if($language['uid']>-1)
			{
				if($language['uid']==0)
				{
					if($this->settings['languageDefaultLanguageLabel']!="")
					{
						$language['title'] = $this->settings['languageDefaultLanguageLabel'];
					}					
					$language['hidden'] = 0;
				}
				if($language['hidden']==0)
				{							
					$languages[] = [$language['title'],$language['uid']];					
				}
			}
		}
		
		$config['items'] = $languages;		
	}

    /**
     * Remove not valid fields from ordering
     *
     * @param array $config tca items
     * @param string $tableName table name
     */
    protected function removeNonValidOrderFields(array &$config, $tableName)
    {
        $allowedFields = array_keys($GLOBALS['TCA'][$tableName]['columns']);

        foreach ($config['items'] as $key => $item) {
            if ($item[1] != '' && !in_array($item[1], $allowedFields)) {
                unset($config['items'][$key]);
            }
        }
    }

    /**
     * Modifies the selectbox of available actions
     *
     * @param array &$config
     */
    public function user_switchableControllerActions(array &$config)
    {
        if (!empty($GLOBALS['TYPO3_CONF_VARS']['EXT']['moox_news']['switchableControllerActions']['list'])) {
            $configuration = (int)$GLOBALS['TYPO3_CONF_VARS']['EXT']['moox_news']['switchableControllerActions']['list'];
            switch ($configuration) {
                case 1:
                    $this->removeActionFromList($config, 'News->list');
                    break;
                case 2:
                    $this->removeActionFromList($config, 'News->list;News->detail');
                    break;
                default:
            }
        }

        // Add additional actions
        if (isset($GLOBALS['TYPO3_CONF_VARS']['EXT']['moox_news']['switchableControllerActions']['newItems'])
            && is_array($GLOBALS['TYPO3_CONF_VARS']['EXT']['moox_news']['switchableControllerActions']['newItems'])
        ) {
            foreach ($GLOBALS['TYPO3_CONF_VARS']['EXT']['moox_news']['switchableControllerActions']['newItems'] as $key => $label) {
                array_push($config['items'], [$this->getLanguageService()->sL($label), $key, '']);
            }
        }
    }

    /**
     * Remove given action from switchableControllerActions
     *
     * @param array $config available items
     * @param string $action action to be removed
     */
    private function removeActionFromList(array &$config, $action)
    {
        foreach ($config['items'] as $key => $item) {
            if ($item[1] === $action) {
                unset($config['items'][$key]);
                continue;
            }
        }
    }

    /**
     * Generate a select box of languages to choose an overlay
     *
     * @return string select box
     */
    public function user_categoryOverlay()
    {
        $html = '';

        $orderBy = $GLOBALS['TCA']['sys_language']['ctrl']['sortby'] ?
            $GLOBALS['TCA']['sys_language']['ctrl']['sortby'] :
            $GLOBALS['TYPO3_DB']->stripOrderBy($GLOBALS['TCA']['sys_language']['ctrl']['default_sortby']);


		$queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)
			->getQueryBuilderForTable('sys_language');

		$queryBuilder
			->getRestrictions()
			->removeAll()
			->add(GeneralUtility::makeInstance(DeletedRestriction::class));

		$languages = $queryBuilder
			->select('*')
			->from('sys_language')
			->orderBy($orderBy)
			->execute();

        // if any language is available
        if ($languages->rowCount() > 0) {
            $html = '<select name="data[newsoverlay]" id="field_newsoverlay" class="form-control">
						<option value="0">' . htmlspecialchars($this->getLanguageService()->sL('LLL:EXT:lang/locallang_general.xlf:LGL.default_value')) . '</option>';

            foreach ($languages as $language) {
                $selected = ((int)$GLOBALS['BE_USER']->uc['newsoverlay'] === (int)$language['uid']) ? ' selected="selected" ' : '';
                $html .= '<option ' . $selected . 'value="' . $language['uid'] . '">' . htmlspecialchars($language['title']) . '</option>';
            }

            $html .= '</select>';
        } else {
            $html .= htmlspecialchars($this->getLanguageService()->sL(
                'LLL:EXT:moox_news/Resources/Private/Language/locallang_be.xlf:usersettings.no-languages-available')
            );
        }

        return $html;
    }

    /**
     * Get tt_content record
     *
     * @param int $uid
     * @return array
     */
    protected function getContentElementRow($uid)
    {
        return BackendUtility::getRecord('tt_content', $uid);
    }

    /**
     * Get page id, if negative, then it is a "after record"
     *
     * @param int $pid
     * @return int
     */
    protected function getPageId($pid)
    {
        $pid = (int)$pid;

        if ($pid > 0) {
            return $pid;
        } else {
            $row = BackendUtility::getRecord('tt_content', abs($pid), 'uid,pid');
            return $row['pid'];
        }
    }

    /**
     * Returns LanguageService
     *
     * @return \TYPO3\CMS\Lang\LanguageService
     */
    protected function getLanguageService()
    {
        return $GLOBALS['LANG'];
    }
}
