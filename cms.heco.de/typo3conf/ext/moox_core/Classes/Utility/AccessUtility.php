<?php
namespace Neulandlotsen\MooxCore\Utility;

/**
 * This file is part of the "moox_core" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

use TYPO3\CMS\Core\SingletonInterface;
use TYPO3\CMS\Core\Utility\GeneralUtility;

class AccessUtility implements SingletonInterface
{
	/**
	 * @param array $userdata
	 * @return array
	 */
	static public function initFrontendUser($userdata = NULL)
	{
		if(is_array($userdata) && count($userdata))
		{
			$objectManager = GeneralUtility::makeInstance('TYPO3\CMS\Extbase\Object\ObjectManager');
			$feUser = $objectManager->get('TYPO3\\CMS\\Frontend\\Authentication\\FrontendUserAuthentication');
			$feUser->lockIP = $GLOBALS['TYPO3_CONF_VARS']['FE']['lockIP'];
			$feUser->checkPid = 0;
			$feUser->lifetime = intval($GLOBALS['TYPO3_CONF_VARS']['FE']['lifetime']);

			$feUser->dontSetCookie = 0;
			if ($GLOBALS['TYPO3_CONF_VARS']['FE']['dontSetCookie']) {
				// TODO: check this
				//$feUser->dontSetCookie = 1;
			}

			$feUser->start();
			$feUser->unpack_uc('');
			$feUser->fetchUserSession();

			$userdata[$feUser->lastLogin_column] = $GLOBALS['EXEC_TIME'];
			$userdata['is_online'] = $GLOBALS['EXEC_TIME'];
			$feUser->user = $userdata;

			$GLOBALS['TSFE']->fe_user = &$feUser;
			$GLOBALS['TSFE']->fe_user->createUserSession($userdata);
			$GLOBALS['TSFE']->initUserGroups();

			// TODO Überprüfen ob benötigt, da deprecated 2019-06-07
			// $GLOBALS['TSFE']->setSysPageWhereClause();
		}
	}
}
?>