<?php
if (!defined ('TYPO3_MODE')) die ('Access denied.');

// extend moox_news classes
$GLOBALS['TYPO3_CONF_VARS']['EXT']['moox_news']['classes']['Domain/Model/News'][] = 'moox_news_seminar';
$GLOBALS['TYPO3_CONF_VARS']['EXT']['moox_news']['classes']['Hooks/DataHandler'][] = 'moox_news_seminar';
$GLOBALS['TYPO3_CONF_VARS']['EXT']['moox_news']['classes']['Service/IcsService'][] = 'moox_news_seminar';
$GLOBALS['TYPO3_CONF_VARS']['EXT']['moox_news']['classes']['Service/GrabberService'][] = 'moox_news_seminar';

// extend moox_news flexform
$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS'][\TYPO3\CMS\Core\Configuration\FlexForm\FlexFormTools::class]['flexParsing'][]
	= \Neulandlotsen\MooxNewsSeminar\Hooks\FlexFormHook::class;

if (TYPO3_MODE === 'BE') {
	$icons = [
		'ext-news-type-mooxnewsseminar' => 'mooxnewsseminar_domain_model_news.svg',	
		'list_icon_moox_news_seminar' => 'mooxnewsseminar_domain_model_news.svg',	
		'list_icon_moox_news_seminar_speaker' => 'mooxnewsseminar_domain_model_speaker.svg',
		'module_speakermanagement' => 'module_speakermanagement.svg',
		'apps-pagetree-folder-contains-mxspeakers' => 'folder.svg',
		'mooxnewsseminar_actions-sort-down' => 'mooxnewsseminar_actions-sort-down.svg',
		'mooxnewsseminar_actions-sort-down-active' => 'mooxnewsseminar_actions-sort-down-active.svg',
		'mooxnewsseminar_actions-sort-up' => 'mooxnewsseminar_actions-sort-up.svg',
		'mooxnewsseminar_actions-sort-up-active' => 'mooxnewsseminar_actions-sort-up-active.svg',
	];
	$iconRegistry = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Imaging\IconRegistry::class);
	foreach ($icons as $identifier => $path) {
		$iconRegistry->registerIcon(
			$identifier,
			\TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
			['source' => 'EXT:moox_news_seminar/Resources/Public/Icons/' . $path]
		);
	}
	
}
?>