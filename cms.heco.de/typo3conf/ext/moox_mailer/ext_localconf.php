<?php
if (!defined('TYPO3_MODE')) {
	die ('Access denied.');
}

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
	'Neulandlotsen.' . $_EXTKEY,
	'Pi1',
	[
		'Redirector' => 'redirect'
	],
	// non-cacheable actions
	[
		'Redirector' => 'redirect',
	]
);

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
	'Neulandlotsen.' . $_EXTKEY,
	'Pi2',
	[		
		'Unregister' => 'redirect'
	],
	// non-cacheable actions
	[		
		'Unregister' => 'redirect'
	]
);

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
	'Neulandlotsen.' . $_EXTKEY,
	'Pi3',
	[		
		'HtmlView' => 'showNewsletter,error'
	],
	// non-cacheable actions
	[		
		'HtmlView' => 'showNewsletter,error'
	]
);

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
	'Neulandlotsen.' . $_EXTKEY,
	'Pi4',
	[		
		'Tracking' => 'redirect'
	],
	// non-cacheable actions
	[		
		'Tracking' => 'redirect'
	]
);

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
	'Neulandlotsen.' . $_EXTKEY,
	'Pi5',
	[		
		'Tracking' => 'open'
	],
	// non-cacheable actions
	[	
		'Tracking' => 'open'
	]
);

// Get the extensions's configuration
$extConf = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Configuration\ExtensionConfiguration::class)->get($_EXTKEY);

// If  tasks should be shown, register information for the tasks
if (true || (!empty($extConf['showTasks']) && is_dir($_SERVER["DOCUMENT_ROOT"].'/maildir')))
{	
	if(\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::isLoaded('moox_news'))
	{
		$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['scheduler']['tasks']['Neulandlotsen\\MooxMailer\\Tasks\\MooxNewsSendTask'] = [
			'extension' => $_EXTKEY,
			'title' => 'LLL:EXT:moox_mailer/Resources/Private/Language/locallang_scheduler.xlf:tx_mooxmailer_tasks_mooxnewssendtask.title',
			'description' => 'LLL:EXT:moox_mailer/Resources/Private/Language/locallang_scheduler.xlf:tx_mooxmailer_tasks_mooxnewssendtask.description',
			'additionalFields' => 'Neulandlotsen\\MooxMailer\\Tasks\\MooxNewsSendTaskAdditionalFieldProvider'
		];		
	}
	$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['scheduler']['tasks']['Neulandlotsen\\MooxMailer\\Tasks\\DaemonSendTask'] = [
		'extension' => $_EXTKEY,
		'title' => 'LLL:EXT:moox_mailer/Resources/Private/Language/locallang_scheduler.xlf:tx_mooxmailer_tasks_daemonsendtask.title',
		'description' => 'LLL:EXT:moox_mailer/Resources/Private/Language/locallang_scheduler.xlf:tx_mooxmailer_tasks_daemonsendtask.description',
		'additionalFields' => 'Neulandlotsen\\MooxMailer\\Tasks\\DaemonSendTaskAdditionalFieldProvider'
	];
	$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['scheduler']['tasks']['Neulandlotsen\\MooxMailer\\Tasks\\GetBouncesTask'] = [
		'extension' => $_EXTKEY,
		'title' => 'LLL:EXT:moox_mailer/Resources/Private/Language/locallang_scheduler.xlf:tx_mooxmailer_tasks_getbouncestask.title',
		'description' => 'LLL:EXT:moox_mailer/Resources/Private/Language/locallang_scheduler.xlf:tx_mooxmailer_tasks_getbouncestask.description'
	];
	$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['scheduler']['tasks']['Neulandlotsen\\MooxMailer\\Tasks\\DetermineQualitiyTask'] = [
		'extension' => $_EXTKEY,
		'title' => 'LLL:EXT:moox_mailer/Resources/Private/Language/locallang_scheduler.xlf:tx_mooxmailer_tasks_determinequalitytask.title',
		'description' => 'LLL:EXT:moox_mailer/Resources/Private/Language/locallang_scheduler.xlf:tx_mooxmailer_tasks_determinequalitytask.description'
	];
}

// register special icons
if (TYPO3_MODE === 'BE')
{
	// enable SignalSlot
	/** @var \TYPO3\CMS\Extbase\SignalSlot\Dispatcher $signalSlotDispatcher */
	$signalSlotDispatcher = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\\CMS\\Extbase\\SignalSlot\\Dispatcher');

	/**
	 * Provides an example .htaccess file for Apache after extension is installed and shows a warning if TYPO3 is not running on Apache.
	 */
	$signalSlotDispatcher->connect(
		'TYPO3\\CMS\\Extensionmanager\\Service\\ExtensionManagementService',
		'hasInstalledExtensions',
		'Neulandlotsen\\MooxMailer\\Service\\InstallService',
		'createMaildir'
	);

	$icons = [
		'mooxmailer_icons_frequency_control' => 'mooxmailer_frequency_control.svg',
		'mooxmailer_icons_template' => 'mooxmailer_domain_model_template.svg',
		'mooxmailer_icons_scheduler' => 'module_main.svg',
		'mooxmailer_icons_mailing_allowed' => 'moox-mailer-mail.svg',
		'apps-pagetree-folder-contains-mxmailer' => 'icon-mooxmailer-folder.svg',
		'apps-pagetree-folder-contains-mxdrafts' => 'icon-mooxmailer-draft-folder.svg',
		'apps-pagetree-folder-contains-mxsent' => 'icon-mooxmailer-sent-folder.svg'
	];
	$iconRegistry = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Imaging\IconRegistry::class);
	foreach ($icons as $identifier => $path) 
	{
		$iconRegistry->registerIcon(
			$identifier,
			\TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
			['source' => 'EXT:moox_mailer/Resources/Public/Icons/' . $path]
		);
	}
}

// extend moox_news model classes
$GLOBALS['TYPO3_CONF_VARS']['EXT']['moox_news']['classes']['Domain/Model/News'][] = 'moox_mailer';
$GLOBALS['TYPO3_CONF_VARS']['EXT']['moox_news']['classes']['Domain/Model/Category'][] = 'moox_mailer';
$GLOBALS['TYPO3_CONF_VARS']['EXT']['moox_news']['classes']['Domain/Repository/Backend/NewsRepository'][] = 'moox_mailer';
	
?>