<?php
namespace Neulandlotsen\MooxNews\ViewHelpers\Widget;

/**
 * This file is part of the "moox_news" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */
use TYPO3\CMS\Extbase\Persistence\ObjectStorage;
use TYPO3\CMS\Extbase\Persistence\QueryResultInterface;
use TYPO3\CMS\Fluid\Core\Widget\AbstractWidgetViewHelper;

/**
 * This ViewHelper renders a Pagination of news objects.
 */
class PaginateViewHelper extends AbstractWidgetViewHelper
{

    /**
     * @var \Neulandlotsen\MooxNews\ViewHelpers\Widget\Controller\PaginateController
     */
    protected $controller;

    /**
     * Inject controller
     *
     * @param \Neulandlotsen\MooxNews\ViewHelpers\Widget\Controller\PaginateController $controller
     */
    public function injectController(\Neulandlotsen\MooxNews\ViewHelpers\Widget\Controller\PaginateController $controller)
    {
        $this->controller = $controller;
    }

    /**
     * Initialize arguments
     */
    public function initializeArguments()
    {
        parent::initializeArguments();
        $this->registerArgument('objects', QueryResultInterface::class, 'Objects to auto-complete', true);
        $this->registerArgument('as', 'string', 'Property to fill', true);
        $this->registerArgument('configuration', 'array', 'configuration', false, ['itemsPerPage' => 10, 'insertAbove' => false, 'insertBelow' => true, 'maximumNumberOfLinks' => 99]);
        $this->registerArgument('initial', 'array', 'Initial configuration', false, []);
		$this->registerArgument('additionalClass', 'string', 'additional paginator class', false, '');
    }

    /**
     * @return string
     * @throws \UnexpectedValueException
     */
    public function render()
    {
        $objects = $this->arguments['objects'];

        if (!($objects instanceof QueryResultInterface || $objects instanceof ObjectStorage || is_array($objects))) {
            throw new \UnexpectedValueException('Supplied file object type ' . get_class($objects) . ' must be QueryResultInterface or ObjectStorage or be an array.', 1454510731);
        }
        return $this->initiateSubRequest();
    }
}
