<?php
namespace Neulandlotsen\MooxMailer\Controller;

/**
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */
 
use Neulandlotsen\MooxMailer\Service\MailerService;
 
class TrackingController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController 
{
	
	/**
	 * frontendUserRepository
	 *
	 * @var \Neulandlotsen\MooxMailer\Domain\Repository\FrontendUserRepository
	 */
	protected $frontendUserRepository;
	
	/**
	 * addressRepository
	 *
	 * @var \Neulandlotsen\MooxMailer\Domain\Repository\AddressRepository
	 */
	protected $addressRepository;
	
	/**
	 * newsletterRepository
	 *
	 * @var \Neulandlotsen\MooxMailer\Domain\Repository\NewsletterRepository
	 */
	protected $newsletterRepository;
	
	/**
	 * deliveryRepository
	 *
	 * @var \Neulandlotsen\MooxMailer\Domain\Repository\DeliveryRepository
	 */
	protected $deliveryRepository;
	
	/**
	 * openingRepository
	 *
	 * @var \Neulandlotsen\MooxMailer\Domain\Repository\OpeningRepository
	 */
	protected $openingRepository;
	
	/**
	 * clickRepository
	 *
	 * @var \Neulandlotsen\MooxMailer\Domain\Repository\ClickRepository
	 */
	protected $clickRepository;
	
	/**
	 * trackingSalt
	 *
	 * @var string
	 */
	public static $trackingSalt = "Sf0Cn3OpNFelyH";
	
	/**
	 * initialize the controller
	 *
	 * @return void
	 */
	protected function initializeAction() 
	{
		parent::initializeAction();		
		$this->frontendUserRepository = $this->objectManager->get('Neulandlotsen\\MooxMailer\\Domain\\Repository\\FrontendUserRepository');
		$this->addressRepository = $this->objectManager->get('Neulandlotsen\\MooxMailer\\Domain\\Repository\\AddressRepository');
		$this->newsletterRepository = $this->objectManager->get('Neulandlotsen\\MooxMailer\\Domain\\Repository\\NewsletterRepository');
		$this->deliveryRepository = $this->objectManager->get('Neulandlotsen\\MooxMailer\\Domain\\Repository\\DeliveryRepository');
		$this->openingRepository = $this->objectManager->get('Neulandlotsen\\MooxMailer\\Domain\\Repository\\OpeningRepository');
		$this->clickRepository 	= $this->objectManager->get('Neulandlotsen\\MooxMailer\\Domain\\Repository\\ClickRepository');
	}
	
	/**
	 * redirect
	 *	
	 * @param string $id
	 * @param string $job
	 * @param string $hash
	 * @param string $target
	 * @param string $url
	 * @return void
	 */
	public function redirectAction($id = "", $job = "", $hash = "", $target = "", $url = "")
	{		
		//$domain = "http://".$_SERVER['SERVER_NAME']."/";
		
		if($url!=""){
			$url = str_replace("~","/",urldecode($url));
		}
		
		// decode url
		if($target=="system"){					
			$url = base64_decode($url);			
			$url = $domain.$url;						
		} elseif($target=="internal"){			
			$url = base64_decode($url);
			$url = $domain.$url;			
		} elseif($target=="external"){
			$url = base64_decode($url);			
		} elseif($target=="email"){
			$url = base64_decode($url);			
		} else {
			$url = "";
		}
		
		if($job!="" && $id!="" && $target != "" && $url!="" && $hash!=""){
			$hash = explode("_",$hash);
			$uid = hexdec($hash[0]);
			$hash = $hash[1];
			$url = urldecode($url);    
			$jobInfo = MailerService::parseMailerJob($job);
			$receiverFound = false;
			
			if(substr(strtolower($url),0,4)!="http"){
				if(substr(strtolower($url),0,8)=="indexphp"){
					$url = str_replace("indexphp","index.php",$url);
				}
				$url = $domain.$url;
			} 
			
			if($uid>0){
				$feUser = $this->frontendUserRepository->findBouncedByUid($uid);
				if(is_object($feUser)){
					$checkHash = md5($feUser->getUid().$feUser->getCrdate().'feuser'.self::$trackingSalt);
					if($checkHash == $hash){
						$receiverFound = true;
						$receiverUid = $uid;
						$receiverType = "feuser";
						$receiver = $feUser;
					}
				}
				unset($feUser);
				if(!$receiverFound){
					$address = $this->addressRepository->findBouncedByUid($uid);
					if(is_object($address)){
						$checkHash = md5($address->getUid().$address->getCrdate().'address'.self::$trackingSalt);						
						if($checkHash == $hash){
							$receiverFound = true;
							$receiverUid = $uid;
							$receiverType = "address";
							$receiver = $address;
						}
					}
					unset($address);
				}
				
			}
			
			if($receiverFound){
				if($receiverType=="feuser"){
				
					$delivery = $this->deliveryRepository->findByJobAndFeUser($job,$receiverUid);
					
					if(is_object($delivery)){
						
						$time = time();
						
						if($delivery->getFirstClick()==0){
							$delivery->setFirstClick($time);
						}
						
						$delivery->setLastClick($time);
						$delivery->setClicks($delivery->getClicks()+1);
						
						if($delivery->getFirstOpening()==0){
							$delivery->setFirstOpening($time);
							$delivery->setLastOpening($time);
							$delivery->setOpenings($delivery->getOpenings()+1);
						}
						
						$this->deliveryRepository->update($delivery);
						
						unset($delivery);
						
						$doPersist = true;
					}
					
					$click = $this->clickRepository->findByJobAndIdAndUrlAndFeUser($job,$id,$url,$receiverUid);
					
				} elseif($receiverType=="address"){
				
					$delivery = $this->deliveryRepository->findByJobAndAddress($job,$receiverUid);
					
					if(is_object($delivery)){
						
						$time = time();
						
						if($delivery->getFirstClick()==0){
							$delivery->setFirstClick($time);
						}
						
						$delivery->setLastClick($time);
						$delivery->setClicks($delivery->getClicks()+1);
						
						if($delivery->getFirstOpening()==0){
							$delivery->setFirstOpening($time);
							$delivery->setLastOpening($time);
							$delivery->setOpenings($delivery->getOpenings()+1);
						}
						
						$this->deliveryRepository->update($delivery);
						
						unset($delivery);
						
						$doPersist = true;
					}
					
					$click = $this->clickRepository->findByJobAndIdAndUrlAndAddress($job,$id,$url,$receiverUid);
				}
				
				if(is_object($click)){
					$isFirstClick = false;
					
					$click->setLastClick(time());										
					$click->setClicks($click->getClicks()+1);
					
					$this->clickRepository->update($click);
					unset($click);
					
					$doPersist = true;
					
				} else {
					$isFirstClick = true;
					
					$click = $this->objectManager->get('Neulandlotsen\\MooxMailer\\Domain\\Model\\Click');
					
					if($receiverType=="feuser"){
						$click->addFeUser($receiver);
					} elseif($receiverType=="address"){
						$click->addAddress($receiver);
					}
					
					$time = time();
					
					$click->setTitle($receiverType." [".$receiverUid."] clicks ".$url);
					$click->setFirstClick($time);
					$click->setLastClick($time);
					$click->setJob($job);
					$click->setJobType($jobInfo['type']);
					$click->setId($id);
					$click->setTarget($target);
					$click->setUrl($url);
					$click->setClicks(1);
					
					$this->clickRepository->add($click);
					
					unset($click);
					
					$doPersist = true;
					
				}
				
				if($receiverType=="feuser"){
								
					$opening = $this->openingRepository->findByJobAndFeUser($job,$receiverUid);
					
				} elseif($receiverType=="address"){
									
					$opening = $this->openingRepository->findByJobAndAddress($job,$receiverUid);
				}
				
				if(!is_object($opening)){
					
					$isFirstOpening = true;
					
					$opening = $this->objectManager->get('Neulandlotsen\\MooxMailer\\Domain\\Model\\Opening');
					
					if($receiverType=="feuser"){
						$opening->addFeUser($receiver);
					} elseif($receiverType=="address"){
						$opening->addAddress($receiver);
					}
					
					$time = time();
					
					$opening->setTitle($receiverType." [".$receiverUid."] opens ".$job);
					$opening->setFirstOpening($time);
					$opening->setLastOpening($time);
					$opening->setJob($job);
					$opening->setJobType($jobInfo['type']);
					$opening->setOpenings(1);
					
					$this->openingRepository->add($opening);
					
					unset($opening);
					
					$doPersist = true;
					
				}
				if($jobInfo['type']=="newsletter"){
					$newsletter = $this->newsletterRepository->findByUid($jobInfo['uid'],FALSE);
					if(is_object($newsletter)){
						
						$time = time();
						
						if($newsletter->getFirstClick()==0){
							$newsletter->setFirstClick($time);
						}
						
						if($newsletter->getFirstOpening()==0){
							$newsletter->setFirstOpening($time);
							$newsletter->setLastOpening($time);
							$newsletter->setOpenings($newsletter->getOpenings()+1);
						}
						
						$newsletter->setLastClick($time);
						$newsletter->setClicks($newsletter->getClicks()+1);
						if($isFirstClick){
							$newsletter->setClicksUnique($newsletter->getClicksUnique()+1);
						}
						if($isFirstOpening){
							$newsletter->setOpeningsUnique($newsletter->getOpeningsUnique()+1);
						}
						$this->newsletterRepository->update($newsletter);
						
						unset($newsletter);
						
						$doPersist = true;
					}
				}
			}
			
			if($doPersist){
				$this->objectManager->get('TYPO3\CMS\Extbase\Persistence\PersistenceManagerInterface')->persistAll();
			}
		}
		
		if($url!=""){
			$this->redirectToURI($url);
		} else {
			exit("wrong redirect settings...");
		}
	}	
	
	/**
	 * open
	 *	
	 * @param string $job
	 * @param string $hash
	 * @param string $css
	 * @return void
	 */
	public function openAction($job = "", $hash = "",$header = "")
	{		
		if($job!="" && $hash!=""){
			
			$hash 			= explode("_",$hash);
			$uid 			= hexdec($hash[0]);
			$hash			= $hash[1];
			$jobInfo 		= MailerService::parseMailerJob($job);
			$receiverFound 	= false;						
			
			if($uid>0){
				$feUser = $this->frontendUserRepository->findBouncedByUid($uid);
				if(is_object($feUser)){
					$checkHash = md5($feUser->getUid().$feUser->getCrdate().'feuser'.self::$trackingSalt);
					if($checkHash == $hash){
						$receiverFound = true;
						$receiverUid 	= $uid;
						$receiverType 	= "feuser";
						$receiver 		= $feUser;
					}
				}
				unset($feUser);
				if(!$receiverFound){
					$address = $this->addressRepository->findBouncedByUid($uid);
					if(is_object($address)){
						$checkHash 		= md5($address->getUid().$address->getCrdate().'address'.self::$trackingSalt);						
						if($checkHash == $hash){
							$receiverFound = true;
							$receiverUid 	= $uid;
							$receiverType 	= "address";
							$receiver 		= $address;
						}
					}
					unset($address);
				}
				
			}	
			if($receiverFound){
				if($receiverType=="feuser"){
				
					$delivery = $this->deliveryRepository->findByJobAndFeUser($job,$receiverUid);
					
					if(is_object($delivery)){
						
						$time = time();
						
						if($delivery->getFirstOpening()==0){
							$delivery->setFirstOpening($time);
						}
						
						$delivery->setLastOpening($time);
						$delivery->setOpenings($delivery->getOpenings()+1);
						
						$this->deliveryRepository->update($delivery);
						
						unset($delivery);
						
						$doPersist = true;
					}
					
					$opening = $this->openingRepository->findByJobAndFeUser($job,$receiverUid);
					
				} elseif($receiverType=="address"){
				
					$delivery = $this->deliveryRepository->findByJobAndAddress($job,$receiverUid);
					
					if(is_object($delivery)){
						
						$time = time();
						
						if($delivery->getFirstOpening()==0){
							$delivery->setFirstOpening($time);
						}
						
						$delivery->setLastOpening($time);
						$delivery->setOpenings($delivery->getOpenings()+1);
						
						$this->deliveryRepository->update($delivery);
						
						unset($delivery);
						
						$doPersist = true;
					}
					
					$opening = $this->openingRepository->findByJobAndAddress($job,$receiverUid);
				}
				
				if(is_object($opening)){
					$isFirstOpening = false;
					
					$opening->setLastOpening(time());										
					$opening->setOpenings($opening->getOpenings()+1);
					
					$this->openingRepository->update($opening);
					unset($opening);
					
					$doPersist = true;
					
				} else {
					$isFirstOpening = true;
					
					$opening = $this->objectManager->get('Neulandlotsen\\MooxMailer\\Domain\\Model\\Opening');
					
					if($receiverType=="feuser"){
						$opening->addFeUser($receiver);
					} elseif($receiverType=="address"){
						$opening->addAddress($receiver);
					}
					
					$time = time();
					
					$opening->setTitle($receiverType." [".$receiverUid."] opens ".$job);
					$opening->setFirstOpening($time);
					$opening->setLastOpening($time);
					$opening->setJob($job);
					$opening->setJobType($jobInfo['type']);
					$opening->setOpenings(1);
					
					$this->openingRepository->add($opening);
					
					unset($opening);
					
					$doPersist = true;
					
				}
				if($jobInfo['type']=="newsletter"){
					$newsletter = $this->newsletterRepository->findByUid($jobInfo['uid'],FALSE);
					if(is_object($newsletter)){
						
						$time = time();
						
						if($newsletter->getFirstOpening()==0){
							$newsletter->setFirstOpening($time);
						}
						
						$newsletter->setLastOpening($time);
						$newsletter->setOpenings($newsletter->getOpenings()+1);
						if($isFirstOpening){
							$newsletter->setOpeningsUnique($newsletter->getOpeningsUnique()+1);
						}
						$this->newsletterRepository->update($newsletter);
						
						unset($newsletter);
						
						$doPersist = true;
					}
				}
			}
			
			if($doPersist){
				$this->objectManager->get('TYPO3\CMS\Extbase\Persistence\PersistenceManagerInterface')->persistAll();
			}
		}
		
		if(strtolower($header)=="css"){
			header('Content-type: text/css');
		} else {
			$header = 'gif';
			header('Content-Type: image/gif');
			$image = imagecreatetruecolor(1,1);
			$white = imagecolorallocate($image, 255, 255, 255);
			imagefill($image, 0, 0, $white);
			imagejpeg($image);
		}
		exit();
	}
}
?>