<?php
namespace Neulandlotsen\MooxNews\ViewHelpers\Widget\Controller;

/**
 * This file is part of the "moox_news" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Utility\ArrayUtility;
use TYPO3\CMS\Extbase\Persistence\ObjectStorage;
use TYPO3\CMS\Extbase\Persistence\QueryResultInterface;
use TYPO3\CMS\Fluid\Core\Widget\AbstractWidgetController;

/**
 * Paginate controller to create the pagination.
 * Extended version from fluid core
 *
 */
class PaginateController extends AbstractWidgetController
{
    /**
     * @var array
     */
    protected $configuration = [
        'itemsPerPage' => 10,
        'insertAbove' => false,
        'insertBelow' => true,
        'maximumNumberOfLinks' => 99,
        'templatePath' => '',
		'addQueryStringMethod' => 'GET',
        'section' => ''
    ];

    /**
     * @var QueryResultInterface|ObjectStorage|array
     */
    protected $objects;

    /**
     * @var int
     */
    protected $currentPage = 1;

    /**
     * @var string
     */
    protected $templatePath = '';

    /**
     * @var int
     */
    protected $numberOfPages = 1;
	
	/**
     * @var int
     */
    protected $displayRangeStart = null;

    /**
     * @var int
     */
    protected $displayRangeEnd = null;

    /**
     * @var int
     */
    protected $maximumNumberOfLinks = 99;

   /**
     * @var int
     */
    protected $initialOffset = 0;
	
   /**
     * @var int
     */
    protected $initialLimit = 0;
    
	/**
     * @var int
     */
    protected $recordId = 0;

	/**
	 * @var string
	 */
	protected $additionalClass = '';

    /**
     * Initialize the action and get correct configuration
     *
     */
    public function initializeAction()
    {
        $this->objects = $this->widgetConfiguration['objects'];
        ArrayUtility::mergeRecursiveWithOverrule($this->configuration, $this->widgetConfiguration['configuration'], false);
        $itemsPerPage = (int)$this->configuration['itemsPerPage'];
        $this->numberOfPages = $itemsPerPage > 0 ? ceil(count($this->objects) / $itemsPerPage) : 0;
        $this->maximumNumberOfLinks = (int)$this->configuration['maximumNumberOfLinks'];
		$this->additionalClass = $this->widgetConfiguration['additionalClass'];
		
        if (isset($this->configuration['templatePath']) && !empty($this->configuration['templatePath'])) {
            $this->templatePath = GeneralUtility::getFileAbsFileName($this->configuration['templatePath']);
        }
        if (isset($this->widgetConfiguration['initial']['offset'])) {
            $this->initialOffset = (int)$this->widgetConfiguration['initial']['offset'];
        }
        if (isset($this->widgetConfiguration['initial']['limit'])) {
            $this->initialLimit = (int)$this->widgetConfiguration['initial']['limit'];
        }
        if (isset($this->widgetConfiguration['initial']['recordId'])) {
            $this->recordId = (int)$this->widgetConfiguration['initial']['recordId'];
        }
    }

    /**
     * Main action
     *
     * @param int $currentPage
     */
    public function indexAction($currentPage = 1)
    {
        // set current page
        $this->currentPage = (int)$currentPage;
        if ($this->currentPage < 1)
		{
            $this->currentPage = 1;
        }

        if ($this->currentPage > $this->numberOfPages)
		{
            // set $modifiedObjects to NULL if the page does not exist
            $modifiedObjects = null;
        } 
		else 
		{
            // modify query
            $itemsPerPage = (integer)$this->configuration['itemsPerPage'];
            $query = $this->objects->getQuery();

            if ($this->currentPage === $this->numberOfPages && $this->initialLimit > 0)
			{
                $difference = $this->initialLimit - ((integer)($itemsPerPage * ($this->currentPage - 1)));
                if ($difference > 0)
				{
                    $query->setLimit($difference);
                } 
				else 
				{
                    $query->setLimit($itemsPerPage);
                }
            } 
			else 
			{
                $query->setLimit($itemsPerPage);
            }

            if ($this->currentPage > 1) 
			{
                $offset = (integer)($itemsPerPage * ($this->currentPage - 1));
                $offset += $this->initialOffset;
                $query->setOffset($offset);
            } 
			elseif ($this->initialOffset > 0) 
			{
                $query->setOffset($this->initialOffset);
            }
            $modifiedObjects = $this->prepareObjectsSlice($itemsPerPage, $offset);
        }

        if ($this->currentPage > 1) {
            $pageLabel = \TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate('paginate_overall', 'news', [$this->currentPage, $this->numberOfPages]);
            $GLOBALS['TSFE']->page['title'] = $GLOBALS['TSFE']->page['title'] . ' - ' . trim($pageLabel, '.');
        }

        $this->view->assign('contentArguments', [
            $this->widgetConfiguration['as'] => $modifiedObjects
        ]);
        $this->view->assign('configuration', $this->configuration);
        $this->view->assign('recordId', $this->recordId);
        $this->view->assign('pageId', $this->getCurrentPageId());
        $this->view->assign('pagination', $this->buildPagination());
		$this->view->assign('additionalClass', $this->additionalClass);

        if (!empty($this->templatePath)) {
            $this->view->setTemplatePathAndFilename($this->templatePath);
        }
    }

    /**
     * Returns an array with the keys "pages", "current", "numberOfPages",
     * "nextPage" & "previousPage"
     *
     * @return array
     */
    protected function buildPagination()
    {
        $this->calculateDisplayRange();
        $pages = [];
        for ($i = $this->displayRangeStart; $i <= $this->displayRangeEnd; $i++) {
            $pages[] = ['number' => $i, 'isCurrent' => $i === $this->currentPage];
        }
        $pagination = [
            'pages' => $pages,
            'current' => $this->currentPage,
            'numberOfPages' => $this->numberOfPages,
            'displayRangeStart' => $this->displayRangeStart,
            'displayRangeEnd' => $this->displayRangeEnd,
            'hasLessPages' => $this->displayRangeStart > 2,
            'hasMorePages' => $this->displayRangeEnd + 1 < $this->numberOfPages
        ];
        if ($this->currentPage < $this->numberOfPages) {
            $pagination['nextPage'] = $this->currentPage + 1;
        }
        if ($this->currentPage > 1) {
            $pagination['previousPage'] = $this->currentPage - 1;
        }
        return $pagination;
    }

    /**
     * If a certain number of links should be displayed, adjust before and after
     * amounts accordingly.
     */
    protected function calculateDisplayRange()
    {
        $maximumNumberOfLinks = $this->maximumNumberOfLinks;
        if ($maximumNumberOfLinks > $this->numberOfPages) {
            $maximumNumberOfLinks = $this->numberOfPages;
        }
        $delta = floor($maximumNumberOfLinks / 2);
        $this->displayRangeStart = $this->currentPage - $delta;
        $this->displayRangeEnd = $this->currentPage + $delta - ($maximumNumberOfLinks % 2 === 0 ? 1 : 0);
        if ($this->displayRangeStart < 1) {
            $this->displayRangeEnd -= $this->displayRangeStart - 1;
        }
        if ($this->displayRangeEnd > $this->numberOfPages) {
            $this->displayRangeStart -= $this->displayRangeEnd - $this->numberOfPages;
        }
        $this->displayRangeStart = (int)max($this->displayRangeStart, 1);
        $this->displayRangeEnd = (int)min($this->displayRangeEnd, $this->numberOfPages);
    }
	
	/**
     * @param int $itemsPerPage
     * @param int $offset
     *
     * @return array|QueryResultInterface
     * @throws \InvalidArgumentException
     */
    protected function prepareObjectsSlice($itemsPerPage, $offset)
    {
        if ($this->objects instanceof QueryResultInterface) {
            $query = $this->objects->getQuery();
            $query->setLimit($itemsPerPage);
            if ($offset > 0) {
                $query->setOffset($offset);
            }
            $modifiedObjects = $query->execute();
            return $modifiedObjects;
        }
        if ($this->objects instanceof ObjectStorage) {
            $modifiedObjects = [];
            $objectArray = $this->objects->toArray();
            $endOfRange = min($offset + $itemsPerPage, count($objectArray));
            for ($i = $offset; $i < $endOfRange; $i++) {
                $modifiedObjects[] = $objectArray[$i];
            }
            return $modifiedObjects;
        }
        if (is_array($this->objects)) {
            $modifiedObjects = array_slice($this->objects, $offset, $itemsPerPage);
            return $modifiedObjects;
        }
        throw new \InvalidArgumentException(
                'The view helper "' . get_class($this)
                . '" accepts as argument "QueryResultInterface", "\SplObjectStorage", "ObjectStorage" or an array. '
                . 'given: ' . get_class($this->objects),
                1385547291
            );
    }
	
    /**
     * @return int
     */
    protected function getCurrentPageId()
    {
        if (is_object($GLOBALS['TSFE'])) {
            return (int)$GLOBALS['TSFE']->id;
        }
        return 0;
    }
}
