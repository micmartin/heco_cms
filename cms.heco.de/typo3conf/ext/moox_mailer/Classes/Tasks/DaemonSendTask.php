<?php
namespace Neulandlotsen\MooxMailer\Tasks;

/**
 * This file is part of the "moox_mailer" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

use TYPO3\CMS\Core\Core\Environment;
use TYPO3\CMS\Extbase\Utility\LocalizationUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Messaging\FlashMessage;
use TYPO3\CMS\Core\Messaging\FlashMessageService;
 
class DaemonSendTask extends \TYPO3\CMS\Scheduler\Task\AbstractTask 
{			
	/**
	 * Mails pro Durchgang
	 *
	 * @var int
	 */
	public $mailsPerProcess;
	
	/**
	 * Logging deaktivieren
	 *
	 * @var bool
	 */
	public $noLog;
	
	/**
	 * mailer dir
	 *
	 * @var string
	 */
	public $mailerDir;
	
	/**
	 * is cli call
	 *
	 * @var bool
	 */
	public $isCli;
	
	/**
	 * flash message service
	 *
	 * @var \TYPO3\CMS\Core\Messaging\FlashMessageService
	 */
	public $flashMessageService;
	
	/**
	 * Process prepared mailer jobs.
	 *
	 * @return	boolean	Returns TRUE on success, FALSE if no items were indexed or none were found.
	 * @see	typo3/sysext/scheduler/tx_scheduler_Task#execute()
	 */
	public function execute()
	{											
		$executionSucceeded = TRUE;

		if ( basename( Environment::getCurrentScript() ) == 'typo3' )
		{
			$isCli = true;
		}
		else
		{
			$isCli = false;
		}
		
		$objectManager = GeneralUtility::makeInstance('TYPO3\CMS\Extbase\Object\ObjectManager');
		
		if(!$isCli)
		{
			$flashMessageService = $objectManager->get(FlashMessageService::class);
			$flashMessageQueue = $flashMessageService->getMessageQueueByIdentifier();
		}
		
		// Set global settings
		$this->setMailerDir(Environment::getPublicPath() . '/' . 'maildir');
		
		// check if maildir ist present
		if(is_dir($this->getMailerDir()))
		{		
			// initiate daemon
			$daemon = $objectManager->get('Neulandlotsen\MooxMailer\Service\DaemonService');
			$daemon->initialize($this);
			
			// log daemon info
			$daemon->log("################ NEW DAEMON EXECUTION ################");
			
			// check if mailer is ready to do something
			if($daemon->mailerReady())
			{				
				// log daemon info
				$daemon->log("DAEMON: is ready...");
				$daemon->log("DAEMON: started...");
				
				// get next job to do
				$job = $daemon->getNextJob();
			
				// do if job is found
				if($job)
				{				
					// load mailer job
					$mailer = $objectManager->get('Neulandlotsen\MooxMailer\Service\MailerService');
					$mailer->initialize($this,$job);
					
					$processed = 0;
					$todo = count($mailer->getMailfilesPrepared());
					
					// log daemon info
					$daemon->log("DAEMON: job found ".$job." (".$todo." mail(s) left to process)...");
					
					if($todo>0)
					{						
						// log daemon info
						$daemon->log("DAEMON: more than one to do...");
						
						// lock other mail instancees
						$daemon->lockMailer("[".date("Y-m-d H:i:s")."] locked by ".$job);
						
						// log daemon info
						$daemon->log("DAEMON: mailer lock set ".$job."...");
						
						// go through all prepared mails of current job
						foreach($mailer->getMailfilesPrepared() AS $mailfile)
						{												
							$processed++;
							
							// get maildata for xml
							$maildata = $mailer->readMailFromXML($mailfile);

							if(!$maildata['parseError'])
							{								
								// send mail in daemon mode
								$mailer->sendMail($maildata,true);
								
								// mark mailfile of this mail as process
								$mailer->markMailfileAsProcessed($mailfile);																
								
							} 
							else 
							{								
								// mark mailfile of this mail as not parseable
								$mailer->markMailfileAsNotParseable($mailfile);
								$mailer->errorlog("ERROR: Mail-XML of File: ".$mailfile." could not be parsed");							
							}
							
							// if daemon has reached max mailcount per process, end process
							if($processed>=$daemon->getMailsPerProcess())
							{
								break;
							}
						}
					} 
					else 
					{						
						// log daemon info						
						$daemon->log("DAEMON: nothing more to do");
						
						if(!$isCli)
						{
							$message = GeneralUtility::makeInstance('TYPO3\\CMS\\Core\\Messaging\\FlashMessage',
								"[".date("Y-m-d H:i:s")."] DAEMON: says nothing to do...\n",
								 '', // the header is optional
								 FlashMessage::OK, // the severity is optional as well and defaults to FlashMessage::OK
								 TRUE // optional, whether the message should be stored in the session or only in the FlashMessageQueue object (default is FALSE)
							);
							$flashMessageQueue->addMessage($message);
						}
					}
					
					// check if job is completly finishe
					if($mailer->jobFinished())
					{					
						$mailer->finishJob();	
						
						// log daemon info	
						$daemon->log("DAEMON: job ".$job." finished (0 Mail(s) left)...");
						
						if(!$isCli)
						{
							$message = GeneralUtility::makeInstance('TYPO3\\CMS\\Core\\Messaging\\FlashMessage',
								"[".date("Y-m-d H:i:s")."] DAEMON: Job ".$mailer->getIdentifier()."_".$mailer->getDescription()." successfully finished (".$mailer->getMailCount()." mail(s) processed)...\n",
								 '', // the header is optional
								 FlashMessage::OK, // the severity is optional as well and defaults to FlashMessage::OK
								 TRUE // optional, whether the message should be stored in the session or only in the FlashMessageQueue object (default is FALSE)
							);
							$flashMessageQueue->addMessage($message);
						}							
					} 
					// if there's anything left to do
					else 
					{					
						$mailer->log("MAILER: Job ".$mailer->getIdentifier()."_".$mailer->getDescription()." successfully processed (".$processed." mail(s) processed, ".$mailer->getMailsPrepared()." mail(s) left)");
						$mailer->log("MAILER: Job ".$mailer->getIdentifier()."_".$mailer->getDescription()." successfully processed (".$processed." mail(s) processed, ".$mailer->getMailsPrepared()." mail(s) left)","global");
						
						// log daemon info	
						$daemon->log("DAEMON: job ".$job." processed (".$processed." mail(s) processed, ".$mailer->getMailsPrepared()." mail(s) left)...");
						
						if(!$isCli)
						{
							$message = GeneralUtility::makeInstance('TYPO3\\CMS\\Core\\Messaging\\FlashMessage',
								"[".date("Y-m-d H:i:s")."] DAEMON: job ".$job." processed (".$processed." mail(s) processed, ".$mailer->getMailsPrepared()." mail(s) left)...\n",
								 '', // the header is optional
								 FlashMessage::OK, // the severity is optional as well and defaults to FlashMessage::OK
								 TRUE // optional, whether the message should be stored in the session or only in the FlashMessageQueue object (default is FALSE)
							);
							$flashMessageQueue->addMessage($message);						
						}
					}
					
					// remove mailer lock 
					$daemon->removeLock();
					
					// log daemon info	
					$daemon->log("DAEMON: mailer lock removed ".$job."...");					
				} 
				else 
				{
					// log daemon info
					$daemon->log("DAEMON: no job prepared, nothing to do...");
					
					if(!$isCli)
					{
						$message = GeneralUtility::makeInstance('TYPO3\\CMS\\Core\\Messaging\\FlashMessage',
							"[".date("Y-m-d H:i:s")."] DAEMON: no job prepared, nothing to do...\n",
							 '', // the header is optional
							 FlashMessage::OK, // the severity is optional as well and defaults to FlashMessage::OK
							 TRUE // optional, whether the message should be stored in the session or only in the FlashMessageQueue object (default is FALSE)
						);
						$flashMessageQueue->addMessage($message);		
					}						
				}
				
				// log daemon info
				$daemon->log("DAEMON: stopped...");
				
				$executionSucceeded = TRUE;
				
			} 
			else 
			{				
				$lockInfo = $daemon->lockInfo();
				$lockInfoParts = explode("]",$lockInfo);
				$lockInfoText = trim($lockInfoParts[1]);
				$lockInfoDate = trim($lockInfoParts[0])."]";
				
				// log to console
				$text = "[".date("Y-m-d H:i:s")."] DAEMON says: mailer ".$lockInfoText." ".$lockInfoDate."...\n";
				
				// log daemon info
				$daemon->log("DAEMON: MAILER LOCKED - ".$lockInfoText." ".$lockInfoDate);
				
				if(!$isCli)
				{
					$message = GeneralUtility::makeInstance('TYPO3\\CMS\\Core\\Messaging\\FlashMessage',
						$text,
						 '', // the header is optional
						 FlashMessage::ERROR, // the severity is optional as well and defaults to FlashMessage::OK
						 TRUE // optional, whether the message should be stored in the session or only in the FlashMessageQueue object (default is FALSE)
					);
					$flashMessageQueue->addMessage($message);
				}
				
				$executionSucceeded = FALSE;
			}			
		} 
		else 
		{			
			if(!$isCli)
			{
				$message = GeneralUtility::makeInstance('TYPO3\\CMS\\Core\\Messaging\\FlashMessage',
					"[".date("Y-m-d H:i:s")."] DAEMON: says: Folder \"maildir\" is missing. Please use \"Mailer-Admin\" to generate mailer folders.",
					 '', // the header is optional
					 FlashMessage::ERROR, // the severity is optional as well and defaults to FlashMessage::OK
					 TRUE // optional, whether the message should be stored in the session or only in the FlashMessageQueue object (default is FALSE)
				);
				$flashMessageQueue->addMessage($message);	
			}

			$executionSucceeded = FALSE;		
		}
				
		return $executionSucceeded;
	}
	
	/**
	 * This method returns the sleep duration as additional information
	 *
	 * @return string Information to display
	 */
	public function getAdditionalInformation()
	{		
		$info = LocalizationUtility::translate( 'LLL:EXT:moox_mailer/Resources/Private/Language/locallang_scheduler.xlf:tx_mooxmailer_tasks_daemonsendtask.mails_per_process_label', 'moox_mailer' ).": ".$this->mailsPerProcess;
				
		return $info;
	}
	
	/**
	 * @return int
	 */
	public function getMailsPerProcess()
	{
		return $this->mailsPerProcess;
	}

	/**
	 * @param int $mailsPerProcess
	 */
	public function setMailsPerProcess($mailsPerProcess)
	{
		$this->mailsPerProcess = $mailsPerProcess;
	}
	
	/**
	 * @return string
	 */
	public function getMailerDir()
	{
		return $this->mailerDir;
	}

	/**
	 * @param string $mailerDir
	 */
	public function setMailerDir($mailerDir)
	{
		$this->mailerDir = $mailerDir;
	}
	
	/**
	 * @return bool $noLog
	 */
	public function getNoLog()
	{
		return $this->noLog;
	}

	/**
	 * @param bool $noLog
	 */
	public function setNoLog($noLog)
	{
		$this->noLog = $noLog;
	}
}
?>