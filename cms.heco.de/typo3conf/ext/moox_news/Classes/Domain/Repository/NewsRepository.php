<?php

namespace Neulandlotsen\MooxNews\Domain\Repository;

/**
 * This file is part of the "moox_news" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */
use Neulandlotsen\MooxNews\Domain\Model\DemandInterface;
use Neulandlotsen\MooxNews\Domain\Model\Dto\NewsDemand;
use Neulandlotsen\MooxNews\Service\CategoryService;
use Neulandlotsen\MooxNews\Utility\ConstraintHelper;
use Neulandlotsen\MooxNews\Utility\Validation;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Database\Query\Restriction\DeletedRestriction;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Utility\LocalizationUtility;
use TYPO3\CMS\Extbase\Persistence\QueryInterface;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;

/**
 * News repository with all the callable functionality
 */
class NewsRepository extends \Neulandlotsen\MooxNews\Domain\Repository\AbstractDemandedRepository
{

    /**
     * Returns a category constraint created by
     * a given list of categories and a junction string
     *
     * @param QueryInterface $query
     * @param  array $categories
     * @param  string $conjunction
     * @param  bool $includeSubCategories
     * @return \TYPO3\CMS\Extbase\Persistence\Generic\Qom\ConstraintInterface|null
     */
    protected function createCategoryConstraint(
        QueryInterface $query,
        $categories,
        $conjunction,
        $includeSubCategories = false
    ) {
        $constraint = null;
        $categoryConstraints = [];

        // If "ignore category selection" is used, nothing needs to be done
        if (empty($conjunction)) {
            return $constraint;
        }

        if (!is_array($categories)) {
            $categories = GeneralUtility::intExplode(',', $categories, true);
        }
        foreach ($categories as $category) {
            if ($includeSubCategories) {
                $subCategories = GeneralUtility::trimExplode(',',
                    CategoryService::getChildrenCategories($category, 0, '', true), true);
                $subCategoryConstraint = [];
                $subCategoryConstraint[] = $query->contains('categories', $category);
                if (count($subCategories) > 0) {
                    foreach ($subCategories as $subCategory) {
                        $subCategoryConstraint[] = $query->contains('categories', $subCategory);
                    }
                }
                if ($subCategoryConstraint) {
                    $categoryConstraints[] = $query->logicalOr($subCategoryConstraint);
                }
            } else {
                $categoryConstraints[] = $query->contains('categories', $category);
            }
        }

        if ($categoryConstraints) {
            switch (strtolower($conjunction)) {
                case 'or':
                    $constraint = $query->logicalOr($categoryConstraints);
                    break;
                case 'notor':
                    $constraint = $query->logicalNot($query->logicalOr($categoryConstraints));
                    break;
                case 'notand':
                    $constraint = $query->logicalNot($query->logicalAnd($categoryConstraints));
                    break;
                case 'and':
                default:
                    $constraint = $query->logicalAnd($categoryConstraints);
            }
        }

        return $constraint;
    }

    /**
     * Returns an array of constraints created from a given demand object.
     *
     * @param QueryInterface $query
     * @param DemandInterface $demand
	 * @param bool $respectEnableFields
     * @throws \UnexpectedValueException
     * @throws \InvalidArgumentException
     * @throws \Exception
     * @return array<\TYPO3\CMS\Extbase\Persistence\Generic\Qom\ConstraintInterface>
     */
    protected function createConstraintsFromDemand(
		QueryInterface $query,
		DemandInterface $demand,
		$respectEnableFields = true
	){
    	/** @var NewsDemand $demand */
        $constraints = [];

		if(is_array($demand->getUids()) && count($demand->getUids()))
		{
			$constraints[] = $query->in('uid', $demand->getUids());
		}

		if(is_array($demand->getTypes()) && count($demand->getTypes()))
		{
			$constraints[] = $query->in('type', $demand->getTypes());		
		} 

		if($demand->getType()!="" && $demand->getType()!=0)
		{
			$constraints[] = $query->equals('type', (string)$demand->getType());		
		}
		
		if($demand->getIncludeOnlySelectedNewsInRss())
		{
			$constraints[] = $query->equals('shareRss', 1);		
		}
	
        if ($demand->getCategories() && $demand->getCategories() !== '0')
		{
            $constraints['categories'] = $this->createCategoryConstraint(
                $query,
                $demand->getCategories(),
                $demand->getCategoryConjunction()?$demand->getCategoryConjunction():'or',
                $demand->getIncludeSubCategories()?$demand->getIncludeSubCategories():1
            );
        }

        if ($demand->getAuthor())
		{
            $constraints['author'] = $query->equals('author', $demand->getAuthor());
        }
		
		if ($demand->getQuery())
		{
			if(is_array($demand->getTypes()))
			{
				$type = reset($demand->getTypes());
			}
			if($type!="" && isset($GLOBALS['TCA']['tx_mooxnews_domain_model_news']['moox']['searchFields'][$type]) && is_array($GLOBALS['TCA']['tx_mooxnews_domain_model_news']['moox']['searchFields'][$type]) && count($GLOBALS['TCA']['tx_mooxnews_domain_model_news']['moox']['searchFields'][$type]))
			{			
				$searchFields = $GLOBALS['TCA']['tx_mooxnews_domain_model_news']['moox']['searchFields'][$type];
			} 
			else 
			{
				$searchFields = $GLOBALS['TCA']['tx_mooxnews_domain_model_news']['moox']['searchFields']['moox_news'];
			}
			if(!count($searchFields))
			{
				$searchFields = ["title","teaser","bodytext"];
			}
			$queryConstraints = [];
			foreach($searchFields AS $searchField)
			{
				$queryConstraints[] = $query->like($searchField, "%".$demand->getQuery()."%");
			}
			$constraints['query'] = $query->logicalOr($queryConstraints);
		}
		
		if ($demand->getChar())
		{
			if($demand->getChar()!="other")
			{				
				$char = substr($demand->getChar(),0,1);
				if(in_array($char,$demand->getCharList()))
				{
					$constraints['char'] = $query->like("title", $char."%");
				}
			} 
			else 
			{
				$charConstraints = [];
				foreach($demand->getCharList() AS $char)
				{
					if($char!="all" && $char!="other")
					{
						$charConstraints[] = $query->logicalNot($query->like("title", $char."%"));
					}
				}				
				if(count($charConstraints))
				{
					$constraints['char'] = $query->logicalAnd($charConstraints);
				}
			}
		}

        // archived
        if ($demand->getArchiveRestriction() == 'archived')
		{
            $constraints['archived'] = $query->logicalAnd(
                $query->lessThan('archive', $GLOBALS['EXEC_TIME']),
                $query->greaterThan('archive', 0)
            );
        } 
		elseif ($demand->getArchiveRestriction() == 'active')
		{
            $constraints['active'] = $query->logicalOr(
                $query->greaterThanOrEqual('archive', $GLOBALS['EXEC_TIME']),
                $query->equals('archive', 0)
            );
        }
		
		if($demand->getDateField()=="type" && is_array($demand->getTypes()) && count($demand->getTypes())==1)
		{
		// TODO: check why count is checked against value "1"
		//if($demand->getDateField()=="type" && is_array($demand->getTypes()))
		//{
			$type = reset($demand->getTypes());
			if(isset($GLOBALS['TCA']['tx_mooxnews_domain_model_news']['moox']['dateTimeDefaultField'][$type])&& $GLOBALS['TCA']['tx_mooxnews_domain_model_news']['moox']['dateTimeDefaultField'][$type]!="")
			{			
				$dateTimeDefaultField = $GLOBALS['TCA']['tx_mooxnews_domain_model_news']['moox']['dateTimeDefaultField'][$type];		
			} 
			else
			{
				$dateTimeDefaultField = $GLOBALS['TCA']['tx_mooxnews_domain_model_news']['moox']['dateTimeDefaultField']['moox_news'];
			}
			$demand->setDateField($dateTimeDefaultField);
		} 
		elseif($demand->getDateField()=="type")
		{
			$dateTimeDefaultField = $GLOBALS['TCA']['tx_mooxnews_domain_model_news']['moox']['dateTimeDefaultField']['moox_news'];			
			$demand->setDateField($dateTimeDefaultField);
		}

        // Time restriction greater than or equal
        $timeRestrictionField = $demand->getDateField();
        $timeRestrictionField = (empty($timeRestrictionField)) ? 'datetime' : $timeRestrictionField;
		$timeRestrictionFieldUnderscored = GeneralUtility::camelCaseToLowerCaseUnderscored($timeRestrictionField);		

        if ($demand->getTimeRestriction()) {
            $timeLimit = ConstraintHelper::getTimeRestrictionLow($demand->getTimeRestriction());

            $constraints['timeRestrictionGreater'] = $query->greaterThanOrEqual(
                $timeRestrictionField,
                $timeLimit
            );
        }

        // Time restriction less than or equal
        if ($demand->getTimeRestrictionHigh()) {
            $timeLimit = ConstraintHelper::getTimeRestrictionHigh($demand->getTimeRestrictionHigh());

            $constraints['timeRestrictionLess'] = $query->lessThanOrEqual(
                $timeRestrictionField,
                $timeLimit
            );
        }

        // top news
        if ($demand->getTopNewsRestriction() == 1) {
            $constraints['topNews1'] = $query->equals('istopnews', 1);
        } elseif ($demand->getTopNewsRestriction() == 2) {
            $constraints['topNews2'] = $query->equals('istopnews', 0);
        }
		
		// privacy
		if ($demand->getPrivacy() === "0")
		{
			$constraints['privacy'] = $query->equals('privacy', 0);
		} 
		elseif ($demand->getPrivacy() === "1")
		{
			$constraints['privacy'] = $query->equals('privacy', 1);		
		}
		
		// exclude from rss
		if ($demand->getExcludeFromRss() == 1)
		{
			$constraints['excludeFromRss'] = $query->equals('excludeFromRss', 0);			
		}

        // storage page
        if ($demand->getStoragePage() != 0) {
            $pidList = GeneralUtility::intExplode(',', $demand->getStoragePage(), true);
            $constraints['pid'] = $query->in('pid', $pidList);
        }

		//calendar filter
		if ($demand->getCalendar()!="" && strlen($demand->getCalendar())>=4 && strlen($demand->getCalendar())<=6)
		{
			if(strlen($demand->getCalendar())==4)
			{
				$calendarYear = $demand->getCalendar();
			}
			else
			{
				$calendarYear = substr($demand->getCalendar(),0,4);
				$calendarMonth = substr($demand->getCalendar(),4);
			}

			if($calendarYear>1900 && $calendarYear<3000)
			{
				if((int)$calendarMonth>0 && (int)$calendarMonth<=12)
				{
					$calendarConstraints[] = $query->greaterThanOrEqual(
						$timeRestrictionField,
						mktime(0,0,0,$calendarMonth,1,$calendarYear)
					);
					$calendarConstraints[] = $query->lessThan(
						$timeRestrictionField,
						(mktime(0,0,0,$calendarMonth+1,1,$calendarYear)-1)
					);					
				}
				else
				{
					$calendarConstraints[] = $query->greaterThanOrEqual(
						$timeRestrictionField,
						mktime(0,0,0,1,1,$calendarYear)
					);
					$calendarConstraints[] = $query->lessThan(
						$timeRestrictionField,
						(mktime(0,0,0,1,1,$calendarYear+1)-1)
					);
				}
				$constraints['calendar'] = $query->logicalAnd($calendarConstraints);
				
			}			
		}
		elseif ($demand->getCalendarFrom()!="" || $demand->getCalendarTo()!="")
		{
			
			$calendarConstraints = [];
			
			if($GLOBALS['TCA']['tx_mooxnews_domain_model_news']['columns'][$timeRestrictionFieldUnderscored]['moox']['oppositeField'])
			{
				$oppositeField = $GLOBALS['TCA']['tx_mooxnews_domain_model_news']['columns'][$timeRestrictionFieldUnderscored]['moox']['oppositeField'];					
			}
			if(!isset($GLOBALS['TCA']['tx_mooxnews_domain_model_news']['columns'][$oppositeField]) || !in_array($GLOBALS['TCA']['tx_mooxnews_domain_model_news']['columns'][$oppositeField]['config']['eval'],['date','datetime']))
			{
				unset($oppositeField);
			}			
			if ($demand->getCalendarFrom()!="")
			{
				
				$calendarFrom = strtotime($demand->getCalendarFrom());
				if($calendarFrom>0)
				{
					$calendarFrom = mktime(0,0,0,date("n",$calendarFrom),date("j",$calendarFrom),date("Y",$calendarFrom));
					if($oppositeField)
					{
						$calendarFromConstraints[] = $query->greaterThanOrEqual($timeRestrictionField,$calendarFrom);
						$calendarFromConstraints[] = $query->greaterThanOrEqual($oppositeField,$calendarFrom);
						$calendarConstraints[] = $query->logicalOr($calendarFromConstraints);
					}
					else
					{
						$calendarConstraints[] = $query->greaterThanOrEqual($timeRestrictionField,$calendarFrom);
					}
				}
			}
			if ($demand->getCalendarTo()!="")
			{
				$calendarTo = strtotime($demand->getCalendarTo());
				if($calendarTo>0)
				{
					$calendarTo = mktime(23,59,59,date("n",$calendarTo),date("j",$calendarTo),date("Y",$calendarTo));
					$calendarConstraints[] = $query->lessThanOrEqual($timeRestrictionField,$calendarTo);
				}
			}
			if(count($calendarConstraints)>1)
			{
				$constraints['calendar'] = $query->logicalAnd($calendarConstraints);
			} 
			elseif(count($calendarConstraints)>0)
			{
				$constraints['calendar'] = $calendarConstraints[0];
			}
		}

		// datepicker filter
		$datepickerConstraints = [];
		if ($demand->getDateFrom()!="")
		{
			$dateFrom = (is_int($demand->getDateFrom()))?$demand->getDateFrom():strtotime($demand->getDateFrom());
			if($dateFrom>0)
			{
				$dateFrom = mktime(0,0,0,date("n",$dateFrom),date("j",$dateFrom),date("Y",$dateFrom));
				$datepickerConstraints[] = $query->greaterThanOrEqual($timeRestrictionField,$dateFrom);
			}
		}
		if ($demand->getDateTo()!="")
		{
			$dateTo = (is_int($demand->getDateTo()))?$demand->getDateTo():strtotime($demand->getDateTo());
			if($dateTo>0)
			{
				$dateTo = mktime(23,59,59,date("n",$dateTo),date("j",$dateTo),date("Y",$dateTo));
				$datepickerConstraints[] = $query->lessThanOrEqual($timeRestrictionField,$dateTo);
			}
		}
		if(count($datepickerConstraints)>1)
		{
			$constraints['datepicker'] = $query->logicalAnd($datepickerConstraints);
		} 
		elseif(count($datepickerConstraints)>0)
		{
			$constraints['datepicker'] = $datepickerConstraints[0];
		}

        // month & year OR year only
		if (is_numeric($demand->getYear()) && $demand->getYear() > 0) {
			if (is_null($demand->getDateField())) {
                throw new \InvalidArgumentException('No Datefield is set, therefore no Datemenu is possible!');
				$demand->setDateField("datetime");
            }
            if ($demand->getMonth() > 0) {
                if ($demand->getDay() > 0) {
                    $begin = mktime(0, 0, 0, $demand->getMonth(), $demand->getDay(), $demand->getYear());
                    $end = mktime(23, 59, 59, $demand->getMonth(), $demand->getDay(), $demand->getYear());
                } else {
                    $begin = mktime(0, 0, 0, $demand->getMonth(), 1, $demand->getYear());
                    $end = mktime(23, 59, 59, ($demand->getMonth() + 1), 0, $demand->getYear());
                }
            } else {
                $begin = mktime(0, 0, 0, 1, 1, $demand->getYear());
                $end = mktime(23, 59, 59, 12, 31, $demand->getYear());
            }
            $constraints['datetime'] = $query->logicalAnd(
                $query->greaterThanOrEqual($demand->getDateField(), $begin),
                $query->lessThanOrEqual($demand->getDateField(), $end)
            );
        }
		elseif ($demand->getYear() == 'undefined') {
			$constraints['datetime'] = $query->logicalOr(
				$query->equals($demand->getDateField(), ''),
				$query->equals($demand->getDateField(), 0),
				$query->equals($demand->getDateField(), NULL)
			);
		}
		
		// ToDo: check this
		/*
		if(is_array($GLOBALS['TCA']['tx_mooxnews_domain_model_news']['moox']['listViewSearchFields']))
		{
			foreach($GLOBALS['TCA']['tx_mooxnews_domain_model_news']['moox']['listViewSearchFields'] AS $extkey => $queryFields)
			{
				if($extkey!="moox_news")
				{
					foreach($queryFields AS $fieldname => $queryField)
					{
						if($fieldname!="default" && $queryField!="")
						{
							$fieldname = GeneralUtility::underscoredToLowerCamelCase($fieldname);
							$getCall = "get".ucfirst($fieldname);
							if(method_exists($demand, $getCall))
							{
								$fieldvalue = $demand->$getCall();
								if($fieldvalue!="")
								{
									$listViewSearchFields = explode(",",$queryField);									
									$queryConstraints = [];
									foreach($listViewSearchFields AS $listViewSearchField)
									{
										$queryConstraints[] = $query->like($listViewSearchField, "%".$fieldvalue."%");
									}									
									$constraints['query'] = $query->logicalOr($queryConstraints);
								}
							}
						}
					}
				}
			}
		}		
		*/
		
		// set constraints based on extended filter fields from external extensions
		if(is_array($GLOBALS['TCA']['tx_mooxnews_domain_model_news']['moox']['additionalFrontendDemandFields']))
		{
			foreach($GLOBALS['TCA']['tx_mooxnews_domain_model_news']['moox']['additionalFrontendDemandFields'] AS $fieldname)
			{	
				if(isset($GLOBALS['TCA']['tx_mooxnews_domain_model_news']['columns'][$fieldname]))
				{

					$field = $GLOBALS['TCA']['tx_mooxnews_domain_model_news']['columns'][$fieldname];
					$fieldnameUnderscored = $fieldname;
					$fieldname = GeneralUtility::underscoredToLowerCamelCase($fieldname);
					$getCall = "get".ucfirst($fieldname);				
					if(method_exists($demand, $getCall))
					{
						$fieldvalue = $demand->$getCall();

						if(trim($fieldvalue)!="" && trim($fieldvalue)!="all")
						{
							if(in_array($field['config']['type'],["input","text","check"]) && !in_array($field['moox']['filter']['type'],["select"]))
							{
								if(is_array($GLOBALS['TCA']['tx_mooxnews_domain_model_news']['moox']['additionalFrontendDemandFields']) && in_array($fieldnameUnderscored,$GLOBALS['TCA']['tx_mooxnews_domain_model_news']['moox']['additionalFrontendDemandFields']))
								{
									if(is_array($field['moox']['frontendDemand']) && in_array($field['moox']['frontendDemand']['operator'],["equals","like","startsWith","endsWith","isset"]))
									{
										if($field['moox']['frontendDemand']['operator']=="equals")
										{
											$constraints[$fieldname] = $query->equals($fieldname, $fieldvalue);
										} 
										elseif($field['moox']['frontendDemand']['operator']=="like")
										{
											$constraints[$fieldname] = $query->like($fieldname, '%' . $fieldvalue . '%');
										} 
										elseif($field['moox']['frontendDemand']['operator']=="startsWith")
										{
											$constraints[$fieldname] = $query->like($fieldname, $fieldvalue . '%');
										} 
										elseif($field['moox']['frontendDemand']['operator']=="endsWith")
										{
											$constraints[$fieldname] = $query->like($fieldname, '%' . $fieldvalue);
										}
										elseif($field['moox']['frontendDemand']['operator']=="isset" && $fieldvalue==1)
										{
											$constraints[$fieldname] = $query->equals($fieldname, 1);
										}
									}
									else
									{
										$constraints[$fieldname] = $query->like($fieldname, '%' . $fieldvalue . '%');
									}
								}
							} 
							else 
							{
								if(in_array($field['moox']['filter']['type'],["select"]) && $fieldvalue=="empty")
								{
									$constraints[$fieldname] = $query->logicalOr(
										$query->equals($fieldname, ""),
										$query->equals($fieldname, NULL)
									);
								} 
								else 
								{
									$constraints[$fieldname] = $query->equals($fieldname, $fieldvalue);
								}
							}
						}
					}
				}							
			}
		}
		
		// sys language
		if (TYPO3_MODE == 'BE')
		{
			$query->getQuerySettings()->setRespectSysLanguage(false);
			$constraints[] = $query->equals('t3_origuid', 0);
			if (is_numeric($demand->getSysLanguageUid()))
			{
				$constraints['language'] = $query->equals('sys_language_uid', $demand->getSysLanguageUid());			
			}
		}

		if(is_array($demand->getTags()) && count($demand->getTags()))
		{
			$tags = $demand->getTags();
			if(count($tags)>1)
			{
				$tagConstraints = [];
				foreach($tags AS $tag)
				{
					$tagConstraints[] = $query->contains('tags', $tag);
				}
				$constraints['tags'] = $query->logicalOr($tagConstraints);	
			} 
			else
			{
				$constraints['tags'] = $query->contains('tags', $tags[0]);		
			}
		} 
		elseif($demand->getTag()!="" && $demand->getTag()!=0)
		{
			$constraints['tags'] = $query->contains('tags', $demand->getTag());	
		}		
		
		// Targets
		if(is_array($demand->getTargets()) && count($demand->getTargets()))
		{
			$targets = $demand->getTargets();
			if(count($targets)>1)
			{
				$targetConstraints = [];
				foreach($targets AS $target)
				{
					$targetConstraints[] = $query->contains('targets', $target);
				}
				$constraints['targets'] = $query->logicalOr($targetConstraints);	
			} 
			else 
			{
				$constraints['targets'] = $query->contains('targets', $targets[0]);		
			}
		} 
		elseif($demand->getTarget()!="" &&  $demand->getTarget()!=0)
		{
			$constraints['targets'] = $query->contains('targets', $demand->getTarget());	
		}

        // Search
        $searchConstraints = $this->getSearchConstraints($query, $demand);
        if (!empty($searchConstraints))
		{
            $constraints['search'] = $query->logicalAnd($searchConstraints);
        }

        // Exclude already displayed
        if ($demand->getExcludeAlreadyDisplayedNews() && isset($GLOBALS['EXT']['moox_news']['alreadyDisplayed']) && !empty($GLOBALS['EXT']['moox_news']['alreadyDisplayed'])) {
            $constraints['excludeAlreadyDisplayedNews'] = $query->logicalNot(
                $query->in(
                    'uid',
                    $GLOBALS['EXT']['moox_news']['alreadyDisplayed']
                )
            );
        }

        // Hide id list
        $hideIdList = $demand->getHideIdList();
        if ($hideIdList) {
            $constraints['excludeAlreadyDisplayedNews'] = $query->logicalNot(
                $query->in(
                    'uid',
                    GeneralUtility::intExplode(',', $hideIdList)
                )
            );
        }

        // Clean not used constraints
        foreach ($constraints as $key => $value) {
            if (is_null($value)) {
                unset($constraints[$key]);
            }
        }

        return $constraints;
    }

    /**
     * Returns an array of orderings created from a given demand object.
     *
     * @param DemandInterface $demand
     * @return array<\TYPO3\CMS\Extbase\Persistence\Generic\Qom\ConstraintInterface>
     */
    protected function createOrderingsFromDemand(DemandInterface $demand)
    {
        $orderings = [];
        if ($demand->getTopNewsFirst()) {
            $orderings['istopnews'] = QueryInterface::ORDER_DESCENDING;
        }

        if (Validation::isValidOrdering($demand->getOrder(), $demand->getOrderByAllowed())) {
            $orderList = GeneralUtility::trimExplode(',', $demand->getOrder(), true);

            if (!empty($orderList)) {
                // go through every order statement
                foreach ($orderList as $orderItem) {
                    list($orderField, $ascDesc) = GeneralUtility::trimExplode(' ', $orderItem, true);
                    // count == 1 means that no direction is given
                    if ($ascDesc) {
                        $orderings[$orderField] = ((strtolower($ascDesc) == 'desc') ?
                            QueryInterface::ORDER_DESCENDING :
                            QueryInterface::ORDER_ASCENDING);
                    } else {
                        $orderings[$orderField] = QueryInterface::ORDER_ASCENDING;
                    }
					$orderFieldUnderscored = GeneralUtility::camelCaseToLowerCaseUnderscored($orderField);
					if(is_array($GLOBALS['TCA']['tx_mooxnews_domain_model_news']['moox']['additionalFrontendSortingFields']) && in_array($orderFieldUnderscored,$GLOBALS['TCA']['tx_mooxnews_domain_model_news']['moox']['additionalFrontendSortingFields']))
					{
						$additionalSorting = $GLOBALS['TCA']['tx_mooxnews_domain_model_news']['columns'][$orderFieldUnderscored]['moox']['additionalSorting'];
						if($additionalSorting!='')
						{
							list($orderField, $ascDesc) = GeneralUtility::trimExplode(' ', $additionalSorting, TRUE);
							// count == 1 means that no direction is given
							if(!isset($orderings[$orderField]))
							{
								if ($ascDesc)
								{
									$orderings[$orderField] = ((strtolower($ascDesc) == 'desc') ?
										QueryInterface::ORDER_DESCENDING :
										QueryInterface::ORDER_ASCENDING);
								} 
								else
								{
									$orderings[$orderField] = QueryInterface::ORDER_ASCENDING;
								}
							}
						}
					}
                }
				if(!isset($orderings['title']))
				{
					$orderings['title'] = QueryInterface::ORDER_ASCENDING;
				}
            }
        }

        return $orderings;
    }

    /**
     * Find first news by import and source id
     *
     * @param string $importSource import source
     * @param int $importId import id
     * @return \Neulandlotsen\MooxNews\Domain\Model\News
     */
    public function findOneByImportSourceAndImportId($importSource, $importId)
    {
        $query = $this->createQuery();
        $query->getQuerySettings()->setRespectStoragePage(false);
        $query->getQuerySettings()->setRespectSysLanguage(false);
        $query->getQuerySettings()->setIgnoreEnableFields(true);

        return $query->matching(
            $query->logicalAnd(
                $query->equals('importSource', $importSource),
                $query->equals('importId', $importId)
            ))->execute()->getFirst();
    }
	
	/**
	 * Find first news by grabber remote uid
	 *
	 * @param string $grabberRemoteUid
	 * @return Neulandlotsen\MooxNews\Domain\Model\News
	 */
	public function findOneByGrabberRemoteUid($grabberRemoteUid)
	{
		$query = $this->createQuery();
		$query->getQuerySettings()->setRespectStoragePage(FALSE);
		$query->getQuerySettings()->setRespectSysLanguage(FALSE);
		$query->getQuerySettings()->setIgnoreEnableFields(TRUE);

		return $query->matching(
			$query->equals('grabberRemoteUid', $grabberRemoteUid)
		)->execute()->getFirst();
	}
	
	/**
	 * Find news that are not indexed yet or index is not up-to-date 
	 *	
	 * @param array $pids
	 * @param integer $limit
	 * @return Neulandlotsen\MooxNews\Domain\Model\News
	 */
	public function findNewsToIndex($pids = [],$limit = 0)
	{
		
		if($pids)
		{
			$pids = (is_array($pids))?$pids:[$pids];
			$pidStmt = "pid IN (".implode(",",$pids).") AND ";
		} 
		
		$preselect['fields'] = 'uid';
		$preselect['table']	= 'tx_mooxnews_domain_model_news';
		$preselect['where'] = $pidStmt.'deleted=0 AND indexed<tstamp';
	
		$newsUids = $GLOBALS['TYPO3_DB']->exec_SELECTgetRows(	
			$preselect['fields'], 
			$preselect['table'], 
			$preselect['where'],
			'',
			'',
			($limit>0)?$limit:''
		);
		
		$uids = [];
		foreach ($newsUids as $newsUid)
		{
			$uids[] = $newsUid['uid'];
		}
		
		$result = [];
		
		if(count($uids)>0)
		{
			$query = $this->createQuery();		
			$query->getQuerySettings()->setRespectStoragePage(FALSE);
			$query->getQuerySettings()->setRespectSysLanguage(FALSE);
			$query->getQuerySettings()->setIgnoreEnableFields(TRUE);		
			$query->matching(
				$query->in('uid', $uids)
			);				
			$result = $query->execute();
		}
		
		return $result;		
	}
	
	/**
	 * Find all news  
	 *	
	 * @param array $pids
	 * @return Neulandlotsen\MooxNews\Domain\Model\News
	 */
	public function findAllNewsToIndex($pids = [])
	{
		
		$query 		= $this->createQuery();
		
		if($pids)
		{
			$pids = (is_array($pids))?$pids:[$pids];
			$query->getQuerySettings()->setRespectStoragePage(TRUE);
			$query->getQuerySettings()->setStoragePageIds($pids);
			$pidStmt = "pid IN (".implode(",",$pids).") AND ";
		} 
		else 
		{
			$query->getQuerySettings()->setRespectStoragePage(FALSE);
		}
		
		$preselect['fields'] = 'uid';
		$preselect['table']	= 'tx_mooxnews_domain_model_news';
		$preselect['where']	= $pidStmt.'deleted=0 AND indexed<tstamp';
	
		$newsUids = $GLOBALS['TYPO3_DB']->exec_SELECTgetRows(	
			$preselect['fields'], 
			$preselect['table'], 
			$preselect['where']
		);
		
		$uids = [];
		foreach ($newsUids as $newsUid)
		{
			$uids[] = $newsUid['uid'];
		}
		
		$query->getQuerySettings()->setRespectSysLanguage(FALSE);
		$query->getQuerySettings()->setIgnoreEnableFields(TRUE);
		
		$query->matching($query->in('uid', $uids));
				
		$result = $query->execute();
		
		return $result;		
	}
	
	/**
	 * Find news by mailer frequency and pid(list)
	 *	
	 * @param array $pids
	 * @param integer $frequenzy
	 * @return Neulandlotsen\MooxNews\Domain\Model\News
	 */
	public function findNewsByMailerFrequenzy($pids = [],$frequency = 0)
	{
		
		if($frequency<1)
		{
			$frequency = 99;
		}
		
		$pids = (is_array($pids))?$pids:[$pids];

		$query 	= $this->createQuery();	
		$query->getQuerySettings()->setRespectStoragePage(FALSE);
		$query->getQuerySettings()->setRespectSysLanguage(FALSE);
		$query->getQuerySettings()->setIgnoreEnableFields(FALSE);
		
		$selectStmt = 'SELECT * FROM tx_mooxnews_domain_model_news WHERE pid IN ('.implode(",",$pids).') AND deleted=0 AND mailer_frequency='.$frequency;
		
		$query->statement($selectStmt);
		
		return $query->execute();		
	}
	
	/**
	 * Find news by mailer frequency and pid(list) and categories
	 *	
	 * @param array $pids
	 * @param array $categories
	 * @param integer $frequenzy
	 * @return Neulandlotsen\MooxNews\Domain\Model\News
	 */
	public function findNewsByMailerFrequenzyAndCategories($pids = [],$categories = [],$frequency = 0)
	{
		
		if($frequency<1)
		{
			$frequency = 99;
		}
		
		$pids = (is_array($pids))?$pids:[$pids];		
		$categories = (is_array($categories))?$categories:[$categories];
		
		$query = $this->createQuery();
		
		$constraints = [];
		foreach($categories AS $category)
		{
			$constraints[] = $query->contains('categories', $category);
		}
		
		$query->getQuerySettings()->setStoragePageIds($pids);
		if(count($constraints))
		{
			$query->matching(
				$query->logicalAnd(
					$query->equals('mailer_frequency', $frequency),
					$query->logicalOr($constraints)
				)
			);
		} else {
			$query->matching(
				$query->equals('mailer_frequency', $frequency)
			);
		}
		return $query->execute();				
	}
	
	/**
	 * Find news by filter
	 *	
	 * @param array $filter	
	 * @return Neulandlotsen\MooxNews\Domain\Model\News
	 */
	public function findNewsByFilter($filter = [])
	{
		
		$constraints = [];
		
		if($filter['mailerFrequenzy']<1)
		{
			$filter['mailerFrequenzy'] = 99;
		}
		
		$filter['newsPids'] = (is_array($filter['newsPids']))?$filter['newsPids']:[$filter['newsPids']];		
		if(isset($filter['categoriesUids'])){
			$filter['categoriesUids'] = (is_array($filter['categoriesUids']))?$filter['categoriesUids']:[$filter['categoriesUids']];
		} else {
			$filter['categoriesUids'] = [];
		}
		
		$query = $this->createQuery();
		
		$query->getQuerySettings()->setStoragePageIds($filter['newsPids']);
		
		if($filter['mailer'])
		{
			$constraints[] = $query->equals('mailer_frequency', $filter['mailerFrequenzy']);
		}
		
		if($filter['sendRestrictedNewsOnly'])
		{
			$constraints[] = $query->logicalNot($query->equals('feGroup', ""));
		} 
		
		if($filter['rss'])
		{
			$constraints[] = $query->logicalNot($query->equals('excludeFromRss', 1));			
		}
		
		if($filter['twitter'])
		{
			$constraints[] = $query->equals('pushToTwitter', 1);
			$constraints[] = $query->logicalNot($query->equals('pushToTwitterText', ""));
		} 
		
		if(count($filter['categoriesUids'])){
			$categoryConstraints = [];
			foreach($filter['categoriesUids'] AS $category)
			{
				$categoryConstraints[] = $query->contains('categories', $category);
			}
			if(count($categoryConstraints))
			{
				$constraints[] = $query->logicalOr($categoryConstraints);
			}
		}
		
		$query->setOrderings([
			'datetime' => QueryInterface::ORDER_ASCENDING,
			'title' => QueryInterface::ORDER_ASCENDING
		]);
		
		return $query->matching(
			$query->logicalAnd($constraints)
		)->execute();				
	}
	
	/**
	 * Find news by pid(list)
	 *	
	 * @param array $pids	
	 * @return Neulandlotsen\MooxNews\Domain\Model\News
	 */
	public function findNewsByPidList($pids = [])
	{
		
		$pids = (is_array($pids))?$pids:[$pids];		
		
		$query = $this->createQuery();
		
		$query->getQuerySettings()->setStoragePageIds($pids);
		
		return $query->execute();
				
	}
	
	/**
	 * all years in frontend news dates
	 *	
	 * @param array $storagePage
	 * @return array
	 */
	public function findAllYears($storagePage = 0)
	{
		
		if(!is_array($storagePage))
		{
			if(!$storagePage)
            {
                $storagePage = 0;
            }
		    $storagePage =  [$storagePage];
		}
		
		$query = $this->createQuery();
		
		$query->getQuerySettings()->setRespectStoragePage(FALSE);		
		$storagePageIdsStmt = " AND pid IN (".implode(",",$storagePage).")";
		$query->getQuerySettings()->setRespectSysLanguage(FALSE);
		$sysLanguageUid = $query->getQuerySettings()->getLanguageUid();
		if(is_numeric($sysLanguageUid))
		{
			$sysLanguageUidStmt = " AND sys_language_uid=".$sysLanguageUid;
		}		
		$query->getQuerySettings()->setIgnoreEnableFields(TRUE);
		$query->statement('SELECT DISTINCT FROM_UNIXTIME(`datetime`, "%Y") AS "year" FROM `tx_mooxnews_domain_model_news` WHERE deleted=0'.$storagePageIdsStmt.$sysLanguageUidStmt.' AND hidden=0 ORDER BY `datetime` DESC');
		$result = $query->execute(true);
		return $result;		
	}
	
	/**
	 * all years in all news dates
	 *	
	 * @param boolean $respectEnableFields if set to false, hidden records are shown	
	 * @return array
	 */
	public function findAllBackendYears($respectEnableFields = TRUE)
	{
				
		$query = $this->createQuery();
		
		$query->getQuerySettings()->setRespectStoragePage(FALSE);
		$query->getQuerySettings()->setRespectSysLanguage(FALSE);
		$query->getQuerySettings()->setIgnoreEnableFields(!$respectEnableFields);
		$query->statement('SELECT DISTINCT FROM_UNIXTIME(`datetime`, "%Y") AS "year" FROM `tx_mooxnews_domain_model_news` WHERE deleted=0 ORDER BY `datetime` DESC');
		
		return $query->execute(TRUE);
				
	}
	
	/**
	 * find extended filter items
	 *	
	 * @param array $field
	 * @param array $storagePage
	 * @param string $table
	 * @param string $where
	 * @param array $dependFields
	 * @return array
	 */
	public function findExtendedFilterItems($field,$storagePage,$table,$where = "",$dependFields = [])
	{		
		$result = [];
		
		if($table!="")
		{
			if(!is_array($storagePage))
			{
				$storagePage = [$storagePage];
			}
			
			$existingFilterValues = [];
			if(count($dependFields))
			{
				$existingFilterValues = $this->findExtendedFilterUniqueValues($field,$storagePage,$dependFields);
				
				if(count($existingFilterValues))
				{
					$existingFilterValuesStatic = [];
					$existingFilterValuesDb = [];
					
					$field = $GLOBALS['TCA']['tx_mooxnews_domain_model_news']['columns'][$field];
					$items = $field['config']['items'];
					
					foreach($existingFilterValues AS $existingFilterValue)
					{
						$inStatic = false;
						foreach($items AS $item)
						{
							if($item[1]==""){
								$item[1] = "empty";
								if($field['moox']['filter']['labels']['select_empty']!=="")
								{
									$item[0] = $field['moox']['filter']['labels']['select_empty'];
								}
							}
							if($existingFilterValue == $item[1])
							{
								$existingFilterValuesStatic[] = ["uid" => $item[1], "title" => LocalizationUtility::translate($item[0], $field['moox']['extkey'])];
								$inStatic = true;
							}							
						}
						if(!$inStatic && is_numeric($existingFilterValue))
						{
							$existingFilterValuesDb[] = $existingFilterValue;
						}
					}
					
					if(count($existingFilterValuesDb))
					{
						$dependStmt = " AND uid IN (".implode(",",$existingFilterValuesDb).")";
					}
				}
			}			
			
			$query = $this->createQuery();
			
			$query->getQuerySettings()->setRespectStoragePage(FALSE);		
			$storagePageIdsStmt = " AND pid IN (".implode(",",$storagePage).")";
			$query->getQuerySettings()->setRespectSysLanguage(TRUE);
			$query->getQuerySettings()->setIgnoreEnableFields(FALSE);
			$query->statement('SELECT uid,title FROM `'.$table.'` WHERE 1=1'.$storagePageIdsStmt.$dependStmt.' '.$where);
			$resultDb = $query->execute(TRUE);
			
			if(is_array($existingFilterValuesStatic))
			{
				$result = $existingFilterValuesStatic;
			} 
			foreach($resultDb AS $item)
			{
				$result[] = ["uid" => $item['uid'], "title" => $item['title']];
			}
		}
		
		return $result;			
	}
	
	/**
	 * find extended filter unique values
	 *	
	 * @param array $field
	 * @param array $storagePage
	 * @param array $dependFields
	 * @return array
	 */
	public function findExtendedFilterUniqueValues($field,$storagePage,$dependFields = [])
	{
		
		$values = [];
		
		if($field!="")
		{
			if(!is_array($storagePage))
			{
				$storagePage = [$storagePage];
			}
			
			$where = "	AND starttime <= UNIX_TIMESTAMP() 
						AND (
							endtime >= UNIX_TIMESTAMP() 
							OR endtime=0
						) 
						AND deleted = 0 
						AND hidden = 0	";
			
			$depend = "";
			if(count($dependFields))
			{
				foreach($dependFields AS $dependField => $dependFieldValue)
				{
					if($dependFieldValue!="all")
					{
						if($dependFieldValue=="empty")
						{
							$depend .= ' AND ('.$dependField.'="" OR '.$dependField.' IS NULL)';
						} 
						else
						{
							$depend .= ' AND '.$dependField.'="'.$dependFieldValue.'"';
						}
					}
				}
			}
					
			$ordering = " ORDER BY ".$field." ASC";
			
			$query = $this->createQuery();
			
			$query->getQuerySettings()->setRespectStoragePage(FALSE);		
			$storagePageIdsStmt = " AND pid IN (".implode(",",$storagePage).")";
			$query->getQuerySettings()->setRespectSysLanguage(TRUE);
			$query->getQuerySettings()->setIgnoreEnableFields(FALSE);
			
			$stmt = 'SELECT DISTINCT '.$field.' FROM `tx_mooxnews_domain_model_news` WHERE 1=1'.$storagePageIdsStmt.' '.$where.$depend.$ordering;
			
			$query->statement($stmt);
			$result = $query->execute(TRUE);
			
			foreach($result AS $value)
			{
				if($value[$field]=="" || $value[$field]==NULL)
				{
					if(!in_array("empty",$values))
					{
						$values[] = "empty";
					}
				} 
				else
				{
					$values[] = $value[$field];
				}
			}			
		}
		
		return $values;			
	}
	
	/**
	 * all sys language uids
	 *	
	 * @param boolean $respectEnableFields if set to false, hidden records are shown	
	 * @return array
	 */
	public function findAllSysLanguageUids($respectEnableFields = TRUE) {
		
		$query = $this->createQuery();
		
		if(count($query->getQuerySettings()->getStoragePageIds()))
		{
			$pidStmt = "WHERE pid IN (".implode(",",$query->getQuerySettings()->getStoragePageIds()).") ";
		}
		
		$query->getQuerySettings()->setRespectStoragePage(TRUE);
		$query->getQuerySettings()->setRespectSysLanguage(FALSE);
		$query->getQuerySettings()->setIgnoreEnableFields(!$respectEnableFields);
		$query->statement('SELECT DISTINCT `sys_language_uid` FROM `tx_mooxnews_domain_model_news` '.$pidStmt.'ORDER BY `sys_language_uid` ASC');
		$result = $query->execute(TRUE);
		return $result;
				
	}
	
	/**
	 * get max sort
	 *	
	 * @param int $pid	
	 * @return array
	 */
	public function findMaxSortingByPid($pid)
	{
		
		$query = $this->createQuery();
		
		$query->getQuerySettings()->setRespectStoragePage(TRUE);
		$query->getQuerySettings()->setRespectSysLanguage(FALSE);
		$query->getQuerySettings()->setIgnoreEnableFields(TRUE);
		$query->statement('SELECT sorting FROM tx_mooxnews_domain_model_news WHERE pid='.$pid.' AND deleted=0 ORDER BY sorting DESC LIMIT 1');
		$result = $query->execute(TRUE);		
		return (int)$result[0]['sorting'];
				
	}

    /**
     * Override default findByUid function to enable also the option to turn of
     * the enableField setting
     *
     * @param int $uid id of record
     * @param bool $respectEnableFields if set to false, hidden records are shown
     * @return \Neulandlotsen\MooxNews\Domain\Model\News
     */
    public function findByUid($uid, $respectEnableFields = true)
    {
        $query = $this->createQuery();
        $query->getQuerySettings()->setRespectStoragePage(false);
        $query->getQuerySettings()->setRespectSysLanguage(false);
        $query->getQuerySettings()->setIgnoreEnableFields(!$respectEnableFields);

        return $query->matching(
            $query->logicalAnd(
                $query->equals('uid', $uid),
                $query->equals('deleted', 0)
            ))->execute()->getFirst();
    }
	
	/**
     * find news by type
     *
     * @param string $type type of record
     * @param bool $respectEnableFields if set to false, hidden records are shown
     * @return \Neulandlotsen\MooxNews\Domain\Model\News
     */
    public function findByType($type = "moox_news", $respectEnableFields = true)
    {
        $query = $this->createQuery();
        $query->getQuerySettings()->setRespectStoragePage(false);
        $query->getQuerySettings()->setRespectSysLanguage(false);
        $query->getQuerySettings()->setIgnoreEnableFields(!$respectEnableFields);

        return $query->matching(
            $query->logicalAnd(
                $query->equals('type', $type),
                $query->equals('deleted', 0)
            )
		)->execute();
    }

    /**
     * Get the count of news records by month/year and
     * returns the result compiled as array
     *
     * @param DemandInterface $demand
     * @return array
     */
    public function countByDate(DemandInterface $demand)
    {
        $data = [];
        $sql = $this->findDemandedRaw($demand);

        // Get the month/year into the result
        $field = $demand->getDateField();
        $field = empty($field) ? 'datetime' : $field;

        $sql = 'SELECT FROM_UNIXTIME(' . $field . ', "%m") AS "_Month",' .
            ' FROM_UNIXTIME(' . $field . ', "%Y") AS "_Year" ,' .
            ' count(FROM_UNIXTIME(' . $field . ', "%m")) as count_month,' .
            ' count(FROM_UNIXTIME(' . $field . ', "%y")) as count_year' .
            ' FROM tx_mooxnews_domain_model_news ' . substr($sql, strpos($sql, 'WHERE '));

        if (TYPO3_MODE === 'FE') {
            $sql .= $GLOBALS['TSFE']->sys_page->enableFields('tx_mooxnews_domain_model_news');
        } else {
            $sql .= \TYPO3\CMS\Backend\Utility\BackendUtility::BEenableFields('tx_mooxnews_domain_model_news') .
                \TYPO3\CMS\Backend\Utility\BackendUtility::deleteClause('tx_mooxnews_domain_model_news');
        }

        // strip unwanted order by
        $sql = $GLOBALS['TYPO3_DB']->stripOrderBy($sql);

        // group by custom month/year fields
        $orderDirection = strtolower($demand->getOrder());
        if ($orderDirection !== 'desc' && $orderDirection != 'asc') {
            $orderDirection = 'asc';
        }
        $sql .= ' GROUP BY _Month, _Year ORDER BY _Year ' . $orderDirection . ', _Month ' . $orderDirection;

        $res = $GLOBALS['TYPO3_DB']->sql_query($sql);
        while ($row = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($res)) {
            $data['single'][$row['_Year']][$row['_Month']] = $row['count_month'];
        }
        $GLOBALS['TYPO3_DB']->sql_free_result($res);

        // Add totals
        if (is_array($data['single'])) {
            foreach ($data['single'] as $year => $months) {
                $countOfYear = 0;
                foreach ($months as $month) {
                    $countOfYear += $month;
                }
                $data['total'][$year] = $countOfYear;
            }
        }

        return $data;
    }
	
	/**
	 * Find news translations
	 *	
	 * @param int $l10nParent l10n parent
	 * @return Neulandlotsen\MooxNews\Domain\Model\News
	 */
	public function findTranslations($l10nParent)
	{
		
		$news = [];

        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable('tx_mooxnews_domain_model_news');
        $queryBuilder
            ->getRestrictions()
            ->removeAll()
            ->add(GeneralUtility::makeInstance(DeletedRestriction::class));

        $query = $queryBuilder
            ->select('*')
            ->from('tx_mooxnews_domain_model_news')
            ->where(
                $queryBuilder->expr()->eq('l10n_parent', $queryBuilder->createNamedParameter($l10nParent, \PDO::PARAM_INT))
            )
            ->orderBy('sys_language_uid', 'ASC')
            ->execute();

		while($newsItem = $query->fetch())
		{
            $news[] = $newsItem;
        }
		
		return $news;
	}

    /**
     * Get the search constraints
     *
     * @param QueryInterface $query
     * @param DemandInterface $demand
     * @return array
     * @throws \UnexpectedValueException
     */
    protected function getSearchConstraints(QueryInterface $query, DemandInterface $demand)
    {
        $constraints = [];
        if ($demand->getSearch() === null) {
            return $constraints;
        }

        /* @var $searchObject \Neulandlotsen\MooxNews\Domain\Model\Dto\Search */
        $searchObject = $demand->getSearch();

        $searchSubject = $searchObject->getSubject();
        if (!empty($searchSubject)) {
            $searchFields = GeneralUtility::trimExplode(',', $searchObject->getFields(), true);
            $searchConstraints = [];

            if (count($searchFields) === 0) {
                throw new \UnexpectedValueException('No search fields defined', 1318497755);
            }

            foreach ($searchFields as $field) {
                if (!empty($searchSubject)) {
                    $searchConstraints[] = $query->like($field, '%' . $searchSubject . '%');
                }
            }

            if (count($searchConstraints)) {
                $constraints[] = $query->logicalOr($searchConstraints);
            }
        }

        $minimumDate = strtotime($searchObject->getMinimumDate());
        if ($minimumDate) {
            $field = $searchObject->getDateField();
            if (empty($field)) {
                throw new \UnexpectedValueException('No date field is defined', 1396348732);
            }
            $constraints[] = $query->greaterThanOrEqual($field, $minimumDate);
        }
        $maximumDate = strtotime($searchObject->getMaximumDate());
        if ($maximumDate) {
            $field = $searchObject->getDateField();
            if (empty($field)) {
                throw new \UnexpectedValueException('No date field is defined', 1396348733);
            }
            $constraints[] = $query->lessThanOrEqual($field, $maximumDate);
        }

        return $constraints;
    }
}
