<?php
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile(
	'moox_core',
	'Configuration/TypoScript/Default',
	'MOOX Core (Default)'
);
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile(
	'moox_core',
	'Configuration/TypoScript/Advanced',
	'MOOX Core (Advanced)'
);
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile(
	'moox_core',
	'Configuration/TypoScript/Minimal',
	'MOOX Core (Minimal | type=99)'
);
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile(
	'moox_core',
	'Configuration/TypoScript/InfiniteScrolling',
	'MOOX Core (Infinite Scrolling)'
);
