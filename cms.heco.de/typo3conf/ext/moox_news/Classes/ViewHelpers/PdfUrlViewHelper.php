<?php

namespace Neulandlotsen\MooxNews\ViewHelpers;

/**
 * This file is part of the "moox_news" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */
 
use Neulandlotsen\MooxNews\Domain\Model\News;
use TYPO3\CMS\Core\Utility\GeneralUtility;

class PdfUrlViewHelper extends \TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper
{

    /**
     * @var string
     */
    protected $tagName = 'a';

	/**
	 * @var \TYPO3\CMS\Extbase\Mvc\Web\Routing\UriBuilder
	 */
	protected $uriBuilder;

    /**
     * @var \Neulandlotsen\MooxCore\Utility\HelperUtility
     */
    protected $helperUtility;

	/**
	 * @param \TYPO3\CMS\Extbase\Mvc\Web\Routing\UriBuilder
	 */
	public function injectUriBuilder(\TYPO3\CMS\Extbase\Mvc\Web\Routing\UriBuilder $uriBuilder)
	{
		$this->uriBuilder = $uriBuilder;
	}

    /**
     * @param \Neulandlotsen\MooxCore\Utility\HelperUtility
     */
    public function injectHelperService(\Neulandlotsen\MooxCore\Utility\HelperUtility $helperUtility)
    {
        $this->helperUtility = $helperUtility;
    }

    public function initializeArguments()
    {
        parent::initializeArguments();
        $this->registerArgument('newsItem', News::class, 'news item', true);
		$this->registerArgument('settings', 'array', 'Settings', false, []);           
    }

    /**
     * @return string previewUrl
     */
    public function render()
    {
        $pdfUrl = '';

        $newsItem = $this->arguments['newsItem'];
		$settings = $this->arguments['settings'];

		if(is_object($newsItem))
		{
			$this->uriBuilder->setTargetPageUid($previewPid);
			$this->uriBuilder->setTargetPageType(66667);
			$this->uriBuilder->uriFor(
				'pdf',
				[
					'news' => $newsItem->getUid(),
					'hash' => $newsItem->getHash()
				],
				'News',
				'MooxNews',
				'Pi2'
			);
			$pdfUrl = $this->uriBuilder->build();
		}
        
        return $pdfUrl;
        
    }
}
