<?php
namespace Neulandlotsen\MooxMailer\Tasks;

/**
 * This file is part of the "moox_mailer" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

use Neulandlotsen\MooxMailer\Service\MailerService;
use Neulandlotsen\MooxMailer\Service\MooxNewsMailerService;
use Neulandlotsen\MooxMailer\Utility\SchedulerHelper;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Core\Messaging\FlashMessage;
use TYPO3\CMS\Core\Messaging\FlashMessageQueue;
use TYPO3\CMS\Extbase\Utility\LocalizationUtility;
use TYPO3\CMS\Extbase\Persistence\QueryInterface;
use TYPO3\CMS\Scheduler\AbstractAdditionalFieldProvider;
use TYPO3\CMS\Scheduler\Task\Enumeration\Action;

//require_once \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('moox_mailer','Classes/Service/MailerService.php'); 
//require_once \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('moox_mailer','Classes/Service/MooxNewsMailerService.php'); 
 
class MooxNewsSendTaskAdditionalFieldProvider extends AbstractAdditionalFieldProvider
{
	/**
	 * Path to the locallang file
	 * @var string
	 */
	const LLPATH = 'LLL:EXT:moox_mailer/Resources/Private/Language/locallang_scheduler.xlf:';
	
	/**
	 * This method is used to define new fields for adding or editing a task
	 * In this case, it adds an pid field
	 *
	 * @param array $taskInfo Reference to the array containing the info used in the add/edit form
	 * @param object $task When editing, reference to the current task object. Null when adding.
	 * @param \TYPO3\CMS\Scheduler\Controller\SchedulerModuleController $parentObject Reference to the calling object (Scheduler's BE module)
	 * @return array	Array containing all the information pertaining to the additional fields
	 */
	public function getAdditionalFields(array &$taskInfo, $task, \TYPO3\CMS\Scheduler\Controller\SchedulerModuleController $parentObject)
	{
        $currentSchedulerModuleAction = $parentObject->getCurrentAction();

	    // Get the extensions's configuration
		$extConf = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Configuration\ExtensionConfiguration::class)->get('moox_mailer');
		
		// Initialize extra fields
		if (empty($taskInfo['newsPids']))
		{
			if ($currentSchedulerModuleAction->equals(Action::EDIT))
			{
				// In case of edit, set to internal value if no data was submitted already
				$taskInfo['newsPids'] = $task->newsPids;
			} 
			else
			{
				// Otherwise set an empty value, as it will not be used anyway
				$taskInfo['newsPids'] = '';
			}
		}
		
		if (empty($taskInfo['userPids']))
		{
			if ($currentSchedulerModuleAction->equals(Action::EDIT))
			{
				// In case of edit, set to internal value if no data was submitted already
				$taskInfo['userPids'] = $task->userPids;
			} 
			else 
			{
				// Otherwise set an empty value, as it will not be used anyway
				$taskInfo['userPids'] = '';
			}
		}

		if (empty($taskInfo['excludedUserGroups']))
		{
			if ($currentSchedulerModuleAction->equals(Action::EDIT))
			{
				// In case of edit, set to internal value if no data was submitted already
				$taskInfo['excludedUserGroups'] = $task->excludedUserGroups;
			}
			else
			{
				// Otherwise set an empty value, as it will not be used anyway
				$taskInfo['excludedUserGroups'] = '';
			}
		}

		if(ExtensionManagementUtility::isLoaded('moox_address'))
		{
			if (empty($taskInfo['addressesPids'])) 
			{
				if ($currentSchedulerModuleAction->equals(Action::EDIT))
				{
					// In case of edit, set to internal value if no data was submitted already
					$taskInfo['addressesPids'] = $task->addressesPids;
				} 
				else 
				{
					// Otherwise set an empty value, as it will not be used anyway
					$taskInfo['addressesPids'] = '';
				}
			}

			if (empty($taskInfo['addressesCategories']))
			{
				if ($currentSchedulerModuleAction->equals(Action::EDIT))
				{
					// In case of edit, set to internal value if no data was submitted already
					$taskInfo['addressesCategories'] = $task->addressesCategories;
				}
				else
				{
					// Otherwise set an empty value, as it will not be used anyway
					$taskInfo['addressesCategories'] = '';
				}
			}
		}

		if (empty($taskInfo['includedNewsCategoriesUids'])) 
		{
			if ($currentSchedulerModuleAction->equals(Action::EDIT))
			{
				// In case of edit, set to internal value if no data was submitted already
				$taskInfo['includedNewsCategoriesUids'] = $task->includedNewsCategoriesUids;
			} 
			else 
			{
				// Otherwise set an empty value, as it will not be used anyway
				$taskInfo['includedNewsCategoriesUids'] = '';
			}
		}
		
		if (empty($taskInfo['excludedNewsCategoriesUids'])) 
		{
			if ($currentSchedulerModuleAction->equals(Action::EDIT))
			{
				// In case of edit, set to internal value if no data was submitted already
				$taskInfo['excludedNewsCategoriesUids'] = $task->excludedNewsCategoriesUids;
			} 
			else 
			{
				// Otherwise set an empty value, as it will not be used anyway
				$taskInfo['excludedNewsCategoriesUids'] = '';
			}
		}
		
		if (empty($taskInfo['sendSingleMails'])) 
		{
			if ($currentSchedulerModuleAction->equals(Action::EDIT))
			{
				// In case of edit, set to internal value if no data was submitted already
				$taskInfo['sendSingleMails'] = $task->sendSingleMails;
			} 
			else 
			{
				// Otherwise set an empty value, as it will not be used anyway
				$taskInfo['sendSingleMails'] = '';
			}
		}
		
		if (empty($taskInfo['subject'])) 
		{
			if ($currentSchedulerModuleAction->equals(Action::EDIT))
			{
				// In case of edit, set to internal value if no data was submitted already
				$taskInfo['subject'] = $task->subject;
			} 
			else 
			{
				// Otherwise set an empty value, as it will not be used anyway
				$taskInfo['subject'] = '';
			}
		}
		
		if (empty($taskInfo['forceSubject'])) 
		{
			if ($currentSchedulerModuleAction->equals(Action::EDIT))
			{
				// In case of edit, set to internal value if no data was submitted already
				$taskInfo['forceSubject'] = $task->forceSubject;
			} 
			else 
			{
				// Otherwise set an empty value, as it will not be used anyway
				$taskInfo['forceSubject'] = '';
			}
		}
		
		if (empty($taskInfo['templateUid'])) 
		{
			if ($currentSchedulerModuleAction->equals(Action::EDIT))
			{
				// In case of edit, set to internal value if no data was submitted already
				$taskInfo['templateUid'] = $task->templateUid;
			} 
			else 
			{
				// Otherwise set an empty value, as it will not be used anyway
				$taskInfo['templateUid'] = '';
			}
		}
		
		if (empty($taskInfo['forceTemplateInSingleMailing'])) 
		{
			if ($currentSchedulerModuleAction->equals(Action::EDIT))
			{
				// In case of edit, set to internal value if no data was submitted already
				$taskInfo['forceTemplateInSingleMailing'] = $task->forceTemplateInSingleMailing;
			} 
			else 
			{
				// Otherwise set an empty value, as it will not be used anyway
				$taskInfo['forceTemplateInSingleMailing'] = '';
			}
		}
		
		if (empty($taskInfo['scheduleUid'])) 
		{
			if ($currentSchedulerModuleAction->equals(Action::EDIT))
			{
				// In case of edit, set to internal value if no data was submitted already
				$taskInfo['scheduleUid'] = $task->scheduleUid;
			} 
			else 
			{
				// Otherwise set an empty value, as it will not be used anyway
				$taskInfo['scheduleUid'] = '';
			}
		}
		
		if (empty($taskInfo['forceScheduledTemplateInSingleMailing'])) 
		{
			if ($currentSchedulerModuleAction->equals(Action::EDIT))
			{
				// In case of edit, set to internal value if no data was submitted already
				$taskInfo['forceScheduledTemplateInSingleMailing'] = $task->forceScheduledTemplateInSingleMailing;
			} 
			else 
			{
				// Otherwise set an empty value, as it will not be used anyway
				$taskInfo['forceScheduledTemplateInSingleMailing'] = '';
			}
		}
		
		if (empty($taskInfo['format'])) 
		{
			if ($currentSchedulerModuleAction->equals(Action::EDIT))
			{
				// In case of edit, set to internal value if no data was submitted already
				$taskInfo['format'] = $task->format;
			} 
			else 
			{
				// Otherwise set an empty value, as it will not be used anyway
				$taskInfo['format'] = '';
			}
		}
		
		if (empty($taskInfo['senderAddress'])) 
		{
			if ($currentSchedulerModuleAction->equals(Action::EDIT))
			{
				// In case of edit, set to internal value if no data was submitted already
				$taskInfo['senderAddress'] = $task->senderAddress;
			} 
			else 
			{
				// Otherwise set an empty value, as it will not be used anyway
				$taskInfo['senderAddress'] = '';
			}
		}
		
		if (empty($taskInfo['senderName'])) 
		{
			if ($currentSchedulerModuleAction->equals(Action::EDIT))
			{
				// In case of edit, set to internal value if no data was submitted already
				$taskInfo['senderName'] = $task->senderName;
			} 
			else 
			{
				// Otherwise set an empty value, as it will not be used anyway
				$taskInfo['senderName'] = '';
			}
		}
		
		if (empty($taskInfo['replyToAddress'])) 
		{
			if ($currentSchedulerModuleAction->equals(Action::EDIT))
			{
				// In case of edit, set to internal value if no data was submitted already
				$taskInfo['replyToAddress'] = $task->replyToAddress;
			} 
			else 
			{
				// Otherwise set an empty value, as it will not be used anyway
				$taskInfo['replyToAddress'] = '';
			}
		}
		
		if (empty($taskInfo['cc'])) 
		{
			if ($currentSchedulerModuleAction->equals(Action::EDIT))
			{
				// In case of edit, set to internal value if no data was submitted already
				$taskInfo['cc'] = $task->cc;
			} 
			else 
			{
				// Otherwise set an empty value, as it will not be used anyway
				$taskInfo['cc'] = '';
			}
		}
		
		if (empty($taskInfo['bcc'])) 
		{
			if ($currentSchedulerModuleAction->equals(Action::EDIT))
			{
				// In case of edit, set to internal value if no data was submitted already
				$taskInfo['bcc'] = $task->bcc;
			} 
			else 
			{
				// Otherwise set an empty value, as it will not be used anyway
				$taskInfo['bcc'] = '';
			}
		}
		
		if (empty($taskInfo['rightsMapping'])) 
		{
			if ($currentSchedulerModuleAction->equals(Action::EDIT))
			{
				// In case of edit, set to internal value if no data was submitted already
				$taskInfo['rightsMapping'] = $task->rightsMapping;
			} 
			else 
			{
				// Otherwise set an empty value, as it will not be used anyway
				$taskInfo['rightsMapping'] = '';
			}
		}
		
		if (empty($taskInfo['simulatedUserGroups'])) 
		{
			if ($currentSchedulerModuleAction->equals(Action::EDIT))
			{
				// In case of edit, set to internal value if no data was submitted already
				$taskInfo['simulatedUserGroups'] = $task->simulatedUserGroups;
			} 
			else 
			{
				// Otherwise set an empty value, as it will not be used anyway
				$taskInfo['simulatedUserGroups'] = '';
			}
		}
		
		if (empty($taskInfo['sendRestrictedNewsOnly'])) 
		{
			if ($currentSchedulerModuleAction->equals(Action::EDIT))
			{
				// In case of edit, set to internal value if no data was submitted already
				$taskInfo['sendRestrictedNewsOnly'] = $task->sendRestrictedNewsOnly;
			} 
			else 
			{
				// Otherwise set an empty value, as it will not be used anyway
				$taskInfo['sendRestrictedNewsOnly'] = 0;
			}
		}
		
		if (empty($taskInfo['sendToUsersWithGroupsOnly'])) 
		{
			if ($currentSchedulerModuleAction->equals(Action::EDIT))
			{
				// In case of edit, set to internal value if no data was submitted already
				$taskInfo['sendToUsersWithGroupsOnly'] = $task->sendToUsersWithGroupsOnly;
			} 
			else 
			{
				// Otherwise set an empty value, as it will not be used anyway
				$taskInfo['sendToUsersWithGroupsOnly'] = 0;
			}
		}
		
		if (empty($taskInfo['mode'])) 
		{
			if ($currentSchedulerModuleAction->equals(Action::EDIT))
			{
				// In case of edit, set to internal value if no data was submitted already
				$taskInfo['mode'] = $task->mode;
			} 
			else 
			{
				// Otherwise set an empty value, as it will not be used anyway
				$taskInfo['mode'] = 'daemon';
			}
		}
		
		if (empty($taskInfo['sendMode'])) 
		{
			if ($currentSchedulerModuleAction->equals(Action::EDIT))
			{
				// In case of edit, set to internal value if no data was submitted already
				$taskInfo['sendMode'] = $task->sendMode;
			} 
			else 
			{
				// Otherwise set an empty value, as it will not be used anyway
				$taskInfo['sendMode'] = 'cumulated';
			}
		}

		if (empty($taskInfo['newsLimit']))
		{
			if ($currentSchedulerModuleAction->equals(Action::EDIT))
			{
				// In case of edit, set to internal value if no data was submitted already
				$taskInfo['newsLimit'] = $task->newsLimit;
			}
			else
			{
				// Otherwise set an empty value, as it will not be used anyway
				$taskInfo['newsLimit'] = '0';
			}
		}
		
		if (empty($taskInfo['debug'])) 
		{
			if ($currentSchedulerModuleAction->equals(Action::EDIT))
			{
				// In case of edit, set to internal value if no data was submitted already
				$taskInfo['debug'] = $task->debug;
			} 
			else 
			{
				// Otherwise set an empty value, as it will not be used anyway
				$taskInfo['debug'] = 0;
			}
		}
		
		if (empty($taskInfo['debugAddress'])) 
		{
			if ($currentSchedulerModuleAction->equals(Action::EDIT))
			{
				// In case of edit, set to internal value if no data was submitted already
				$taskInfo['debugAddress'] = $task->debugAddress;
			} 
			else 
			{
				// Otherwise set an empty value, as it will not be used anyway
				$taskInfo['debugAddress'] = '';
			}
		}
		
		if (empty($taskInfo['debugMailLimit'])) 
		{
			if ($currentSchedulerModuleAction->equals(Action::EDIT))
			{
				// In case of edit, set to internal value if no data was submitted already
				$taskInfo['debugMailLimit'] = $task->debugMailLimit;
			} 
			else 
			{
				// Otherwise set an empty value, as it will not be used anyway
				$taskInfo['debugMailLimit'] = '5';
			}
		}
		
		if (empty($taskInfo['detailPid'])) 
		{
			if ($currentSchedulerModuleAction->equals(Action::EDIT))
			{
				// In case of edit, set to internal value if no data was submitted already
				$taskInfo['detailPid'] = $task->detailPid;
			} 
			else 
			{
				// Otherwise set an empty value, as it will not be used anyway
				$taskInfo['detailPid'] = '';
			}
		}

		if (empty($taskInfo['loginPid']))
		{
			if ($currentSchedulerModuleAction->equals(Action::EDIT))
			{
				// In case of edit, set to internal value if no data was submitted already
				$taskInfo['loginPid'] = $task->loginPid;
			}
			else
			{
				// Otherwise set an empty value, as it will not be used anyway
				$taskInfo['loginPid'] = '';
			}
		}
		
		if (!empty($extConf['enableAutoLogin']))
		{			
			if (empty($taskInfo['autoLoginUser'])) 
			{
				if ($currentSchedulerModuleAction->equals(Action::EDIT))
				{
					// In case of edit, set to internal value if no data was submitted already
					$taskInfo['autoLoginUser'] = $task->autoLoginUser;
				} 
				else 
				{
					// Otherwise set an empty value, as it will not be used anyway
					$taskInfo['autoLoginUser'] = '';
				}
			}
		} 
		else 
		{
			$taskInfo['autoLoginUser'] = '';
		}

		/*
		if (empty($taskInfo['redirectorShortLink'])) 
		{
			if ($currentSchedulerModuleAction->equals(Action::EDIT))
			{
				// In case of edit, set to internal value if no data was submitted already
				$taskInfo['redirectorShortLink'] = $task->redirectorShortLink;
			} 
			else 
			{
				// Otherwise set an empty value, as it will not be used anyway
				$taskInfo['redirectorShortLink'] = '';
			}
		}
		*/
		
		if (empty($taskInfo['respectTags'])) 
		{
			if ($currentSchedulerModuleAction->equals(Action::EDIT))
			{
				// In case of edit, set to internal value if no data was submitted already
				$taskInfo['respectTags'] = $task->respectTags;
			} 
			else 
			{
				// Otherwise set an empty value, as it will not be used anyway
				$taskInfo['respectTags'] = 0;
			}
		}
		
		if (empty($taskInfo['respectZipregions'])) 
		{
			if ($currentSchedulerModuleAction->equals(Action::EDIT))
			{
				// In case of edit, set to internal value if no data was submitted already
				$taskInfo['respectZipregions'] = $task->respectZipregions;
			} 
			else 
			{
				// Otherwise set an empty value, as it will not be used anyway
				$taskInfo['respectZipregions'] = 0;
			}
		}

		if (empty($taskInfo['attachRelated']))
		{
			if ($currentSchedulerModuleAction->equals(Action::EDIT))
			{
				// In case of edit, set to internal value if no data was submitted already
				$taskInfo['attachRelated'] = $task->attachRelated;
			}
			else
			{
				// Otherwise set an empty value, as it will not be used anyway
				$taskInfo['attachRelated'] = 0;
			}
		}

		if (empty($taskInfo['attachSelf']))
		{
			if ($currentSchedulerModuleAction->equals(Action::EDIT))
			{
				// In case of edit, set to internal value if no data was submitted already
				$taskInfo['attachSelf'] = $task->attachSelf;
			}
			else
			{
				// Otherwise set an empty value, as it will not be used anyway
				$taskInfo['attachSelf'] = 0;
			}
		}

		if (empty($taskInfo['directNews']))
		{
			if ($currentSchedulerModuleAction->equals(Action::EDIT))
			{
				// In case of edit, set to internal value if no data was submitted already
				$taskInfo['directNews'] = $task->directNews;
			}
			else
			{
				// Otherwise set an empty value, as it will not be used anyway
				$taskInfo['directNews'] = 0;
			}
		}
		
		$additionalFields = [];
		
		// Write the code for the field
		$fieldID = 'task_newsPids';		
		$fieldCode = SchedulerHelper::getMooxNewsFoldersSelector('tx_scheduler[newsPids]',$taskInfo['newsPids']);		
		$additionalFields[$fieldID] = [
			'code' => $fieldCode,
			'label' => LocalizationUtility::translate(self::LLPATH.'tx_mooxmailer_tasks_mooxnewssendtask.news_pids_label','moox_mailer'),
			'cshKey' => '_MOD_tools_txschedulerM1',
			'cshLabel' => $fieldID
		];
		
		// Write the code for the field
		$fieldID = 'task_userPids';		
		$fieldCode = SchedulerHelper::getFeUsersFoldersSelector('tx_scheduler[userPids]',$taskInfo['userPids']);
		$additionalFields[$fieldID] = [
			'code' => $fieldCode,
			'label' => LocalizationUtility::translate(self::LLPATH.'tx_mooxmailer_tasks_mooxnewssendtask.user_pids_label','moox_mailer'),
			'cshKey' => '_MOD_tools_txschedulerM1',
			'cshLabel' => $fieldID
		];

		// Write the code for the field
		$fieldID = 'task_excludedUserGroups';
		$fieldCode = SchedulerHelper::getFeUsersGroupsSelector('tx_scheduler[excludedUserGroups]',$taskInfo['excludedUserGroups']);
		$additionalFields[$fieldID] = [
			'code' => $fieldCode,
			'label' => LocalizationUtility::translate(self::LLPATH.'tx_mooxmailer_tasks_mooxnewssendtask.excluded_user_groups_label','moox_mailer'),
			'cshKey' => '_MOD_tools_txschedulerM1',
			'cshLabel' => $fieldID
		];
				
		// Write the code for the field
		if(ExtensionManagementUtility::isLoaded('moox_address')){
			$fieldID = 'task_addressesPids';		
			$fieldCode = SchedulerHelper::getMooxAddressFoldersSelector('tx_scheduler[addressesPids]',$taskInfo['addressesPids']);		
			$additionalFields[$fieldID] = [
				'code' => $fieldCode,
				'label' => LocalizationUtility::translate(self::LLPATH.'tx_mooxmailer_tasks_mooxnewssendtask.addresses_pids_label','moox_mailer'),
				'cshKey' => '_MOD_tools_txschedulerM1',
				'cshLabel' => $fieldID
			];
			/*
			$fieldID = 'task_addressesCategories';
			$fieldCode = SchedulerHelper::getMooxAddressCategorySelector('tx_scheduler[addressesCategories]',$taskInfo['addressesCategories']);
			$additionalFields[$fieldID] = [
				'code' => $fieldCode,
				'label' => LocalizationUtility::translate(self::LLPATH.'tx_mooxmailer_tasks_mooxnewssendtask.addresses_categories_label','moox_mailer'),
				'cshKey' => '_MOD_tools_txschedulerM1',
				'cshLabel' => $fieldID
			];
			*/
		}
		
		// Write the code for the field
		$fieldID = 'task_includedNewsCategoriesUids';		
		$fieldCode = SchedulerHelper::getMooxNewsCategorySelector('tx_scheduler[includedNewsCategoriesUids]',$taskInfo['includedNewsCategoriesUids']);		
		$additionalFields[$fieldID] = [
			'code' => $fieldCode,
			'label' => LocalizationUtility::translate(self::LLPATH.'tx_mooxmailer_tasks_mooxnewssendtask.included_news_categories_uids_label','moox_mailer'),
			'cshKey' => '_MOD_tools_txschedulerM1',
			'cshLabel' => $fieldID
		];
			
		// Write the code for the field
		$fieldID = 'task_excludedNewsCategoriesUids';		
		$fieldCode = SchedulerHelper::getMooxNewsCategorySelector('tx_scheduler[excludedNewsCategoriesUids]',$taskInfo['excludedNewsCategoriesUids']);		
		$additionalFields[$fieldID] = [
			'code' => $fieldCode,
			'label' => LocalizationUtility::translate(self::LLPATH.'tx_mooxmailer_tasks_mooxnewssendtask.excluded_news_categories_uids_label','moox_mailer'),
			'cshKey' => '_MOD_tools_txschedulerM1',
			'cshLabel' => $fieldID
		];
		
		// Write the code for the field
		$fieldID = 'task_sendSingleMails';		
		$fieldCode = '<input type="checkbox" name="tx_scheduler[sendSingleMails]" id="' . $fieldID . '" value="1" '.($taskInfo['sendSingleMails']?'checked="checked" ':'').'/>';	
		$additionalFields[$fieldID] = [
			'code' => $fieldCode,
			'label' => LocalizationUtility::translate(self::LLPATH.'tx_mooxmailer_tasks_mooxnewssendtask.send_single_mails_label','moox_mailer'),
			'cshKey' => '_MOD_tools_txschedulerM1',
			'cshLabel' => $fieldID
		];
		
		// Write the code for the field
		$fieldID = 'task_subject';
		$fieldCode = '<input type="text" size=53" name="tx_scheduler[subject]" class="form-control" id="' . $fieldID . '" value="' . $taskInfo['subject'] . '" />';		
		$additionalFields[$fieldID] = [
			'code' => $fieldCode,
			'label' => LocalizationUtility::translate(self::LLPATH.'tx_mooxmailer_tasks_mooxnewssendtask.subject_label','moox_mailer'),
			'cshKey' => '_MOD_tools_txschedulerM1',
			'cshLabel' => $fieldID
		];	

		// Write the code for the field
		$fieldID = 'task_forceSubject';		
		$fieldCode = '<input type="checkbox" name="tx_scheduler[forceSubject]" id="' . $fieldID . '" value="1" '.($taskInfo['forceSubject']?'checked="checked" ':'').'/>';	
		$additionalFields[$fieldID] = [
			'code' => $fieldCode,
			'label' => LocalizationUtility::translate(self::LLPATH.'tx_mooxmailer_tasks_mooxnewssendtask.force_subject_label','moox_mailer'),
			'cshKey' => '_MOD_tools_txschedulerM1',
			'cshLabel' => $fieldID
		];

		// Write the code for the field
		$fieldID = 'task_templateUid';		
		$fieldCode = SchedulerHelper::getTemplateSelector('tx_scheduler[templateUid]',$taskInfo['templateUid']);		
		$additionalFields[$fieldID] = [
			'code' => $fieldCode,
			'label' => LocalizationUtility::translate(self::LLPATH.'tx_mooxmailer_tasks_mooxnewssendtask.template_uid_label','moox_mailer'),
			'cshKey' => '_MOD_tools_txschedulerM1',
			'cshLabel' => $fieldID
		];
		
		// Write the code for the field
		$fieldID = 'task_forceTemplateInSingleMailing';		
		$fieldCode = '<input type="checkbox" name="tx_scheduler[forceTemplateInSingleMailing]" id="' . $fieldID . '" value="1" '.($taskInfo['forceTemplateInSingleMailing']?'checked="checked" ':'').'/>';	
		$additionalFields[$fieldID] = [
			'code' => $fieldCode,
			'label' => LocalizationUtility::translate(self::LLPATH.'tx_mooxmailer_tasks_mooxnewssendtask.force_template_in_single_mailing_label','moox_mailer'),
			'cshKey' => '_MOD_tools_txschedulerM1',
			'cshLabel' => $fieldID
		];
		
		// Write the code for the field
		$fieldID = 'task_scheduleUid';		
		$fieldCode = SchedulerHelper::getScheduleSelector('tx_scheduler[scheduleUid]',$taskInfo['scheduleUid']);		
		$additionalFields[$fieldID] = [
			'code' => $fieldCode,
			'label' => LocalizationUtility::translate(self::LLPATH.'tx_mooxmailer_tasks_mooxnewssendtask.schedule_uid_label','moox_mailer'),
			'cshKey' => '_MOD_tools_txschedulerM1',
			'cshLabel' => $fieldID
		];
		
		// Write the code for the field
		$fieldID = 'task_forceScheduledTemplateInSingleMailing';		
		$fieldCode = '<input type="checkbox" name="tx_scheduler[forceScheduledTemplateInSingleMailing]" id="' . $fieldID . '" value="1" '.($taskInfo['forceScheduledTemplateInSingleMailing']?'checked="checked" ':'').'/>';	
		$additionalFields[$fieldID] = [
			'code' => $fieldCode,
			'label' => LocalizationUtility::translate(self::LLPATH.'tx_mooxmailer_tasks_mooxnewssendtask.force_scheduled_template_in_single_mailing_label','moox_mailer'),
			'cshKey' => '_MOD_tools_txschedulerM1',
			'cshLabel' => $fieldID
		];
		
		// Write the code for the field
		$fieldID = 'task_format';		
		$fieldCode = SchedulerHelper::getFormatSelector('tx_scheduler[format]',$taskInfo['format']);		
		$additionalFields[$fieldID] = [
			'code' => $fieldCode,
			'label' => LocalizationUtility::translate(self::LLPATH.'tx_mooxmailer_tasks_mooxnewssendtask.format_label','moox_mailer'),
			'cshKey' => '_MOD_tools_txschedulerM1',
			'cshLabel' => $fieldID
		];
		
		// Write the code for the field
		$fieldID = 'task_senderAddress';
		$fieldCode = '<input type="text" size=53" name="tx_scheduler[senderAddress]" class="form-control" id="' . $fieldID . '" value="' . $taskInfo['senderAddress'] . '" />';		
		$additionalFields[$fieldID] = [
			'code' => $fieldCode,
			'label' => LocalizationUtility::translate(self::LLPATH.'tx_mooxmailer_tasks_mooxnewssendtask.sender_address_label','moox_mailer'),
			'cshKey' => '_MOD_tools_txschedulerM1',
			'cshLabel' => $fieldID
		];	
		
		// Write the code for the field
		$fieldID = 'task_senderName';
		$fieldCode = '<input type="text" size=53" name="tx_scheduler[senderName]" class="form-control" id="' . $fieldID . '" value="' . $taskInfo['senderName'] . '" />';		
		$additionalFields[$fieldID] = [
			'code' => $fieldCode,
			'label' => LocalizationUtility::translate(self::LLPATH.'tx_mooxmailer_tasks_mooxnewssendtask.sender_name_label','moox_mailer'),
			'cshKey' => '_MOD_tools_txschedulerM1',
			'cshLabel' => $fieldID
		];

		// Write the code for the field
		$fieldID = 'task_replyToAddress';
		$fieldCode = '<input type="text" size=53" name="tx_scheduler[replyToAddress]" class="form-control" id="' . $fieldID . '" value="' . $taskInfo['replyToAddress'] . '" />';		
		$additionalFields[$fieldID] = [
			'code' => $fieldCode,
			'label' => LocalizationUtility::translate(self::LLPATH.'tx_mooxmailer_tasks_mooxnewssendtask.reply_to_address_label','moox_mailer'),
			'cshKey' => '_MOD_tools_txschedulerM1',
			'cshLabel' => $fieldID
		];
		
		// Write the code for the field
		$fieldID = 'task_cc';
		$fieldCode = '<textarea rows="3" cols="53" name="tx_scheduler[cc]" class="form-control" id="' . $fieldID . '">' . $taskInfo['cc'] . '</textarea>';		
		$additionalFields[$fieldID] = [
			'code' => $fieldCode,
			'label' => LocalizationUtility::translate(self::LLPATH.'tx_mooxmailer_tasks_mooxnewssendtask.cc_label','moox_mailer'),
			'cshKey' => '_MOD_tools_txschedulerM1',
			'cshLabel' => $fieldID
		];
		
		// Write the code for the field
		$fieldID = 'task_bcc';
		$fieldCode = '<textarea rows="3" cols="53" class="wide" name="tx_scheduler[bcc]" id="' . $fieldID . '">' . $taskInfo['bcc'] . '</textarea>';		
		$additionalFields[$fieldID] = [
			'code' => $fieldCode,
			'label' => LocalizationUtility::translate(self::LLPATH.'tx_mooxmailer_tasks_mooxnewssendtask.bcc_label','moox_mailer'),
			'cshKey' => '_MOD_tools_txschedulerM1',
			'cshLabel' => $fieldID
		];
		
		/*
		// Write the code for the field		
		$fieldID = 'task_rightsMapping';
		$fieldCode = '<textarea rows="5" cols="53" class="wide" name="tx_scheduler[rightsMapping]" id="' . $fieldID . '">' . $taskInfo['rightsMapping'] . '</textarea>';		
		$additionalFields[$fieldID] = [
			'code' => $fieldCode,
			'label' => LocalizationUtility::translate(self::LLPATH.'tx_mooxmailer_tasks_mooxnewssendtask.rights_mapping_label','moox_mailer'),
			'cshKey' => '_MOD_tools_txschedulerM1',
			'cshLabel' => $fieldID
		];
		*/
		
		// Write the code for the field
		$fieldID = 'task_sendRestrictedNewsOnly';		
		$fieldCode = '<input type="checkbox" name="tx_scheduler[sendRestrictedNewsOnly]" id="' . $fieldID . '" value="1" '.($taskInfo['sendRestrictedNewsOnly']?'checked="checked" ':'').'/>';	
		$additionalFields[$fieldID] = [
			'code' => $fieldCode,
			'label' => LocalizationUtility::translate(self::LLPATH.'tx_mooxmailer_tasks_mooxnewssendtask.send_restricted_news_only_label','moox_mailer'),
			'cshKey' => '_MOD_tools_txschedulerM1',
			'cshLabel' => $fieldID
		];
		
		// Write the code for the field
		$fieldID = 'task_sendToUsersWithGroupsOnly';		
		$fieldCode = '<input type="checkbox" name="tx_scheduler[sendToUsersWithGroupsOnly]" id="' . $fieldID . '" value="1" '.($taskInfo['sendToUsersWithGroupsOnly']?'checked="checked" ':'').'/>';	
		$additionalFields[$fieldID] = [
			'code' => $fieldCode,
			'label' => LocalizationUtility::translate(self::LLPATH.'tx_mooxmailer_tasks_mooxnewssendtask.send_to_users_with_groups_only','moox_mailer'),
			'cshKey' => '_MOD_tools_txschedulerM1',
			'cshLabel' => $fieldID
		];		
		
		// Write the code for the field
		$fieldID = 'task_simulatedUserGroups';		
		$fieldCode = SchedulerHelper::getFeUsersGroupsSelector('tx_scheduler[simulatedUserGroups]',$taskInfo['simulatedUserGroups']);		
		$additionalFields[$fieldID] = [
			'code' => $fieldCode,
			'label' => LocalizationUtility::translate(self::LLPATH.'tx_mooxmailer_tasks_mooxnewssendtask.simulated_user_groups_label','moox_mailer'),
			'cshKey' => '_MOD_tools_txschedulerM1',
			'cshLabel' => $fieldID
		];
		
		// Write the code for the field
		$fieldID = 'task_mode';		
		$fieldCode = SchedulerHelper::getModeSelector('tx_scheduler[mode]',$taskInfo['mode']);		
		$additionalFields[$fieldID] = [
			'code' => $fieldCode,
			'label' => LocalizationUtility::translate(self::LLPATH.'tx_mooxmailer_tasks_mooxnewssendtask.mode_label','moox_mailer'),
			'cshKey' => '_MOD_tools_txschedulerM1',
			'cshLabel' => $fieldID
		];
		
		// Write the code for the field
		$fieldID = 'task_sendMode';		
		$fieldCode = SchedulerHelper::getSendModeSelector('tx_scheduler[sendMode]',$taskInfo['sendMode']);		
		$additionalFields[$fieldID] = [
			'code' => $fieldCode,
			'label' => LocalizationUtility::translate(self::LLPATH.'tx_mooxmailer_tasks_mooxnewssendtask.send_mode_label','moox_mailer'),
			'cshKey' => '_MOD_tools_txschedulerM1',
			'cshLabel' => $fieldID
		];

		// Write the code for the field
		$fieldID = 'task_newsLimit';
		$fieldCode = '<input type="text" size="5" name="tx_scheduler[newsLimit]" class="form-control" id="' . $fieldID . '" value="' . $taskInfo['newsLimit'] . '" />';
		$additionalFields[$fieldID] = [
			'code' => $fieldCode,
			'label' => LocalizationUtility::translate(self::LLPATH.'tx_mooxmailer_tasks_mooxnewssendtask.news_limit_label','moox_mailer'),
			'cshKey' => '_MOD_tools_txschedulerM1',
			'cshLabel' => $fieldID
		];

		// Write the code for the field
		$fieldID 	= 'task_attachRelated';
		$fieldCode 	= '<input type="checkbox" name="tx_scheduler[attachRelated]" id="' . $fieldID . '" value="1" '.($taskInfo['attachRelated']?'checked="checked" ':'').'/>';
		$additionalFields[$fieldID] = [
			'code' => $fieldCode,
			'label' => LocalizationUtility::translate(self::LLPATH.'tx_mooxmailer_tasks_mooxnewssendtask.attach_related_label','moox_mailer'),
			'cshKey' => '_MOD_tools_txschedulerM1',
			'cshLabel' => $fieldID
		];

		// Write the code for the field
		$fieldID 	= 'task_attachSelf';
		$fieldCode 	= '<input type="checkbox" name="tx_scheduler[attachSelf]" id="' . $fieldID . '" value="1" '.($taskInfo['attachSelf']?'checked="checked" ':'').'/>';
		$additionalFields[$fieldID] = [
			'code' => $fieldCode,
			'label' => LocalizationUtility::translate(self::LLPATH.'tx_mooxmailer_tasks_mooxnewssendtask.attach_self_label','moox_mailer'),
			'cshKey' => '_MOD_tools_txschedulerM1',
			'cshLabel' => $fieldID
		];
		
		// Write the code for the field
		$fieldID 	= 'task_detailPid';		
		$fieldCode 	= SchedulerHelper::getMooxNewsDetailPageSelector('tx_scheduler[detailPid]',$taskInfo['detailPid']);		
		$additionalFields[$fieldID] = [
			'code' => $fieldCode,
			'label' => LocalizationUtility::translate(self::LLPATH.'tx_mooxmailer_tasks_mooxnewssendtask.detail_pid_label','moox_mailer'),
			'cshKey' => '_MOD_tools_txschedulerM1',
			'cshLabel' => $fieldID
		];

		// Write the code for the field
		$fieldID = 'task_loginPid';
		$fieldCode = '<input type="text" size="5" name="tx_scheduler[loginPid]" class="form-control" id="' . $fieldID . '" value="' . $taskInfo['loginPid'] . '" />';
		$additionalFields[$fieldID] = [
			'code' => $fieldCode,
			'label' => LocalizationUtility::translate(self::LLPATH.'tx_mooxmailer_tasks_mooxnewssendtask.login_pid_label','moox_mailer'),
			'cshKey' => '_MOD_tools_txschedulerM1',
			'cshLabel' => $fieldID
		];
		
		// Write the code for the field
		/*
		$fieldID = 'task_redirectorShortLink';
		$fieldCode = '<input type="text" size=53" name="tx_scheduler[redirectorShortLink]" class="form-control" id="' . $fieldID . '" value="' . $taskInfo['redirectorShortLink'] . '" />';		
		$additionalFields[$fieldID] = [
			'code' => $fieldCode,
			'label' => LocalizationUtility::translate(self::LLPATH.'tx_mooxmailer_tasks_mooxnewssendtask.redirector_short_link','moox_mailer'),
			'cshKey' => '_MOD_tools_txschedulerM1',
			'cshLabel' => $fieldID
		];
		*/

		// Write the code for the field
		$fieldID 	= 'task_directNews';
		$fieldCode 	= '<input type="checkbox" name="tx_scheduler[directNews]" id="' . $fieldID . '" value="1" '.($taskInfo['directNews']?'checked="checked" ':'').'/>';
		$additionalFields[$fieldID] = [
			'code' => $fieldCode,
			'label' => LocalizationUtility::translate(self::LLPATH.'tx_mooxmailer_tasks_mooxnewssendtask.direct_news_label','moox_mailer'),
			'cshKey' => '_MOD_tools_txschedulerM1',
			'cshLabel' => $fieldID
		];
		
		// Write the code for the field
		if (!empty($extConf['enableAutoLogin'])){
			$fieldID = 'task_autoLoginUser';		
			$fieldCode = SchedulerHelper::getAutoLoginUserSelector('tx_scheduler[autoLoginUser]',$taskInfo['autoLoginUser']);		
			$additionalFields[$fieldID] = [
				'code' => $fieldCode,
				'label' => LocalizationUtility::translate(self::LLPATH.'tx_mooxmailer_tasks_mooxnewssendtask.auto_login_user_label','moox_mailer'),
				'cshKey' => '_MOD_tools_txschedulerM1',
				'cshLabel' => $fieldID
			];
		}
		
		// Write the code for the field
		$fieldID 	= 'task_respectTags';		
		$fieldCode 	= '<input type="checkbox" name="tx_scheduler[respectTags]" id="' . $fieldID . '" value="1" '.($taskInfo['respectTags']?'checked="checked" ':'').'/>';	
		$additionalFields[$fieldID] = [
			'code' => $fieldCode,
			'label' => LocalizationUtility::translate(self::LLPATH.'tx_mooxmailer_tasks_mooxnewssendtask.respect_tags_label','moox_mailer'),
			'cshKey' => '_MOD_tools_txschedulerM1',
			'cshLabel' => $fieldID
		];
		
		// Write the code for the field
		$fieldID 	= 'task_respectZipregions';		
		$fieldCode 	= '<input type="checkbox" name="tx_scheduler[respectZipregions]" id="' . $fieldID . '" value="1" '.($taskInfo['respectZipregions']?'checked="checked" ':'').'/>';	
		$additionalFields[$fieldID] = [
			'code' => $fieldCode,
			'label' => LocalizationUtility::translate(self::LLPATH.'tx_mooxmailer_tasks_mooxnewssendtask.respect_zipregions_label','moox_mailer'),
			'cshKey' => '_MOD_tools_txschedulerM1',
			'cshLabel' => $fieldID
		];
		
		// Write the code for the field
		$fieldID 	= 'task_debug';		
		$fieldCode 	= '<input type="checkbox" name="tx_scheduler[debug]" id="' . $fieldID . '" value="1" '.($taskInfo['debug']?'checked="checked" ':'').'/>';	
		$additionalFields[$fieldID] = [
			'code' => $fieldCode,
			'label' => LocalizationUtility::translate(self::LLPATH.'tx_mooxmailer_tasks_mooxnewssendtask.debug_label','moox_mailer'),
			'cshKey' => '_MOD_tools_txschedulerM1',
			'cshLabel' => $fieldID
		];
		
		// Write the code for the field
		$fieldID = 'task_debugAddress';
		$fieldCode = '<input type="text" size=53" name="tx_scheduler[debugAddress]" class="form-control" id="' . $fieldID . '" value="' . $taskInfo['debugAddress'] . '" />';		
		$additionalFields[$fieldID] = [
			'code' => $fieldCode,
			'label' => LocalizationUtility::translate(self::LLPATH.'tx_mooxmailer_tasks_mooxnewssendtask.debug_address_label','moox_mailer'),
			'cshKey' => '_MOD_tools_txschedulerM1',
			'cshLabel' => $fieldID
		];	
		
		// Write the code for the field
		$fieldID = 'task_debugMailLimit';
		$fieldCode = '<input type="text" size="5" name="tx_scheduler[debugMailLimit]" class="form-control" id="' . $fieldID . '" value="' . $taskInfo['debugMailLimit'] . '" />';		
		$additionalFields[$fieldID] = [
			'code' => $fieldCode,
			'label' => LocalizationUtility::translate(self::LLPATH.'tx_mooxmailer_tasks_mooxnewssendtask.debug_mail_limit_label','moox_mailer'),
			'cshKey' => '_MOD_tools_txschedulerM1',
			'cshLabel' => $fieldID
		];	
		
		return $additionalFields;
	}

	/**
	 * This method checks any additional data that is relevant to the specific task
	 * If the task class is not relevant, the method is expected to return TRUE
	 *
	 * @param array $submittedData Reference to the array containing the data submitted by the user
	 * @param \TYPO3\CMS\Scheduler\Controller\SchedulerModuleController $parentObject Reference to the calling object (Scheduler's BE module)
	 * @return bool TRUE if validation was ok (or selected class is not relevant), FALSE otherwise
	 */
	public function validateAdditionalFields(array &$submittedData, \TYPO3\CMS\Scheduler\Controller\SchedulerModuleController $parentObject) 
	{		
		$result = TRUE;
		
		if ($submittedData['senderAddress']=="" || !GeneralUtility::validEmail($submittedData['senderAddress']))
		{
			$this->addMessage(LocalizationUtility::translate(self::LLPATH.'tx_mooxmailer_tasks_mooxnewssendtask.sender_address_error').": ".$submittedData['senderAddress'], FlashMessage::ERROR);
			$result = FALSE;
		} 
		
		if ($submittedData['replyToAddress']!="" && !GeneralUtility::validEmail($submittedData['replyToAddress']))
		{
			$this->addMessage(LocalizationUtility::translate(self::LLPATH.'tx_mooxmailer_tasks_mooxnewssendtask.reply_to_address_error').": ".$submittedData['replyToAddress'], FlashMessage::ERROR);
			$result = FALSE;
		}
		
		if (isset($submittedData['debug']) && ($submittedData['debugAddress']=="" || !GeneralUtility::validEmail($submittedData['debugAddress'])))
		{
            $this->addMessage(LocalizationUtility::translate(self::LLPATH.'tx_mooxmailer_tasks_mooxnewssendtask.debug_address_error').": ".$submittedData['debugAddress'], FlashMessage::ERROR);
			$result = FALSE;
		} 
		
		if ($submittedData['cc']!="")
		{
			$cc = explode("\r\n",$submittedData['cc']);
			$message = "";
			$line = 1;
			foreach ($cc AS $address)
			{
				if(!GeneralUtility::validEmail($address))
				{
					if($message!="")
					{
						$message .= "<br />";
					}
					$message .= LocalizationUtility::translate(self::LLPATH.'tx_mooxmailer_tasks_mooxnewssendtask.cc_address_error')." ".$line.": ".$address;
					$result = FALSE;
				}
				$line++;
			}
			if($message!="")
			{
                $this->addMessage($message, FlashMessage::ERROR);
			}
		} 
		
		if ($submittedData['bcc']!="")
		{
			$bcc = explode("\r\n",$submittedData['bcc']);
			$message = "";
			$line = 1;
			foreach ($bcc AS $address)
			{
				if(!GeneralUtility::validEmail($address))
				{
					if($message!="")
					{
						$message .= "<br />";
					}
					$message .= LocalizationUtility::translate(self::LLPATH.'tx_mooxmailer_tasks_mooxnewssendtask.bcc_address_error')." ".$line.": ".$address;
					$result = FALSE;
				}
				$line++;
			}
			if($message!=""){
                $this->addMessage($message, FlashMessage::ERROR);
			}
		} 
		
		if ($submittedData['rightsMapping']!="")
		{
			$rules = explode("\r\n",$submittedData['rightsMapping']);
			$message = "";
			$line = 1;
			foreach ($rules AS $rule)
			{
				$rightsMappingError = false;
				$ruleParts = explode(":",$rule);
				$categoryUid = $ruleParts[0];
				$feUserGroupsUids = $ruleParts[1];				
				if(!preg_match('/^([\d])+$/',trim($categoryUid)) || !preg_match('/^([\d]){1}([\d,])*$/',trim($feUserGroupsUids)))
				{					
					$result = FALSE;
					$rightsMappingError = true;
				} 
				else 
				{
					$feUserGroupsUidsArray = explode(",",$feUserGroupsUids);
					foreach($feUserGroupsUidsArray AS $uid)
					{
						if(!preg_match('/^([\d])+$/',trim($uid)))
						{
							$result = FALSE;
							$rightsMappingError = true;
						}
					}
				}
				if($rightsMappingError)
				{
					$message .= LocalizationUtility::translate(self::LLPATH.'tx_mooxmailer_tasks_mooxnewssendtask.rights_mapping_error')." ".$line.": ".$rule;
				}
				$line++;
			}
			if($message!="")
			{
                $this->addMessage($message, \TYPO3\CMS\Core\Messaging\FlashMessage::ERROR);
			}
		} 
		
		$submittedData['newsPids'] = (is_array($submittedData['newsPids']))?implode(",",$submittedData['newsPids']):$submittedData['newsPids'];
		$submittedData['userPids'] = (is_array($submittedData['userPids']))?implode(",",$submittedData['userPids']):$submittedData['userPids'];
		$submittedData['addressesPids'] = (is_array($submittedData['addressesPids']))?implode(",",$submittedData['addressesPids']):$submittedData['addressesPids'];
		$submittedData['addressesCategories'] = (is_array($submittedData['addressesCategories']))?implode(",",$submittedData['addressesCategories']):$submittedData['addressesCategories'];
		$submittedData['excludedUserGroups'] = (is_array($submittedData['excludedUserGroups']))?implode(",",$submittedData['excludedUserGroups']):$submittedData['excludedUserGroups'];
		$submittedData['includedNewsCategoriesUids'] = (is_array($submittedData['includedNewsCategoriesUids']))?implode(",",$submittedData['includedNewsCategoriesUids']):$submittedData['includedNewsCategoriesUids'];
		$submittedData['excludedNewsCategoriesUids'] = (is_array($submittedData['excludedNewsCategoriesUids']))?implode(",",$submittedData['excludedNewsCategoriesUids']):$submittedData['excludedNewsCategoriesUids'];
		$submittedData['sendSingleMails'] = isset($submittedData['sendSingleMails'])?1:0;
		$submittedData['forceSubject'] = isset($submittedData['forceSubject'])?1:0;
		$submittedData['forceTemplateInSingleMailing'] = isset($submittedData['forceTemplateInSingleMailing'])?1:0;
		$submittedData['forceScheduledTemplateInSingleMailing'] = isset($submittedData['forceScheduledTemplateInSingleMailing'])?1:0;
		$submittedData['debug'] = isset($submittedData['debug'])?1:0;
		$submittedData['simulatedUserGroups'] = (is_array($submittedData['simulatedUserGroups']))?implode(",",$submittedData['simulatedUserGroups']):$submittedData['simulatedUserGroups'];
		$submittedData['sendRestrictedNewsOnly'] = isset($submittedData['sendRestrictedNewsOnly'])?1:0;
		$submittedData['sendToUsersWithGroupsOnly'] = isset($submittedData['sendToUsersWithGroupsOnly'])?1:0;
		$submittedData['attachRelated'] = isset($submittedData['attachRelated'])?1:0;
		$submittedData['attachSelf'] = isset($submittedData['attachSelf'])?1:0;
		$submittedData['directNews'] = isset($submittedData['directNews'])?1:0;
		$submittedData['loginPid'] = ((int)$submittedData['loginPid']>0)?(int)$submittedData['loginPid']:'';
		$submittedData['newsLimit'] = ((int)$submittedData['newsLimit']>0)?(int)$submittedData['newsLimit']:'';
		
		return $result;
	}

	/**
	 * This method is used to save any additional input into the current task object
	 * if the task class matches
	 *
	 * @param array $submittedData Array containing the data submitted by the user
	 * @param \TYPO3\CMS\Scheduler\Task\AbstractTask $task Reference to the current task object
	 * @return void
	 */
	public function saveAdditionalFields(array $submittedData, \TYPO3\CMS\Scheduler\Task\AbstractTask $task)
	{				
		$task->newsPids = $submittedData['newsPids'];
		$task->userPids = $submittedData['userPids'];
		$task->addressesPids = $submittedData['addressesPids'];
		$task->addressesCategories = $submittedData['addressesCategories'];
		$task->excludedUserGroups = $submittedData['excludedUserGroups'];
		$task->includedNewsCategoriesUids = $submittedData['includedNewsCategoriesUids'];
		$task->excludedNewsCategoriesUids = $submittedData['excludedNewsCategoriesUids'];
		$task->sendSingleMails = $submittedData['sendSingleMails'];
		$task->subject = $submittedData['subject'];
		$task->forceSubject = $submittedData['forceSubject'];
		$task->templateUid = $submittedData['templateUid'];
		$task->forceTemplateInSingleMailing = $submittedData['forceTemplateInSingleMailing'];
		$task->scheduleUid = $submittedData['scheduleUid'];
		$task->forceScheduledTemplateInSingleMailing = $submittedData['forceScheduledTemplateInSingleMailing'];
		$task->format = $submittedData['format'];
		$task->senderAddress = $submittedData['senderAddress'];
		$task->senderName = $submittedData['senderName'];
		$task->replyToAddress = $submittedData['replyToAddress'];
		$task->cc = $submittedData['cc'];
		$task->bcc = $submittedData['bcc'];
		$task->rightsMapping = $submittedData['rightsMapping'];
		$task->simulatedUserGroups = $submittedData['simulatedUserGroups'];
		$task->sendRestrictedNewsOnly = $submittedData['sendRestrictedNewsOnly'];
		$task->sendToUsersWithGroupsOnly = $submittedData['sendToUsersWithGroupsOnly'];
		$task->mode = $submittedData['mode'];
		$task->sendMode = $submittedData['sendMode'];
		$task->detailPid = $submittedData['detailPid'];
		$task->loginPid = $submittedData['loginPid'];
		$task->newsLimit = $submittedData['newsLimit'];
		$task->autoLoginUser = $submittedData['autoLoginUser'];
		$task->debug = $submittedData['debug'];
		$task->debugAddress = $submittedData['debugAddress'];
		$task->debugMailLimit = $submittedData['debugMailLimit'];
		$task->redirectorShortLink = $submittedData['redirectorShortLink'];
		$task->respectTags = $submittedData['respectTags'];
		$task->respectZipregions = $submittedData['respectZipregions'];
		$task->respectZipregions = $submittedData['respectZipregions'];
		$task->attachRelated = $submittedData['attachRelated'];
		$task->attachSelf = $submittedData['attachSelf'];
		$task->directNews = $submittedData['directNews'];
	}				
}

?>