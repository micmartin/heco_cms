<?php
namespace Neulandlotsen\MooxMailer\Domain\Model;

/**
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */
 
class Newsletter extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{
	
	/**
	 * @var DateTime
	 */
	protected $crdate;

	/**
	 * @var DateTime
	 */
	protected $tstamp;
	
	/**
	 * Sichtbarkeit
	 *
	 * @var int
	 */
	protected $hidden;
	
	/**
	 * Seite
	 *
	 * @var int	
	 */
	protected $page;
	
	/**
	 * Titel
	 *
	 * @var string	
	 * @TYPO3\CMS\Extbase\Annotation\Validate("NotEmpty")
	 */
	protected $title;
	
	/**
	 * Betreff
	 *
	 * @var string
	 */
	protected $subject;
	
	/**
	 * Absender-Name
	 *
	 * @var string	
	 */
	protected $senderName;		
	
	/**
	 * Sender-Email
	 *
	 * @var string	
	 */
	protected $senderEmail;	
	
	/**
	 * Antwort-Email
	 *
	 * @var string	
	 */
	protected $replyToEmail;	
	
	/**
	 * User-Ordner
	 *
	 * @var string	
	 */
	protected $userFolders;	
	
	/**
	 * Adressen-Ordner
	 *
	 * @var string	
	 */
	protected $addressFolders;
	
	/**
	 * Test-Adressen-Ordner
	 *
	 * @var string	
	 */
	protected $testAddressFolders;

	/**
	 * Ordner zum Verschieben nach dem Senden
	 *
	 * @var string	
	 */
	protected $moveToFolder;	
	
	/**
	 * Autologin
	 *
	 * @var bool	
	 */
	protected $autologin;
	
	/**
	 * @var string
	 * @TYPO3\CMS\Extbase\Annotation\Validate("NotEmpty")
	 */
	protected $zipmode;
	
	/**
	 * @var string
	 * @TYPO3\CMS\Extbase\Annotation\Validate("NotEmpty")
	 */
	protected $ziplist;
	
	/**
	 * Html
	 *
	 * @var string	
	 */
	protected $html;		
	
	/**
	 * Gesendet
	 *
	 * @var int	
	 */
	protected $sent;
	
	/**
	 * Betreff nach dem Versenden
	 *
	 * @var string	
	 */
	protected $sentSubject;
	
	/**
	 * Anzahl gesendeter Mails
	 *
	 * @var int	
	 */
	protected $sentCount;

	/**
	 * job id die durch den Mailer-Service vergeben wurde
	 *
	 * @var string	
	 */
	protected $job;	
	
	/**
	 * Datum der erste Öffnung
	 *
	 * @var int	
	 */
	protected $firstOpening;
	
	/**
	 * Datum der letzte Öffnung
	 *
	 * @var int	
	 */
	protected $lastOpening;
	
	/**
	 * Öffnungen
	 *
	 * @var int
	 */
	protected $openings;
	
	/**
	 * Öffnungen unique
	 *
	 * @var int	
	 */
	protected $openingsUnique;
	
	/**
	 * Datum des ersten Clicks
	 *
	 * @var int	
	 */
	protected $firstClick;
	
	/**
	 * Datum des letzten Clicks
	 *
	 * @var int	
	 */
	protected $lastClick;
	
	/**
	 * Clicks
	 *
	 * @var int
	 */
	protected $clicks;
	
	/**
	 * Clicks unique
	 *
	 * @var int	
	 */
	protected $clicksUnique;
	
	/**
	 * Mails
	 *
	 * @var int
	 */
	protected $mails;
	
	/**
	 * Fails
	 *
	 * @var int	
	 */
	protected $fails;
	
	/**
	 * Bounces
	 *
	 * @var int	
	 */
	protected $bounces;
	
	/**
	 * hash
	 *
	 * @var string	
	 */
	protected $hash;	
	
	/**
	 * Get creation date
	 *
	 * @return int
	 */
	public function getCrdate()
	{
		return $this->crdate;
	}

	/**
	 * Set creation date
	 *
	 * @param int $crdate
	 * @return void
	 */
	public function setCrdate($crdate)
	{
		$this->crdate = $crdate;
	}

	/**
	 * Get year of crdate
	 *
	 * @return int
	 */
	public function getYearOfCrdate()
	{
		return $this->getCrdate()->format('Y');
	}

	/**
	 * Get month of crdate
	 *
	 * @return int
	 */
	public function getMonthOfCrdate()
	{
		return $this->getCrdate()->format('m');
	}

	/**
	 * Get day of crdate
	 *
	 * @return int
	 */
	public function getDayOfCrdate()
	{
		return (int)$this->crdate->format('d');
	}

	/**
	 * Get timestamp
	 *
	 * @return int
	 */
	public function getTstamp()
	{
		return $this->tstamp;
	}

	/**
	 * Set time stamp
	 *
	 * @param int $tstamp time stamp
	 * @return void
	 */
	public function setTstamp($tstamp)
	{
		$this->tstamp = $tstamp;
	}
	
	/**
	 * Returns the hidden
	 *
	 * @return int $hidden
	 */
	public function getHidden()
	{
		return $this->hidden;
	}

	/**
	 * Sets the hidden
	 *
	 * @param int $hidden
	 * @return void
	 */
	public function setHidden($hidden)
	{
		$this->hidden = $hidden;
	}
	
	/**
	 * Returns the page
	 *
	 * @return int $page
	 */
	public function getPage()
	{
		return $this->page;
	}

	/**
	 * Sets the page
	 *
	 * @param int $page
	 * @return void
	 */
	public function setPage($page)
	{
		$this->page = $page;
	}
	
	/**
	 * Returns the title
	 *
	 * @return string $title
	 */
	public function getTitle()
	{
		return $this->title;
	}

	/**
	 * Sets the title
	 *
	 * @param string $title
	 * @return void
	 */
	public function setTitle($title)
	{
		$this->title = $title;
	}
	
	/**
	 * Returns the subject
	 *
	 * @return string $subject
	 */
	public function getSubject()
	{
		return $this->subject;
	}

	/**
	 * Sets the subject
	 *
	 * @param string $subject
	 * @return void
	 */
	public function setSubject($subject)
	{
		$this->subject = $subject;
	}
	
	/**
	 * Returns the sender name
	 *
	 * @return string $senderName
	 */
	public function getSenderName()
	{
		return $this->senderName;
	}

	/**
	 * Sets the sender name
	 *
	 * @param string $senderName
	 * @return void
	 */
	public function setSenderName($senderName)
	{
		$this->senderName = $senderName;
	}
	
	/**
	 * Returns the sender email
	 *
	 * @return string $senderEmail
	 */
	public function getSenderEmail()
	{
		return $this->senderEmail;
	}

	/**
	 * Sets the sender email
	 *
	 * @param string $senderEmail
	 * @return void
	 */
	public function setSenderEmail($senderEmail)
	{
		$this->senderEmail = $senderEmail;
	}
	
	/**
	 * Returns the reply to email
	 *
	 * @return string $replyToEmail
	 */
	public function getReplyToEmail()
	{
		return $this->replyToEmail;
	}

	/**
	 * Sets the reply to email
	 *
	 * @param string $replyToEmail
	 * @return void
	 */
	public function setReplyToEmail($replyToEmail)
	{
		$this->replyToEmail = $replyToEmail;
	}
	
	/**
	 * Returns the user folders
	 *
	 * @return string $userFolders
	 */
	public function getUserFolders()
	{
		return $this->userFolders;
	}

	/**
	 * Sets the user folders
	 *
	 * @param string $userFolders
	 * @return void
	 */
	public function setUserFolders($userFolders)
	{
		$this->userFolders = $userFolders;
	}
	
	/**
	 * Returns the address folders
	 *
	 * @return string $addressFolders
	 */
	public function getAddressFolders()
	{
		return $this->addressFolders;
	}

	/**
	 * Sets the address folders
	 *
	 * @param string $addressFolders
	 * @return void
	 */
	public function setAddressFolders($addressFolders)
	{
		$this->addressFolders = $addressFolders;
	}
	
	/**
	 * Returns the test address folders
	 *
	 * @return string $testAddressFolders
	 */
	public function getTestAddressFolders()
	{
		return $this->testAddressFolders;
	}

	/**
	 * Sets the test address folders
	 *
	 * @param string $testAddressFolders
	 * @return void
	 */
	public function setTestAddressFolders($testAddressFolders)
	{
		$this->testAddressFolders = $testAddressFolders;
	}
	
	/**
	 * Returns the move to folder
	 *
	 * @return string $moveToFolder
	 */
	public function getMoveToFolder()
	{
		return $this->moveToFolder;
	}

	/**
	 * Sets the move to folder
	 *
	 * @param string $moveToFolder
	 * @return void
	 */
	public function setMoveToFolder($moveToFolder)
	{
		$this->moveToFolder = $moveToFolder;
	}
	
	/**
	 * Returns the autologin
	 *
	 * @return bool $autologin
	 */
	public function getAutologin()
	{
		return $this->autologin;
	}

	/**
	 * Sets the autologin
	 *
	 * @param bool $autologin
	 * @return void
	 */
	public function setAutologin($autologin)
	{
		$this->autologin = $autologin;
	}
	
	/**
	 * @return string $zipmode
	 */
	public function getZipmode()
	{
		return $this->zipmode;
	}

	/**
	 * @param string $zipmode
	 * @return void
	 */
	public function setZipmode($zipmode)
	{
		$this->zipmode = $zipmode;
	}
	
	/**
	 * @return string $ziplist
	 */
	public function getZiplist()
	{
		return $this->ziplist;
	}

	/**
	 * @param string $ziplist
	 * @return void
	 */
	public function setZiplist($ziplist)
	{
		$this->ziplist = $ziplist;
	}
	
	/**
	 * Returns the html
	 *
	 * @return string $html
	 */
	public function getHtml()
	{
		return $this->html;
	}

	/**
	 * Sets the html
	 *
	 * @param string $html
	 * @return void
	 */
	public function setHtml($html)
	{
		$this->html = $html;
	}		
	
	/**
	 * Returns the sent
	 *
	 * @return int $sent
	 */
	public function getSent()
	{
		return $this->sent;
	}

	/**
	 * Sets the sent
	 *
	 * @param int $sent
	 * @return void
	 */
	public function setSent($sent)
	{
		$this->sent = $sent;
	}	

	/**
	 * Returns the sent subject
	 *
	 * @return string $sentSubject
	 */
	public function getSentSubject()
	{
		return $this->sentSubject;
	}

	/**
	 * Sets the sent subject
	 *
	 * @param string $sentSubject
	 * @return void
	 */
	public function setSentSubject($sentSubject)
	{
		$this->sentSubject = $sentSubject;
	}
	
	/**
	 * Returns the sent count
	 *
	 * @return int $sentCount
	 */
	public function getSentCount()
	{
		return $this->sentCount;
	}

	/**
	 * Sets the sent count
	 *
	 * @param int $sentCount
	 * @return void
	 */
	public function setSentCount($sentCount)
	{
		$this->sentCount = $sentCount;
	}

	/**
	 * Returns the job
	 *
	 * @return string $job
	 */
	public function getJob()
	{
		return $this->job;
	}

	/**
	 * Sets the job
	 *
	 * @param string $job
	 * @return void
	 */
	public function setJob($job)
	{
		$this->job = $job;
	}
	
	/**
	 * Returns the first opening
	 *
	 * @return int $firstOpening
	 */
	public function getFirstOpening()
	{
		return $this->firstOpening;
	}

	/**
	 * Sets the first opening
	 *
	 * @param int $firstOpening
	 * @return void
	 */
	public function setFirstOpening($firstOpening)
	{
		$this->firstOpening = $firstOpening;
	}
	
	/**
	 * Returns the last opening
	 *
	 * @return int $lastOpening
	 */
	public function getLastOpening()
	{
		return $this->lastOpening;
	}

	/**
	 * Sets the last opening
	 *
	 * @param int $lastOpening
	 * @return void
	 */
	public function setLastOpening($lastOpening)
	{
		$this->lastOpening = $lastOpening;
	}
	
	/**
	 * Returns the openings
	 *
	 * @return int $openings
	 */
	public function getOpenings()
	{
		return $this->openings;
	}

	/**
	 * Sets the openings
	 *
	 * @param int $openings
	 * @return void
	 */
	public function setOpenings($openings)
	{
		$this->openings = $openings;
	}
	
	/**
	 * Returns the openings unique
	 *
	 * @return int $openingsUnique
	 */
	public function getOpeningsUnique()
	{
		return $this->openingsUnique;
	}

	/**
	 * Sets the openings unique
	 *
	 * @param int $openingsUnique
	 * @return void
	 */
	public function setOpeningsUnique($openingsUnique)
	{
		$this->openingsUnique = $openingsUnique;
	}
	
	/**
	 * Returns the first click
	 *
	 * @return int $firstClick
	 */
	public function getFirstClick()
	{
		return $this->firstClick;
	}

	/**
	 * Sets the first click
	 *
	 * @param int $firstClick
	 * @return void
	 */
	public function setFirstClick($firstClick)
	{
		$this->firstClick = $firstClick;
	}
	
	/**
	 * Returns the last click
	 *
	 * @return int $lastClick
	 */
	public function getLastClick()
	{
		return $this->lastClick;
	}

	/**
	 * Sets the last click
	 *
	 * @param int $lastClick
	 * @return void
	 */
	public function setLastClick($lastClick)
	{
		$this->lastClick = $lastClick;
	}
	
	/**
	 * Returns the clicks
	 *
	 * @return int $clicks
	 */
	public function getClicks()
	{
		return $this->clicks;
	}

	/**
	 * Sets the clicks
	 *
	 * @param int $clicks
	 * @return void
	 */
	public function setClicks($clicks)
	{
		$this->clicks = $clicks;
	}
	
	/**
	 * Returns the clicks unique
	 *
	 * @return int $clicksUnique
	 */
	public function getClicksUnique()
	{
		return $this->clicksUnique;
	}

	/**
	 * Sets the clicks unique
	 *
	 * @param int $clicksUnique
	 * @return void
	 */
	public function setClicksUnique($clicksUnique)
	{
		$this->clicksUnique = $clicksUnique;
	}
		
	/**
	 * Returns the bounces
	 *
	 * @return int $bounces
	 */
	public function getBounces()
	{
		return $this->bounces;
	}

	/**
	 * Sets the bounces
	 *
	 * @param int $bounces
	 * @return void
	 */
	public function setBounces($bounces)
	{
		$this->bounces = $bounces;
	}
	
	/**
	 * Returns the fails
	 *
	 * @return int $fails
	 */
	public function getFails()
	{
		return $this->fails;
	}

	/**
	 * Sets the fails
	 *
	 * @param int $fails
	 * @return void
	 */
	public function setFails($fails)
	{
		$this->fails = $fails;
	}
	
	/**
	 * Returns the hash
	 *
	 * @return string $hash
	 */
	public function getHash()
	{
		return md5($this->crdate->format("YmdHis")."mOOx".$this->uid);
	}
}
?>