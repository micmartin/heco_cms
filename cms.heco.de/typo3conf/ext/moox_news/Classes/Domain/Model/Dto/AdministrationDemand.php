<?php
namespace Neulandlotsen\MooxNews\Domain\Model\Dto;

/**
 * This file is part of the "moox_news" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

/**
 * Administration Demand model
 */
class AdministrationDemand extends \Neulandlotsen\MooxNews\Domain\Model\Dto\NewsDemand
{

    /**
     * @var string
     */
    protected $recursive;

    /**
     * @var array
     */
    protected $selectedCategories = [];

    /**
     * @var string
     */
    protected $sortingField = 'datetime';

    /**
     * @var string
     */
    protected $sortingDirection = 'desc';
	
	/**
	 * @var boolean
	 */
	protected $sortingMode;

    /**
     * @var string
     */
    protected $searchWord;

    /**
     * @var int
     */
    protected $hidden;

    /**
     * @var int
     */
    protected $archived;

	/**
	 * @var integer
	 */
	protected $mailerFrequency;

	/**
     * @var string
     */
	protected $type;

    /**
     * @return string
     */
    public function getRecursive()
    {
        return $this->recursive;
    }

    /**
     * @param $recursive
     */
    public function setRecursive($recursive)
    {
        $this->recursive = $recursive;
    }

    /**
     * @return array
     */
    public function getSelectedCategories()
    {
        return $this->selectedCategories;
    }

    /**
     * @param $selectedCategories
     */
    public function setSelectedCategories($selectedCategories)
    {
        if ($selectedCategories === '0' || $selectedCategories === ['0']) {
            return;
        }
        if (is_string($selectedCategories)) {
            $selectedCategories = explode(',', $selectedCategories);
        }
        $this->selectedCategories = $selectedCategories;
    }

    /**
     * @return string
     */
    public function getSortingField()
    {
        return $this->sortingField;
    }

    /**
     * @param $sortingField
     */
    public function setSortingField($sortingField)
    {
        $this->sortingField = $sortingField;
    }

    /**
     * @return string
     */
    public function getSortingDirection()
    {
        return $this->sortingDirection;
    }

    /**
     * @param $sortingDirection
     */
    public function setSortingDirection($sortingDirection)
    {
        $this->sortingDirection = $sortingDirection;
    }

	/**
	 * @param boolean $sortingMode
	 */
	public function setSortingMode($sortingMode) 
	{
		$this->sortingMode = $sortingMode;
	}

	/**
	 * @return boolean
	 */
	public function getSortingMode() 
	{
		return $this->sortingMode;
	}

    /**
     * @return string
     */
    public function getSearchWord()
    {
        return $this->searchWord;
    }

    /**
     * @param string $searchWord
     */
    public function setSearchWord($searchWord)
    {
        $this->searchWord = $searchWord;
    }

	/**
     * @return int
     */
    public function getHidden()
    {
        return $this->hidden;
    }

    /**
     * @param int $hidden
     */
    public function setHidden($hidden)
    {
        $this->hidden = $hidden;
    }

    /**
     * @return int
     */
    public function getArchived()
    {
        return $this->archived;
    }

    /**
     * @param int $archived
     */
    public function setArchived($archived)
    {
        $this->archived = $archived;
    }

	/**
	 * @param string $type
	 */
	public function setType($type) 
	{
		$this->type = $type;
	}

	/**
	 * @return string
	 */
	public function getType() 
	{
		return $this->type;
	}
	
	/**
	 * @param string $mailerFrequency	 
	 */
	public function setMailerFrequency($mailerFrequency) 
	{
		$this->mailerFrequency = $mailerFrequency;
	}

	/**
	 * @return string
	 */
	public function getMailerFrequency() 
	{
		return $this->mailerFrequency;
	}
}
