<?php
namespace Neulandlotsen\MooxMailer\ViewHelpers;

/**
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */

use Neulandlotsen\MooxMailer\Controller\NewsletterController;
 
/**
 * ViewHelper to replace markers with real content
 *
 * # Example: Basic Example
 * # Description: Replace markers with real content
 * <code>
 *	<mx:replaceMarkers>{text}</mx:replaceMarkers>
 * </code> 
 *
 */
class ReplaceMarkersViewHelper extends \TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper {

    public function initializeArguments()
    {
        parent::initializeArguments();
        $this->registerArgument('object', 'object', 'object', true);
    }

    /**
	 * Replace markers with real content
	 *
	 * @param string $mode
	 * @param string $content
	 * @return string
	 */
	public function render($mode = "page", $content = NULL) {
        $object = $this->arguments['object'];

		if ($content === NULL) {
			$content = $this->renderChildren();
		}
		
		if($mode=="news"){
			$content = NewsletterController::replaceMarkers($content,$object);
		} else {
			$content = NewsletterController::replaceMarkers($content,$object);
		}
		return $content;
	}
}
