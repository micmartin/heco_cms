<?php
namespace Neulandlotsen\MooxCore\Controller;

/**
 * This file is part of the "moox_core" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

use Neulandlotsen\MooxCore\Utility\HelperUtility as CoreHelperUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use Neulandlotsen\MooxCore\Service\HelperService;
use Neulandlotsen\MooxCore\Utility\PdfGenerator;
	 
class PdfController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController 
{
	/**
	 * @var HelperService
	 */
	protected $helperService;

	/**
	 * @var PdfGenerator
	 */
	protected $pdfGenerator;

    /**
     * @param HelperService $helperService
     */
    public function injectHelperService(HelperService $helperService)
    {
        $this->helperService = $helperService;
    }

    /**
     * @param PdfGenerator $pdfGenerator
     */
    public function injectPdfGenerator(PdfGenerator $pdfGenerator)
    {
        $this->pdfGenerator = $pdfGenerator;
    }

	/**
	 * @param string $source	
	 */
	public function generatePdfAction($source = "")
	{
		if($source)
		{
			$file = CoreHelperUtility::getRemoteFile($source);
			if($file)
			{
				$this->pdfGenerator->generateFromHtml($file);
			}
		}
		$this->view = NULL;
		exit();
	}
}
?>