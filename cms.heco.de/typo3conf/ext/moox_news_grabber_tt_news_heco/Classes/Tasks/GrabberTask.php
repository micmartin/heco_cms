<?php
namespace Neulandlotsen\MooxNewsGrabberTtNewsHeco\Tasks;

/**
 * This file is part of the "" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */
 
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Utility\LocalizationUtility;
use TYPO3\CMS\Core\Messaging\FlashMessage;
use TYPO3\CMS\Core\Messaging\FlashMessageService;
 
class GrabberTask extends \TYPO3\CMS\Scheduler\Task\AbstractTask
{					
	/**
	 * @var string
	 */
	public $newsPids;
	
	/**
	 * @var string
	 */
	static public $url = 'http://heco.neulandlotsen.de/export/exportNews.php?auth=5d09b18c1e7ee7b88cc1d8b4563a3d7f';

	/**
	 * @var string
	 */
	static public $importSource = 'tt_news';

	/**
	 * @var array
	 */
	static public $categories = [];

	/**
	 * @var bool
	 */
	 static public $allowUpdate = true;
	
	/**
	 * @var bool
	 */
	 static public $forceUpdate = false;
	
	/**
	 * Prepares the grabber job.
	 *
	 * @return	boolean	Returns TRUE on success, FALSE if no items were indexed or none were found.
	 * @see	typo3/sysext/scheduler/tx_scheduler_Task#execute()
	 */
	public function execute() 
	{											
		$executionSucceeded = true;
		
		$objectManager = GeneralUtility::makeInstance('TYPO3\CMS\Extbase\Object\ObjectManager');
		$flashMessageService = $objectManager->get(FlashMessageService::class);
		$flashMessageQueue = $flashMessageService->getMessageQueueByIdentifier();
		$newsRepository = $objectManager->get('Neulandlotsen\MooxNews\Domain\Repository\NewsRepository');

		$ch = curl_init();
		curl_setopt ($ch, CURLOPT_URL, self::$url);
		curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_HEADER, false);
		$xml = curl_exec($ch);

		// Feed einlesen
		if(!$xml = simplexml_load_string($xml))
		{			
			$message = GeneralUtility::makeInstance('TYPO3\\CMS\\Core\\Messaging\\FlashMessage',
				'Fehler beim Einlesen der XML Datei von tt_news!',
				'', // the header is optional
				FlashMessage::ERROR, 
				TRUE 
			);
			$flashMessageQueue->addMessage($message);
			
			$executionSucceeded = false;		
		} 
		else 
		{			
			// Items vorhanden?
			if(!isset($xml->news))
			{
				$message = GeneralUtility::makeInstance('TYPO3\\CMS\\Core\\Messaging\\FlashMessage',
					'Keine News im Feed!',
					'', // the header is optional
					FlashMessage::WARNING, 
					TRUE
				);
				$flashMessageQueue->addMessage($message);			
			} 
			else 
			{				
				$items = [];
				
				$objectManager = GeneralUtility::makeInstance('TYPO3\CMS\Extbase\Object\ObjectManager');
				$grabberService = $objectManager->get('Neulandlotsen\MooxNews\Service\GrabberService');

				// Items holen
				foreach($xml->news as $item)
				{
					$images = [];
					if(isset($item->images) && count($item->images->image)>1)
					{
						foreach($item->images->image AS $image)
						{
							$url = (string)$image->url;

							if (trim($url) != '')
							{
								//$test = get_headers($url, 1);
								$ch = curl_init($url);
								curl_setopt($ch, CURLOPT_NOBODY, true);
								curl_exec($ch);
								$httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
								if ($httpcode == 200)
								{
									$images[] = [
										'url'=>$url,
										'caption'=>(string)$image->caption,
										'alt'=>(string)$image->alt,
										'title'=>(string)$image->title,
										'secured' => 0
									];
								}
							}
						}
					}
					elseif((string)$item->{'images'}->image->url!='')
					{
						$image = $item->{'images'}->image;
						$url = (string)$image->url;
						//$test = get_headers($url, 1);
						$ch = curl_init($url);
						curl_setopt($ch, CURLOPT_NOBODY, true);
						curl_exec($ch);
						$httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
						if ($httpcode == 200)
						{
							$images[] = [
								'url'=>$url,
								'caption'=>(string)$image->caption,
								'alt'=>(string)$image->alt,
								'title'=>(string)$image->title,
								'secured' => 0
							];
						}
					}

					$categories = [];

					$categoryMapper = [
						28 => 1,
						23 => 2,
						133 => 3,
						35 => 4,
						36 => 4,
						38 => 4,
						39 => 4,
						40 => 4,
						41 => 4,
						42 => 4,
						43 => 4,
						47 => 4,
						48 => 4,
						49 => 4,
						50 => 4,
						51 => 4,
						52 => 4,
						53 => 4,
						54 => 4,
						55 => 4,
						57 => 4,
						59 => 4,
						60 => 4,
						62 => 4,
						63 => 4,
						64 => 4,
						65 => 4,
						70 => 4,
						71 => 4,
						72 => 4,
						73 => 4,
						74 => 4,
						75 => 4,
						76 => 4,
						77 => 4,
						78 => 4,
						79 => 4,
						80 => 4,
						81 => 4,
						82 => 4,
						83 => 4,
						84 => 4,
						85 => 4,
						86 => 4,
						89 => 4,
						90 => 4,
						91 => 4,
						92 => 4,
						94 => 4,
						95 => 4,
						96 => 4,
						97 => 4,
						98 => 4,
						99 => 4,
						100 => 4,
						101 => 4,
						102 => 4,
						103 => 4,
						104 => 4,
						105 => 4,
						106 => 4,
						108 => 4,
						109 => 4,
						110 => 4,
						111 => 4,
						117 => 4,
						118 => 4,
						119 => 4,
						120 => 4,
						121 => 4,
						122 => 4,
						123 => 4,
						124 => 4,
						125 => 4,
						126 => 4,
						127 => 4,
						128 => 4,
						129 => 4,
						130 => 4,
						131 => 4,
						132 => 4
					];

                    if(isset($item->categories) && count($item->categories->category)>1)
					{
						foreach($item->categories->category AS $category)
						{
							$category = (string)$category;
							if(isset($categoryMapper[$category]))
							{
								$category = $categoryMapper[$category];
								$categories[] = $category;
							}
						}
					}
					elseif((string)$item->categories->category!='')
					{
						$category = (string)$item->categories->category;
						if(isset($categoryMapper[$category]))
						{
							$category = $categoryMapper[$category];
							$categories[] = $category;
						}
					}

					$pidMapper = [
						"831" => 15,
						807 => 15,
						802 => 15,
						773 => 15,
						749 => 15,
						726 => 15,
						705 => 15,
						656 => 15,
						629 => 15,
						581 => 15,
						345 => 15,
						344 => 15,
						346 => 15,
						352 => 15,
						353 => 15,
						359 => 15,
						372 => 15,
						373 => 15,
						382 => 15,
						388 => 15,
						389 => 15,
						390 => 15,
						391 => 15,
						392 => 15,
						393 => 15,
						394 => 15,
						//585 => 15,
						//458 => 15,
						441 => 15,
						//447 => 15,
					];
                    $languageMapper = [
                    	0 => 0,
						2 => 1,
						3 => 2,
					];

					if (isset($pidMapper[(int)$item->pid]) && $pidMapper[(int)$item->pid] > 0 && isset($languageMapper[(int)$item->sys_language_uid])) {

						$doImport = true;
						$l10nParent = (int)$item->l10n_parent;
						$l10nState = "";
						if($l10nParent>0)
						{
							$defaultLanguageRecord = $newsRepository->findOneByImportSourceAndImportId(self::$importSource,$l10nParent);
							if(is_object($defaultLanguageRecord))
							{
								$l10nParent = $defaultLanguageRecord->getUid();
								if(count($images))
								{
									$l10nState = ['fal_media' => 'custom'];
								}
							}
							else
							{
								$doImport = false;
							}
						}
						if((int)$item->pid==441)
						{
							$datetime = mktime(0, 0, 0, 1, 1, (int)$item->title);
						}
						else
						{
							$datetime = (int)$item->datetime;
						}
						if($l10nParent!=0)
						{
							//$doImport = false;
						}

						if($doImport)
						{
							$items[] = [
								'uid' => (int)$item->uid,
								'pid' => $pidMapper[(int)$item->pid],
								'title' => (string)$item->title,
								'bodytext' => (string)$item->bodytext,
								'teaser' => (string)$item->short,
								'tstamp' => (int)$item->tstamp,
								'crdate' => (int)$item->crdate,
								'datetime' => $datetime,
								'starttime' => (int)$item->starttime,
								'endtime' => (int)$item->endtime,
								'hidden' => (int)$item->hidden,
								'archive' => (int)$item->archivedate,
								'author' => (string)$item->author,
								'author_email' => (string)$item->author_email,
								'images' => $images,
								'categories' => $categories,
								'sys_language_uid' => $languageMapper[(int)$item->sys_language_uid],
								'l10n_parent' => $l10nParent,
								'l10n_source' => $l10nParent,
								'l10n_state' => $l10nState,
								't3_origuid' => $l10nParent
							];
						}
					}
				}

				$cnt = 0;
				$inserted = [];
				$insertCnt = 0;
				$updateCnt = 0;
				$updated = [];
				$skipCnt = 0;

				foreach($items AS $item)
				{					
					$item['hash'] = md5(print_r($item,true));
					
					$grabber = $grabberService->getAction($item['uid'],$item['hash'],self::$forceUpdate);

					if(in_array($grabber['action'],['insert','update']) || ($grabber['action']=="skip" && self::$forceUpdate))
					{
						$news = [];
						$news['tstamp'] = $item['tstamp'];
						$news['crdate'] = $item['crdate'];
						$news['datetime'] = $item['datetime'];
						$news['hidden'] = $item['hidden'];
						$news['title'] = $item['title'];
						$news['teaser'] = $item['teaser'];
						$news['bodytext'] = $item['bodytext'];
						$news['author'] = $item['author'];
						$news['author_email'] = $item['author_email'];
						$news['starttime'] = $item['starttime'];
						$news['endtime'] = $item['endtime'];
						$news['archive'] = $item['archive'];
						$news['import_id'] = $item['uid'];
						$news['import_source'] = self::$importSource;
						$news['grabber_remote_uid'] = $item['uid'];
						$news['grabber_remote_hash'] = $item['hash'];
						$news['categories'] = $item['categories'];
						$news['type'] = "moox_news";
						$news['sys_language_uid'] = $item['sys_language_uid'];
						$news['l10n_parent'] = $item['l10n_parent'];
						$news['l10n_source'] = $item['l10n_source'];
						$news['t3_origuid'] = $item['t3_origuid'];

						if(in_array($grabber['action'],['insert']))
						{
							$news['l10n_state'] = $item['l10n_state'];
						}

						$news['images'] = [];
						foreach($item['images'] AS $image)
						{
							$news['images'][] = [
								'src' => (string)$image['url'],
								'caption' => (is_string($image['caption']))?$image['caption']:'',
								'alt' => (is_string($image['alt']))?$image['alt']:'',
								'title' => (is_string($image['title']))?$image['title']:''
							];
						}
					}

					if($grabber['action']=="insert")
					{
						$news['pid'] = $item['pid'];

						$grabberService->writeNews($news);

						$inserted[] = $news['title'];

						$insertCnt++;
					}
					elseif(self::$allowUpdate && ($grabber['action']=="update" || ($grabber['action']=="skip" && self::$forceUpdate)))
					{

						$news['uid'] = $grabber['uid'];
						$news['pid'] = $grabber['pid'];

						$grabberService->updateNews($news);

						$updated[] = $news['title'];

						$updateCnt++;						
					}
					else
					{
						$skipCnt++;
					}
					$cnt++;					
				}
				
				$messageTxt = $cnt." News verarbeitet | ".$insertCnt." News importiert | ".$updateCnt." News aktualisiert | ".$skipCnt." News übersprungen";
				
				$message = GeneralUtility::makeInstance('TYPO3\\CMS\\Core\\Messaging\\FlashMessage',
					$messageTxt,
					'', // the header is optional
					FlashMessage::OK, 
					TRUE 
				);
				$flashMessageQueue->addMessage($message);
				
			}
		}
		
		return $executionSucceeded;
	}
	
	/**
	 * This method returns the sleep duration as additional information
	 *
	 * @return string Information to display
	 */
	public function getAdditionalInformation()
	{		
		$info = "Default-Storage-PID: ".$this->getNewsPids();
		//LocalizationUtility::translate( 'LLL:EXT:moox_news_grabber_tt_news_papierzentrum/Resources/Private/Language/locallang_scheduler.xlf:tx_mooxnewsgrabberttnewsheco_tasks_grabbertask.news_pids_label', 'moox_news_grabber_tt_news' ).": ".$this->getNewsPids();
				
		return $info;
	}

	/**
	 * @return string
	 */
	public function getNewsPids()
	{
		return $this->newsPids;
	}

	/**
	 * @param string $newsPids news pids
	 */
	public function setNewsPids($newsPids)
	{
		$this->newsPids = $newsPids;
	}

	/**
	 * @param SimpleXMLElement $xmlObject
	 * @param array $out
	 */
	public function xml2array ( $xmlObject, $out = [] )
	{
		foreach ( (array) $xmlObject as $index => $node )
			$out[$index] = ( is_object ( $node ) ) ? $this->xml2array ( $node ) : $node;

		return $out;
	}
}
?>