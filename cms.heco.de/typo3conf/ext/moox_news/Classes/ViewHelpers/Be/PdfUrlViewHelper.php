<?php

namespace Neulandlotsen\MooxNews\ViewHelpers\Be;

/**
 * This file is part of the "moox_news" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */
 
use Neulandlotsen\MooxNews\Domain\Model\News;
use TYPO3\CMS\Core\Utility\GeneralUtility;

class PdfUrlViewHelper extends \TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper
{

    /**
     * @var string
     */
    protected $tagName = 'a';

    public function initializeArguments()
    {
        parent::initializeArguments();
        $this->registerArgument('newsItem', News::class, 'news item', true);
    }

    /**
     * @return string previewUrl
     */
    public function render()
    {
        $pdfUrl = '';

        $newsItem = $this->arguments['newsItem'];

		if(is_object($newsItem))
		{
			$pdfUrl = "/?type=66667&no_cache=1&tx_mooxnews_pi2[news]=".$newsItem->getUid()."&tx_mooxnews_pi2[hash]=".$newsItem->getHash()."&tx_mooxnews_pi2[inline]=1&tx_mooxnews_pi2[controller]=News&tx_mooxnews_pi2[action]=pdf";
		}
        
        return $pdfUrl;
        
    }
}
