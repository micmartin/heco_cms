<?php
namespace Neulandlotsen\MooxNews\ViewHelpers\Be;

/**
 * This file is part of the "moox_news" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

use TYPO3\CMS\Backend\Routing\UriBuilder;
use TYPO3\CMS\Backend\Utility\BackendUtility;
use TYPO3\CMS\Core\FormProtection\FormProtectionFactory;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3Fluid\Fluid\Core\Rendering\RenderingContextInterface;
use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper;
use TYPO3Fluid\Fluid\Core\ViewHelper\ViewHelperInterface;

/**
 * Edit Record ViewHelper, see FormEngine logic
 */
class EditRecordViewHelper extends AbstractViewHelper implements ViewHelperInterface
{

    public function initializeArguments()
    {
        parent::initializeArguments();
        $this->registerArgument('parameters', 'string', 'parameters', true);
        $this->registerArgument('redirectUrl', 'string', '', false, '');
    }

    /**
     * Returns a URL to link to FormEngine
     *
     * @return string URL to FormEngine module + parameters
     * @see \TYPO3\CMS\Backend\Utility\BackendUtility::getModuleUrl()
     */
    public function render()
    {
        return static::renderStatic(
            [
                'parameters' => $this->arguments['parameters'],
				'redirectUrl' => $this->arguments['redirectUrl']
            ],
            $this->buildRenderChildrenClosure(),
            $this->renderingContext
        );
    }

    /**
     * @param array $arguments
     * @param callable $renderChildrenClosure
     * @param RenderingContextInterface $renderingContext
     * @return string
     */
    public static function renderStatic(
        array $arguments,
        \Closure $renderChildrenClosure,
        RenderingContextInterface $renderingContext
    ) 
	{
        $parameters = GeneralUtility::explodeUrl2Array($arguments['parameters']);

        if($arguments['redirectUrl']=="")
		{
			$parameters['returnUrl'] = 'index.php?M=moox_MooxNewsMod1&id=' . (int)GeneralUtility::_GET('id')
				. '&moduleToken=' . FormProtectionFactory::get()->generateToken('moduleCall', 'moox_MooxNewsMod1');
		} 
		else
		{
			$parameters['returnUrl'] = $arguments['redirectUrl'];
		}
        $uriBuilder = GeneralUtility::makeInstance(UriBuilder::class);

        return $uriBuilder->buildUriFromRoute('record_edit', $parameters);
    }
}